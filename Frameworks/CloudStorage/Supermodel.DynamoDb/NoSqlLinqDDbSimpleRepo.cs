﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Linq2DynamoDb.DataContext;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.UIComponents;
using Supermodel.DDD.UnitOfWork;

namespace Supermodel.DynamoDb
{
    public class NoSqlLinqDDbSimpleRepo<EntityT> : INoSqlLinqDDbDataRepo<EntityT> where EntityT : class, IEntity, new()
    {
		#region Properties
        public virtual DataTable<EntityT> DataTable => (DataTable<EntityT>)UnitOfWorkContextCore.CurrentDbContext.Items<EntityT>();
        public virtual IQueryable<EntityT> Items => DataTable;
        #endregion

		#region Methods
        public virtual void Add(EntityT item)
        {
            if (item.Id == 0) DataTable.InsertOnSubmit(item);
        }
        
        public virtual void Delete(EntityT item)
        {
            DataTable.RemoveOnSubmit(item);
        }

        public virtual IEntity GetIEntityById(long id)
        {
            return GetById(id);
        }
        public virtual async Task<IEntity> GetIEntityByIdAsync(long id)
        {
            return await GetByIdAsync(id);
        }

        public virtual EntityT GetById(long id)
        {
            var item = GetByIdOrDefault(id);
            if (item == null) throw new SupermodelException("GetById can't find an entity");
            return item;
        }
        public virtual async Task<EntityT> GetByIdAsync(long id)
        {
            var item = await GetByIdOrDefaultAsync(id);
            if (item == null) throw new SupermodelException("GetById can't find an entity");
            return item;
        }

        public virtual EntityT GetByIdOrDefault(long id)
        {
            return DataTable.SingleOrDefault(p => p.Id == id);
        }
        public virtual async Task<EntityT> GetByIdOrDefaultAsync(long id)
        {
            return await DataTable.SingleOrDefaultAsync(p => p.Id == id);
        }

        public virtual IEnumerable<IEntity> GetIEntityAll()
        {
            return GetAll();
        }
        public virtual async Task<IEnumerable<IEntity>> GetIEntityAllAsync()
        {
            return await GetAllAsync();
        }

        public virtual List<EntityT> GetAll()
        {
            return Items.ToList();
        }
        public virtual async Task<List<EntityT>> GetAllAsync()
        {
            return await Items.ToListAsync();
        }

        public virtual List<SingleSelectMvcModel.Option> GetDropdownOptions<MvcModelT>() where MvcModelT : MvcModelForEntityCore
        {
            var entities = GetAll();
            return GetDropdownOptions<MvcModelT>(entities);
        }
        public virtual async Task<List<SingleSelectMvcModel.Option>> GetDropdownOptionsAsync<MvcModelT>() where MvcModelT : MvcModelForEntityCore
        {
            var entities = await GetAllAsync();
            return GetDropdownOptions<MvcModelT>(entities);
        }

        public virtual List<MultiSelectMvcModelCore.Option> GetMultiSelectOptions<MvcModelT>() where MvcModelT : MvcModelForEntityCore
        {
            var entities = GetAll();
            return GetMultiSelectOptions<MvcModelT>(entities);
        }
        public virtual async Task<List<MultiSelectMvcModelCore.Option>> GetMultiSelectOptionsAsync<MvcModelT>() where MvcModelT : MvcModelForEntityCore
        {
            var entities = await GetAllAsync();
            return GetMultiSelectOptions<MvcModelT>(entities);
        }

        public virtual List<MultiSelectMvcModelCore.Option> GetMultiSelectOptions<MvcModelT>(IEnumerable<EntityT> entities) where MvcModelT : MvcModelForEntityCore
        {
            var mvcModels = new List<MvcModelT>();
            mvcModels = (List<MvcModelT>)mvcModels.MapFromObject(entities);
            mvcModels = mvcModels.OrderBy(p => p.Label).ToList();
            return !mvcModels.Any() ?
                new List<MultiSelectMvcModelCore.Option>() :
                mvcModels.Select(item => new MultiSelectMvcModelCore.Option(item.Id.ToString(CultureInfo.InvariantCulture), item.Label, item.IsDisabled)).ToList();
        }
        public virtual List<SingleSelectMvcModel.Option> GetDropdownOptions<MvcModelT>(IEnumerable<EntityT> entities) where MvcModelT : MvcModelForEntityCore
        {
            var mvcModels = new List<MvcModelT>();
            mvcModels = (List<MvcModelT>)mvcModels.MapFromObject(entities);
            mvcModels = mvcModels.OrderBy(p => p.Label).ToList();
            return !mvcModels.Any() ?
                new List<SingleSelectMvcModel.Option>() :
                mvcModels.Select(item => new SingleSelectMvcModel.Option(item.Id.ToString(CultureInfo.InvariantCulture), item.Label, item.IsDisabled)).ToList();
        }
        #endregion
    }
}
