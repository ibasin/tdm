﻿using Supermodel.DDD.UnitOfWork;

namespace Supermodel.DynamoDb
{
    public class DDbContextTransaction : IDbContextTransaction
    {
        public void Dispose()
        {
            //Do nothing. DDb does not suport transcations.
        }

        public void CommitIfApplies()
        {
            //Do nothing. DDb does not suport transcations.
        }

        public void RollbackIfApplies()
        {
            //Do nothing. DDb does not suport transcations.
        }
    }
}
