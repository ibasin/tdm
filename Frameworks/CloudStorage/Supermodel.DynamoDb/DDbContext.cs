﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;
using Linq2DynamoDb.DataContext;
using Linq2DynamoDb.DataContext.Caching;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Repository;
using Supermodel.DDD.UnitOfWork;

namespace Supermodel.DynamoDb
{
    public abstract class DDbContext : DataContext, IDbContext
    {
        #region Constructors
        protected DDbContext(IAmazonDynamoDB client, string tableNamePrefix) : base(client, tableNamePrefix)
        {
            CommitOnDispose = true;
            IsReadOnly = false;
            IsCompletedAndFinalized = false;
            CustomValues = new Dictionary<string, object>();
        }
        #endregion

        #region Methods
        //Returns the assemblies containing entities and entity config classes for this unit of work. Default returns AppDomain.CurrentDomain.GetAssemblies(). Override it for finer control 
        protected virtual Assembly[] GetDomainEntitiesAssemblies() { return AppDomain.CurrentDomain.GetAssemblies(); }

        public void InitStorageIfApplies()
        {
            //Set up configurations that are derived from DDbDataModelConfigForEntity
            foreach (var assembly in GetDomainEntitiesAssemblies())
            {
                Type[] assemblyTypes;
                try { assemblyTypes = assembly.GetTypes(); }
                catch (ReflectionTypeLoadException) { continue; }
                
                foreach (var assemblyType in assemblyTypes)
                {
                    if (assemblyType.IsAbstract || assemblyType.IsGenericType) continue;

                    var baseTypeOfDDbDataModelConfigForEntity = ReflectionHelper.IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(assemblyType, typeof(DDbDataModelConfigForEntity<>));

                    if (baseTypeOfDDbDataModelConfigForEntity != null)
                    {
                        var instanceOfDDbDataModelConfigForEntityInstance = ReflectionHelper.CreateType(assemblyType);
                        instanceOfDDbDataModelConfigForEntityInstance.ExecuteMethod("CreateTableIfNotExists", this);
                    }
                }
            }
        }

        public int FinalSaveChanges()
        {
            int result;
            try
            {
                result = SaveChanges();
            }
            finally
            {
                MakeCompletedAndFinalized();
            }
            return result;
        }
        public async Task<int> FinalSaveChangesAsync()
        {
            int result;
            try
            {
                result = await SaveChangesAsync();
            }
            finally
            {
                MakeCompletedAndFinalized();
            }
            return result;
        }

        public int SaveChanges()
        {
            try
            {
                if (!CommitOnDispose || IsReadOnly || IsCompletedAndFinalized) return 0;
                SubmitChanges();
                return 1;
            }
            // ReSharper disable RedundantCatchClause
            #pragma warning disable 168
            catch (Exception ex)
            {
                throw; //we need this for debugging
            }
            #pragma warning restore 168
            // ReSharper restore RedundantCatchClause
        }
        public async Task<int> SaveChangesAsync()
        {
            try
            {
                if (!CommitOnDispose || IsReadOnly || IsCompletedAndFinalized) return 0;
                await this.SubmitChangesAsync();
                return 1;
            }
            // ReSharper disable RedundantCatchClause
            #pragma warning disable 168
            catch (Exception ex)
            {
                throw; //we need this for debugging
            }
            #pragma warning restore 168
            // ReSharper restore RedundantCatchClause
        }

        public IQueryable<EntityT> Items<EntityT>() where EntityT : class, IEntity, new()
        {
            var cacheImplementation = GetCacheImplementation<EntityT>();
            return cacheImplementation != null ? GetTable<EntityT>(cacheImplementation) : GetTable<EntityT>();
        }

        public virtual IDataRepo<EntityT> CreateRepo<EntityT>() where EntityT : class, IEntity, new()
        {
            if (CustomRepoFactoryList != null)
            {
                foreach (var customFactory in CustomRepoFactoryList)
                {
                    var repo = customFactory.CreateRepo<EntityT>();
                    if (repo != null) return repo;
                }
            }
            return new NoSqlLinqDDbSimpleRepo<EntityT>();
        }
        protected virtual List<IRepoFactory> CustomRepoFactoryList => null;

        public IDbContextTransaction BeginTransactionIfApplies()
        {
            return new DDbContextTransaction();
        }

        public void Dispose()
        {
            if (CommitOnDispose && !IsReadOnly && !IsCompletedAndFinalized) SaveChanges();
        }
        #endregion

        #region Properties
        public bool CommitOnDispose { get; set; }
        public bool IsReadOnly { get; protected set; }
        public void MakeReadOnly()
        {
            IsReadOnly = true;
        }
        public bool IsCompletedAndFinalized { get; protected set; }
        public void MakeCompletedAndFinalized()
        {
            IsCompletedAndFinalized = true;
        }

        //() => new EnyimTableCache(_cacheClient, TimeSpan.FromMinutes(30))
        public abstract Func<ITableCache> GetCacheImplementation<EntityT>() where EntityT : class, IEntity, new();
        public virtual ProvisionedThroughput MaxHashIdTableDefaultThroughput => new ProvisionedThroughput(10, 10);

        public Dictionary<string, object> CustomValues { get; }
        #endregion
    }
}
