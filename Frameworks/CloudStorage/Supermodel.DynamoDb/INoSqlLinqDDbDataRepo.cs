﻿using Linq2DynamoDb.DataContext;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Repository;

namespace Supermodel.DynamoDb
{
    public interface INoSqlLinqDDbDataRepo<EntityT> : ILinqDataRepo<EntityT> where EntityT : class, IEntity, new()
    {
        DataTable<EntityT> DataTable { get; }
    }
}
