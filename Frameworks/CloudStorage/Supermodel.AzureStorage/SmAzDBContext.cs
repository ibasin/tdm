﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Repository;
using Supermodel.DDD.UnitOfWork;

namespace Supermodel.AzureStorage
{
    public class SmAzDBContext : IDbContext
    {
        public void Dispose()
        {
            throw new System.NotImplementedException();
        }

        public void InitStorageIfApplies()
        {
            throw new System.NotImplementedException();
        }

        public int FinalSaveChanges()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> FinalSaveChangesAsync()
        {
            throw new System.NotImplementedException();
        }

        public int SaveChanges()
        {
            throw new System.NotImplementedException();
        }

        public Task<int> SaveChangesAsync()
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<EntityT> Items<EntityT>() where EntityT : class, IEntity, new()
        {
            throw new System.NotImplementedException();
        }

        public IDataRepo<EntityT> CreateRepo<EntityT>() where EntityT : class, IEntity, new()
        {
            throw new System.NotImplementedException();
        }

        public IDbContextTransaction BeginTransactionIfApplies()
        {
            throw new System.NotImplementedException();
        }

        public bool CommitOnDispose { get; set; }
        public bool IsReadOnly { get; }
        public void MakeReadOnly()
        {
            throw new System.NotImplementedException();
        }

        public bool IsCompletedAndFinalized { get; }
        public void MakeCompletedAndFinalized()
        {
            throw new System.NotImplementedException();
        }

        public Dictionary<string, object> CustomValues { get; }
    }
}
