﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Microsoft.WindowsAzure.Storage.Table.Protocol;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;

namespace Supermodel.AzureStorage
{
    public class SmAzTableEntity : Entity, ITableEntity
    {
        #region Constructors
        public SmAzTableEntity()
        {
        }
        #endregion

        #region Methods
        public virtual void ReadEntity(IDictionary<string, EntityProperty> properties, OperationContext operationContext)
        {
            IEnumerable<PropertyInfo> objectProperties = GetType().GetProperties();
            foreach (var property in objectProperties)
            {
                // reserved properties
                if (property.Name == TableConstants.PartitionKey || property.Name == TableConstants.RowKey || property.Name == TableConstants.Timestamp || property.Name == "ETag")
                {
                    continue;
                }

                // Enforce public getter / setter
                if (property.GetSetMethod() == null || !property.GetSetMethod().IsPublic || property.GetGetMethod() == null || !property.GetGetMethod().IsPublic)
                {
                    continue;
                }

                //if property shpuld be ignored
                if (Attribute.IsDefined(property, typeof(IgnorePropertyAttribute)) || Attribute.IsDefined(property, typeof(NotMappedAttribute)))
                {
                    continue;
                }

                // only proceed with properties that have a corresponding entry in the dictionary
                if (!properties.ContainsKey(property.Name))
                {
                    continue;
                }

                var entityProperty = properties[property.Name];

                if ((bool)entityProperty.PropertyGetNonPublic("IsNull"))
                {
                    property.SetValue(this, null, null);
                }
                else
                {
                    switch (entityProperty.PropertyType)
                    {
                        case EdmType.String:
                            if (property.PropertyType != typeof(string) && property.PropertyType != typeof(String))
                            {
                                continue;
                            }

                            property.SetValue(this, entityProperty.StringValue, null);
                            break;
                        case EdmType.Binary:
                            if (property.PropertyType != typeof(byte[]))
                            {
                                continue;
                            }

                            property.SetValue(this, entityProperty.BinaryValue, null);
                            break;
                        case EdmType.Boolean:
                            if (property.PropertyType != typeof(bool) && property.PropertyType != typeof(Boolean) && property.PropertyType != typeof(Boolean?) && property.PropertyType != typeof(bool?))
                            {
                                continue;
                            }

                            property.SetValue(this, entityProperty.BooleanValue, null);
                            break;
                        case EdmType.DateTime:
                            if (property.PropertyType == typeof(DateTime))
                            {
                                // ReSharper disable once PossibleInvalidOperationException
                                property.SetValue(this, entityProperty.DateTimeOffsetValue.Value.UtcDateTime, null);
                            }
                            else if (property.PropertyType == typeof(DateTime?))
                            {
                                property.SetValue(this, entityProperty.DateTimeOffsetValue.HasValue ? entityProperty.DateTimeOffsetValue.Value.UtcDateTime : (DateTime?)null, null);
                            }
                            else if (property.PropertyType == typeof(DateTimeOffset))
                            {
                                // ReSharper disable once PossibleInvalidOperationException
                                property.SetValue(this, entityProperty.DateTimeOffsetValue.Value, null);
                            }
                            else if (property.PropertyType == typeof(DateTimeOffset?))
                            {
                                property.SetValue(this, entityProperty.DateTimeOffsetValue, null);
                            }

                            break;
                        case EdmType.Double:
                            if (property.PropertyType != typeof(double) && property.PropertyType != typeof(Double) && property.PropertyType != typeof(Double?) && property.PropertyType != typeof(double?))
                            {
                                continue;
                            }

                            property.SetValue(this, entityProperty.DoubleValue, null);
                            break;
                        case EdmType.Guid:
                            if (property.PropertyType != typeof(Guid) && property.PropertyType != typeof(Guid?))
                            {
                                continue;
                            }

                            property.SetValue(this, entityProperty.GuidValue, null);
                            break;
                        case EdmType.Int32:
                            if (property.PropertyType != typeof(int) && property.PropertyType != typeof(Int32) && property.PropertyType != typeof(Int32?) && property.PropertyType != typeof(int?))
                            {
                                continue;
                            }

                            property.SetValue(this, entityProperty.Int32Value, null);
                            break;
                        case EdmType.Int64:
                            if (property.PropertyType != typeof(long) && property.PropertyType != typeof(Int64) && property.PropertyType != typeof(long?) && property.PropertyType != typeof(Int64?))
                            {
                                continue;
                            }

                            property.SetValue(this, entityProperty.Int64Value, null);
                            break;
                    }
                }
            }
        }
        public virtual IDictionary<string, EntityProperty> WriteEntity(OperationContext operationContext)
        {
            var retVals = new Dictionary<string, EntityProperty>();
            IEnumerable<PropertyInfo> objectProperties = GetType().GetProperties();

            foreach (var property in objectProperties)
            {
                // reserved properties
                if (property.Name == TableConstants.PartitionKey || property.Name == TableConstants.RowKey || property.Name == TableConstants.Timestamp || property.Name == "ETag")
                {
                    continue;
                }

                // Enforce public getter / setter
                if (property.GetSetMethod() == null || !property.GetSetMethod().IsPublic || property.GetGetMethod() == null || !property.GetGetMethod().IsPublic)
                {
                    continue;
                }

                //if property shpuld be ignored
                if (Attribute.IsDefined(property, typeof(IgnorePropertyAttribute)) || Attribute.IsDefined(property, typeof(NotMappedAttribute)))
                {
                    continue;
                }

                var newProperty = CreateEntityPropertyFromObject(property.GetValue(this, null), false);
                //var newProperty = (EntityProperty)ReflectionHelper.ExecuteStaticMethod(typeof(EntityProperty), "CreateEntityPropertyFromObject", property.GetValue(this, null), false);

                // property will be null if unknown type
                if (newProperty != null)
                {
                    retVals.Add(property.Name, newProperty);
                }
            }

            return retVals;
        }
        private static EntityProperty CreateEntityPropertyFromObject(object value, bool allowUnknownTypes)
        {
            if (value is string) return new EntityProperty((string)value);
            if (value is byte[]) return new EntityProperty((byte[])value);
            if (value is bool) return new EntityProperty(new bool?((bool)value));
            if (value is bool?) return new EntityProperty((bool?)value);
            if (value is System.DateTime) return new EntityProperty(new System.DateTime?((System.DateTime)value));
            if (value is System.DateTime?) return new EntityProperty((System.DateTime?)value);
            if (value is DateTimeOffset) return new EntityProperty(new DateTimeOffset?((DateTimeOffset)value));
            if (value is DateTimeOffset?) return new EntityProperty((DateTimeOffset?)value);
            if (value is double) return new EntityProperty(new double?((double)value));
            if (value is double?) return new EntityProperty((double?)value);
            if (value is Guid?) return new EntityProperty((Guid?)value);
            if (value is Guid) return new EntityProperty(new Guid?((Guid)value));
            if (value is int) return new EntityProperty(new int?((int)value));
            if (value is int?) return new EntityProperty((int?)value);
            if (value is long) return new EntityProperty(new long?((long)value));
            if (value is long?) return new EntityProperty((long?)value);
            if (value == null) return new EntityProperty((string)null);
            if (allowUnknownTypes) return new EntityProperty(value.ToString());
            return (EntityProperty)null;
        }
        #endregion

        #region Properties
        [IgnoreProperty] public override long Id { get { return base.Id; } protected set { base.Id = value;  } }
        public virtual string PartitionKey { get; set; } = "SmAz-Default-Partition";
        public virtual string RowKey
        {
            get { return Id.ToString(); }
            set
            {
                try { Id = long.Parse(value); }
                catch (Exception) { throw new SupermodelException("Default Supermodel implementation for RowKey must be an string parsable to integer"); }
            }
        }
        public virtual DateTimeOffset Timestamp { get; set; }
        public virtual string ETag { get; set; }
        #endregion
    }
}
