﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.DynamoDBv2.Model;
using Linq2DynamoDb.DataContext.Utils;
using ReflectionMapper;
using Supermodel;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.UnitOfWork;

namespace Linq2DynamoDb.DataContext
{
    public class CapacityUpdateException : SupermodelException
    {
        public CapacityUpdateException(string msg) : base(msg) { }
    }

    public static class SupermodelExtensions
    {
        #region Validation, BeforeSave & AssignId add-ons
        public static void ValidateUpdates(Table tableDefinition, List<object> addedObjects, List<object> modifiedObjects, List<object> removedObjects)
        {
            //Call BeforeSave for all changes
            foreach (var obj in addedObjects)
            {
                var entity = obj as IEntity;
                entity?.BeforeSave(EntityState.Added);
            }
            foreach (var obj in modifiedObjects)
            {
                var entity = obj as IEntity;
                entity?.BeforeSave(EntityState.Modified);
            }
            foreach (var obj in removedObjects)
            {
                var entity = obj as IEntity;
                entity?.BeforeSave(EntityState.Deleted);
            }

            //Run Validations
            var vr = new ValidationResultList();
            foreach (var obj in addedObjects)
            {
                if (!Validator.TryValidateObject(obj, new ValidationContext(obj), vr)) ThrowDbEntityValidationException(vr);
            }
            foreach (var obj in modifiedObjects)
            {
                if (!Validator.TryValidateObject(obj, new ValidationContext(obj), vr)) ThrowDbEntityValidationException(vr);
            }
        }

        public static void AssignHashIds(Table tableDefinition, List<object> addedObjects, Dictionary<EntityKey, Document> addedEntities, Dictionary<EntityKey, IEntityWrapper> addedEntitiesDictionary)
        {
            var addedObjectsCount = addedObjects.Count;
            if (addedObjectsCount == 0) return;
            var lastNewHashId = GetNextHashIds(tableDefinition, addedObjectsCount);
            var newId = lastNewHashId - addedObjectsCount;
            foreach (var obj in addedObjects)
            {
                var entity = (IEntity)obj;

                var oldTempid = entity.Id;
                var oldTempEntityKey = new EntityKey(oldTempid.ToPrimitive(typeof(int)));

                newId++;
                var newEntityKey = new EntityKey(newId.ToPrimitive(typeof(int)));

                //Replace document in the addedEntities dictionary with a corrected one
                var document = addedEntities[oldTempEntityKey];
                document["Id"] = newId.ToPrimitive(typeof(int));
                addedEntities.Remove(oldTempEntityKey);
                addedEntities.Add(newEntityKey, document);

                //Replace document in the addedEntitiesDictionary dictionary with a corrected one
                var entityWrapper = addedEntitiesDictionary[oldTempEntityKey];
                entityWrapper.Entity.PropertySet("Id", newId); //Entity is fixed here
                addedEntitiesDictionary.Remove(oldTempEntityKey);
                addedEntitiesDictionary.Add(newEntityKey, entityWrapper);
            }
        }
        public static async Task AssignHashIdsAsync(Table tableDefinition, List<object> addedObjects, Dictionary<EntityKey, Document> addedEntities, Dictionary<EntityKey, IEntityWrapper> addedEntitiesDictionary)
        {
            var addedObjectsCount = addedObjects.Count;
            if (addedObjectsCount == 0) return;
            var lastNewHashId = await GetNextHashIdsAsync(tableDefinition, addedObjectsCount);
            var newId = lastNewHashId - addedObjectsCount;
            foreach (var obj in addedObjects)
            {
                var entity = (IEntity)obj;

                var oldTempid = entity.Id;
                var oldTempEntityKey = new EntityKey(oldTempid.ToPrimitive(typeof(int)));

                newId++;
                var newEntityKey = new EntityKey(newId.ToPrimitive(typeof(int)));

                //Replace document in the addedEntities dictionary with a corrected one
                var document = addedEntities[oldTempEntityKey];
                document["Id"] = newId.ToPrimitive(typeof(int));
                addedEntities.Remove(oldTempEntityKey);
                addedEntities.Add(newEntityKey, document);

                //Replace document in the addedEntitiesDictionary dictionary with a corrected one
                var entityWrapper = addedEntitiesDictionary[oldTempEntityKey];
                entityWrapper.Entity.PropertySet("Id", newId); //Entity is fixed here
                addedEntitiesDictionary.Remove(oldTempEntityKey);
                addedEntitiesDictionary.Add(newEntityKey, entityWrapper);
            }
        }

        private static int GetNextHashIds(Table tableDefinition, int incrementBy = 1)
        {
            var context = (DataContext)UnitOfWorkContextCore.CurrentDbContext;
            var entityTableName = tableDefinition.TableName;

            var request = new UpdateItemRequest
            {
                Key = new Dictionary<string, AttributeValue> { { "Table", new AttributeValue { S = entityTableName } } },
                ExpressionAttributeNames = new Dictionary<string, string>
                {
                    {"#A", "CurrentHashId"}
                },
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                {
                    {":incr", new AttributeValue { N = incrementBy.ToString() }}
                },
                UpdateExpression = "SET #A = #A + :incr",
                TableName = context.MaxHashIdTableNameWithPrefix,
                ReturnValues = new ReturnValue("UPDATED_NEW")
            };
            var response = context.Client.UpdateItem(request);
            var currentHashId = int.Parse(response.Attributes["CurrentHashId"].N);
            return currentHashId;
        }
        private static async Task<int> GetNextHashIdsAsync(Table tableDefinition, int incrementBy = 1)
        {
            var context = (DataContext)UnitOfWorkContextCore.CurrentDbContext;
            var entityTableName = tableDefinition.TableName;

            var request = new UpdateItemRequest
            {
                Key = new Dictionary<string, AttributeValue> { { "Table", new AttributeValue { S = entityTableName } } },
                ExpressionAttributeNames = new Dictionary<string, string>
                {
                    {"#A", "CurrentHashId"}
                },
                ExpressionAttributeValues = new Dictionary<string, AttributeValue>
                {
                    {":incr", new AttributeValue { N = incrementBy.ToString() }}
                },
                UpdateExpression = "SET #A = #A + :incr",
                TableName = context.MaxHashIdTableNameWithPrefix,
                ReturnValues = new ReturnValue("UPDATED_NEW")
            };
            var response = await context.Client.UpdateItemAsync(request);
            var currentHashId = int.Parse(response.Attributes["CurrentHashId"].N);
            return currentHashId;
        }

        private static void ThrowDbEntityValidationException(ValidationResultList vr)
        {
            var error = vr.Single();
            var message = error.ErrorMessage;
            var fieldsStr = "";
            var first = true;
            foreach (var field in error.MemberNames)
            {
                if (first)
                {
                    first = false;
                    fieldsStr+= field;
                }
                else
                {
                    fieldsStr+= ", " + field;
                }
            }

            throw new DbEntityValidationException($"Validation Error: '{message}'; Affected Properties(s): {fieldsStr}" );
        }
        #endregion

        #region Data Context Extensions

        #region GetTableNameForType
        public static string GetTableNameForType<EntityT>(this DataContext context)
        {
            return context.GetTableNameForType(typeof(EntityT));
        }
        public static string GetTableNameForType(this DataContext context, Type entityType)
        {
            return context.GetTableNameForType(entityType);
        }
        #endregion

        #region TableExists methods
        public static bool TableExists(this DataContext context, Type entityType)
        {
            return context.TableExists(context.GetTableNameForType(entityType));
        }
        public static bool TableExists<EntityT>(this DataContext context)
        {
            return context.TableExists(typeof(EntityT));
        }
        public static bool TableExists(this DataContext context, string tableName)
        {
            Table t;
            return Table.TryLoadTable(context.Client, tableName, out t);
        }
        #endregion

        #region SetThroughput methods
        public const int MaxNumberOfDecreases = 4;

        public static void UpdateTableThroughput<TableT>(this DataContext context, ProvisionedThroughput tableNewThroughput, params UpdateGlobalSecondaryIndexAction[] gsiUpdates)
        {
            var tableName = context.GetTableNameForType<TableT>();
            context.UpdateTableThroughput(tableName, tableNewThroughput, gsiUpdates);
        }
        public static Task UpdateTableThroughputAsync<TableT>(this DataContext context, ProvisionedThroughput tableNewThroughput, params UpdateGlobalSecondaryIndexAction[] gsiUpdates)
        {
            var tableName = context.GetTableNameForType<TableT>();
            return context.UpdateTableThroughputAsync(tableName, tableNewThroughput, gsiUpdates);
        }

        public static void UpdateMaxHashIdsTableThroughput(this DataContext context, ProvisionedThroughput tableNewThroughput)
        {
            var tableName = context.MaxHashIdTableNameWithPrefix;
            context.UpdateTableThroughput(tableName, tableNewThroughput);
        }
        public static Task UpdateMaxHashIdsTableThroughputAsync(this DataContext context, ProvisionedThroughput tableNewThroughput)
        {
            var tableName = context.MaxHashIdTableNameWithPrefix;
            return context.UpdateTableThroughputAsync(tableName, tableNewThroughput);
        }

        public static void UpdateTableThroughput(this DataContext context, Type entityType, ProvisionedThroughput tableNewThroughput, params UpdateGlobalSecondaryIndexAction[] gsiUpdates)
        {
            var tableName = context.GetTableNameForType(entityType);
            context.UpdateTableThroughput(tableName, tableNewThroughput, gsiUpdates);
        }
        public static Task UpdateTableThroughputAsync(this DataContext context, Type entityType, ProvisionedThroughput tableNewThroughput, params UpdateGlobalSecondaryIndexAction[] gsiUpdates)
        {
            var tableName = context.GetTableNameForType(entityType);
            return context.UpdateTableThroughputAsync(tableName, tableNewThroughput, gsiUpdates);
        }

        public static void UpdateTableThroughput(this DataContext context, string tabelName, ProvisionedThroughput tableNewThroughput, params UpdateGlobalSecondaryIndexAction[] gsiIndexActions)
        {
            var tableDecsription = context.GetTableDescription(tabelName);

            //Are we increasing capacity for the table?
            var capacityIncrease = tableDecsription.ProvisionedThroughput.ReadCapacityUnits < tableNewThroughput.ReadCapacityUnits ||
                                   tableDecsription.ProvisionedThroughput.ReadCapacityUnits < tableNewThroughput.ReadCapacityUnits;

            //Are we going to be able to decrease table capacity afterwards?
            var cantDecreaseCapacityAnymore = tableDecsription.ProvisionedThroughput.NumberOfDecreasesToday >= MaxNumberOfDecreases;

            var gsiUpdates = new List<GlobalSecondaryIndexUpdate>();
            if (gsiIndexActions != null)
            {
                foreach (var gsiIndexAction in gsiIndexActions)
                {
                    //Get index info
                    var gsiIndexDecsription = tableDecsription.GlobalSecondaryIndexes.SingleOrDefault(x => x.IndexName == gsiIndexAction.IndexName);
                    if (gsiIndexDecsription == null) throw new CapacityUpdateException($"GSI '{gsiIndexAction.IndexName}' does not exist for table '{tabelName}'");

                    //Are we increasing capacity for the index?
                    capacityIncrease |= gsiIndexDecsription.ProvisionedThroughput.ReadCapacityUnits < gsiIndexAction.ProvisionedThroughput.ReadCapacityUnits ||
                                        gsiIndexDecsription.ProvisionedThroughput.ReadCapacityUnits < gsiIndexAction.ProvisionedThroughput.ReadCapacityUnits;

                    //Are we going to be able to decrease table capacity afterwards?
                    cantDecreaseCapacityAnymore |= gsiIndexDecsription.ProvisionedThroughput.NumberOfDecreasesToday >= MaxNumberOfDecreases;

                    //If anything changed, add that update
                    if (gsiIndexDecsription.ProvisionedThroughput.ReadCapacityUnits != gsiIndexAction.ProvisionedThroughput.ReadCapacityUnits ||
                        gsiIndexDecsription.ProvisionedThroughput.ReadCapacityUnits != gsiIndexAction.ProvisionedThroughput.ReadCapacityUnits)
                    {
                        gsiUpdates.Add(new GlobalSecondaryIndexUpdate { Update = gsiIndexAction });
                    }
                }
            }
            
            //Did we change the throughput for the table?
            if (tableDecsription.ProvisionedThroughput.ReadCapacityUnits == tableNewThroughput.ReadCapacityUnits &&
                tableDecsription.ProvisionedThroughput.ReadCapacityUnits == tableNewThroughput.ReadCapacityUnits)
            {
                tableNewThroughput = null;
            }

            //If we have nohting to update (all is the same), we are done
            if (tableNewThroughput == null && !gsiUpdates.Any()) return;
            
            //If we are increasing but won't be able to decrease, don't do it and throw exception
            if (capacityIncrease && cantDecreaseCapacityAnymore) throw new CapacityUpdateException("This capacity increase cannot be decreased today and won't be allowed");

            var tableUpdateRequest = new UpdateTableRequest { TableName = tabelName, ProvisionedThroughput = tableNewThroughput, GlobalSecondaryIndexUpdates = gsiUpdates };
            context.UpdateTable(tableUpdateRequest);
        }
        public static async Task UpdateTableThroughputAsync(this DataContext context, string tabelName, ProvisionedThroughput tableNewThroughput, params UpdateGlobalSecondaryIndexAction[] gsiIndexActions)
        {
            var tableDecsription = await context.GetTableDescriptionAsync(tabelName);

            //Are we increasing capacity for the table?
            var capacityIncrease = tableDecsription.ProvisionedThroughput.ReadCapacityUnits < tableNewThroughput.ReadCapacityUnits ||
                                   tableDecsription.ProvisionedThroughput.ReadCapacityUnits < tableNewThroughput.ReadCapacityUnits;

            //Are we going to be able to decrease table capacity afterwards?
            var cantDecreaseCapacityAnymore = tableDecsription.ProvisionedThroughput.NumberOfDecreasesToday >= MaxNumberOfDecreases;

            var gsiUpdates = new List<GlobalSecondaryIndexUpdate>();
            if (gsiIndexActions != null)
            {
                foreach (var gsiIndexAction in gsiIndexActions)
                {
                    //Get index info
                    var gsiIndexDecsription = tableDecsription.GlobalSecondaryIndexes.SingleOrDefault(x => x.IndexName == gsiIndexAction.IndexName);
                    if (gsiIndexDecsription == null) throw new CapacityUpdateException($"GSI '{gsiIndexAction.IndexName}' does not exist for table '{tabelName}'");

                    //Are we increasing capacity for the index?
                    capacityIncrease |= gsiIndexDecsription.ProvisionedThroughput.ReadCapacityUnits < gsiIndexAction.ProvisionedThroughput.ReadCapacityUnits ||
                                        gsiIndexDecsription.ProvisionedThroughput.ReadCapacityUnits < gsiIndexAction.ProvisionedThroughput.ReadCapacityUnits;

                    //Are we going to be able to decrease table capacity afterwards?
                    cantDecreaseCapacityAnymore |= gsiIndexDecsription.ProvisionedThroughput.NumberOfDecreasesToday >= MaxNumberOfDecreases;

                    //If anything changed, add that update
                    if (gsiIndexDecsription.ProvisionedThroughput.ReadCapacityUnits != gsiIndexAction.ProvisionedThroughput.ReadCapacityUnits ||
                        gsiIndexDecsription.ProvisionedThroughput.ReadCapacityUnits != gsiIndexAction.ProvisionedThroughput.ReadCapacityUnits)
                    {
                        gsiUpdates.Add(new GlobalSecondaryIndexUpdate { Update = gsiIndexAction });
                    }
                }
            }
            
            //Did we change the throughput for the table?
            if (tableDecsription.ProvisionedThroughput.ReadCapacityUnits == tableNewThroughput.ReadCapacityUnits &&
                tableDecsription.ProvisionedThroughput.ReadCapacityUnits == tableNewThroughput.ReadCapacityUnits)
            {
                tableNewThroughput = null;
            }

            //If we have nohting to update (all is the same), we are done
            if (tableNewThroughput == null && !gsiUpdates.Any()) return;
            
            //If we are increasing but won't be able to decrease, don't do it and throw exception
            if (capacityIncrease && cantDecreaseCapacityAnymore) throw new CapacityUpdateException("This capacity increase cannot be decreased today and won't be allowed");

            var tableUpdateRequest = new UpdateTableRequest { TableName = tabelName, ProvisionedThroughput = tableNewThroughput, GlobalSecondaryIndexUpdates = gsiUpdates };
            await context.UpdateTableAsync(tableUpdateRequest);
        }
        #endregion

        #region TableDescription methods
        public static TableDescription GetTableDescription<TableT>(this DataContext context)
        {
            var tableName = context.GetTableNameForType(typeof(TableT));
            return context.GetTableDescription(tableName);
        }
        public static Task<TableDescription> GetTableDescriptionAsync<TableT>(this DataContext context)
        {
            var tableName = context.GetTableNameForType(typeof(TableT));
            return context.GetTableDescriptionAsync(tableName);
        }

        public static TableDescription GetMaxHashIdsTableDescription(this DataContext context)
        {
            var tableName = context.MaxHashIdTableNameWithPrefix;
            return context.GetTableDescription(tableName);
        }
        public static Task<TableDescription> GetMaxHashIdsTableDescriptionAsync(this DataContext context)
        {
            var tableName = context.MaxHashIdTableNameWithPrefix;
            return context.GetTableDescriptionAsync(tableName);
        }

        public static TableDescription GetTableDescription(this DataContext context, string tableName)
        {
            var request = new DescribeTableRequest { TableName = tableName };
            var response = context.Client.DescribeTable(request);
            return response?.Table;
        }
        public static async Task<TableDescription> GetTableDescriptionAsync(this DataContext context, string tableName)
        {
            var request = new DescribeTableRequest { TableName = tableName };
            var response = await context.Client.DescribeTableAsync(request);
            return response?.Table;
        }
        #endregion

        #region UpdateTable methods
        public static UpdateTableResponse UpdateTable<TableT>(this DataContext context, UpdateTableRequest tableUpdateRequest)
        {
            tableUpdateRequest.TableName = context.GetTableNameForType(typeof(TableT));
            return context.UpdateTable(tableUpdateRequest);
        }
        public static Task<UpdateTableResponse> UpdateTableAsync<TableT>(this DataContext context, UpdateTableRequest tableUpdateRequest)
        {
            tableUpdateRequest.TableName = context.GetTableNameForType(typeof(TableT));
            return context.UpdateTableAsync(tableUpdateRequest);
        }

        public static UpdateTableResponse UpdateMaxHashIdsTable(this DataContext context, UpdateTableRequest tableUpdateRequest)
        {
            tableUpdateRequest.TableName = context.MaxHashIdTableNameWithPrefix;
            return context.UpdateTable(tableUpdateRequest);
        }
        public static Task<UpdateTableResponse> UpdateMaxHashIdsTableAsync(this DataContext context, UpdateTableRequest tableUpdateRequest)
        {
            tableUpdateRequest.TableName = context.MaxHashIdTableNameWithPrefix;
            return context.UpdateTableAsync(tableUpdateRequest);
        }

        public static UpdateTableResponse UpdateTable(this DataContext context, UpdateTableRequest tableUpdateRequest)
        {
            var response = context.Client.UpdateTable(tableUpdateRequest);
            context.WaitUntilTableReady(tableUpdateRequest.TableName);
            return response;
        }
        public static async Task<UpdateTableResponse> UpdateTableAsync(this DataContext context, UpdateTableRequest tableUpdateRequest)
        {
            var response = await context.Client.UpdateTableAsync(tableUpdateRequest);
            await context.WaitUntilTableReadyAsync(tableUpdateRequest.TableName);
            return response;
        }
        #endregion

        #region WaitUntilTableReady methods
        public static void WaitUntilTableReady<TableT>(this DataContext context)
        {
            context.WaitUntilTableReady(context.GetTableNameForType(typeof(TableT)));
        }
        public static Task WaitUntilTableReadyAsync<TableT>(this DataContext context)
        {
            return context.WaitUntilTableReadyAsync(context.GetTableNameForType(typeof(TableT)));
        }

        public static void WaitUntilMaxHashIdsTableReady(this DataContext context)
        {
            context.WaitUntilTableReady(context.MaxHashIdTableNameWithPrefix);
        }
        public static Task WaitUntilMaxHashIdsTableReadyAsync(this DataContext context)
        {
            return context.WaitUntilTableReadyAsync(context.MaxHashIdTableNameWithPrefix);
        }

        public static void WaitUntilTableReady(this DataContext context, string tableName)
        {
            string status = null;
            // Let us wait until table is created. Call DescribeTable.
            do
            {
                System.Threading.Thread.Sleep(5000); // Wait 5 seconds.
                try
                {
                    var res = context.Client.DescribeTable(new DescribeTableRequest { TableName = tableName });
                    status = res.Table.TableStatus;
                }
                catch (ResourceNotFoundException){} // DescribeTable is eventually consistent. So you might get resource not found. So we handle the potential exception.
            }
            while (status != "ACTIVE");
        }
        public static async Task WaitUntilTableReadyAsync(this DataContext context, string tableName)
        {
            string status = null;
            // Let us wait until table is created. Call DescribeTable.
            do
            {
                await Task.Delay(5000); // Wait 5 seconds.
                try
                {
                    var res = context.Client.DescribeTable(new DescribeTableRequest { TableName = tableName });
                    status = res.Table.TableStatus;
                }
                catch (ResourceNotFoundException){} // DescribeTable is eventually consistent. So you might get resource not found. So we handle the potential exception.
            }
            while (status != "ACTIVE");
        }
        #endregion

        #endregion
    }
}
