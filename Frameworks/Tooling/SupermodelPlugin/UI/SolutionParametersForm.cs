﻿using System;
using System.Windows.Forms;
using Microsoft.Win32;
using SupermodelPlugin.SupermodelCreator;

namespace SupermodelPlugin.UI
{
    public partial class SolutionParametersForm : Form, ICreatorParams
    {
        #region Constructors
        public SolutionParametersForm()
        {
            InitializeComponent();
            MinimizeBox = MaximizeBox = false;
            webTBSCheckBox.Checked = makeWebTBSVisibleOutsideCheckBox.Checked = true;
            iOS.Checked = android.Checked = uwp.Checked = true;

            webApiProjectComboBox.Items.Add("Web.TBS");
            webApiProjectComboBox.SelectedIndex = 0;

            mobileApiComboBox.Items.Add("Xamarin Forms APIs");
            mobileApiComboBox.Items.Add("Platforms' Native APIs");
            mobileApiComboBox.SelectedIndex = 0;

            location.Text = (Registry.GetValue(RegistryKeyName, RegistryValueName, "") ?? "").ToString();

            UpdateDerivedFields();
        }
        #endregion

        #region Event Handlers
        private void webTBSCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDerivedFields();
            makeWebTBSVisibleOutsideCheckBox.Checked = webTBSCheckBox.Checked;
        }
        private void webJQMCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDerivedFields();
            makeWebJQMVisibleOutsideCheckBox.Checked = webJQMCheckBox.Checked;
        }
        private void webCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDerivedFields();
            makeWebVisibleOutsideCheckBox.Checked = webCheckBox.Checked;
        }
        private void cmdCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDerivedFields();
        }

        private void uwp_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDerivedFields();
        }
        private void iOS_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDerivedFields();
        }
        private void android_CheckedChanged(object sender, EventArgs e)
        {
            UpdateDerivedFields();
        }

        private void name_TextChanged(object sender, EventArgs e)
        {
            UpdateDerivedFields();
        }
        private void location_TextChanged(object sender, EventArgs e)
        {
            UpdateDerivedFields();
        }
        private void browseBtn_Click(object sender, EventArgs e)
        {
            using (var dialog = new FolderBrowserDialog())
            {
                var result = dialog.ShowDialog();
                if (result == DialogResult.OK) location.Text = dialog.SelectedPath;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
        private void okBtn_Click(object sender, EventArgs e)
        {
            Registry.SetValue(RegistryKeyName, RegistryValueName, location.Text);
            DialogResult = DialogResult.OK;
        }
        #endregion

        #region Methods
        private void UpdateDerivedFields()
        {
            if (!iOS.Checked && !android.Checked && !uwp.Checked) mobileApiComboBox.Enabled = false;
            else mobileApiComboBox.Enabled = true;

            makeWebVisibleOutsideCheckBox.Enabled = webCheckBox.Checked;
            makeWebTBSVisibleOutsideCheckBox.Enabled = webTBSCheckBox.Checked;
            makeWebJQMVisibleOutsideCheckBox.Enabled = webJQMCheckBox.Checked;

            var index = 0;
            var foundOldSelection = false;
            var selectedText = (string)webApiProjectComboBox.SelectedItem;
            webApiProjectComboBox.Items.Clear();
            if (webTBSCheckBox.Checked)
            {
                const string optionText = "Web.TBS";
                webApiProjectComboBox.Items.Add(optionText);
                if (selectedText == optionText)
                {
                    webApiProjectComboBox.SelectedIndex = index;
                    foundOldSelection = true;
                }
                index++;
            }
            if (webJQMCheckBox.Checked)
            {
                const string optionText = "Web.JQM";
                webApiProjectComboBox.Items.Add(optionText);
                if (selectedText == optionText)
                {
                    webApiProjectComboBox.SelectedIndex = index;
                    foundOldSelection = true;
                }
                index++;
            }
            if (webCheckBox.Checked)
            {
                const string optionText = "Web";
                webApiProjectComboBox.Items.Add(optionText);
                if (selectedText == optionText)
                {
                    webApiProjectComboBox.SelectedIndex = index;
                    foundOldSelection = true;
                }
                index++;
            }

            if (index > 0)
            {
                if (!foundOldSelection) webApiProjectComboBox.SelectedIndex = 0;
                if (iOS.Checked || android.Checked|| uwp.Checked) webApiProjectComboBox.Enabled = true;
                else webApiProjectComboBox.Enabled = false;
                iOS.Enabled = android.Enabled = uwp.Enabled = true;
            }
            else
            {
                webApiProjectComboBox.Enabled = false;
                iOS.Checked = android.Checked = uwp.Checked = false;
                iOS.Enabled = android.Enabled = uwp.Enabled = false;
            }

            //mobileApiComboBox.Items.Clear();
            //mobileApiComboBox.Items.Add("Xamarin Forms APIs");
            //mobileApiComboBox.Items.Add("Platforms' Native APIs");
            //mobileApiComboBox.SelectedIndex = 0;

            okBtn.Enabled = IsValidAndComplete;
        }
        #endregion

        #region ICreatorParams
        public bool IsValidAndComplete
        {
            get
            {
                if (string.IsNullOrEmpty(SolutionName)) return false;
                if (!Creator.IsValidSolutionName(SolutionName)) return false;
                if (string.IsNullOrEmpty(SolutionLocation)) return false;
                if (!Web && !WebTBS && !WebJQM && !Cmd) return false;
                return true;
            }
        }
        
        //Server
        public bool WebTBS => webTBSCheckBox.Checked;
        public bool MakeWebTBSVisibleOutside => makeWebTBSVisibleOutsideCheckBox.Checked;

        public bool WebJQM => webJQMCheckBox.Checked;
        public bool MakeWebJQMVisibleOutside => makeWebJQMVisibleOutsideCheckBox.Checked;

        public bool Web => webCheckBox.Checked;
        public bool MakeWebVisibleOutside => makeWebVisibleOutsideCheckBox.Checked;

        public bool Cmd => cmdCheckBox.Checked;

        public Creator.DataSource DataSource => Creator.DataSource.SqlServer;

        //Mobile
        // ReSharper disable once InconsistentNaming
        public bool IOS => iOS.Checked;
        public bool Droid => android.Checked;
        public bool UWP => uwp.Checked;
        public bool AnyMobile => iOS.Checked || android.Checked || uwp.Checked;

        public MobileApiEnum? MobileApi
        {
            get
            {
                if (!AnyMobile) return null;
                return mobileApiComboBox.SelectedIndex == 0 ? MobileApiEnum.XamarinForms : MobileApiEnum.Native;
            }
        }
        public WebApiProjectEnum? WebApiProject
        {
            get
            {
                if (!AnyMobile) return null;

                switch ((string)webApiProjectComboBox.SelectedItem)
                {
                    case "Web.TBS": return WebApiProjectEnum.WebTBS;
                    case "Web.JQM": return WebApiProjectEnum.WebJQM;
                    case "Web": return WebApiProjectEnum.Web;
                    default: return WebApiProjectEnum.WebTBS;
                } 
            }
        }

        public string SolutionName => name.Text;
        public string SolutionLocation => location.Text;
        #endregion

        #region Constants
        const string RegistryKeyName = @"HKEY_CURRENT_USER\SOFTWARE\SupermodelFramework";
        const string RegistryValueName = "Location";
        #endregion
    }
}
