﻿namespace SupermodelPlugin.UI
{
    partial class SolutionParametersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.iOS = new System.Windows.Forms.CheckBox();
            this.android = new System.Windows.Forms.CheckBox();
            this.uwp = new System.Windows.Forms.CheckBox();
            this.mobileApiComboBox = new System.Windows.Forms.ComboBox();
            this.mobileApiToUseLabel = new System.Windows.Forms.Label();
            this.NameLabel = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.locationLabel = new System.Windows.Forms.Label();
            this.location = new System.Windows.Forms.TextBox();
            this.browseBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.makeWebVisibleOutsideCheckBox = new System.Windows.Forms.CheckBox();
            this.webCheckBox = new System.Windows.Forms.CheckBox();
            this.webTBSCheckBox = new System.Windows.Forms.CheckBox();
            this.makeWebTBSVisibleOutsideCheckBox = new System.Windows.Forms.CheckBox();
            this.webJQMCheckBox = new System.Windows.Forms.CheckBox();
            this.makeWebJQMVisibleOutsideCheckBox = new System.Windows.Forms.CheckBox();
            this.cmdCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.webApiProjectComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.SuspendLayout();
            // 
            // iOS
            // 
            this.iOS.AutoSize = true;
            this.iOS.Location = new System.Drawing.Point(38, 422);
            this.iOS.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.iOS.Name = "iOS";
            this.iOS.Size = new System.Drawing.Size(61, 24);
            this.iOS.TabIndex = 1;
            this.iOS.Text = "iOS";
            this.iOS.UseVisualStyleBackColor = true;
            this.iOS.CheckedChanged += new System.EventHandler(this.iOS_CheckedChanged);
            // 
            // android
            // 
            this.android.AutoSize = true;
            this.android.Location = new System.Drawing.Point(38, 457);
            this.android.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.android.Name = "android";
            this.android.Size = new System.Drawing.Size(90, 24);
            this.android.TabIndex = 2;
            this.android.Text = "Android";
            this.android.UseVisualStyleBackColor = true;
            this.android.CheckedChanged += new System.EventHandler(this.android_CheckedChanged);
            // 
            // uwp
            // 
            this.uwp.AutoSize = true;
            this.uwp.Location = new System.Drawing.Point(38, 494);
            this.uwp.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.uwp.Name = "uwp";
            this.uwp.Size = new System.Drawing.Size(282, 24);
            this.uwp.TabIndex = 3;
            this.uwp.Text = "UWP (Universal Windows Platform)";
            this.uwp.UseVisualStyleBackColor = true;
            this.uwp.CheckedChanged += new System.EventHandler(this.uwp_CheckedChanged);
            // 
            // mobileApiComboBox
            // 
            this.mobileApiComboBox.FormattingEnabled = true;
            this.mobileApiComboBox.Location = new System.Drawing.Point(39, 563);
            this.mobileApiComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.mobileApiComboBox.Name = "mobileApiComboBox";
            this.mobileApiComboBox.Size = new System.Drawing.Size(817, 28);
            this.mobileApiComboBox.TabIndex = 4;
            // 
            // mobileApiToUseLabel
            // 
            this.mobileApiToUseLabel.AutoSize = true;
            this.mobileApiToUseLabel.Location = new System.Drawing.Point(34, 538);
            this.mobileApiToUseLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.mobileApiToUseLabel.Name = "mobileApiToUseLabel";
            this.mobileApiToUseLabel.Size = new System.Drawing.Size(93, 20);
            this.mobileApiToUseLabel.TabIndex = 5;
            this.mobileApiToUseLabel.Text = "Mobile APIs";
            // 
            // NameLabel
            // 
            this.NameLabel.AutoSize = true;
            this.NameLabel.Location = new System.Drawing.Point(34, 765);
            this.NameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.NameLabel.Name = "NameLabel";
            this.NameLabel.Size = new System.Drawing.Size(55, 20);
            this.NameLabel.TabIndex = 7;
            this.NameLabel.Text = "Name:";
            // 
            // name
            // 
            this.name.AllowDrop = true;
            this.name.Location = new System.Drawing.Point(39, 789);
            this.name.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(817, 26);
            this.name.TabIndex = 8;
            this.name.TextChanged += new System.EventHandler(this.name_TextChanged);
            // 
            // locationLabel
            // 
            this.locationLabel.AutoSize = true;
            this.locationLabel.Location = new System.Drawing.Point(34, 837);
            this.locationLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.locationLabel.Name = "locationLabel";
            this.locationLabel.Size = new System.Drawing.Size(74, 20);
            this.locationLabel.TabIndex = 9;
            this.locationLabel.Text = "Location:";
            // 
            // location
            // 
            this.location.Location = new System.Drawing.Point(39, 862);
            this.location.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.location.Name = "location";
            this.location.Size = new System.Drawing.Size(817, 26);
            this.location.TabIndex = 10;
            this.location.TextChanged += new System.EventHandler(this.location_TextChanged);
            // 
            // browseBtn
            // 
            this.browseBtn.Location = new System.Drawing.Point(867, 858);
            this.browseBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.browseBtn.Name = "browseBtn";
            this.browseBtn.Size = new System.Drawing.Size(88, 35);
            this.browseBtn.TabIndex = 11;
            this.browseBtn.Text = "Browse";
            this.browseBtn.UseVisualStyleBackColor = true;
            this.browseBtn.Click += new System.EventHandler(this.browseBtn_Click);
            // 
            // okBtn
            // 
            this.okBtn.Location = new System.Drawing.Point(14, 942);
            this.okBtn.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(112, 35);
            this.okBtn.TabIndex = 12;
            this.okBtn.Text = "Ok";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.okBtn_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(136, 942);
            this.cancelButton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(112, 35);
            this.cancelButton.TabIndex = 13;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // makeWebVisibleOutsideCheckBox
            // 
            this.makeWebVisibleOutsideCheckBox.AutoSize = true;
            this.makeWebVisibleOutsideCheckBox.Location = new System.Drawing.Point(68, 257);
            this.makeWebVisibleOutsideCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.makeWebVisibleOutsideCheckBox.Name = "makeWebVisibleOutsideCheckBox";
            this.makeWebVisibleOutsideCheckBox.Size = new System.Drawing.Size(687, 24);
            this.makeWebVisibleOutsideCheckBox.TabIndex = 15;
            this.makeWebVisibleOutsideCheckBox.Text = "Make visible in IIS Express outside of the localhost (recommended for debugging i" +
    "n Parallels)";
            this.makeWebVisibleOutsideCheckBox.UseVisualStyleBackColor = true;
            // 
            // webCheckBox
            // 
            this.webCheckBox.AutoSize = true;
            this.webCheckBox.Location = new System.Drawing.Point(39, 228);
            this.webCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.webCheckBox.Name = "webCheckBox";
            this.webCheckBox.Size = new System.Drawing.Size(322, 24);
            this.webCheckBox.TabIndex = 17;
            this.webCheckBox.Text = "Web (no JavaScript library dependacies).";
            this.webCheckBox.UseVisualStyleBackColor = true;
            this.webCheckBox.CheckedChanged += new System.EventHandler(this.webCheckBox_CheckedChanged);
            // 
            // webTBSCheckBox
            // 
            this.webTBSCheckBox.AutoSize = true;
            this.webTBSCheckBox.Location = new System.Drawing.Point(39, 62);
            this.webTBSCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.webTBSCheckBox.Name = "webTBSCheckBox";
            this.webTBSCheckBox.Size = new System.Drawing.Size(321, 24);
            this.webTBSCheckBox.TabIndex = 19;
            this.webTBSCheckBox.Text = "Web.TBS (Twitter Bootstrap front-end).";
            this.webTBSCheckBox.UseVisualStyleBackColor = true;
            this.webTBSCheckBox.CheckedChanged += new System.EventHandler(this.webTBSCheckBox_CheckedChanged);
            // 
            // makeWebTBSVisibleOutsideCheckBox
            // 
            this.makeWebTBSVisibleOutsideCheckBox.AutoSize = true;
            this.makeWebTBSVisibleOutsideCheckBox.Location = new System.Drawing.Point(68, 92);
            this.makeWebTBSVisibleOutsideCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.makeWebTBSVisibleOutsideCheckBox.Name = "makeWebTBSVisibleOutsideCheckBox";
            this.makeWebTBSVisibleOutsideCheckBox.Size = new System.Drawing.Size(687, 24);
            this.makeWebTBSVisibleOutsideCheckBox.TabIndex = 18;
            this.makeWebTBSVisibleOutsideCheckBox.Text = "Make visible in IIS Express outside of the localhost (recommended for debugging i" +
    "n Parallels)";
            this.makeWebTBSVisibleOutsideCheckBox.UseVisualStyleBackColor = true;
            // 
            // webJQMCheckBox
            // 
            this.webJQMCheckBox.AutoSize = true;
            this.webJQMCheckBox.Location = new System.Drawing.Point(38, 146);
            this.webJQMCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.webJQMCheckBox.Name = "webJQMCheckBox";
            this.webJQMCheckBox.Size = new System.Drawing.Size(292, 24);
            this.webJQMCheckBox.TabIndex = 21;
            this.webJQMCheckBox.Text = "Web.JQM (JQuery Mobile front-end).";
            this.webJQMCheckBox.UseVisualStyleBackColor = true;
            this.webJQMCheckBox.CheckedChanged += new System.EventHandler(this.webJQMCheckBox_CheckedChanged);
            // 
            // makeWebJQMVisibleOutsideCheckBox
            // 
            this.makeWebJQMVisibleOutsideCheckBox.AutoSize = true;
            this.makeWebJQMVisibleOutsideCheckBox.Location = new System.Drawing.Point(66, 177);
            this.makeWebJQMVisibleOutsideCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.makeWebJQMVisibleOutsideCheckBox.Name = "makeWebJQMVisibleOutsideCheckBox";
            this.makeWebJQMVisibleOutsideCheckBox.Size = new System.Drawing.Size(687, 24);
            this.makeWebJQMVisibleOutsideCheckBox.TabIndex = 20;
            this.makeWebJQMVisibleOutsideCheckBox.Text = "Make visible in IIS Express outside of the localhost (recommended for debugging i" +
    "n Parallels)";
            this.makeWebJQMVisibleOutsideCheckBox.UseVisualStyleBackColor = true;
            // 
            // cmdCheckBox
            // 
            this.cmdCheckBox.AutoSize = true;
            this.cmdCheckBox.Location = new System.Drawing.Point(39, 309);
            this.cmdCheckBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cmdCheckBox.Name = "cmdCheckBox";
            this.cmdCheckBox.Size = new System.Drawing.Size(249, 24);
            this.cmdCheckBox.TabIndex = 22;
            this.cmdCheckBox.Text = "Command Line Batch Process";
            this.cmdCheckBox.UseVisualStyleBackColor = true;
            this.cmdCheckBox.CheckedChanged += new System.EventHandler(this.cmdCheckBox_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(33, 615);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 20);
            this.label1.TabIndex = 24;
            this.label1.Text = "Web API Project";
            // 
            // webApiProjectComboBox
            // 
            this.webApiProjectComboBox.FormattingEnabled = true;
            this.webApiProjectComboBox.Location = new System.Drawing.Point(38, 640);
            this.webApiProjectComboBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.webApiProjectComboBox.Name = "webApiProjectComboBox";
            this.webApiProjectComboBox.Size = new System.Drawing.Size(818, 28);
            this.webApiProjectComboBox.TabIndex = 23;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(18, 18);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(962, 337);
            this.groupBox1.TabIndex = 25;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Server";
            // 
            // groupBox2
            // 
            this.groupBox2.Location = new System.Drawing.Point(18, 386);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(962, 314);
            this.groupBox2.TabIndex = 26;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mobile";
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(18, 729);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox3.Size = new System.Drawing.Size(962, 188);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Solution";
            // 
            // SolutionParametersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 1006);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.webApiProjectComboBox);
            this.Controls.Add(this.cmdCheckBox);
            this.Controls.Add(this.webJQMCheckBox);
            this.Controls.Add(this.makeWebJQMVisibleOutsideCheckBox);
            this.Controls.Add(this.webTBSCheckBox);
            this.Controls.Add(this.makeWebTBSVisibleOutsideCheckBox);
            this.Controls.Add(this.webCheckBox);
            this.Controls.Add(this.makeWebVisibleOutsideCheckBox);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okBtn);
            this.Controls.Add(this.browseBtn);
            this.Controls.Add(this.location);
            this.Controls.Add(this.locationLabel);
            this.Controls.Add(this.name);
            this.Controls.Add(this.NameLabel);
            this.Controls.Add(this.mobileApiToUseLabel);
            this.Controls.Add(this.mobileApiComboBox);
            this.Controls.Add(this.uwp);
            this.Controls.Add(this.android);
            this.Controls.Add(this.iOS);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "SolutionParametersForm";
            this.Text = "Solution Parameters";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckBox iOS;
        private System.Windows.Forms.CheckBox android;
        private System.Windows.Forms.CheckBox uwp;
        private System.Windows.Forms.ComboBox mobileApiComboBox;
        private System.Windows.Forms.Label mobileApiToUseLabel;
        private System.Windows.Forms.Label NameLabel;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label locationLabel;
        private System.Windows.Forms.TextBox location;
        private System.Windows.Forms.Button browseBtn;
        private System.Windows.Forms.Button okBtn;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.CheckBox makeWebVisibleOutsideCheckBox;
        private System.Windows.Forms.CheckBox webCheckBox;
        private System.Windows.Forms.CheckBox webTBSCheckBox;
        private System.Windows.Forms.CheckBox makeWebTBSVisibleOutsideCheckBox;
        private System.Windows.Forms.CheckBox webJQMCheckBox;
        private System.Windows.Forms.CheckBox makeWebJQMVisibleOutsideCheckBox;
        private System.Windows.Forms.CheckBox cmdCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox webApiProjectComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}