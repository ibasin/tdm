﻿//------------------------------------------------------------------------------
// <copyright file="CreateSolution.cs" company="Company">
//     Copyright (c) Company.  All rights reserved.
// </copyright>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel.Design;
using System.IO;
using System.Windows.Forms;
using EnvDTE;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using SupermodelPlugin.SupermodelCreator;
using SupermodelPlugin.UI;

namespace SupermodelPlugin
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class CreateSolution
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 0x0100;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("83711c95-8480-4fb0-bdc2-672165734002");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly Package _package;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateSolution"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        private CreateSolution(Package package)
        {
            _package = package ?? throw new ArgumentNullException(nameof(package));
            var commandService = ServiceProvider.GetService(typeof(IMenuCommandService)) as OleMenuCommandService;
            if (commandService != null)
            {
                var menuCommandID = new CommandID(CommandSet, CommandId);
                var menuItem = new MenuCommand(MenuItemCallback, menuCommandID);
                commandService.AddCommand(menuItem);
            }
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static CreateSolution Instance
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private IServiceProvider ServiceProvider => _package;

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static void Initialize(Package package)
        {
            Instance = new CreateSolution(package);
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        private void MenuItemCallback(object sender, EventArgs e)
        {
            //Creator.CreateSnapshot(@"..\..\..\..\..\..\XXYXX\XXYXX", @"..\..\");
            //VsShellUtilities.ShowMessageBox(ServiceProvider, "New Template Successfully Created", "", OLEMSGICON.OLEMSGICON_INFO, OLEMSGBUTTON.OLEMSGBUTTON_OK, OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
            //return;

            var form = new SolutionParametersForm { StartPosition = FormStartPosition.CenterScreen };
            try
            {
                var result = form.ShowDialog();
                if (result == DialogResult.OK)
                {
                    //Create path
                    var path = form.SolutionName;
                    if (form.SolutionLocation != null) path = Path.Combine(form.SolutionLocation, form.SolutionName);

                    //Create dir and extract files into it
                    var goodToGo = true;
                    if (Directory.Exists(path))
                    {
                        // ReSharper disable LocalizableElement
                        var dialogResult = MessageBox.Show($"Directory '{path}' already exists. \n\nDo you want to delete it?", "Directory already exists", MessageBoxButtons.YesNo);
                        // ReSharper restore LocalizableElement
                        if (dialogResult == DialogResult.Yes)
                        {
                            try
                            {
                                Directory.Delete(path, true);
                            }
                            catch (Exception)
                            {
                                VsShellUtilities.ShowMessageBox(ServiceProvider, $"Unable to delete directory '{path}'.", "", OLEMSGICON.OLEMSGICON_CRITICAL, OLEMSGBUTTON.OLEMSGBUTTON_OK, OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
                                goodToGo = false;
                            }
                        }
                        else
                        {
                            VsShellUtilities.ShowMessageBox(ServiceProvider, $"Unable to create the new Solution.\n\nDirectory '{path}' already exists.", "", OLEMSGICON.OLEMSGICON_CRITICAL, OLEMSGBUTTON.OLEMSGBUTTON_OK, OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
                            goodToGo = false;
                        }
                    }

                    if (goodToGo)
                    {
                        //Create solution
                        Creator.CreateSupermodelShell(form);

                        //VsShellUtilities.ShowMessageBox(ServiceProvider, "Solution Successfully Created", "", OLEMSGICON.OLEMSGICON_INFO, OLEMSGBUTTON.OLEMSGBUTTON_OK, OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
                        //return;

                        var applicationObject = (DTE)ServiceProvider.GetService(typeof(SDTE));
                        // ReSharper disable once AssignNullToNotNullAttribute
                        var solutionFilePath = Path.Combine(form.SolutionLocation, form.SolutionName, form.SolutionName + ".sln");
                        applicationObject.Solution.Open(solutionFilePath);

                        try
                        {
                            var readmePath = Path.Combine(form.SolutionLocation, form.SolutionName, "Documentation", "readme.txt");
                            applicationObject.ItemOperations.OpenFile(readmePath);
                        }
                        // ReSharper disable once EmptyGeneralCatchClause
                        catch (Exception) { }
                    }
                }
            }
            catch (CreatorException ex1)
            {
                VsShellUtilities.ShowMessageBox(ServiceProvider, ex1.Message, "", OLEMSGICON.OLEMSGICON_CRITICAL, OLEMSGBUTTON.OLEMSGBUTTON_OK, OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
                // ReSharper disable once EmptyGeneralCatchClause, AssignNullToNotNullAttribute
                try { Directory.Delete(Path.Combine(form.SolutionLocation, form.SolutionName), true); } catch(Exception) { }
            }
            catch (Exception ex)
            {
                VsShellUtilities.ShowMessageBox(ServiceProvider, $"Unexpected error creating Supermodel Solution: \n\n{ex.Message}", "", OLEMSGICON.OLEMSGICON_CRITICAL, OLEMSGBUTTON.OLEMSGBUTTON_OK, OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
                // ReSharper disable once EmptyGeneralCatchClause, AssignNullToNotNullAttribute
                try { Directory.Delete(Path.Combine(form.SolutionLocation, form.SolutionName), true); } catch (Exception) { }
            }
        }
    }
}
