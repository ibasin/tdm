﻿    public class XXYXXUserUpdatePasswordApiModel : ApiModelForEntity<XXYXXUser>
    {
        #region Overrides
        public override object MapToObjectCustom(object obj, Type objType)
        {
            var user = (XXYXXUser)obj;
            user.Password = NewPassword;
            return base.MapToObjectCustom(obj, objType);
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //we don't want to the domain-level validation to be called here (which called by the base method) because we don't fill that object completely
            // ReSharper disable once CollectionNeverUpdated.Local
            var vr = new ValidationResultList();
            return vr;
        }
        #endregion
        
        #region Properties
        [Required, NotRMapped] public string OldPassword { get; set; }

        [Required, NotRMapped, MustEqualTo("ConfirmPassword", ErrorMessage = "Passwords do not Match")]
        public string NewPassword { get; set; }

        [Required, NotRMapped, MustEqualTo("NewPassword", ErrorMessage = "Passwords do not Match")]
        public string ConfirmPassword { get; set; }
        #endregion
    }

