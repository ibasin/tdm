﻿using System;

namespace SupermodelPlugin.SupermodelCreator
{
    public class CreatorException : Exception
    {
        public CreatorException(string msg) : base(msg) { }
    }
}
