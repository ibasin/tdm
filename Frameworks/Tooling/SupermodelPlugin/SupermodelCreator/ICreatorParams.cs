﻿namespace SupermodelPlugin.SupermodelCreator
{
    public interface ICreatorParams
    {
        bool IsValidAndComplete { get; }
        
        //Server
        bool WebTBS { get; }
        bool MakeWebTBSVisibleOutside { get; }

        bool WebJQM { get; }
        bool MakeWebJQMVisibleOutside { get; }

        bool Web { get; }
        bool MakeWebVisibleOutside { get; }

        bool Cmd { get; }

        Creator.DataSource DataSource { get; }

        //Mobile
        // ReSharper disable once InconsistentNaming
        bool IOS { get; }
        bool Droid { get; }
        bool UWP { get; }
        bool AnyMobile { get; }
        MobileApiEnum? MobileApi { get; }
        WebApiProjectEnum? WebApiProject { get; }

        //Solution
        string SolutionName { get; }
        string SolutionLocation { get; }
    }
}
