﻿using System;
using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Reflection;
using System.Text;

namespace SupermodelPlugin.SupermodelCreator
{
    public static class Creator
    {
        #region Embedded Types
        public enum DataSource
        {
            SqlServer,
            AWSDynamoDb,
            AzureTableStorage
        }
        #endregion

        #region Methods
        public static void CreateSupermodelShell(ICreatorParams creatorParams)
        {
            //Check if the name is valid
            if (!IsValidSolutionName(creatorParams.SolutionName)) throw new Exception($"{creatorParams.SolutionName} is not a valid name for a Supermodel solution");

            //Create path
            var path = creatorParams.SolutionName;
            if (creatorParams.SolutionLocation != null) path = Path.Combine(creatorParams.SolutionLocation, creatorParams.SolutionName);
            
            //Create dir and extract files into it
            if (Directory.Exists(path)) throw new CreatorException($"Unable to create the new Solution.\n\nDirectory '{path}' already exists.");
            Directory.CreateDirectory(path);
            // ReSharper disable once AssignNullToNotNullAttribute
            ZipFile.ExtractToDirectory(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), ZipFileName), path);

            //Adjust for Xamarin.Forms UI vs Native UI
            AdjustForXamarinFormsUIvsNativeUI(creatorParams.MobileApi, path);

            //Auto-assign random port
            int newPortWebTBS, newPortWebJQM, newPortWeb;
            while(true)
            {
                newPortWebTBS = Random.Next(41000, 59000);
                newPortWebJQM = Random.Next(41000, 59000);
                newPortWeb = Random.Next(41000, 59000);
                if (newPortWebTBS != newPortWebJQM && newPortWebTBS != newPortWeb && newPortWebJQM != newPortWeb) break;
            }

            //Assign new port in proj file
            AutoAssignRandomPortForAWebProj(path + @"\XXYXX\Server\Web.TBS\Web.TBS.csproj", OldPortWebTBS, newPortWebTBS);
            AutoAssignRandomPortForAWebProj(path + @"\XXYXX\Server\Web.JQM\Web.JQM.csproj", OldPortWebJQM, newPortWebJQM);
            AutoAssignRandomPortForAWebProj(path + @"\XXYXX\Server\Web\Web.csproj", OldPortWeb, newPortWeb);

            //Adjust ModelGenerator to read ApiControllers from the right project
            AdjustModelGeneratorToPointToRightProject(creatorParams.WebApiProject, path, newPortWebTBS, newPortWebJQM, newPortWeb);

            //Register sites with IISExpress
            RegisterSitesWithIISExpress(creatorParams, newPortWebTBS, newPortWebJQM, newPortWeb, path);

            //Adjust WebApi bundled
            AdjustWebApiStuffBundled(creatorParams.WebApiProject, creatorParams.MobileApi, creatorParams.AnyMobile, path);

            //RemoveProjectsNotNeeded
            RemoveProjectsNotNeeded(path, creatorParams.IOS, creatorParams.Droid, creatorParams.UWP, creatorParams.WebTBS, creatorParams.WebJQM, creatorParams.Web, creatorParams.Cmd);

            //If only one web project, make model be MvcModel
            IfOnlyOneWebProjectMakeModelBeMvcModel(creatorParams.WebTBS, creatorParams.WebJQM, creatorParams.Web, path);

            //Replace GUIDs
            GenerateReplacementGuids();
            ReplaceGuidsInDir(path);

            //Generate new random key for encrypting username/password locally
            if (creatorParams.MobileApi == MobileApiEnum.XamarinForms) GenerateNewRandomEncryptionKey(path);

            //rename files and directories containing Marker, find and replace Marker inside the files
            ReplaceMarkerInDir(path, creatorParams.SolutionName);
        }
        public static void CreateSnapshot(string projectTemplateDirectory, string destinationDir = null)
        {
            DeleteWhatWeDontNeedForSnapshot(projectTemplateDirectory);

            var zipFileNamePath = ZipFileName;
            if (destinationDir != null) zipFileNamePath = Path.Combine(destinationDir, ZipFileName);

            if (File.Exists(zipFileNamePath)) File.Delete(zipFileNamePath);
            ZipFile.CreateFromDirectory(projectTemplateDirectory, zipFileNamePath);
        }
        public static bool IsValidSolutionName(string name)
        {
            var provider = CodeDomProvider.CreateProvider("C#");
            return provider.IsValidIdentifier(name);
            //var match = Regex.Match(name, "^[a-zA-Z_$][a-zA-Z_$0-9]*$");
            //return match.Success;
        }

        private static void RegisterSitesWithIISExpress(ICreatorParams creatorParams, int newPortWebTBS, int newPortWebJQM, int newPortWeb, string path)
        {
            const string netshArgumentsLocalHost = @" http add urlacl url = http://localhost:[PORT]/ user=Everyone";
            const string netshArgumentsIP = @" http add urlacl url = http://[IPADDRESS]:[PORT]/ user=Everyone";
            var netshExeFullPath = Path.Combine(Environment.SystemDirectory, "netsh.exe");

            var sb = new StringBuilder();
            if (creatorParams.WebTBS)
            {
                //RunNetshScript(newPortWebTBS, @"http add urlacl url = http://localhost:[PORT]/ user=Everyone");
                sb.AppendLine(netshExeFullPath + netshArgumentsLocalHost.ReplaceStrWithCheck("[PORT]", newPortWebTBS.ToString()));
                if (creatorParams.MakeWebTBSVisibleOutside)
                {
                    //MakeIISExpressWebsiteVisibleFromOutside(path, OldPortWebTBS, newPortWebTBS, @"http add urlacl url = http://[IPADDRESS]:[PORT]/ user=Everyone");
                    AdjustWebApiIpAddressAndPortInApplicationhostCondfig(path, OldPortWebTBS, newPortWebTBS);
                    sb.AppendLine(netshExeFullPath + netshArgumentsIP.ReplaceStrWithCheck("[IPADDRESS]", GetServerIpAddress()).ReplaceStrWithCheck("[PORT]", newPortWebTBS.ToString()));
                }
            }
            if (creatorParams.WebJQM)
            {
                //RunNetshScript(newPortWebJQM, @"http add urlacl url = http://localhost:[PORT]/ user=Everyone");
                sb.AppendLine(netshExeFullPath + netshArgumentsLocalHost.ReplaceStrWithCheck("[PORT]", newPortWebJQM.ToString()));
                if (creatorParams.MakeWebJQMVisibleOutside)
                {
                    //MakeIISExpressWebsiteVisibleFromOutside(path, OldPortWebJQM, newPortWebJQM, @"http add urlacl url = http://[IPADDRESS]:[PORT]/ user=Everyone");
                    AdjustWebApiIpAddressAndPortInApplicationhostCondfig(path, OldPortWebJQM, newPortWebJQM);
                    sb.AppendLine(netshExeFullPath + netshArgumentsIP.ReplaceStrWithCheck("[IPADDRESS]", GetServerIpAddress()).ReplaceStrWithCheck("[PORT]", newPortWebJQM.ToString()));
                }
            }
            if (creatorParams.Web)
            {
                //RunNetshScript(newPortWeb, @"http add urlacl url = http://localhost:[PORT]/ user=Everyone");
                sb.AppendLine(netshExeFullPath + netshArgumentsLocalHost.ReplaceStrWithCheck("[PORT]", newPortWeb.ToString()));
                if (creatorParams.MakeWebVisibleOutside)
                {
                    //MakeIISExpressWebsiteVisibleFromOutside(path, OldPortWeb, newPortWeb, @"http add urlacl url = http://[IPADDRESS]:[PORT]/ user=Everyone");
                    AdjustWebApiIpAddressAndPortInApplicationhostCondfig(path, OldPortWeb, newPortWeb);
                    sb.AppendLine(netshExeFullPath + netshArgumentsIP.ReplaceStrWithCheck("[IPADDRESS]", GetServerIpAddress()).ReplaceStrWithCheck("[PORT]", newPortWeb.ToString()));

                }
            }
            var batFile = path + @"\Documentation\RegisterSitesWithIISExpress.bat";
            var batFileContent = sb.ToString();

            //If we put somehting in the file save and execute it with admin priveleges
            if (!string.IsNullOrEmpty(batFileContent))
            {
                File.WriteAllText(batFile, batFileContent);

                var info = new ProcessStartInfo("cmd.exe")
                {
                    UseShellExecute = true,
                    Arguments = $"/c \"{batFile}\"",
                    WindowStyle = ProcessWindowStyle.Hidden,
                    CreateNoWindow = true,
                    Verb = "runas"
                };

                try
                {
                    var process = Process.Start(info);
                    if (process == null) throw new Exception("batch process after starting is null");
                    process.WaitForExit();
                    if (process.ExitCode != 0) throw new Exception($"returned exit code {process.ExitCode}");
                }
                catch (Win32Exception ex1)
                {
                    const int errorCancelled = 1223; //The operation was canceled by the user.
                    if (ex1.NativeErrorCode == errorCancelled) throw new CreatorException("You must allow Administrator access in order to register web projects with netsh.");
                    throw;
                }
                catch (Exception ex2)
                {
                    throw new CreatorException($"Error executing RegisterSitesWithIISExpress.bat: {ex2.Message}");
                }
            }
        }
        private static void AdjustWebApiIpAddressAndPortInApplicationhostCondfig(string path, int oldPort, int newPort)
        {
            var applicationhostFile = path + @"\.vs\config\applicationhost.config";
            var applicationhostFileContents = File.ReadAllText(applicationhostFile);
            applicationhostFileContents = applicationhostFileContents.Replace(OldIPAddress, GetServerIpAddress()).ReplaceStrWithCheck(oldPort.ToString(), newPort.ToString());
            File.WriteAllText(applicationhostFile, applicationhostFileContents);
        }
        private static void AdjustWebApiIpAddressAndPortInWebApiDataContext(string path, int newPort)
        {
            var webApiDataContextFile = path + @"\XXYXX\Mobile\XXYXX.Shared\Supermodel\Persistance\XXYXXWebApiDataContext.cs";
            var webApiDataContextFileContents = File.ReadAllText(webApiDataContextFile);
            webApiDataContextFileContents = webApiDataContextFileContents.ReplaceStrWithCheck(OldIPAddress, GetServerIpAddress()).ReplaceStrWithCheck(OldPortWebTBS.ToString(), newPort.ToString());
            File.WriteAllText(webApiDataContextFile, webApiDataContextFileContents);
        }
        private static void AdjustWebApiStuffBundled(WebApiProjectEnum? webApiProject, MobileApiEnum? mobileApi, bool anyMobile, string path)
        {
            if (!anyMobile)
            {
                RemoveUserUpdatePasswordApiModel(path);
                DisableWebApiForWebTBS(path);
                DisableWebApiForWebJQM(path);
                DisableWebApiForWeb(path);
            }
            else if (mobileApi == MobileApiEnum.XamarinForms)
            {
                if (webApiProject != WebApiProjectEnum.WebTBS) DisableWebApiForWebTBS(path);
                if (webApiProject != WebApiProjectEnum.WebJQM) DisableWebApiForWebJQM(path);
                if (webApiProject != WebApiProjectEnum.Web) DisableWebApiForWeb(path);
            }
            else
            {
                RemoveUserUpdatePasswordApiModel(path);

                if (webApiProject != WebApiProjectEnum.WebTBS) DisableWebApiForWebTBS(path);
                else RemoveUserUpdatePasswordControllerFromWebTBS(path);
                
                if (webApiProject != WebApiProjectEnum.WebJQM) DisableWebApiForWebJQM(path);
                else RemoveUserUpdatePasswordControllerFromWebJQM(path);

                if (webApiProject != WebApiProjectEnum.Web) DisableWebApiForWeb(path);
                else RemoveUserUpdatePasswordControllerFromWeb(path);
            }
        }
        private static void RemoveUserUpdatePasswordApiModel(string path)
        {
            var userFile = path + @"\XXYXX\Server\Domain\Entities\XXYXXUser.cs";
            var userFileContent = File.ReadAllText(userFile);

            var assemblyName = typeof(Creator).Assembly.GetName().Name;
            var userSnippet = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.AllMobile.userSnippet.txt");
            userFileContent = userFileContent.RemoveStrWithCheck("using Supermodel.DDD.Models.View.WebApi;").RemoveStrWithCheck(userSnippet);

            File.WriteAllText(userFile, userFileContent);
        }

        private static void DisableWebApiForWebTBS(string path)
        {
            RemoveWebApiAuthenticationRegistrationForWebTBS(path);
            RemoveUserUpdatePasswordControllerFromWebTBS(path);
        }
        private static void RemoveWebApiAuthenticationRegistrationForWebTBS(string path)
        {
            var globalFile = path + @"\XXYXX\Server\Web.TBS\Global.asax.cs";
            var globalFileContent = File.ReadAllText(globalFile);

            globalFileContent = globalFileContent
                .ReplaceStrWithCheck("SupermodelInitialization.WebInit<XXYXXDbContext>(webApiAuthFilter: new XXYXXAuthenticateAttribute());", "SupermodelInitialization.WebInit<XXYXXDbContext>();")
                .RemoveStrWithCheck("using Domain.Supermodel.Auth;");

            File.WriteAllText(globalFile, globalFileContent);
        }
        private static void RemoveUserUpdatePasswordControllerFromWebTBS(string path)
        {
            var projFile = path + @"\XXYXX\Server\Web.TBS\Web.TBS.csproj";
            var projFileContent = File.ReadAllText(projFile);
            projFileContent = projFileContent.RemoveStrWithCheck("<Compile Include=\"ApiControllers\\XXYXXUserUpdatePasswordController.cs\" />");
            File.WriteAllText(projFile, projFileContent);
            File.Delete(path + @"\XXYXX\Server\Web.TBS\ApiControllers\XXYXXUserUpdatePasswordController.cs");
        }

        private static void DisableWebApiForWebJQM(string path)
        {
            RemoveWebApiAuthenticationRegistrationForWebJQM(path);
            RemoveUserUpdatePasswordControllerFromWebJQM(path);
        }
        private static void RemoveWebApiAuthenticationRegistrationForWebJQM(string path)
        {
            var globalFile = path + @"\XXYXX\Server\Web.JQM\Global.asax.cs";
            var globalFileContent = File.ReadAllText(globalFile);

            globalFileContent = globalFileContent
                .ReplaceStrWithCheck("SupermodelInitialization.WebInit<XXYXXDbContext>(webApiAuthFilter: new XXYXXAuthenticateAttribute());", "SupermodelInitialization.WebInit<XXYXXDbContext>();")
                .RemoveStrWithCheck("using Domain.Supermodel.Auth;");

            File.WriteAllText(globalFile, globalFileContent);
        }
        private static void RemoveUserUpdatePasswordControllerFromWebJQM(string path)
        {
            var projFile = path + @"\XXYXX\Server\Web.JQM\Web.JQM.csproj";
            var projFileContent = File.ReadAllText(projFile);
            projFileContent = projFileContent.RemoveStrWithCheck("<Compile Include=\"ApiControllers\\XXYXXUserUpdatePasswordController.cs\" />");
            File.WriteAllText(projFile, projFileContent);
            File.Delete(path + @"\XXYXX\Server\Web.JQM\ApiControllers\XXYXXUserUpdatePasswordController.cs");
        }

        private static void DisableWebApiForWeb(string path)
        {
            RemoveWebApiAuthenticationRegistrationForWeb(path);
            RemoveUserUpdatePasswordControllerFromWeb(path);
        }
        private static void RemoveWebApiAuthenticationRegistrationForWeb(string path)
        {
            var globalFile = path + @"\XXYXX\Server\Web\Global.asax.cs";
            var globalFileContent = File.ReadAllText(globalFile);

            globalFileContent = globalFileContent
                .ReplaceStrWithCheck("SupermodelInitialization.WebInit<XXYXXDbContext>(webApiAuthFilter: new XXYXXAuthenticateAttribute());", "SupermodelInitialization.WebInit<XXYXXDbContext>();")
                .RemoveStrWithCheck("using Domain.Supermodel.Auth;");

            File.WriteAllText(globalFile, globalFileContent);
        }
        private static void RemoveUserUpdatePasswordControllerFromWeb(string path)
        {
            var projFile = path + @"\XXYXX\Server\Web\Web.csproj";
            var projFileContent = File.ReadAllText(projFile);
            projFileContent = projFileContent.RemoveStrWithCheck("<Compile Include=\"ApiControllers\\XXYXXUserUpdatePasswordController.cs\" />");
            File.WriteAllText(projFile, projFileContent);
            File.Delete(path + @"\XXYXX\Server\Web\ApiControllers\XXYXXUserUpdatePasswordController.cs");
        }

        private static void RemoveProjectsNotNeeded(string path, bool ios, bool droid, bool uwp, bool webTBS, bool webJQM, bool web, bool cmd)
        {
            var solutionFile = path + @"\XXYXX.sln";
            var solutionFileContent = File.ReadAllText(solutionFile);

            var userFile = path + @"\XXYXX\Server\Domain\Entities\XXYXXUser.cs";
            var userFileContent = File.ReadAllText(userFile);

            var assemblyName = typeof(Creator).Assembly.GetName().Name;

            if (!ios)
            {
                var snippet1 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.iOS.snippet1.txt");
                var snippet2 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.iOS.snippet2.txt");
                var snippet3 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.iOS.snippet3.txt");
                var snippet4 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.iOS.snippet4.txt");
                solutionFileContent = solutionFileContent
                    .RemoveStrWithCheck(snippet1)
                    .RemoveStrWithCheck(snippet2)
                    .RemoveStrWithCheck(snippet3)
                    .RemoveStrWithCheck(snippet4);
                Directory.Delete(path + @"\XXYXX\Mobile\XXYXX.iOS", true);
            }

            if (!droid)
            {
                var snippet1 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Droid.snippet1.txt");
                var snippet2 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Droid.snippet2.txt");
                var snippet3 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Droid.snippet3.txt");
                var snippet4 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Droid.snippet4.txt");
                solutionFileContent = solutionFileContent
                    .RemoveStrWithCheck(snippet1)
                    .RemoveStrWithCheck(snippet2)
                    .RemoveStrWithCheck(snippet3)
                    .RemoveStrWithCheck(snippet4);
                Directory.Delete(path + @"\XXYXX\Mobile\XXYXX.Droid", true);
            }

            if (!uwp)
            {
                var snippet1 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.UWP.snippet1.txt");
                var snippet2 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.UWP.snippet2.txt");
                var snippet3 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.UWP.snippet3.txt");
                var snippet4 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.UWP.snippet4.txt");
                solutionFileContent = solutionFileContent
                    .RemoveStrWithCheck(snippet1)
                    .RemoveStrWithCheck(snippet2)
                    .RemoveStrWithCheck(snippet3)
                    .RemoveStrWithCheck(snippet4);
                Directory.Delete(path + @"\XXYXX\Mobile\XXYXX.UWP", true);
            }

            if (!ios && !droid && !uwp)
            {
                var snippet1 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.AllMobile.snippet1.txt");
                var snippet2 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.AllMobile.snippet2.txt");
                var snippet3 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.AllMobile.snippet3.txt");
                var snippet4 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.AllMobile.snippet4.txt");
                var snippet5 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.AllMobile.snippet5.txt");
                var snippet6 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.AllMobile.snippet6.txt");
                solutionFileContent = solutionFileContent
                    .RemoveStrWithCheck(snippet1)
                    .RemoveStrWithCheck(snippet2)
                    .RemoveStrWithCheck(snippet3)
                    .RemoveStrWithCheck(snippet4)
                    .RemoveStrWithCheck(snippet5)
                    .RemoveStrWithCheck(snippet6);
                Directory.Delete(path + @"\XXYXX\Mobile", true);
                Directory.Delete(path + @"\XXYXX\Util", true);
            }

            if (!webTBS)
            {
                var snippet1 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.TBS.snippet1.txt");
                var snippet2 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.TBS.snippet2.txt");
                var snippet3 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.TBS.snippet3.txt");
                solutionFileContent = solutionFileContent
                    .RemoveStrWithCheck(snippet1)
                    .RemoveStrWithCheck(snippet2)
                    .RemoveStrWithCheck(snippet3);
                Directory.Delete(path + @"\XXYXX\Server\Web.TBS", true);

                var userSnippet = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.TBS.userSnippet.txt");
                userFileContent = userFileContent.RemoveStrWithCheck(userSnippet);
            }

            if (!webJQM)
            {
                var snippet1 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.JQM.snippet1.txt");
                var snippet2 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.JQM.snippet2.txt");
                var snippet3 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.JQM.snippet3.txt");
                solutionFileContent = solutionFileContent
                    .RemoveStrWithCheck(snippet1)
                    .RemoveStrWithCheck(snippet2)
                    .RemoveStrWithCheck(snippet3);
                Directory.Delete(path + @"\XXYXX\Server\Web.JQM", true);

                var userSnippet = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.JQM.userSnippet.txt");
                userFileContent = userFileContent.RemoveStrWithCheck(userSnippet);
            }

            if (!web)
            {
                var snippet1 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.snippet1.txt");
                var snippet2 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.snippet2.txt");
                var snippet3 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.snippet3.txt");
                solutionFileContent = solutionFileContent
                    .RemoveStrWithCheck(snippet1)
                    .RemoveStrWithCheck(snippet2)
                    .RemoveStrWithCheck(snippet3);
                Directory.Delete(path + @"\XXYXX\Server\Web", true);

                var userSnippet = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Web.userSnippet.txt");
                userFileContent = userFileContent.RemoveStrWithCheck(userSnippet);
            }

            if (!cmd)
            {
                var snippet1 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Cmd.snippet1.txt");
                var snippet2 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Cmd.snippet2.txt");
                var snippet3 = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Delete.Cmd.snippet3.txt");
                solutionFileContent = solutionFileContent
                    .RemoveStrWithCheck(snippet1)
                    .RemoveStrWithCheck(snippet2)
                    .RemoveStrWithCheck(snippet3);
                Directory.Delete(path + @"\XXYXX\Server\XXYXX.Cmd", true);
            }

            File.WriteAllText(userFile, userFileContent);
            File.WriteAllText(solutionFile, solutionFileContent);
        }
        private static void IfOnlyOneWebProjectMakeModelBeMvcModel(bool webTBS, bool webJQM, bool web, string path)
        {
            var count = 0;
            if (webTBS) count++;
            if (webJQM) count++;
            if (web) count++;
            if (count != 1) return;

            var userFile = path + @"\XXYXX\Server\Domain\Entities\XXYXXUser.cs";
            var userFileContent = File.ReadAllText(userFile);

            if (webTBS)
            {
                userFileContent = userFileContent.ReplaceStrWithCheck("XXYXXUserUpdatePasswordTBSMvcModel", "XXYXXUserUpdatePasswordMvcModel");

                var detailFile = path + @"\XXYXX\Server\Web.TBS\Views\XXYXXUserUpdatePassword\Detail.cshtml";
                var detailFileContent = File.ReadAllText(detailFile);
                detailFileContent = detailFileContent.ReplaceStrWithCheck("XXYXXUserUpdatePasswordTBSMvcModel", "XXYXXUserUpdatePasswordMvcModel");
                File.WriteAllText(detailFile, detailFileContent);

                var userUpdatePasswordControllerFile = path + @"\XXYXX\Server\Web.TBS\Controllers\XXYXXUserUpdatePasswordController.cs";
                var userUpdatePasswordControllerFileContent = File.ReadAllText(userUpdatePasswordControllerFile);
                userUpdatePasswordControllerFileContent = userUpdatePasswordControllerFileContent.ReplaceStrWithCheck("XXYXXUserUpdatePasswordTBSMvcModel", "XXYXXUserUpdatePasswordMvcModel");
                File.WriteAllText(userUpdatePasswordControllerFile, userUpdatePasswordControllerFileContent);
            }

            if (webJQM)
            {
                userFileContent = userFileContent.ReplaceStrWithCheck("XXYXXUserUpdatePasswordJQMMvcModel", "XXYXXUserUpdatePasswordMvcModel");

                var detailFile = path + @"\XXYXX\Server\Web.JQM\Views\XXYXXUserUpdatePassword\Detail.cshtml";
                var detailFileContent = File.ReadAllText(detailFile);
                detailFileContent = detailFileContent.ReplaceStrWithCheck("XXYXXUserUpdatePasswordJQMMvcModel", "XXYXXUserUpdatePasswordMvcModel");
                File.WriteAllText(detailFile, detailFileContent);

                var userUpdatePasswordControllerFile = path + @"\XXYXX\Server\Web.JQM\Controllers\XXYXXUserUpdatePasswordController.cs";
                var userUpdatePasswordControllerFileContent = File.ReadAllText(userUpdatePasswordControllerFile);
                userUpdatePasswordControllerFileContent = userUpdatePasswordControllerFileContent.ReplaceStrWithCheck("XXYXXUserUpdatePasswordJQMMvcModel", "XXYXXUserUpdatePasswordMvcModel");
                File.WriteAllText(userUpdatePasswordControllerFile, userUpdatePasswordControllerFileContent);
            }

            if (!webTBS) userFileContent = userFileContent.RemoveStrWithCheck("using Supermodel.DDD.Models.View.Mvc.TwitterBS;");
            if (!webJQM) userFileContent = userFileContent.RemoveStrWithCheck("using Supermodel.DDD.Models.View.Mvc.JQMobile;");
            if (!web) userFileContent = userFileContent.RemoveStrWithCheck("using Supermodel.DDD.Models.View.Mvc;");

            File.WriteAllText(userFile, userFileContent);
        }
        private static void GenerateReplacementGuids()
        {
            NewWebTBSProjGuidStr = Guid.NewGuid().ToString().ToUpper();
            NewWebJQMProjGuidStr = Guid.NewGuid().ToString().ToUpper();
            NewWebProjGuidStr = Guid.NewGuid().ToString().ToUpper();
            NewCmdProjGuidStr = Guid.NewGuid().ToString().ToUpper();
            NewDomainProjGuidStr = Guid.NewGuid().ToString().ToUpper();
            NewiOSProjGuidStr = Guid.NewGuid().ToString().ToUpper();
            NewDroidProjGuidStr = Guid.NewGuid().ToString().ToUpper();
            NewModelGenProjGuidStr = Guid.NewGuid().ToString().ToUpper();
            NewUWPProjGuidStr = Guid.NewGuid().ToString().ToUpper();
            NewSharedProjGuidStr = Guid.NewGuid().ToString().ToUpper();
        }
        private static void ReplaceGuidsInDir(string directory)
        {
            //Ignore .nuget and Frameworks directories
            var directoryName = Path.GetFileName(directory);
            if (directoryName == ".nuget" || directoryName == "Frameworks") return;

            foreach (var file in Directory.GetFiles(directory))
            {
                var ext = Path.GetExtension(file);
                if (ext == ".csproj" || ext == ".projitems" || ext == ".shproj" || ext == ".sln")
                {
                    //Replace Guids in file contents
                    var fileContents = File.ReadAllText(file);

                    fileContents = fileContents.Replace(WebTBSProjGuidStr, NewWebTBSProjGuidStr);
                    fileContents = fileContents.Replace(WebTBSProjGuidStr.ToLower(), NewWebTBSProjGuidStr.ToLower());

                    fileContents = fileContents.Replace(WebJQMProjGuidStr, NewWebJQMProjGuidStr);
                    fileContents = fileContents.Replace(WebJQMProjGuidStr.ToLower(), NewWebJQMProjGuidStr.ToLower());

                    fileContents = fileContents.Replace(WebProjGuidStr, NewWebProjGuidStr);
                    fileContents = fileContents.Replace(WebProjGuidStr.ToLower(), NewWebProjGuidStr.ToLower());

                    fileContents = fileContents.Replace(CmdProjGuidStr, NewCmdProjGuidStr);
                    fileContents = fileContents.Replace(CmdProjGuidStr.ToLower(), NewCmdProjGuidStr.ToLower());

                    fileContents = fileContents.Replace(DomainProjGuidStr, NewDomainProjGuidStr);
                    fileContents = fileContents.Replace(DomainProjGuidStr.ToLower(), NewDomainProjGuidStr.ToLower());

                    fileContents = fileContents.Replace(IOSProjGuidStr, NewiOSProjGuidStr);
                    fileContents = fileContents.Replace(IOSProjGuidStr.ToLower(), NewiOSProjGuidStr.ToLower());

                    fileContents = fileContents.Replace(DroidProjGuidStr, NewDroidProjGuidStr);
                    fileContents = fileContents.Replace(DroidProjGuidStr.ToLower(), NewDroidProjGuidStr.ToLower());

                    fileContents = fileContents.Replace(ModelGenProjGuidStr, NewModelGenProjGuidStr);
                    fileContents = fileContents.Replace(ModelGenProjGuidStr.ToLower(), NewModelGenProjGuidStr.ToLower());

                    fileContents = fileContents.Replace(UWPProjGuidStr, NewUWPProjGuidStr);
                    fileContents = fileContents.Replace(UWPProjGuidStr.ToLower(), NewUWPProjGuidStr.ToLower());

                    fileContents = fileContents.Replace(SharedProjGuidStr, NewSharedProjGuidStr);
                    fileContents = fileContents.Replace(SharedProjGuidStr.ToLower(), NewSharedProjGuidStr.ToLower());

                    File.WriteAllText(file, fileContents);
                }
            }

            foreach (var subDir in Directory.GetDirectories(directory)) ReplaceGuidsInDir(subDir);
        }
        private static void AutoAssignRandomPortForAWebProj(string webProjFile, int oldPort, int newPort)
        {
            var webProjFileContents = File.ReadAllText(webProjFile);
            webProjFileContents = webProjFileContents.ReplaceStrWithCheck($"<DevelopmentServerPort>{oldPort}</DevelopmentServerPort>", $"<DevelopmentServerPort>{newPort}</DevelopmentServerPort>");
            webProjFileContents = webProjFileContents.ReplaceStrWithCheck($"<IISUrl>http://localhost:{oldPort}/</IISUrl>", $"<IISUrl>http://localhost:{newPort}/</IISUrl>");
            File.WriteAllText(webProjFile, webProjFileContents);
        }
        private static void AdjustModelGeneratorToPointToRightProject(WebApiProjectEnum? webApiProject, string path, int newPortWebTBS, int newPortWebJQM, int newPortWeb)
        {
            var programFile = path + @"\XXYXX\Util\ModelGenerator\Program.cs";
            var programFileContents = File.ReadAllText(programFile);

            var modelGeneratorProjFile = path + @"\XXYXX\Util\ModelGenerator\ModelGenerator.csproj";
            var modelGeneratorProjFileContents = File.ReadAllText(modelGeneratorProjFile);

            var assemblyName = typeof(Creator).Assembly.GetName().Name;
            var refSnippet = ReadResourceTextFile($"{assemblyName}.SupermodelCreator.Snippets2Modify.ModelGeneratorReferencingWebProj.txt");
            string updatedRefSnippet = refSnippet;
            switch (webApiProject)
            {
                case WebApiProjectEnum.WebTBS:
                {
                    programFileContents = programFileContents.ReplaceStrWithCheck("typeof(Web.TBS.Global).Assembly", "typeof(Web.TBS.Global).Assembly");
                    AdjustWebApiIpAddressAndPortInWebApiDataContext(path, newPortWebTBS);
                    updatedRefSnippet = refSnippet.ReplaceStrWithCheck("Web.TBS", "Web.TBS").ReplaceStrWithCheck(WebTBSProjGuidStr.ToLower(), WebTBSProjGuidStr.ToLower());
                    break;
                }
                case WebApiProjectEnum.WebJQM:
                {
                    programFileContents = programFileContents.ReplaceStrWithCheck("typeof(Web.TBS.Global).Assembly", "typeof(Web.JQM.Global).Assembly");
                    AdjustWebApiIpAddressAndPortInWebApiDataContext(path, newPortWebJQM);
                    updatedRefSnippet = refSnippet.ReplaceStrWithCheck("Web.TBS", "Web.JQM").ReplaceStrWithCheck(WebTBSProjGuidStr.ToLower(), WebJQMProjGuidStr.ToLower());
                    break;
                }
                case WebApiProjectEnum.Web:
                {
                    programFileContents = programFileContents.ReplaceStrWithCheck("typeof(Web.TBS.Global).Assembly", "typeof(Web.Global).Assembly");
                    AdjustWebApiIpAddressAndPortInWebApiDataContext(path, newPortWeb);
                    updatedRefSnippet = refSnippet.ReplaceStrWithCheck("Web.TBS", "Web").ReplaceStrWithCheck(WebTBSProjGuidStr.ToLower(), WebProjGuidStr.ToLower());
                    break;
                }
            }
            modelGeneratorProjFileContents = modelGeneratorProjFileContents.ReplaceStrWithCheck(refSnippet, updatedRefSnippet);

            File.WriteAllText(modelGeneratorProjFile, modelGeneratorProjFileContents);
            File.WriteAllText(programFile, programFileContents);
        }
        private static void AdjustForXamarinFormsUIvsNativeUI(MobileApiEnum? mobileApi, string path)
        {
            if (mobileApi == MobileApiEnum.XamarinForms)
            {
                //Droid
                File.Delete(path + @"\XXYXX\Mobile\XXYXX.Droid\MainActivity.cs");
                File.Move(path + @"\XXYXX\Mobile\XXYXX.Droid\MainActivity.XamarinForms.cs", path + @"\XXYXX\Mobile\XXYXX.Droid\MainActivity.cs");

                //iOS
                File.Delete(path + @"\XXYXX\Mobile\XXYXX.iOS\AppDelegate.cs");
                File.Move(path + @"\XXYXX\Mobile\XXYXX.iOS\AppDelegate.XamarinForms.cs", path + @"\XXYXX\Mobile\XXYXX.iOS\AppDelegate.cs");

                //UWP
                File.Delete(path + @"\XXYXX\Mobile\XXYXX.UWP\App.xaml.cs");
                File.Delete(path + @"\XXYXX\Mobile\XXYXX.UWP\MainPage.xaml");
                File.Delete(path + @"\XXYXX\Mobile\XXYXX.UWP\MainPage.xaml.cs");
                File.Move(path + @"\XXYXX\Mobile\XXYXX.UWP\App.XamarinForms.xaml.cs", path + @"\XXYXX\Mobile\XXYXX.UWP\App.xaml.cs");
                File.Move(path + @"\XXYXX\Mobile\XXYXX.UWP\MainPage.XamarinForms.xaml", path + @"\XXYXX\Mobile\XXYXX.UWP\MainPage.xaml");
                File.Move(path + @"\XXYXX\Mobile\XXYXX.UWP\MainPage.XamarinForms.xaml.cs", path + @"\XXYXX\Mobile\XXYXX.UWP\MainPage.xaml.cs");
            }
            else
            {
                //Droid
                File.Delete(path + @"\XXYXX\Mobile\XXYXX.Droid\MainActivity.XamarinForms.cs");

                //iOS
                File.Delete(path + @"\XXYXX\Mobile\XXYXX.iOS\AppDelegate.XamarinForms.cs");

                //UWP
                File.Delete(path + @"\XXYXX\Mobile\XXYXX.UWP\App.XamarinForms.xaml.cs");
                File.Delete(path + @"\XXYXX\Mobile\XXYXX.UWP\MainPage.XamarinForms.xaml");
                File.Delete(path + @"\XXYXX\Mobile\XXYXX.UWP\MainPage.XamarinForms.xaml.cs");

                //Shared
                //XXYXX.Shared.projitems
                var projitemsFile = path + @"\XXYXX\Mobile\XXYXX.Shared\XXYXX.Shared.projitems";
                var projitemsFileContent = File.ReadAllText(projitemsFile);

                projitemsFileContent = projitemsFileContent.RemoveStrWithCheck("<Compile Include=\"$(MSBuildThisFileDirectory)AppCore\\XXYXXApp.cs\" />");
                Directory.Delete(path + @"\XXYXX\Mobile\XXYXX.Shared\AppCore", true);

                projitemsFileContent = projitemsFileContent.RemoveStrWithCheck("<EmbeddedResource Include=\"$(MSBuildThisFileDirectory)EmbeddedResources\\Logo.png\" />");
                Directory.Delete(path + @"\XXYXX\Mobile\XXYXX.Shared\EmbeddedResources", true);

                projitemsFileContent = projitemsFileContent.RemoveStrWithCheck("<Compile Include=\"$(MSBuildThisFileDirectory)Models\\XXYXXUserUpdatePassword.cs\" />");
                Directory.Delete(path + @"\XXYXX\Mobile\XXYXX.Shared\Models", true);

                projitemsFileContent = projitemsFileContent.RemoveStrWithCheck("<Compile Include=\"$(MSBuildThisFileDirectory)Pages\\Login\\LoginPage.cs\" />");
                projitemsFileContent = projitemsFileContent.RemoveStrWithCheck("<Compile Include=\"$(MSBuildThisFileDirectory)Pages\\Home\\HomePage.cs\" />");
                projitemsFileContent = projitemsFileContent.RemoveStrWithCheck("<Compile Include=\"$(MSBuildThisFileDirectory)Pages\\ChangePassword\\ChangePasswordPage.cs\" />");
                Directory.Delete(path + @"\XXYXX\Mobile\XXYXX.Shared\Pages", true);

                File.WriteAllText(projitemsFile, projitemsFileContent);
            }
        }
        private static void ReplaceMarkerInDir(string directory, string name)
        {
            //Ignore .nuget and Frameworks directories
            var directoryName = Path.GetFileName(directory);
            if (directoryName == ".nuget" || directoryName == "Frameworks") return;

            //Rename dir
            var newDirectory = directory.Replace(Marker, name);
            if (directory.Contains(Marker) && name != Marker) Directory.Move(directory, newDirectory);

            foreach (var file in Directory.GetFiles(newDirectory))
            {
                //Replace marker in file contents
                var fileContents = File.ReadAllText(file);
                if (fileContents.Contains(Marker))
                {
                    fileContents = fileContents.Replace(Marker, name);
                    File.WriteAllText(file, fileContents);
                }

                //Replace marker in file name 
                if (file.Contains(Marker) && name != Marker) File.Move(file, file.Replace(Marker, name));
            }

            foreach (var subDir in Directory.GetDirectories(newDirectory)) ReplaceMarkerInDir(subDir, name);
        }
        private static void GenerateNewRandomEncryptionKey(string path)
        {
            const string oldKey2Replace = @"{ 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF }; //TODO: This should have been replaced by the VS plugin";

            var sb = new StringBuilder();
            for (var i = 0; i < 16; i++)
            {
                var nextHex = Random.Next(255);
                sb.AppendFormat(i == 0 ? "0x{0:X2}" : ", 0x{0:X2}", nextHex);
            }
            var newKey = "{ " + sb + " }; //Randomly genrated by Supermodel VS plugin";

            var appFile = path + @"\XXYXX\Mobile\XXYXX.Shared\AppCore\XXYXXApp.cs";
            var appFileContents = File.ReadAllText(appFile);
            appFileContents = appFileContents.ReplaceStrWithCheck(oldKey2Replace, newKey);
            File.WriteAllText(appFile, appFileContents);
        }
        public static void DeleteWhatWeDontNeedForSnapshot(string directory)
        {
            //Ignore .nuget and Frameworks directories
            var directoryName = Path.GetFileName(directory);

            foreach (var file in Directory.GetFiles(directory))
            {
                var ext = Path.GetExtension(file);
                var fileName = Path.GetFileName(file);
                if (fileName == "project.lock.json" || fileName == "SupermodelVSSolutionTemplate.XXYXX.zip" || ext == ".suo" || ext == ".user") File.Delete(file);
            }

            foreach (var dir in Directory.GetDirectories(directory))
            {
                var dirName = Path.GetFileName(dir);
                if (dirName == "bin" || dirName == "obj" || dirName == "packages") Directory.Delete(dir, true);
                else DeleteWhatWeDontNeedForSnapshot(dir);
            }
        }

        private static string RemoveStrWithCheck(this string me, string str)
        {
            if (!me.Contains(str)) throw new Exception($"RemoveStrWithCheck: '{str.Substring(0, 60)}...' not found. \n" + GetStackTrace());
            return me.Replace(str, "");
        }
        private static string ReplaceStrWithCheck(this string me, string str1, string str2)
        {
            if (!me.Contains(str1)) throw new Exception($"ReplaceStrWithCheck: '{str1.Substring(0, 60)}...' not found. \n" + GetStackTrace());
            return me.Replace(str1, str2);
        }
        private static string ReadResourceTextFile(string resourceName)
        {
            using (var stream = typeof(Creator).GetTypeInfo().Assembly.GetManifestResourceStream(resourceName))
            {
                if (stream == null) throw new ArgumentException(resourceName + " is not found.");
                using (var reader = new StreamReader(stream)) { return reader.ReadToEnd(); }
            }
        }
        private static string GetServerIpAddress()
        {
            var localIp = "?";

            //try to find an ip using new method
            foreach (var ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    var ipProperties = ni.GetIPProperties();
                    foreach (var ip in ipProperties.UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == AddressFamily.InterNetwork && ipProperties.DnsSuffix == "localdomain")
                        {
                            localIp = ip.Address.ToString();
                        }
                    }
                }
            }

            //if new method did not work, use old method
            if (localIp == "?")
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                var addressList = host.AddressList;
                foreach (var ip in addressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork) localIp = ip.ToString();
                }
            }

            return localIp;
        }
        private static string GetStackTrace()
        {
            var stackTrace = new StackTrace();
            var stackFrames = stackTrace.GetFrames();
            var sb = new StringBuilder();
            if (stackFrames != null)
            {
                for(var i = 1; i < stackFrames.Length; i++)
                {
                    sb.Append($"\n{stackFrames[i].GetMethod()}");
                }
            }
            return sb.ToString();
        }
        #endregion

        #region Properties and Contants
        private static Random Random { get; } = new Random();
        private const string Marker = "XXYXX";
        private const string ZipFileName = "SupermodelVSSolutionTemplate.XXYXX.zip";

        const string WebTBSProjGuidStr = "AE75C293-4436-4E20-A846-F382656DAC2B";
        const string WebJQMProjGuidStr = "F06A3700-807C-4844-8218-BA13D1295F0E";
        const string WebProjGuidStr = "D7408613-8671-4FBC-87D2-310B4C77851E";
        const string CmdProjGuidStr = "6C8593BB-1AB5-4978-9C36-3EAC346B854A";
        const string DomainProjGuidStr = "94A06D7A-E408-4F87-AB12-F82BA95B503A";
        const string IOSProjGuidStr = "E10F2FFA-51CD-450B-9A5F-239233CEFEFB";
        const string DroidProjGuidStr = "AB720255-DB3B-4FF6-9313-57926BB40B4C";
        const string ModelGenProjGuidStr = "E3CF3252-AD84-4E23-BD38-9F9136752520";
        const string UWPProjGuidStr = "98AA2380-F62D-4785-AF29-BE9DA3F0C16C";
        const string SharedProjGuidStr = "03C0964B-5C5F-4D62-86FF-8381CBE9169E";

        const string OldIPAddress = "10.211.55.9";
        const int OldPortWebTBS = 52554;
        const int OldPortWebJQM = 59297;
        const int OldPortWeb = 50627;

        private static string NewWebTBSProjGuidStr { get; set; }
        private static string NewWebJQMProjGuidStr { get; set; }
        private static string NewWebProjGuidStr { get; set; }
        private static string NewCmdProjGuidStr { get; set; }
        private static string NewDomainProjGuidStr { get; set; }
        private static string NewiOSProjGuidStr { get; set; }
        private static string NewDroidProjGuidStr { get; set; }
        private static string NewModelGenProjGuidStr { get; set; }
        private static string NewUWPProjGuidStr { get; set; }
        private static string NewSharedProjGuidStr { get; set; }
        #endregion
    }
}
