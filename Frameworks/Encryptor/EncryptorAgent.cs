﻿using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using PCLCrypto;

namespace Encryptor
{
    public static class EncryptorAgent
    {
		public static byte[] Lock(byte[] key, string str, out byte[] iv)
		{
            var aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
            var cryptoKey = aes.CreateSymmetricKey(key);
            iv = WinRTCrypto.CryptographicBuffer.GenerateRandom(16); //16 bytes is the stnadrd size for IV lenth for AES 256
            var encryptor = WinRTCrypto.CryptographicEngine.CreateEncryptor(cryptoKey, iv);

            using (var memstrm = new MemoryStream())
            {
                using (var csw = new CryptoStream(memstrm, encryptor, CryptoStreamMode.Write))
                {
                    csw.Write(Encoding.ASCII.GetBytes(str), 0, str.Length); //This breaks old code
                    csw.FlushFinalBlock();
                    var cryptdata = memstrm.ToArray();
                    return cryptdata;
                }
            }
		}

        public static byte[] Lock(byte[] key, string str, byte[] iv)
		{
		    var aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
		    var cryptoKey = aes.CreateSymmetricKey(key);
		    //iv = WinRTCrypto.CryptographicBuffer.GenerateRandom(16); //16 bytes is the stnadrd size for IV lenth for AES 256
		    var encryptor = WinRTCrypto.CryptographicEngine.CreateEncryptor(cryptoKey, iv);
		
		    using (var memstrm = new MemoryStream())
		    {
		        using (var csw = new CryptoStream(memstrm, encryptor, CryptoStreamMode.Write))
		        {
		            csw.Write(Encoding.ASCII.GetBytes(str), 0, str.Length); //This breaks old code
		            csw.FlushFinalBlock();
		            var cryptdata = memstrm.ToArray();
		            return cryptdata;
		        }
		    }
		}

        public static byte[] LockUnicode(byte[] key, string str, out byte[] iv)
        {
            var aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
            var cryptoKey = aes.CreateSymmetricKey(key);
            iv = WinRTCrypto.CryptographicBuffer.GenerateRandom(16); //16 bytes is the stnadrd size for IV lenth for AES 256
            var encryptor = WinRTCrypto.CryptographicEngine.CreateEncryptor(cryptoKey, iv);

            using (var memstrm = new MemoryStream())
            {
                using (var csw = new CryptoStream(memstrm, encryptor, CryptoStreamMode.Write))
                {
                    var bytes = Converter.StringToByteArr(str);
                    csw.Write(bytes, 0, bytes.Length); //This breaks old code
                    csw.FlushFinalBlock();
                    var cryptdata = memstrm.ToArray();
                    return cryptdata;
                }
            }
        }

        public static string Unlock(byte[] key, byte[] code, byte[] iv)
        {
            var aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
            var cryptoKey = aes.CreateSymmetricKey(key);
            var decryptor = WinRTCrypto.CryptographicEngine.CreateDecryptor(cryptoKey, iv);

            using (var memstrm = new MemoryStream(code) { Position = 0 })
            {
                using (var csr = new CryptoStream(memstrm, decryptor, CryptoStreamMode.Read))
                {
                    var dataFragments = new List<byte[]>();
                    var recv = 0;
                    while (true)
                    {
                        var dataFragment = new byte[1024];
                        var recvdThisFragment = csr.Read(dataFragment, 0, dataFragment.Length);
                        if (recvdThisFragment == 0) break;
                        dataFragments.Add(dataFragment);
                        recv = recv + recvdThisFragment;
                    }

                    var data = new byte[dataFragments.Count * 1024];
                    var idx = 0;

                    //now let's build the entire data
                    foreach (var t in dataFragments)
                    {
                        for (var j = 0; j < 1024; j++) data[idx++] = t[j];
                    }

                    var newphrase = Encoding.ASCII.GetString(data, 0, recv);
                    return newphrase;
                }
            }
        }

        public static string UnlockUnicode(byte[] key, byte[] code, byte[] iv)
        {
            var aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
            var cryptoKey = aes.CreateSymmetricKey(key);
            var decryptor = WinRTCrypto.CryptographicEngine.CreateDecryptor(cryptoKey, iv);

            using (var memstrm = new MemoryStream(code) { Position = 0 })
            {
                using (var csr = new CryptoStream(memstrm, decryptor, CryptoStreamMode.Read))
                {
                    var dataFragments = new List<byte[]>();
                    var recv = 0;
                    while (true)
                    {
                        var dataFragment = new byte[1024];
                        var recvdThisFragment = csr.Read(dataFragment, 0, dataFragment.Length);
                        if (recvdThisFragment == 0) break;
                        dataFragments.Add(dataFragment);
                        recv = recv + recvdThisFragment;
                    }

                    var data = new byte[dataFragments.Count * 1024];
                    var idx = 0;

                    //now let's build the entire data
                    foreach (var t in dataFragments)
                    {
                        for (var j = 0; j < 1024; j++) data[idx++] = t[j];
                    }

                    var newphrase = Encoding.Unicode.GetString(data, 0, recv);
                    return newphrase;
                }
            }
        }

        //Sample key, 16 bytes
        //private readonly static byte[] _key = { 0xA6, 0x46, 0x10, 0xF1, 0xEA, 0x16, 0x51, 0xA0, 0xB2, 0x41, 0x27, 0x5C, 0x23, 0x9C, 0xF0, 0xDD };
    }
}

