﻿using System;
using System.IO;
using System.Runtime.Serialization;
using PCLCrypto;

namespace Encryptor
{
    public static class HashExtensions
    {
        public static string GetHash(this object instance, PCLCrypto.HashAlgorithm hashAlgorithm)
        {
            return ComputeHash(instance, hashAlgorithm);
        }
        //public static string GetKeyedHash<T>(this object instance, byte[] key) where T : KeyedHashAlgorithm, new()
        //{
        //    T cryptoServiceProvider = new T { Key = key };
        //    return ComputeHash(instance, cryptoServiceProvider);
        //}
        public static string GetMD5Hash(this object instance)
        {
            return instance.GetHash(PCLCrypto.HashAlgorithm.Md5);
        }
        public static string GetSHA1Hash(this object instance)
        {
            return instance.GetHash(PCLCrypto.HashAlgorithm.Sha1);
        }        
        public static string GetSHA256Hash(this object instance)
        {
            return instance.GetHash(PCLCrypto.HashAlgorithm.Sha256);
        }        
        public static string GetSHA512Hash(this object instance)
        {
            return instance.GetHash(PCLCrypto.HashAlgorithm.Sha512);
        }        
        private static string ComputeHash(object instance, PCLCrypto.HashAlgorithm hashAlgorithm)
        {
            var serializer = new DataContractSerializer(instance.GetType());
            using (var memoryStream = new MemoryStream())
            {
                serializer.WriteObject(memoryStream, instance);
                var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(hashAlgorithm);
                return Convert.ToBase64String(hasher.HashData(memoryStream.ToArray()));
            }
        }
    }
}
