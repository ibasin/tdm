﻿using System;
using System.IO;

namespace RuntimeGenerator
{
    class Program
    {
        static void Main()
        {
            //try
            //{
            //    byte[] key = { 0xA6, 0x44, 0x40, 0xF1, 0xEA, 0x16, 0x51, 0xA0, 0xB2, 0x41, 0x2A, 0x5C, 0x23, 0x9C, 0xF3, 0xDD };
            //    var text = "I like to eat parking meters";
            //    byte[] iv = WinRTCrypto.CryptographicBuffer.GenerateRandom(16);
            //    var codeLockedWithOld = EncryptorAgentOld.Lock(key, text, iv);
            //    var codeLockedWithNew = EncryptorAgent.Lock(key, text, iv);

            //    var b = codeLockedWithOld.SequenceEqual(codeLockedWithNew);

            //    var newText1 = EncryptorAgent.Unlock(key, codeLockedWithOld, iv);
            //    var newText2 = EncryptorAgentOld.Unlock(key, codeLockedWithNew, iv);
            //}
            //catch (Exception ex)
            //{
            //    throw;
            //}

            //byte[] key = { 0xA6, 0x46, 0x10, 0xF1, 0xEA, 0x16, 0x51, 0xA0, 0xB2, 0x41, 0x27, 0x5C, 0x23, 0x9C, 0xF0, 0xDD };
            //var h1 = key.GetHash(HashAlgorithm.Md5);
            //var h2 = key.GetHash<MD5Cng>();
            //var r = h1 == h2;

            //var text = "Hello World";
            //byte[] iv = WinRTCrypto.CryptographicBuffer.GenerateRandom(16); 
            //var e1 = EncryptorAgentOld.Lock(key, text, iv);
            //var e2 = EncryptorAgent.Lock(key, text, iv);
            //var b = e1.SequenceEqual(e2);
            //var text2 = EncryptorAgent.Unlock(key, e1, iv);

            var sb = new CSOutStringBuilder();
            //sb.AppendLine("//Autogenrated on " + DateTime.Now);
            sb = GenerateForDir(@"..\..\Supermodel.Mobile.Runtime", sb, true);
            //File.WriteAllText(@"C:\Users\Ilya\Documents\Supermobile\supermobile\RuntimeGenerator\" + "Supermodel.Mobile.Runtime.cs", sb.ToString());
            File.WriteAllText(@"..\..\Supermodel.Mobile.Runtime.cs", sb.ToString());
            File.WriteAllText(@"..\..\..\..\Supermodel\Mobile\Xamarin\Supermodel.Mobile.Runtime.cs", sb.ToString());
            Console.WriteLine("Supermodel.Mobile.Runtime.cs Generated Successfully. Press Enter...");
            Console.ReadLine();
        }

        private static CSOutStringBuilder GenerateForDir(string initialDir, CSOutStringBuilder sb = null, bool root = false)
        {
            try
            {
                if (sb == null) sb = new CSOutStringBuilder();
                if (!root) sb.AppendLine("");
                var dirName = Path.GetFileName(initialDir);
                sb.AppendLineIndentPlus("#region " + dirName);

                foreach (var dir in Directory.GetDirectories(initialDir))
                {
                    GenerateForDir(dir, sb);
                }
                
                foreach (var file in Directory.GetFiles(initialDir))
                {
                    sb.AppendLine("");
                    sb.AppendLine("#region " + Path.GetFileNameWithoutExtension(file));

                    if (File.ReadAllText(file).Replace("// ReSharper disable once CheckNamespace", "").TrimStart().StartsWith("using"))
                    {
                        var currentColor = Console.ForegroundColor;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"Warning: {file} starts with 'using' statement");
                        Console.ForegroundColor = currentColor;
                    }

                    using (var fileStream = new StreamReader(file))
                    {
                        while (true)
                        {
                            var line = fileStream.ReadLine();
                            if (line == null) break;
                            sb.AppendLine(line);
                        }
                    }
                    
                    sb.AppendLine("#endregion");
                }

                sb.AppendLine("");
                //if (dirName == "Multipart" || dirName == "ReflectionMapper" || dirName == "Encryptor") sb.AppendLine("#endif");
                sb.AppendLineIndentMinus("#endregion");
            }
            catch (Exception ex)
            {
                Console.WriteLine("FATAL ERROR: " + ex.Message);
                Console.WriteLine("Press enter");
                Console.ReadLine();
                Environment.Exit(-1);
            }
            return sb;
        }
    }
}
