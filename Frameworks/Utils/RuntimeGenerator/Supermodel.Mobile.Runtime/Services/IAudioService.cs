﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Services
{
    public interface IAudioService
    {
        void Play(byte[] wavSound);
        
        //void StartRecording();
        //byte[] StopRecording();
    }
}