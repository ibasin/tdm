﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Services
{
    #if __IOS__
    using AVFoundation;
    using Foundation;

    public class AudioService : IAudioService
    {
        public void Play(byte[] wavSound)
        {
            if (_player == null || !_player.Playing)
            {
                _player = AVAudioPlayer.FromData(NSData.FromArray(wavSound));
                _player.Play();
            }
        }

        //public void StartRecording()
        //{
        //    throw new System.NotImplementedException();
        //    //var recorder = new AVAudioRecorder();
        //    //recorder.
        //}

        //public byte[] StopRecording()
        //{
        //    throw new System.NotImplementedException();
        //}

        private static AVAudioPlayer _player;
    }
    #endif

    #if __ANDROID__
    using Services;
    using Android.Media;
    using Java.IO;
    using Android.App;
    using XForms.App;

	public class AudioService : IAudioService
	{
		public void Play(byte[] wavSound)
		{
		    if (_player == null || !_player.IsPlaying)
		    {
		        if (_player != null) _player.Release();
                var tempWav = File.CreateTempFile("temp", "wav", FormsApplication.MainActivity.CacheDir);
		        tempWav.DeleteOnExit();
		        var fos = new FileOutputStream(tempWav);
		        fos.Write(wavSound);
		        fos.Close();
		                
		        _player = new MediaPlayer();
		        var fis = new FileInputStream(tempWav);
		        _player.SetDataSource(fis.FD);
		
		        _player.Prepare();
		        _player.Start();
		    }        
		}
		
		private static MediaPlayer _player; //AudioTrack
	}
    #endif

    #if WINDOWS_UWP
    using System.IO;
    using Windows.UI.Xaml.Controls;

    public class AudioService : IAudioService
    {
        public void Play(byte[] wavSound)
        {
            var player = new MediaElement();
            var stream = new MemoryStream(wavSound);
            player.SetSource(stream.AsRandomAccessStream(), "wav");
            player.Play();
        }
    }
    #endif
}
