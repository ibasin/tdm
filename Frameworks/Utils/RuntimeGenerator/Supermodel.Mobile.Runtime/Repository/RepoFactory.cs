﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Repository
{
    using System;
    using Models;
    using ReflectionMapper;
    using UnitOfWork;
    
    public static class RepoFactory
    {
        public static IDataRepo<ModelT> Create<ModelT>() where ModelT : class, IModel, new()
        {
            return UnitOfWorkContextCore.CurrentDataContext.CreateRepo<ModelT>();
        }
        public static object CreateForRuntimeType(Type modelType)
        {
            return ReflectionHelper.ExecuteStaticGenericMethod(typeof(RepoFactory), "Create", new[] { modelType });
        }
    }
}
