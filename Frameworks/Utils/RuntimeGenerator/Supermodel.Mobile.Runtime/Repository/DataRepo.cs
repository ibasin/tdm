﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Repository
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataContext.Core;
    using Exceptions;
    using Models;
    using UnitOfWork;
    
    public class DataRepo<ModelT> : IDataRepo<ModelT> where ModelT : class, IModel, new()
    {
        #region Reads
        public virtual Task<ModelT> GetByIdAsync(long id)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetByIdAsync operation");
            return ((IReadableDataContext) context).GetByIdAsync<ModelT>(id);
        }
        public virtual Task<ModelT> GetByIdOrDefaultAsync(long id)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetByIdOrDefaultAsync operation");
            return ((IReadableDataContext) context).GetByIdOrDefaultAsync<ModelT>(id);
        }
        public virtual Task<List<ModelT>> GetAllAsync(int? skip = null, int? take = null)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetAllAsync operation");
            return ((IReadableDataContext) context).GetAllAsync<ModelT>(skip, take);
        }
        public virtual Task<long> GetCountAllAsync(int? skip = null, int? take = null)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetCountAllAsync operation");
            return ((IReadableDataContext) context).GetCountAllAsync<ModelT>(skip, take);
        }
        #endregion

        #region Batch Reads
        public virtual void DelayedGetById(out DelayedModel<ModelT> model, long id)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetById operation");
            ((IReadableDataContext) context).DelayedGetById(out model, id);
        }
        public virtual void DelayedGetByIdOrDefault(out DelayedModel<ModelT> model, long id)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetByIdOrDefault operation");
            ((IReadableDataContext) context).DelayedGetByIdOrDefault(out model, id);
        }
        public virtual void DelayedGetAll(out DelayedModels<ModelT> models)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetAll operation");
            ((IReadableDataContext) context).DelayedGetAll(out models);
        }
        public virtual void DelayedGetCountAll(out DelayedCount count)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetCountAll operation");
            ((IReadableDataContext) context).DelayedGetCountAll<ModelT>(out count);
        }
        #endregion

        #region Queries
        public virtual Task<List<ModelT>> GetWhereAsync(object searchBy, string sortBy = null, int? skip = null, int? take = null)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetWhereAsync operation");
            return ((IQuerableReadableDataContext) context).GetWhereAsync<ModelT>(searchBy, sortBy, skip, take);
        }
        public virtual Task<long> GetCountWhereAsync(object searchBy)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetCountWhereAsync operation");
            return ((IQuerableReadableDataContext) context).GetCountWhereAsync<ModelT>(searchBy);
        }
        #endregion

        #region Delayed Queries
        public virtual void DelayedGetWhere(out DelayedModels<ModelT> models, object searchBy, string sortBy = null, int? skip = null, int? take = null)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetWhere operation");
            ((IQuerableReadableDataContext) context).DelayedGetWhere(out models, searchBy, sortBy, skip, take);
        }
        public virtual void DelayedGetCountWhere(out DelayedCount count, object searchBy)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetCountWhere operation");
            ((IQuerableReadableDataContext) context).DelayedGetCountWhere<ModelT>(out count, searchBy);
        }
        #endregion

        #region Writes
        public virtual void Add(ModelT model)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IWriteableDataContext)) throw new SupermodelException("Curent DataContext does not support Add operation");
            ((IWriteableDataContext) context).Add(model);
        }
        public virtual void Delete(ModelT model)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IWriteableDataContext)) throw new SupermodelException("Curent DataContext does not support Delete operation");
            ((IWriteableDataContext) context).Delete(model);
        }
        public virtual void ForceUpdate(ModelT model)
        {
            var context = UnitOfWorkContextCore.CurrentDataContext;
            if (!(context is IWriteableDataContext)) throw new SupermodelException("Curent DataContext does not support ForceUpdate operation");
            ((IWriteableDataContext) context).ForceUpdate(model);
        }
        #endregion
    }
}
