﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Models
{
	using System;
    
    public class BinaryFile
	{
		public String Name { get; set; }
		public Byte[] BinaryContent { get; set; }
	}
}
