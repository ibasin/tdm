﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using Newtonsoft.Json;
    using Repository;
    using DataContext.Core;
    using ReflectionMapper;
    using XForms.ViewModels;
    using System.ComponentModel;
    using Xamarin.Forms;
    using DataContext.Sqlite;
    using Exceptions;
    using UnitOfWork;
    using System.Linq;

    public abstract class Model : IModel, ISupermodelListTemplate
    {
        #region Methods
        public virtual List<ChildModelT> GetChildList<ChildModelT>(params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new()
        {
            throw new NotImplementedException("If Model has children, you must overrdie GetChildList<ChildModelT>");
        }
        public virtual ChildModelT GetChild<ChildModelT>(Guid childGuidIdentity, params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new()
        {
            if (parentGuidIdentities == null) throw new ArgumentNullException(nameof(parentGuidIdentities), "Overrdie AfterLoad() on root Model and assign parent adentities for each child");
            var child = GetChildOrDefault<ChildModelT>(childGuidIdentity, parentGuidIdentities);
            if (child == null) throw new InvalidOperationException("No element satisfies the condition in predicate.");
            return child;
        }
        public virtual ChildModelT GetChildOrDefault<ChildModelT>(Guid childGuidIdentity, params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new()
        {
            if (parentGuidIdentities == null) throw new ArgumentNullException(nameof(parentGuidIdentities), "Overrdie AfterLoad() on root Model and assign parent adentities for each child");
            var child = GetChildList<ChildModelT>(parentGuidIdentities).SingleOrDefault(x => x.ChildGuidIdentity == childGuidIdentity);
            return child;
        }
        public virtual void AddChild<ChildModelT>(ChildModelT child, int? index = null) where ChildModelT : ChildModel, new()
        {
            if (child == null) throw new ArgumentNullException(nameof(child));
            if (child.ParentGuidIdentities == null) throw new ArgumentNullException(nameof(child.ParentGuidIdentities), "Overrdie AfterLoad() on root Model and assign ParentIdentities for each child");
            if (GetChildOrDefault<ChildModelT>(child.ChildGuidIdentity, child.ParentGuidIdentities) != null) throw new SupermodelException("Model.AddChild<ChildModelT>(): Attempting to add a duplicate child");
            GetChildList<ChildModelT>(child.ParentGuidIdentities).Add(child);
        }
        public virtual int DeleteChild<ChildModelT>(ChildModelT child) where ChildModelT : ChildModel, new()
        {
            if (child == null) throw new ArgumentNullException(nameof(child));
            if (child.ParentGuidIdentities == null) throw new ArgumentNullException(nameof(child.ParentGuidIdentities), "Overrdie AfterLoad() on root Model and assign ParentIdentities for each child");
            var index = GetChildList<ChildModelT>(child.ParentGuidIdentities).IndexOf(child);
            if (index < 0) throw new SupermodelException("DeleteChild(): Element not foubnd");
            GetChildList<ChildModelT>(child.ParentGuidIdentities).RemoveAt(index);
            return index;
        }

        public virtual void Add()
        {
            CreateRepo().ExecuteMethod("Add", this);
        }
        public virtual void Delete()
        {
            CreateRepo().ExecuteMethod("Delete", this);
        }
        public virtual void Update()
        {
            CreateRepo().ExecuteMethod("ForceUpdate", this);
        }

        public virtual void BeforeSave(PendingAction.OperationEnum operation)
        {
            //default is doing nothing
        }
        public virtual void AfterLoad()
        {
            //default is doing nothing
        }

        public virtual object CreateRepo()
        {
            return RepoFactory.CreateForRuntimeType(GetType());
        }
        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new ValidationResultList();
        }
        #endregion

        #region ISupermodelListTemplate implemetation
        public virtual DataTemplate GetListCellDataTemplate(EventHandler deleteItemHandler)
        {
            var dataTemplate = new DataTemplate(() =>
            {
                var cell = ReturnACell();
                if (deleteItemHandler != null)
                {
                    var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true };
                    deleteAction.SetBinding(MenuItem.CommandParameterProperty, new Binding("."));
                    cell.ContextActions.Add(deleteAction);
                    deleteAction.Clicked += deleteItemHandler;
                }
                return cell;
            });
            SetUpBindings(dataTemplate);
            return dataTemplate;
        }
        public virtual Cell ReturnACell()
        {
            var msg = $"In order to use '{GetType().Name}' class with CRUD features, you must override ReturnACell() and SetUpBindings() methods!";
            throw new NotImplementedException(msg);
        }
        public virtual void SetUpBindings(DataTemplate dataTemplate)
        {
            var msg = $"In order to use '{GetType().Name}' class with CRUD features, you must override ReturnACell() and SetUpBindings() methods!";
            throw new NotImplementedException(msg);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region Properties
        public long Id { get; set; }

        [NotRMapped] public DateTime? BroughtFromMasterDbOnUtc { get; set; }
        public bool ShouldSerializeBroughtFromMasterDbOnUtc()
        {
            if (UnitOfWorkContextCore.StackCount == 0) return false;
            return UnitOfWorkContextCore.CurrentDataContext is SqliteDataContext;
        }
        //public bool ShouldSerializeBroughtFromMasterDbOnUtc() { return !SerializingForMasterDb; }
        //public IModel PerpareForSerializingForMasterDb() { SerializingForMasterDb = true; return this; }
        //public IModel PerpareForSerializingForLocalDb() { SerializingForMasterDb = false; return this; }
        //[JsonIgnore] protected bool SerializingForMasterDb { get; set; }

        [JsonIgnore, NotRMapped] public virtual bool IsNew => Id == 0;
        #endregion
    }
}
