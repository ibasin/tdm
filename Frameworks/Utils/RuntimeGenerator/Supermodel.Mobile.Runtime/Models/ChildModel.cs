﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Models
{
    using ReflectionMapper;
    using XForms.ViewModels;
    using System;
    using System.ComponentModel;
    using Xamarin.Forms;
    using Newtonsoft.Json;

    public abstract class ChildModel: ISupermodelListTemplate
    {
        #region Overrdies
        [JsonIgnore, NotRCompared] public virtual Guid[] ParentGuidIdentities { get; set; }
        [JsonIgnore, NotRCompared] public virtual Guid ChildGuidIdentity { get; set; } = Guid.NewGuid();

        public virtual DataTemplate GetListCellDataTemplate(EventHandler deleteItemHandler)
        {
            var dataTemplate = new DataTemplate(() =>
            {
                var cell = ReturnACell();
                if (deleteItemHandler != null)
                {
                    var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true, Parent = cell };
                    deleteAction.SetBinding(MenuItem.CommandParameterProperty, new Binding("."));
                    cell.ContextActions.Add(deleteAction);
                    deleteAction.Clicked += deleteItemHandler;
                }
                return cell;
            });
            SetUpBindings(dataTemplate);
            return dataTemplate;
        }
        public virtual Cell ReturnACell()
        {
            var msg = $"In order to use '{GetType().Name}' class with CRUD features, you must override ReturnACell() and SetUpBindings() methods!";
            throw new NotImplementedException(msg);
        }
        public virtual void SetUpBindings(DataTemplate dataTemplate)
        {
            var msg = $"In order to use '{GetType().Name}' class with CRUD features, you must override ReturnACell() and SetUpBindings() methods!";
            throw new NotImplementedException(msg);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
