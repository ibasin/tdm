﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Models
{
    using System;

    public abstract class DirectChildModel : ChildModel
    {
        protected DirectChildModel()
        {
            // ReSharper disable once VirtualMemberCallInConstructor
            ParentGuidIdentities = new Guid[0];
        }
    }
}
