﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using DataContext.Core;
    using System.Collections.Generic;

    public interface IModel : /*IObjectWithIdentity,*/ IValidatableObject
    {
        long Id { get; set; }
        
        DateTime? BroughtFromMasterDbOnUtc { get; set; }

        bool IsNew { get; }
        
        //IModel PerpareForSerializingForMasterDb();
        //IModel PerpareForSerializingForLocalDb();

        void Add();
        void Delete();
        void Update();

        void BeforeSave(PendingAction.OperationEnum operation);
        void AfterLoad();

        List<ChildModelT> GetChildList<ChildModelT>(params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new();
        ChildModelT GetChild<ChildModelT>(Guid childGuidIdentity, params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new();
        ChildModelT GetChildOrDefault<ChildModelT>(Guid childGuidIdentity, params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new();
        void AddChild<ChildModelT>(ChildModelT child, int? index = null) where ChildModelT : ChildModel, new();
        int DeleteChild<ChildModelT>(ChildModelT child) where ChildModelT : ChildModel, new();
    }
}

