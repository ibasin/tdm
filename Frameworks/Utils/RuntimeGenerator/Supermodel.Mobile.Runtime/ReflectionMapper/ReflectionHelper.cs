﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.ReflectionMapper
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Text.RegularExpressions;

    public static class ReflectionHelper
    {
        #region Methods
        public static string GetThrowingContext()
        {
            #if WINDOWS_UWP
            var stackFrame = new StackTrace(new Exception(), false).GetFrames()[2];
            #else
            var stackFrame = new StackTrace().GetFrame(2);
            #endif
            return stackFrame.GetMethod().DeclaringType + "::" + stackFrame.GetMethod().Name + "()";
        }
        public static string GetCurrentContext()
        {
            #if WINDOWS_UWP
            var stackFrame = new StackTrace(new Exception(), false).GetFrames()[1];
            #else
            var stackFrame = new StackTrace().GetFrame(1);
            #endif
            return stackFrame.GetMethod().DeclaringType + "::" + stackFrame.GetMethod().Name + "()";
        }
        public static object CreateType(Type type, params object[] args)
        {
            //if (type.IsGenericType)
            if (type.GetTypeInfo().IsGenericType)
            {
                return CreateGenericType(type.GetGenericTypeDefinition(), type.GetGenericArguments());
            }
            else
            {
                return Activator.CreateInstance(type, args);
            }
        }
        public static object CreateGenericType(Type genericType, Type[] innerTypes, params object[] args)
        {
            var specificType = genericType.MakeGenericType(innerTypes);
            return Activator.CreateInstance(specificType, args);
        }
        public static object CreateGenericType(Type genericType, Type innerType, params object[] args)
        {
            return CreateGenericType(genericType, new[] {innerType}, args);
        }
        public static object ExecuteStaticMethod(Type typeofClassWithStaticMethod, string methodName, params object[] args)
        {
            var methodInfo = typeofClassWithStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.Public).Single(m => m.Name == methodName && !m.IsGenericMethod && m.GetParameters().Length == args.Length);
            return methodInfo.Invoke(null, args);
        }
        
        public static object ExecuteNonPublicStaticMethod(Type typeofClassWithStaticMethod, string methodName, params object[] args)
        {
            var methodInfo = typeofClassWithStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.NonPublic).Single(m => m.Name == methodName && !m.IsGenericMethod && m.GetParameters().Length == args.Length);
            return methodInfo.Invoke(null, args);
        }
        
        public static object ExecuteStaticGenericMethod(Type typeofClassWithGenericStaticMethod, string methodName, Type[] genericArguments, params object[] args)
        {
            var methodInfo = typeofClassWithGenericStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.Public).Single(m => m.Name == methodName && m.IsGenericMethod&& m.GetParameters().Length == args.Length);
            var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
            return genericMethodInfo.Invoke(null, args);
        }

        public static object ExecuteNonPublicStaticGenericMethod(Type typeofClassWithGenericStaticMethod, string methodName, Type[] genericArguments, params object[] args)
        {
            var methodInfo = typeofClassWithGenericStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.NonPublic).Single(m => m.Name == methodName && m.IsGenericMethod&& m.GetParameters().Length == args.Length);
            var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
            return genericMethodInfo.Invoke(null, args);
        }

        public static bool IsClassADerivedFromClassB(Type a, Type b)
        {
            return IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(a, b) != null;
        }
        public static Type IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(Type a, Type b)
        {
            if (a == b) return a;

            //var aBaseType = a.BaseType;
            var aBaseType = a.GetTypeInfo().BaseType;

            if (aBaseType == null) return null;

            //if (b.IsGenericTypeDefinition && aBaseType.IsGenericType)
            if (b.GetTypeInfo().IsGenericTypeDefinition && aBaseType.GetTypeInfo().IsGenericType)
            {
                if (aBaseType.GetGenericTypeDefinition() == b) return aBaseType;
            }
            else
            {
                if (aBaseType == b) return aBaseType;
            }
            return IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(aBaseType, b);
        }
        #endregion

        #region Extnesions for Fluent interface
        public static string InsertSpacesBetweenWords(this string str)
        {
            var result = Regex.Replace(str, @"(\B[A-Z][^A-Z]+)|\B(?<=[^A-Z]+)([A-Z]+)(?![^A-Z])", " $1$2");
            return result
                .Replace(" Or ", " or ")
                .Replace(" And ", " and ")
                .Replace(" Of ", " of ")
                .Replace(" On ", " on ")
                .Replace(" The ", " the ")
                .Replace(" For ", " for ")
                .Replace(" At ", " at ")
                .Replace(" A ", " a ")
                .Replace(" In ", " in ")
                .Replace(" By ", " by ")
                .Replace(" About ", " about ")
                .Replace(" To ", " to ")
                .Replace(" From ", " from ")
                .Replace(" With ", " with ")
                .Replace(" Over ", " over ")
                .Replace(" Into ", " into ")
                .Replace(" Without ", " without ");
        }

        public static string GetTypeDescription(this Type type)
        {
            //var attr = MyAttribute.GetCustomAttribute(type, typeof(DescriptionAttribute), true);
            var attr = type.GetTypeInfo().GetCustomAttribute(typeof(DescriptionAttribute), true);
            return attr != null ? ((DescriptionAttribute)attr).Description : type.ToString().InsertSpacesBetweenWords();
        }
        public static string GetTypeFriendlyDescription(this Type type)
        {
            //var attr = MyAttribute.GetCustomAttribute(type, typeof(DescriptionAttribute), true);
            var attr = type.GetTypeInfo().GetCustomAttribute(typeof(DescriptionAttribute), true);
            return attr != null ? ((DescriptionAttribute)attr).Description : type.Name.InsertSpacesBetweenWords();
        }
        public static string GetDisplayNameForProperty(this Type type, string propertyName)
        {
            var propertyInfo = type.GetProperty(propertyName);
            var attr = propertyInfo.GetCustomAttribute(typeof (DisplayNameAttribute), true);
            return attr != null ? ((DisplayNameAttribute)attr).DisplayName : propertyName.InsertSpacesBetweenWords();
        }
        public static object ExecuteGenericMethod(this object me, string methodName, Type[] genericArguments, params object[] args)
        {
            try
            {
                var methodInfo = me.GetType().GetMethods().Single(x => x.Name == methodName && x.IsGenericMethod);
                var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
                return genericMethodInfo.Invoke(me, args);
            }
            catch (Exception)
            {
                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
            }
        }
        public static object ExecuteNonPublicGenericMethod(this object me, string methodName, Type[] genericArguments, params object[] args)
        {
            try
            {
                var methodInfo = me.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Single(x => x.Name == methodName && x.IsGenericMethod);
                var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
                return genericMethodInfo.Invoke(me, args);
            }
            catch (Exception)
            {
                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
            }
        }
        public static object ExecuteMethod(this object me, string methodName, params object[] args)
        {
            var method = me.GetType().GetMethods().SingleOrDefault(x => x.Name == methodName);
            if (method == null)
            {
                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
            }
            return method.Invoke(me, args);
        }
        public static object ExecuteNonPublicMethod(this object me, string methodName, params object[] args)
        {
            try
            {
                return me.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Single(x => x.Name == methodName).Invoke(me, args);
            }
            catch (Exception)
            {
                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
            }
        }

        public static object PropertyGet(this object me, string propertyName, object[] index = null)
        {
            return me.GetPropertyInfo(propertyName).GetValue(me, index);
        }
        public static object PropertyGetNonPublic(this object me, string propertyName, object[] index = null)
        {
            return me.GetPropertyInfoNonPublic(propertyName).GetValue(me, index);
        }

        public static void PropertySet(this object me, string propertyName, object newValue, bool ignoreNoSetMethod = false, object[] index = null)
        {
            var myProperty = me.GetPropertyInfo(propertyName);
            var setMethod = myProperty.GetSetMethod(true);
            if (setMethod != null)
            {
                setMethod.Invoke(me, new[] { newValue });
            }
            else
            {
                if (!ignoreNoSetMethod) myProperty.SetValue(me, newValue, index);
            }
        }
        public static void PropertySetNonPublic(this object me, string propertyName, object newValue, bool ignoreNoSetMethod = false, object[] index = null)
        {
            var myProperty = me.GetPropertyInfoNonPublic(propertyName);
            if (ignoreNoSetMethod)
            {
                if (myProperty.GetSetMethod() != null) myProperty.SetValue(me, newValue, index);
            }
            else
            {
                myProperty.SetValue(me, newValue, index);
            }
        }

        //public static object StaticPropertyGet(this Type myType, string propertyName, object[] index = null)
        //{
        //    return myType.GetStaticPropertyInfoWithType(propertyName).GetValue(null, index);
        //}
        //public static void StaticPropertySet(this Type myType, string propertyName, object newValue, bool ignoreNoSetMethod = false, object[] index = null)
        //{
        //    var myProperty = myType.GetStaticPropertyInfoWithType(propertyName);
        //    var setMethod = myProperty.GetSetMethod(true);
        //    if (setMethod != null)
        //    {
        //        setMethod.Invoke(null, new[] { newValue });
        //    }
        //    else
        //    {
        //        if (!ignoreNoSetMethod) myProperty.SetValue(null, newValue, index);
        //    }
        //}

        #endregion

        #region Private Helpers
        private static PropertyInfo GetPropertyInfoNonPublic(this object me, string propertyName)
        {
            return me.GetType().GetPropertyInfoNonPublicWithType(propertyName);
        }
        //private static PropertyInfo GetStaticPropertyInfoNonPublic(this object me, string propertyName)
        //{
        //    return me.GetType().GetStaticPropertyInfoNonPublicWithType(propertyName);
        //}
        private static PropertyInfo GetPropertyInfoNonPublicWithType(this Type myType, string propertyName)
        {
            try
            {
                return myType.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance).Single(p => p.Name == propertyName);
            }
            catch (Exception)
            {
                throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
            }
        }
        //private static PropertyInfo GetStaticPropertyInfoNonPublicWithType(this Type myType, string propertyName)
        //{
        //    try
        //    {
        //        return myType.GetProperties(BindingFlags.NonPublic | BindingFlags.Static).Single(p => p.Name == propertyName);
        //    }
        //    catch (Exception)
        //    {
        //        throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
        //    }
        //}


        private static PropertyInfo GetPropertyInfo(this object me, string propertyName)
        {
            return me.GetType().GetPropertyInfoWithType(propertyName);
        }
        //private static PropertyInfo GetStaticPropertyInfo(this object me, string propertyName)
        //{
        //    return me.GetType().GetStaticPropertyInfoWithType(propertyName);
        //}
        private static PropertyInfo GetPropertyInfoWithType(this Type myType, string propertyName)
        {
            try
            {
                return myType.GetProperty(propertyName);
            }
            catch(Exception)
            {
                throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
            }
        }
        //private static PropertyInfo GetStaticPropertyInfoWithType(this Type myType, string propertyName)
        //{
        //    try
        //    {
        //        //return myType.GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy).Single(p => p.Name == propertyName);
        //        var x = myType.GetProperties(BindingFlags.Public | BindingFlags.Static);
        //        return myType.GetProperties().Single(p => p.Name == propertyName);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
        //    }
        //}
        #endregion
    }
}
