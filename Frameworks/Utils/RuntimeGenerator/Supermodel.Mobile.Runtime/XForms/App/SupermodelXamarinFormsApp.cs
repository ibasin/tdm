﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.App
{
    using Xamarin.Forms;
    using Pages.Login;
    using UnitOfWork;
    using DataContext.Core;

    public abstract class SupermodelXamarinFormsApp : Application
    {
        protected SupermodelXamarinFormsApp()
        {
            FormsApplication.SetRunningApp(this); 
        }

        public IAuthHeaderGenerator AuthHeaderGenerator { get; set; }
        public virtual UnitOfWork<DataContextT> NewUnitOfWork<DataContextT>(ReadOnly readOnly = ReadOnly.No) where DataContextT : class, IDataContext, new()
        {
            var unitOfWork = new UnitOfWork<DataContextT>(readOnly);
            if (unitOfWork.Context is IWebApiAuthorizationContext)
            {
                if (AuthHeaderGenerator != null) UnitOfWorkContext.AuthHeader = AuthHeaderGenerator.CreateAuthHeader();
            }
            return unitOfWork;
        }

        public abstract void HandleUnauthorized();
        public abstract byte[] LocalStorageEncryptionKey { get; }
    }
}
