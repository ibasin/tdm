﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.App
{
    using Xamarin.Forms;

    // ReSharper disable UnusedAutoPropertyAccessor.Local
    #if __IOS__
	using Xamarin.Forms.Platform.iOS;
    using UIKit;
    using Foundation;
			    
	public abstract class FormsApplication<AppT> : FormsApplication where AppT : SupermodelXamarinFormsApp, new()
	{
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            AppDelegate = this;
            Forms.Init();
            LoadApplication(new AppT());
            return base.FinishedLaunching(app, options);
        }        
        
        public static AppT RunningApp => (AppT)_runningApp;
	}
    public abstract class FormsApplication : FormsApplicationDelegate
    {
        public static FormsApplicationDelegate AppDelegate { get; protected set; }
        public static void SetRunningApp(SupermodelXamarinFormsApp runningApp) { _runningApp = runningApp; }
        public static SupermodelXamarinFormsApp GetRunningApp() { return _runningApp; }

        // ReSharper disable once InconsistentNaming
        protected static SupermodelXamarinFormsApp _runningApp;
    }
    #elif __ANDROID__
    using Xamarin.Forms.Platform.Android;
    using Android.OS;

    public abstract class FormsApplication<AppT> : FormsApplication where AppT : SupermodelXamarinFormsApp, new()
    {
        protected override void OnCreate(Bundle bundle)
        {
            MainActivity = this;
            base.OnCreate(bundle);
            Forms.Init(this, bundle);
            LoadApplication(new AppT());
        }        
        public static AppT RunningApp => (AppT)_runningApp;
    }
    public abstract class FormsApplication : FormsApplicationActivity
    {
        public static FormsApplicationActivity MainActivity { get; protected set; }
        public static void SetRunningApp(SupermodelXamarinFormsApp runningApp) { _runningApp = runningApp; }
        public static SupermodelXamarinFormsApp GetRunningApp() { return _runningApp; }
        
        // ReSharper disable once InconsistentNaming
        protected static SupermodelXamarinFormsApp _runningApp;
    }
    #else
    public abstract class FormsApplication<AppT> : FormsApplication where AppT : SupermodelXamarinFormsApp, new()
    {
        public static AppT RunningApp => (AppT)_runningApp;
    }
    public abstract class FormsApplication
    {
        public static void SetRunningApp(SupermodelXamarinFormsApp runningApp) { _runningApp = runningApp; }
        public static SupermodelXamarinFormsApp GetRunningApp() { return _runningApp; }
        
        // ReSharper disable once InconsistentNaming
        protected static SupermodelXamarinFormsApp _runningApp;
    }
    #endif
    // ReSharper restore UnusedAutoPropertyAccessor.Local
}
