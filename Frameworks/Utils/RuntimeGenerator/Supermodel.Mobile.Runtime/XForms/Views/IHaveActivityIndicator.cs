﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Views
{
    using System.Threading.Tasks;

    public interface IHaveActivityIndicator
    {
        Task WaitForPageToBecomeActiveAsync();
        bool ActivityIndicatorOn { get; set; }
        string Message { get; set; }
    }
}