﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using System;
    using ReflectionMapper;
    using Xamarin.Forms;
    using Exceptions;
    using Base;
    using Pages.CRUDDetail;

    public class ToggleSwitchXFModel : SingleCellWritableUIComponentXFModel
    {
        #region Constructors
		public ToggleSwitchXFModel()
		{
			Switch = new Switch { HorizontalOptions = LayoutOptions.EndAndExpand, VerticalOptions = LayoutOptions.Center, OnColor = XFormsSettings.SwitchOnColor };
			Switch.SetBinding(Switch.IsToggledProperty, "IsToggled");
		    Switch.PropertyChanged += (sender, args) =>
		    {
		        if (_currentValue != Switch.IsToggled)
		        {
		            _currentValue = Switch.IsToggled;
		            OnChanged?.Invoke((IBasicCRUDDetailPage)ParentPage);
		        }
		    };
            StackLayoutView.Children.Add(Switch);
            Tapped += (sender, args) => Switch.Focus();
		}
        #endregion

        #region ICustomMapper implemtation
        public override object MapFromObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(bool) && objType != typeof(bool?))
            {
                throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
            }

            if (obj is bool) IsToggled = (bool)obj;
            else IsToggled = (bool?)obj ?? false;

            return this;
        }
        public override object MapToObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(bool) && objType != typeof(bool?))
            {
                throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
            }
            
            return IsToggled;
        }
        #endregion

        #region EventHandling
        public Action<IBasicCRUDDetailPage> OnChanged { get; set; }
        #endregion

        #region Properties
        public bool IsToggled
        {
            get => Switch.IsToggled;
            set
            {
                _currentValue = value;
                if (value == Switch.IsToggled) return;
                Switch.IsToggled = value;
                OnPropertyChanged();
            }
        }
        private bool _currentValue;
        public Switch Switch { get; }

        public override object WrappedValue => Switch.IsToggled;

        public override TextAlignment TextAlignmentIfApplies
        {
            get
            {
                switch(Switch.HorizontalOptions.Alignment)
                {
                    case LayoutAlignment.Start: return TextAlignment.Start;
                    case LayoutAlignment.Center: return TextAlignment.Center;
                    case LayoutAlignment.End: return TextAlignment.End;
                    default: throw new SupermodelException("Invalid value for Switch.HorizontalOptions.Alignment. This should never happen");
                }
            }
            set
            {
                switch(value)
                {
                    case TextAlignment.Start: 
                        Switch.HorizontalOptions = LayoutOptions.StartAndExpand;
                        break;
                    case TextAlignment.Center: 
                        Switch.HorizontalOptions = LayoutOptions.CenterAndExpand;
                        break;
                    case TextAlignment.End: 
                        Switch.HorizontalOptions = LayoutOptions.EndAndExpand;
                        break;
                    default: throw new SupermodelException("Invalid value for TextAlignmentIfApplies. This should never happen");
                }
            }
        }

        public bool Active
        {
            get => _active;
            set
            {
                if (_active == value) return;
                _active = IsEnabled = value;
                DisplayNameLabel.TextColor = value ? XFormsSettings.LabelTextColor : XFormsSettings.DisabledTextColor;
                Switch.OnColor = value ? XFormsSettings.SwitchOnColor : XFormsSettings.DisabledTextColor;
                RequiredFieldIndicator.TextColor = value ? XFormsSettings.RequiredAsteriskColor : XFormsSettings.DisabledTextColor;
            }
        }
        private bool _active = true;
        #endregion
    }
}
