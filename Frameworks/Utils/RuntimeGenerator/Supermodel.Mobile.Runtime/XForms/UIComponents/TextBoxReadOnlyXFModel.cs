﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using Xamarin.Forms;
    using System;
    using Base;
    using XForms;

    public class TextBoxReadOnlyXFModel : SingleCellReadOnlyUIComponentForTextXFModel
    {
        #region Constructors
        public TextBoxReadOnlyXFModel()
        {
            TextLabel = new Label { HorizontalOptions = LayoutOptions.EndAndExpand, VerticalOptions = LayoutOptions.Center, LineBreakMode = LineBreakMode.TailTruncation, FontSize = XFormsSettings.LabelFontSize, TextColor = XFormsSettings.ValueTextColor };
            StackLayoutView.Children.Add(TextLabel);
        }
        #endregion

        #region ICustomMapper implemtation
        public override object MapToObjectCustom(object obj, Type objType)
        {
            return obj;
        }
        #endregion

        #region Properties
        public override string Text
        {
            get => TextLabel.Text;
            set => TextLabel.Text = value;
        }
        public Label TextLabel { get; }
        public override TextAlignment TextAlignmentIfApplies
        {
            get => TextLabel.HorizontalTextAlignment;
            set => TextLabel.HorizontalTextAlignment = value;
        }
        #endregion
    }
}
