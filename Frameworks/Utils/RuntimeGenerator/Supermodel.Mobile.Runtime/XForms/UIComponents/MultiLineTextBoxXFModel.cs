﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using Xamarin.Forms;
    using Base;
    using XForms;
    using System;
    using Pages.CRUDDetail;

    public class MultiLineTextBoxXFModel : SingleCellWritableUIComponentForTextXFModel
    {
        #region Constructors
        public MultiLineTextBoxXFModel(Keyboard keyboard) : this()
        {
            Editor.Keyboard = keyboard;
        }
        public MultiLineTextBoxXFModel()
		{
            Editor = new Editor
		    {
		        HorizontalOptions = LayoutOptions.FillAndExpand,
		        VerticalOptions = LayoutOptions.FillAndExpand,
		        HeightRequest = XFormsSettings.MultiLineTextBoxCellHeight - XFormsSettings.MultiLineTextLabelHeight,
		        FontSize = XFormsSettings.LabelFontSize,
		        TextColor = XFormsSettings.ValueTextColor
            };
		    Editor.SetBinding(Entry.TextProperty, "Text");
		    Editor.PropertyChanged += (sender, args) =>
		    {
		        if (_currentValue != Editor.Text)
		        {
		            _currentValue = Editor.Text;
		            OnChanged?.Invoke((IBasicCRUDDetailPage)ParentPage);
		        }
		    };
            StackLayoutView.Orientation = StackOrientation.Vertical;
            #pragma warning disable 618
		    StackLayoutView.Padding = Device.OnPlatform(new Thickness(8, 10), new Thickness(8, 0), new Thickness(8, 10));
            #pragma warning restore 618
            StackLayoutView.Children.Add(Editor);
		    SetHeight(XFormsSettings.MultiLineTextBoxCellHeight);
		    StackLayoutView.HeightRequest = XFormsSettings.MultiLineTextBoxCellHeight;
		    Tapped += (sender, args) => Editor.Focus();
		}
        #endregion

        #region EventHandling
        public Action<IBasicCRUDDetailPage> OnChanged { get; set; }
        #endregion

        #region Properties
        public void SetHeight(int newHeight)
        {
            Height = newHeight;
            Editor.HeightRequest = ShowDisplayNameIfApplies ? newHeight - XFormsSettings.MultiLineTextLabelHeight : newHeight - 20;
        }
        public override string Text
        {
            get => Editor.Text;
            set
            {
                _currentValue = value;
                if (value == Editor.Text) return;
                Editor.Text = value;
                OnPropertyChanged();
            }
        }
        private string _currentValue;

        public Editor Editor { get; }
        public override TextAlignment TextAlignmentIfApplies { get; set; }

        public bool Active
        {
            get => _active;
            set
            {
                if (_active == value) return;
                _active = IsEnabled = value;
                DisplayNameLabel.TextColor = value ? XFormsSettings.LabelTextColor : XFormsSettings.DisabledTextColor;
                Editor.TextColor = value ? XFormsSettings.ValueTextColor : XFormsSettings.DisabledTextColor;
                RequiredFieldIndicator.TextColor = value ? XFormsSettings.RequiredAsteriskColor : XFormsSettings.DisabledTextColor;
            }
        }
        private bool _active = true;
        #endregion
    }
}
