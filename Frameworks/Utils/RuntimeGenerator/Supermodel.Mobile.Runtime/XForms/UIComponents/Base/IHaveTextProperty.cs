 // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.Base
{
    public interface IHaveTextProperty
    {
        string Text { get; set; }
    }
}