﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.Base
{
    using ViewModels;
    using Xamarin.Forms;    
    
    public interface  IReadOnlyUIComponentXFModel : ISupermodelMobileDetailTemplate
    {
        bool ShowDisplayNameIfApplies { get; set; }
        string DisplayNameIfApplies { get; set; }
        TextAlignment TextAlignmentIfApplies { get; set; }
    }
}