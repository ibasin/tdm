﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.Base
{
    using System;
    using ReflectionMapper;

    public abstract class SingleCellWritableUIComponentXFModel : SingleCellWritableUIComponentWithoutBackingXFModel, IRMapperCustom
    {
        #region ICustomMapper implemtation
        public abstract object MapFromObjectCustom(object obj, Type objType);
        public abstract object MapToObjectCustom(object obj, Type objType);
        #endregion        
    }
}
