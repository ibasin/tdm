 // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.Base
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using ReflectionMapper;
    
    public abstract class BinaryFilesWritableXFModel : BinaryFilesReadOnlyXFModel, IWritableUIComponentXFModel
    {
        #region Custom Mapper implementation
        public override object MapToObjectCustom(object obj, Type objType)
        {
            var modelsWithImage = (IEnumerable<IModelWithBinaryFile>)obj;

            // modelsWithImages == null during validation
            if (modelsWithImage != null)
            {
                //remove all
                modelsWithImage.ExecuteMethod("Clear");

                //add all the images for which an exact match was not found
                foreach (var modelWithBinaryFileXFModel in ModelsWithBinaryFileXFModels)
                {
                    var modelsWithImageUnderlyingType = obj.GetType().GenericTypeArguments[0];
                    var modelWithImage = (IModelWithBinaryFile)ReflectionHelper.CreateType(modelsWithImageUnderlyingType);
                    modelsWithImage.ExecuteMethod("Add", modelWithImage);

                    //update BinaryFile
                    modelWithBinaryFileXFModel.MapTo(modelWithImage);
                }
            }
            return obj;
        }
        #endregion

        #region Properties
        public abstract string ErrorMessage { get; set; }
        public abstract bool Required { get; set; }
        public object WrappedValue => ModelsWithBinaryFileXFModels.FirstOrDefault();
        #endregion
    }
}