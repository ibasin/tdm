﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.Base
{
    using Models;

    public interface IModelWithBinaryFile
    {
        long Id { get; set; }
        string GetTitle();
        void SetTitle(string value);
        BinaryFile GetBinaryFile();
        void SetBinaryFile(BinaryFile value);
    }
}