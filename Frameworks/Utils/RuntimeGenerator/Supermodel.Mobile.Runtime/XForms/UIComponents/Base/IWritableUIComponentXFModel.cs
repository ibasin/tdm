﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.Base
{
    using Xamarin.Forms;

    public interface IWritableUIComponentXFModel : IReadOnlyUIComponentXFModel
    {
        string ErrorMessage { get; set; }
        bool Required { get; set; }
        object WrappedValue { get; }
    }
}
