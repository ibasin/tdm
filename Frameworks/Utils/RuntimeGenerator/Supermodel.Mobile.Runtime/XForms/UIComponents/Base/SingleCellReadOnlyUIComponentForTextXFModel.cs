// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.Base
{
    using System;

    public abstract class SingleCellReadOnlyUIComponentForTextXFModel : SingleCellReadOnlyUIComponentXFModel, IHaveTextProperty
    {
        #region ICustomMapper implemtation
        public override object MapFromObjectCustom(object obj, Type objType)
        {
            return SingleCellUIComponentForTextXFModelCommonLib.MapFromObjectCustom(this, obj, objType);
        }
        public override object MapToObjectCustom(object obj, Type objType)
        {
            return SingleCellUIComponentForTextXFModelCommonLib.MapToObjectCustom(this, obj, objType);
        }
        #endregion
        
        #region Properties
        public abstract string Text { get; set; }
        #endregion
    }
}