// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.Base
{
    using System;
    using ReflectionMapper;    
    
    public static class SingleCellUIComponentForTextXFModelCommonLib
    {
        #region ICustomMapper Common Implemtation for Text
        public static object MapFromObjectCustom(IHaveTextProperty me, object obj, Type objType)
        {
            if (objType != typeof(string) && objType != typeof(int)  && objType != typeof(long)  && objType != typeof(double)  && objType != typeof(float)
                && objType != typeof(int?) && objType != typeof(long?) && objType != typeof(double?) && objType != typeof(float?))
            {
                throw new PropertyCantBeAutomappedException($"{me.GetType().Name} can't be automapped to {objType.Name}");
            }

            var domainObjStr = obj?.ToString();
            if (domainObjStr != null) me.Text = domainObjStr;

            return me;
        }
        public static object MapToObjectCustom(IHaveTextProperty me, object obj, Type objType)
        {
            //If both Text and domain property are blank, return the current state
            var domainObjStr = obj?.ToString();
            if (string.IsNullOrEmpty(me.Text) && string.IsNullOrEmpty(domainObjStr)) return obj;
            
            if (objType == typeof(string)) return me.Text;

            try
            {
                if (objType == typeof(int)) return int.Parse(me.Text);
                if (objType == typeof(int?)) return string.IsNullOrEmpty(me.Text) ? (int?)null : int.Parse(me.Text);

                if (objType == typeof(long)) return long.Parse(me.Text);
                if (objType == typeof(long?)) return string.IsNullOrEmpty(me.Text) ? (long?)null : long.Parse(me.Text);
            
                if (objType == typeof(double)) return double.Parse(me.Text);
                if (objType == typeof(double?)) return string.IsNullOrEmpty(me.Text) ? (double?)null : double.Parse(me.Text);

                if (objType == typeof(float)) return float.Parse(me.Text);
                if (objType == typeof(float?)) return string.IsNullOrEmpty(me.Text) ? (float?)null : float.Parse(me.Text);
            }
            catch (FormatException)
            {
                throw new ValidationResultException("Invalid Format");
            }
            catch(OverflowException)
            {
                throw new ValidationResultException("Invalid Format");
            }

            throw new PropertyCantBeAutomappedException($"{me.GetType().Name} can't be automapped to {objType.Name}");
        }
        #endregion
    }
}