﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using System;
    using Base;
    using Xamarin.Forms;    
    using ReflectionMapper;
    using XForms;

    public class DateReadOnlyXFModel : SingleCellReadOnlyUIComponentForTextXFModel
    {
        #region Constructors
        public DateReadOnlyXFModel()
        {
            TextLabel = new Label { HorizontalOptions = LayoutOptions.EndAndExpand, VerticalOptions = LayoutOptions.Center, LineBreakMode = LineBreakMode.TailTruncation, FontSize = XFormsSettings.LabelFontSize, TextColor = XFormsSettings.ValueTextColor };
            StackLayoutView.Children.Add(TextLabel);
        }
        #endregion

        #region ICstomMapper implementations
        public override object MapFromObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
            
            if (obj == null) Value = null;
            else Value = (DateTime)obj; 

            return this;
        }
        // ReSharper disable once RedundantAssignment
        public override object MapToObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
            if (objType == typeof(DateTime) && Value == null) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1} because {0} is null but {1} is not nullable", GetType().Name, objType.Name));
            obj = Value; //This assignment does not do anything but we still do it for consistency
            return obj;
        }
        #endregion

        #region Properties
        public DateTime? Value
        {
            get
            {
                if (DateTime.TryParse(Text, out var result)) return result;
                else return null;
            }
            set => Text = value == null ? "" : value.Value.ToString("d");
        }
        public override string Text
        {
            get => TextLabel.Text;
            set => TextLabel.Text = value;
        }
        public Label TextLabel { get; }
        public override TextAlignment TextAlignmentIfApplies
        {
            get => TextLabel.HorizontalTextAlignment;
            set => TextLabel.HorizontalTextAlignment = value;
        }
        #endregion
    }
}
