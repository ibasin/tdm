﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using CustomControls;
    using System;
    using Base;
    using Xamarin.Forms;    
    using ReflectionMapper;
    using XForms;
    using Pages.CRUDDetail;

    public class DateXFModel : SingleCellWritableUIComponentWithoutBackingXFModel, IRMapperCustom
    {
        #region Constructors
		public DateXFModel()
		{
			DatePicker = new ExtDatePicker{ HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, Border = false, TextAlignment = TextAlignment.End, FontSize = XFormsSettings.LabelFontSize, TextColor = XFormsSettings.ValueTextColor };
		    DatePicker.PropertyChanged += (sender, args) =>
		    {
		        if (_currentValue != DatePicker.Date)
		        {
		            _currentValue = DatePicker.Date;
		            OnChanged?.Invoke((IBasicCRUDDetailPage)ParentPage);
		        }
		    };

            StackLayoutView.Children.Add(DatePicker);
            Tapped += (sender, args) => DatePicker.Focus();
		}
        #endregion

        #region ICstomMapper implementations
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
            
            if (obj == null) Value = null;
            else Value = (DateTime)obj; 

            return this;
        }
        // ReSharper disable once RedundantAssignment
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
            if (objType == typeof(DateTime) && Value == null) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1} because {0} is null but {1} is not nullable", GetType().Name, objType.Name));
            obj = Value; //This assignment does not do anything but we still do it for consistency
            return obj;
        }
        #endregion

        #region EventHandling
        public Action<IBasicCRUDDetailPage> OnChanged { get; set; }
        #endregion

        #region Properties
        public DateTime? Value
        {
            get => DatePicker.Date;
            set
            {
                _currentValue = value;
                if (value == null) DatePicker.Date = DateTime.Today;
                else DatePicker.Date = value.Value;
            }
        }
        private DateTime? _currentValue;

        public ExtDatePicker DatePicker { get; }

        public override object WrappedValue => Value;

        public override TextAlignment TextAlignmentIfApplies
        {
            get => DatePicker.TextAlignment;
            set => DatePicker.TextAlignment = value;
        }

        public bool Active
        {
            get => _active;
            set
            {
                if (_active == value) return;
                _active = IsEnabled = value;
                DisplayNameLabel.TextColor = value ? XFormsSettings.LabelTextColor : XFormsSettings.DisabledTextColor;
                DatePicker.TextColor = value ? XFormsSettings.ValueTextColor : XFormsSettings.DisabledTextColor;
                RequiredFieldIndicator.TextColor = value ? XFormsSettings.RequiredAsteriskColor : XFormsSettings.DisabledTextColor;
            }
        }
        private bool _active = true;
        #endregion
    }
}
