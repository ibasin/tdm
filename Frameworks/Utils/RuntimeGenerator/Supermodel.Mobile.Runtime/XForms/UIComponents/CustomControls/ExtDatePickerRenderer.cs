﻿#if __IOS__
// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
{
    using Xamarin.Forms.Platform.iOS;
    using Xamarin.Forms;
    using UIKit;
    
    public class ExtDatePickerRenderer : DatePickerRenderer
    {
        protected void UpdateControl()
        {
            if (Control != null && Element != null)
            {
                Control.BorderStyle = UITextBorderStyle.None;
                var element = (ExtDatePicker)Element;
                switch (element.TextAlignment)
                {
                    case TextAlignment.Start: Control.TextAlignment = UITextAlignment.Left; break;
                    case TextAlignment.End: Control.TextAlignment = UITextAlignment.Right; break;
                    case TextAlignment.Center: Control.TextAlignment = UITextAlignment.Center; break;
                }
            }
        }
        
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
        {
            base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
            UpdateControl();
        }

        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);
            UpdateControl();
        }
    }
}
#endif

#if __ANDROID__
// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
{
    using Xamarin.Forms.Platform.Android;
    using Xamarin.Forms;
    using Android.Views;
    
    public class ExtDatePickerRenderer : DatePickerRenderer
    {
        protected void UpdateControl()
        {
            if (Control != null && Element != null)
            {
                //Android elements are already borderless by default
                var element = (ExtDatePicker)Element;
                switch (element.TextAlignment)
                {
                    case Xamarin.Forms.TextAlignment.Start: Control.Gravity = GravityFlags.Left; break;
                    case Xamarin.Forms.TextAlignment.End: Control.Gravity = GravityFlags.Right; break;
                    case Xamarin.Forms.TextAlignment.Center: Control.Gravity = GravityFlags.Center; break;
                }
            }
        }
        
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
        {
            base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
            UpdateControl();
        }

        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);
            UpdateControl();
        }
    }
}
#endif

#if WINDOWS_UWP
// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
{
    using Xamarin.Forms.Platform.UWP;
    using Windows.UI.Xaml;
    using Xamarin.Forms;

    public class ExtDatePickerRenderer : DatePickerRenderer
    {
        protected void UpdateControl()
        {
            if (Control != null && Element != null)
            {
                //Android elements are already borderless by default
                var element = (ExtDatePicker)Element;
                //switch (element.TextAlignment)
                //{
                //    case Xamarin.Forms.TextAlignment.Start: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Left; break;
                //    case Xamarin.Forms.TextAlignment.End: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Right; break;
                //    case Xamarin.Forms.TextAlignment.Center: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Center; break;
                //}
                Control.BorderThickness = new Windows.UI.Xaml.Thickness(element.Border ? 1 : 0);
            }
        }
        
        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
        {
            base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
            UpdateControl();
        }

        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
        {
            base.OnElementChanged(e);
            UpdateControl();
        }
    }
}
#endif



