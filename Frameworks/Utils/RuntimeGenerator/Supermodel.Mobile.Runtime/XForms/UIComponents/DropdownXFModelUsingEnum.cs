﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.UIComponents
{
    using Utils;
    using System.Linq;
    using System;
    using ReflectionMapper;
    using System.Collections.Generic;
    using System.Globalization;    
    
    public class DropdownXFModelUsingEnum<EnumT> : DropdownXFModel, IRMapperCustom where EnumT : struct, IConvertible
    {
        #region Constructors
        public DropdownXFModelUsingEnum()
        {
            var enumValues = new List<object>();
            foreach (var item in Enum.GetValues(typeof(EnumT))) enumValues.Add(item);
            enumValues = enumValues.OrderBy(x => x.GetScreenOrder()).ToList();

            foreach (var option in enumValues) Options.Add(new EnumOption((EnumT)option));
            SelectedValue = null;
        }
        public DropdownXFModelUsingEnum(EnumT selectedEnum) : this()
        {
            SelectedEnum = selectedEnum;
        }
        #endregion

        #region Nested Options class
        public class EnumOption : Option
        {
            public EnumOption(EnumT value, string label, bool isDisabled) : base(value.ToString(CultureInfo.InvariantCulture), label, isDisabled) { }
            public EnumOption(EnumT value) : this(value, value.GetDescription(), value.IsDisabled()) { }
            public EnumT EnumValue => (EnumT)Enum.Parse(typeof(EnumT), Value);
        }
        #endregion

        #region ICstomMapper implementations
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(EnumT) && objType != typeof(EnumT?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
            SelectedEnum = (EnumT?)obj;
            return this;
        }
        // ReSharper disable once RedundantAssignment
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(EnumT) && objType != typeof(EnumT?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
            if (objType == typeof(EnumT) && SelectedEnum == null) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1} because {0} is null but {1} is not nullable", GetType().Name, objType.Name));
            obj = SelectedEnum; //This assignment does not do anyhting but we still do it for consistency
            return obj;
        }
        #endregion

        #region Properties
        public EnumT? SelectedEnum
        {
            get
            {
                if (string.IsNullOrEmpty(SelectedValue)) return null;
                return (EnumT)Enum.Parse(typeof(EnumT), SelectedValue);
            }
            set => SelectedValue = value == null ? "" : ((EnumT)value).ToString(CultureInfo.InvariantCulture);
        }
        #endregion
    }
}
