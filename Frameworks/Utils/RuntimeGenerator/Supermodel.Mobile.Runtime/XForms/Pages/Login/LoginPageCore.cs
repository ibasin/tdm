// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.Login
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using DataContext.WebApi;
    using Exceptions;
    using Views;
    using Xamarin.Forms;   
    using App;

    public abstract class LoginPageCore<LoginViewModelT, LoginViewT> : ContentPage, IHaveActivityIndicator
        where LoginViewModelT: ILoginViewModel, new()
        where LoginViewT : LoginViewBase<LoginViewModelT>, new()
    {
        #region Contructors
        protected LoginPageCore()
        {
            Content = LoginView = new ViewWithActivityIndicator<LoginViewT>(new LoginViewT());
            AutologinIfConnectionLost = false;
            LoginView.ContentView.SetUpSignInClickedHandler(SignInClicked);
        }
        #endregion

        #region Methods
        public abstract Task<LoginResult> TryLoginAsync();
        public abstract IAuthHeaderGenerator GetAuthHeaderGenerator(LoginViewModelT loginViewModel);
        public IAuthHeaderGenerator GetBlankAuthHeaderGenerator()
        {
            var authHeader = GetAuthHeaderGenerator(new LoginViewModelT());
            authHeader.Clear();
            return authHeader;
        }
        public async Task AutologinIfPossibleAsync()
        {
            var authHeaderGenerator = GetBlankAuthHeaderGenerator();
            if (authHeaderGenerator.LoadFromAppProperties())
            {
                bool connectionLost;
                do
                {
                    connectionLost = false;
                    try
                    {
                        using(var activityIndicator = new ActivityIndicatorFor(LoginView, "Logging you in..."))
                        {
                            FormsApplication.GetRunningApp().AuthHeaderGenerator = authHeaderGenerator;
                            var loginResult = await TryLoginAsync();
                            if (loginResult.LoginSuccessful)
                            {
                                if (!string.IsNullOrEmpty(loginResult.UserLabel)) activityIndicator.Element.Message = "Welcome, " + loginResult.UserLabel + "!";

                                FormsApplication.GetRunningApp().AuthHeaderGenerator.UserId = loginResult.UserId; 
                                FormsApplication.GetRunningApp().AuthHeaderGenerator.UserLabel = loginResult.UserLabel;

                                await Task.Delay(800); //short delay so that the message can be read
                                if (await DoLoginAsync(true, false)) await FormsApplication.GetRunningApp().AuthHeaderGenerator.SaveToAppPropertiesAsync();
                            }
                            else
                            {
                                await authHeaderGenerator.ClearAndSaveToPropertiesAsync();
                                FormsApplication.GetRunningApp().AuthHeaderGenerator = null;
                            }
                        }
                    }
                    catch (SupermodelWebApiException ex1)
                    {
                        connectionLost = true;
                        var result = await DisplayAlert("Server Error", ex1.ContentJsonMessage, "Cancel", "Try again");
                        if (result) return;
                    }
                    catch (WebException)
                    {
                        if (AutologinIfConnectionLost)
                        {
                            await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Work Offline");
                            using (new ActivityIndicatorFor(LoginView))
                            {
                                await DoLoginAsync(true, false);
                            }
                            return;
                        }
                        
                        var result = await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Cancel", "Try again");
                        if (result) return;
                    }
                    catch (Exception ex3)
                    {
                        connectionLost = true;
                        var result = await DisplayAlert("Unexpected Error", ex3.Message, "Cancel", "Try again");
                        if (result) return;
                    }
                }
                while(connectionLost);            
            }
        }
        #endregion

        #region Overrides
        protected virtual void OnDisappearingBase()
        {
            // ReSharper disable once RedundantBaseQualifier
            base.OnDisappearing();
        }
        protected virtual void OnAppearingBase()
        {
            // ReSharper disable once RedundantBaseQualifier
            base.OnAppearing();
        }
        protected override async void OnAppearing()
        {
            PageActive = true;

            if (_trueTitle != null)
            {
                Title = _trueTitle;
                _trueTitle = null;
            }
            
            //If Sign Out is clicked
            if (_loggedIn)
            {
                var answer = await DisplayAlert("Alert", "Are you sure you want to sign out?", "Yes", "No");

                if (answer)
                {
                    if (!await OnConfirmedLogOutAsync()) await DoLoginAsync(true, true);
                }
                else
                {
                    await DoLoginAsync(true, true);
                }
            }
            OnAppearingBase();
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            PageActive = false;
        }
        protected virtual async Task<bool> OnConfirmedLogOutAsync()
        {
            await FormsApplication.GetRunningApp().AuthHeaderGenerator.ClearAndSaveToPropertiesAsync();
            FormsApplication.GetRunningApp().AuthHeaderGenerator = null;
            _loggedIn = false;
            return true;
        }
        protected virtual async Task<bool> DoLoginAsync(bool autologin, bool isJumpBack)
        {
            _trueTitle = Title;
            Title = "Sign Out";

            var result = await OnSuccessfulLoginAsync(autologin, isJumpBack);

            if (result) _loggedIn = true;
            else Title = _trueTitle;

            return result;
        }
        public abstract Task<bool> OnSuccessfulLoginAsync(bool autologin, bool isJumpBack);
        #endregion

        #region IHaveActivityIndicator implementation
        public async Task WaitForPageToBecomeActiveAsync()
        {
            while(!PageActive) await Task.Delay(25);
        }
        public bool ActivityIndicatorOn
        {
            get => LoginView.ActivityIndicatorOn;
            set => LoginView.ActivityIndicatorOn = value;
        }
        public string Message
        {
            get { return LoginView.Message; }
            set { LoginView.Message = value; }
        }
        #endregion

        #region Event Handlers
        public virtual async void SignInClicked(object sender, EventArgs args)
        {
            var loginViewModel = LoginView.ContentView.ViewModel;
            var validationError = loginViewModel.GetValidationError();
            if (validationError != null)
            {
                FormsApplication.GetRunningApp().AuthHeaderGenerator = null;
                await DisplayAlert("Login", validationError, "Ok");
                return;
            }

            bool connectionLost;
            do
            {
                connectionLost = false;
                try
                {
                    using(var activityIndicator = new ActivityIndicatorFor(LoginView, "Logging you in..."))
                    {
                        FormsApplication.GetRunningApp().AuthHeaderGenerator = GetAuthHeaderGenerator(loginViewModel);
                        var loginResult = await TryLoginAsync();
                        if (loginResult.LoginSuccessful)
                        {
                            if (!string.IsNullOrEmpty(loginResult.UserLabel)) activityIndicator.Element.Message = "Welcome, " + loginResult.UserLabel + "!";

                            FormsApplication.GetRunningApp().AuthHeaderGenerator.UserId = loginResult.UserId; 
                            FormsApplication.GetRunningApp().AuthHeaderGenerator.UserLabel = loginResult.UserLabel;

                            await Task.Delay(800); //short delay so that the message can be read
                            if (await DoLoginAsync(false, false)) await FormsApplication.GetRunningApp().AuthHeaderGenerator.SaveToAppPropertiesAsync();
                        }
                        else
                        {
                            FormsApplication.GetRunningApp().AuthHeaderGenerator = null;
                            await DisplayAlert("Unable to sign in", "Username and password combination provided is invalid. Please try again.", "Ok");
                        }
                    }
                }
                catch (SupermodelWebApiException ex1)
                {
                    connectionLost = true;
                    var result = await DisplayAlert("Server Error", ex1.ContentJsonMessage, "Cancel", "Try again");
                    if (result) return;
                }
                catch (WebException)
                {
                    connectionLost = true;
                    var result = await DisplayAlert("Server Error", "Connection to the cloud cannot be established.", "Cancel", "Try again");
                    if (result) return;
                }
                catch (Exception ex3)
                {
                    connectionLost = true;
                    var result = await DisplayAlert("Unexpected Error", ex3.Message, "Cancel", "Try again");
                    if (result) return;
                }
            }
            while(connectionLost);
        }
        #endregion

        #region Properties
        public ViewWithActivityIndicator<LoginViewT> LoginView { get; set; }
        public bool AutologinIfConnectionLost { get; set; }
        
        // ReSharper disable InconsistentNaming
        protected bool _loggedIn;
        protected string _trueTitle = "Sign In";
        // ReSharper restore InconsistentNaming

        protected bool PageActive { get; set; }
        #endregion        
    }
}