﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.Login
{
    using System.Threading.Tasks;
    using Encryptor;    
    
    public interface IAuthHeaderGenerator
    {
        long? UserId { get; set; }
        string UserLabel { get; set; }
        AuthHeader CreateAuthHeader();

        void Clear();
        Task ClearAndSaveToPropertiesAsync();
        bool LoadFromAppProperties();
        Task SaveToAppPropertiesAsync();
    }
}