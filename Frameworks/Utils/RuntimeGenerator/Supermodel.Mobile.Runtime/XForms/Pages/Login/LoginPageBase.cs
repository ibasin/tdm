﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.Login
{
    using System.Threading.Tasks;
    using DataContext.WebApi; 
    using Models;
    using UnitOfWork;
    using App;

    public abstract class LoginPageBase<LoginViewModelT, LoginViewT, LoginValidationModelT, WebApiDataContextT> : LoginPageCore<LoginViewModelT, LoginViewT>
        where LoginViewModelT: ILoginViewModel, new()
        where LoginViewT : LoginViewBase<LoginViewModelT>, new()
        where LoginValidationModelT : class, IModel
        where WebApiDataContextT : WebApiDataContext, new()
    {
        #region Overrides
        public override async Task<LoginResult> TryLoginAsync()
        {
            using(FormsApplication.GetRunningApp().NewUnitOfWork<WebApiDataContextT>(ReadOnly.Yes))
            {
                return await UnitOfWorkContext.ValidateLoginAsync<LoginValidationModelT>();
            }
        }
        #endregion
    }
}
