﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.Login
{
    using DataContext.WebApi;
    using Models;    
    
    public abstract class UsernameAndPasswordLoginPage<LoginValidationModelT, WebApiDataContextT> : LoginPageBase<UsernameAndPasswordLoginViewModel, UsernameAndPasswordLoginView, LoginValidationModelT, WebApiDataContextT>
        where LoginValidationModelT : class, IModel
        where WebApiDataContextT : WebApiDataContext, new()
    {}
}
