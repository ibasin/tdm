﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.Login
{
    using System;
    using Xamarin.Forms;
    
    public abstract class LoginViewBase<LoginViewModelT> : StackLayout where LoginViewModelT: ILoginViewModel, new()
    {
        #region Methods
        public abstract void SetUpSignInClickedHandler(EventHandler handler);
        #endregion

        #region Properties
        public LoginViewModelT ViewModel { get; set; }
        #endregion
    }
}
