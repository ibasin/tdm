﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.Login
{
    using System.ComponentModel;

    public interface ILoginViewModel : INotifyPropertyChanged
    {
        string GetValidationError();
    }
}
