﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDList
{
    using System.Linq;
    using System;
    using System.Collections.ObjectModel;
    using System.Net;
    using System.Threading.Tasks;
    using Exceptions;
    using Xamarin.Forms;
    using ViewModels;
    using Views;
    using System.Collections.Generic;
    using Models;
    using DataContext.Core;
    using App;
    using UnitOfWork;
    using Repository;

    public abstract class CRUDListPage<ModelT, DataContextT> : CRUDListPageCore<ModelT> 
        where ModelT : class, ISupermodelListTemplate, IModel, new()
        where DataContextT : class, IDataContext, new()
    {
        #region Event Handlers
        public override async void ItemAppearingHandler(object sender, ItemVisibilityEventArgs args)
        {
            if (LoadedAll || LoadingInProgress) return;
            if (Models.Last() == args.Item) await LoadListContentAsync(Models.Count, Take);
        }
        public virtual async void RefreshingHandler(object sender, EventArgs args)
        {
            Models = null;
            await LoadListContentAsync(showActivityIndicator: false);
            ListView.ListPanel.ContentView.IsRefreshing = false;
        }
        #endregion

        #region Overrides
        protected override void InitContent(bool readOnly)
        {
            Content = StackLayout = new StackLayout();
            if (readOnly) ListView = new CRUDListView<ModelT>(null, false);
            else Content = ListView = new CRUDListView<ModelT>(DeleteItemHandler, false);
            StackLayout.Children.Add(ListView);

            ListView.ListPanel.ContentView.Refreshing += RefreshingHandler;
        }
        protected override async Task<bool> DeleteItemInternalAsync(ModelT model)
        {
            using (FormsApplication.GetRunningApp().NewUnitOfWork<DataContextT>())
            {
                model.Delete();
                await UnitOfWorkContext.FinalSaveChangesAsync();
                return true;
            }
        }
        protected virtual async Task<List<ModelT>> GetItemsInternalAsync(int skip, int? take)
        {
            using (FormsApplication.GetRunningApp().NewUnitOfWork<DataContextT>(ReadOnly.Yes))
            {
                var repo = RepoFactory.Create<ModelT>();
                return await repo.GetAllAsync(skip, take);
            }
        }
        #endregion

        #region Methods
        public virtual async Task LoadListContentAsync(int skip = 0, int? take = -1, bool showActivityIndicator = true)
        {
            var navStackCurrentPage = Application.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
            var modalStackCurrentPage = Application.Current.MainPage.Navigation.ModalStack.LastOrDefault();

            if (this != navStackCurrentPage && this != modalStackCurrentPage) throw new Exception("LoadListContentAsync() can only be called when the Page is active");

            if (take < 0) take = Take;
            bool connectionLost;
            do
            {
                connectionLost = false;
                try
                {
                    LoadingInProgress = true;
                    LoadedAll = false;
                    
                    if (Models == null) Models = new ObservableCollection<ModelT>();
                    if (skip == 0) Models.Clear();
                    
                    if (showActivityIndicator)
                    {
                        using(new ActivityIndicatorFor(ListView.ListPanel))
                        {
                            var models = await GetItemsInternalAsync(skip, take);
                            if (take == null || models.Count < take) LoadedAll = true;

                            foreach (var model in models)
                            {
                                if (Models.All(x => x.Id != model.Id)) Models.Add(model);
                            }
                            ListView.ListPanel.ContentView.ItemsSource = Models;
                        }
                    }
                    else
                    {
                        var models = await GetItemsInternalAsync(skip, take);
                        if (take == null || models.Count < take) LoadedAll = true;

                        foreach (var model in models)
                        {
                            if (Models.All(x => x.Id != model.Id)) Models.Add(model);
                        }
                        ListView.ListPanel.ContentView.ItemsSource = Models;
                    }
                }
                catch (SupermodelWebApiException ex1)
                {
                    if (ex1.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        UnauthorizedHandler();
                    }
                    else if (ex1.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        connectionLost = true;
                        await DisplayAlert("Internal Server Error", ex1.ContentJsonMessage, "Ok");
                    }
                    else
                    {
                        connectionLost = true;
                        await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
                    }
                }
                catch (WebException)
                {
                    connectionLost = true;
                    await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
                }
                catch (Exception ex2)
                {
                    connectionLost = true;
                    await DisplayAlert("Unexpected Error", ex2.Message, "Try again");
                }
                finally
                {
                    LoadingInProgress = false;
                }
            } 
            while (connectionLost);
        }
        #endregion
    }
}
