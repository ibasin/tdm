﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDList
{
    using System;
    using Views;
    using ViewModels;
    using Xamarin.Forms;
    
    public class CRUDListView<ModelT> : StackLayout where ModelT : class, ISupermodelListTemplate, new()
    {
        #region Constructors
        public CRUDListView(EventHandler deleteHandler, bool searchBar)
        {
            if (searchBar)
            {
                Spacing = 1;
                SearchBar = new SearchBar { Placeholder = "Type your search term here" };
                Children.Add(SearchBar);
            }

            ListPanel = new ViewWithActivityIndicator<ListView>(new ListView
            {
                ItemTemplate = new ModelT().GetListCellDataTemplate(deleteHandler),
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
                IsPullToRefreshEnabled = true
            });
            Children.Add(ListPanel);
        }
        #endregion

        #region Properties
        public readonly SearchBar SearchBar;
        public readonly ViewWithActivityIndicator<ListView> ListPanel;
        #endregion
     }
}