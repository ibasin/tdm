// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
{
    public interface IHaveSectionNames
    {
        string GetSectionName(int sectionScreenNumber);
    }
}