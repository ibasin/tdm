﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
{
    using ViewModels;
    using Models;
    using DataContext.Core;
    using System;
    using System.Collections.ObjectModel;
    using ReflectionMapper;
    using System.Threading.Tasks;

    public abstract class CRUDChildDetailPage<ModelT, ChildModelT, XFModelT, DataContextT> : CRUDDetailPageBase<ModelT, XFModelT, DataContextT>
        where ModelT : class, ISupermodelNotifyPropertyChanged, IModel, new()
        where ChildModelT : ChildModel, new()
        where XFModelT : XFModelForChildModel<ModelT, ChildModelT>, new()
        where DataContextT : class, IDataContext, new()
    {
        #region Initializers
        public virtual CRUDChildDetailPage<ModelT, ChildModelT, XFModelT, DataContextT> Init(ObservableCollection<ModelT> models, string title, ModelT model, Guid childGuidIdentity, params Guid[] parentGuidIdentities)
        {
            var xfModel = new XFModelT();
            xfModel.Init(model, childGuidIdentity, parentGuidIdentities);
            xfModel = xfModel.MapFrom(model);

            var originalXFModel = new XFModelT();
            originalXFModel.Init(model, childGuidIdentity, parentGuidIdentities);
            originalXFModel = originalXFModel.MapFrom(model);

            XFModel = xfModel;
            OriginalXFModel = originalXFModel;

            return (CRUDChildDetailPage<ModelT, ChildModelT, XFModelT, DataContextT>)base.Init(models, title, model, xfModel, originalXFModel);
        }
        #endregion

        #region Overrdies
        protected override XFModelT GetBlankXFModel()
        {
            var blankXfModel = (XFModelT)new XFModelT().Init(Model, OriginalXFModel.ChildGuidIdentity, OriginalXFModel.ParentGuidIdentities);
            return blankXfModel;
        }
        #endregion
    }
}
