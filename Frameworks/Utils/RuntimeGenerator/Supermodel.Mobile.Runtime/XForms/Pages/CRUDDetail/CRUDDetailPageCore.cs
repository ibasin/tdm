// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
{
    using System.Collections.ObjectModel;
    using Newtonsoft.Json;
    using Encryptor;
    using Models;
    using App;
    using ViewModels;
    using Xamarin.Forms;
    using System.Linq;

    public abstract class CRUDDetailPageCore<ModelT, XFModelT> : ContentPage, IBasicCRUDDetailPage
        where ModelT : class, ISupermodelNotifyPropertyChanged, IModel, new()
        where XFModelT : XFModel, new()
    {
        #region Overrides
        protected virtual void AddCancelButton()
        {
            var cancelToolbarItem = new ToolbarItem("Cancel", CancelBtnIconFilename, async () => {
                DissaperingBecasueOfCancellation = true;
                await Navigation.PopAsync(true);
            });
            ToolbarItems.Add(cancelToolbarItem);
        }
        protected virtual void UnauthorizedHandler()
        {
            FormsApplication.GetRunningApp().HandleUnauthorized();
        }
        protected virtual string ComputeModelHash(ModelT model)
        {
            //We hash Json to ignore changes that do not get persisted
            return JsonConvert.SerializeObject(Model).GetMD5Hash();
        }
        #endregion

        #region Methods
        //override this method to affect the entire view
        public virtual void InitContent()
        {
            //if (DetailView == null)
            //{
            //    DetailView = new CRUDDetailView();
            //    Content = StackLayout = new StackLayout { Children = { DetailView } };

            //    OnLoad();
            //    InitDetailView();
            //}
            DetailView = new CRUDDetailView();
            Content = StackLayout = new StackLayout { Children = { DetailView } };

            OnLoad();
            InitDetailView();
        }
        //Override this method to create sections
        public virtual void InitDetailView()
        {
            // ReSharper disable once SuspiciousTypeConversion.Global
            var sectionResolver = XFModel as IHaveSectionNames;

            var lastCell = XFModel.RenderDetail(this)?.LastOrDefault();
            if (lastCell != null)
            {
                var sectionNum = 0;
                while (true)
                {
                    var cells = XFModel.RenderDetail(this, sectionNum, sectionNum + 99);
                    if (cells.Any())
                    {
                        var sectionName = sectionResolver?.GetSectionName(sectionNum);
                        var section = string.IsNullOrEmpty(sectionName) ? new TableSection() : new TableSection(sectionName);
                        DetailView.ContentView.Root.Add(section);
                        foreach (var cell in cells) section.Add(cell);
                    }
                    if (cells.Contains(lastCell)) break;
                    sectionNum += 100;
                }
            }
        }
        public virtual void OnLoad(){}
        #endregion

        #region Properties
        public StackLayout StackLayout { get; set; }
        public CRUDDetailView DetailView { get; set; }
        public ObservableCollection<ModelT> Models { get; set; } 
        public ModelT Model { get; set; }
        public XFModelT XFModel { get; set; }
        public XFModel GetXFModel() { return XFModel; }
        public T GetXFModel<T>() where T : XFModel { return (T)(XFModel)XFModel; }


        public XFModelT OriginalXFModel { get; set; }

        protected virtual bool CancelButton => false;
        protected virtual string CancelBtnIconFilename => null;
        protected bool DissaperingBecasueOfCancellation { get; set; } //default is false
        #endregion
    }
}