﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
{
    using System.Collections.ObjectModel;
    using ViewModels;
    using ReflectionMapper;
    using Models;
    using DataContext.Core;

    public abstract class CRUDDetailPage<ModelT, XFModelT, DataContextT> : CRUDDetailPageBase<ModelT, XFModelT, DataContextT>
        where ModelT : class, ISupermodelNotifyPropertyChanged, IModel, new()
        where XFModelT : XFModelForModel<ModelT>, new()
        where DataContextT : class, IDataContext, new()
    {
        #region Initializers
        public virtual CRUDDetailPage<ModelT, XFModelT, DataContextT> Init(ObservableCollection<ModelT> models, string title, ModelT model)
        {
            var xfModel = new XFModelT();
            xfModel.Init(model);
            xfModel = xfModel.MapFrom(model);

            var originalXFModel = new XFModelT();
            originalXFModel.Init(model);
            originalXFModel = originalXFModel.MapFrom(model);

            return (CRUDDetailPage<ModelT, XFModelT, DataContextT>)base.Init(models, title, model, xfModel, originalXFModel);
        }
        #endregion

        #region Overrdies
        protected override XFModelT GetBlankXFModel()
        {
            var blankModel = new ModelT();
            var blankXfModel = (XFModelT)new XFModelT().Init(blankModel);
            return blankXfModel;
        }
        #endregion
    }
}
