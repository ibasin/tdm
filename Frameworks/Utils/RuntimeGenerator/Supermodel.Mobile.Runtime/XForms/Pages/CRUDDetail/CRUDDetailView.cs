﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
{
    using Xamarin.Forms;
    using Views;

    public class CRUDDetailView : ViewWithActivityIndicator<TableView>
    {
        public CRUDDetailView() : base(new TableView { Intent = TableIntent.Form, HasUnevenRows = true, Root = new TableRoot()}){}
    }
}
