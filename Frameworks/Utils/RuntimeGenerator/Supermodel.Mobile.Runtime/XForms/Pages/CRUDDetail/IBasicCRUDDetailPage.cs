// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
{
    using ViewModels;
    using Xamarin.Forms;
    using System.Threading.Tasks;

    public interface IBasicCRUDDetailPage : ILayout, IPageController, IElementConfiguration<Page>
    {
        void InitContent();

        CRUDDetailView DetailView { get; set; }
        XFModel GetXFModel();
        T GetXFModel<T>() where T : XFModel;

        Task DisplayAlert(string title, string message, string cancel);
        Task<bool> DisplayAlert(string title, string message, string accept, string cancel);
    }
}