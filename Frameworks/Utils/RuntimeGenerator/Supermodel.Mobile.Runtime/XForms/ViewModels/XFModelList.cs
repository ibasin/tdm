﻿//// ReSharper disable once CheckNamespace
//namespace Supermodel.Mobile.XForms.ViewModels
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using ReflectionMapper;
//    using Models;

//    public class XFModelList<XFModelForModelT, ModelT> : List<XFModelForModelT>, IRMapperCustom
//        where XFModelForModelT : XFModelForModel<ModelT>, new()
//        where ModelT : class, IObjectWithIdentity, ISupermodelNotifyPropertyChanged, IModel, new()
//    {
//        #region IRMapperCustom implemtation
//        public virtual object MapFromObjectCustom(object obj, Type objType)
//        {
//            Clear();
//            var modelList = (ICollection<ModelT>)obj;
//            foreach (var model in modelList.ToList())
//            {
//                var xfModel = new XFModelForModelT().MapFrom(model);
//                Add(xfModel);
//            }
//            return this;
//        }
//        public virtual object MapToObjectCustom(object obj, Type objType)
//        {
//            var modelList = (ICollection<ModelT>)obj;

//            //Add or Update
//            foreach (var xfModel in this)
//            {
//                var modelMatch = modelList.SingleOrDefault(x => x.Identity == xfModel.Identity);
//                if (modelMatch != null)
//                {
//                    xfModel.MapTo(modelMatch);
//                }
//                else
//                {
//                    var newModel = xfModel.MapTo(new ModelT());
//                    modelList.Add(newModel);
//                }
//            }

//            //Delete
//            foreach (var toDoItem in modelList.ToList())
//            {
//                if (this.All(x => x.Identity != toDoItem.Identity)) toDoItem.Delete();
//            }

//            //Set Parent for All
//            //foreach (var model in modelList)
//            //{
//            //    model.ParentToDoListId = Id;
//            //}

//            return modelList;
//        }
//        #endregion
//    }
//}
