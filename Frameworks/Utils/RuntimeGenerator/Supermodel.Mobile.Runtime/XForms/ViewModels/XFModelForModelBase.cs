// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Net;
    using System.Reflection;
    using System.Threading.Tasks;
    using DataContext.Core;
    using Exceptions;
    using Models;
    using ReflectionMapper;
    using UnitOfWork;
    using Utils;
    using App;
    using Pages.CRUDDetail;
    using Views;
    using Xamarin.Forms;

    public abstract class XFModelForModelBase<ModelT> : XFModel, IRMapperCustom where ModelT : class, IModel, ISupermodelNotifyPropertyChanged, new()
    {
        #region ICustom mapper implementation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            return this.MapFromObjectCustomBase(obj);
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            return this.MapToObjectCustomBase(obj);
        }
        #endregion

        #region Methods
        public virtual List<Cell> RenderChildCells<ChildModelT>(Page page, List<ChildModelT> childModels, Func<Page, ChildModelT, Task> onTappedAsync = null)
            where ChildModelT : ChildModel, new()
        {
            var cells = new List<Cell>();
            foreach (var childModel in childModels)
            {
                var dataTemplate = childModel.GetListCellDataTemplate(null);

                var cell = dataTemplate.CreateContent() as Cell;
                // ReSharper disable once PossibleNullReferenceException
                cell.BindingContext = childModel;

                if (onTappedAsync != null)
                {
                    cell.Tapped += async (sender, args) =>
                    {
                        await onTappedAsync(page, childModel);
                    };
                }

                cells.Add(cell);
            }
            return cells;
        }
        public virtual List<Cell> RenderDeletableChildCells<ChildModelT, DataContextT>(Page page, List<ChildModelT> childModels, ObservableCollection<ModelT> parentModels, Func<Page, ChildModelT, Task> onTappedAsync = null)
            where ChildModelT : ChildModel, new()
            where DataContextT : class, IDataContext, new()
        {
            var crudPage = (IBasicCRUDDetailPage)page;

            var cells = new List<Cell>();
            foreach (var childModel in childModels)
            {
                var dataTemplate = childModel.GetListCellDataTemplate(async (sender, args) => 
                {
                    bool connectionLost;
                    do
                    {
                        connectionLost = false;
                        try
                        {
                            using (new ActivityIndicatorFor(crudPage.DetailView))
                            {
                                var deletingCell = (Cell)((MenuItem)sender).Parent;
			
                                using (FormsApplication.GetRunningApp().NewUnitOfWork<DataContextT>())
                                {
                                    //var oldToDoItems = Model.ToDoItems;
                                    var index = Model.DeleteChild(childModel);
                                    Model.Update();
                                    try
                                    {
                                        await UnitOfWorkContext.FinalSaveChangesAsync();
                                    }
                                    catch(Exception)
                                    {
                                        Model.AddChild(childModel, index);
                                        throw;
                                    }
                                }

                                //If no issues, Remove the cell from the screen (from all sections)
                                foreach (var section in crudPage.DetailView.ContentView.Root) section.Remove(deletingCell);

                                //And mark all properteis as changed. This way the list will always update
                                foreach (var property in Model.GetType().GetTypeInfo().DeclaredProperties) Model.OnPropertyChanged(property.Name);
                            }
                        }
                        catch (SupermodelWebApiException ex1)
                        {
                            if (ex1.StatusCode == HttpStatusCode.Unauthorized)
                            {
                                FormsApplication.GetRunningApp().HandleUnauthorized();
                            }
                            else if (ex1.StatusCode == HttpStatusCode.NotFound)
                            {
                                parentModels.RemoveAll(x => x == Model);
                                await crudPage.DisplayAlert("Not Found", "Item you are trying to update no longer exists.", "Ok");
                            }
                            else if (ex1.StatusCode == HttpStatusCode.Conflict)
                            {
                                await crudPage.DisplayAlert("Unable to Delete", ex1.ContentJsonMessage, "Ok");
                            }
                            else if (ex1.StatusCode == HttpStatusCode.InternalServerError)
                            {
                                await crudPage.DisplayAlert("Internal Server Error", ex1.ContentJsonMessage, "Ok");
                            }
                            else
                            {
                                connectionLost = true;
                                await crudPage.DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
                            }
                        }
                        catch (SupermodelDataContextValidationException ex2)
                        {
                            var vrl = ex2.ValidationErrors;
                            if (vrl.Count != 1) throw new SupermodelException("vrl.Count != 1. This should never happen!");
                            //Model = OriginalXFModel.MapTo(Model); //This is where we would normally restore model to the original, but not here
                            if (!vrl[0].Any()) throw new SupermodelException("!vrl[0].Any(): Server returned validation error with no validation results");
                            crudPage.GetXFModel().ShowValidationErrors(vrl[0]);
                        }
                        catch (WebException)
                        {
                            connectionLost = true;
                            await crudPage.DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
                        }
                        catch (Exception ex3)
                        {
                            await crudPage.DisplayAlert("Unexpected Error", ex3.Message, "Try again");
                            connectionLost = true;
                        }
                    }
                    while (connectionLost);
                });

                var cell = dataTemplate.CreateContent() as Cell;
                // ReSharper disable once PossibleNullReferenceException
                cell.BindingContext = childModel;

                if (onTappedAsync != null)
                {
                    cell.Tapped += async (sender, args) =>
                    {
                        await onTappedAsync((Page)crudPage, childModel);
                    };
                }

                cells.Add(cell);
            }
            return cells;
        }
        #endregion

        #region Validation
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // ReSharper disable once ConstantNullCoalescingCondition
            var vr = (ValidationResultList)base.Validate(validationContext) ?? new ValidationResultList();
            var tempEntityForValidation = CreateTempValidationEntity();
            Validator.TryValidateObject(tempEntityForValidation, new ValidationContext(tempEntityForValidation), vr); 
            return vr;
        }
        #endregion

        #region Private Helper Methods
        protected virtual ModelT CreateTempValidationEntity()
        {
            return (ModelT)this.MapToObject(new ModelT());
        }
        #endregion

        #region Standard Properties
        //[ScaffoldColumn(false), NotRMapped, NotRCompared] public virtual long Id { get; set; }
        [ScaffoldColumn(false), NotRMapped, NotRCompared] public ModelT Model { get; protected set; }
        //[ScaffoldColumn(false), NotRMapped, NotRCompared] public virtual bool IsNew => Id == 0;
        #endregion
    }
}