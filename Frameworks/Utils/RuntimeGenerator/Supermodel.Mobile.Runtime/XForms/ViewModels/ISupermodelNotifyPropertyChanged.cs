// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.ViewModels
{
    using System.ComponentModel;

    public interface ISupermodelNotifyPropertyChanged : INotifyPropertyChanged
    {
        void OnPropertyChanged(string propertyName);
    }
}