// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.ViewModels
{
    using Models;

    public abstract class XFModelForModel<ModelT> : XFModelForModelBase<ModelT> where ModelT : class, IModel, ISupermodelNotifyPropertyChanged, new()
    {
        #region Constructors
        public virtual XFModelForModel<ModelT> Init(ModelT model)
        {
            Model = model;
            return this;
        }
        #endregion
    }
}