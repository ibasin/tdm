// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.ViewModels
{
    using Models;
    using System;
    using System.ComponentModel.DataAnnotations;
    using ReflectionMapper;

    public abstract class XFModelForChildModel<ModelT, ChildModelT> : XFModelForModelBase<ModelT> 
        where ModelT : class, IModel, ISupermodelNotifyPropertyChanged, new()
        where ChildModelT : ChildModel, new()
    {
        #region Constructors
        public virtual XFModelForChildModel<ModelT, ChildModelT> Init(ModelT model, Guid childGuidIdentity, params Guid[] parentGuidIdentities)
        {
            ParentGuidIdentities = parentGuidIdentities ?? throw new ArgumentNullException(nameof(parentGuidIdentities), "You may need tp override OnLoad on root Model and set up ParentIdentities for all ChildModels there");
            ChildGuidIdentity = childGuidIdentity;
            Model = model;
            return this;
        }
        #endregion

        #region Overrdies
        public override object MapFromObjectCustom(object obj, Type objType)
        {
            var model = (ModelT)obj;
            var childModel = model.GetChildOrDefault<ChildModelT>(ChildGuidIdentity, ParentGuidIdentities) ?? new ChildModelT { ChildGuidIdentity = ChildGuidIdentity, ParentGuidIdentities = ParentGuidIdentities };
            this.MapFromObjectCustomBase(childModel);
            return this;
        }
        public override object MapToObjectCustom(object obj, Type objType)
        {
            var model = (ModelT)obj;
            var childModel = model.GetChildOrDefault<ChildModelT>(ChildGuidIdentity, ParentGuidIdentities);
            if (childModel == null)
            {
                childModel = new ChildModelT { ParentGuidIdentities = ParentGuidIdentities };
                this.MapToObjectCustomBase(childModel);
                model.AddChild(childModel);
            }
            else
            {
                this.MapToObjectCustomBase(childModel);
            }
            return obj;
        }
        #endregion

        #region Properties
        [ScaffoldColumn(false), NotRMapped, NotRCompared] public virtual Guid[] ParentGuidIdentities { get; set; }
        [ScaffoldColumn(false), NotRMapped, NotRCompared] public virtual Guid ChildGuidIdentity { get; set; } = Guid.NewGuid();
        #endregion
    }
}