﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.ViewModels
{
    using System;
    
    public class BinaryFileXFModel
	{
		public String Name { get; set; }
		public Byte[] BinaryContent { get; set; }
	}

}
