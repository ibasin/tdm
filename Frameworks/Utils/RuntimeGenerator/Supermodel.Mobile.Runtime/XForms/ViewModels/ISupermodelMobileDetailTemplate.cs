// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.ViewModels
{
    using System.Collections.Generic;
    using Xamarin.Forms;

    public interface ISupermodelMobileDetailTemplate
    {
        List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue);
    }
}