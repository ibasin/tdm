// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.XForms.ViewModels
{
    using Xamarin.Forms;
    using System;

    public interface ISupermodelListTemplate : ISupermodelNotifyPropertyChanged
    {
        DataTemplate GetListCellDataTemplate(EventHandler deleteHandler);
    }
}