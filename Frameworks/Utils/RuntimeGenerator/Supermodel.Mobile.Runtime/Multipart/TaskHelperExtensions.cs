﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Multipart
{
    using System.Threading.Tasks;
    
    public static class TaskHelperExtensions
    {
        public static async Task<object> CastToObject(this Task task)
        {
            await task;
            return null;
        }

        public static async Task<object> CastToObject<T>(this Task<T> task)
        {
            return (object)await task;
        }

        public static void ThrowIfFaulted(this Task task)
        {
            task.GetAwaiter().GetResult();
        }

        public static bool TryGetResult<ResultT>(this Task<ResultT> task, out ResultT result)
        {
            if (task.Status == TaskStatus.RanToCompletion)
            {
                result = task.Result;
                return true;
            }

            result = default(ResultT);
            return false;
        }
    }
}
