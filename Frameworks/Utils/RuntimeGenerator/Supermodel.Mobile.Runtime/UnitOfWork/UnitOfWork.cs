﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.UnitOfWork
{
    using System;
    using DataContext.Core;
    using Exceptions;
    using Async;

    public class UnitOfWork<DataContextT> : IDisposable where DataContextT : class, IDataContext, new()
    {
        #region Constructors
        public UnitOfWork(ReadOnly readOnly = ReadOnly.No)
        {
            Releaser = null;
            Context = new DataContextT();
            if (readOnly == ReadOnly.Yes) Context.MakeReadOnly();
            UnitOfWorkContext<DataContextT>.PushDbContext(Context);
        }
        #endregion

        #region IDisposable implemetation
        public virtual void Dispose()
        {
            Context.Dispose();

            var context = UnitOfWorkContext<DataContextT>.PopDbContext();

            // ReSharper disable PossibleUnintendedReferenceComparison
            if (context != Context) throw new SupermodelException("POP on Dispose popped mismatched Data Context.");
            // ReSharper restore PossibleUnintendedReferenceComparison

            if (Releaser != null) Releaser.Value.Dispose();
        }
        #endregion

        #region Properties
        public DataContextT Context { get; private set; }
        public AsyncLock.Releaser? Releaser { get; set; }
        #endregion
    }
}
