﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.UnitOfWork
{
    // ReSharper disable once RedundantUsingDirective
    using System.Runtime.Remoting.Messaging;
    using System.Net.Http;
    using System.Threading.Tasks;
    
    public static class HttpClientExtensions
    {
        public static async Task<HttpResponseMessage> GetAsyncAndPreserveContext(this HttpClient httpClient, string url)
        {
            //This is to account for a Mono problem in Android
            #if __ANDROID__
			var contextStack = CallContext.LogicalGetData("SupermodelDataContextStack");
            var migrationInProgressFlag = CallContext.LogicalGetData("SupermodelSqliteMigrationInProgressOnThisThread");
            #endif

            var dataResponse = await httpClient.GetAsync(url);
            
            #if __ANDROID__
            CallContext.LogicalSetData("SupermodelSqliteMigrationInProgressOnThisThread", migrationInProgressFlag);
            CallContext.LogicalSetData("SupermodelDataContextStack", contextStack);
            #endif

            return dataResponse;
        }

        public static async Task<HttpResponseMessage> SendAsyncAndPreserveContext(this HttpClient httpClient, HttpRequestMessage request)
        {
            //This is to account for a Mono problem in Android
            #if __ANDROID__
			var contextStack = CallContext.LogicalGetData("SupermodelDataContextStack");
            var migrationInProgressFlag = CallContext.LogicalGetData("SupermodelSqliteMigrationInProgressOnThisThread");
            #endif

            var dataResponse = await httpClient.SendAsync(request);
            
            #if __ANDROID__
            CallContext.LogicalSetData("SupermodelSqliteMigrationInProgressOnThisThread", migrationInProgressFlag);
            CallContext.LogicalSetData("SupermodelDataContextStack", contextStack);
            #endif

            return dataResponse;
        }
    }
}
