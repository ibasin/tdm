﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.UnitOfWork
{
    using System;
    using Exceptions;
    using Encryptor;
    using System.Threading.Tasks;
    using Models;
    using System.Collections.Generic;
    using DataContext.Core;
    using DataContext.WebApi;
    using System.Runtime.Remoting.Messaging;
    using System.Linq;
    using DataContext.Sqlite;

    //Shortcuts for the most often used Context methods
    public static class UnitOfWorkContext
    {
        #region Methods and Properties
        public static Dictionary<string, object> CustomValues => UnitOfWorkContextCore.CurrentDataContext.CustomValues;
        public static Task ResetDatabaseAsync()
        {
            if (!(UnitOfWorkContextCore.CurrentDataContext is SqliteDataContext)) throw new SupermodelException("ResetDatabaseAsync() is only valid for SqliteDataContext");
            return ((SqliteDataContext)UnitOfWorkContextCore.CurrentDataContext).ResetDatabaseAsync();
        }
        public static Task SaveChangesAsync()
        {
            if (!(UnitOfWorkContextCore.CurrentDataContext is IWriteableDataContext)) throw new SupermodelException("SaveChangesAsync() is only valid for IWriteableDataContext");
            return ((IWriteableDataContext)UnitOfWorkContextCore.CurrentDataContext).SaveChangesAsync();
        }
        public static Task FinalSaveChangesAsync()
        {
            if (!(UnitOfWorkContextCore.CurrentDataContext is IWriteableDataContext)) throw new SupermodelException("FinalSaveChangesAsync() is only valid for IWriteableDataContext");
            return ((IWriteableDataContext)UnitOfWorkContextCore.CurrentDataContext).FinalSaveChangesAsync();
        }
        public static void DetectUpdates()
        {
            if (!(UnitOfWorkContextCore.CurrentDataContext is IWriteableDataContext)) throw new SupermodelException("DetectUpdates() is only valid for IWriteableDataContext");
            ((IWriteableDataContext)UnitOfWorkContextCore.CurrentDataContext).DetectAllUpdates();
        }
        public static bool CommitOnDispose
        {
            get => UnitOfWorkContextCore.CurrentDataContext.CommitOnDispose;
            set => UnitOfWorkContextCore.CurrentDataContext.CommitOnDispose = value;
        }
        public static AuthHeader AuthHeader
        {
            get
            {
                if (!(UnitOfWorkContextCore.CurrentDataContext is IWebApiAuthorizationContext)) throw new SupermodelException("AuthHeader is only accesable for IWebApiAuthorizationContext");
                return ((IWebApiAuthorizationContext)UnitOfWorkContextCore.CurrentDataContext).AuthHeader;
            }
            set
            {
                if (!(UnitOfWorkContextCore.CurrentDataContext is IWebApiAuthorizationContext)) throw new SupermodelException("AuthorizationHeader is only accesable for IWebApiAuthorizationContext");
                ((IWebApiAuthorizationContext)UnitOfWorkContextCore.CurrentDataContext).AuthHeader = value;
            }
        }
        public static Task<LoginResult> ValidateLoginAsync<ModelT>() where ModelT : class, IModel
        {
            if (!(UnitOfWorkContextCore.CurrentDataContext is IWebApiAuthorizationContext)) throw new SupermodelException("AuthorizationHeader is only accesable for IWebApiAuthorizationContext");
            return ((IWebApiAuthorizationContext)UnitOfWorkContextCore.CurrentDataContext).ValidateLoginAsync<ModelT>();
        }
        public static int CacheAgeToleranceInSeconds
        {
            get
            {
                if (!(UnitOfWorkContextCore.CurrentDataContext is ICachedDataContext)) throw new SupermodelException("CacheAgeToleranceInSeconds is only accesable for ICachedDataContext");
                return ((ICachedDataContext)UnitOfWorkContextCore.CurrentDataContext).CacheAgeToleranceInSeconds;
            }
            set
            {
                if (!(UnitOfWorkContextCore.CurrentDataContext is ICachedDataContext)) throw new SupermodelException("CacheAgeToleranceInSeconds is only accesable for ICachedDataContext");
                ((ICachedDataContext)UnitOfWorkContextCore.CurrentDataContext).CacheAgeToleranceInSeconds = value;
            }
        }
        public static Task PurgeCacheAsync(int? cacheExpirationAgeInSeconds = null, Type modelType = null)
        {
            if (!(UnitOfWorkContextCore.CurrentDataContext is ICachedDataContext)) throw new SupermodelException("CacheAgeToleranceInSeconds is only accesable for ICachedDataContext");
            return ((ICachedDataContext)UnitOfWorkContextCore.CurrentDataContext).PurgeCacheAsync(cacheExpirationAgeInSeconds, modelType);
        }
        #endregion
    }
    
    public static class UnitOfWorkContext<DataContextT> where DataContextT : class, IDataContext, new()
    {
        #region Methods and Properties
        public static DataContextT PopDbContext()
        {
            return (DataContextT)UnitOfWorkContextCore.PopDbContext();
        }
        public static void PushDbContext(DataContextT context)
        {
            UnitOfWorkContextCore.PushDbContext(context);
        }
        public static int StackCount => UnitOfWorkContextCore.StackCount;
        public static DataContextT CurrentDataContext => (DataContextT)UnitOfWorkContextCore.CurrentDataContext;
        public static bool HasDbContext()
        {
            return StackCount > 0;
        }
        #endregion
    }

    public static class UnitOfWorkContextCore
    {
        #region Methods and Properties
        public static IDataContext PopDbContext()
        {
            try
            {
                IDataContext context;
                _contextStackImmutable = _contextStackImmutable.Pop(out context);
                return context;

            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException("Stack is empty");
            }
        }
        public static void PushDbContext(IDataContext context)
        {
            _contextStackImmutable = _contextStackImmutable.Push(context);
        }
        public static int StackCount => _contextStackImmutable.Count();
        public static IDataContext CurrentDataContext
        {
            get
            {
                try
                {
                    return _contextStackImmutable.Peek();
                }
                catch (InvalidOperationException)
                {
                    throw new SupermodelException("Current UnitOfWork does not exist. All database access oprations must be wrapped in 'using(new UnitOfWork())'");
                }
            }
        }
        #endregion

        #region Private variables
        //private sealed class StackWrapper : MarshalByRefObject
        //{
        //    public ImmutableStack<IDataContext> Value { get; set; }
        //}

        // ReSharper disable once InconsistentNaming
        private static ImmutableStack<IDataContext> _contextStackImmutable
        {
            get
            {
                var contextStack = CallContext.LogicalGetData("SupermodelDataContextStack") as ImmutableStack<IDataContext>;
                return contextStack ?? ImmutableStack.Create<IDataContext>();
            }
            set
            {
                CallContext.LogicalSetData("SupermodelDataContextStack", value);
            }
        }
        #endregion
    }
}
