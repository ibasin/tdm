﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Attributes
{
    using System;
    
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class ForceRequiredLabelAttribute : Attribute { }
}