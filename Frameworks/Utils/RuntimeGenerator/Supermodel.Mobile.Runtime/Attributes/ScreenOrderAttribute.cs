﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Attributes
{
    using System;
    
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class ScreenOrderAttribute : Attribute 
    {
        public ScreenOrderAttribute(int order)
        {
            Order = order;
        }

        public int Order { get; set; }
    }
}
