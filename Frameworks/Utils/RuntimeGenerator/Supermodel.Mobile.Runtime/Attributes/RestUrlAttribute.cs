﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Attributes
{
    using System;

    public class RestUrlAttribute : Attribute
    {
        public RestUrlAttribute(string url)
        {
            Url = url;
        }
        
        public string Url { get; set; }
    }
}
