﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.Offline
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Sqlite;
    using WebApi;
    using Exceptions;
    using Models;
    using ReflectionMapper;
    using Repository;
    using Core;
    using UnitOfWork;
    using Xamarin.Forms;
    
    public abstract class Synchronizer<ModelT, WebApiDataContextT, SqliteDataContextT> 
        where ModelT : class, IModel, new()
        where SqliteDataContextT : SqliteDataContext, new()
        where WebApiDataContextT : WebApiDataContext, new()
    {
        #region Constructiors
        protected Synchronizer()
        {
            RefreshFromMasterAfterSynch = true;
        }
        #endregion

        #region Methods
        public async Task SynchronizeAsync()
        {
            using(var webApiUOW = new UnitOfWork<WebApiDataContextT>())
            {
                SetUpWebApiContext(UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext);

                using(var sqliteUOW = new UnitOfWork<SqliteDataContextT>())
                {
                    SetUpSqliteContext(UnitOfWorkContext<SqliteDataContextT>.CurrentDataContext);
                    
                    //----------------------------------------------------------------------------------------
                    //Load all the models to synchronize
                    //----------------------------------------------------------------------------------------
                    var masterModels = await LoadAllSynchFromMasterAsync();
                    var localModels = await LoadAllSynchFromLocalAsync();
                    
                    //----------------------------------------------------------------------------------------
                    //Run the synching algorithm
                    //----------------------------------------------------------------------------------------
                    SynchLists(masterModels, localModels);

                    //----------------------------------------------------------------------------------------
                    //Let's try to validate all the models that are to be saved locally
                    //----------------------------------------------------------------------------------------
                    SupermodelDataContextValidationException localValidationException = null;
                    try
                    {
                        UnitOfWorkContext<SqliteDataContextT>.CurrentDataContext.ValidatePendingActions();
                    }
                    catch (SupermodelDataContextValidationException ex)
                    {
                        localValidationException = ex;
                    }
                    catch (Exception)
                    {
                        webApiUOW.Context.CommitOnDispose = sqliteUOW.Context.CommitOnDispose = false;
                        throw;
                    }
                    if (localValidationException != null)
                    {
                        await ResolveLocalValidationError(localValidationException, webApiUOW, sqliteUOW);
                    }

                    //----------------------------------------------------------------------------------------
                    //Register for resresh if needed
                    //----------------------------------------------------------------------------------------

                    DelayedModels<ModelT> delayedMasterModels = null;
                    if (RefreshFromMasterAfterSynch)
                    {
			            var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
                        UnitOfWorkContext.DetectUpdates();
			            UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
                        RegisterDelayedLoadAllSynchFromMaster(out delayedMasterModels);
                    }

                    //----------------------------------------------------------------------------------------
                    //Then try to save changes to the web api
                    //----------------------------------------------------------------------------------------
                    UnitOfWorkContext<SqliteDataContextT>.PopDbContext();

                    SupermodelDataContextValidationException serverValidationException = null;
                    try
                    {
                        await UnitOfWorkContext.FinalSaveChangesAsync();
                    }
                    catch (SupermodelDataContextValidationException ex)
                    {
                        serverValidationException = ex;
                    }
                    catch(Exception)
                    {
                        webApiUOW.Context.CommitOnDispose = sqliteUOW.Context.CommitOnDispose = false;
                        throw;
                    }
                    
                    if (serverValidationException != null)
                    {
                        await ResolveServerValidationError(serverValidationException, webApiUOW, sqliteUOW);
                    }
                    UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteUOW.Context);

                    //----------------------------------------------------------------------------------------
                    //Refsresh local models if we have data to refresh with (that is if we registered for it ealrier)
                    //----------------------------------------------------------------------------------------
                    if (delayedMasterModels != null)
                    {
                        foreach (var localModel in localModels)
                        {
                            var matchingDelpayedMasterModel = delayedMasterModels.Values.SingleOrDefault(x => x.Id == localModel.Id);
                            if (matchingDelpayedMasterModel != null) CopyModel1IntoModel2(matchingDelpayedMasterModel, localModel);
                        }
                    }

                    //----------------------------------------------------------------------------------------
                    //If that succeds, then we save changes to the local db, should not have any more validation errors, since we already checked
                    //----------------------------------------------------------------------------------------
                    LastSynchDateTimeUtc = DateTime.UtcNow;
                    await UnitOfWorkContext.FinalSaveChangesAsync();
                }
            }
        }
        public bool IsUploadPending(ModelT model)
        {
            return LastSynchDateTimeUtc == null || GetModifiedDateTimeUtc(model) > LastSynchDateTimeUtc;
        }
        #endregion

        #region Main Algorithm
        protected virtual void SynchLists(List<ModelT> masterModels, List<ModelT> localModels)
        {
            //find updated on the server, update locally
            //find updated on the client, update on the server
            //find created on the server, create locally
            //find deleted locally, delete on the server
            foreach (var masterModel in masterModels)
            {
                //Try to find a matching local model
                var matchingLocalModel = localModels.SingleOrDefault(x => x.Id == masterModel.Id);
                if (matchingLocalModel != null) //if we found one
                {
                    if (GetModifiedDateTimeUtc(masterModel) > LastSynchDateTimeUtc && GetModifiedDateTimeUtc(matchingLocalModel) > LastSynchDateTimeUtc)
                    {
                        //if model was updated on both server and client, call the hook to let the user resolve conflict
                        HandleModelUpdatedOnServerAndDevice(masterModel, matchingLocalModel);
                    }
                    else
                    {
                        //otherwise, we just figure out the newer one and copy it into the older one
                        CopyNewerModelIntoOlder(masterModel, matchingLocalModel);
                    }
                }
                else //if not, there could be two scenarios:
                {
                    //If the model on the server was created after our last synch or if we never syhcned before
                    if (GetCreatedDateTimeUtc(masterModel) > LastSynchDateTimeUtc || LastSynchDateTimeUtc == null)
                    {
                        //we need to add the master model to our local storage
                        //var localModel = new ModelT();
                        //CopyModel1IntoModel2(masterModel, localModel);
                        //localModel.Add();
                        masterModel.Add(); //Add master model to local storage
                    }
                    else
                    {
                        //otherwise it means that we deleted the model on the client and now we need to delete it from the server
                        if (GetModifiedDateTimeUtc(masterModel) > LastSynchDateTimeUtc)
                        {
                            //if since our last synch the model was modified on the server and deleted on the client, call the hook to let the user resolve conflict
                            HandleModelUpdatedOnServerDeletedOnDevice(masterModel);
                        }
                        else
                        {
                            var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
                            masterModel.Delete();
                            UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
                        }
                    }
                }
            }

            //find created locally, create on the server
            //find deleted on the server, delete locally
            foreach (var localModel in localModels)
            {
                if (localModel.Id < 0)
                {
                    //if this model was never created on the server, go ahead and create it
                    var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
                    localModel.Id = 0; //make sure when we add it on the server, Id == 0. When it is saved on the server, the local model's id should get updated
                    localModel.Add(); //add local model to server context
                    UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
                }
                else
                {
                    //Try to find a matching server model
                    var matchingServerModel = masterModels.SingleOrDefault(x => x.Id == localModel.Id);
                    if (matchingServerModel == null) //if one exists, we already updated it and need not worry
                    {
                        //otherwise, it means that the model was deleted on the server
                        if (GetModifiedDateTimeUtc(localModel) > LastSynchDateTimeUtc)
                        {
                            //if since our last synch the model was modified on the device and deleted on the client, call the hook to let the user resolve conflict
                            HandleModelUpdatedOnDeviceDeletedOnServer(localModel);
                        }
                        else
                        {
                            localModel.Delete();
                        }
                    }
                }
            }
        }
        #endregion

        #region Conflict Resolution Methods
        protected virtual Task ResolveLocalValidationError(SupermodelDataContextValidationException validationException, UnitOfWork<WebApiDataContextT> webApiUOW, UnitOfWork<SqliteDataContextT> sqliteUOW)
        {
            //default implemetation just rolls back transcations and rethrows the exception
            webApiUOW.Context.CommitOnDispose = sqliteUOW.Context.CommitOnDispose = false;
            throw validationException;
        }
        protected virtual Task ResolveServerValidationError(SupermodelDataContextValidationException validationException, UnitOfWork<WebApiDataContextT> webApiUOW, UnitOfWork<SqliteDataContextT> sqliteUOW)
        {
            //default implemetation just rolls back transcations and rethrows the exception
            webApiUOW.Context.CommitOnDispose = sqliteUOW.Context.CommitOnDispose = false;
            throw validationException;
        }
        protected virtual void HandleModelUpdatedOnDeviceDeletedOnServer(ModelT localModel)
        {
            //default implements "last one wins" approach
            localModel.Delete();
        }
        protected virtual void HandleModelUpdatedOnServerAndDevice(ModelT masterModel, ModelT localModel)
        {
            //default implements "last one wins" approach
            CopyNewerModelIntoOlder(masterModel, localModel);
        }
        protected virtual void HandleModelUpdatedOnServerDeletedOnDevice(ModelT masterModel)
        {
            //default implements "last one wins" approach
            var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
            masterModel.Delete();
            UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
        }
        #endregion

        #region Modified and Created Utc DateTime Resolution
        public abstract DateTime GetModifiedDateTimeUtc(ModelT model);
        public abstract DateTime GetCreatedDateTimeUtc(ModelT model);
        #endregion

        #region Helpers that are Meant to be Overriden for Customization
        protected virtual void RegisterDelayedLoadAllSynchFromMaster(out DelayedModels<ModelT> delayedMasterModels)
        {
            var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
            RepoFactory.Create<ModelT>().DelayedGetAll(out delayedMasterModels);
            UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
        }
        protected virtual Task<List<ModelT>> LoadAllSynchFromMasterAsync()
        {
            var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
            var result = RepoFactory.Create<ModelT>().GetAllAsync();
            UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
            return result;
        }
        protected virtual Task<List<ModelT>> LoadAllSynchFromLocalAsync()
        {
            return RepoFactory.Create<ModelT>().GetAllAsync();
        }
        public virtual void CopyModel1IntoModel2(ModelT model1, ModelT model2)
        {
            if (model1.BroughtFromMasterDbOnUtc == null || model2.BroughtFromMasterDbOnUtc == null) throw new SupermodelException("(model1.BroughtFromMasterDbOnUtc == null || model2.BroughtFromMasterDbOnUtc == null): this should not happen");
            model1.BroughtFromMasterDbOnUtc = model2.BroughtFromMasterDbOnUtc = (model1.BroughtFromMasterDbOnUtc > model2.BroughtFromMasterDbOnUtc ? model1.BroughtFromMasterDbOnUtc : model2.BroughtFromMasterDbOnUtc);
            model2.MapFrom(model1);
        }
        public virtual void CopyNewerModelIntoOlder(ModelT model1, ModelT model2)
        {
            if (GetModifiedDateTimeUtc(model1) > GetModifiedDateTimeUtc(model2)) CopyModel1IntoModel2(model1, model2);
            else CopyModel1IntoModel2(model2, model1);
        }
        protected abstract void SetUpWebApiContext(WebApiDataContextT context);
        protected abstract void SetUpSqliteContext(SqliteDataContextT context);
        #endregion

        #region LastSynch DateTime Handling
        public virtual DateTime? LastSynchDateTimeUtc
        {
            get => _lastSynchDateTimeUtc ?? (_lastSynchDateTimeUtc = LastSynchDateTimeUtcInternal);
            set => _lastSynchDateTimeUtc = LastSynchDateTimeUtcInternal = value;
        }
        private DateTime? _lastSynchDateTimeUtc;
        
        protected virtual DateTime? LastSynchDateTimeUtcInternal
        {
            get
            {
                if (!Application.Current.Properties.ContainsKey("smLastSynchDateTimeUtc")) return null;
                return Application.Current.Properties["smLastSynchDateTimeUtc"] as DateTime?;
            }
            set
            {
                Application.Current.Properties["smLastSynchDateTimeUtc"] = value;
                #pragma warning disable 4014
                Application.Current.SavePropertiesAsync();
                #pragma warning restore 4014
            }
        }
        #endregion

        #region Properties
        public bool RefreshFromMasterAfterSynch { get; set; }
        #endregion
    }
}
