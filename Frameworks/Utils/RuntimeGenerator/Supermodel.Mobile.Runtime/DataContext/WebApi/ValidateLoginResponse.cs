﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.WebApi
{
    public class ValidateLoginResponse
    {
        public long? UserId { get; set; }
        public string UserLabel { get; set; }
    }
}
