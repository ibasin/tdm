﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.WebApi
{
    public interface IQueryStringProvider
    {
        string GetQueryString(object searchBy, int? skip, int? take, string sortBy);
    }
}
