// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.Core
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;
    
    public interface IReadableDataContext : IDataContext
    {
        #region Reads
        Task<ModelT> GetByIdAsync<ModelT>(long id) where ModelT : class, IModel, new();
        Task<ModelT> GetByIdOrDefaultAsync<ModelT>(long id) where ModelT : class, IModel, new();
        Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null) where ModelT : class, IModel, new();
        Task<long> GetCountAllAsync<ModelT>(int? skip = null, int? take = null) where ModelT : class, IModel, new();
        #endregion

        #region Batch Reads
        void DelayedGetById<ModelT>(out DelayedModel<ModelT> model, long id) where ModelT : class, IModel, new();
        void DelayedGetByIdOrDefault<ModelT>(out DelayedModel<ModelT> model, long id) where ModelT : class, IModel, new();
        void DelayedGetAll<ModelT>(out DelayedModels<ModelT> models) where ModelT : class, IModel, new();
        void DelayedGetCountAll<ModelT>(out DelayedCount count) where ModelT : class, IModel, new();
        #endregion
    }
}