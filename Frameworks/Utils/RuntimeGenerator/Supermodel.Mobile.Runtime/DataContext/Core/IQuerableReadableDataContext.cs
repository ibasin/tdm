﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.Core
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Models;
    
    public interface IQuerableReadableDataContext : IReadableDataContext
    {
        #region Queries
        Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null) where ModelT : class, IModel, new();
        Task<long> GetCountWhereAsync<ModelT>(object searchBy, int? skip = null, int? take = null) where ModelT : class, IModel, new();
        #endregion

        #region Delayed Queries
        void DelayedGetWhere<ModelT>(out DelayedModels<ModelT> models, object searchBy, string sortBy = null, int? skip = null, int? take = null) where ModelT : class, IModel, new();
        void DelayedGetCountWhere<ModelT>(out DelayedCount count, object searchBy) where ModelT : class, IModel, new();
        #endregion
    }
}