﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.Core
{
    using System;
    using System.Threading.Tasks;
    
    public interface ICachedDataContext
    {
        int CacheAgeToleranceInSeconds { get; set; }
        Task PurgeCacheAsync(int? cacheExpirationAgeInSeconds = null, Type modelType = null);
    }
}
