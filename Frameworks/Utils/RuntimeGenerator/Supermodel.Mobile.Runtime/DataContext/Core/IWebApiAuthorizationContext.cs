﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.Core
{
    using Encryptor;
    using System.Threading.Tasks;
    using Models;
    using WebApi;

    public interface IWebApiAuthorizationContext
    {
        AuthHeader AuthHeader { get; set; }
        Task<LoginResult> ValidateLoginAsync<ModelT>() where ModelT : class, IModel;
    }
}
