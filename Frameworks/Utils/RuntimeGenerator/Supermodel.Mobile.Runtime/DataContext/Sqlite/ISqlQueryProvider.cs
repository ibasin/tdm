﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.Sqlite
{
    using Models;
    
    public interface ISqlQueryProvider
    {
        object GetIndex<ModelT>(int idxNum0To29, ModelT model);
        string GetWhereClause<ModelT>(object searchBy, string sortBy);
        string GetSkipAndTakeForWhereClause<ModelT>(int? skip, int? take);
    }
}
