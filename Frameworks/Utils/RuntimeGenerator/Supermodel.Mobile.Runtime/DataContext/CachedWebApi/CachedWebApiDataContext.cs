﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.DataContext.CachedWebApi
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Encryptor;
    using System.Text;
    using System.Threading.Tasks;
    using Core;
    using Sqlite;
    using WebApi;
    using Exceptions;
    using Models;
    using Repository;
    using UnitOfWork;
    using ReflectionMapper;
    using SQLite;

    public class CachedWebApiDataContext<WebApiDataContextT, SqlliteDataContextT> : DataContextBase, IWebApiAuthorizationContext, ICachedDataContext
        where WebApiDataContextT : WebApiDataContext, new()
        where SqlliteDataContextT : SqliteDataContext, new()
    {
        #region Constructors
        public CachedWebApiDataContext()
        {
            CacheAgeToleranceInSeconds = 5 * 60; // 5 min
        }
        #endregion

        #region Methods
        public async Task PurgeCacheAsync(int? cacheExpirationAgeInSeconds = null, Type modelType = null)
        {
            var sqlliteContext = new SqlliteDataContextT();
            await sqlliteContext.InitDbAsync();
            var db = new SQLiteAsyncConnection(sqlliteContext.DatabaseFilePath);
            var sb = new StringBuilder();
            sb.AppendFormat(@"DELETE FROM {0}", sqlliteContext.DataTableName);
            var first = true;
                    
            if (cacheExpirationAgeInSeconds != null)
            {
                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
                if (first)
                {
                    sb.Append(" WHERE ");
                    first = false;
                }
                else
                // ReSharper disable HeuristicUnreachableCode
                {
                    sb.Append(" AND ");
                }
                // ReSharper restore HeuristicUnreachableCode
                sb.AppendFormat("BroughtFromMasterDbOnUtcTicks < {0}", DateTime.UtcNow.AddSeconds(cacheExpirationAgeInSeconds.Value).Ticks);
            }

            if (modelType != null)
            {
                if (first)
                {
                    sb.Append(" WHERE ");
                    first = false;
                }
                else
                {
                    sb.Append(" AND ");
                }
                sb.AppendFormat("ModelTypeLogicalName == '{0}'", GetModelTypeLogicalName(modelType));
            }

            if (first)
            {
                sb.Append(" WHERE ");
                // ReSharper disable once RedundantAssignment
                first = false;
            }
            else
            {
                sb.Append(" AND ");
            }
            sb.AppendFormat("ModelTypeLogicalName != '{0}'", sqlliteContext.SchemaVersionModelType);

            var commandText = sb.ToString();
            await db.ExecuteAsync(commandText);
        }
        #endregion

        #region ValidateLogin
        public virtual async Task<LoginResult> ValidateLoginAsync<ModelT>() where ModelT : class, IModel
        {
            using (new UnitOfWork<WebApiDataContextT>())
            {
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;

                return await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.ValidateLoginAsync<ModelT>();
            }
        }
        #endregion

        #region DataContext Reads
        public override async Task<ModelT> GetByIdOrDefaultAsync<ModelT>(long id)
        {
            //First check local cache
            using (new UnitOfWork<SqlliteDataContextT>())
            {
                var cachedModel = await RepoFactory.Create<ModelT>().GetByIdOrDefaultAsync(id);
                if (cachedModel != null)
                {
                    if (cachedModel.BroughtFromMasterDbOnUtc != null && cachedModel.BroughtFromMasterDbOnUtc.Value.AddSeconds(CacheAgeToleranceInSeconds) > DateTime.UtcNow)
                    {
                        ManagedModels.Add(new ManagedModel(cachedModel));
                        return cachedModel;
                    }
                    else
                    {
                        cachedModel.Delete();
                    }
                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
                }
                else
                {
                    //Mark done for performance reasons
                    UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.MakeCompletedAndFinalized();
                }
            }

            //if we get here, we need to get the data from web api service
            ModelT masterModel;
            using (new UnitOfWork<WebApiDataContextT>())
            {
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
                
                masterModel = await RepoFactory.Create<ModelT>().GetByIdOrDefaultAsync(id);
            }

            //Now save master model to cache and add it to managed models if we have something to save
            if (masterModel != null)
            {
                using (new UnitOfWork<SqlliteDataContextT>())
                {
                    ManagedModels.Add(new ManagedModel(masterModel)); 
                    UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.AddOrUpdate(masterModel);
                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
                }
                
            }

            return masterModel;
        }
        public override async Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null)
        {
            //first delete local cache, we will be refreshing it with new data
            var sqlliteContext = new SqlliteDataContextT();
            await sqlliteContext.InitDbAsync();
            var db = new SQLiteAsyncConnection(sqlliteContext.DatabaseFilePath);
            var commandText = $@"DELETE FROM {sqlliteContext.DataTableName} WHERE ModelTypeLogicalName == '{GetModelTypeLogicalName(typeof(ModelT))}'";
            await db.ExecuteAsync(commandText);
            
            //get the data from web api service
            List<ModelT> masterModels;
            using (new UnitOfWork<WebApiDataContextT>())
            {
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;

                masterModels = await RepoFactory.Create<ModelT>().GetAllAsync(skip, take);
            }

            //Now save master models to cache if we have something to save
            if (masterModels.Any())
            {
                using (new UnitOfWork<SqlliteDataContextT>())
                {
                    foreach (var masterModel in masterModels)
                    {
                        ManagedModels.Add(new ManagedModel(masterModel)); 
                        UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.AddOrUpdate(masterModel);
                    }
                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
                }
            }

            return masterModels;
        }
        public override async Task<long> GetCountAllAsync<ModelT>(int? skip = null, int? take = null)
        {
            using (new UnitOfWork<WebApiDataContextT>())
            {
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;

                return await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.GetCountAllAsync<ModelT>(skip, take);
            }
        }
        #endregion

        #region DataContext Queries
        public override async Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null)
        {
            //if we get here, we need to get the data from web api service
            List<ModelT> masterModels;
            using (new UnitOfWork<WebApiDataContextT>())
            {
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;

                masterModels = await RepoFactory.Create<ModelT>().GetWhereAsync(searchBy, sortBy, skip, take);
            }

            //Now save master models to cache if we have something to save
            if (masterModels.Any())
            {
                using (new UnitOfWork<SqlliteDataContextT>())
                {
                    foreach (var masterModel in masterModels)
                    {
                        ManagedModels.Add(new ManagedModel(masterModel));
                        UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.AddOrUpdate(masterModel);
                    }
                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
                }
            }

            return masterModels;
        }
        public override async Task<long> GetCountWhereAsync<ModelT>(object searchBy, int? skip = null, int? take = null)
        {
            using (new UnitOfWork<WebApiDataContextT>())
            {
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;

                return await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.GetCountWhereAsync<ModelT>(searchBy, skip, take);
            }
        }
        #endregion

        #region DataContext Save Changes
        public override async Task SaveChangesInternalAsync(List<PendingAction> pendingActions)
        {
            //First we delete all objects in cache that are about to be updated -- we do this for transcational integrity
            var actionsToLoopThrough = IsReadOnly ? PendingActions.Where(x => x.IsReadOnlyAction) : PendingActions;
            using (new UnitOfWork<SqlliteDataContextT>())
            {
                // ReSharper disable once PossibleMultipleEnumeration
                foreach (var pendingAction in actionsToLoopThrough)
                {
                    switch (pendingAction.Operation)
                    {
                        case PendingAction.OperationEnum.AddWithExistingId:
                        case PendingAction.OperationEnum.Update:
                        case PendingAction.OperationEnum.Delete:
                        case PendingAction.OperationEnum.AddOrUpdate:
                        case PendingAction.OperationEnum.DelayedGetById:
                        case PendingAction.OperationEnum.DelayedGetByIdOrDefault:
                        {
                            UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("Delete", new[] { pendingAction.ModelType }, pendingAction.ModelId);
                            break;
                        }
                        case PendingAction.OperationEnum.DelayedGetAll:
                        {
                            var sqlliteContext = new SqlliteDataContextT();
                            await sqlliteContext.InitDbAsync();
                            var db = new SQLiteAsyncConnection(sqlliteContext.DatabaseFilePath);
                            var commandText = $@"DELETE FROM {sqlliteContext.DataTableName} WHERE ModelTypeLogicalName == '{GetModelTypeLogicalName(pendingAction.ModelType)}'";
                            await db.ExecuteAsync(commandText);
                            break;
                        }
                        case PendingAction.OperationEnum.GenerateIdAndAdd:
                        case PendingAction.OperationEnum.DelayedGetWhere:
                        case PendingAction.OperationEnum.DelayedGetCountAll:
                        case PendingAction.OperationEnum.DelayedGetCountWhere:
                        {
                            //for these we do nothing
                            break;
                        }
                        default:
                        {
                            throw new SupermodelException("Unsupported Operation");
                        }
                    }
                }
                await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
            }

            //Then we attempt to save to web api service
            using (new UnitOfWork<WebApiDataContextT>())
            {
                if (IsReadOnly) UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;

                await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.SaveChangesInternalAsync(PendingActions);
                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeCompletedAndFinalized();
            }

            //Them if we were successful, we update local db
            using (new UnitOfWork<SqlliteDataContextT>())
            {
                // ReSharper disable once PossibleMultipleEnumeration
                foreach (var pendingAction in actionsToLoopThrough)
                {
                    switch (pendingAction.Operation)
                    {
                        case PendingAction.OperationEnum.AddWithExistingId:
                        case PendingAction.OperationEnum.GenerateIdAndAdd:
                        case PendingAction.OperationEnum.Update:
                        case PendingAction.OperationEnum.AddOrUpdate:
                        {
                            UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("AddOrUpdate", new[] { pendingAction.ModelType }, pendingAction.Model);
                            break;
                        }
                        case PendingAction.OperationEnum.Delete: //we need to delete again becasue we could have read data with delayed read
                        {
                            UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("Delete", new[] { pendingAction.ModelType }, pendingAction.ModelId);
                            break;
                        }
                        case PendingAction.OperationEnum.DelayedGetById:
                        case PendingAction.OperationEnum.DelayedGetByIdOrDefault:
                        {
                            var model = (IModel)pendingAction.DelayedValue.GetValue();
                            if (model != null)
                            {
                                UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("AddOrUpdate", new[] { pendingAction.ModelType }, model);
                            }
                            break;
                        }
                        case PendingAction.OperationEnum.DelayedGetAll:
                        case PendingAction.OperationEnum.DelayedGetWhere:
                        {
                            var models = (IEnumerable<IModel>)pendingAction.DelayedValue.GetValue();
                            foreach (var model in models)
                            {
                                UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("AddOrUpdate", new[] { pendingAction.ModelType }, model);
                            }
                            break;
                        }
                        case PendingAction.OperationEnum.DelayedGetCountAll:
                        case PendingAction.OperationEnum.DelayedGetCountWhere:
                        {
                            //for these we do nothing
                            break;
                        }
                        default:
                        {
                            throw new SupermodelException("Unsupported Operation");
                        }
                    }
                }
                await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
            }
        }
        #endregion

        #region Configuration Properties
        public AuthHeader AuthHeader { get; set; }
        public int CacheAgeToleranceInSeconds { get; set; }
        #endregion
    }
}
