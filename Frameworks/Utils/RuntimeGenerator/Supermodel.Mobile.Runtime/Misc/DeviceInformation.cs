﻿ // ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Misc
{
    #if __IOS__
    using Foundation; 
    using System.IO;
    using System;
    using UIKit;
    using MonoTouch;
    using Security;
    #endif

    #if __ANDROID__
    using Java.Lang;
    using Java.IO;
    using Android.OS;
    using Android.App;
    #endif

    #if WINDOWS_UWP
    using System;
    #endif
    
    public static class DeviceInformation
    {
		public static bool IsRunningOnEmulator()
        {
            // ReSharper disable once ConvertToConstant.Local
            var result = false;

            #if __IOS__
            if (ObjCRuntime.Runtime.Arch == ObjCRuntime.Arch.SIMULATOR) result = true;
            #endif

            #if __ANDROID__
            if (Build.Fingerprint != null && (Build.Fingerprint.Contains("vbox") || Build.Fingerprint.Contains("generic"))) result = true;
            #endif

            #if WINDOWS_UWP
            var deviceInfo = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
            result = (deviceInfo.SystemProductName == "Virtual" && deviceInfo.SystemManufacturer == "Microsoft");
            #endif

            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            return result;
        }
        public static bool? IsJailbroken(bool returnNullIfNotSupported)
		{
            if (IsRunningOnEmulator()) return null; //if we are not running on physical hardware return null (unknown)

            // ReSharper disable once ConvertToConstant.Local
            var result = false;

            #if __IOS__
            var paths = new [] {@"/Applications/Cydia.app", @"/Library/MobileSubstrate/MobileSubstrate.dylib", @"/bin/bash", @"/usr/sbin/sshd", @"/etc/apt" };
            foreach (var path in paths)
            {
                if (NSFileManager.DefaultManager.FileExists(path)) result = true;
            }

            try
            {
                const string filename = @"/private/jailbreak.txt";
                File.WriteAllText(filename, "This is a test.");
                result = true;
                File.Delete(filename);
            }
            // ReSharper disable once EmptyGeneralCatchClause
            catch (Exception){} //if exception is thrown, we are not jailbroken

            if (UIApplication.SharedApplication.CanOpenUrl(new NSUrl("cydia://package/com.exapmle.package"))) result = true;
            #endif

            #if __ANDROID__
            var buildTags = Android.OS.Build.Tags;
            if (buildTags != null && buildTags.Contains("test-keys")) result = true;

            if (new File("/system/app/Superuser.apk").Exists()) result = true;

            var paths = new[] { "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su" };
            foreach(var path in paths) 
            {
                if (new File(path).Exists()) result = true;
            }

            try
            {
                using (var process = Runtime.GetRuntime().Exec(new [] { "/system/xbin/which", "su" }))
                {
                    var br = new BufferedReader(new InputStreamReader(process.InputStream));
                    if (br.ReadLine() != null) result = true;
                }
            }
            catch{}
            #endif

            #if WINDOWS_UWP
            if (returnNullIfNotSupported) return null;
            else throw new NotImplementedException("IsJailbroken() not implemented on Winodws Mobile");
            #endif

		    // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            return result;
		}
        public static bool? IsDeviceSecuredByPassocde(bool returnNullIfNotSupported)
        {
            if (IsRunningOnEmulator()) return null; //if we are not running on physical hardware return null (unknown)

            // ReSharper disable once ConvertToConstant.Local
            var result = false;
            
            #if __IOS__
            const string text = "Supermodel.Mobile Passcode Test";
            var record = new SecRecord(SecKind.GenericPassword) { Generic = NSData.FromString (text), Accessible = SecAccessible.WhenPasscodeSetThisDeviceOnly };
            var status = SecKeyChain.Add(record);
            if (status == SecStatusCode.Success || status == SecStatusCode.DuplicateItem)
            {
                result = true;
                SecKeyChain.Remove(record);
            }
            #endif

            #if __ANDROID__
            var km = (KeyguardManager)Android.App.Application.Context.GetSystemService(Android.Content.Context.KeyguardService);
		    if (km.IsKeyguardSecure) result = true;
            #endif

            #if WINDOWS_UWP
            if (returnNullIfNotSupported) return null;
            else throw new NotImplementedException("IsDeviceSecuredByPassocde() not implemented on Winodws Mobile");
            #endif
            
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            return result;
        }
    }
}
