﻿// ReSharper disable once CheckNamespace
namespace Supermodel.Mobile.Encryptor
{
    using System;
    using System.Text;
    using PCLCrypto;

    public static class HashAgent
    {
		public static string HashByteArray(byte[] data)
		{
            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Sha1);
            return Convert.ToBase64String(hasher.HashData(data));
		}

        public static string GenerateGuidSalt()
        {
            return Guid.NewGuid().ToString();
        }
        public static string Generate256BitSalt()
		{
			return Convert.ToBase64String(GenerateBinary256BitSalt()); 
		}

        public static byte[] GenerateBinaryGuidSalt()
        {
            return Converter.StringToByteArr(GenerateGuidSalt());
        }
        public static byte[] GenerateBinary256BitSalt ()
		{
			return WinRTCrypto.CryptographicBuffer.GenerateRandom(32); 
		}

        public static string Generate5MinTimeStampSalt(DateTime dt, string format = "{0:|yyyy|M|d|H|m}")
        {
            //return $"{RoundUp5Min(dt):|yyyy|M|d|H|m}";
            return string.Format(format, RoundUp5Min(dt));
        }

        public static string HashPasswordSHA1(string password, string salt)
        {
            return HashPassword(password, salt, HashAlgorithm.Sha1);
        }
        public static string HashPasswordSHA1Unicode(string password, string salt)
        {
            return HashPasswordUnicode(password, salt, HashAlgorithm.Sha1);
        }
        public static byte[] HashPasswordSHA1(string password, byte[] salt)
        {
            return HashPassword(password, salt, HashAlgorithm.Sha1);
        }
        public static byte[] HashPasswordSHA1Unicode(string password, byte[] salt)
        {
            return HashPasswordUnicode(password, salt, HashAlgorithm.Sha1);
        }
        
        public static string HashPasswordMD5(string password, string salt)
        {
            return HashPassword(password, salt, HashAlgorithm.Md5);            
        }
        public static string HashPasswordMD5Unicode(string password, string salt)
        {
            return HashPasswordUnicode(password, salt, HashAlgorithm.Md5);
        }
        public static byte[] HashPasswordMD5(string password, byte[] salt)
        {
            return HashPassword(password, salt, HashAlgorithm.Md5);
        }
        public static byte[] HashPasswordMD5Unicode(string password, byte[] salt)
        {
            return HashPasswordUnicode(password, salt, HashAlgorithm.Md5);
        }
        
        public static string HashPasswordSHA256(string password, string salt)
        {
            return HashPassword(password, salt, HashAlgorithm.Sha256);            
        }
        public static string HashPasswordSHA256Unicode(string password, string salt)
        {
            return HashPasswordUnicode(password, salt, HashAlgorithm.Sha256);
        }
        public static byte[] HashPasswordSHA256(string password, byte[] salt)
        {
            return HashPassword(password, salt, HashAlgorithm.Sha256);            
        }
        public static byte[] HashPasswordSHA256Unicode(string password, byte[] salt)
        {
            return HashPasswordUnicode(password, salt, HashAlgorithm.Sha256);
        }

        public static string HashPasswordSHA512(string password, string salt)
        {
            return HashPassword(password, salt, HashAlgorithm.Sha512);            
        }
        public static string HashPasswordSHA512Unicode(string password, string salt)
        {
            return HashPasswordUnicode(password, salt, HashAlgorithm.Sha512);
        }
        public static byte[] HashPasswordSHA512(string password, byte[] salt)
        {
            return HashPassword(password, salt, HashAlgorithm.Sha512);            
        }
        public static byte[] HashPasswordSHA512Unicode(string password, byte[] salt)
        {
            return HashPassword(password, salt, HashAlgorithm.Sha512);            
        }

        private static string HashPassword(string password, string salt, HashAlgorithm hashAlgorithm)
        {
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (salt == null) throw new ArgumentNullException(nameof(salt));

            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(hashAlgorithm);
            return Converter.BinaryToHex(hasher.HashData(Encoding.UTF8.GetBytes(password + salt))); //This breaks old code
        }
        private static string HashPasswordUnicode(string password, string salt, HashAlgorithm hashAlgorithm)
        {
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (salt == null) throw new ArgumentNullException(nameof(salt));

            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(hashAlgorithm);
            return Converter.BinaryToHex(hasher.HashData(Converter.StringToByteArr(password + salt)));
        }
        private static byte[] HashPassword(string password, byte[] salt, HashAlgorithm hashAlgorithm)
        {
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (salt == null) throw new ArgumentNullException(nameof(salt));

            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(hashAlgorithm);
            return hasher.HashData(Encoding.UTF8.GetBytes(password + salt)); //This breaks old code
        }
        private static byte[] HashPasswordUnicode(string password, byte[] salt, HashAlgorithm hashAlgorithm)
        {
            if (password == null) throw new ArgumentNullException(nameof(password));
            if (salt == null) throw new ArgumentNullException(nameof(salt));

            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(hashAlgorithm);
            return hasher.HashData(Converter.StringToByteArr(password + salt));
        }
        
        private static DateTime RoundUp5Min(DateTime dt)
        {
            var d = TimeSpan.FromMinutes(5);
            return new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks);
        }
    }
}
