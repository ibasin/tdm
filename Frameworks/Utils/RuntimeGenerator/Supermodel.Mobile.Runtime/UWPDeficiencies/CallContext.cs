﻿#if WINDOWS_UWP
// ReSharper disable once CheckNamespace
namespace System.Runtime.Remoting.Messaging
{
    using Collections.Generic;
    using Threading;

    public static class CallContext
    {
        #region Methods
		public static object LogicalGetData(string key)
		{
            if (!_logicalContextDictionary.ContainsKey(key)) return null;
		    return _logicalContextDictionary[key];
		}
		public static void LogicalSetData(string key, object value)
		{
		    _logicalContextDictionary[key] = value;
		}
        private static Dictionary<string, object> _logicalContextDictionary
        {
            get
            {
                if (_logicalData.Value == null) _logicalData.Value = new Dictionary<string, object>();
                return _logicalData.Value;
            }
        }
        #endregion

        #region Private Variables
        private static AsyncLocal<Dictionary<string, object>> _logicalData = new AsyncLocal<Dictionary<string, object>>();
        #endregion
    }
}
#endif
