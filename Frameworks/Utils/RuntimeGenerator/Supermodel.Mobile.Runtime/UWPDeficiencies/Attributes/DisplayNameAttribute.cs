﻿#if WINDOWS_UWP

// ReSharper disable once CheckNamespace
namespace System.ComponentModel
{
    using System; 

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Event | AttributeTargets.Class | AttributeTargets.Method)]
    public class DisplayNameAttribute : Attribute
    {
        #region Constructors
        public DisplayNameAttribute() : this (string.Empty) {}
        public DisplayNameAttribute(string displayName)
        {
            _displayName = displayName; 
        }
        #endregion

        #region Methods
        public virtual string DisplayName => DisplayNameValue;

        protected string DisplayNameValue
        {
            get { return _displayName; }
            set { _displayName = value; }
        } 
 
        public override bool Equals(object obj) 
        {
            if (obj == this) return true; 
            var other = obj as DisplayNameAttribute; 
            return (other != null) && other.DisplayName == DisplayName; 
        } 

        public override int GetHashCode()
        { 
            return DisplayName.GetHashCode();
        }

        public bool IsDefaultAttribute()
        { 
            return (Equals(Default));
        } 
        #endregion

        #region Properties
        public static readonly DisplayNameAttribute Default = new DisplayNameAttribute();
        private string _displayName;
        #endregion
    }
}
#endif