﻿#if WINDOWS_UWP
// ReSharper disable once CheckNamespace
namespace System.ComponentModel
{
    using System; 

    [AttributeUsage(AttributeTargets.All)]
    public class DescriptionAttribute : Attribute
    { 
        #region Constructors
        public DescriptionAttribute() : this (string.Empty) {}
        public DescriptionAttribute(string description) { this.description = description;  }
        #endregion
        
        #region Methods
        public virtual string Description => DescriptionValue;

        protected string DescriptionValue
        {
            get { return description; }
            set { description = value; }
        } 
 
        public override bool Equals(object obj)
        {
            if (obj == this)  return true; 
            var other = obj as DescriptionAttribute; 
            return (other != null) && other.Description == Description; 
        } 

        public override int GetHashCode()
        { 
            return Description.GetHashCode();
        }
        #endregion

        #region Properties
        public static readonly DescriptionAttribute Default = new DescriptionAttribute();
        private string description;
        #endregion
    } 
}
#endif