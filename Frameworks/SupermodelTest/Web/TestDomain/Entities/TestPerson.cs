﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.Metadata;
using Supermodel.DDD.Models.View.Mvc.TwitterBS;
using Supermodel.DDD.Models.View.Mvc.UIComponents;
using Supermodel.DDD.Models.View.WebApi;
using JQMobile = Supermodel.DDD.Models.View.Mvc.JQMobile.JQMobile;

namespace TestDomain.Entities
{
    #region DemoPersonTBSMvc
    public class DemoPersonSearchTBSMvcModel : TwitterBS.MvcModel
    {
        public string Name { get; set; }
    }
    public class DemoPersonTBSMvcModel : TwitterBS.MvcModelForEntity<TestPerson>
    {
        public TwitterBS.TextBoxForStringMvcModel Name { get; set; }
        public TwitterBS.TextBoxForDoubleMvcModel Height { get; set; }

        public override string Label => Name.Value;
    }
    #endregion

    #region DemoPersonMobileMvc
    public class DemoPersonSearchJQMMvcModel : JQMobile.MvcModel
    {
        public DemoPersonSearchJQMMvcModel()
        {
            Name = new JQMobile.SearchBoxMvcModel  { HtmlAttributesAsObj = new { Placeholder = "Find By Name" } };
        }
        
        [HideLabel] public JQMobile.SearchBoxMvcModel Name { get; set; }
    }
    public class DemoPersonJQMMvcModel : JQMobile.MvcModelForEntity<TestPerson>
    {
        [Required] public JQMobile.TextBoxForStringMvcModel Name { get; set; }
        public JQMobile.TextBoxForDoubleMvcModel Height { get; set; }
        
        public override string Label { get { return Name.Value; } }
    }
    #endregion

    #region DemoPersonAPI
    public class DemoPersonSearchApiModel : SearchApiModel
    {
        public string Name { get; set; }
        public double? FromHeight { get; set; }
        public double? ToHeight { get; set; }
    }
    public class DemoPersonApiModel : ApiModelForEntity<TestPerson>
    {
        public string Name { get; set; }
        public double? Height { get; set; }
    }
    #endregion

    #region TestPersonApi
    public class TestPersonSearchApiModel : SearchApiModel
    {
        public string Name { get; set; }
        public DateTime? DOB { get; set; }
    }
    
    public class TestPersonApiModel : ApiModelForEntity<TestPerson>
    {
        [Required] public string Name { get; set; }
        public DateTime? DOB { get; set; }
        public int? ApprovalRating { get; set; }
        public double? Height { get; set; }
        public int? FamilySize { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string PIN { get; set; }
        public TestPerson.SexEnum? Sex { get; set; }
        public TestPerson.RaceEnum? Race { get; set; }
        public TestPerson.PoliticalAffiliationEnum? PoliticalAffiliation { get; set; }

        //public BinaryFileApiModel Image { get; set; }

        //public BinaryFile Image { get; set; }
        public bool? RegisteredToVote { get; set; }
        public bool? SecurityClearance { get; set; }
    }
    #endregion

    #region TestPersonTBSMvc
    public class TestPersonSearchTBSMvcModel : TwitterBS.MvcModel, IValidatableObject
    {
        [ScreenOrder(50)] public string Name { get; set; }
        public TwitterBS.DropdownMvcModelUsingEnum<TestPerson.SexEnum> Sex { get; set; }
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var vr = new ValidationResultList();

            if (Name != null && Name.Length > 3) vr.AddValidationResult(this, x => x.Name, "Name too long");

            return vr;
        }
    }
    public class TestPersonTBSMvcModel : TwitterBS.MvcModelForEntity<TestPerson>
    {
        public TestPersonTBSMvcModel()
        {
            Name = new TwitterBS.TextBoxForStringMvcModel { HtmlAttributesAsObj = new { Placeholder = "Enter Name Here" } };
            //NotHookedUpSearch = new JQMobile.SearchBoxMvcModel { HtmlAttributesAsObj = new { Placeholder = "Not Hooked Up Search Here" } };
            PIN = new TwitterBS.TextBoxForPasswordMvcModel();
            //ImageView = new TwitterBS.ImageMvcModel { HtmlAttributesAsObj = new { style = "max-width: 100%; max-height: 200px;" } };
        }

        
        [ListColumn(OrderBy = "Name", OrderByDesc = "-Name"), Required] public TwitterBS.TextBoxForStringMvcModel Name { get; set; }
        [RMapTo(".Image"), DisplayName("Picture")] public TwitterBS.ImageMvcModel ImageView { get; set; }
        public TwitterBS.BinaryFileMvcModel Image { get; set; }

        public TwitterBS.SliderHorizontalForIntMvcModel ApprovalRating { get; set; }
        //public TwitterBS.SliderVerticalForIntMvcModel ApprovalRating { get; set; }
        [DisplayName("Date of Birth")] public TwitterBS.DateMvcModel DOB { get; set; }
        [ListColumn(OrderBy = "Height", OrderByDesc = "-Height")] public TwitterBS.TextBoxForDoubleMvcModel Height { get; set; }
        public TwitterBS.TextBoxForIntMvcModel FamilySize { get; set; }
        [NotRMapped, DisplayName("Secret PIN")] public TwitterBS.TextBoxForPasswordMvcModel PIN { get; set; }
        public TwitterBS.DropdownMvcModelUsing<TestProfesisonMvcModel> Profession { get; set; }
        //public TwitterBS.RadioSelectVerticalMvcModelUsing<TestProfesisonMvcModel> Profession { get; set; }
        //public TwitterBS.RadioSelectHorizontalMvcModelUsing<TestProfesisonMvcModel> Profession { get; set; }
        public TwitterBS.DropdownMvcModelUsingEnum<TestPerson.SexEnum> Sex { get; set; }
        public TwitterBS.RadioSelectVerticalMvcModelUsingEnum<TestPerson.RaceEnum> Race { get; set; }
        public TwitterBS.RadioSelectHorizontalMvcModelUsingEnum<TestPerson.PoliticalAffiliationEnum> PoliticalAffiliation { get; set; } 
        public TwitterBS.CheckboxMvcModel SecurityClearance { get; set; }
        //public TwitterBS.CheckboxesListVerticalMvcModel<TestGroupMvcModel> Groups { get; set; }

        public TwitterBS.CheckboxesListHorizontalMvcModelUsing<TestGroupMvcModel> Groups { get; set; }

        public ICollection<TestChildTBSMvcModel> Children { get; set; }

        public override string Label { get { return Name.Value; } }
    }
    #endregion

    #region TestPersonMobileMvc
    public class TestPersonSearchMobileMvcModel : JQMobile.MvcModel, IValidatableObject
    {
        public string Name { get; set; }
        public JQMobile.DropdownMvcModelUsingEnum<TestPerson.SexEnum> Sex { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var vr = new ValidationResultList();

            if (Name != null && Name.Length > 30) vr.AddValidationResult(this, x => x.Name, "Name too long");

            return vr;
        }
    }
    public class TestPersonMobileMvcModel : JQMobile.MvcModelForEntity<TestPerson>
    {
        public TestPersonMobileMvcModel()
        {
            Name = new JQMobile.TextBoxForStringMvcModel { HtmlAttributesAsObj = new { Placeholder = "Enter Name Here" } };
            NotHookedUpSearch = new JQMobile.SearchBoxMvcModel { HtmlAttributesAsObj = new { Placeholder = "Not Hooked Up Search Here" } };
            PIN = new JQMobile.TextBoxForPasswordMvcModel();
            ImageView = new JQMobile.ImageMvcModel { HtmlAttributesAsObj = new { style = "max-width: 100%; max-height: 200px;" } };
        }

        [NotRMapped, HideLabel] public JQMobile.SearchBoxMvcModel NotHookedUpSearch { get; set; }
        [Required] public JQMobile.TextBoxForStringMvcModel Name { get; set; }
        
        [RMapTo(".Image"), DisplayName("Picture")] public JQMobile.ImageMvcModel ImageView { get; set; }
        [RMapTo(".Image")] public JQMobile.BinaryFileMvcModel ImageFile { get; set; }

        public JQMobile.SliderForIntMvcModel ApprovalRating { get; set; }
        [DisplayName("Date of Birth")] public JQMobile.DateMvcModel DOB { get; set; }
        public JQMobile.TextBoxForDoubleMvcModel Height { get; set; }
        public JQMobile.TextBoxForIntMvcModel FamilySize { get; set; }
        public JQMobile.TextBoxForTelephoneMvcModel Phone { get; set; }
        public JQMobile.TextBoxForEmailMvcModel Email { get; set; }
        public JQMobile.TextBoxForUrlMvcModel Url { get; set; }
        [NotRMapped, DisplayName("Secret PIN")] public JQMobile.TextBoxForPasswordMvcModel PIN { get; set; }
        //public JQMobile.DropdownMvcModelUsing<TestProfesisonMvcModel> Profession { get; set; }
        public JQMobile.RadioSelectVerticalMvcModelUsing<TestProfesisonMvcModel> Profession { get; set; }
        //public JQMobile.RadioSelectHorizontalMvcModelUsing<TestProfesisonMvcModel> Profession { get; set; }
        public JQMobile.DropdownMvcModelUsingEnum<TestPerson.SexEnum> Sex { get; set; }
        public JQMobile.RadioSelectVerticalMvcModelUsingEnum<TestPerson.RaceEnum> Race { get; set; }
        public JQMobile.RadioSelectHorizontalMvcModelUsingEnum<TestPerson.PoliticalAffiliationEnum> PoliticalAffiliation { get; set; } 
        public JQMobile.ToggleSwitchMvcModel RegisteredToVote { get; set; }
        public JQMobile.CheckboxMvcModel SecurityClearance { get; set; }
        public JQMobile.CheckboxesListVerticalMvcModelUsing<TestGroupMvcModel> Groups { get; set; }
        //public JQMobile.CheckboxesListHorizontalMvcModel<TestGroupMvcModel> Groups { get; set; } 

        public ICollection<TestChildMobileMvcModel> Children { get; set; }
        
        public override string Label { get { return Name.Value; } }
    }
    #endregion

    #region TestPersonMvc
    public class TestPersonSearchMvcModel : MvcModel, IValidatableObject
    {
        public string Name { get; set; }
        public DropdownMvcModelUsingEnum<TestPerson.SexEnum> Sex { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var vr = new ValidationResultList();

            if (Name != null && Name.Length > 3) vr.AddValidationResult(this, x => x.Name, "Name too long");

            return vr;
        }
    }
    public class TestPersonMvcModel : MvcModelForEntity<TestPerson>
    {
        [ListColumn(OrderBy = "Name", OrderByDesc = "-Name"), Required] public string Name { get; set; }
        [ListColumn(Header = "Date of Birth", OrderBy = "DOB", OrderByDesc = "-DOB")] public DateMvcModel DOB { get; set; }
        // /*[Required]*/ public DropdownMvcModelUsingEnum<TestPerson.SexEnum> Sex { get; set; }
        //public RadioSelectFormModelUsingEnum<TestPerson.RaceEnum> Race { get; set; }
        // /*[Required]*/ public DropdownMvcModelUsing<TestProfesisonMvcModel> Profession { get; set; } 
        // /*[Required]*/ public CheckboxesListMvcModelUsing<TestGroupMvcModel> Groups { get; set; }
        
        //public BinaryFileMvcModel Image { get; set; }

        public ICollection<TestChildMvcModel> Children { get; set; }

        public override string Label { get { return Name; } }
    }
    #endregion

    #region Entity
    public class TestPerson : Entity
    {
        public TestPerson()
        {
            Image = new BinaryFile();
        }
        
        public enum RaceEnum { [Disabled] White, Black, Asian };
        public enum SexEnum { Man, Woman };
        public enum PoliticalAffiliationEnum { Democrat, Republican };

        [Required] public string Name { get; set; }
        public DateTime? DOB { get; set; }
        public int? ApprovalRating { get; set; }
        public double? Height { get; set; }
        public int? FamilySize { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }
        public string PIN { get; set; }
        public SexEnum? Sex { get; set; }
        public RaceEnum? Race { get; set; }
        public PoliticalAffiliationEnum? PoliticalAffiliation { get; set; }

        public BinaryFile Image { get; set; }
        public bool? RegisteredToVote { get; set; }
        public bool? SecurityClearance { get; set; }
        
        public virtual TestProfession Profession { get; set; }

        public virtual ICollection<TestGroup> Groups { get; set; }
        public virtual ICollection<TestChild> Children { get; set; }

        protected override void DeleteInternal()
        {
            foreach (var child in Children.ToList()) child.Delete();
            base.DeleteInternal();
        }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var vr = (ValidationResultList)base.Validate(validationContext) ?? new ValidationResultList();
            if (Name.Length > 30) vr.AddValidationResult(this, x => x.Name, "Name is longer than 30 characters");
            if (Height > 2.5) vr.AddValidationResult(this, x => x.Height, "Height cannot exceed 2.5 meters");
            return vr;
        }
    }
    #endregion
}
