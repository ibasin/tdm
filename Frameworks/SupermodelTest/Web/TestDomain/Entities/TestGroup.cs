﻿using System.Collections.Generic;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.TwitterBS;
using JQMobile = Supermodel.DDD.Models.View.Mvc.JQMobile.JQMobile;

namespace TestDomain.Entities
{
    public class TestGroupTBSMvcModel : TwitterBS.MvcModelForEntity<TestGroup>
    {
        public TwitterBS.TextBoxForStringMvcModel Name { get; set; }
        public TwitterBS.TextAreaMvcModel Description { get; set; }

        public override string Label { get { return Name.Value; } }
    }
    
    public class TestGroupMobileMvcModel : JQMobile.MvcModelForEntity<TestGroup>
    {
        public JQMobile.TextBoxForStringMvcModel Name { get; set; }
        public JQMobile.TextAreaMvcModel Description { get; set; }

        public override string Label { get { return Name.Value; } }
    }
    
    public class TestGroupMvcModel : MvcModelForEntity<TestGroup>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        //public BinaryFileFormModel Certificate { get; set; }
        public override string Label
        {
            get { return Name; }
        }

        public override bool IsDisabled
        {
            get
            {
                if (Name == "Group 1") return true;
                else return false;
            }
        }
    }

    public class TestGroup : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        //public BinaryFile Certificate { get; set; }

        public virtual ICollection<TestPerson> People { get; set; }
    }
}
