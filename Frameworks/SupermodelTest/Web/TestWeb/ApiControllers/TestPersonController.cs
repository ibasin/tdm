﻿using System.Linq;
using System.Web.Http;
using TestDomain.Entities;
using TestDomain.Supermodel;

namespace TestWeb.ApiControllers
{
    [Authorize]
    public class TestPersonController : TestEnhancedApiCRUDController<TestPerson, TestPersonApiModel, TestPersonSearchApiModel>
    {
        protected override IQueryable<TestPerson> ApplySearchBy(IQueryable<TestPerson> items, TestPersonSearchApiModel searchBy)
        {
            if (searchBy.Name != null) items = items.Where(x => x.Name.Contains(searchBy.Name));
            return items;
        }
    }
}
