﻿using System.Linq;
using Supermodel.MvcAndWebApi.Controllers.MvcControllers.Sync;
using TestDomain.Entities;
using TestDomain.Supermodel;

namespace TestWeb.Controllers
{
    public class JQMDemoPersonController : SyncEnhancedMvcCRUDController<TestPerson, DemoPersonJQMMvcModel, DemoPersonSearchJQMMvcModel, TestDbContext>
    {
        protected override IQueryable<TestPerson> ApplySearchBy(IQueryable<TestPerson> items, DemoPersonSearchJQMMvcModel searchBy)
        {
            if (!string.IsNullOrEmpty(searchBy.Name.Value)) items = items.Where(x => x.Name.Contains(searchBy.Name.Value));
            return base.ApplySearchBy(items, searchBy);
        }
    }
}