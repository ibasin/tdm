﻿using System.Linq;
using System.Web.Mvc;
using Supermodel.Extensions;
using Supermodel.MvcAndWebApi;
using TestDomain.Entities;
using TestDomain.Supermodel;

namespace TestWeb.Controllers
{
    public class TestGroupsController : TestCRUDController<TestGroup, TestGroupMvcModel> { }
    public class TestPeopleController : TestPageableSortableSearchableCRUDController<TestPerson, TestPersonMvcModel, TestPersonSearchMvcModel>
    {
        protected override IQueryable<TestPerson> ApplySearchBy(IQueryable<TestPerson> items, TestPersonSearchMvcModel searchBy)
        {
            if (searchBy.Name != null) items = items.Where(x => x.Name.Contains(searchBy.Name));
            if (searchBy.Sex?.SelectedEnum != null) items = items.Where(x => x.Sex == searchBy.Sex.SelectedEnum);
            return items;
        }
    }
    public class TestChildrenController : TestChildCRUDController<TestChild, TestPerson, TestChildMvcModel>
    {
        public override ActionResult List(long? parentId)
        {
            var routeValues = HttpContext.Request.QueryString.Supermodel().ToRouteValueDictionary().Supermodel().AddOrUpdateWith("id", parentId).Supermodel().RemoveKey("parentId");
            return this.Supermodel().RedirectToActionStrong<TestPeopleController>(x => x.Detail(parentId.Value, new HttpGet()), routeValues);
        }
    }
}
