﻿using System.Linq;
using Supermodel.MvcAndWebApi.Controllers.MvcControllers.Sync;
using TestDomain.Entities;
using TestDomain.Supermodel;

namespace TestWeb.Controllers
{
    public class TBSDemoPersonController : SyncEnhancedMvcCRUDController<TestPerson, DemoPersonTBSMvcModel, DemoPersonSearchTBSMvcModel, TestDbContext>
    {
        protected override IQueryable<TestPerson> ApplySearchBy(IQueryable<TestPerson> items, DemoPersonSearchTBSMvcModel searchBy)
        {
            if (!string.IsNullOrEmpty(searchBy.Name)) items = items.Where(x => x.Name.Contains(searchBy.Name));
            return base.ApplySearchBy(items, searchBy);
        }
    }
}