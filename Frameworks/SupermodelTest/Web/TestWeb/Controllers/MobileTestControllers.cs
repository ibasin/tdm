﻿using System.Linq;
using System.Web.Mvc;
using Supermodel.DDD.UnitOfWork;
using Supermodel.Extensions;
using Supermodel.MvcAndWebApi;
using TestDomain.Entities;
using TestDomain.Supermodel;

namespace TestWeb.Controllers
{
    public class MobileTestPeopleController : TestPageableSortableSearchableCRUDController<TestPerson, TestPersonMobileMvcModel, TestPersonSearchMobileMvcModel>
    {
        protected override ActionResult AfterCreate(long id, TestPerson entityItem, TestPersonMobileMvcModel mvcModelItem)
        {
            UnitOfWorkContext<TestDbContext>.CurrentDbContext.SaveChanges();
            TempData.Supermodel().NextPageModalMessage = "Person created successfully!";
            var routeValues = HttpContext.Request.QueryString.Supermodel().ToRouteValueDictionary();
            return this.Supermodel().RedirectToActionStrong(x => x.Detail(entityItem.Id, new HttpGet()), routeValues);
        }

        protected override IQueryable<TestPerson> ApplySearchBy(IQueryable<TestPerson> items, TestPersonSearchMobileMvcModel searchBy)
        {
            if (searchBy.Name != null) items = items.Where(x => x.Name.Contains(searchBy.Name));
            if (searchBy.Sex != null && searchBy.Sex.SelectedEnum != null) items = items.Where(x => x.Sex == searchBy.Sex.SelectedEnum);
            return items;
        }
    }
    public class MobileTestChildrenController : TestChildCRUDController<TestChild, TestPerson, TestChildMobileMvcModel>
    {
        public override ActionResult List(long? parentId)
        {
            var routeValues = HttpContext.Request.QueryString.Supermodel().ToRouteValueDictionary().Supermodel().AddOrUpdateWith("id", parentId).Supermodel().RemoveKey("parentId");
            return this.Supermodel().RedirectToActionStrong<MobileTestPeopleController>(x => x.Detail(parentId.Value, new HttpGet()), routeValues);
        }
    }

    public class MobileTestGroupsController : TestCRUDController<TestGroup, TestGroupMobileMvcModel>{}
}
