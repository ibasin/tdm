﻿using System;
using Supermodel.Mobile.Xamarin;
using TestWeb.ApiControllers;

namespace XamarinModelGenerator
{
    public class Program
    {
        static void Main(string[] args)
        {
            var modelGenerator = new ModelGenerator(typeof(TestPersonController).Assembly);
            modelGenerator.GenerateModelsAndSaveToDisk();
            Console.WriteLine("All done! Press enter...");
            Console.ReadLine();
        }
    }
}
