﻿using System;

namespace ReflectionMapper
{
    public interface IRMapperCustom
    {
        //must also have a default constructor!
        object MapFromObjectCustom(object obj, Type objType);
        object MapToObjectCustom(object obj, Type objType);    
    }
}
