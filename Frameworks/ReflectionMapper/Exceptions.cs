﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ReflectionMapper
{
    public class ReflectionMapperException : Exception
    {
        public ReflectionMapperException() { }
        public ReflectionMapperException(string msg) : base(msg) { }
    }

    public class ReflectionPropertyCantBeInvoked : ReflectionMapperException
    {
        public ReflectionPropertyCantBeInvoked(Type type, string propertyName)
            : base($"Property '{propertyName}' does not exist in Type '{type.Name}'") { }
    }

    public class ReflectionMethodCantBeInvoked : ReflectionMapperException
    {
        public ReflectionMethodCantBeInvoked(Type type, string methodName)
            : base($"Method '{methodName}' does not exist in Type '{type.Name}'") { }
    }    
    
    public class PropertyCantBeAutomappedException : ReflectionMapperException
    {
        public PropertyCantBeAutomappedException(string msg) : base(msg) { }
    }

    public class ValidationResultException : ReflectionMapperException
    {
        public ValidationResultException(string errorMessage, IEnumerable<string> memberNames)
        {
            ValidationResultList = new ValidationResultList { new ValidationResult(errorMessage, memberNames) };
        }

        public ValidationResultException(ValidationResultList validationResultList)
        {
            ValidationResultList = validationResultList;
        }

        public ValidationResultException(string errorMessage) : this(errorMessage, new List<string> {" "}) { }
                
        public ValidationResultList ValidationResultList { get; protected set; }
    }
}
