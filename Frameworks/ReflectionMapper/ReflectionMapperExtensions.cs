﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.ComponentModel.DataAnnotations;

namespace ReflectionMapper
{
    public static class ReflectionMapperExtensions
    {
        public static MeT MapFrom<MeT, ObjT>(this MeT me, ObjT obj)
        {
            return (MeT)me.MapFromObject(obj);
        }
        public static ObjT MapTo<MeT, ObjT>(this MeT me, ObjT obj)
        {
            return (ObjT)me.MapToObject(obj);
        }
        public static object MapFromObject(this object me, object obj)
        {
            //first check if me implements IRMapperCustomMapper, if it does we use that. If custom mapper does not exist, we go property by property by calling the base method
            if (me is IRMapperCustom) me = (me as IRMapperCustom).MapFromObjectCustom(obj, obj.GetType());
            else me = me.MapFromObjectCustomBase(obj);
            return me;
        }
        public static object MapToObject(this object me, object obj)
        {
            //first check if me implements ICustomMapper, if it does we use that. If custom mapper does not exist, we go property by property by calling the base method
            if (me is IRMapperCustom) obj = (me as IRMapperCustom).MapToObjectCustom(obj, obj.GetType());
            else obj = me.MapToObjectCustomBase(obj);
            return obj;
        }

        public static object MapFromObjectCustomBase(this object me, object obj)
        {
            //if obj == null, we set me to null too, the caller must assign the result of this method
            if (obj == null) return null;

            //if me is null, report error
            if (me == null) throw new ReflectionMapperException("MapFromObjectCustomBase(): me is null");

            //ICollection<ICustomeMapper>
            if (AreCompatibleCollections(me.GetType(), obj.GetType()))
            {
                var objIEnumerable = (IEnumerable)obj;

                var myIEnumerableGenericArg = me.GetType().GetInterfaces().Single(x => x.Name == typeof(IEnumerable<>).Name).GetGenericArguments()[0];

                foreach (var objItemObj in objIEnumerable)
                {
                    me.ExecuteMethod("Add", objItemObj != null ? ReflectionHelper.CreateType(myIEnumerableGenericArg).ExecuteMethod("MapFromObjectCustom", objItemObj, objItemObj.GetType()) : null);
                }
                return me;
            }
            
            foreach (var property in me.GetType().GetProperties())
            {
                //If property is marked NotRMappedAttribute, don't worry about this one
                //if (MyAttribute.GetCustomAttribute(property, typeof(NotRMappedAttribute), true) != null || MyAttribute.GetCustomAttribute(property, typeof(NotRMappedFromAttribute), true) != null) continue;
                if (property.GetCustomAttribute(typeof(NotRMappedAttribute), true) != null || property.GetCustomAttribute(typeof(NotRMappedFromAttribute), true) != null) continue;

                //Find matching properties
                var myProperty = property;
                var objPropertyMeta = GetMatchingProperty(me, myProperty, obj);

                //ICustomMapper
                if (typeof(IRMapperCustom).IsAssignableFrom(myProperty.PropertyType))
                {
                    try
                    {
                        //Get the property we are mapping to
                        var objPropertyObj = objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name);

                        var myPropertyObj = me.PropertyGet(myProperty.Name);
                        if (myPropertyObj == null)
                        {
                            //we create blank object for the property in question
                            myPropertyObj = !myProperty.PropertyType.GetTypeInfo().IsGenericType ? ReflectionHelper.CreateType(myProperty.PropertyType) : ReflectionHelper.CreateGenericType(myProperty.PropertyType.GetGenericTypeDefinition(), myProperty.PropertyType.GetGenericArguments());
                        }

                        //then we ask that property to map itself to the matching property of the object
                        // ReSharper disable PossibleNullReferenceException
                        (myPropertyObj as IRMapperCustom).MapFromObjectCustom(objPropertyObj, objPropertyMeta.PropertyInfo.PropertyType);
                        // ReSharper restore PossibleNullReferenceException

                        me.PropertySet(myProperty.Name, myPropertyObj);
                    }
                    catch (ValidationResultException ex)
                    {
                        var vr = new ValidationResultList();
                        foreach (var validationResult in ex.ValidationResultList)
                        {
		                    if (validationResult.MemberNames.Any(x => !string.IsNullOrWhiteSpace(x))) vr.Add(validationResult);
		                    else vr.Add(new ValidationResult(validationResult.ErrorMessage, new [] { property.Name }));
                        }
                        throw new ValidationResultException(vr);
                    }
                    catch (Exception ex)
                    {
                        throw new PropertyCantBeAutomappedException($"Property '{myProperty.Name}' of class '{me.GetType().Name}' can't be automapped to type '{obj.GetType().Name}' property '{objPropertyMeta.PropertyInfo.Name}' because ICustomMapper implementation threw an exception: {ex.Message}.");
                    }
                }

                //ICollection<ICustomMapper>
                else if (AreCompatibleCollections(myProperty.PropertyType, objPropertyMeta.PropertyInfo.PropertyType))
                {
                    var objIEnumerable = (IEnumerable)objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name);

                    var myIEnumerableGenericArg = myProperty.PropertyType.GetTypeInfo().ImplementedInterfaces.Single(x => x.GetTypeInfo().IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)).GetGenericArguments()[0];

                    if (objIEnumerable != null)
                    {
                        if (me.PropertyGet(myProperty.Name) == null)
                        {
                            me.PropertySet(myProperty.Name, ReflectionHelper.CreateGenericType(objIEnumerable.GetType().GetGenericTypeDefinition(), myIEnumerableGenericArg));
                        }
                        else
                        {
                            me.PropertyGet(myProperty.Name).ExecuteMethod("Clear");
                        }

                        foreach (var objItemObj in objIEnumerable)
                        {
                            me.PropertyGet(myProperty.Name).ExecuteMethod("Add", objItemObj != null ? ReflectionHelper.CreateType(myIEnumerableGenericArg).ExecuteMethod("MapFromObjectCustom", objItemObj, objItemObj.GetType()) : null);
                        }
                    }
                }

                //This is the default case of when the types match exactly
                else if (myProperty.PropertyType == objPropertyMeta.PropertyInfo.PropertyType)
                {
                    me.PropertySet(myProperty.Name, objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name), true);
                }

                //If all fails
                else
                {
                    throw new PropertyCantBeAutomappedException($"Property '{myProperty.Name}' of class '{me.GetType().Name}' can't be automapped to type '{obj.GetType().Name}' property '{objPropertyMeta.PropertyInfo.Name}' because their types are incompatible.");
                }
            }
            return me;
        }
        public static object MapToObjectCustomBase(this object me, object obj)
        {
            //if me == null, we set obj to null too, the caller must assign the result of this method
            if (me == null) return null;

            //if me is null, report error
            if (obj == null) throw new ReflectionMapperException("MapToObjectCustomBase(): obj is null");

            //ICollection<ICustomeMapper>
            if (AreCompatibleCollections(me.GetType(), obj.GetType()))
            {
                var myICollection = (ICollection)me;

                var objICollectionGenericArg = obj.GetType().GetTypeInfo().ImplementedInterfaces.Single(x => x.GetTypeInfo().IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)).GetGenericArguments()[0];

                foreach (var myItemObj in myICollection)
                {
                    obj.ExecuteMethod("Add", myItemObj != null ? myItemObj.ExecuteMethod("MapToObjectCustom", ReflectionHelper.CreateType(objICollectionGenericArg), objICollectionGenericArg) : null);
                }
                return obj;
            }
            
            foreach (var property in me.GetType().GetProperties())
            {
                //If property is marked NotRMappedAttribute, don't worry about this one
                //if (MyAttribute.GetCustomAttribute(property, typeof(NotRMappedAttribute), true) != null || MyAttribute.GetCustomAttribute(property, typeof(NotRMappedToAttribute), true) != null) continue;
                if (property.GetCustomAttribute(typeof(NotRMappedAttribute), true) != null || property.GetCustomAttribute(typeof(NotRMappedToAttribute), true) != null) continue;

                //Find matching properties
                var myProperty = property;
                var objPropertyMeta = GetMatchingProperty(me, myProperty, obj);

                //ICustomMapper
                if (typeof(IRMapperCustom).IsAssignableFrom(myProperty.PropertyType))
                {
                    try
                    {
                        // ReSharper disable PossibleNullReferenceException
                        var propertyValue = (me.PropertyGet(myProperty.Name) as IRMapperCustom).MapToObjectCustom(objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name), objPropertyMeta.PropertyInfo.PropertyType);
                        // ReSharper restore PossibleNullReferenceException
                        objPropertyMeta.Obj.PropertySet(objPropertyMeta.PropertyInfo.Name, propertyValue);
                    }
                    catch (ValidationResultException ex)
                    {
                        var vr = new ValidationResultList();
                        foreach (var validationResult in ex.ValidationResultList)
                        {
		                    if (validationResult.MemberNames.Any(x => !string.IsNullOrWhiteSpace(x))) vr.Add(validationResult);
		                    else vr.Add(new ValidationResult(validationResult.ErrorMessage, new [] { property.Name }));
                        }
                        throw new ValidationResultException(vr);
                    }
                    catch (Exception ex)
                    {
                        throw new PropertyCantBeAutomappedException($"Property '{myProperty.Name}' of class '{me.GetType().Name}' can't be automapped to type '{obj.GetType().Name}' property '{objPropertyMeta.PropertyInfo.Name}' because ICsutomMapper threw an exception: {ex.Message}.");
                    }
                }

                //IEnumerable<ICustomMapper>
                else if (AreCompatibleCollections(myProperty.PropertyType, objPropertyMeta.PropertyInfo.PropertyType))
                {
                    var myIEnumerable = (IEnumerable)me.PropertyGet(myProperty.Name);
                    var objIEnumerableGenericArg = objPropertyMeta.PropertyInfo.PropertyType.GetTypeInfo().ImplementedInterfaces.Single(x => x.GetTypeInfo().IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)).GetGenericArguments()[0];


                    if (myIEnumerable != null)
                    {
                        if (objPropertyMeta.Obj.PropertyGet(myProperty.Name) == null)
                        {
                            objPropertyMeta.Obj.PropertySet(objPropertyMeta.PropertyInfo.Name, ReflectionHelper.CreateGenericType(myIEnumerable.GetType().GetGenericTypeDefinition(), objIEnumerableGenericArg));
                        }
                        else
                        {
                            objPropertyMeta.Obj.PropertyGet(myProperty.Name).ExecuteMethod("Clear");
                        }
                        
                        foreach (var myItemObj in myIEnumerable)
                        {
                            objPropertyMeta.Obj.PropertyGet(myProperty.Name).ExecuteMethod("Add", (myItemObj != null) ? myItemObj.ExecuteMethod("MapToObjectCustom", ReflectionHelper.CreateType(objIEnumerableGenericArg), objIEnumerableGenericArg) : null);
                        }
                    }
                }

                //This is the default case of when the types match exactly
                else if (myProperty.PropertyType == objPropertyMeta.PropertyInfo.PropertyType)
                {
                    objPropertyMeta.Obj.PropertySet(objPropertyMeta.PropertyInfo.Name, me.PropertyGet(myProperty.Name), true);
                }

                //If all fails
                else
                {
                    throw new PropertyCantBeAutomappedException($"Property '{myProperty.Name}' of class '{me.GetType().Name}' can't be automapped to type '{obj.GetType().Name}' property '{objPropertyMeta.PropertyInfo.Name}' because their types are incompatible.");
                }
            }

            return obj;
        }
        
        public static bool IsEqualToObject(this object me, object obj)
        {
            //Handle nulls
            if (me == null && obj == null) return true;
            if (me == null || obj == null) return false;

            //We only compare identical types or when obj is derived from me (this is needed for EF support)
            if (!ReflectionHelper.IsClassADerivedFromClassB(obj.GetType(), me.GetType())) throw new ArgumentException($"Cannot compare incompatible types {me.GetType().Name} and {obj.GetType().Name}");

            //IEnumerables
            // ReSharper disable CanBeReplacedWithTryCastAndCheckForNull
            if (me is IEnumerable)
            // ReSharper restore CanBeReplacedWithTryCastAndCheckForNull
            {
                var objIEnumerable = (IEnumerable)obj;
                var myIEnumerable = (IEnumerable)me;

                var objIEnumerableEnumerator = objIEnumerable.GetEnumerator();
                var myIEnumerableEnumerator = myIEnumerable.GetEnumerator();

                while (true)
                {
                    var objEnumeratorDone = !objIEnumerableEnumerator.MoveNext();
                    var myEnumeratorDone = !myIEnumerableEnumerator.MoveNext();

                    //if both are done and we have not found a difference, return true
                    if (objEnumeratorDone && myEnumeratorDone) return true;

                    //If both are not done but one is done, enumerators are of diffrenet length, return false
                    if (objEnumeratorDone || myEnumeratorDone) return false;

                    var objItem = objIEnumerableEnumerator.Current;
                    var myItem = myIEnumerableEnumerator.Current;

                    if (!myItem.IsEqualToObject(objItem)) return false;
                }
            }

            //Go through all properties that are both readable and writable
            foreach (var property in me.GetType().GetProperties().Where(x => x.CanRead && x.CanWrite))
            {
                //If property is marked NotRComparedAttribute, don't worry about this one
                if (property.GetCustomAttribute(typeof(NotRComparedAttribute), true) != null) continue;

                //Find matching properties
                var myProperty = property;
                var objProperty = obj.GetType().GetProperty(myProperty.Name);

                //Get the property values
                var objPropertyObj = obj.PropertyGet(objProperty.Name);
                var myPropertyObj = me.PropertyGet(myProperty.Name);

                //Handle nulls
                if (myPropertyObj == null)
                {
                    if (objPropertyObj == null) continue;
                    else return false;
                }

                //IComparer
                var myPropertyObjAsComparable = myPropertyObj as IComparable;
                if (myPropertyObjAsComparable != null)
                {
                    try
                    {
                        if (myPropertyObjAsComparable.CompareTo(objPropertyObj) != 0) return false;
                    }
                    catch (Exception ex)
                    {
                        throw new ArgumentException($"Property '{myProperty.Name}' of type '{me.GetType().Name}' can't be compared to type '{obj.GetType().Name}' property '{objProperty.Name}' because IComparable implementation threw an exception: {ex.Message}.");
                    }
                }
                
                //Everything else
                else
                {
                    try
                    {
                        if (!myPropertyObj.IsEqualToObject(objPropertyObj)) return false;
                    }
                    catch (Exception ex)
                    {
                        throw new ArgumentException($"Property '{myProperty.Name}' of type '{me.GetType().Name}' can't be compared to type '{obj.GetType().Name}' property '{objProperty.Name}' because IsEqualToObject() method threw an exception: {ex.Message}.");
                    }
                }
            }
            return true;
        }

        #region Private Helper Methods
		private static bool AreCompatibleCollections(Type activeType, Type passiveType)
		{
		    //both types are generic
		    //if (!activeType.IsGenericType) return false;
		    if (!activeType.GetTypeInfo().IsGenericType) return false;
		    //if (!passiveType.IsGenericType) return false;
		    if (!passiveType.GetTypeInfo().IsGenericType) return false;

		    //get generic type defintions
		    var activeTypeGenericDefinition = activeType.GetGenericTypeDefinition();
		    var passiveTypeGenericDefinition = passiveType.GetGenericTypeDefinition();
		
		    //make sure the types are the same except for the generic parameter
		    if (activeTypeGenericDefinition != passiveTypeGenericDefinition) return false;
		
			//set up ICollection inetrafce in a variable
			//var activeICollectionInterface = activeType.GetInterface(typeof(ICollection<>).Name);
			var activeICollectionInterface = activeType.GetTypeInfo().ImplementedInterfaces.SingleOrDefault(x => x.GetTypeInfo().IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>));
			if (activeICollectionInterface == null && activeTypeGenericDefinition == typeof(ICollection<>)) activeICollectionInterface = activeType;
						
			//var passiveICollectionInterface = passiveType.GetInterface(typeof(ICollection<>).Name);
			var passiveICollectionInterface = passiveType.GetTypeInfo().ImplementedInterfaces.SingleOrDefault(x => x.GetTypeInfo().IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>));
			if (passiveICollectionInterface == null && passiveTypeGenericDefinition == typeof(ICollection<>)) passiveICollectionInterface = passiveType;
		            
		    //both types are ICollection
		    if (activeICollectionInterface == null) return false;
		    if (passiveICollectionInterface == null) return false;
		
		    //both are generic types (this might be generic but just in case)
		    //if (!activeICollectionInterface.IsGenericType) return false;
		    if (!activeICollectionInterface.GetTypeInfo().IsGenericType) return false;
		    //if (!passiveICollectionInterface.IsGenericType) return false;
		    if (!passiveICollectionInterface.GetTypeInfo().IsGenericType) return false;

		    //my active generic ICollection<> type argument implements ICustomMapper
		    if (!typeof(IRMapperCustom).IsAssignableFrom(activeICollectionInterface.GetGenericArguments()[0])) return false;
		
		    return true;
		}
        private static ReflectionMapperPropertyMetadata GetMatchingProperty(object me, PropertyInfo myProperty, object obj)
        {
            // ReSharper disable PossibleMultipleEnumeration
            // ReSharper disable AccessToForEachVariableInClosure

            var myPropertyName = myProperty.Name;
            var parentObj = obj;

            //var myReflectionMappedToAttribute = MyAttribute.GetCustomAttribute(myProperty, typeof(RMapToAttribute), true);
            var myReflectionMappedToAttribute = myProperty.GetCustomAttribute(typeof(RMapToAttribute), true);
            if (myReflectionMappedToAttribute != null)
            {
                var attrPropertyName = ((RMapToAttribute)myReflectionMappedToAttribute).PropertyName;
                if (!string.IsNullOrEmpty(attrPropertyName)) myPropertyName = attrPropertyName;
                
                var attrObjectPath = ((RMapToAttribute) myReflectionMappedToAttribute).ObjectPath;
                if (!string.IsNullOrEmpty(attrObjectPath))
                {
                    var mappedToNameComponents = attrObjectPath.Split('.');
                    if (mappedToNameComponents.Length < 1) throw new ReflectionMapperException("Invalid path in ReflectionMappedTo Attribute");

                    foreach (var subPropertyName in mappedToNameComponents)
                    {
                        var subObjProperties = parentObj.GetType().GetProperties().Where(x => x.Name == subPropertyName);
                        if (subObjProperties.Count() != 1) throw new ReflectionMapperException("Invalid path in ReflectionMappedTo Attribute");
                        parentObj = parentObj.PropertyGet(subObjProperties.Single().Name);
                        if (parentObj == null) throw new ReflectionMapperException($"{subPropertyName} in ReflectionMappedTo Attribute path is null");
                    }
                }
            }
            
            var objProperties = parentObj.GetType().GetProperties().Where(x => x.Name == myPropertyName);
            if (objProperties.Count() != 1) throw new PropertyCantBeAutomappedException(string.Format("Property '{0}' of class '{1}' can't be automapped to type '{2}' because '{3}' property does not exist in type '{2}'.", myProperty.Name, me.GetType().Name, parentObj.GetType().Name, myPropertyName));
            var propertyInfo = objProperties.Single();

            return new ReflectionMapperPropertyMetadata { Obj = parentObj, PropertyInfo = propertyInfo };

            // ReSharper restore AccessToForEachVariableInClosure
            // ReSharper restore PossibleMultipleEnumeration
        }
        #endregion
    }
}
