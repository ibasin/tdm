using System.Collections.Generic;
using System.Threading.Tasks;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.UIComponents;

namespace Supermodel.DDD.Repository
{
    public interface IDataRepoGenericTypeIgnorant
    {
        IEntity GetIEntityById(long id);
        Task<IEntity> GetIEntityByIdAsync(long id);

        IEnumerable<IEntity> GetIEntityAll();
        Task<IEnumerable<IEntity>> GetIEntityAllAsync();

        List<SingleSelectMvcModel.Option> GetDropdownOptions<MvcModelT>() where MvcModelT : MvcModelForEntityCore;
        Task<List<SingleSelectMvcModel.Option>> GetDropdownOptionsAsync<MvcModelT>() where MvcModelT : MvcModelForEntityCore;

        List<MultiSelectMvcModelCore.Option> GetMultiSelectOptions<MvcModelT>() where MvcModelT : MvcModelForEntityCore;
        Task<List<MultiSelectMvcModelCore.Option>> GetMultiSelectOptionsAsync<MvcModelT>() where MvcModelT : MvcModelForEntityCore;
    }
}