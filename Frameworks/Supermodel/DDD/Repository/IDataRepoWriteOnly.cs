using Supermodel.DDD.Models.Domain;

namespace Supermodel.DDD.Repository
{
    public interface IDataRepoWriteOnly<in EntityT> where EntityT : class, IEntity, new()
    {
        void Add(EntityT item);
        
        void Delete(EntityT item);
    }
}