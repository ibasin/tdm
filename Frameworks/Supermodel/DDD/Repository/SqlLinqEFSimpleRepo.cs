﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.UIComponents;
using Supermodel.DDD.UnitOfWork;

namespace Supermodel.DDD.Repository 
{
    public class SqlLinqEFSimpleRepo<EntityT> : ISqlLinqEFDataRepo<EntityT> where EntityT : class, IEntity, new()
    {
		#region Properties
        public virtual DbSet<EntityT> DbSet => (DbSet<EntityT>)UnitOfWorkContextCore.CurrentDbContext.Items<EntityT>();
        public virtual IQueryable<EntityT> Items
        {
            get
            {
                //this should improve performance
                if (UnitOfWorkContextCore.CurrentDbContext.IsReadOnly) return DbSet.AsNoTracking();
                return DbSet;
            }
        }
        #endregion

		#region Methods
        public virtual void Add(EntityT item)
        {
            if (item.Id == 0) DbSet.Add(item);
        }
        
        public virtual void Delete(EntityT item)
        {
            DbSet.Remove(item);
        }

        public virtual IEntity GetIEntityById(long id)
        {
            return GetById(id);
        }
        public virtual async Task<IEntity> GetIEntityByIdAsync(long id)
        {
            return await GetByIdAsync(id);
        }

        public virtual EntityT GetById(long id)
        {
            var item = GetByIdOrDefault(id);
            if (item == null) throw new SupermodelSystemErrorException("GetById can't find an entity");
            return item;
        }
        public virtual async Task<EntityT> GetByIdAsync(long id)
        {
            var item = await GetByIdOrDefaultAsync(id);
            if (item == null) throw new SupermodelSystemErrorException("GetById can't find an entity");
            return item;
        }

        public virtual EntityT GetByIdOrDefault(long id)
        {
            return Items.SingleOrDefault(p => p.Id == id);
        }
        public virtual async Task<EntityT> GetByIdOrDefaultAsync(long id)
        {
            return await Items.SingleOrDefaultAsync(p => p.Id == id);
        }

        public virtual IEnumerable<IEntity> GetIEntityAll()
        {
            return GetAll();
        }
        public virtual async Task<IEnumerable<IEntity>> GetIEntityAllAsync()
        {
            return await GetAllAsync();
        }

        public virtual List<EntityT> GetAll()
        {
            return Items.ToList();
        }
        public virtual async Task<List<EntityT>> GetAllAsync()
        {
            return await Items.ToListAsync();
        }

        public virtual List<SingleSelectMvcModel.Option> GetDropdownOptions<MvcModelT>() where MvcModelT : MvcModelForEntityCore
        {
            var entities = GetAll();
            return GetDropdownOptions<MvcModelT>(entities);
        }
        public virtual async Task<List<SingleSelectMvcModel.Option>> GetDropdownOptionsAsync<MvcModelT>() where MvcModelT : MvcModelForEntityCore
        {
            var entities = await GetAllAsync();
            return GetDropdownOptions<MvcModelT>(entities);
        }

        public virtual List<MultiSelectMvcModelCore.Option> GetMultiSelectOptions<MvcModelT>() where MvcModelT : MvcModelForEntityCore
        {
            var entities = GetAll();
            return GetMultiSelectOptions<MvcModelT>(entities);
        }
        public virtual async Task<List<MultiSelectMvcModelCore.Option>> GetMultiSelectOptionsAsync<MvcModelT>() where MvcModelT : MvcModelForEntityCore
        {
            var entities = await GetAllAsync();
            return GetMultiSelectOptions<MvcModelT>(entities);
        }

        public virtual List<MultiSelectMvcModelCore.Option> GetMultiSelectOptions<MvcModelT>(IEnumerable<EntityT> entities) where MvcModelT : MvcModelForEntityCore
        {
            var myTypeName = GetType().FullName ?? throw new SystemException("SingleSelectMvcModelUsing<TMvcModel>.GetDropdownOptionsAsync: GetType().FullName is null");
            if (!UnitOfWorkContext.CustomValues.ContainsKey(myTypeName))
            {
                var mvcModels = new List<MvcModelT>();
                mvcModels = (List<MvcModelT>)mvcModels.MapFromObject(entities.ToList());
                mvcModels = mvcModels.OrderBy(p => p.Label).ToList();
                UnitOfWorkContext.CustomValues[myTypeName] = !mvcModels.Any() ?
                    new List<MultiSelectMvcModelCore.Option>() :
                    mvcModels.Select(item => new MultiSelectMvcModelCore.Option(item.Id.ToString(CultureInfo.InvariantCulture), item.Label, item.IsDisabled)).ToList();
                return (List<MultiSelectMvcModelCore.Option>)UnitOfWorkContext.CustomValues[myTypeName];
            }
            
            //Create deep copy
            var listOfOptions = ((List<MultiSelectMvcModelCore.Option>)UnitOfWorkContext.CustomValues[myTypeName]).Select(x => new MultiSelectMvcModelCore.Option(x.Value, x.Label, x.IsDisabled, x.Selected)).ToList();
            return listOfOptions;
        }
        public virtual List<SingleSelectMvcModel.Option> GetDropdownOptions<MvcModelT>(IEnumerable<EntityT> entities) where MvcModelT : MvcModelForEntityCore
        {
            var myTypeName = GetType().FullName ?? throw new SystemException("SingleSelectMvcModelUsing<TMvcModel>.GetDropdownOptionsAsync: GetType().FullName is null");
            if (!UnitOfWorkContext.CustomValues.ContainsKey(myTypeName))
            {
                var mvcModels = new List<MvcModelT>();
                mvcModels = (List<MvcModelT>)mvcModels.MapFromObject(entities.ToList());
                mvcModels = mvcModels.OrderBy(p => p.Label).ToList();
                UnitOfWorkContext.CustomValues[myTypeName] = !mvcModels.Any() ?
                    new List<SingleSelectMvcModel.Option>() :
                    mvcModels.Select(item => new SingleSelectMvcModel.Option(item.Id.ToString(CultureInfo.InvariantCulture), item.Label, item.IsDisabled)).ToList();
            }
            return (List<SingleSelectMvcModel.Option>)UnitOfWorkContext.CustomValues[myTypeName];
        }
        #endregion
    }
}
