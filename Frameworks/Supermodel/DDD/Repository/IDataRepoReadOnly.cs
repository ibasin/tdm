using System.Collections.Generic;
using System.Threading.Tasks;
using Supermodel.DDD.Models.Domain;

namespace Supermodel.DDD.Repository
{
    public interface IDataRepoReadOnly<EntityT> where EntityT : class, IEntity, new()
    {
        EntityT GetById(long id);
        Task<EntityT> GetByIdAsync(long id);

        EntityT GetByIdOrDefault(long id);
        Task<EntityT> GetByIdOrDefaultAsync(long id);

        List<EntityT> GetAll();
        Task<List<EntityT>> GetAllAsync();
    }
}