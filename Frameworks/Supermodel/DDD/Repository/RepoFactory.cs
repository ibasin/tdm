﻿using System;
using Supermodel.DDD.Models.Domain;
using ReflectionMapper;
using Supermodel.DDD.UnitOfWork;

namespace Supermodel.DDD.Repository
{
    public static class RepoFactory
    {
        public static IDataRepo<EntityT> Create<EntityT>() where EntityT : class, IEntity, new()
        {
            return UnitOfWorkContextCore.CurrentDbContext.CreateRepo<EntityT>();
        }
        public static object CreateForRuntimeType(Type modelType)
        {
            return ReflectionHelper.ExecuteStaticGenericMethod(typeof(RepoFactory), "Create", new[] { modelType });
        }
    }
}
