﻿using System;

namespace Supermodel.DDD.UnitOfWork
{
    public class UnitOfWork<ContextT> : IDisposable where ContextT : class, IDbContext, new()
    {
        #region Constructors
        public UnitOfWork(ReadOnly readOnly = ReadOnly.No)
        {
            Context = new ContextT();
            if (readOnly == ReadOnly.Yes) Context.MakeReadOnly();
            UnitOfWorkContext<ContextT>.PushDbContext(Context);
        }
        #endregion

        #region IDisposable implemetation
        public void Dispose()
        {
            Context.Dispose();

            var context = UnitOfWorkContext<ContextT>.PopDbContext();

            // ReSharper disable PossibleUnintendedReferenceComparison
            if (context != Context) throw new SupermodelSystemErrorException("UnitOfWork: POP on Dispose popped mismatched DB Context.");
            // ReSharper restore PossibleUnintendedReferenceComparison
        }
        #endregion

        #region Properties
        public ContextT Context { get; private set; }
        #endregion
    }
}
