﻿using System;

namespace Supermodel.DDD.UnitOfWork
{
    public class UnitOfWorkIfNoAmbientContext<DbContextT> : IDisposable where DbContextT : class, IDbContext, new()
    {
        public DbContextT DbContext { get; private set; }

        public UnitOfWorkIfNoAmbientContext(MustBeWritable mustBeWritable)
        {
            bool? createNewContext;
            var mustBeWritableBool = (mustBeWritable == MustBeWritable.Yes);

            if (UnitOfWorkContextCore.StackCount > 0 && UnitOfWorkContextCore.CurrentDbContext.GetType() == typeof(DbContextT))
            {
                var ambientContextReadOnlyBool = UnitOfWorkContext<DbContextT>.CurrentDbContext.IsReadOnly;
                if (mustBeWritableBool && ambientContextReadOnlyBool) createNewContext = true;
                else createNewContext = false;
            }
            else
            {
                createNewContext = true;
            }

            if ((bool)createNewContext)
            {
                DbContext = new DbContextT();
                if (!mustBeWritableBool) DbContext.MakeReadOnly();
                UnitOfWorkContext<DbContextT>.PushDbContext(DbContext);
            }
            else
            {
                DbContext = null;
            }
        }

        public void Dispose()
        {
            if (DbContext == null) return;

            DbContext.Dispose();
            var dbContext = UnitOfWorkContext<DbContextT>.PopDbContext();
            // ReSharper disable PossibleUnintendedReferenceComparison
            if (dbContext != DbContext) throw new SupermodelSystemErrorException("UnitOfWork: POP on Dispose popped mismatched DB Context.");
            // ReSharper restore PossibleUnintendedReferenceComparison
        }
    }
}
