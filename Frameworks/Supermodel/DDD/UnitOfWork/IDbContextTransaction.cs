﻿using System;

namespace Supermodel.DDD.UnitOfWork
{
    public interface IDbContextTransaction : IDisposable
    {
        void CommitIfApplies();
        void RollbackIfApplies();
    }
}
