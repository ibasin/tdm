﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Threading.Tasks;

namespace Supermodel.DDD.UnitOfWork
{
    public static class UnitOfWorkContext
    {
        #region Methods and Properties
        public static Dictionary<string, object> CustomValues { get {  return UnitOfWorkContextCore.CurrentDbContext.CustomValues;  } }

        public static void SaveChanges()
        {
            UnitOfWorkContextCore.CurrentDbContext.SaveChanges();
        }
        public static Task SaveChangesAsync()
        {
            return UnitOfWorkContextCore.CurrentDbContext.SaveChangesAsync();
        }
        public static void FinalSaveChanges()
        {
            UnitOfWorkContextCore.CurrentDbContext.FinalSaveChanges();
        }
        public static Task FinalSaveChangesAsync()
        {
            return UnitOfWorkContextCore.CurrentDbContext.FinalSaveChangesAsync();
        }
        public static bool CommitOnDispose
        {
            get { return UnitOfWorkContextCore.CurrentDbContext.CommitOnDispose; }
            set { UnitOfWorkContextCore.CurrentDbContext.CommitOnDispose = value; }
        }
        //public static ContextT GetCurrentDbContext<ContextT>() where ContextT : EFDbContext
        //{
        //    return (ContextT)UnitOfWorkContextCore.CurrentDbContext;
        //}
        #endregion
    }
    
    public static class UnitOfWorkContext<ContextT> where ContextT : IDbContext
    {
        #region Methods and Properties
        public static ContextT PopDbContext()
        {
            return (ContextT)UnitOfWorkContextCore.PopDbContext();
        }
        public static void PushDbContext(ContextT context)
        {
            UnitOfWorkContextCore.PushDbContext(context);
        }
        public static int StackCount
        {
            get { return UnitOfWorkContextCore.StackCount; }
        }
        public static ContextT CurrentDbContext
        {
            get { return (ContextT)UnitOfWorkContextCore.CurrentDbContext; }
        }
        public static bool HasDbContext()
        {
            return StackCount > 0;
        }
        #endregion
    }

    public static class UnitOfWorkContextCore
    {
        #region Methods and Properties
        public static IDbContext PopDbContext()
        {
            try
            {
                IDbContext context;
                _contextStackImmutable = _contextStackImmutable.Pop(out context);
                return context;

            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException("Stack is empty");
            }
        }
        public static void PushDbContext(IDbContext context)
        {
            _contextStackImmutable = _contextStackImmutable.Push(context);
        }
        public static int StackCount
        {
            get { return _contextStackImmutable.Count(); }
        }
        public static IDbContext CurrentDbContext
        {
            get
            {
                try
                {
                    return _contextStackImmutable.Peek();
                }
                catch (InvalidOperationException)
                {
                    throw new SupermodelSystemErrorException("Current UnitOfWork does not exist. All database access oprations must be wrapped in 'using(new UnitOfWork())'");                    throw;
                }
            }
        }
        #endregion

        #region Private variables
        private sealed class StackWrapper : MarshalByRefObject
        {
            public ImmutableStack<IDbContext> Value { get; set; }
        }
        
        // ReSharper disable once InconsistentNaming
        private static ImmutableStack<IDbContext> _contextStackImmutable
        {
            get
            {
                var contextStackWrapper = CallContext.LogicalGetData("SupermodelDbContextStack") as StackWrapper;
                return contextStackWrapper == null ? ImmutableStack.Create<IDbContext>() : contextStackWrapper.Value;
            }
            set
            {
                CallContext.LogicalSetData("SupermodelDbContextStack", new StackWrapper { Value = value });
            }
        }
        #endregion
    }
}
