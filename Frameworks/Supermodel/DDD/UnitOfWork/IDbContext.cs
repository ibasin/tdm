﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Repository;

namespace Supermodel.DDD.UnitOfWork
{
    public interface IDbContext : IDisposable
    {
        #region Methods
        void InitStorageIfApplies();

        int FinalSaveChanges();
        Task<int> FinalSaveChangesAsync();

        int SaveChanges();
        Task<int> SaveChangesAsync();

        IQueryable<EntityT> Items<EntityT>() where EntityT : class, IEntity, new();
        IDataRepo<EntityT> CreateRepo<EntityT>() where EntityT : class, IEntity, new();

        IDbContextTransaction BeginTransactionIfApplies();
        #endregion

        #region Properties
        bool CommitOnDispose { get; set; }
        bool IsReadOnly { get; }
        void MakeReadOnly();
        bool IsCompletedAndFinalized { get; }
        void MakeCompletedAndFinalized();
        Dictionary<string, object> CustomValues { get; }
        #endregion
    }
}
