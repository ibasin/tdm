﻿using System;
using System.Data.Entity;

namespace Supermodel.DDD.UnitOfWork
{
    public class EFDbContextTransaction : IDbContextTransaction
    {
        #region Constructors
        public EFDbContextTransaction(DbContextTransaction transaction)
        {
            WrappedTransaction = transaction;
        }
        #endregion

        #region Methods
        public void Dispose()
        {
            WrappedTransaction.Dispose();
        }

        public void CommitIfApplies()
        {
            WrappedTransaction.Commit();
        }

        public void RollbackIfApplies()
        {
            WrappedTransaction.Rollback();
        }
        #endregion

        #region Properties
        protected DbContextTransaction WrappedTransaction { get; set; }
        #endregion
    }
}
