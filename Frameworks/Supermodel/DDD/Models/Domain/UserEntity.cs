﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Encryptor;
using ReflectionMapper;
using Supermodel.DDD.Repository;
using Supermodel.DDD.UnitOfWork;

namespace Supermodel.DDD.Models.Domain
{
    public abstract class UserEntity<UserEntityT, DbContextT> : Entity 
        where DbContextT : class, IDbContext, new()
        where UserEntityT : UserEntity<UserEntityT, DbContextT>, new()
    {
        #region Overrdies
        public virtual string HashPassword(string password, string hash)
        {
            return HashAgent.HashPasswordSHA1(password, hash);
        }
        #endregion
        
        #region Methods
        public bool PasswordEquals(string password)
        {
            if (password == null) return false;
            return HashPassword(password, PasswordHashSalt) == PasswordHashValue;
        }
        [NotMapped] public string Password
        {
            set
            {
                PasswordHashSalt = HashAgent.GenerateGuidSalt();
                PasswordHashValue = HashPassword(value, PasswordHashSalt);
            }
        }

        public static long? GetCurrentUserId(HttpContextBase httpContextBase)
        {
            return GetCurrentUserId(httpContextBase.ApplicationInstance.Context);
        }
        public static long? GetCurrentUserId(HttpContext httpContext)
        {
            var userIdStr = httpContext?.User?.Identity.Name;
            if (String.IsNullOrEmpty(userIdStr)) return null;
            return Int32.Parse(userIdStr);
        }

        public static UserEntityT GetCurrentUser(HttpContextBase httpContextBase)
        {
            return GetCurrentUser(httpContextBase.ApplicationInstance.Context);
        }
        public static async Task<UserEntityT> GetCurrentUserAsync(HttpContextBase httpContextBase)
        {
            return await GetCurrentUserAsync(httpContextBase.ApplicationInstance.Context);
        }

        public static UserEntityT GetCurrentUser(HttpContext httpContext)
        {
            using(new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.No))
            {
                var repo = RepoFactory.Create<UserEntityT>();
                var currentUserId = GetCurrentUserId(httpContext);
                if (currentUserId == null) return null;
                return repo.GetById((long)currentUserId);
            }
        }
        public static async Task<UserEntityT> GetCurrentUserAsync(HttpContext httpContext)
        {
            using(new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.No))
            {
                var repo = RepoFactory.Create<UserEntityT>();
                var currentUserId = GetCurrentUserId(httpContext);
                if (currentUserId == null) return null;
                return await repo.GetByIdAsync((long)currentUserId);
            }
        }
        #endregion

        #region Validation
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            // ReSharper disable once ConstantNullCoalescingCondition
            var vr = (ValidationResultList)base.Validate(validationContext) ?? new ValidationResultList();
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                var repo = (ILinqDataRepo<UserEntityT>)RepoFactory.Create<UserEntityT>();
                if (repo.Items.Any(u => u.Username == Username && u.Id != Id)) vr.AddValidationResult(this, x => x.Username, "User with this Username already exists");
            }
            return vr;
        }
        #endregion
        
        #region Properties
        [Required, MaxLength(100), Index("IDX_Username", IsUnique = true)] public string Username { get; set; }
        [Required] public string PasswordHashValue { get; set; }
        [Required] public string PasswordHashSalt { get; set; }
        #endregion
     }
}
