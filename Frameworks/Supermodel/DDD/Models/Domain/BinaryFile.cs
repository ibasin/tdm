﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.IO;
using System.Linq;

using Newtonsoft.Json;

namespace Supermodel.DDD.Models.Domain
{
    public class BinaryFile : IComparable
    {
        #region Methods
        public int CompareTo(object obj)
        {
            var typedObj = (BinaryFile)obj;
            if (Name == null) return 0; //if we are an empty object, we say it equals b/c then we do not override db value
            var result = string.CompareOrdinal(Name, typedObj.Name);
            if (result != 0) return result;
            return BinaryContent.GetHashCode().CompareTo(typedObj.GetHashCode());
        }
        public override bool Equals(object obj)
        {
            return this.Equals(obj as BinaryFile);
        }
        public bool Equals(BinaryFile other)
        {
            return other != null && (Name == null && other.Name == null ||
                    Name != null && Name.Equals(other.Name)) && (BinaryContent == null && other.BinaryContent == null ||
                    BinaryContent != null && BinaryContent.SequenceEqual(other.BinaryContent));
        }
        public void Empty()
        {
            Name = null;
            BinaryContent = null;
        }
        #endregion

        #region Overrides
        public override int GetHashCode()
        {
            // http://stackoverflow.com/a/263416/39396
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;
                if (Name != null) hash = hash * 23 + Name.GetHashCode();
                if (BinaryContent != null) hash = hash * 23 + BinaryContent.GetHashCode();
                return hash;
            }
        }
        public override string ToString()
        {
            return Name;
        }
        #endregion

        #region Properties
        public string Name { get; set; }
        [JsonIgnore, NotMapped] public string Extension => Path.GetExtension(Name);
        [JsonIgnore, NotMapped] public string FileNameWithoutExtension => Path.GetFileNameWithoutExtension(Name);
        public byte[] BinaryContent { get; set; }
        [JsonIgnore, NotMapped] public bool IsEmpty => string.IsNullOrEmpty(Name) && (BinaryContent == null || BinaryContent.Length == 0);
        #endregion
    }

    public class BinaryFileTypeMapping : ComplexTypeConfiguration<BinaryFile>
    {
        public BinaryFileTypeMapping()
        {
            Property(o => o.Name);
            Property(o => o.BinaryContent);
        }
    }
}
