﻿using System.ComponentModel.DataAnnotations;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View
{
    interface IViewModelForEntity : IValidatableObject, IRMapperCustom
    {
        long Id { get; set; }
    }
}
