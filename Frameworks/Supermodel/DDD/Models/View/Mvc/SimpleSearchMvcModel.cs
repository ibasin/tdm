﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ReflectionMapper;
using Supermodel.DDD.Models.View.Mvc.UIComponents;

namespace Supermodel.DDD.Models.View.Mvc
{
    public class SimpleSearchMvcModel : MvcModel, IValidatableObject 
    {
        #region Validation
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new ValidationResultList();
        }
        #endregion

        #region Properties
        public TextBoxForStringMvcModel SearchTerm { get; set; } = new TextBoxForStringMvcModel();
        #endregion
    }
}
