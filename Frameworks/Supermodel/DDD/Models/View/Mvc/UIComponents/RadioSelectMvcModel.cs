﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class RadioSelectMvcModel : SingleSelectMvcModel
    {
        #region ISupermodelEditorTemplate implemtation
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return RadioSelectCommonEditorTemplate(html, UlHtmlAttributesAsDict, LiHtmlAttributesAsDict, InputHtmlAttributesAsDict);
        }

        public static MvcHtmlString RadioSelectCommonEditorTemplate(HtmlHelper html, IDictionary<string, object> ulHtmlAttributesAsDict, IDictionary<string, object> liHtmlAttributesAsDict, IDictionary<string, object> inputHtmlAttributesAsDict)
        {
            if (html.ViewData.Model == null) throw new NullReferenceException("Html.RadioSelectFormModelEditor() is called for a model that is null");
            if (!(html.ViewData.Model is SingleSelectMvcModel)) throw new InvalidCastException("Html.RadioSelectFormModelEditor() is called for a model of type diffrent from RadioSelectFormModel.");

            var radio = (SingleSelectMvcModel)html.ViewData.Model;
            var result = new StringBuilder();

            result.AppendLine("<ul " + UtilsLib.GenerateAttributesString(ulHtmlAttributesAsDict) + ">");

            foreach (var option in radio.Options)
            {
                var isSelectedOption = (radio.SelectedValue != null && string.CompareOrdinal(radio.SelectedValue, option.Value) == 0);
                if (isSelectedOption || !option.IsDisabled)
                {
                    result.AppendLine("<li " + UtilsLib.GenerateAttributesString(liHtmlAttributesAsDict) + ">");
                    result.AppendLine(html.RadioButton("", option.Value, (radio.SelectedValue == option.Value), inputHtmlAttributesAsDict) + (!option.IsDisabled ? option.Label : option.Label + " [DISABLED]"));
                    result.AppendLine("</li>");
                }
            }
            result.AppendLine("</ul>");
            result.AppendLine(string.Format("<input id='{0}' name='{0}' type='hidden' value=''>", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName("")));
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region Properties
        public object UlHtmlAttributesAsObj { set => UlHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
        public IDictionary<string, object> UlHtmlAttributesAsDict { get; set; }

        public object LiHtmlAttributesAsObj { set => LiHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
        public IDictionary<string, object> LiHtmlAttributesAsDict { get; set; }

        public object InputHtmlAttributesAsObj { set => InputHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
        public IDictionary<string, object> InputHtmlAttributesAsDict { get; set; }
        #endregion
    }
}
