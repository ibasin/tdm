﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class RadioSelectMvcModelForEntity : SingleSelectMvcModelForEntity
    {
        #region ISupermodelEditorTemplate implementation
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
        {
            return RadioSelectMvcModel.RadioSelectCommonEditorTemplate(html, UlHtmlAttributesAsDict, LiHtmlAttributesAsDict, InputHtmlAttributesAsDict);
        }
        #endregion

        #region Properties
        public object UlHtmlAttributesAsObj { set { UlHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> UlHtmlAttributesAsDict { get; set; }

        public object LiHtmlAttributesAsObj { set { LiHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> LiHtmlAttributesAsDict { get; set; }

        public object InputHtmlAttributesAsObj { set { InputHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> InputHtmlAttributesAsDict { get; set; }
        #endregion
    }
}
