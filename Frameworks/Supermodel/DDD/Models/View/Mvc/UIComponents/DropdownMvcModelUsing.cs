using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class DropdownMvcModelUsing<MvcModelT> : SingleSelectMvcModelUsing<MvcModelT> where MvcModelT : MvcModelForEntityCore
    {
        #region ISupermodelEditorTemplate implementation
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
        {
            return DropdownMvcModel.DropdownCommonEditorTemplate(html, HtmlAttributesAsDict);
        }
        #endregion

        #region Properties
        public object HtmlAttributesAsObj { set { HtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> HtmlAttributesAsDict { get; set; }
        #endregion
    }
}