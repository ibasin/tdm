﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Supermodel.Extensions;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class SliderForDoubleMvcModel : UIComponents.TextBoxForDoubleMvcModel
    {
        #region Contructors
        public SliderForDoubleMvcModel()
        {
            Min = 0;
            Max = 1;
            Step = 0.01;
        }
        #endregion

        #region ISupermodelEditorTemplate implemetation
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                
            htmlAttributes.Add("type", "range");
            htmlAttributes.Add("Min", Min);
            htmlAttributes.Add("Max", Max);
            htmlAttributes.Add("Step", Step);

            return html.TextBox("", GetStringValue(), htmlAttributes);
        }
        #endregion

        #region ISupermodelDisplayTemplate implementation
        public override MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return EditorTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute).Supermodel().DisableAllControls();
        }
        #endregion

        #region Properties
        public double Min { get; set; }
        public double Max { get; set; }
        public double Step { get; set; }
        #endregion
    }
}
