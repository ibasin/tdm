﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class RadioSelectMvcModelUsingEnum<EnumT> : SingleSelectMvcModelUsingEnum<EnumT> where EnumT : struct, IConvertible
    {
        #region Constructors
        public RadioSelectMvcModelUsingEnum(){}

        public RadioSelectMvcModelUsingEnum(EnumT selectedEnum) : this()
        {
            SelectedEnum = selectedEnum;
        }
        #endregion

        #region ISupermodelEditorTemplate implementation
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return RadioSelectMvcModel.RadioSelectCommonEditorTemplate(html, UlHtmlAttributesAsDict, LiHtmlAttributesAsDict, InputHtmlAttributesAsDict);
        }
        #endregion

        #region Properties
        public object UlHtmlAttributesAsObj { set { UlHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> UlHtmlAttributesAsDict { get; set; }

        public object LiHtmlAttributesAsObj { set { LiHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> LiHtmlAttributesAsDict { get; set; }

        public object InputHtmlAttributesAsObj { set { InputHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> InputHtmlAttributesAsDict { get; set; }
        #endregion    
    }
}
