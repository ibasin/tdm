﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using ReflectionMapper;
using Supermodel.DDD.Models.View.WebApi;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class BinaryFileMvcModel : BinaryFileApiModel, ISupermodelMvcModelBinder, ISupermodelEditorTemplate, ISupermodelDisplayTemplate, ISupermodelHiddenTemplate
    {
        #region Properties
        public object HtmlAttributesAsObj { set => HtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
        public IDictionary<string, object> HtmlAttributesAsDict { get; set; }
        #endregion

        #region IModelBinder implementation
        public virtual object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext == null) throw new ArgumentNullException(nameof(controllerContext));
            if (bindingContext == null) throw new ArgumentNullException(nameof(bindingContext));

            var rawFile = controllerContext.HttpContext.Request.Files[bindingContext.ModelName];

            var file = (BinaryFileMvcModel)ReflectionHelper.CreateType(GetType());
            if (rawFile == null || rawFile.ContentLength == 0 || string.IsNullOrEmpty(rawFile.FileName))
            {
                var originalValue = (BinaryFileMvcModel)bindingContext.Model;
                if (originalValue.IsEmpty)
                {
                    file.Name = null;
                    file.BinaryContent = null;
                    if (bindingContext.ModelMetadata.IsRequired) bindingContext.ModelState.AddModelError(bindingContext.ModelName, $"The field {bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName} is required");
                }
                else
                {
                    file = originalValue;
                }
            }
            else
            {
                file.Name = Path.GetFileName(rawFile.FileName);
                file.BinaryContent = new byte[rawFile.ContentLength];
                rawFile.InputStream.Read(file.BinaryContent, 0, rawFile.ContentLength);
            }
            return file;
        }
        #endregion

        #region ISupermodelEditorTemplate implemtation
        public virtual MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            if (!(html.ViewData.Model is BinaryFileMvcModel)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type diffrent from BinaryFileFormModel.");
            var model = ((BinaryFileMvcModel)html.ViewData.Model);

            var fileInputName = html.ViewContext.ViewData.TemplateInfo.HtmlFieldPrefix;
            var fileInputId = fileInputName.Replace(".", "_");

            var result = new StringBuilder();
            result.AppendLine(string.Format("<input type=\"file\" name=\"{0}\" id=\"{1}\" " + UtilsLib.GenerateAttributesString(HtmlAttributesAsDict) + "/>", fileInputName, fileInputId));

            if (!string.IsNullOrEmpty(model.Name) && html.ViewDataContainer.ViewData.ModelState.IsValid)
            {
                var route = new RouteValueDictionary
                {
                    {"id", html.ViewContext.RouteData.Values["id"]},
                    {"parentId",HttpContext.Current.Request.QueryString["parentId"] },
                    {"pn", html.ViewData.ModelMetadata.PropertyName }
                };
                
                // ReSharper disable Mvc.ActionNotResolved
                result.AppendLine("<br />" + html.ActionLink(model.Name, "GetBinaryFile", route));
                // ReSharper restore Mvc.ActionNotResolved

                if (!html.ViewData.ModelMetadata.IsRequired)
                {
                    // ReSharper disable Mvc.ActionNotResolved
                    result.AppendLine("<br />" + html.ActionLink("Delete File", "DeleteBinaryFile", route, new Dictionary<string, object> { { "data-sm-ConfirmMsg", "This will permanently delete the file. Are you sure?" } }));
                    // ReSharper restore Mvc.ActionNotResolved
                }
            }
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region ISupermodelDisplayTemplate implementation
        public virtual MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
        {
            if (!(html.ViewData.Model is BinaryFileMvcModel)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type diffrent from BinaryFileFormModel.");
            var model = ((BinaryFileMvcModel)html.ViewData.Model);

            var result = new StringBuilder();
            if (!string.IsNullOrEmpty(model.Name) && html.ViewDataContainer.ViewData.ModelState.IsValid)
            {
                var route = new { id = html.ViewContext.RouteData.Values["id"], parentId = HttpContext.Current.Request.QueryString["parentId"], pn = html.ViewData.ModelMetadata.PropertyName };
                
                // ReSharper disable Mvc.ActionNotResolved
                result.AppendLine(html.ActionLink(model.Name, "GetBinaryFile", route).ToString());
                // ReSharper restore Mvc.ActionNotResolved
            }
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region ISupermodelHiddenTemplate implemetation
        public virtual MvcHtmlString HiddenTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
        {
            throw new SupermodelSystemErrorException("BinaryFileMvcModel cannot be used as hidden.");
        }
        #endregion
    }
}
