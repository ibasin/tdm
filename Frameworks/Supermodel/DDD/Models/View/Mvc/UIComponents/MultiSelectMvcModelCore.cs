using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Supermodel.Extensions;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public abstract class MultiSelectMvcModelCore : IComparable, ISupermodelMvcModelBinder, ISupermodelEditorTemplate, ISupermodelDisplayTemplate, ISupermodelHiddenTemplate
    {
        #region Nested Option class
        public class Option
        {
            public Option(string value, string label, bool isDisabled) : this(value, label, isDisabled, false) { }
            public Option(string value, string label, bool isDisabled, bool selected)
            {
                Value = value;
                Label = label;
                IsDisabled = isDisabled;
                Selected = selected;
            }
            public string Value { get; private set; }
            public string Label { get; private set; }
            public bool IsDisabled { get; private set; }
            public bool Selected { get; set; }
            public bool IsShown { get { return Selected || !IsDisabled; } }
        }
        #endregion 

        #region Properties
        public List<Option> Options = new List<Option>();
        #endregion

        #region Methods
        public List<SelectListItem> GetSelectListItemList()
        {
            var selectListItemList = new List<SelectListItem>();
            foreach (var option in Options)
            {
                if (option.IsShown)
                {
                    var item = new SelectListItem { Value = option.Value, Text = !option.IsDisabled ? option.Label : option.Label + " [DISABLED]", Selected = option.Selected };
                    selectListItemList.Add(item);
                }
            }
            return selectListItemList;
        }
        #endregion

        #region IComparable implemtation
        public int CompareTo(object obj)
        {
            var other = (MultiSelectMvcModelCore)obj;
            if (Options.Count != other.Options.Count) return 1;

            foreach (var option in Options)
            {
                // ReSharper disable AccessToModifiedClosure
                if (other.Options.Find(x => x.Value == option.Value && x.Label == option.Label && x.Selected == option.Selected) == null) return 1;
                // ReSharper restore AccessToModifiedClosure
            }
            return 0;
        }
        #endregion

        #region ISupermodelEditorTemplate implementation
        public abstract MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null);
        #endregion

        #region ISupermodelDisplayTemplate implemntation
        public virtual MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return EditorTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute).Supermodel().DisableAllControls();
        }
        #endregion

        #region ISupermodelHiddenTemplate implemntation
        public virtual MvcHtmlString HiddenTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            //if (html.ViewData.ModelState.IsValid) html.ViewData.ModelState.Clear(); //if model is valid, we want to grab stuff from the model -- not model state

            var sb = new StringBuilder();
            foreach (var option in Options)
            {
                if (option.Selected) sb.Append(html.Hidden("", option.Value));
            }
            sb.Append(html.Hidden("", "")); //blank option
            return MvcHtmlString.Create(sb.ToString());
        }
        #endregion

        #region ISupermodelModelBinder implemtation
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (controllerContext == null) throw new ArgumentNullException(nameof(controllerContext));
            if (bindingContext == null) throw new ArgumentNullException(nameof(bindingContext));

            var key = bindingContext.ModelName;
            var val = bindingContext.ValueProvider.GetValue(key);
            string attemptedValue;
            if (val == null || string.IsNullOrEmpty(val.AttemptedValue))
            {
                if (bindingContext.ModelMetadata.IsRequired) bindingContext.ModelState.AddModelError(key, $"The field {bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName} is required");
                attemptedValue = "";
            }
            else
            {
                attemptedValue = val.AttemptedValue;
            }

            bindingContext.ModelState.SetModelValue(key, val);
            var attemptedValues = attemptedValue.Split(',');
            var existingModel = (MultiSelectMvcModelCore)bindingContext.Model;
            if (existingModel != null)
            {
                //Clear out selected
                existingModel.Options.ForEach(x => x.Selected = false);
                foreach (var selectedValue in attemptedValues)
                {
                    // ReSharper disable AccessToModifiedClosure
                    var selectedOption = existingModel.Options.Find(x => x.Value == selectedValue);
                    // ReSharper restore AccessToModifiedClosure
                    
                    if (selectedOption != null) selectedOption.Selected = true;
                }
                return existingModel;
            }

            Options.Clear();
            foreach (var value in attemptedValues) Options.Add(new Option(value, "N/A", false, true));
            return this;
        }
        #endregion
    }
}