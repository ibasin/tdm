using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class CheckboxesListMvcModelUsing<MvcModelT> : MultiSelectMvcModelUsing<MvcModelT> where MvcModelT : MvcModelForEntityCore
    {
        #region ISupermodelEditorTemplate implementation
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            if (html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + " is called for a model that is null");
            if (!(html.ViewData.Model is MultiSelectMvcModelCore)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type diffrent from CheckboxesListFormModel.");

            var multiSelect = (MultiSelectMvcModelCore)html.ViewData.Model;
            return RenderCheckBoxesList(html, "", multiSelect.GetSelectListItemList());
        }

        protected virtual MvcHtmlString RenderCheckBoxesList(HtmlHelper html, string name, IEnumerable<SelectListItem> selectList)
        {
            //if (html.ViewData.ModelState.IsValid) html.ViewData.ModelState.Clear(); //if model is valid, we want to grab stuff from the model -- not model state
            
            var fullName = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);
            if (String.IsNullOrEmpty(fullName)) throw new ArgumentException(@"Value cannot be null or empty.", nameof(name));

            var result = new StringBuilder();
            
            result.AppendLine("<ul "+ UtilsLib.GenerateAttributesString(UlHtmlAttributesAsDict) +">");

            foreach (var item in selectList)
            {
                result.AppendLine("<li "+ UtilsLib.GenerateAttributesString(LiHtmlAttributesAsDict) +">");

                result.AppendLine(item.Selected ?
                    string.Format("<input name='{0}' type='checkbox' value='{1}' checked " + UtilsLib.GenerateAttributesString(InputHtmlAttributesAsDict) + "/>", fullName, item.Value) :
                    string.Format("<input name='{0}' type='checkbox' value='{1}' " + UtilsLib.GenerateAttributesString(InputHtmlAttributesAsDict) + "/>", fullName, item.Value));

                result.AppendLine("<label>" + item.Text + "</label>");
                result.AppendLine("</li>");
            }
            result.AppendLine("</ul>");
            result.AppendLine($"<input name='{fullName}' type='hidden' value='' />");
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region Properties
        public object UlHtmlAttributesAsObj { set => UlHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
        public IDictionary<string, object> UlHtmlAttributesAsDict { get; set; }

        public object LiHtmlAttributesAsObj { set => LiHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
        public IDictionary<string, object> LiHtmlAttributesAsDict { get; set; }

        public object InputHtmlAttributesAsObj { set => InputHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
        public IDictionary<string, object> InputHtmlAttributesAsDict { get; set; }
        #endregion
    }
}