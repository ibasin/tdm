﻿using System;
using System.Web.Mvc;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class TextBoxForDoubleMvcModel : TextBoxForNumberMvcModelBase<double?>
    {
        #region Constructors
        public TextBoxForDoubleMvcModel()
        {
            //Step = 0.01;
            Pattern = null;
        }
        #endregion

        #region ISupermodelModelBinder implemtation
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var key = bindingContext.ModelName;
            var val = bindingContext.ValueProvider.GetValue(key);
            string attemptedValue;
            if (val == null || string.IsNullOrEmpty(val.AttemptedValue))
            {
                if (bindingContext.ModelMetadata.IsRequired) bindingContext.ModelState.AddModelError(key, $"The field {bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName} is required");
                // ReSharper disable RedundantAssignment
                attemptedValue = "";
                // ReSharper restore RedundantAssignment
                Value = null;
            }
            else
            {
                attemptedValue = val.AttemptedValue;
                try
                {
                    Value = double.Parse(attemptedValue);
                }
                catch (FormatException)
                {
                    Value = null;
                    bindingContext.ModelState.AddModelError(key, $"The field {bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName} is invalid");
                }
            }

            bindingContext.ModelState.SetModelValue(key, val);

            var existingModel = (TextBoxForDoubleMvcModel)bindingContext.Model;
            if (existingModel != null)
            {
                existingModel.Value = Value;
                return existingModel;
            }
            return this;
        }
        #endregion

        #region IComparable implemetation
        public override int CompareTo(object obj)
        {
            var valueToCompareWith = ((TextBoxForDoubleMvcModel)obj).Value;
            if (Value == null && valueToCompareWith == null) return 0;
            if (Value == null || valueToCompareWith == null) return 1;
            return ((double)Value).CompareTo((double)valueToCompareWith);
        }
        #endregion
    }
}
