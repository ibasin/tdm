using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class ListBoxMvcModelUsing<MvcModelT> : MultiSelectMvcModelUsing<MvcModelT> where MvcModelT : MvcModelForEntityCore
    {
        #region ISupermodelEditorTemplate implemtation
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            if (html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + " is called for a model that is null");
            if (!(html.ViewData.Model is MultiSelectMvcModelCore)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type diffrent from ListBoxFormModel.");

            var multiSelect = (MultiSelectMvcModelCore)html.ViewData.Model;
            var fullName = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName("");
            
            //if (html.ViewData.ModelState.IsValid) html.ViewData.ModelState.Clear(); //if model is valid, we want to grab stuff from the model -- not model state
            var result = html.ListBox("", multiSelect.GetSelectListItemList(), HtmlAttributesAsDict) + $"<input name='{fullName}' type='hidden' value='' />";
            
            return MvcHtmlString.Create(result);
        }
        #endregion

        #region Properties
        public object HtmlAttributesAsObj { set { HtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> HtmlAttributesAsDict { get; set; }
        #endregion
    }
}