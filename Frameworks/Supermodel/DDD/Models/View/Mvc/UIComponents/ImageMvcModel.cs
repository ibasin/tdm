﻿using System;
using System.Web;
using System.Web.Mvc;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class ImageMvcModel : BinaryFileMvcModel
    {
        public ImageMvcModel()
        {
            HtmlAttributesAsObj = null;
        }
        
        public override object MapToObjectCustom(object obj, Type objType)
        {
            //do nothing
            return obj;
        }

        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return DisplayTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute);
        }

        public override MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            if (!(html.ViewData.Model is ImageMvcModel)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type diffrent from ImageMvcModel.");
            var model = ((ImageMvcModel)html.ViewData.Model);

            if (string.IsNullOrEmpty(model.Name) || !html.ViewDataContainer.ViewData.ModelState.IsValid) return MvcHtmlString.Create("");

            var imageLink = VirtualPathUtility.ToAbsolute($"~/{html.ViewContext.RouteData.Values["Controller"]}/GetBinaryFile/{html.ViewContext.RouteData.Values["id"]}?pn={html.ViewData.ModelMetadata.PropertyName}");
            return new MvcHtmlString($"<div><img src='{imageLink}' {UtilsLib.GenerateAttributesString(HtmlAttributesAsDict)}/></div>");
        }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            throw new InvalidOperationException(); //This should never be called
        }
    }
}
