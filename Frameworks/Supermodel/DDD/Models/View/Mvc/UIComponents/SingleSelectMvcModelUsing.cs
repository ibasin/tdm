﻿using System.Globalization;
using ReflectionMapper;
using Supermodel.DDD.Repository;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public abstract class SingleSelectMvcModelUsing<MvcModelT> : SingleSelectMvcModelForEntity where MvcModelT : MvcModelForEntityCore
    {
        #region Constructors
        protected SingleSelectMvcModelUsing()
        {
            var mvcModelForEntitybaseType = ReflectionHelper.IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(typeof(MvcModelT), typeof(MvcModelForEntity<>));
            if (mvcModelForEntitybaseType == null) throw new SupermodelSystemErrorException("DropdownMvcModelUsing<MvcModelT> has invalid type parameter");
            var entityType = mvcModelForEntitybaseType.GetGenericArguments()[0];
            
            Options = ((IDataRepoGenericTypeIgnorant)RepoFactory.CreateForRuntimeType(entityType)).GetDropdownOptions<MvcModelT>();
            SelectedValue = "";
        }
        protected SingleSelectMvcModelUsing(long selectedId) : this()
        {
            SelectedValue = selectedId.ToString(CultureInfo.InvariantCulture);
        }
        #endregion
    }
}
