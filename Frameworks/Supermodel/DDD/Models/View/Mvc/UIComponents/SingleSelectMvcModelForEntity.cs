﻿using System;
using System.Globalization;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Repository;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public abstract class SingleSelectMvcModelForEntity : SingleSelectMvcModel, IRMapperCustom
    {
        #region ICustomMapper implemtation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            if (!objType.IsEntityType()) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

            var entity = (IEntity)obj;
            SelectedValue = entity?.Id.ToString(CultureInfo.InvariantCulture) ?? "";

            return this;
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            if (!objType.IsEntityType()) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

            if (string.IsNullOrEmpty(SelectedValue)) return null;

            var id = long.Parse(SelectedValue);
            var entity = (IEntity)obj;
            if (entity != null && entity.Id == id) return entity;

            var repo = RepoFactory.CreateForRuntimeType(objType);
            var newEntity = (IEntity)repo.ExecuteMethod("GetById", id);
            return newEntity;
        }
        #endregion
    }
}
