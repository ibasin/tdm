﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Supermodel.Extensions;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class CheckboxMvcModel : BooleanMvcModel
    {
        #region ISupermodelEditorTemplateImplemtation
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
        {
            return html.CheckBox("", Value, HtmlAttributesAsDict);
        }
        #endregion

        #region ISupermodelDisplayTemplate
        public override MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
        {
            return EditorTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute).Supermodel().DisableAllControls();
        }
        #endregion

        #region ISupermodelModelBinder implemtation
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var key = bindingContext.ModelName;
            var val = bindingContext.ValueProvider.GetValue(key);

            //Checkbox is always requeired, if value is null, we assume false
            if (val == null || string.IsNullOrEmpty(val.AttemptedValue)) Value = false;
            else Value = (val.AttemptedValue.ToLower() != "false");

            bindingContext.ModelState.SetModelValue(key, val);

            var existingModel = (BooleanMvcModel)bindingContext.Model;
            if (existingModel != null)
            {
                existingModel.Value = Value;
                return existingModel;
            }
            return this;
        }
        #endregion

        #region ToString override
        public override string ToString()
        {
            return Value.ToString();
        }
        #endregion

        #region Properties
        public object HtmlAttributesAsObj { set { HtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> HtmlAttributesAsDict { get; set; }
        #endregion
    }
}
