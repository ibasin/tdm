﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class TextBoxForStringMvcModel : IRMapperCustom, ISupermodelEditorTemplate, ISupermodelDisplayTemplate, ISupermodelHiddenTemplate, ISupermodelMvcModelBinder, IComparable
    {
        #region ICustomMapper implemtation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(string)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

            var domainObj = (string)obj;
            Value = domainObj;

            return this;
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(string)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

            return Value;
        }
        #endregion

        #region ISupermodelEditorTemplate implemtation
        public virtual MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
            return html.TextBox("", Value ?? "", htmlAttributes);
        }
        #endregion

        #region ISupermodelDisplayTemplate implemetation
        public virtual MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return MvcHtmlString.Create(Value ?? "");
        }
        #endregion

        #region ISupermodelHiddenTemplate implemtation
        public virtual MvcHtmlString HiddenTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return html.Hidden("", Value ?? "");
        }
        #endregion

        #region ISupermodelModelBinder implemtation
        public virtual object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            string key = bindingContext.ModelName;
            ValueProviderResult val = bindingContext.ValueProvider.GetValue(key);
            string attemptedValue;
            if (val == null || string.IsNullOrEmpty(val.AttemptedValue))
            {
                if (bindingContext.ModelMetadata.IsRequired) bindingContext.ModelState.AddModelError(key, $"The field {bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName} is required");
                // ReSharper disable RedundantAssignment
                attemptedValue = "";
                // ReSharper restore RedundantAssignment
                Value = null;
            }
            else
            {
                attemptedValue = val.AttemptedValue;
                try
                {
                    Value = attemptedValue;
                }
                catch (FormatException)
                {
                    Value = null;
                    bindingContext.ModelState.AddModelError(key, $"The field {bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName} is invalid");
                }
            }

            bindingContext.ModelState.SetModelValue(key, val);

            var existingModel = (TextBoxForStringMvcModel)bindingContext.Model;
            if (existingModel != null)
            {
                existingModel.Value = Value;
                return existingModel;
            }
            return this;
        }
        #endregion

        #region IComparable implemetation
        public int CompareTo(object obj)
        {
            var valueToCompareWith = ((TextBoxForStringMvcModel)obj).Value;
            if (Value == null && valueToCompareWith == null) return 0;
            if (Value == null || valueToCompareWith == null) return 1;
            return String.Compare((Value), valueToCompareWith, StringComparison.InvariantCulture);
        }
        #endregion

        #region ToString override
        public override string ToString()
        {
            return Value;
        }
        #endregion

        #region Properies
        public string Value { get; set; }

        public object HtmlAttributesAsObj { set { HtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> HtmlAttributesAsDict { get; set; }
        #endregion
    }
}
