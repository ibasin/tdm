﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class TextBoxForPasswordMvcModel : TextBoxForStringMvcModel
    {
        #region Overrides
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();

            htmlAttributes.Add("type", "password");
            htmlAttributes.Add("autocomplete", "off");

            return html.TextBox("", Value ?? "", htmlAttributes);
        }
        #endregion
    }
}