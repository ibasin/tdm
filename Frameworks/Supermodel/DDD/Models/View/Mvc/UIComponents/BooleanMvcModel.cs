﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public abstract class BooleanMvcModel : IRMapperCustom, ISupermodelEditorTemplate, ISupermodelDisplayTemplate, ISupermodelMvcModelBinder, ISupermodelHiddenTemplate, IComparable
    {
        #region ICustomMapper implemtation
        public object MapFromObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(bool) && objType != typeof(bool?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

            if (obj != null)
            {
                var domainObj = (bool)obj;
                Value = domainObj;
            }
            else
            {
                Value = false;
            }

            return this;
        }
        public object MapToObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(bool) && objType != typeof(bool?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

            return Value;
        }
        #endregion

        #region IComparable implemetation
        public int CompareTo(object obj)
        {
            var valueToCompareWith = ((BooleanMvcModel)obj).Value;
            return Value.CompareTo(valueToCompareWith);
        }
        #endregion

        #region Properies
        public bool Value { get; set; }
        #endregion

        #region ISupermodelEditorTemplate implemtation
        public abstract MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null);
        #endregion

        #region ISupermodelDisplayTemplate implemtation
        public abstract MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null);
        #endregion

        #region ISupermodelHiddenTemplate implemetation
        public virtual MvcHtmlString HiddenTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
        {
            return html.Hidden("", Value);
        }
        #endregion

        #region BindModel implemtation
        public abstract object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext);
        #endregion

        #region ToString override
        public override string ToString()
        {
            return Value.ToString();
        }
        #endregion
    }
}
