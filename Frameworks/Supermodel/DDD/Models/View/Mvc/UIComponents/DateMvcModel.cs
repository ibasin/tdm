﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class DateMvcModel : IRMapperCustom, ISupermodelEditorTemplate, ISupermodelDisplayTemplate, ISupermodelHiddenTemplate, ISupermodelMvcModelBinder, IComparable
    {
        #region Constructors
        public DateMvcModel()
        {
            Value = null;
        }
        #endregion

        #region ICustomMapper implemtation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

            var domainObj = (DateTime?) obj;
            Value = domainObj;

            return this;
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

            return Value;
        }
        #endregion

        #region ISupermodelEditorTemplate implemtation
        public virtual MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            var dateTimeStr = (Value == null) ? "" : ((DateTime) Value).ToShortDateString();
            var htmlAttrDict = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
            htmlAttrDict.Add("data-sm-DatePicker", true);
            return html.TextBox("", dateTimeStr, htmlAttrDict);
        }
        #endregion

        #region ISupermodelDisplayTemplate
        public virtual MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            var dateTimeStr = (Value == null) ? "" : ((DateTime)Value).ToShortDateString();
            return MvcHtmlString.Create(dateTimeStr);
        }
        #endregion

        #region ISupermodelHiddenTemplate
        public virtual MvcHtmlString HiddenTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            var dateTimeStr = (Value == null) ? "" : ((DateTime)Value).ToShortDateString();
            return html.Hidden("", dateTimeStr);
        }
        #endregion

        
        #region ISupermodelModelBinder implemtation
        public virtual object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var key = bindingContext.ModelName;
            var val = bindingContext.ValueProvider.GetValue(key);
            string attemptedValue;
            if (val == null || string.IsNullOrEmpty(val.AttemptedValue))
            {
                if (bindingContext.ModelMetadata.IsRequired) bindingContext.ModelState.AddModelError(key, $"The field {bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName} is required");
                // ReSharper disable RedundantAssignment
                attemptedValue = "";
                // ReSharper restore RedundantAssignment
                Value = null;
            }
            else
            {
                attemptedValue = val.AttemptedValue;
                try
                {
                    Value = DateTime.Parse(attemptedValue);
                }
                catch(FormatException)
                {
                    Value = null;
                    bindingContext.ModelState.AddModelError(key, $"The field {bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName} is invalid");
                }
            }

            bindingContext.ModelState.SetModelValue(key, val);

            var existingModel = (DateMvcModel)bindingContext.Model;
            if (existingModel != null)
            {
                existingModel.Value = Value;
                return existingModel;
            }
            return this;
        }
        #endregion

        #region IComparable implemetation
        public int CompareTo(object obj)
        {
            var valueToCompareWith = ((DateMvcModel)obj).Value;
            if (Value == null && valueToCompareWith == null) return 0;
            if (Value == null || valueToCompareWith == null) return 1;
            return ((DateTime) Value).CompareTo(valueToCompareWith);
        }
        #endregion

        #region ToString override
        public override string ToString()
        {
            return Value != null ? Value.ToString() : "";
        }
        #endregion

        #region Properties
        public DateTime? Value { get; set; }
        public object HtmlAttributesAsObj { set { HtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> HtmlAttributesAsDict { get; set; }
        #endregion
    }

    public static class MvcModelDateTimeExtensions
    {
        public static DateTime? GetValue(this DateMvcModel me)
        {
            return me.Value;
        }
        public static DateMvcModel SetValue(this DateMvcModel me, DateTime? value)
        {
            me.Value = value;
            return me;
        }
    }
}
