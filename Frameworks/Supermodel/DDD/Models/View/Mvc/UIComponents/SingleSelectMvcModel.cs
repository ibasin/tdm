﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Supermodel.Extensions;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public abstract class SingleSelectMvcModel : IComparable, ISupermodelMvcModelBinder, ISupermodelEditorTemplate, ISupermodelDisplayTemplate, ISupermodelHiddenTemplate
    {
        #region Option nested class
        public class Option
        {
            public Option(string value, string label, bool isDisabled = false)
            {
                Value = value;
                Label = label;
                IsDisabled = isDisabled;
            }
            public string Value { get; private set; }
            public string Label { get; private set; }
            public bool IsDisabled { get; private set; }
        }
        public List<Option> Options = new List<Option>();
        #endregion

        #region Methods
        public List<SelectListItem> GetSelectListItemList()
        {
            var selectListItemList = new List<SelectListItem> { new SelectListItem { Value = "", Text = "" } };
            foreach (var option in Options)
            {
                var isSelectedOption = (SelectedValue != null && string.CompareOrdinal(SelectedValue, option.Value) == 0);
                if (isSelectedOption || !option.IsDisabled)
                {
                    var item = new SelectListItem { Value = option.Value, Text = !option.IsDisabled ? option.Label : option.Label + " [DISABLED]", Selected = isSelectedOption };
                    selectListItemList.Add(item);
                }
            }
            return selectListItemList;
        }
        public int CompareTo(object obj)
        {
            return string.CompareOrdinal(SelectedValue, ((SingleSelectMvcModel)obj).SelectedValue);
        }
        #endregion

        #region ISupermodelModelBinder implemtation
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var key = bindingContext.ModelName;
            var val = bindingContext.ValueProvider.GetValue(key);
            string attemptedValue;
            if (val == null || string.IsNullOrEmpty(val.AttemptedValue))
            {
                if (bindingContext.ModelMetadata.IsRequired) bindingContext.ModelState.AddModelError(key, $"The {bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName} field is required");
                attemptedValue = "";
            }
            else
            {
                attemptedValue = val.AttemptedValue.Replace(",", "").Trim();
            }

            bindingContext.ModelState.SetModelValue(key, val);

            SelectedValue = attemptedValue;

            var existingModel = (SingleSelectMvcModel)bindingContext.Model;
            if (existingModel != null)
            {
                existingModel.SelectedValue = SelectedValue;
                return existingModel;
            }
            return this;
        }
        #endregion

        #region ISupermodelEditorTemplate implemtation
        public abstract MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null);
        #endregion

        #region ISupermodelDipslayTemplate implementation
        public MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
        {
            return this.EditorTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute).Supermodel().DisableAllControls();
        }
        #endregion

        #region ISupermodelHIddenTemplate implemtation
        public virtual MvcHtmlString HiddenTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
        {
            return html.Hidden("", SelectedValue);
        }
        #endregion

        #region ToString override
        public override string ToString()
        {
            return SelectedLabel;
        }
        #endregion

        #region Properties
        public string SelectedValue { get; set; }
        public string SelectedLabel
        {
            get
            {
                var selectedOption = Options.FirstOrDefault(x => x.Value == SelectedValue);
                return selectedOption != null ? selectedOption.Label : null;
            }
        }
        public bool IsEmpty
        {
            get { return string.IsNullOrEmpty(SelectedValue); }
        }
        #endregion
    }
}
