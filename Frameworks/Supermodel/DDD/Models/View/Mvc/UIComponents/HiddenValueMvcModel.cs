﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc.Metadata;
using Supermodel.DDD.Repository;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    [HideLabel]
    public class HiddenValueMvcModel : IRMapperCustom, ISupermodelEditorTemplate, ISupermodelDisplayTemplate, ISupermodelHiddenTemplate, ISupermodelMvcModelBinder, IComparable
    {
        #region ICustomMapper implemtation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            if (obj == null)
            {
                Value = null;
                return this;
            }
            
            if (typeof(ICollection).IsAssignableFrom(objType))
            {
                var iCollectionInnerType = objType.GetICollectionGenericArg();
                if (!iCollectionInnerType.IsEntityType()) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

                var sb = new StringBuilder();
                var first = true;
                foreach (var entity in (IEnumerable<IEntity>)obj)
                {
                    var optionValue = entity.Id.ToString(CultureInfo.InvariantCulture);
                    if (first)
                    {
                        sb.Append(optionValue);
                        first = false;
                    }
                    else
                    {
                        sb.Append("," + optionValue);
                    }
                }
                Value = sb.ToString();
                return this;
            } 
                            
            if (typeof(IEntity).IsAssignableFrom(objType))
            {
                Value = ((IEntity)obj).Id.ToString(CultureInfo.InvariantCulture);
            }
            else if (objType == typeof(DateTime) || objType == typeof(DateTime?))
            {
                Value = ((DateTime)obj).ToShortDateString();
            }
            else
            {
                //Enums and value types
                Value = obj.ToString();
            }
            return this;
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            if (typeof(ICollection).IsAssignableFrom(objType))
            {
                var iCollectionInnerType = objType.GetICollectionGenericArg();
                if (!iCollectionInnerType.IsEntityType()) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

                object collection;
                //if (objType.IsGenericType) collection = obj ?? ReflectionHelper.CreateGenericType(objType.GetGenericTypeDefinition(), objType.GetGenericArguments());
                if (objType.IsGenericType) collection = obj ?? ReflectionHelper.CreateGenericType(typeof(List<>), objType.GetGenericArguments()[0]);
                else collection = obj ?? ReflectionHelper.CreateType(objType);
                var repo = RepoFactory.CreateForRuntimeType(iCollectionInnerType);

                //first clear the collection
                collection.ExecuteMethod("Clear");

                //Then add the new ones
                foreach (var option in Value.Split(','))
                {
                    var id = long.Parse(option);

                    var newEntity = (IEntity)repo.ExecuteMethod("GetById", id);
                    collection.ExecuteMethod("Add", newEntity);
                }
                return collection;
            }

            if (string.IsNullOrEmpty(Value)) return null;

            if (typeof (IEntity).IsAssignableFrom(objType))
            {
                var id = long.Parse(Value);
                var entity = (IEntity) obj;
                if (entity != null && entity.Id == id) return entity;

                var repo = RepoFactory.CreateForRuntimeType(objType);
                obj = repo.ExecuteMethod("GetById", id);
            }
            else
            {
                obj = ReflectionHelper.ExecuteStaticMethod(objType, "Parse", Value);
            }
            return obj;
        }
        #endregion

        #region ISupermodelEditorTemplate implemtation
        public virtual MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
            return html.Hidden("", Value ?? "", htmlAttributes);
        }
        #endregion

        #region ISupermodelDisplayTemplate implemetation
        public virtual MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            //We disp[lay nothing for hidden
            return MvcHtmlString.Create("");
        }
        #endregion

        #region ISupermodelHiddenTemplate implemtation
        public virtual MvcHtmlString HiddenTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return html.Hidden("", Value ?? "");
        }
        #endregion

        #region ISupermodelModelBinder implemtation
        public virtual object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            string key = bindingContext.ModelName;
            ValueProviderResult val = bindingContext.ValueProvider.GetValue(key);
            string attemptedValue;
            if (val == null || string.IsNullOrEmpty(val.AttemptedValue))
            {
                if (bindingContext.ModelMetadata.IsRequired) bindingContext.ModelState.AddModelError(key, $"The field {bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName} is required");
                // ReSharper disable RedundantAssignment
                attemptedValue = "";
                // ReSharper restore RedundantAssignment
                Value = null;
            }
            else
            {
                attemptedValue = val.AttemptedValue;
                try
                {
                    Value = attemptedValue;
                }
                catch (FormatException)
                {
                    Value = null;
                    bindingContext.ModelState.AddModelError(key, $"The field {bindingContext.ModelMetadata.DisplayName ?? bindingContext.ModelMetadata.PropertyName} is invalid");
                }
            }

            bindingContext.ModelState.SetModelValue(key, val);

            var existingModel = (HiddenValueMvcModel)bindingContext.Model;
            if (existingModel != null)
            {
                existingModel.Value = Value;
                return existingModel;
            }
            return this;
        }
        #endregion

        #region IComparable implemetation
        public int CompareTo(object obj)
        {
            var valueToCompareWith = ((TextBoxForStringMvcModel)obj).Value;
            if (Value == null && valueToCompareWith == null) return 0;
            if (Value == null || valueToCompareWith == null) return 1;
            return String.Compare((Value), valueToCompareWith, StringComparison.InvariantCulture);
        }
        #endregion

        #region ToString override
        public override string ToString()
        {
            return Value;
        }
        #endregion

        #region Properies
        public string Value { get; set; }

        public object HtmlAttributesAsObj { set { HtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> HtmlAttributesAsDict { get; set; }
        #endregion
    }
}
