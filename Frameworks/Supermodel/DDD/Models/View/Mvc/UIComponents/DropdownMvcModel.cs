using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View.Mvc.UIComponents
{
    public class DropdownMvcModel : SingleSelectMvcModel
    {
        #region ISupermodelEditorTemplate implemtation
        public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return DropdownCommonEditorTemplate(html, HtmlAttributesAsDict);
        }
        public static MvcHtmlString DropdownCommonEditorTemplate(HtmlHelper html, IDictionary<string, object> htmlAttributesAsDict)
        {
            if (html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + "is called for a model that is null");
            if (!(html.ViewData.Model is SingleSelectMvcModel)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type diffrent from SingleSelectMvcModelBase.");

            var dropdown = (SingleSelectMvcModel)html.ViewData.Model;
            return html.DropDownList("", dropdown.GetSelectListItemList(), htmlAttributesAsDict);
        }
        #endregion

        #region Properties
        public object HtmlAttributesAsObj { set { HtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
        public IDictionary<string, object> HtmlAttributesAsDict { get; set; }
        #endregion
    }
}