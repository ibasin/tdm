﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

namespace Supermodel.DDD.Models.View.Mvc
{
    // ReSharper disable UnusedTypeParameter
    public abstract class JQMobileAndTwitterBSSupermodelNamespaceHtmlHelperExtenstionsBase<ModelT>
    // ReSharper restore UnusedTypeParameter
    {
        #region Constructors
        protected JQMobileAndTwitterBSSupermodelNamespaceHtmlHelperExtenstionsBase(HtmlHelper html)
        {
            _html = html;
        }
        #endregion

        #region RESTful ActionLink and ActionLinkHtmlContent helpers
        public MvcHtmlString RESTfulActionLink(HttpVerbs actionVerb, string linkText, object htmlAttributes, string actionName, object routeValues, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, new MvcHtmlString(linkText), htmlAttributes, actionName, routeValues, confMsg);
        }
        public MvcHtmlString RESTfulActionLinkHtmlContent(HttpVerbs actionVerb, MvcHtmlString linkHtml, object htmlAttributes, string actionName, object routeValues, string confMsg)
        {
            var url = UrlHelper.GenerateUrl(null, actionName, null, new RouteValueDictionary(routeValues), _html.RouteCollection, _html.ViewContext.RequestContext, true);
            return RESTfulActionLinkFormContent(linkHtml, url, actionVerb, htmlAttributes, confMsg);
        }

        public MvcHtmlString RESTfulActionLink(HttpVerbs actionVerb, string linkText, object htmlAttributes, string actionName, object routeValues, string controllerName, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, new MvcHtmlString(linkText), htmlAttributes, actionName, routeValues, controllerName, confMsg);
        }
        public MvcHtmlString RESTfulActionLinkHtmlContent(HttpVerbs actionVerb, MvcHtmlString linkHtml, object htmlAttributes, string actionName, object routeValues, string controllerName, string confMsg)
        {
            var url = UrlHelper.GenerateUrl(null, actionName, controllerName, new RouteValueDictionary(routeValues), _html.RouteCollection, _html.ViewContext.RequestContext, true);
            return RESTfulActionLinkFormContent(linkHtml, url, actionVerb, htmlAttributes, confMsg);
        }

        public MvcHtmlString RESTfulActionLink(HttpVerbs actionVerb, string linkText, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, new MvcHtmlString(linkText), actionName, routeValues, htmlAttributes, confMsg);
        }
        public MvcHtmlString RESTfulActionLinkHtmlContent(HttpVerbs actionVerb, MvcHtmlString linkHtml, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string confMsg)
        {
            var url = UrlHelper.GenerateUrl(null, actionName, null, routeValues, _html.RouteCollection, _html.ViewContext.RequestContext, true);
            return RESTfulActionLinkFormContent(linkHtml, url, actionVerb, htmlAttributes, confMsg);
        }

        public MvcHtmlString RESTfulActionLink(HttpVerbs actionVerb, string linkText, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, IDictionary<string, object> formHtmlAttributes, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, new MvcHtmlString(linkText), actionName, routeValues, htmlAttributes, formHtmlAttributes, confMsg);
        }
        public MvcHtmlString RESTfulActionLinkHtmlContent(HttpVerbs actionVerb, MvcHtmlString linkHtml, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, IDictionary<string, object> formHtmlAttributes, string confMsg)
        {
            var url = UrlHelper.GenerateUrl(null, actionName, null, routeValues, _html.RouteCollection, _html.ViewContext.RequestContext, true);
            return RESTfulActionLinkFormContent(linkHtml, url, actionVerb, htmlAttributes, confMsg);
        }

        public MvcHtmlString RESTfulActionLink(HttpVerbs actionVerb, string linkText, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, new MvcHtmlString(linkText), actionName, controllerName, routeValues, htmlAttributes, confMsg);
        }
        public MvcHtmlString RESTfulActionLinkHtmlContent(HttpVerbs actionVerb, MvcHtmlString linkHtml, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string confMsg)
        {
            var url = UrlHelper.GenerateUrl(null, actionName, controllerName, routeValues, _html.RouteCollection, _html.ViewContext.RequestContext, true);
            return RESTfulActionLinkFormContent(linkHtml, url, actionVerb, htmlAttributes, confMsg);
        }
        
        private static MvcHtmlString RESTfulActionLinkFormContent(MvcHtmlString linkHtml, string url, HttpVerbs actionVerb, object htmlAttributes, string confMsg)
        {
            return RESTfulActionLinkFormContent(linkHtml, url, actionVerb, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes), confMsg);
        }
        private static MvcHtmlString RESTfulActionLinkFormContent(MvcHtmlString linkHtml, string url, HttpVerbs actionVerb, IDictionary<string, object> htmlAttributes, string confMsg)
        {
            //var result = new StringBuilder();

            string httpMethodOverride;
            switch (actionVerb)
            {
                case HttpVerbs.Delete:
                    httpMethodOverride = "Delete";
                    break;
                case HttpVerbs.Head:
                    httpMethodOverride = "Head";
                    break;
                case HttpVerbs.Put:
                    httpMethodOverride = "Put";
                    break;
                case HttpVerbs.Post:
                    httpMethodOverride = "Post";
                    break;
                default:
                    throw new SupermodelSystemErrorException("Unsupported HttpVerb in ActionLinkFormContent");
            }

            //button
            var aTag = new TagBuilder("button") 
            {
                InnerHtml = linkHtml != null ? linkHtml.ToString() : String.Empty
            };
            aTag.MergeAttribute("type", "button");
            
            if (confMsg != null) aTag.MergeAttribute("onclick", "supermodel_restfulLinkToUrlWithConfirmation('" + url + "', '" + httpMethodOverride + "', '" + confMsg + "')");
            else aTag.MergeAttribute("onclick", "supermodel_restfulLinkToUrlWithConfirmation('" + url + "', '" + httpMethodOverride + "')");

            aTag.MergeAttributes(htmlAttributes);

            return MvcHtmlString.Create(aTag.ToString());
        }
        #endregion

        #region HtmlHelper context
        protected readonly HtmlHelper _html;
        #endregion
    }
}
