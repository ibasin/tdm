﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ReflectionMapper;
using Supermodel.DDD.Models.View.Mvc.Metadata;
using Supermodel.Extensions;

namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
{
    public abstract partial class TwitterBS
    {
        public abstract class MvcModel : Mvc.MvcModel
        {
            #region Nested Scaffolding settings class
            public new static class ScaffoldingSettings
            {
                static ScaffoldingSettings()
                {
                    //CRUD Edit
                    FormId = "sm-form";
                    LabelCssClass = "control-label";
                    RequiredAsteriskCssClass = "sm-required";

                    BackButtonCssClass = "btn btn-success";
                    SaveButtonCssClass = "btn btn-primary";
                    FindButtonCssClass = "btn btn-primary";
                    ResetButtonCssClass = "btn btn-danger";
                    NewSearchButtonCssClass = "btn btn-success";

                    CRUDListTableCssClass = "table";
                    CRUDListEditCssClass = "btn btn-success";
                    CRUDListAddNewCssClass = "btn btn-success";
                    CRUDListDeleteCssClass = "btn btn-danger";
                }
                
                //CRUD Search
                public static string SearchTitleCssClass { get; set; }
                
                public static string FindButtonId { get; set; }
                public static string FindButtonCssClass { get; set; }

                public static string ResetButtonId { get; set; }
                public static string ResetButtonCssClass { get; set; }

                public static string NewSearchButtonId { get; set; }
                public static string NewSearchButtonCssClass { get; set; }
                
                //CRUDEdit
                public static string EditTitleCssClass { get; set; }

                public static string FormId { get; set; }
                
                public static string DisplayCssClass { get; set; }
                
                public static string LabelCssClass { get; set; }

                public static string RequiredAsteriskCssClass { get; set; }

                public static string SaveButtonId { get; set; }
                public static string SaveButtonCssClass { get; set; }

                public static string BackButtonId { get; set; }
                public static string BackButtonCssClass { get; set; }

                //CRUD List
                public static string ListTitleCssClass { get; set; }
                public static string ChildListTitleCssClass { get; set; }

                public static string CRUDListEditCssClass { get; set; }
                public static string CRUDListAddNewCssClass { get; set; }
                public static string CRUDListDeleteCssClass { get; set; }

                public static string CRUDListTopDivId { get; set; }
                public static string CRUDListTopDivCssClass { get; set; }

                public static string CRUDListTableId { get; set; }
                public static string CRUDListTableCssClass { get; set; }

                //Pagination
                public static string PaginationCssClass { get; set; }
            }
            #endregion

            #region ISupermodelEditorTemplate implemetation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                return TwitterBSCommonMvcModelEditorTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute);
            }
            public static MvcHtmlString TwitterBSCommonMvcModelEditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                if (html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + " is called for a model that is null");
                if (!(html.ViewData.Model is Mvc.MvcModel)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type different from MvcModel.");

                var result = new StringBuilder();
                if (html.ViewData.TemplateInfo.TemplateDepth <= 1)
                {
                    var properties = html.ViewData.ModelMetadata.Properties.Where(
                        pm => pm.ShowForEdit &&
                              !html.ViewData.TemplateInfo.Visited(pm) &&
                              (pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100) >= screenOrderFrom &&
                              (pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100) <= screenOrderTo)
                                         .OrderBy(pm => pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100);

                    foreach (var prop in properties)
                    {
                        //By default we do not scaffold ICollections
                        if (prop.ModelType.Name == typeof(ICollection<>).Name) continue;

                        if (prop.HideSurroundingHtml || Attribute.GetCustomAttribute(prop.ModelType, typeof(HideLabelAttribute)) != null)
                        {
                            result.AppendLine(html.Supermodel().Editor(prop.PropertyName).ToString());
                        }
                        else
                        {
                            var propMarkerAttribute = markerAttribute;
                            if (prop.AdditionalValues.ContainsKey("HtmlAttr")) propMarkerAttribute += " " + ((HtmlAttrAttribute)prop.AdditionalValues["HtmlAttr"]).Attr;

                            result.AppendLine("<div class='control-group'" + propMarkerAttribute + " >");
                            //Label
                            if (!prop.AdditionalValues.ContainsKey("HideLabel"))
                            {
                                var labelHtml = html.Label(prop.PropertyName, new { @class = ScaffoldingSettings.LabelCssClass }).ToString();
                                if (!prop.AdditionalValues.ContainsKey("NoRequiredLabel"))
                                {
                                    if ((prop.IsRequired && prop.ModelType != typeof(bool)) || prop.AdditionalValues.ContainsKey("ForceRequiredLabel")) labelHtml = labelHtml.Replace("</label>", "<em class='" + ScaffoldingSettings.RequiredAsteriskCssClass + "'>*</em></label>");
                                }
                                result.AppendLine(labelHtml);
                            }

                            //Value
                            if (!prop.AdditionalValues.ContainsKey("DisplayOnly") || prop.IsReadOnly)
                            {
                                if (!prop.AdditionalValues.ContainsKey("HideLabel")) result.AppendLine("<div class='controls'>");
                                result.AppendLine(html.Supermodel().Editor(prop.PropertyName).ToString());
                                var validationMessage = html.ValidationMessage(prop.PropertyName);
                                if (validationMessage != null) result.AppendLine(validationMessage.ToString());
                            }
                            else
                            {
                                if (!prop.AdditionalValues.ContainsKey("HideLabel")) result.AppendLine("<div class='controls displayOnly'>");
                                result.AppendLine("<span " + UtilsLib.MakeClassAttribue(ScaffoldingSettings.DisplayCssClass) + ">");
                                result.AppendLine(html.Supermodel().Display(prop.PropertyName).ToString());
                                result.AppendLine("</span>");
                            }
                            if (!prop.AdditionalValues.ContainsKey("HideLabel")) result.AppendLine("</div>");

                            result.AppendLine("</div>");
                        }
                    }
                }
                return MvcHtmlString.Create(result.ToString());
            }
            #endregion

            #region ISupermodelDisplayTemplate implemetation
            public override MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                return TwitterBSCommonMvcModelDisplayTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute);
            }
            public static MvcHtmlString TwitterBSCommonMvcModelDisplayTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                if (html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + " is called for a model that is null");
                if (!(html.ViewData.Model is Mvc.MvcModel)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type different from MvcModel.");

                var result = new StringBuilder();
                if (html.ViewData.TemplateInfo.TemplateDepth <= 1)
                {
                    var properties = html.ViewData.ModelMetadata.Properties.Where(
                        pm => pm.ShowForEdit &&
                              !html.ViewData.TemplateInfo.Visited(pm) &&
                              (pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100) >= screenOrderFrom &&
                              (pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100) <= screenOrderTo)
                                         .OrderBy(pm => pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100);

                    foreach (var prop in properties)
                    {
                        //By default we do not scaffold ICollections
                        if (prop.ModelType.Name == typeof(ICollection<>).Name) continue;

                        if (prop.HideSurroundingHtml)
                        {
                            result.AppendLine(html.Supermodel().Display(prop.PropertyName).ToString());
                        }
                        else
                        {
                            var propMarketAttribute = markerAttribute;
                            if (prop.AdditionalValues.ContainsKey("HtmlAttr")) propMarketAttribute += " " + ((HtmlAttrAttribute)prop.AdditionalValues["HtmlAttr"]).Attr;

                            result.AppendLine("<div class='control-group'" + propMarketAttribute + " >");
                            //Label
                            if (!prop.AdditionalValues.ContainsKey("HideLabel"))
                            {
                                var labelHtml = html.Label(prop.PropertyName, new { @class = ScaffoldingSettings.LabelCssClass }).ToString();
                                if (!prop.AdditionalValues.ContainsKey("NoRequiredLabel"))
                                {
                                    if ((prop.IsRequired && prop.ModelType != typeof(bool)) || prop.AdditionalValues.ContainsKey("ForceRequiredLabel")) labelHtml = labelHtml.Replace("</label>", "<em class='" + ScaffoldingSettings.RequiredAsteriskCssClass + "'>*</em></label>");
                                }
                                result.AppendLine(labelHtml);
                            }

                            //Value
                            if (!prop.AdditionalValues.ContainsKey("HideLabel")) result.AppendLine("<div class='controls displayOnly'>");
                            result.AppendLine("<span " + UtilsLib.MakeClassAttribue(ScaffoldingSettings.DisplayCssClass) + ">");
                            result.AppendLine(html.Supermodel().Display(prop.PropertyName).ToString());
                            result.AppendLine("</span>");
                            if (!prop.AdditionalValues.ContainsKey("HideLabel")) result.AppendLine("</div>");

                            result.AppendLine("</div>");
                        }
                    }
                }
                return MvcHtmlString.Create(result.ToString());
            }
            #endregion
        }
    }
}
