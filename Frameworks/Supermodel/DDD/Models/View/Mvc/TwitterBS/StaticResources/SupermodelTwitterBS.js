﻿$(function () {
    //This is to make menu work on iPad
    if (navigator.platform.indexOf('iPhone') != -1 || navigator.platform.indexOf('iPad') != -1) {
        $('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { e.stopPropagation(); });
    }

    $('.sm-slider').slider();

    (function () {
        var hasConfirmed = false;
        $("input[type=submit][data-sm-ConfirmMsg], input[type=button][data-sm-ConfirmMsg], a[data-sm-ConfirmMsg], button[data-sm-ConfirmMsg]").click(function () {
            var $this = $(this);
            if (!hasConfirmed) {
                bootbox.confirm($this.attr('data-sm-ConfirmMsg'), function (result) {
                    if (result) {
                        hasConfirmed = true;
                        $this.trigger("click");
                        hasConfirmed = false;
                    }
                });
                return false;
            }
            if ($this[0].tagName.toLowerCase() == 'a') window.location.href = $this.attr('href');
            return true;
        });
    })();
});

function supermodel_restfulLinkToUrlWithConfirmation(path, httpOverrideVerb, confirmationMsg) {
    if (confirmationMsg == null) {
        supermodel_restfulLinkToUrl(path, httpOverrideVerb);
    } else {
        bootbox.confirm(confirmationMsg, function (result) {
            if (result) supermodel_restfulLinkToUrl(path, httpOverrideVerb);
        });
    }
}

function supermodel_restfulLinkToUrl(path, httpOverrideVerb) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", path);

    if (httpOverrideVerb.toLowerCase() != 'post') {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "X-HTTP-Method-Override");
        hiddenField.setAttribute("value", httpOverrideVerb);
        form.appendChild(hiddenField);
    }

    document.body.appendChild(form);
    form.submit();
}