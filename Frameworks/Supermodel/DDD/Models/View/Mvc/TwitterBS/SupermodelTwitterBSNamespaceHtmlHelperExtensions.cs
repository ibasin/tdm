﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.Extensions;
using System.Web.Mvc.Html;

namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
{
    // ReSharper disable UnusedTypeParameter
    public class SupermodelTwitterBSNamespaceHtmlHelperExtensions<ModelT> : JQMobileAndTwitterBSSupermodelNamespaceHtmlHelperExtenstionsBase<ModelT>
    // ReSharper restore UnusedTypeParameter
    {
        #region Constructors
        public SupermodelTwitterBSNamespaceHtmlHelperExtensions(HtmlHelper html) : base(html) { }
        #endregion

        #region CRUD Search Form Helpers
        public MvcHtmlString CRUDSearchFormInAccordeonForModel(string accordeonElementId, IEnumerable<TwitterBS.AccordeonPanel> panels, string pageTitle = null, string action = null, string controller = null, bool resetButton = false)
        {
            return CRUDSearchFormInAccordeonForModel(accordeonElementId, panels, MvcHtmlString.Create(pageTitle), action, controller, resetButton);
        }
        public MvcHtmlString CRUDSearchFormInAccordeonForModel(string accordeonElementId, IEnumerable<TwitterBS.AccordeonPanel> panels, MvcHtmlString pageTitle, string action, string controller, bool resetButton)
        {
            return CRUDSearchFormInAccordeonHelper((Expression<Func<ModelT, ModelT>>)null, accordeonElementId, panels, pageTitle, controller, action, resetButton);
        }

        public MvcHtmlString CRUDSearchFormInAccordeonFor<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, string accordeonElementId, IEnumerable<TwitterBS.AccordeonPanel> panels, string pageTitle = null, string action = null, string controller = null, bool resetButton = false) where ValueT : MvcModel
        {
            return CRUDSearchFormInAccordeonFor(searchByModelExpression, accordeonElementId, panels, MvcHtmlString.Create(pageTitle), action, controller, resetButton);
        }
        public MvcHtmlString CRUDSearchFormInAccordeonFor<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, string accordeonElementId, IEnumerable<TwitterBS.AccordeonPanel> panels, MvcHtmlString pageTitle, string action, string controller, bool resetButton) where ValueT : MvcModel
        {
            return CRUDSearchFormInAccordeonHelper(searchByModelExpression, accordeonElementId, panels, pageTitle, controller, action, resetButton);
        }

        public MvcHtmlString CRUDSearchFormForModel(string pageTitle = null, string action = null, string controller = null, bool resetButton = false)
        {
            return CRUDSearchFormForModel(MvcHtmlString.Create(pageTitle), action, controller, resetButton);
        }
        public MvcHtmlString CRUDSearchFormForModel(MvcHtmlString pageTitle, string action, string controller, bool resetButton)
        {
            return CRUDSearchFormHelper((Expression<Func<ModelT, ModelT>>)null, pageTitle, controller, action, resetButton);
        }

        public MvcHtmlString CRUDSearchFormFor<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, string pageTitle = null, string action = null, string controller = null, bool resetButton = false) where ValueT : MvcModel
        {
            return CRUDSearchFormFor(searchByModelExpression, MvcHtmlString.Create(pageTitle), action, controller, resetButton);
        }
        public MvcHtmlString CRUDSearchFormFor<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, MvcHtmlString pageTitle, string action, string controller, bool resetButton) where ValueT : MvcModel
        {
            return CRUDSearchFormHelper(searchByModelExpression, pageTitle, controller, action, resetButton);
        }

        public MvcHtmlString CRUDSearchFormHeader(string pageTitle, string action, string controller, bool resetButton)
        {
            return CRUDSearchFormHeader(new MvcHtmlString(pageTitle), action, controller);
        }
        public MvcHtmlString CRUDSearchFormHeader(MvcHtmlString pageTitle, string action, string controller)
        {
            var result = new StringBuilder();

            action = action ?? "List";
            controller = controller ?? _html.ViewContext.RouteData.Values["controller"].ToString();
            result.AppendLine(_html.Supermodel().BeginGetFormMvcHtmlString(action, controller, null, new Dictionary<string, object> { { "class", TwitterBS.MvcModel.ScaffoldingSettings.FormId + " form-horizontal" } }).ToString());
            if (!MvcHtmlString.IsNullOrEmpty(pageTitle)) result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(TwitterBS.MvcModel.ScaffoldingSettings.SearchTitleCssClass) + ">" + pageTitle + "</h1>");

            return MvcHtmlString.Create(result.ToString());
        }

        public MvcHtmlString CRUDSearchFormFooter(bool resetButton)
        {
            var result = new StringBuilder();

            //result.AppendLine(_html.Hidden("smSkip", 0).ToString());
            result.AppendLine("<input id='smSkip' name='smSkip' type='hidden' value='0'>");
            result.AppendLine(_html.Hidden("smTake", _html.Supermodel().GetTakeValue()).ToString());
            result.AppendLine(_html.Hidden("smSortBy", _html.Supermodel().GetSortByValue()).ToString());

            result.AppendLine("<div class='control-group'>");
            result.AppendLine("<div class='controls'>");

            result.AppendLine("<button type='submit' " + UtilsLib.MakeIdAndClassAttribues(TwitterBS.MvcModel.ScaffoldingSettings.FindButtonId, TwitterBS.MvcModel.ScaffoldingSettings.FindButtonCssClass) + "><i class='icon-search icon-white'></i> Find&nbsp;</button>");
            if (resetButton) result.AppendLine("<button type='reset' " + UtilsLib.MakeIdAndClassAttribues(TwitterBS.MvcModel.ScaffoldingSettings.ResetButtonId, TwitterBS.MvcModel.ScaffoldingSettings.ResetButtonCssClass) + "><i class='icon-refresh icon-white'></i> Reset&nbsp;</button>");

            result.AppendLine("</div>");
            result.AppendLine("</div>");
            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());

            return MvcHtmlString.Create(result.ToString());
        }

        private MvcHtmlString CRUDSearchFormHelper<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, MvcHtmlString pageTitle, string controller, string action, bool resetButton)
        {
            var result = new StringBuilder();

            result.AppendLine(CRUDSearchFormHeader(pageTitle, action, controller).ToString());

            if (searchByModelExpression != null)
            {
                var html = (HtmlHelper<ModelT>)_html;

                var modelMetadata = ModelMetadata.FromLambdaExpression(searchByModelExpression, html.ViewData);
                var htmlFieldName = ExpressionHelper.GetExpressionText(searchByModelExpression);

                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                var nestedHtml = _html.Supermodel().MakeNestedHtmlHelper(modelMetadata, htmlFieldName);
                nestedHtml.ViewData.TemplateInfo.HtmlFieldPrefix = "";
                if (!(modelMetadata.Model is ISupermodelEditorTemplate)) throw new SupermodelSystemErrorException("CRUDSearchFormFor points to property that is not an MvcModel");
                result.AppendLine((modelMetadata.Model as ISupermodelEditorTemplate).EditorTemplate(nestedHtml).ToString());
            }
            else
            {
                result.AppendLine(_html.Supermodel().EditorForModel().ToString());
            }

            result.AppendLine(CRUDSearchFormFooter(resetButton).ToString());

            return MvcHtmlString.Create(result.ToString());
        }
        private MvcHtmlString CRUDSearchFormInAccordeonHelper<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, string accordeonElementId, IEnumerable<TwitterBS.AccordeonPanel> panels, MvcHtmlString pageTitle, string controller, string action, bool resetButton)
        {
            var result = new StringBuilder();

            result.AppendLine(CRUDSearchFormHeader(pageTitle, action, controller).ToString());

            result.AppendLine($"<div class='accordion' id='{accordeonElementId}'>");

            if (searchByModelExpression != null)
            {
                var html = (HtmlHelper<ModelT>)_html;

                var modelMetadata = ModelMetadata.FromLambdaExpression(searchByModelExpression, html.ViewData);
                var htmlFieldName = ExpressionHelper.GetExpressionText(searchByModelExpression);

                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                var nestedHtml = _html.Supermodel().MakeNestedHtmlHelper(modelMetadata, htmlFieldName);
                nestedHtml.ViewData.TemplateInfo.HtmlFieldPrefix = "";

                var supermodelModelEditorTemplate = modelMetadata.Model as ISupermodelEditorTemplate;
                if (supermodelModelEditorTemplate == null) throw new Exception("Model must implement ISupermodelEditorTemplate");
                foreach (var panel in panels)
                {
                    result.AppendLine("<div class='accordion-group'>");
                    result.AppendLine("<div class='accordion-heading'>");
                    result.AppendLine($"<a class='accordion-toggle' data-toggle='collapse' data-parent='#{accordeonElementId}' href='#{panel.ElementId}'>");
                    result.AppendLine(panel.Title);
                    result.AppendLine("</a>");
                    result.AppendLine("</div>");
                    result.AppendLine($"<div id='{panel.ElementId}' class='accordion-body collapse'>");
                    result.AppendLine("<div class='accordion-inner'>");

                    result.AppendLine(supermodelModelEditorTemplate.EditorTemplate(nestedHtml, panel.ScreenOrderFrom, panel.ScreenOrderTo).ToString());

                    result.AppendLine("</div>");
                    result.AppendLine("</div>");
                    result.AppendLine("</div>");
                }
            }
            else
            {
                var supermodelModelEditorTemplate = _html.ViewData.Model as ISupermodelEditorTemplate;
                if (supermodelModelEditorTemplate == null) throw new Exception("Model must implement ISupermodelEditorTemplate");
                foreach (var panel in panels)
                {
                    result.AppendLine("<div class='accordion-group'>");
                    result.AppendLine("<div class='accordion-heading'>");
                    result.AppendLine($"<a class='accordion-toggle' data-toggle='collapse' data-parent='#{accordeonElementId}' href='#{panel.ElementId}'>");
                    result.AppendLine(panel.Title);
                    result.AppendLine("</a>");
                    result.AppendLine("</div>");
                    result.AppendLine($"<div id='{panel.ElementId}' class='accordion-body collapse'>");
                    result.AppendLine("<div class='accordion-inner'>");

                    result.AppendLine(supermodelModelEditorTemplate.EditorTemplate(_html, panel.ScreenOrderFrom, panel.ScreenOrderTo).ToString());

                    result.AppendLine("</div>");
                    result.AppendLine("</div>");
                    result.AppendLine("</div>");
                }
            }
            result.AppendLine("</div>");

            result.AppendLine(CRUDSearchFormFooter(resetButton).ToString());

            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region CRUD Edit TwitterBS helpers
        public MvcHtmlString CRUDEdit<EntityT, MvcModelT>(string pageTitle, bool readOnly = false, bool skipBackButton = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            return CRUDEdit<EntityT, MvcModelT>(new MvcHtmlString(pageTitle), readOnly, skipBackButton);
        }

        public MvcHtmlString CRUDEdit<EntityT, MvcModelT>(MvcHtmlString pageTitle = null, bool readOnly = false, bool skipBackButton = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            return CRUDEditHelper<EntityT, MvcModelT>(pageTitle, readOnly, skipBackButton);
        }

        public MvcHtmlString CRUDEditHeader<EntityT, MvcModelT>(string pageTitle, bool readOnly = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            return CRUDEditHeader<EntityT, MvcModelT>(new MvcHtmlString(pageTitle), readOnly);
        }

        public MvcHtmlString CRUDEditHeader<EntityT, MvcModelT>(MvcHtmlString pageTitle = null, bool readOnly = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            var result = new StringBuilder();
            var model = (MvcModelT)_html.ViewData.Model;

            //Start form
            result.AppendLine(_html.Supermodel().BeginMultipartFormMvcHtmlString(new Dictionary<string, object> { { "class", TwitterBS.MvcModel.ScaffoldingSettings.FormId + " form-horizontal" } }).ToString());
            if (!MvcHtmlString.IsNullOrEmpty(pageTitle)) result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(TwitterBS.MvcModel.ScaffoldingSettings.EditTitleCssClass) + ">" + pageTitle + "</h1>");

            //Override Http Verb if needed
            if (model.Id != 0) result.AppendLine(_html.HttpMethodOverride(HttpVerbs.Put).ToString());

            return MvcHtmlString.Create(readOnly ? result.ToString().Supermodel().DisableAllControls() : result.ToString());
        }

        public MvcHtmlString CRUDEditFooter<EntityT, MvcModelT>(bool readOnly = false, bool skipBackButton = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            var result = new StringBuilder();
            var model = (MvcModelT)_html.ViewData.Model;

            result.AppendLine("<div class='control-group'>");
            result.AppendLine("<div class='controls'>");
            if (!skipBackButton)
            {
                long? parentId = null;
                if (ReflectionHelper.IsClassADerivedFromClassB(model.GetType(), typeof(ChildMvcModelForEntity<,>))) parentId = (long?)model.PropertyGet("ParentId");

                //make sure we keep query string
                var routeValues = _html.Supermodel().QueryStringRouteValues();
                var newRouteValues = HtmlHelper.AnonymousObjectToHtmlAttributes(new { parentId });
                routeValues.Supermodel().AddOrUpdateWith(newRouteValues);

                //set up html attributes
                var htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(new { id = TwitterBS.MvcModel.ScaffoldingSettings.BackButtonId, @class = TwitterBS.MvcModel.ScaffoldingSettings.BackButtonCssClass });

                result.AppendLine(_html.Supermodel().ActionLinkHtmlContent(MvcHtmlString.Create("<i class='icon-arrow-left icon-white'></i> Back&nbsp;"), "List", routeValues, htmlAttributes).ToString());
            }
            if (!readOnly) result.AppendLine("<button type='submit' " + UtilsLib.MakeIdAndClassAttribues(TwitterBS.MvcModel.ScaffoldingSettings.SaveButtonId, TwitterBS.MvcModel.ScaffoldingSettings.SaveButtonCssClass) + "><i class='icon-ok icon-white'></i> Save&nbsp;</button>");
            result.AppendLine("</div>");
            result.AppendLine("</div>");
            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());

            return MvcHtmlString.Create(readOnly ? result.ToString().Supermodel().DisableAllControls() : result.ToString());
        }

        private MvcHtmlString CRUDEditHelper<EntityT, MvcModelT>(MvcHtmlString pageTitle, bool readOnly, bool skipBackButton)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            var result = new StringBuilder();

            result.AppendLine(CRUDEditHeader<EntityT, MvcModelT>(pageTitle, readOnly).ToString());
            result.AppendLine(_html.Supermodel().EditorForModel().ToString().Supermodel().DisableAllControlsIf(readOnly));
            result.AppendLine(CRUDEditFooter<EntityT, MvcModelT>(readOnly, skipBackButton).ToString());
            result.AppendLine();

            return MvcHtmlString.Create(result.ToString());
        }

        public MvcHtmlString CRUDEditInAccordeon<EntityT, MvcModelT>(string accordeonElementId, IEnumerable<TwitterBS.AccordeonPanel> panels, string pageTitle, bool readOnly = false, bool skipBackButton = false, bool skipHeaderAndFooter = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            return CRUDEditInAccordeon<EntityT, MvcModelT>(accordeonElementId, panels, new MvcHtmlString(pageTitle), readOnly, skipBackButton, skipHeaderAndFooter);
        }
        public MvcHtmlString CRUDEditInAccordeon<EntityT, MvcModelT>(string accordeonElementId, IEnumerable<TwitterBS.AccordeonPanel> panels, MvcHtmlString pageTitle = null, bool readOnly = false, bool skipBackButton = false, bool skipHeaderAndFooter = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            return CRUDEditInAcoordeonHelper<EntityT, MvcModelT>(accordeonElementId, panels, pageTitle, readOnly, skipBackButton, skipHeaderAndFooter);
        }
        private MvcHtmlString CRUDEditInAcoordeonHelper<EntityT, MvcModelT>(string accordeonElementId, IEnumerable<TwitterBS.AccordeonPanel> panels, MvcHtmlString pageTitle, bool readOnly, bool skipBackButton, bool skipHeaderAndFooter = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            var result = new StringBuilder();

            if (!skipHeaderAndFooter) result.AppendLine(CRUDEditHeader<EntityT, MvcModelT>(pageTitle, readOnly).ToString());

            //result.AppendLine(_html.Supermodel().EditorForModel().ToString().Supermodel().DisableAllControlsIf(readOnly));
            result.AppendLine($"<div class='accordion' id='{accordeonElementId}'>");
            foreach (var panel in panels)
            {
                result.AppendLine("<div class='accordion-group'>");
                result.AppendLine("<div class='accordion-heading'>");
                result.AppendLine($"<a class='accordion-toggle' data-toggle='collapse' data-parent='#{accordeonElementId}' href='#{panel.ElementId}'>");
                result.AppendLine(panel.Title);
                result.AppendLine("</a>");
                result.AppendLine("</div>");
                result.AppendLine($"<div id='{panel.ElementId}' class='accordion-body collapse'>");
                result.AppendLine("<div class='accordion-inner'>");

                var supermodelModelEditorTemplate = _html.ViewData.Model as ISupermodelEditorTemplate;
                if (supermodelModelEditorTemplate == null) throw new Exception("Model must implement ISupermodelEditorTemplate");
                result.AppendLine(supermodelModelEditorTemplate.EditorTemplate(_html, panel.ScreenOrderFrom, panel.ScreenOrderTo).ToString().Supermodel().DisableAllControlsIf(readOnly));

                result.AppendLine("</div>");
                result.AppendLine("</div>");
                result.AppendLine("</div>");
            }
            result.AppendLine("</div>");

            if (!skipHeaderAndFooter) result.AppendLine(CRUDEditFooter<EntityT, MvcModelT>(readOnly, skipBackButton).ToString());
            result.AppendLine();

            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region CRUD List TwitterBS helpers
        public MvcHtmlString CRUDList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), null, skipAddNew, viewOnly);
        }

        public MvcHtmlString CRUDList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType = null, MvcHtmlString pageTitle = null, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, null, skipAddNew, viewOnly);
        }

        public MvcHtmlString CRUDChildrenList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, long parentId, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), parentId, skipAddNew, viewOnly);
        }

        public MvcHtmlString CRUDChildrenList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long parentId, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, parentId, skipAddNew, viewOnly);
        }

        private MvcHtmlString CRUDListHelper<EntityT, MvcModelT>(IEnumerable<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long? parentId, bool skipAddNew, bool viewOnly)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            var controllerName = controllerType != null ?
                                     controllerType.Supermodel().GetControllerName() :
                                     _html.ViewContext.RouteData.Values["controller"].ToString();

            var result = new StringBuilder();
            if (parentId == null || parentId > 0)
            {
                if (!MvcHtmlString.IsNullOrEmpty(pageTitle))
                {
                    if (parentId == null) result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(TwitterBS.MvcModel.ScaffoldingSettings.ListTitleCssClass) + ">" + pageTitle + "</h1>");
                    else result.AppendLine("<h2 " + UtilsLib.MakeClassAttribue(TwitterBS.MvcModel.ScaffoldingSettings.ChildListTitleCssClass) + ">" + pageTitle + "</h2>");
                }
                result.AppendLine("<div" + UtilsLib.MakeIdAndClassAttribues(TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTopDivId, TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTopDivCssClass) + ">");
                if (!skipAddNew)
                {
                    //make sure we keep query string
                    var routeValues = _html.Supermodel().QueryStringRouteValues();
                    var newRouteValues = HtmlHelper.AnonymousObjectToHtmlAttributes(new { id = (long) 0, parentId });
                    routeValues.Supermodel().AddOrUpdateWith(newRouteValues);

                    //set up html attributes
                    var htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = TwitterBS.MvcModel.ScaffoldingSettings.CRUDListAddNewCssClass });

                    result.AppendLine("<p>" + _html.Supermodel().ActionLinkHtmlContent(MvcHtmlString.Create("<i class='icon-plus icon-white'></i>"), "Detail", controllerName, routeValues, htmlAttributes) + "</p>");
                }
                result.AppendLine("<table" + UtilsLib.MakeIdAndClassAttribues(TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTableId, TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTableCssClass) + ">");
                result.AppendLine("<thead>");
                result.AppendLine("<tr>");
                result.AppendLine("<th>Name</th>");
                result.AppendLine("<th> Actions </th>");
                result.AppendLine("</tr>");
                result.AppendLine("</thead>");
                result.AppendLine("<tbody>");
                foreach (var item in items)
                {
                    result.AppendLine("<tr>");
                    result.AppendLine("<td>" + item.Label + "</td>");

                    //make sure we keep query string
                    var routeValues = _html.Supermodel().QueryStringRouteValues();
                    var newRouteValues = HtmlHelper.AnonymousObjectToHtmlAttributes(new { id = item.Id, parentId });
                    routeValues.Supermodel().AddOrUpdateWith(newRouteValues);

                    result.AppendLine("<td>");
                    if (viewOnly)
                    {
                        result.AppendLine(_html.Supermodel().ActionLinkHtmlContent(MvcHtmlString.Create("<i class='icon-eye-open icon-white'></i>"), "Detail", controllerName, routeValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = TwitterBS.MvcModel.ScaffoldingSettings.CRUDListEditCssClass })).ToString());
                    }
                    else
                    {
                        result.AppendLine("<div class='btn-group'>");

                        result.AppendLine(_html.Supermodel().ActionLinkHtmlContent(MvcHtmlString.Create("<i class='icon-edit icon-white'></i>"), "Detail", controllerName, routeValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = TwitterBS.MvcModel.ScaffoldingSettings.CRUDListEditCssClass })).ToString());
                        result.AppendLine(_html.Supermodel().TwitterBS.RESTfulActionLinkHtmlContent(HttpVerbs.Delete, MvcHtmlString.Create("<i class='icon-remove icon-white'></i>"), "Detail", controllerName, routeValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = TwitterBS.MvcModel.ScaffoldingSettings.CRUDListDeleteCssClass }), "Are you sure?").ToString());
                        result.AppendLine("</div>");
                    }
                    result.AppendLine("</td>");
                    result.AppendLine("</tr>");
                }
                result.AppendLine("</tbody>");
                result.AppendLine("</table>");
                result.AppendLine("</div>");
            }
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region CRUD Multi-Column List No Actions
        public MvcHtmlString CRUDMultiColumnListNoActions<T>(ICollection<T> items, string pageTitle) where T : new()
        {
            return CRUDMultiColumnListNoActionsHelper(items, new MvcHtmlString(pageTitle));
        }

        public MvcHtmlString CRUDMultiColumnListNoActions<T>(ICollection<T> items, MvcHtmlString pageTitle = null) where T : new()
        {
            return CRUDMultiColumnListNoActionsHelper(items, pageTitle);
        }

        public MvcHtmlString CRUDMutliColumnChildrenListNoActions<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, long parentId, bool skipAddNew = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), parentId, skipAddNew, false, false);
        }

        public MvcHtmlString CRUDMutliColumnChildrenListNoActions<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long parentId, bool skipAddNew = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, parentId, skipAddNew, false, false);
        }

        private MvcHtmlString CRUDMultiColumnListNoActionsHelper<T>(IEnumerable<T> items, MvcHtmlString pageTitle) where T : new()
        {
            var result = new StringBuilder();
            if (!MvcHtmlString.IsNullOrEmpty(pageTitle)) result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(TwitterBS.MvcModel.ScaffoldingSettings.ListTitleCssClass) + ">" + pageTitle + "</h1>");
            result.AppendLine("<div" + UtilsLib.MakeIdAndClassAttribues(TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTopDivId, TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTopDivCssClass) + ">");
            result.AppendLine("<table" + UtilsLib.MakeIdAndClassAttribues(TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTableId, TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTableCssClass) + ">");
            result.AppendLine("<thead>");
            result.AppendLine("<tr>");
            result = new T().ToHtmlTableHeader(_html, result);
            result.AppendLine("</tr>");
            result.AppendLine("</thead>");
            result.AppendLine("<tbody>");
            foreach (var item in items)
            {
                result.AppendLine("<tr>");

                //Render list columns using reflection
                result = item.ToHtmlTableRow(_html, result);

                result.AppendLine("</tr>");
            }
            result.AppendLine("</tbody>");
            result.AppendLine("</table>");
            result.AppendLine("</div>");
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region CRUD MutliColumn List helpers
        public MvcHtmlString CRUDMultiColumnList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), null, skipAddNew, viewOnly, true);
        }

        public MvcHtmlString CRUDMultiColumnList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle = null, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, null, skipAddNew, viewOnly, true);
        }

        public MvcHtmlString CRUDMultiColumnChildrenList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, long parentId, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), parentId, skipAddNew, viewOnly, true);
        }

        public MvcHtmlString CRUDMultiColumnChildrenList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long parentId, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, parentId, skipAddNew, viewOnly, true);
        }

        private MvcHtmlString CRUDMultiColumnListHelper<EntityT, MvcModelT>(IEnumerable<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long? parentId, bool skipAddNew, bool viewOnly, bool actionsColumn)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            var controllerName = controllerType != null ?
                                     controllerType.Supermodel().GetControllerName() :
                                     _html.ViewContext.RouteData.Values["controller"].ToString();

            var result = new StringBuilder();
            if (parentId == null || parentId > 0)
            {
                if (!MvcHtmlString.IsNullOrEmpty(pageTitle))
                {
                    if (parentId == null) result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(TwitterBS.MvcModel.ScaffoldingSettings.ListTitleCssClass) + ">" + pageTitle + "</h1>");
                    else result.AppendLine("<h2 " + UtilsLib.MakeClassAttribue(TwitterBS.MvcModel.ScaffoldingSettings.ChildListTitleCssClass) + ">" + pageTitle + "</h2>");
                }
                result.AppendLine("<div" + UtilsLib.MakeIdAndClassAttribues(TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTopDivId, TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTopDivCssClass) + ">");
                if (!skipAddNew)
                {
                    //make sure we keep query string
                    var routeValues = _html.Supermodel().QueryStringRouteValues();
                    var newRouteValues = HtmlHelper.AnonymousObjectToHtmlAttributes(new { id = (long) 0, parentId });
                    routeValues.Supermodel().AddOrUpdateWith(newRouteValues);

                    //set up html attributes
                    var htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = TwitterBS.MvcModel.ScaffoldingSettings.CRUDListAddNewCssClass });

                    result.AppendLine("<p>" + _html.Supermodel().ActionLinkHtmlContent(MvcHtmlString.Create("<i class='icon-plus icon-white'></i>"), "Detail", controllerName, routeValues, htmlAttributes) + "</p>");
                }
                result.AppendLine("<table" + UtilsLib.MakeIdAndClassAttribues(TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTableId, TwitterBS.MvcModel.ScaffoldingSettings.CRUDListTableCssClass) + ">");
                result.AppendLine("<thead>");
                result.AppendLine("<tr>");
                result = new MvcModelT().ToHtmlTableHeader(_html, result);
                if (actionsColumn) result.AppendLine("<th> Actions </th>");
                result.AppendLine("</tr>");
                result.AppendLine("</thead>");
                result.AppendLine("<tbody>");
                foreach (var item in items)
                {
                    result.AppendLine("<tr>");

                    //Render list columns using reflection
                    result = item.ToHtmlTableRow(_html, result);

                    //make sure we keep query string
                    var routeValues = _html.Supermodel().QueryStringRouteValues();
                    var newRouteValues = HtmlHelper.AnonymousObjectToHtmlAttributes(new { id = item.Id, parentId });
                    routeValues.Supermodel().AddOrUpdateWith(newRouteValues);

                    if (actionsColumn)
                    {
                        result.AppendLine("<td>");
                        if (viewOnly)
                        {
                            result.AppendLine(_html.Supermodel().ActionLinkHtmlContent(MvcHtmlString.Create("<i class='icon-eye-open icon-white'></i>"), "Detail", controllerName, routeValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = TwitterBS.MvcModel.ScaffoldingSettings.CRUDListEditCssClass })).ToString());
                        }
                        else
                        {
                            result.AppendLine("<div class='btn-group'>");

                            result.AppendLine(_html.Supermodel().ActionLinkHtmlContent(MvcHtmlString.Create("<i class='icon-edit icon-white'></i>"), "Detail", controllerName, routeValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = TwitterBS.MvcModel.ScaffoldingSettings.CRUDListEditCssClass })).ToString());
                            result.AppendLine(_html.Supermodel().TwitterBS.RESTfulActionLinkHtmlContent(HttpVerbs.Delete, MvcHtmlString.Create("<i class='icon-remove icon-white'></i>"), "Detail", controllerName, routeValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = TwitterBS.MvcModel.ScaffoldingSettings.CRUDListDeleteCssClass }), "Are you sure?").ToString());
                            result.AppendLine("</div>");
                        }
                        result.AppendLine("</td>");
                    }
                    result.AppendLine("</tr>");
                }
                result.AppendLine("</tbody>");
                result.AppendLine("</table>");
                result.AppendLine("</div>");
            }
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region New Search Action Link
        public MvcHtmlString NewSearchActionLink(MvcHtmlString linkHtml = null)
        {
            linkHtml = linkHtml ?? MvcHtmlString.Create("<i class='icon-search icon-white'></i> New Search&nbsp;");
            var routeValues = _html.Supermodel().QueryStringRouteValues();
            var controller = _html.ViewContext.RouteData.Values["controller"].ToString();
            var htmlAttributesDict = HtmlHelper.AnonymousObjectToHtmlAttributes(new { id = TwitterBS.MvcModel.ScaffoldingSettings.NewSearchButtonId, @class = TwitterBS.MvcModel.ScaffoldingSettings.NewSearchButtonCssClass });
            return _html.Supermodel().ActionLinkHtmlContent(linkHtml, "Search", controller, routeValues, htmlAttributesDict);
        }
        #endregion

        #region CRUD Pagination Helpers
        public MvcHtmlString Pagination(int visiblePages)
        {
            var skip = _html.Supermodel().GetSkipValue();
            var take = _html.Supermodel().GetTakeValue();
            var totalCount = (int?)_html.ViewBag.SupermodelTotalCount;

            if (skip == null || take == null || totalCount == null) throw new ArgumentNullException();

            return Pagination(visiblePages, (int)skip, (int)take, (int)totalCount);
        }
        public MvcHtmlString Pagination(int visiblePages, int skipped, int taken, int totalCount)
        {
            var result = new StringBuilder();
            if (taken < totalCount)
            {
                var paginationAdditionalClass = "";
                if (!string.IsNullOrEmpty(MvcModel.ScaffoldingSettings.PaginationCssClass)) paginationAdditionalClass = " " + MvcModel.ScaffoldingSettings.PaginationCssClass;
                result.AppendLine("<div class='pagination" + paginationAdditionalClass + "'>");
                result.AppendLine("<ul>");

                var currentPage = skipped / taken + 1;

                var firstPage = currentPage - visiblePages / 2;
                if (firstPage < 1) firstPage = 1;

                var lastPage = firstPage + visiblePages - 1;
                if (lastPage > totalCount / taken)
                {
                    firstPage -= lastPage - totalCount / taken;
                    if (firstPage < 1) firstPage = 1;
                    lastPage = (int)Math.Ceiling((double)totalCount / taken);
                }

                //Prev page
                if (currentPage > 1) result.AppendLine("<li>" + GetPageActionLink("«", currentPage - 1, taken) + "</li>");
                else result.AppendLine("<li class='disabled'><a href='#'>«</a></li>");

                //Neighboring pages
                for (var page = firstPage; page <= lastPage; page++)
                {
                    var linkStr = GetPageActionLink(page.ToString(CultureInfo.InvariantCulture), page, taken);
                    if (page == currentPage) result.AppendLine("<li class='active'>" + linkStr + "</li>");
                    else result.AppendLine("<li>" + linkStr + "</li>");
                }

                //Next page
                if (currentPage < lastPage) result.AppendLine("<li>" + GetPageActionLink("»", currentPage + 1, taken) + "</li>");
                else result.AppendLine("<li class='disabled'><a href='#'>»</a></li>");

                result.AppendLine("</ul>");
                result.AppendLine("</div>");
            }
            return MvcHtmlString.Create(result.ToString());
        }
        public MvcHtmlString PaginationTotalRecordsCount()
        {
            return _html.Supermodel().PaginationTotalRecordsCount();
        }
        private string GetPageActionLink(string linkText, int pageNum, int pageSize)
        {
            return _html.ActionLink(linkText, (string)_html.ViewContext.RouteData.Values["action"], _html.Supermodel().QueryStringRouteValues().Supermodel().AddOrUpdateWith("smSkip", (pageNum - 1) * pageSize)).ToString();
        }
        #endregion

        #region Static Placeholder helpers
        public MvcHtmlString ResponsiveHeader(WebViewPage webPage, string jqVersion, string tbsVersion, string rootUrl = "http://www.supermodelframework.com/sharedwebfiles/")
        {
            rootUrl = webPage.Url.Content(rootUrl);

            var sb = new StringBuilder();
            sb.AppendLine("<meta name='viewport' content='initial-scale=1.0, maximum-scale=1.0' />");
            if (tbsVersion[0] < '3')
            {
                sb.AppendLine("<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/twitter-bootstrap/" + tbsVersion + "/css/bootstrap-combined.min.css' type='text/css'/>");
            }
            else
            {
                sb.AppendLine("<link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/" + tbsVersion + "/css/bootstrap.min.css' type='text/css'/>");
            }
            sb.AppendLine("<link rel='stylesheet' href='" + rootUrl + "supermodel/TwitterBS/slider/css/slider.css' type='text/css'/>");
            sb.AppendLine("<link rel='stylesheet' href='" + rootUrl + "supermodel/TwitterBS/SupermodelTwitterBS.css' type='text/css'/>");

            sb.AppendLine("<script src='https://code.jquery.com/jquery-" + jqVersion + ".min.js'></script>");
            sb.AppendLine("<script src='https://stackpath.bootstrapcdn.com/bootstrap/" + tbsVersion + "/js/bootstrap.min.js'></script>");
            sb.AppendLine("<script src='" + rootUrl + "supermodel/twitterbs/bootbox/bootbox.min.js'></script>");
            //sb.AppendLine("<script src='https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.2.0/bootbox.min.js'></script>");
            sb.AppendLine("<script src='" + rootUrl + "supermodel/twitterbs/slider/js/bootstrap-slider.js'></script>");
            sb.AppendLine("<script src='" + rootUrl + "Supermodel/twitterbs/SupermodelTwitterBS.js' type='text/javascript'></script>");
            sb.AppendLine("<link rel='stylesheet' href = '" + rootUrl + "Css/Custom/site.css' type = 'text/css' />");

            return MvcHtmlString.Create(sb.ToString());
        }
        public MvcHtmlString DialogsScript(WebViewPage webPage)
        {
            var sb = new StringBuilder();
            sb.AppendLine("$(function () {");
            if (webPage.TempData.Supermodel().NextPageStartupScript != null) sb.AppendLine(webPage.TempData.Supermodel().NextPageStartupScript);
            if (webPage.TempData.Supermodel().NextPageAlertMessage != null) sb.AppendLine("alert('" + HttpUtility.HtmlEncode(webPage.TempData.Supermodel().NextPageAlertMessage) + "');");
            if (webPage.TempData.Supermodel().NextPageModalMessage != null)
            {
                sb.AppendLine("bootbox.alert('" + webPage.TempData.Supermodel().NextPageModalMessage + "'.replace(/\\n/g, '<br />'));");
            }
            sb.AppendLine("});");
            return MvcHtmlString.Create(sb.ToString());
        }

        // ReSharper disable InconsistentNaming
        public MvcHtmlString iOSNavBarFix(WebViewPage webPage)
        // ReSharper restore InconsistentNaming
        {
            var sb = new StringBuilder();
            sb.AppendLine("$(function () {");

            sb.AppendLine("if (navigator.platform.indexOf('iPhone') != -1 || navigator.platform.indexOf('iPad') != -1) {");
            sb.AppendLine("$('body').on('touchstart.dropdown', '.dropdown-menu', function (e) { window.scrollTo(0,0); e.stopPropagation(); });");
            sb.AppendLine("}");

            sb.AppendLine("});");
            return MvcHtmlString.Create(sb.ToString());
        }

        public MvcHtmlString MasterLayout(WebViewPage webPage, string appName, string jqVersion, string tbsVersion, string rootUrl = "http://www.supermodelframework.com/sharedwebfiles/")
        {
            var sb = new StringBuilder();
            sb.AppendLine("<!DOCTYPE html>");
            sb.AppendLine("<html>");
            sb.AppendLine("<head>");
            sb.AppendLine("<title>" + appName + "</title>");
            sb.AppendLine(ResponsiveHeader(webPage, jqVersion, tbsVersion, rootUrl).ToString());
            sb.AppendLine("</head> ");
            sb.AppendLine("<body>");
            sb.AppendLine("<script type='text/javascript'>");
            sb.AppendLine(DialogsScript(webPage).ToString());
            sb.AppendLine(iOSNavBarFix(webPage).ToString());
            sb.AppendLine("</script>");
            sb.AppendLine(webPage.RenderBody().ToString());
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");
            return MvcHtmlString.Create(sb.ToString());
        }
        #endregion

        #region SortBy helpers
        public MvcHtmlString SortByDropdown(Extensions.Supermodel.SortByOptions sortByOptions, object htmlAttributes = null)
        {
            return SortByDropdown(sortByOptions, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcHtmlString SortByDropdown(Extensions.Supermodel.SortByOptions sortByOptions, IDictionary<string, object> htmlAttributesDict)
        {
            return _html.Supermodel().SortByDropdown(sortByOptions, htmlAttributesDict);
        }

        public MvcHtmlString SortableColumnHeader(string headerName, string orderBy, string orderByDesc, MvcHtmlString sortedHtml = null, MvcHtmlString sortedHtmlDesc = null, object htmlAttributes = null)
        {
            return SortableColumnHeader(headerName, orderBy, orderByDesc, sortedHtml, sortedHtmlDesc, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcHtmlString SortableColumnHeader(string headerName, string orderBy, string orderByDesc, MvcHtmlString sortedHtml, MvcHtmlString sortedHtmlDesc, IDictionary<string, object> htmlAttributesDict)
        {
            return _html.Supermodel().SortableColumnHeader(headerName, orderBy, orderByDesc, sortedHtml, sortedHtmlDesc, htmlAttributesDict);
        }
        #endregion
    }
}
