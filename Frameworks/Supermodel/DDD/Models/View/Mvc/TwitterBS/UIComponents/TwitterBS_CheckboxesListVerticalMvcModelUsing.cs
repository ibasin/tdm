﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using ReflectionMapper;
using Supermodel.DDD.Models.View.Mvc.UIComponents;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
// ReSharper restore CheckNamespace
{
    public abstract partial class TwitterBS
    {
        public class CheckboxesListVerticalMvcModelUsing<MvcModelT> : MultiSelectMvcModelUsing<MvcModelT> where MvcModelT : MvcModelForEntityCore
        {
            #region ISupermodel EditorTemplate implementation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                if (html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + " is called for a model that is null");
                if (!(html.ViewData.Model is MultiSelectMvcModelCore)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type diffrent from CheckboxesListFormModel.");

                var multiSelect = (MultiSelectMvcModelCore)html.ViewData.Model;
                return RenderCheckBoxesList(html, "", multiSelect.GetSelectListItemList(), InputHtmlAttributesAsDict, LabelHtmlAttributesAsDict);
            }
            public static MvcHtmlString RenderCheckBoxesList(HtmlHelper html, string name, IEnumerable<SelectListItem> selectList, IDictionary<string, object> inputHtmlAttributesAsDict, IDictionary<string, object> labelHtmlAttributesAsDict)
            {
                //if (html.ViewData.ModelState.IsValid) html.ViewData.ModelState.Clear(); //if model is valid, we want to grab stuff from the model -- not model state
                
                var fullName = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);
                if (String.IsNullOrEmpty(fullName)) throw new ArgumentException(@"Value cannot be null or empty.", nameof(name));

                var result = new StringBuilder();

                labelHtmlAttributesAsDict = labelHtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                if (!labelHtmlAttributesAsDict.ContainsKey("class")) labelHtmlAttributesAsDict.Add("class", "checkbox");

                var index = 1;
                foreach (var item in selectList)
                {
                    var id = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName("") + "__" + index++;

                    result.AppendLine("<label " + UtilsLib.GenerateAttributesString(labelHtmlAttributesAsDict) + ">");
                    result.AppendLine(item.Selected ?
                        string.Format("<input name='{0}' type='checkbox' value='{1}' id='{2}' checked " + UtilsLib.GenerateAttributesString(inputHtmlAttributesAsDict) + "/>", fullName, item.Value, id) :
                        string.Format("<input name='{0}' type='checkbox' value='{1}' id='{2}' " + UtilsLib.GenerateAttributesString(inputHtmlAttributesAsDict) + "/>", fullName, item.Value, id));
                    result.AppendLine(item.Text);
                    result.AppendLine("</label>");
                }
                result.AppendLine($"<input name='{fullName}' type='hidden' value='' />");
                return MvcHtmlString.Create(result.ToString());
            }
            #endregion

            #region Properties
            public object InputHtmlAttributesAsObj { set => InputHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
            public IDictionary<string, object> InputHtmlAttributesAsDict { get; set; }

            public object LabelHtmlAttributesAsObj { set => LabelHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
            public IDictionary<string, object> LabelHtmlAttributesAsDict { get; set; }
            #endregion
        }
    }
}
