﻿using System;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
// ReSharper restore CheckNamespace
{
    public abstract partial class TwitterBS
    {
        public class DropdownMvcModelUsingEnum<EnumT> : UIComponents.DropdownMvcModelUsingEnum<EnumT> where EnumT : struct, IConvertible {}
    }
}

