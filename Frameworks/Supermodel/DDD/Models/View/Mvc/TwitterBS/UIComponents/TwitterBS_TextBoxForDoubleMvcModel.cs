﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
// ReSharper restore CheckNamespace
{
    public abstract partial class TwitterBS
    {
        public class TextBoxForDoubleMvcModel : UIComponents.TextBoxForDoubleMvcModel
        {
            #region ISupermodelEditorTemplate implemtation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                htmlAttributes.Add("type", "number");
                htmlAttributes.Add("data-clear-btn", true);
                if (!htmlAttributes.ContainsKey("step")) htmlAttributes.Add("step", "any");
                return html.TextBox("", GetStringValue(), htmlAttributes);
            }
            #endregion
        }
    }
}
