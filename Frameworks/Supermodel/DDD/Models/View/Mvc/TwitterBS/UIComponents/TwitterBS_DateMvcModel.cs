﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
// ReSharper restore CheckNamespace
{
    public abstract partial class TwitterBS
    {
        public class DateMvcModel : UIComponents.DateMvcModel
        {
            #region Overrides
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                var dateTimeStr = (Value == null) ? "" : ((DateTime)Value).ToString("yyyy-MM-dd");
                var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                
                htmlAttributes.Add("type", "date");

                return html.TextBox("", dateTimeStr, htmlAttributes);
            }
            #endregion
        }
    }
}
