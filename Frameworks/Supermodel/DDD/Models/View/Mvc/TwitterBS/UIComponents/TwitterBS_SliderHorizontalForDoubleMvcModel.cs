﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
// ReSharper restore CheckNamespace
{
    public abstract partial class TwitterBS
    {
        public class SliderHorizontalForDoubleMvcModel : UIComponents.SliderForIntMvcModel
        {
            #region ISupermodelEditorTemplate implemetation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                if (HtmlAttributesAsDict == null) HtmlAttributesAsDict = new Dictionary<string, object>();
                HtmlAttributesAsDict.Add("type", "text");
                HtmlAttributesAsDict.Add("data-slider-min", Min);
                HtmlAttributesAsDict.Add("data-slider-max", Max);
                HtmlAttributesAsDict.Add("data-slider-step", Step);
                HtmlAttributesAsDict.Add("data-slider-value", Value ?? Min);
                HtmlAttributesAsDict.Add("data-slider-orientation", "horizontal");
                HtmlAttributesAsDict.Add("class", "sm-slider");

                return html.TextBox("", Value ?? Min, HtmlAttributesAsDict);
            }
            #endregion
        }
    }
}
