﻿using System.Collections.Generic;
using System.Web.Mvc;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
// ReSharper restore CheckNamespace
{
    public abstract partial class TwitterBS
    {
        public class RadioSelectVerticalMvcModelUsing<MvcModelT> : UIComponents.SingleSelectMvcModelUsing<MvcModelT> where MvcModelT : MvcModelForEntityCore
        {
            #region ISupermodelEditorTemplate implemtation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                return RadioSelectVerticalMvcModel.RadioSelectCommonEditorTemplate(html, InputHtmlAttributesAsDict, LabelHtmlAttributesAsDict);
            }
            #endregion

            #region Properties
            public object InputHtmlAttributesAsObj { set { InputHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
            public IDictionary<string, object> InputHtmlAttributesAsDict { get; set; }

            public object LabelHtmlAttributesAsObj { set { LabelHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
            public IDictionary<string, object> LabelHtmlAttributesAsDict { get; set; }
            #endregion
        }
    }
}

