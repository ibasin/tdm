﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
// ReSharper restore CheckNamespace
{
    public abstract partial class TwitterBS
    {
        public class TextAreaMvcModel : TextBoxForStringMvcModel
        {
            #region Constructors
            public TextAreaMvcModel()
            {
                Cols = 40;
                Rows = 8;
            }
            #endregion

            #region Overrides
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                
                htmlAttributes.Add("cols", Cols);
                htmlAttributes.Add("Rows", Rows);
                htmlAttributes.Add("data-clear-btn", true);
                
                return html.TextArea("", Value ?? "", htmlAttributes);
            }
            #endregion

            #region Properties
            public uint Cols { get; set; }
            public uint Rows { get; set; }
            #endregion
        }
    }
}
