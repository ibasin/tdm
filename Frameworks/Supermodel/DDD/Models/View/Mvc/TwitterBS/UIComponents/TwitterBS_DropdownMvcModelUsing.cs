﻿// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
// ReSharper restore CheckNamespace
{
    public abstract partial class TwitterBS
    {
        public class DropdownMvcModelUsing<MvcModelT> : UIComponents.DropdownMvcModelUsing<MvcModelT> where MvcModelT : MvcModelForEntityCore {}
    }
}

