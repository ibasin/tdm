﻿using System.Collections.Generic;
using System.Web.Mvc;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
// ReSharper restore CheckNamespace
{
    public abstract partial class TwitterBS
    {
        public class RadioSelectHorizontalMvcModel : UIComponents.SingleSelectMvcModel
        {
            #region ISupermodelEditorTemplate implemntation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                return RadioSelectCommonEditorTemplate(html, InputHtmlAttributesAsDict, LabelHtmlAttributesAsDict);
            }
            public static MvcHtmlString RadioSelectCommonEditorTemplate(HtmlHelper html, IDictionary<string, object> inputHtmlAttributesAsDict, IDictionary<string, object> labelHtmlAttributesAsDict)
            {
                labelHtmlAttributesAsDict = labelHtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                labelHtmlAttributesAsDict.Add("class", "radio inline");
                return RadioSelectVerticalMvcModel.RadioSelectCommonEditorTemplate(html, inputHtmlAttributesAsDict, labelHtmlAttributesAsDict);
            }
            #endregion

            #region Properties
            public object InputHtmlAttributesAsObj { set { InputHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
            public IDictionary<string, object> InputHtmlAttributesAsDict { get; set; }

            public object LabelHtmlAttributesAsObj { set { LabelHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
            public IDictionary<string, object> LabelHtmlAttributesAsDict { get; set; }
            #endregion
        }
    }
}
