﻿using System.ComponentModel.DataAnnotations;

namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
{
    public abstract partial class TwitterBS
    {
        public class LoginMvcModel : MvcModel, ILoginMvcModel
        {
            public LoginMvcModel()
            {
                Username = new TextBoxForStringMvcModel();
                Password = new TextBoxForPasswordMvcModel();
            }
            
            public UIComponents.TextBoxForStringMvcModel Username { get; set; }
            public UIComponents.TextBoxForPasswordMvcModel Password { get; set; }

            [ScaffoldColumn(false)] public string UsernameStr
            {
                get => Username.Value;
                set => Username.Value = value;
            }
            [ScaffoldColumn(false)] public string PasswordStr
            {
                get => Password.Value;
                set => Password.Value = value;
            }
        }
    }
}