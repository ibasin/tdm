﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View.Mvc.TwitterBS
{
    public abstract partial class TwitterBS
    {
        public class SimpleSearchMvcModel : MvcModel, IValidatableObject 
        {
            #region Validation
            public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
            {
                return new ValidationResultList();
            }
            #endregion

            #region Properties
            public TextBoxForStringMvcModel SearchTerm { get; set; } = new TextBoxForStringMvcModel();
            #endregion
        }
    }
}
