﻿using System;
using System.Collections.Generic;
using System.Linq;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;

namespace Supermodel.DDD.Models.View.Mvc
{
    public class MvcModelList<MvcModelForEntityT, EntityT> : List<MvcModelForEntityT>, IRMapperCustom
        where MvcModelForEntityT : MvcModelForEntity<EntityT>, new()
        where EntityT : class, IEntity, new()
    {
        #region IRMapperCustom implemtation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            Clear();
            var entityList = (ICollection<EntityT>)obj;
            foreach (var entity in entityList.ToList())
            {
                var mvcModel = new MvcModelForEntityT().MapFrom(entity);
                Add(mvcModel);
            }
            return this;
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            var entityList = (ICollection<EntityT>)obj;

            //Add or Update
            foreach (var mvcModel in this)
            {
                EntityT entityMatch = null;
                if (!mvcModel.IsNewModel) entityMatch = entityList.SingleOrDefault(x => x.Id == mvcModel.Id);
                if (entityMatch != null)
                {
                    mvcModel.MapTo(entityMatch);
                }
                else
                {
                    var newEntity = mvcModel.MapTo(new EntityT());
                    entityList.Add(newEntity);
                }
            }

            //Delete
            foreach (var toDoItem in entityList.ToList())
            {
                if (this.All(x => x.Id != toDoItem.Id)) toDoItem.Delete();
            }

            //Set Parent for All
            //foreach (var entity in entityList)
            //{
            //    entity.ParentToDoListId = Id;
            //}

            return entityList;
        }
        #endregion
    }
}
