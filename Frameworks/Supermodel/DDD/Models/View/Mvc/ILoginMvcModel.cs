﻿namespace Supermodel.DDD.Models.View.Mvc
{
    public interface ILoginMvcModel
    {
        string UsernameStr { get; set; }
        string PasswordStr { get; set; }
    }
}
