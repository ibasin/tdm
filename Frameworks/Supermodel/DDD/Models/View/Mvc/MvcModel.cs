using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using ReflectionMapper;
using Supermodel.DDD.Models.View.Mvc.Metadata;
using Supermodel.Extensions;

namespace Supermodel.DDD.Models.View.Mvc
{
    public abstract class MvcModel : ISupermodelEditorTemplate, ISupermodelDisplayTemplate, ISupermodelHiddenTemplate
    {
        #region Nested Scaffolding settings class
        public static class ScaffoldingSettings
        {
            static ScaffoldingSettings()
            {
                FormId = "sm-form";
            }
            
            //EditorTemplate
            public static string FormId { get; set; }
            public static string FormTableId { get; set; }
            public static string FormTableCssClass { get; set; }

            //CRUD Search
            public static string SearchTitleCssClass { get; set; }
            
            public static string FindButtonId { get; set; }
            public static string FindButtonCssClass { get; set; }

            public static string ResetButtonId { get; set; }
            public static string ResetButtonCssClass { get; set; }

            public static string NewSearchButtonId { get; set; }
            public static string NewSearchButtonCssClass { get; set; }

            //CRUDEdit
            public static string EditTitleCssClass { get; set; }

            public static string DisplayCssClass { get; set; }

            public static string LabelCssClass { get; set; }

            public static string RequiredAsteriskCssClass { get; set; }

            public static string SaveButtonId { get; set; }
            public static string SaveButtonCssClass { get; set; }

            public static string BackButtonId { get; set; }
            public static string BackButtonCssClass { get; set; }


            //CRUDList
            public static string ListTitleCssClass { get; set; }
            public static string ChildListTitleCssClass { get; set; }

            public static string CRUDListTopDivId { get; set; }
            public static string CRUDListTopDivCssClass { get; set; }

            public static string CRUDListTableId { get; set; }
            public static string CRUDListTableCssClass { get; set; }

            public static string CRUDListEditCssClass { get; set; }
            public static string CRUDListDeleteCssClass { get; set; }
            public static string CRUDListAddNewCssClass { get; set; }

            //Pagination
            public static string PaginationCssClass { get; set; }
        }
        #endregion

        #region ISupermodelEditorTemplate implementation
        public virtual MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            return EditorTemplateCommon(html, screenOrderFrom, screenOrderTo, markerAttribute);
        }
        #endregion

        #region ISupermodelDisplayTemplate implementation
        public virtual MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
        {
            return DisplayTemplateCommon(html, screenOrderFrom, screenOrderTo, markerAttribute);
        }
        #endregion

        #region ISupermodelHiddenTemplateImplemetation
        public virtual MvcHtmlString HiddenTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
        {
            return HiddenTemplateCommon(html, screenOrderFrom, screenOrderTo, markerAttribute);
        }
        #endregion

        #region Static Versions for Reuse
        public static MvcHtmlString EditorTemplateCommon(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            if (html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + " is called for a model that is null");
            if (!(html.ViewData.Model is MvcModel)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type different from MvcModel.");

            var result = new StringBuilder();
            if (screenOrderFrom == int.MinValue) result.AppendLine("<table" + UtilsLib.MakeIdAndClassAttribues(ScaffoldingSettings.FormTableId, ScaffoldingSettings.FormTableCssClass) + ">");
            if (html.ViewData.TemplateInfo.TemplateDepth <= 1)
            {
                var properties = html.ViewData.ModelMetadata.Properties.Where(
                        pm => pm.ShowForEdit &&
                        !html.ViewData.TemplateInfo.Visited(pm) &&
                        (pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100) >= screenOrderFrom &&
                        (pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100) <= screenOrderTo)
                        .OrderBy(pm => pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100);

                foreach (var prop in properties)
                {
                    //By default we do not scaffold ICollections
                    if (prop.ModelType.Name == typeof(ICollection<>).Name) continue;

                    if (prop.HideSurroundingHtml || Attribute.GetCustomAttribute(prop.ModelType, typeof(HideLabelAttribute)) != null)
                    {
                        result.AppendLine(html.Supermodel().Editor(prop.PropertyName).ToString());
                    }
                    else
                    {
                        var propMarkerAttribute = markerAttribute;
                        if (prop.AdditionalValues.ContainsKey("HtmlAttr")) propMarkerAttribute += " " + ((HtmlAttrAttribute)prop.AdditionalValues["HtmlAttr"]).Attr;

                        result.AppendLine("<tr " + propMarkerAttribute + ">");

                        //Label
                        if (!prop.AdditionalValues.ContainsKey("HideLabel"))
                        {
                            result.AppendLine("<td nowrap>");
                            result.AppendLine("<span class = '" + ScaffoldingSettings.LabelCssClass + "'>");

                            result.AppendLine(html.Label(prop.PropertyName).ToString());

                            if (!prop.AdditionalValues.ContainsKey("NoRequiredLabel"))
                            {
                                if ((prop.IsRequired && prop.ModelType != typeof(bool)) || prop.AdditionalValues.ContainsKey("ForceRequiredLabel"))
                                {
                                    result.AppendLine("<span" + UtilsLib.MakeClassAttribue(ScaffoldingSettings.RequiredAsteriskCssClass) + "><sup>*</sup></span>");
                                }
                            }
                            result.AppendLine("</span>");
                            result.AppendLine("</td>");
                        }

                        //Value
                        result.AppendLine(!prop.AdditionalValues.ContainsKey("HideLabel") ? "<td>" : "<td colspan='2'>");
                        if (!prop.AdditionalValues.ContainsKey("DisplayOnly") || prop.IsReadOnly)
                        {
                            result.AppendLine(html.Supermodel().Editor(prop.PropertyName).ToString());
                            result.AppendLine("<br />");
                            var validationMessage = html.ValidationMessage(prop.PropertyName);
                            if (validationMessage != null) result.AppendLine(validationMessage.ToString());
                        }
                        else
                        {
                            result.AppendLine("<span " + UtilsLib.MakeClassAttribue(ScaffoldingSettings.DisplayCssClass) + ">");
                            result.AppendLine(html.Supermodel().Display(prop.PropertyName).ToString());
                            result.AppendLine("</span>");
                        }
                        result.AppendLine("</td>");

                        result.AppendLine("</tr>");
                    }
                }
            }
            if (screenOrderTo == int.MaxValue) result.Append("</table>");
            return MvcHtmlString.Create(result.ToString());
        }
        public static MvcHtmlString DisplayTemplateCommon(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            if (html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + " is called for a model that is null");
            if (!(html.ViewData.Model is MvcModel)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type different from MvcModel.");

            var result = new StringBuilder();
            if (screenOrderFrom == int.MinValue) result.AppendLine("<table" + UtilsLib.MakeIdAndClassAttribues(ScaffoldingSettings.FormTableId, ScaffoldingSettings.FormTableCssClass) + ">");
            if (html.ViewData.TemplateInfo.TemplateDepth <= 1)
            {
                var properties = html.ViewData.ModelMetadata.Properties.Where(
                        pm => pm.ShowForEdit &&
                        !html.ViewData.TemplateInfo.Visited(pm) &&
                        (pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100) >= screenOrderFrom &&
                        (pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100) <= screenOrderTo)
                        .OrderBy(pm => pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100);

                foreach (var prop in properties)
                {
                    //By default we do not scaffold ICollections
                    if (prop.ModelType.Name == typeof(ICollection<>).Name) continue;

                    if (prop.HideSurroundingHtml)
                    {
                        result.AppendLine(html.Supermodel().Display(prop.PropertyName).ToString());
                    }
                    else
                    {
                        var propMarkerAttribute = markerAttribute;
                        if (prop.AdditionalValues.ContainsKey("HtmlAttr")) propMarkerAttribute += " " + ((HtmlAttrAttribute)prop.AdditionalValues["HtmlAttr"]).Attr;
                        result.AppendLine("<tr " + propMarkerAttribute + ">");

                        //Label
                        if (!prop.AdditionalValues.ContainsKey("HideLabel"))
                        {
                            result.AppendLine("<td nowrap>");
                            result.AppendLine("<span class = '" + ScaffoldingSettings.LabelCssClass + "'>");

                            result.AppendLine(html.Label(prop.PropertyName).ToString().Replace("</label>", ":</label>"));

                            result.AppendLine("</span>");
                            result.AppendLine("</td>");
                        }

                        //Value
                        result.AppendLine(!prop.AdditionalValues.ContainsKey("HideLabel") ? "<td>" : "<td colspan='2'>");
                        result.AppendLine("<span " + UtilsLib.MakeClassAttribue(ScaffoldingSettings.DisplayCssClass) + ">");
                        result.AppendLine(html.Supermodel().Display(prop.PropertyName).ToString());
                        result.AppendLine("</span>");
                        result.AppendLine("</td>");

                        result.AppendLine("</tr>");
                    }
                }
            }
            if (screenOrderTo == int.MaxValue) result.Append("</table>");
            return MvcHtmlString.Create(result.ToString());
        }
        public static MvcHtmlString HiddenTemplateCommon(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
        {
            if (html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + " is called for a model that is null");
            if (!(html.ViewData.Model is MvcModel)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type different from MvcModel.");

            var result = new StringBuilder();
            if (html.ViewData.TemplateInfo.TemplateDepth <= 1)
            {
                var properties = html.ViewData.ModelMetadata.Properties.Where(
                        pm => pm.ShowForEdit &&
                        !html.ViewData.TemplateInfo.Visited(pm) &&
                        (pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100) >= screenOrderFrom &&
                        (pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100) <= screenOrderTo)
                        .OrderBy(pm => pm.AdditionalValues.ContainsKey("ScreenOrder") ? ((ScreenOrderAttribute)pm.AdditionalValues["ScreenOrder"]).Order : 100);

                foreach (var prop in properties)
                {
                    //By default we do not scaffold ICollections
                    if (prop.ModelType.Name == typeof(ICollection<>).Name) continue;

                    var propMarkerAttribute = markerAttribute;
                    if (prop.AdditionalValues.ContainsKey("HtmlAttr")) propMarkerAttribute += " " + ((HtmlAttrAttribute)prop.AdditionalValues["HtmlAttr"]).Attr;
                    
                    var hiddenFieldHtml = html.Supermodel().Hidden(prop.PropertyName).ToString();
                    if (!string.IsNullOrEmpty(propMarkerAttribute)) result.AppendLine(hiddenFieldHtml.Replace(">", " " + propMarkerAttribute + ">"));
                    else result.AppendLine(hiddenFieldHtml);
                }
            }
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion
    }
}