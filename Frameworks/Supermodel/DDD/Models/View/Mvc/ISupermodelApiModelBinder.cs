﻿using System.Web.Http.ModelBinding;

namespace Supermodel.DDD.Models.View.Mvc
{
    public interface ISupermodelApiModelBinder : IModelBinder { }
}
