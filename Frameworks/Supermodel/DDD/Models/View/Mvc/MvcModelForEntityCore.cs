using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using ReflectionMapper;
using Supermodel.DDD.Models.View.Mvc.UIComponents;

namespace Supermodel.DDD.Models.View.Mvc
{
    public abstract class MvcModelForEntityCore : MvcModel, IRMapperCustom
    {
        #region Methods
        [NotRMapped, ScaffoldColumn(false)] public bool IsNewModel => Id == 0;

        #endregion

        #region ICustom mapper implementation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            return this.MapFromObjectCustomBase(obj);
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            return this.MapToObjectCustomBase(obj);
        }
        #endregion

        #region Standard Properties for Mvc Models
        [ScaffoldColumn(false)] public virtual long Id { get; set; }

        [ScaffoldColumn(false), NotRMapped] public abstract string Label { get; }

        [ScaffoldColumn(false), NotRMapped] public virtual bool IsDisabled => false;

        [ScaffoldColumn(false), NotRMapped]
        public virtual bool ContainsBinaryFileMvcModelProperties
        {
            get { return GetType().GetProperties().Any(property => typeof(BinaryFileMvcModel).IsAssignableFrom(property.PropertyType)); }
        }
        #endregion
    }
}