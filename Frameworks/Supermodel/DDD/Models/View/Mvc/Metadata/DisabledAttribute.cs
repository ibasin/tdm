﻿using System;

namespace Supermodel.DDD.Models.View.Mvc.Metadata
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class DisabledAttribute : Attribute { }
}
