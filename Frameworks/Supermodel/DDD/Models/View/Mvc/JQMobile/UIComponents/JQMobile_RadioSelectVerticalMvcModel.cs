﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.JQMobile
// ReSharper restore CheckNamespace
{
    public abstract partial class JQMobile
    {
        public class RadioSelectVerticalMvcModel : UIComponents.SingleSelectMvcModel
        {
            #region ISupermodelEditorTemplate implementation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                return RadioSelectCommonEditorTemplate(html, FieldsetHtmlAttributesAsDict, InputHtmlAttributesAsDict, LabelHtmlAttributesAsDict);
            }
            public static MvcHtmlString RadioSelectCommonEditorTemplate(HtmlHelper html, IDictionary<string, object> fieldsetHtmlAttributesAsDict, IDictionary<string, object> inputHtmlAttributesAsDict, IDictionary<string, object> labelHtmlAttributesAsDict)
            {
                if (html.ViewData.Model == null) throw new NullReferenceException("Html.RadioSelectFormModelEditor() is called for a model that is null");
                if (!(html.ViewData.Model is UIComponents.SingleSelectMvcModel)) throw new InvalidCastException("Html.RadioSelectFormModelEditor() is called for a model of type diffrent from RadioSelectFormModel.");

                var radio = (UIComponents.SingleSelectMvcModel)html.ViewData.Model;
                var result = new StringBuilder();

                fieldsetHtmlAttributesAsDict = fieldsetHtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                fieldsetHtmlAttributesAsDict.Add("data-role", "controlgroup");
                result.AppendLine("<fieldset " + UtilsLib.GenerateAttributesString(fieldsetHtmlAttributesAsDict) + "'>");

                result.AppendLine("<legend></legend>");
                var index = 1;
                foreach (var option in radio.Options)
                {
                    var isSelectedOption = (radio.SelectedValue != null && string.CompareOrdinal(radio.SelectedValue, option.Value) == 0);
                    if (isSelectedOption || !option.IsDisabled)
                    {
                        var id = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName("") + "__" + index++;

                        var tmpInputHtmlAttributesAsDict = inputHtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                        tmpInputHtmlAttributesAsDict.Add("id", id);

                        result.AppendLine(html.RadioButton("", option.Value, (radio.SelectedValue == option.Value), tmpInputHtmlAttributesAsDict).ToString());
                        var labelText = !option.IsDisabled ? option.Label : option.Label + " [DISABLED]";

                        var tmpLabelHtmlAttributesAsDict = labelHtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                        tmpLabelHtmlAttributesAsDict.Add("for", id);
                        result.AppendFormat("<label {0}>{1}</label>", UtilsLib.GenerateAttributesString(tmpLabelHtmlAttributesAsDict), labelText);
                    }
                }
                result.AppendLine("</fieldset>");
                result.AppendLine(string.Format("<input id='{0}' name='{0}' type='hidden' value=''>", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName("")));
                return MvcHtmlString.Create(result.ToString());
            }
            #endregion

            #region Properties
            public object FieldsetHtmlAttributesAsObj { set => FieldsetHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
            public IDictionary<string, object> FieldsetHtmlAttributesAsDict { get; set; }

            public object InputHtmlAttributesAsObj { set => InputHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
            public IDictionary<string, object> InputHtmlAttributesAsDict { get; set; }

            public object LabelHtmlAttributesAsObj { set => LabelHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
            public IDictionary<string, object> LabelHtmlAttributesAsDict { get; set; }
            #endregion
        }
    }
}
