﻿// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.JQMobile
// ReSharper restore CheckNamespace
{
    public abstract partial class JQMobile
    {
        public class DropdownMvcModelUsing<MvcModelT> : UIComponents.DropdownMvcModelUsing<MvcModelT> where MvcModelT : MvcModelForEntityCore {}
    }
}

