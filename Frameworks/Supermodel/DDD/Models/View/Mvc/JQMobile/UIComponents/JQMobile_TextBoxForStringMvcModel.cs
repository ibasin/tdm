﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.JQMobile
// ReSharper restore CheckNamespace
{
    public abstract partial class JQMobile
    {
        public class TextBoxForStringMvcModel : UIComponents.TextBoxForStringMvcModel 
        {
            #region ISupermodelEditorTemplate implemtation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                htmlAttributes.Add("data-clear-btn", true);
                return html.TextBox("", Value ?? "", htmlAttributes);
            }
            #endregion
        }
    }
}
