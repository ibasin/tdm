﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.JQMobile
// ReSharper restore CheckNamespace
{
    public abstract partial class JQMobile
    {
        public class TextBoxForPasswordMvcModel : UIComponents.TextBoxForPasswordMvcModel
        {
            #region Overrides
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();

                htmlAttributes.Add("type", "password");
                htmlAttributes.Add("data-clear-btn", true);
                htmlAttributes.Add("autocomplete", "off");

                return html.TextBox("", Value ?? "", htmlAttributes);
            }
            #endregion
        }
    }
}