﻿using System.Collections.Generic;
using System.Web.Mvc;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.JQMobile
// ReSharper restore CheckNamespace
{
    public abstract partial class JQMobile
    {
        public class SliderForIntMvcModel : UIComponents.SliderForIntMvcModel
        {
            #region ISupermodelEditorTemplate implemetation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                if (HtmlAttributesAsDict == null) HtmlAttributesAsDict = new Dictionary<string, object>();
                HtmlAttributesAsDict.Add("data-highlight", true);
                return base.EditorTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute);
            }
            #endregion
        }
    }
}
