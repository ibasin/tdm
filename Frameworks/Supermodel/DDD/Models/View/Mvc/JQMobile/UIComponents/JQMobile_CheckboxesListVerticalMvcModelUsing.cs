﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;
using ReflectionMapper;
using Supermodel.DDD.Models.View.Mvc.UIComponents;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.JQMobile
// ReSharper restore CheckNamespace
{
    public abstract partial class JQMobile
    {
        public class CheckboxesListVerticalMvcModelUsing<MvcModelT> : MultiSelectMvcModelUsing<MvcModelT> where MvcModelT : MvcModelForEntityCore
        {
            #region ISupermodel EditorTemplate implementation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                if (html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + " is called for a model that is null");
                if (!(html.ViewData.Model is MultiSelectMvcModelCore)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type diffrent from CheckboxesListFormModel.");

                var multiSelect = (MultiSelectMvcModelCore)html.ViewData.Model;

                return RenderCheckBoxesList(html, "", multiSelect.GetSelectListItemList(), FieldsetHtmlAttributesAsDict, InputHtmlAttributesAsDict, LabelHtmlAttributesAsDict);
            }
            public static MvcHtmlString RenderCheckBoxesList(HtmlHelper html, string name, IEnumerable<SelectListItem> selectList, IDictionary<string, object> fieldsetHtmlAttributesAsDict, IDictionary<string, object> inputHtmlAttributesAsDict, IDictionary<string, object> labelHtmlAttributesAsDict)
            {
                //if (html.ViewData.ModelState.IsValid) html.ViewData.ModelState.Clear(); //if model is valid, we want to grab stuff from the model -- not model state
                
                var fullName = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(name);
                if (string.IsNullOrEmpty(fullName)) throw new ArgumentException(@"Value cannot be null or empty.", nameof(name));

                var result = new StringBuilder();

                fieldsetHtmlAttributesAsDict = fieldsetHtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                fieldsetHtmlAttributesAsDict.Add("data-role", "controlgroup");
                result.AppendLine("<fieldset " + UtilsLib.GenerateAttributesString(fieldsetHtmlAttributesAsDict) + "'>");
                
                result.AppendLine("<legend></legend>");
                var index = 1;
                foreach (var item in selectList)
                {
                    var id = html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName("") + "__" + index++;
                    result.AppendLine(item.Selected ?
                        string.Format("<input name='{0}' type='checkbox' value='{1}' id='{2}' checked " + UtilsLib.GenerateAttributesString(inputHtmlAttributesAsDict) + "/>", fullName, item.Value, id) :
                        string.Format("<input name='{0}' type='checkbox' value='{1}' id='{2}' " + UtilsLib.GenerateAttributesString(inputHtmlAttributesAsDict) + "/>", fullName, item.Value, id));

                    var tmpLabelHtmlAttributesAsDict = labelHtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                    tmpLabelHtmlAttributesAsDict.Add("for", id);
                    result.AppendFormat("<label {0}>{1}</label>", UtilsLib.GenerateAttributesString(tmpLabelHtmlAttributesAsDict), item.Text);
                }
                result.AppendLine("</fieldset>");
                result.AppendLine($"<input name='{fullName}' type='hidden' value='' />");
                return MvcHtmlString.Create(result.ToString());
            }
            #endregion

            #region Properties
            public object FieldsetHtmlAttributesAsObj { set => FieldsetHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
            public IDictionary<string, object> FieldsetHtmlAttributesAsDict { get; set; }

            public object InputHtmlAttributesAsObj { set => InputHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
            public IDictionary<string, object> InputHtmlAttributesAsDict { get; set; }

            public object LabelHtmlAttributesAsObj { set => LabelHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); }
            public IDictionary<string, object> LabelHtmlAttributesAsDict { get; set; }
            #endregion
        }
    }
}
