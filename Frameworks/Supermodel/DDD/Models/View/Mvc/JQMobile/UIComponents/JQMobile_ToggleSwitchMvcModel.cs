﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Supermodel.DDD.Models.View.Mvc.UIComponents;
using Supermodel.Extensions;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.JQMobile
// ReSharper restore CheckNamespace
{
    public abstract partial class JQMobile
    {
        public class ToggleSwitchMvcModel : BooleanMvcModel
        {
            #region ISupermodelEditorTemplate implemtation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
            {
                var optionsList = new List<SelectListItem>
                {
                    new SelectListItem {Value = "off", Text = "Off", Selected = !Value},
                    new SelectListItem {Value = "on", Text = "On", Selected = Value}
                };
                var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                htmlAttributes.Add("data-role", "slider");
                return html.DropDownList("", optionsList, htmlAttributes);
            }

            #endregion

            #region ISupermodelDisplayTemplate
            public override MvcHtmlString DisplayTemplate(HtmlHelper html, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue, string markerAttribute = null)
            {
                return EditorTemplate(html, screenOrderFrom, screenOrderTo, markerAttribute).Supermodel().DisableAllControls();
            }
            #endregion

            #region ISupermodelModelBinder implemtation
            public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                var key = bindingContext.ModelName;
                var val = bindingContext.ValueProvider.GetValue(key);

                //Toggle switch is always requeired, if value is null, we assume false
                if (val == null || string.IsNullOrEmpty(val.AttemptedValue)) Value = false;
                else Value = (val.AttemptedValue.ToLower() == "on");

                bindingContext.ModelState.SetModelValue(key, val);

                var existingModel = (BooleanMvcModel)bindingContext.Model;
                if (existingModel != null)
                {
                    existingModel.Value = Value;
                    return existingModel;
                }
                return this;
            }
            #endregion

            #region Properties
            public object HtmlAttributesAsObj { set { HtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
            public IDictionary<string, object> HtmlAttributesAsDict { get; set; }
            #endregion
        }
    }
}
