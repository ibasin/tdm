﻿using System.Collections.Generic;
using System.Web.Mvc;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.JQMobile
// ReSharper restore CheckNamespace
{
    public abstract partial class JQMobile
    {
        public class RadioSelectHorizontalMvcModel : UIComponents.SingleSelectMvcModel
        {
            #region ISupermodelEditorTemplate implemntation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                return RadioSelectCommonEditorTemplate(html, FieldsetHtmlAttributesAsDict, InputHtmlAttributesAsDict, LabelHtmlAttributesAsDict);
            }
            public static MvcHtmlString RadioSelectCommonEditorTemplate(HtmlHelper html, IDictionary<string, object> fieldsetHtmlAttributesAsDict, IDictionary<string, object> inputHtmlAttributesAsDict, IDictionary<string, object> labelHtmlAttributesAsDict)
            {
                fieldsetHtmlAttributesAsDict = fieldsetHtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                fieldsetHtmlAttributesAsDict.Add("data-type", "horizontal");
                return RadioSelectVerticalMvcModel.RadioSelectCommonEditorTemplate(html, fieldsetHtmlAttributesAsDict, inputHtmlAttributesAsDict, labelHtmlAttributesAsDict);
            }
            #endregion

            #region Properties
            public object FieldsetHtmlAttributesAsObj { set { FieldsetHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
            public IDictionary<string, object> FieldsetHtmlAttributesAsDict { get; set; }

            public object InputHtmlAttributesAsObj { set { InputHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
            public IDictionary<string, object> InputHtmlAttributesAsDict { get; set; }

            public object LabelHtmlAttributesAsObj { set { LabelHtmlAttributesAsDict = value == null ? null : HtmlHelper.AnonymousObjectToHtmlAttributes(value); } }
            public IDictionary<string, object> LabelHtmlAttributesAsDict { get; set; }
            #endregion
        }
    }
}
