﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Mvc.Html;

// ReSharper disable CheckNamespace
namespace Supermodel.DDD.Models.View.Mvc.JQMobile
// ReSharper restore CheckNamespace
{
    public abstract partial class JQMobile
    {
        public class TextBoxForIntMvcModel : UIComponents.TextBoxForIntMvcModel
        {
            #region ISupermodelEditorTemplate implemtation
            public override MvcHtmlString EditorTemplate(HtmlHelper html, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue, string markerAttribute = null)
            {
                var htmlAttributes = HtmlAttributesAsDict.Clone() ?? new Dictionary<string, object>();
                htmlAttributes.Add("type", "number");
                htmlAttributes.Add("data-clear-btn", true);
                if (Pattern != null) htmlAttributes.Add("pattern", Pattern);
                //if (Step != null) htmlAttributes.Add("step", Step);
                return html.TextBox("", GetStringValue(), htmlAttributes);
            }
            #endregion
        }
    }
}
