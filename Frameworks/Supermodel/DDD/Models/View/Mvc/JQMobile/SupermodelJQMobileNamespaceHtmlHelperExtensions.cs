﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.Extensions;

namespace Supermodel.DDD.Models.View.Mvc.JQMobile
{
    // ReSharper disable UnusedTypeParameter
    public class SupermodelJQMobileNamespaceHtmlHelperExtensions<ModelT> : JQMobileAndTwitterBSSupermodelNamespaceHtmlHelperExtenstionsBase<ModelT>
    // ReSharper restore UnusedTypeParameter
    {
        #region Constructors
        public SupermodelJQMobileNamespaceHtmlHelperExtensions(HtmlHelper html):base(html){}
	    #endregion

        #region CRUD Search Form Helpers
        public MvcHtmlString CRUDSearchFormForModel(string pageTitle = null, string menuPanelName = null, string action = null, string controller = null)
        {
            return CRUDSearchFormForModel(MvcHtmlString.Create(pageTitle), menuPanelName, action, controller);
        }
        public MvcHtmlString CRUDSearchFormForModel(MvcHtmlString pageTitle, string menuPanelName, string action, string controller)
        {
            return CRUDSearchFormHelper((Expression<Func<ModelT, ModelT>>)null, pageTitle, menuPanelName, controller, action);
        }

        public MvcHtmlString CRUDSearchFormFor<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, string pageTitle = null, string menuPanelName = null, string action = null, string controller = null, bool resetButton = false) where ValueT : MvcModel
        {
            return CRUDSearchFormFor(searchByModelExpression, MvcHtmlString.Create(pageTitle), menuPanelName, action, controller);
        }
        public MvcHtmlString CRUDSearchFormFor<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, MvcHtmlString pageTitle, string menuPanelName, string action, string controller) where ValueT : MvcModel
        {
            return CRUDSearchFormHelper(searchByModelExpression, pageTitle, menuPanelName, controller, action);
        }

        public MvcHtmlString CRUDSearchFormHeader(string pageTitle, string menuPanelName, string action, string controller)
        {
            return CRUDSearchFormHeader(new MvcHtmlString(pageTitle), menuPanelName, action, controller);
        }
        public MvcHtmlString CRUDSearchFormHeader(MvcHtmlString pageTitle, string menuPanelName, string action, string controller)
        {
            var result = new StringBuilder();

            action = action ?? "List";
            controller = controller ?? _html.ViewContext.RouteData.Values["controller"].ToString();

            result.AppendLine(_html.Supermodel().BeginGetFormMvcHtmlString(action, controller, null, new Dictionary<string, object> { { "class", JQMobile.MvcModel.ScaffoldingSettings.FormId } }).ToString());
            //if (!MvcHtmlString.IsNullOrEmpty(pageTitle)) result.AppendLine("<h1>" + pageTitle + "</h1>");

            //Header
            result.AppendLine("<div data-role='header' data-position='fixed'>");

            //Menu button
            if (menuPanelName != null) result.AppendLine($"<a href='#{menuPanelName}' class='ui-btn ui-btn-icon-right ui-icon-bars ui-btn-icon-notext'>Menu;</a>");

            //Title
            result.AppendLine("<h1>" + pageTitle + "</h1>");

            //Update button
            result.AppendLine("<button type='submit' data-icon='search' data-transition='slidedown' data-direction='reverse' class='ui-btn-right'>Find</button>");
            result.AppendLine("</div>");

            return MvcHtmlString.Create(result.ToString());
        }

        public MvcHtmlString CRUDSearchFormFooter()
        {
            var result = new StringBuilder();

            //result.AppendLine(_html.Hidden("smSkip", 0).ToString());
            result.AppendLine("<input id='smSkip' name='smSkip' type='hidden' value='0'>");
            result.AppendLine(_html.Hidden("smTake", _html.Supermodel().GetTakeValue()).ToString());
            result.AppendLine(_html.Hidden("smSortBy", _html.Supermodel().GetSortByValue()).ToString());

            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());

            return MvcHtmlString.Create(result.ToString());
        }

        private MvcHtmlString CRUDSearchFormHelper<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, MvcHtmlString pageTitle, string menuPanelName, string controller, string action)
        {
            var result = new StringBuilder();

            result.AppendLine(CRUDSearchFormHeader(pageTitle, menuPanelName, action, controller).ToString());

            if (searchByModelExpression != null)
            {
                var html = (HtmlHelper<ModelT>)_html;

                var modelMetadata = ModelMetadata.FromLambdaExpression(searchByModelExpression, html.ViewData);
                var htmlFieldName = ExpressionHelper.GetExpressionText(searchByModelExpression);

                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                var nestedHtml = _html.Supermodel().MakeNestedHtmlHelper(modelMetadata, htmlFieldName);
                nestedHtml.ViewData.TemplateInfo.HtmlFieldPrefix = "";
                if (!(modelMetadata.Model is ISupermodelEditorTemplate)) throw new SupermodelSystemErrorException("CRUDSearchFormFor points to property that is not an MvcModel");
                result.AppendLine((modelMetadata.Model as ISupermodelEditorTemplate).EditorTemplate(nestedHtml).ToString());
            }
            else
            {
                result.AppendLine(_html.Supermodel().EditorForModel().ToString());
            }
            
            result.AppendLine(CRUDSearchFormFooter().ToString());

            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region CRUD Edit JQMObile helpers
        public MvcHtmlString CRUDEdit<EntityT, MvcModelT>(string pageTitle, string menuPanelName = null, bool readOnly = false, bool skipBackButton = false, bool skipDeleteButton = false, bool forceNoDataAjax = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            return CRUDEdit<EntityT, MvcModelT>(new MvcHtmlString(pageTitle), menuPanelName, readOnly, skipBackButton, skipDeleteButton, forceNoDataAjax);
        }

        public MvcHtmlString CRUDEdit<EntityT, MvcModelT>(MvcHtmlString pageTitle = null, string menuPanelName = null, bool readOnly = false, bool skipBackButton = false, bool skipDeleteButton = false, bool forceNoDataAjax = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            return CRUDEditHelper<EntityT, MvcModelT>(pageTitle, menuPanelName, readOnly, skipBackButton, skipDeleteButton, forceNoDataAjax);
        }

        public MvcHtmlString CRUDEditHeader<EntityT, MvcModelT>(string pageTitle, string menuPanelName = null, bool readOnly = false, bool skipBackButton = false, bool forceNoDataAjax = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            return CRUDEditHeader<EntityT, MvcModelT>(new MvcHtmlString(pageTitle), menuPanelName, readOnly, skipBackButton, forceNoDataAjax);
        }

        public MvcHtmlString CRUDEditHeader<EntityT, MvcModelT>(MvcHtmlString pageTitle = null, string menuPanelName = null, bool readOnly = false, bool skipBackButton = false, bool forceNoDataAjax = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            var result = new StringBuilder();
            var model = (MvcModelT)_html.ViewData.Model;

            long? parentId = null;
            if (ReflectionHelper.IsClassADerivedFromClassB(model.GetType(), typeof(ChildMvcModelForEntity<,>))) parentId = (long?)model.PropertyGet("ParentId");

            //Start form
            var formHtmlAttributes = new Dictionary<string, object> { { "class", JQMobile.MvcModel.ScaffoldingSettings.FormId } };
            if (!model.IsNewModel)
            {
                formHtmlAttributes.Add("data-transition", "slide");
                formHtmlAttributes.Add("data-direction", "reverse");
            }
            if (forceNoDataAjax) formHtmlAttributes.Add("data-ajax", "false");
            result.AppendLine(_html.Supermodel().BeginMultipartFormMvcHtmlString(formHtmlAttributes).ToString());

            //Override Http Verb if needed
            if (model.Id != 0) result.AppendLine(_html.HttpMethodOverride(HttpVerbs.Put).ToString());

            //Header
            result.AppendLine("<div data-role='header' data-position='fixed'>");

            //Back button
            if (menuPanelName != null && !skipBackButton) result.AppendLine("<div data-role='controlgroup' data-type='horizontal' class='ui-mini ui-btn-left'>");
            if (menuPanelName != null) result.AppendLine($"<a href='#{menuPanelName}' class='ui-btn ui-btn-icon-right ui-icon-bars ui-btn-icon-notext'>Menu;</a>");
            if (!skipBackButton)
            {
                var htmlAttributes = new Dictionary<string, object> { {"class", "ui-btn ui-btn-icon-left ui-icon-arrow-l" },  { "data-direction", "reverse" } };
                htmlAttributes.Add("data-transition", model.IsNewModel ? "flip" : "slide");
                
                //make sure we keep query string values
                var routeValues = _html.Supermodel().QueryStringRouteValues();
                var newRouteValues = new RouteValueDictionary { { "parentId", parentId }, { "_", DateTime.Now.Ticks } };
                routeValues.Supermodel().AddOrUpdateWith(newRouteValues);

                // ReSharper disable Mvc.ActionNotResolved
                result.AppendLine(_html.ActionLink("Back", "List", routeValues, htmlAttributes).ToString());
                // ReSharper restore Mvc.ActionNotResolved

            }
            if (menuPanelName != null && !skipBackButton) result.AppendLine("</div>");

            //Title
            var titleText = "New Record";
            // ReSharper disable PossibleNullReferenceException
            if (!model.IsNewModel) titleText = !MvcHtmlString.IsNullOrEmpty(pageTitle) ? pageTitle.ToString() : model.Label;
            // ReSharper restore PossibleNullReferenceException
            result.AppendLine("<h1>" + titleText + "</h1>");

            //Update button
            if (!readOnly)
            {
                var submitText = model.IsNewModel ? "Create" : "Update";
                result.AppendLine("<button type='submit' data-icon='check' class='ui-btn-right'>" + submitText + "</button>");
            }
            result.AppendLine("</div>");

            return MvcHtmlString.Create(readOnly ? result.ToString().Supermodel().DisableAllControls() : result.ToString());
        }

        public MvcHtmlString CRUDEditFooter<EntityT, MvcModelT>(bool readOnly = false, bool skipDeleteButton = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            var result = new StringBuilder();
            var model = (MvcModelT)_html.ViewData.Model;

            long? parentId = null;
            if (ReflectionHelper.IsClassADerivedFromClassB(model.GetType(), typeof(ChildMvcModelForEntity<,>))) parentId = (long?)model.PropertyGet("ParentId");

            //End form
            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());

            //Delete button
            if (!model.IsNewModel)
            {
                result.AppendLine("<div class='" + JQMobile.MvcModel.ScaffoldingSettings.FormId + "'>");
                var htmlAttributes = new Dictionary<string, object> { { "data-role", "button" }, { "data-icon", "delete" } };
                var deleteFormHtmlAttributes = new Dictionary<string, object> { { "data-transition", "slide" }, { "data-direction", "reverse" } };

                //make sure we keep quesry string parameters: skip, take, etc.
                var routeValues = _html.Supermodel().QueryStringRouteValues();
                var newRouteValues = new RouteValueDictionary { { "id", model.Id }, { "parentId", parentId }, { "_", DateTime.Now.Ticks } };
                routeValues.Supermodel().AddOrUpdateWith(newRouteValues);

                if (!skipDeleteButton) result.AppendLine(_html.Supermodel().JQMobile.RESTfulActionLink(HttpVerbs.Delete, "Delete", "Detail", routeValues, htmlAttributes, deleteFormHtmlAttributes, "Are you sure?").ToString());
                
                result.AppendLine("</div>");
            }

            return MvcHtmlString.Create(readOnly ? result.ToString().Supermodel().DisableAllControls() : result.ToString());
        }

        private MvcHtmlString CRUDEditHelper<EntityT, MvcModelT>(MvcHtmlString pageTitle, string menuPanelName, bool readOnly, bool skipBackButton, bool skipDeleteButton, bool forceNoDataAjax)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            var result = new StringBuilder();

            result.AppendLine(CRUDEditHeader<EntityT, MvcModelT>(pageTitle, menuPanelName, readOnly, skipBackButton, forceNoDataAjax).ToString());
            result.AppendLine(_html.Supermodel().EditorForModel().ToString().Supermodel().DisableAllControlsIf(readOnly));
            result.AppendLine(CRUDEditFooter<EntityT, MvcModelT>(readOnly, skipDeleteButton).ToString());
            result.AppendLine();

            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region Page Header JQMObile helpers
        public MvcHtmlString PageHeader(string pageTitle, string menuPanelName = null)
        {
            return PageHeader(new MvcHtmlString(pageTitle), menuPanelName);
        }
        public MvcHtmlString PageHeader(MvcHtmlString pageTitle = null, string menuPanelName = null)
        {
            var result = new StringBuilder();
            result.AppendLine("<div data-role='header' data-position='fixed'>");
            if (menuPanelName != null) result.AppendLine($"<a href='#{menuPanelName}' class='ui-btn ui-btn-left ui-btn-icon-right ui-icon-bars ui-btn-icon-notext'>Menu;</a>");
            result.AppendLine($"<h1>{pageTitle}</h1>");
            result.AppendLine("</div>");

            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region CRUD List JQMobile helpers
        public MvcHtmlString CRUDList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, bool skipAddNew = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), null, skipAddNew);
        }

        public MvcHtmlString CRUDList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType = null, MvcHtmlString pageTitle = null, bool skipAddNew = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, null, skipAddNew);
        }

        public MvcHtmlString CRUDChildrenList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, long parentId, bool skipAddNew = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), parentId, skipAddNew);
        }

        public MvcHtmlString CRUDChildrenList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long parentId, bool skipAddNew = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, parentId, skipAddNew);
        }

        private MvcHtmlString CRUDListHelper<EntityT, MvcModelT>(IEnumerable<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long? parentId, bool skipAddNew)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            var controllerName = controllerType != null ?
                                     controllerType.Supermodel().GetControllerName() :
                                     _html.ViewContext.RouteData.Values["controller"].ToString();

            var result = new StringBuilder();
            if (parentId == null || parentId > 0)
            {
                result.AppendLine("<ul data-role='listview' data-inset='true' data-divider-theme='b' class='sm-crudList'>");
                result.AppendLine("<li data-role='list-divider'>");
                result.AppendLine("<div>");
                result.AppendLine("<div class='sm-crudListAddNew'>");
                if (!skipAddNew)
                {
                    //Make sure we keep quesry string values
                    var routeValues = _html.Supermodel().QueryStringRouteValues();
                    var newRouteValues = new RouteValueDictionary { { "id", 0 }, { "parentId", parentId }, { "_", DateTime.Now.Ticks } };
                    routeValues.Supermodel().AddOrUpdateWith(newRouteValues);

                    result.AppendLine(_html.ActionLink("Plus", "Detail", controllerName, routeValues, new Dictionary<string, object> { { "data-transition", "flip" }, { "data-icon", "plus" }, { "data-role", "button" }, { "data-iconpos", "notext" }, { "data-inline", "true" } }).ToString());
                }
                result.AppendLine("</div>");
                result.AppendLine("<div class='sm-crudListTitle'>");
                if (!MvcHtmlString.IsNullOrEmpty(pageTitle)) result.AppendLine(pageTitle.ToString());
                result.AppendLine("</div>");
                result.AppendLine("</div>");
                result.AppendLine("</li>");
                foreach (var item in items)
                {
                    //Make sure we keep quesry string values
                    var routeValues = _html.Supermodel().QueryStringRouteValues();
                    var newRouteValues = new RouteValueDictionary { { "id", item.Id }, { "parentId", parentId }, { "_", DateTime.Now.Ticks } };
                    routeValues.Supermodel().AddOrUpdateWith(newRouteValues);

                    result.AppendLine("<li>" + _html.ActionLink(_html.Encode(item.Label), "Detail", controllerName, routeValues, new Dictionary<string, object> { { "data-transition", "slide" } }) + "</li>");
                }
                result.AppendLine("</ul>");
            }
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion
        
        #region New Search Action Link
        public MvcHtmlString NewSearchActionLink(MvcHtmlString linkHtml = null)
        {
            linkHtml = linkHtml ?? MvcHtmlString.Create("<i class='icon-search icon-white'></i> New Search");
            var routeValues = _html.Supermodel().QueryStringRouteValues();
            var controller = _html.ViewContext.RouteData.Values["controller"].ToString();
            var htmlAttributesDict = HtmlHelper.AnonymousObjectToHtmlAttributes(new { data_role = "button", data_icon = "search", data_iconpos = "left", data_transition="slidedown" });
            return _html.Supermodel().ActionLinkHtmlContent(linkHtml, "Search", controller, routeValues, htmlAttributesDict);
        }
        #endregion

        #region CRUD Pagination Helpers
        public MvcHtmlString Pagination(int visiblePages)
        {
            var skip = _html.Supermodel().GetSkipValue();
            var take = _html.Supermodel().GetTakeValue();
            var totalCount = (long?) _html.ViewBag.SupermodelTotalCount;
            
            if (skip == null || take == null || totalCount == null) throw new ArgumentNullException();
            
            return Pagination(visiblePages, (int)skip, (int)take, (int)totalCount);
        }
        public MvcHtmlString Pagination(int visiblePages, int skipped, int taken, int totalCount)
        {
            var result = new StringBuilder();
            if (taken < totalCount)
            {
                var paginationAdditionalClass = "";
                if (!string.IsNullOrEmpty(MvcModel.ScaffoldingSettings.PaginationCssClass)) paginationAdditionalClass = " " + MvcModel.ScaffoldingSettings.PaginationCssClass;
                result.AppendLine("<div data-role='controlgroup' data-type='horizontal' data-mini='true' class='center-wrapper" + paginationAdditionalClass + "'>");

                var currentPage = skipped / taken + 1;

                var firstPage = currentPage - visiblePages / 2;
                if (firstPage < 1) firstPage = 1;

                var lastPage = firstPage + visiblePages - 1;
                if (lastPage > totalCount / taken)
                {
                    firstPage -= lastPage - totalCount / taken;
                    if (firstPage < 1) firstPage = 1;
                    lastPage = (int)Math.Ceiling((double)totalCount / taken);
                }

                //Prev page
                if (currentPage > 1) result.AppendLine(GetPageActionLink("Previous", new { data_role = "button", data_iconpos = "notext", data_icon = "arrow-l", data_transition = "flow", data_direction = "reverse" }, currentPage - 1, taken));
                else result.AppendLine("<a href='#' data-role='button' data-iconpos='notext' data-icon='arrow-l' class='ui-disabled'>Add</a>");

                //Neighboring pages
                for (var page = firstPage; page <= lastPage; page++)
                {
                    if (page < currentPage)
                    {
                        result.AppendLine(GetPageActionLink(page.ToString(CultureInfo.InvariantCulture), new { data_role = "button", data_transition = "flow", data_direction = "reverse" }, page, taken));
                    }
                    else if (page > currentPage)
                    {
                        result.AppendLine(GetPageActionLink(page.ToString(CultureInfo.InvariantCulture), new { data_role = "button", data_transition = "flow" }, page, taken));
                    }
                    else //if equal
                    {
                        result.AppendLine("<a href='#' data-role='button' class='ui-disabled'>" + page + "</a>");
                    }
                }

                //Next page
                if (currentPage < lastPage) result.AppendLine(GetPageActionLink("Previous", new { data_role = "button", data_iconpos = "notext", data_icon = "arrow-r", data_transition = "flow" }, currentPage + 1, taken));
                else result.AppendLine("<a href='#' data-role='button' data-iconpos='notext' data-icon='arrow-r' class='ui-disabled'>Add</a>");

                result.AppendLine("</div>");
            }
            return MvcHtmlString.Create(result.ToString());
        }
        public MvcHtmlString PaginationTotalRecordsCount()
        {
            return _html.Supermodel().PaginationTotalRecordsCount();
        }
        private string GetPageActionLink(string linkText, object htmlAttributes, int pageNum, int pageSize)
        {
            var routeDict = _html.ViewContext.RequestContext.HttpContext.Request.QueryString.Supermodel().ToRouteValueDictionary().
                     Supermodel().AddOrUpdateWith("smSkip", (pageNum - 1) * pageSize).
                     Supermodel().AddOrUpdateWith("_", DateTime.Now.Ticks);

            return _html.ActionLink(linkText, (string)_html.ViewContext.RouteData.Values["action"], routeDict, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes)).ToString();
            //return _html.ActionLink(linkText, (string)_html.ViewContext.RouteData.Values["action"], _html.Supermodel().QueryStringRouteValues().Supermodel().AddOrUpdateWith("skip", (pageNum - 1) * pageSize), htmlAttributes).ToString();
        }
        #endregion

        #region Static Placeholder helpers
        public MvcHtmlString Header(WebViewPage webPage, string jqVersion, string jqmVersion, bool removeiPhoneBars, string rootUrl = "http://www.supermodelframework.com/sharedwebfiles/")
        {
            rootUrl = webPage.Url.Content(rootUrl);
            
            var sb = new StringBuilder();
            sb.AppendLine("<meta name='viewport' content='width=device-width, initial-scale=1.0' />");
            if (removeiPhoneBars)
            {
                sb.AppendLine("<meta content='yes' name='apple-mobile-web-app-capable'>");  //this one is for iOS
                sb.AppendLine("<meta content='yes' name='mobile-web-app-capable'>");        //this one is for Android
                sb.AppendLine("<link href='" + rootUrl + "Supermodel/jqmobile/SupermodelJQMobileBlankStartup.png' media='(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2)' rel='apple-touch-startup-image'>");
                sb.AppendLine("<meta name='apple-mobile-web-app-status-bar-style' content='black' />");
            }
            sb.AppendLine("<meta http-equiv='Pragma' content='no-cache'>");
            sb.AppendLine("<meta http-equiv='Expires' content='-1'>");

            sb.AppendLine("<link rel='stylesheet' href='" + rootUrl + "Supermodel/jqmobile/SupermodelJQMobile.css' type='text/css'/>");

            sb.AppendLine("<link rel='stylesheet' href='http://code.jquery.com/mobile/" + jqmVersion + "/jquery.mobile-" + jqmVersion + ".min.css' />");
            sb.AppendLine("<link rel='stylesheet' type='text/css' href='http://dev.jtsage.com/cdn/simpledialog/latest/jquery.mobile.simpledialog.min.css' />");

            sb.AppendLine("<script src='http://code.jquery.com/jquery-" + jqVersion + ".min.js'></script>");
            sb.AppendLine("<script src='http://code.jquery.com/mobile/" + jqmVersion + "/jquery.mobile-" + jqmVersion + ".min.js'></script>");
            sb.AppendLine("<script src='http://dev.jtsage.com/cdn/simpledialog/latest/jquery.mobile.simpledialog2.min.js' type='text/javascript'></script>");
            sb.AppendLine("<script src='" + rootUrl + "Supermodel/jqmobile/SupermodelJQMobile.js' type='text/javascript'></script>");
            sb.AppendLine("<link rel='stylesheet' href = '" + rootUrl + "Css/Custom/site.css' type = 'text/css' />");

            return MvcHtmlString.Create(sb.ToString());
        }
        public MvcHtmlString DialogsScript(WebViewPage webPage)
        {
            var sb = new StringBuilder();

            sb.AppendLine("$(document).one('pageinit', function () {");
            if (webPage.TempData.Supermodel().NextPageStartupScript != null) sb.AppendLine(webPage.TempData.Supermodel().NextPageStartupScript);
            if (webPage.TempData.Supermodel().NextPageAlertMessage != null) sb.AppendLine("alert('" + HttpUtility.HtmlEncode(webPage.TempData.Supermodel().NextPageAlertMessage) + "');");
            if (webPage.TempData.Supermodel().NextPageModalMessage != null)
            {
                sb.AppendLine("supermodel_alert('" + HttpUtility.HtmlEncode(webPage.TempData.Supermodel().NextPageModalMessage) + "');");
            }
            sb.AppendLine("});");
            return MvcHtmlString.Create(sb.ToString());
        }
        public MvcHtmlString MasterLayout(WebViewPage webPage, string appName, string jqVersion, string jqmVersion, bool removeiPhoneBars, string rootUrl = "http://www.supermodelframework.com/sharedwebfiles/")
        {
            var sb = new StringBuilder();
            sb.AppendLine("<!DOCTYPE html>");
            sb.AppendLine("<html>");
            sb.AppendLine("<head>");
            sb.AppendLine("<title>" + appName + "</title>");
            sb.AppendLine(Header(webPage, jqVersion, jqmVersion, removeiPhoneBars, rootUrl).ToString());
            sb.AppendLine("</head> ");
            sb.AppendLine("<body>");
            // ReSharper disable PossibleNullReferenceException
            sb.AppendLine("<div data-role='page' data-url='" + webPage.Request.Url.AbsoluteUri + "'>");
            // ReSharper restore PossibleNullReferenceException

            //This is placed here intentianally if you dont' put it in the page div, it wont refresh and run every time you load
            //sb.AppendLine("<script src='" + rootUrl + "Supermodel/jqmobile/SupermodelJQMobile.js' type='text/javascript'></script>");

            sb.AppendLine("<script>" + DialogsScript(webPage) + "</script>");
            sb.AppendLine(webPage.RenderBody().ToString());
            sb.AppendLine("</div>");
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");
            return MvcHtmlString.Create(sb.ToString());
        }
        #endregion

        #region SortBy helpers
        public MvcHtmlString SortByDropdown(Extensions.Supermodel.SortByOptions sortByOptions)
        {
            return _html.Supermodel().SortByDropdown(sortByOptions);
        } 
        #endregion
    }
}
