﻿using System.ComponentModel.DataAnnotations;

namespace Supermodel.DDD.Models.View.Mvc.JQMobile
{
    public abstract partial class JQMobile
    {
        public class LoginMvcModel : MvcModel, ILoginMvcModel
        {
            public LoginMvcModel()
            {
                Username = new TextBoxForEmailMvcModel();
                Password = new TextBoxForPasswordMvcModel();
            }
            
            public TextBoxForEmailMvcModel Username { get; set; }
            public TextBoxForPasswordMvcModel Password { get; set; }

            [ScaffoldColumn(false)] public string UsernameStr
            {
                get => Username.Value;
                set => Username.Value = value;
            }
            [ScaffoldColumn(false)] public string PasswordStr
            {
                get => Password.Value;
                set => Password.Value = value;
            }
        }
    }
}