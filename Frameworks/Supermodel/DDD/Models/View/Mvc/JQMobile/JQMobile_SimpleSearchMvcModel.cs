﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View.Mvc.JQMobile
{
    public abstract partial class JQMobile
    {
        public class SimpleSearchMvcModel : MvcModel, IValidatableObject 
        {
            #region Validation
            public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
            {
                return new ValidationResultList();
            }
            #endregion

            #region Properties
            public SearchBoxMvcModel SearchTerm { get; set; } = new SearchBoxMvcModel();
            #endregion
        }
    }
}
