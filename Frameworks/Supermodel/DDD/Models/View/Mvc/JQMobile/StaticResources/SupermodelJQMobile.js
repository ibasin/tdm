﻿$(document).on('pageinit', function () {
    (function () {
        var hasConfirmed = false;
        $('input[type=submit][data-sm-ConfirmMsg], input[type=button][data-sm-ConfirmMsg], a[data-sm-ConfirmMsg], button[data-sm-ConfirmMsg]').click(function () {
            var $this = $(this);
            $this.parent().removeClass("ui-btn-active");
            if (!hasConfirmed) {
                $(document).simpledialog2({
                    mode: 'button',
                    headerText: '',
                    headerClose: false,
                    buttonPrompt: $this.attr('data-sm-ConfirmMsg'),
                    buttons: {
                        'Yes': {
                            click: function () {
                                hasConfirmed = true;
                                $this.trigger('click');
                                hasConfirmed = false;
                            },
                            theme: 'f'
                        },
                        'No': {
                            click: function () { /*do nothing*/
                            },
                            icon: 'delete',
                            theme: 'c'
                        }
                    }
                });
                $(".ui-simpledialog-container").css("z-index", 1500);
                $(".ui-simpledialog-screen").css("z-index", 1499).height($(document).height());
                return false;
            }
            if ($this[0].tagName.toLowerCase() == 'a') window.location.href = $this.attr('href');
            return true;
        });
    })();
});

function supermodel_alert(message) {
    setTimeout(function () {
        $(document).simpledialog2({
            mode: 'button',
            headerText: '',
            headerClose: false,
            buttonPrompt: message,
            buttons: {
                'Ok': { click: function () { /*do nothing*/ }, },
            }
        });
        $(".ui-simpledialog-container").css("z-index", 1500); //.css("top", (screen.height / 2 - $(".ui-simpledialog-container").height() / 2).toString() + "px")
        $(".ui-simpledialog-screen").css("z-index", 1499).height($(document).height());
    }, 300);
}

function supermodel_restfulLinkToUrlWithConfirmation(path, httpOverrideVerb, confirmationMsg) {
    if (confirmationMsg == null) {
        supermodel_restfulLinkToUrl(path, httpOverrideVerb);
    } else {
        $(document).simpledialog2({
            mode: 'button',
            headerText: '',
            headerClose: false,
            buttonPrompt: confirmationMsg,
            buttons: {
                'Yes': {
                    click: function () {
                        supermodel_restfulLinkToUrl(path, httpOverrideVerb);
                    },
                    theme: 'f'
                },
                'No': {
                    click: function () { /*do nothing*/
                    },
                    icon: 'delete',
                    theme: 'c'
                }
            }
        });
    }
}

function supermodel_restfulLinkToUrl(path, httpOverrideVerb) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", path);

    if (httpOverrideVerb.toLowerCase() != 'post') {
        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "X-HTTP-Method-Override");
        hiddenField.setAttribute("value", httpOverrideVerb);
        form.appendChild(hiddenField);
    }

    document.body.appendChild(form);
    form.submit();
}