﻿using System.ComponentModel.DataAnnotations;
using Supermodel.DDD.Models.View.Mvc.UIComponents;

namespace Supermodel.DDD.Models.View.Mvc
{
    public class LoginMvcModel : MvcModel, ILoginMvcModel
    {
        public TextBoxForStringMvcModel Username { get; set; }
        public TextBoxForPasswordMvcModel Password { get; set; }

        [ScaffoldColumn(false)] public string UsernameStr
        {
            get => Username.Value;
            set => Username.Value = value;
        }
        [ScaffoldColumn(false)] public string PasswordStr
        {
            get => Password.Value;
            set => Password.Value = value;
        }
    }
}