using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;

namespace Supermodel.DDD.Models.View.WebApi
{
    public abstract class ApiModelForEntity<EntityT> : ApiModelForAnyEntity where EntityT : class, IEntity, new()
    {
        #region Validation
        //The default implemetation just grabs domain model validation but this can be overriden
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var tempEntityForValidation = CreateTempValidationEntity();
            var vr = new ValidationResultList();
            Validator.TryValidateObject(tempEntityForValidation, new ValidationContext(tempEntityForValidation), vr); 
            return vr;
        }
        #endregion

        #region Private Helper Methods
        protected virtual EntityT CreateTempValidationEntity()
        {
            return (EntityT)this.MapToObject(new EntityT().ConstructVirtualProperties());
        }
        #endregion
    }
}