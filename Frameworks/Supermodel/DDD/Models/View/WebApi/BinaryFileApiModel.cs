﻿using System;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;

namespace Supermodel.DDD.Models.View.WebApi
{
    public class BinaryFileApiModel : IRMapperCustom, IComparable
    {
        #region Properties
        public string Name { get; set; }
        public byte[] BinaryContent { get; set; }
        #endregion

        #region Methods
        [JsonIgnore, NotRMapped] public string Extension { get { return Path.GetExtension(Name); } }
        [JsonIgnore, NotRMapped] public string FileNameWithoutExtension { get { return Path.GetFileNameWithoutExtension(Name); } }
        [JsonIgnore] public bool IsEmpty
        {
            get { return string.IsNullOrEmpty(Name) && (BinaryContent == null || BinaryContent.Length == 0); }
        }
        public void Empty()
        {
            Name = null;
            BinaryContent = null;
        }
        #endregion

        #region IComparable implemtation
        public int CompareTo(object obj)
        {
            if (!(obj is BinaryFileApiModel)) throw new SupermodelSystemErrorException("obj is not BinaryFileApiModel");
            var typedObj = obj as BinaryFileApiModel;
            if (Name == null) return 0; //if we are an empty object, we say it equals b/c then we do not override db value
            var result = string.CompareOrdinal(Name, typedObj.Name);
            return result != 0 ? result : BinaryContent.GetHashCode().CompareTo(typedObj.GetHashCode());
        }
        #endregion

        #region Overrides for Equal and Hash
        public override bool Equals(object obj)
        {
            return Equals(obj as BinaryFileApiModel);
        }
        public bool Equals(BinaryFileApiModel other)
        {
            return other != null &&
                   (Name == null && other.Name == null ||
                    Name != null && Name.Equals(other.Name)) &&
                   (BinaryContent == null && other.BinaryContent == null ||
                    BinaryContent != null && BinaryContent.SequenceEqual(other.BinaryContent));
        }
        public override int GetHashCode()
        {
            // http://stackoverflow.com/a/263416/39396
            unchecked // Overflow is fine, just wrap
            {
                var hash = 17;
                if (Name != null) hash = hash * 23 + Name.GetHashCode();
                if (BinaryContent != null) hash = hash * 23 + BinaryContent.GetHashCode();
                return hash;
            }
        }
        #endregion

        #region ICustomMapper implemtation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(BinaryFile)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

            var binaryFileObj = (BinaryFile)obj;
            Name = (binaryFileObj == null) ? "" : binaryFileObj.Name;
            BinaryContent = (binaryFileObj == null) ? new byte[0] : binaryFileObj.BinaryContent;

            return this;
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            if (objType != typeof(BinaryFile)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");

            var binaryFileObject = new BinaryFile { Name = Name, BinaryContent = BinaryContent };
            return binaryFileObject;
        }
        #endregion
    }
}
