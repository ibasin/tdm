﻿using System.Linq;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using ReflectionMapper;
using Supermodel.DDD.Models.View.Mvc;

namespace Supermodel.DDD.Models.View.WebApi
{
    public abstract class SearchApiModel : ApiModel, ISupermodelApiModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            bindingContext.Model = ReflectionHelper.CreateType(GetType());
            foreach (var property in bindingContext.Model.GetType().GetProperties().Where(p => p.GetSetMethod() != null && p.GetGetMethod() != null))
            {
                var valResult = bindingContext.ValueProvider.GetValue(property.Name);
                if (valResult != null)
                {
                    var valObj = valResult.ConvertTo(property.PropertyType);
                    if (valObj != null) bindingContext.Model.PropertySet(property.Name, valObj);
                }
            }
            return true;
        }
    }
}
