using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using ReflectionMapper;

namespace Supermodel.DDD.Models.View.WebApi
{
    public abstract class ApiModelForAnyEntity : IViewModelForEntity
    {
        #region Methods
        [NotRMapped, ScaffoldColumn(false), JsonIgnore] public bool IsNewModel { get { return Id == 0; } }
        #endregion
        
        #region ICustom mapper implementation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            return this.MapFromObjectCustomBase(obj);
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            return this.MapToObjectCustomBase(obj);
        }
        #endregion

        #region Validation
        //The default implemetation just grabs domain model validation but this can be overriden
        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            return new ValidationResultList();
        }
        #endregion

        #region Standard Properties for Web API Models
        public virtual long Id { get; set; }
        #endregion
    }
}