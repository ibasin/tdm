﻿using System;
using System.Collections.Generic;
using System.Linq;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;

namespace Supermodel.DDD.Models.View.WebApi
{
    public class ApiModelList<ApiModelForEntityT, EntityT> : List<ApiModelForEntityT>, IRMapperCustom
        where ApiModelForEntityT : ApiModelForEntity<EntityT>, new()
        where EntityT : class, IEntity, new()
    {
        #region IRMapperCustom implemtation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            Clear();
            var entityList = (ICollection<EntityT>)obj;
            foreach (var entity in entityList.ToList())
            {
                var apiModel = new ApiModelForEntityT().MapFrom(entity);
                Add(apiModel);
            }
            return this;
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            var entityList = (ICollection<EntityT>)obj;

            //Add or Update
            foreach (var apiModel in this)
            {
                EntityT entityMatch = null;
                if (!apiModel.IsNewModel) entityMatch = entityList.SingleOrDefault(x => x.Id == apiModel.Id);
                if (entityMatch != null)
                {
                    apiModel.MapTo(entityMatch);
                }
                else
                {
                    var newEntity = apiModel.MapTo(new EntityT());
                    entityList.Add(newEntity);
                }
            }

            //Delete
            foreach (var toDoItem in entityList.ToList())
            {
                if (this.All(x => x.Id != toDoItem.Id)) toDoItem.Delete();
            }

            //Set Parent for All
            //foreach (var entity in entityList)
            //{
            //    entity.ParentToDoListId = Id;
            //}

            return entityList;
        }
        #endregion
    }
}
