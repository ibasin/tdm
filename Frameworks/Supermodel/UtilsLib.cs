﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Web.Mvc;

namespace Supermodel
{
    public static class UtilsLib
    {
        //public static Dictionary<string, object> ObjectToCaseSensitiveDictionary(object values)
        //{
        //    var dict = new Dictionary<string, object>(StringComparer.Ordinal);
        //    if (values != null)
        //    {
        //        foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(values))
        //        {
        //            object val = prop.GetValue(values);
        //            dict[prop.Name] = val;
        //        }
        //    }
        //    return dict;
        //}

        public static string GenerateAttributesString(IDictionary<string, object> dict)
        {
            if (dict == null) return " ";
            var sb = new StringBuilder();
            foreach (var pair in dict) sb.AppendFormat("{0}='{1}'", pair.Key, pair.Value);
            return " " + sb.ToString().Trim() + " ";
        }

        public static MvcHtmlString MakeIdAndClassAttribues(string id, string cssClass)
        {
            return MvcHtmlString.Create(MakeIdAttribue(id).ToString() + MakeClassAttribue(cssClass));
        }

        public static MvcHtmlString MakeIdAttribue(string id)
        {
            return MvcHtmlString.Create(string.IsNullOrEmpty(id) ? "" : $" Id='{id}' ");
        }

        public static MvcHtmlString MakeClassAttribue(string cssClass)
        {
            return MvcHtmlString.Create(string.IsNullOrEmpty(cssClass) ? "" : $" class='{cssClass}' ");
        }

        public static Dictionary<KeyT, ValueT> Clone<KeyT, ValueT>(this IDictionary<KeyT, ValueT> me)
        {
            if (me == null) return null;
            var result = new Dictionary<KeyT, ValueT>();
            foreach (var pair in me)
            {
                result.Add(pair.Key, pair.Value);
            }
            return result;
        }
    }
}
