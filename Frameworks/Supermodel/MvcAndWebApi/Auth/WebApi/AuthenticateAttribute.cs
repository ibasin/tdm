﻿using System;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using Encryptor;

namespace Supermodel.MvcAndWebApi.Auth.WebApi
{
    public abstract class AuthenticateAttribute : Attribute, IAuthenticationFilter
    {
        #region IAuthenticationFilter implementation
        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            //We only authenticate we have not been authenticated with another method
            if ((context.Principal == null || 
                context.Principal.Identity == null ||
                string.IsNullOrEmpty(context.Principal.Identity.Name) || 
                context.Principal is WindowsPrincipal) && context.Request.Headers.Any(x => x.Key == AuthHeaderName))
            {
                try
                {
                    string identityName, password;
                    var results = context.Request.Headers.Where(x => x.Key == AuthHeaderName).ToList();
                    if (!results.Any()) return Task.FromResult(0);
                    var authHeader = results.Single().Value.Single();

                    if (authHeader.StartsWith("Basic "))
                    {
                        string username;
                        HttpAuthAgent.ReadBasicAuthToken(authHeader, out username, out password);
                        identityName = AuthenticateBasicAndGetIdentityName(username, password);
                    }
                    else if (authHeader.StartsWith("SMCustomEncrypted "))
                    {
                        string[] args;
                        HttpAuthAgent.ReadSMCustomEncryptedAuthToken(EncryptionKey, authHeader, out args);
                        identityName = AuthenticateEncryptedAndGetIdentityName(args, out password);
                    }
                    else
                    {
                        throw new SupermodelSystemErrorException("'" + context.Request.Headers.Authorization.Scheme + "' is invalid authorization scheme. 'Basic' or 'SMCustomEncrypted' expected");
                    }
                    
                    if (identityName != null)
                    {
                        var principal = new HttpListenerBasicPrincipal(new HttpListenerBasicIdentity(identityName, password));
                        context.Principal = principal;
                    }
                }
                // ReSharper disable once EmptyGeneralCatchClause
                catch (Exception) { } // we suppress expections if principal is not set we are not authorized
            }
            return Task.FromResult(0);
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            context.Result = new ResultWithChallenge(context.Result, Realm);
            return Task.FromResult(0);
        }
        #endregion

        #region Abstract Memebers
        protected abstract string AuthenticateBasicAndGetIdentityName(string username, string password);
        protected abstract string AuthenticateEncryptedAndGetIdentityName(string[] args, out string password);
        protected abstract byte[] EncryptionKey { get; }
        #endregion

        #region Virtual Methods
        protected virtual string AuthHeaderName { get { return "Authorization"; } }
        protected virtual string Realm { get { return "SupermodelRealm"; } }
        #endregion

        #region Properties and Fields
        public bool AllowMultiple { get { return false; } }
        #endregion
    }
}
