using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Supermodel.MvcAndWebApi.Auth.WebApi
{
    public class ResultWithChallenge : IHttpActionResult
    {
        private readonly IHttpActionResult _next;
        private readonly string _realm;

        public ResultWithChallenge(IHttpActionResult next, string realm)
        {
            _next = next;
            _realm = realm;
        }

        public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var res = await _next.ExecuteAsync(cancellationToken);
            if (res.StatusCode == HttpStatusCode.Unauthorized) res.Headers.WwwAuthenticate.Add(new AuthenticationHeaderValue("Basic", _realm));
            return res;
        }
    }
}