﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Batch;
using Supermodel.DDD.UnitOfWork;

namespace Supermodel.MvcAndWebApi.WebApiBatch
{
/*
POST http://localhost:49295/api/$batch

Content-Type: multipart/mixed; Boundary="7684979d-d8dd-40d9-9cd9-fb4420a75ee9"
Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
 
--7684979d-d8dd-40d9-9cd9-fb4420a75ee9
Content-Type: application/http; msgtype=request

GET /api/TestPerson/6 HTTP/1.1
Host: cnn.com


--7684979d-d8dd-40d9-9cd9-fb4420a75ee9
Content-Type: application/http; msgtype=request

GET /api/TestPerson/3 HTTP/1.1
Host: cnn.com


--7684979d-d8dd-40d9-9cd9-fb4420a75ee9--
*/

    public class SupermodelHttpBatchHandler<DbContextT> : HttpBatchHandler where DbContextT : class, IDbContext, new()
    {
        private const string MultiPartContentSubtype = "mixed";
        private const string ApplicationSupportedContentType = "application/json";

        public SupermodelHttpBatchHandler(HttpServer httpServer): base(httpServer)
        {
            ExecutionOrder = BatchExecutionOrder.Sequential;
            SupportedContentTypes = new List<string> { ApplicationSupportedContentType, "multipart/mixed" };
        }

        public IList<string> SupportedContentTypes { get; }
        public BatchExecutionOrder ExecutionOrder { get; set; }

        public override async Task<HttpResponseMessage> ProcessBatchAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            ValidateRequest(request);
            var subRequests = await ParseBatchRequestsAsync(request, cancellationToken);

            using (new UnitOfWork<DbContextT>())
            {
                using (var dbContextTransaction = UnitOfWorkContext<DbContextT>.CurrentDbContext.BeginTransactionIfApplies())
                {
                    try
                    {
                        var responses = await ExecuteRequestMessagesAsync(subRequests, cancellationToken);
                        var result = await CreateResponseMessageAsync(responses, request);

                        if (UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose)
                        {
                            await UnitOfWorkContext<DbContextT>.CurrentDbContext.FinalSaveChangesAsync();
                            dbContextTransaction.CommitIfApplies();
                        }
                        else
                        {
                            dbContextTransaction.RollbackIfApplies();
                        }
                        
                        return result;
                    }
                    catch (Exception ex)
                    {
                        UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                        //Try to rollback if possible, if not ignore errors
                        try { dbContextTransaction.RollbackIfApplies(); }
                        finally { throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message)); }
                    }
                    finally
                    {
                        foreach (var subRequest in subRequests)
                        {
                            request.RegisterForDispose(subRequest.GetResourcesForDisposal());
                            request.RegisterForDispose(subRequest);
                        }
                    }
                }
            }
        }

        public Task<HttpResponseMessage> CreateResponseMessageAsync(IEnumerable<HttpResponseMessage> batchResponses, HttpRequestMessage request)
        {
            if (batchResponses == null) throw new ArgumentNullException(nameof(batchResponses));
            if (request == null) throw new ArgumentNullException(nameof(request));

            var batchContent = new MultipartContent(MultiPartContentSubtype);

            foreach (var batchResponse in batchResponses)
            {
                if (!batchResponse.IsSuccessStatusCode) UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                batchContent.Add(new HttpMessageContent(batchResponse));
            }

            var response = request.CreateResponse();
            response.Content = batchContent;
            
            return Task.FromResult(response);
        }

        public async Task<IList<HttpResponseMessage>> ExecuteRequestMessagesAsync(IEnumerable<HttpRequestMessage> requests, CancellationToken cancellationToken)
        {
            if (requests == null) throw new ArgumentNullException(nameof(requests));
            var responses = new List<HttpResponseMessage>();
            try
            {
                switch (ExecutionOrder)
                {
                    case BatchExecutionOrder.Sequential:
                        foreach (var request in requests)
                        {
                            request.Headers.Add("Accept", ApplicationSupportedContentType);
                            responses.Add(await Invoker.SendAsync(request, cancellationToken));
                        }
                        break;

                    case BatchExecutionOrder.NonSequential:
                        responses.AddRange( await Task.WhenAll(requests.Select(request => Invoker.SendAsync(request, cancellationToken))));
                        break;
                }
            }
            catch
            {
                foreach (HttpResponseMessage response in responses)
                {
                    response?.Dispose();
                }
                throw;
            }

            return responses;
        }

        public async Task<IList<HttpRequestMessage>> ParseBatchRequestsAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            var requests = new List<HttpRequestMessage>();
            cancellationToken.ThrowIfCancellationRequested();
            var streamProvider = await request.Content.ReadAsMultipartAsync(cancellationToken);
            foreach (var httpContent in streamProvider.Contents)
            {
                cancellationToken.ThrowIfCancellationRequested();
                var innerRequest = await httpContent.ReadAsHttpRequestMessageAsync(cancellationToken);
                foreach (var header in request.Headers)
                {
                    if (innerRequest.Headers.All(x => x.Key != header.Key))
                    {
                        innerRequest.Headers.TryAddWithoutValidation(header.Key, header.Value);
                    }
                }
                innerRequest.CopyBatchRequestProperties(request);
                requests.Add(innerRequest);
            }
            return requests;
        }

        private void ValidateRequest(HttpRequestMessage request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            if (request.Content == null) throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.BadRequest, "BatchRequestMissingContent"));

            var contentType = request.Content.Headers.ContentType;

            if (contentType == null) throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.BadRequest, "BatchContentTypeMissing"));
            if (!SupportedContentTypes.Contains(contentType.MediaType, StringComparer.OrdinalIgnoreCase)) throw new HttpResponseException(request.CreateErrorResponse(HttpStatusCode.BadRequest, "BatchMediaTypeNotSupported"));
        }
    }
}

