﻿using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using ReflectionMapper;
using Supermodel.DDD.Models.View.Mvc;

namespace Supermodel.MvcAndWebApi.DefaultModelBinders
{
    public class SupermodelDefaultActionValueBinder : DefaultActionValueBinder
    {
        protected override HttpParameterBinding GetParameterBinding(HttpParameterDescriptor parameter)
        {
            //Check for ISupermodelApiModelBinder here, this is the highest presedence
            if (parameter.ParameterType.GetInterface(typeof(ISupermodelApiModelBinder).Name) != null)
            {
                var binder = (ISupermodelApiModelBinder)ReflectionHelper.CreateType(parameter.ParameterType);
                var valueProviderFactories = parameter.Configuration.Services.GetValueProviderFactories();
                return new ModelBinderParameterBinding(parameter, binder, valueProviderFactories);
            }

            //Otherwise, we do stock stuff
            return base.GetParameterBinding(parameter);
            
            // Attribute has the highest precedence in stock implementation. Presence of a model binder attribute overrides.
            //var parameterBinderAttribute = parameter.ParameterBinderAttribute;
            //if (parameterBinderAttribute != null) return parameterBinderAttribute.GetBinding(parameter);

            //// No attribute, so lookup in global map.
            //var parameterBindingRules = parameter.Configuration.ParameterBindingRules;
            //if (parameterBindingRules != null)
            //{
            //    var parameterBinding = parameterBindingRules.LookupBinding(parameter);
            //    if (parameterBinding != null) return parameterBinding;
            //}

            //return parameter.ActionDescriptor.SupportedHttpMethods.Contains(HttpMethod.Get) ? parameter.BindWithAttribute(new FromUriAttribute()) : base.GetParameterBinding(parameter);
        }
    }
}
