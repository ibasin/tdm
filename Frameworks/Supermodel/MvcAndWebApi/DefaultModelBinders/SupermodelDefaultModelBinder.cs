﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using ReflectionMapper;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.UIComponents;

namespace Supermodel.MvcAndWebApi.DefaultModelBinders
{
    public class SupermodelDefaultModelBinder : DefaultModelBinder
    {
        protected override void BindProperty(ControllerContext controllerContext, ModelBindingContext bindingContext, PropertyDescriptor propertyDescriptor)
        {
            // need to skip properties that aren't part of the request, else we might hit a StackOverflowException
            var fullPropertyKey = CreateSubPropertyName(bindingContext.ModelName, propertyDescriptor.Name);

            if (!bindingContext.ValueProvider.ContainsPrefix(fullPropertyKey)) return;

            // call into the property's model binder
            /*****Supermodel custom code***********/
            IModelBinder propertyBinder;
            if (propertyDescriptor.PropertyType.GetInterface(typeof(ISupermodelMvcModelBinder).Name) != null)
            {
                //For performance reasons we try to reuse property if it exists already (some properties, for example dropdowns from other entities could be quite expensive to build)
                var propertyObject = propertyDescriptor.GetValue(bindingContext.Model);
                if (propertyObject != null) propertyBinder = (IModelBinder)propertyObject;
                else propertyBinder = (IModelBinder)ReflectionHelper.CreateType(propertyDescriptor.PropertyType);
            }
            else
            {
                propertyBinder = Binders.GetBinder(propertyDescriptor.PropertyType);
            }

            /*****End Supermodel custom code*******/
            object originalPropertyValue = propertyDescriptor.GetValue(bindingContext.Model);
            var propertyMetadata = bindingContext.PropertyMetadata[propertyDescriptor.Name];
            propertyMetadata.Model = originalPropertyValue;
            var innerBindingContext = new ModelBindingContext
            {
                ModelMetadata = propertyMetadata,
                ModelName = fullPropertyKey,
                ModelState = bindingContext.ModelState,
                ValueProvider = bindingContext.ValueProvider
            };
            object newPropertyValue = GetPropertyValue(controllerContext, innerBindingContext, propertyDescriptor, propertyBinder);
            
            /*****Supermodel custom code***********/
            //BinaryFileMvcModel is a special case
            if (originalPropertyValue is BinaryFileMvcModel)
            {
                if (!((BinaryFileMvcModel)newPropertyValue).IsEmpty) propertyMetadata.Model = newPropertyValue;
                else newPropertyValue = originalPropertyValue;
            }
            else
            {
                propertyMetadata.Model = newPropertyValue;    
            }
            /*****End Supermodel custom code*******/

            // validation
            var modelState = bindingContext.ModelState[fullPropertyKey];
            if (modelState == null || modelState.Errors.Count == 0)
            {
                if (OnPropertyValidating(controllerContext, bindingContext, propertyDescriptor, newPropertyValue))
                {
                    SetProperty(controllerContext, bindingContext, propertyDescriptor, newPropertyValue);
                    OnPropertyValidated(controllerContext, bindingContext, propertyDescriptor, newPropertyValue);
                }
            }
            else
            {
                SetProperty(controllerContext, bindingContext, propertyDescriptor, newPropertyValue);

                // Convert FormatExceptions (type conversion failures) into InvalidValue messages
                foreach (var error in modelState.Errors.Where(err => string.IsNullOrEmpty(err.ErrorMessage) && err.Exception != null).ToList())
                {
                    for (var exception = error.Exception; exception != null; exception = exception.InnerException)
                    {
                        if (exception is FormatException)
                        {
                            var displayName = propertyMetadata.GetDisplayName();
                            var errorMessageTemplate = GetValueInvalidResource(controllerContext);
                            var errorMessage = string.Format(CultureInfo.CurrentCulture, errorMessageTemplate, modelState.Value.AttemptedValue, displayName);
                            modelState.Errors.Remove(error);
                            modelState.Errors.Add(errorMessage);
                            break;
                        }
                    }
                }
            }
        }
        #region Private Helpers
        private static string GetUserResourceString(ControllerContext controllerContext, string resourceName)
        {
            string result = null;

            if (!string.IsNullOrEmpty(ResourceClassKey) && (controllerContext != null) && (controllerContext.HttpContext != null))
            {
                result = controllerContext.HttpContext.GetGlobalResourceObject(ResourceClassKey, resourceName, CultureInfo.CurrentUICulture) as string;
            }

            return result;
        }
        private static string GetValueInvalidResource(ControllerContext controllerContext)
        {
            return GetUserResourceString(controllerContext, "PropertyValueInvalid") ?? "The value '{0}' is not valid for {1}";
        }
        #endregion
    }
}
