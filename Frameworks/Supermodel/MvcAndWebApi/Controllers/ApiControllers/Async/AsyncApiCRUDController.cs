﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.WebApi;
using Supermodel.DDD.UnitOfWork;
using Supermodel.MvcAndWebApi.WebApiValidation;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Async
{
    public abstract class AsyncApiCRUDController<EntityT, ApiModelT, DbContextT> : AsyncApiCRUDController<EntityT, ApiModelT, ApiModelT, DbContextT>
        where DbContextT : class, IDbContext, new()
        where EntityT : class, IEntity, new()
        where ApiModelT : ApiModelForEntity<EntityT>, new()
    { }

    public abstract class AsyncApiCRUDController<EntityT, DetailApiModelT, ListApiModelT, DbContextT> : ApiController, IAsyncApiCRUDController<DetailApiModelT, ListApiModelT>
        where DbContextT : class, IDbContext, new()
        where EntityT : class, IEntity, new()
        where DetailApiModelT : ApiModelForEntity<EntityT>, new()
        where ListApiModelT : ApiModelForEntity<EntityT>, new()
    {
        #region Action Methods
        [HttpGet, Authorize]
        public virtual async Task<HttpResponseMessage> ValidateLogin()
        {
            //Authorize attribute is the key here
            return Request.CreateResponse(HttpStatusCode.OK, await GetValidateLoginResponseAsync());        
        }
        
        [HttpGet]
        //Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
        public virtual async Task<HttpResponseMessage> All(int? smSkip = null, int? smTake = null)
        {
            using (new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.No))
            {
                var entities = await GetPagedItems(smSkip, smTake).ToListAsync();
                var apiModels = new List<ListApiModelT>();
                apiModels = (List<ListApiModelT>)apiModels.MapFromObject(entities);
                return Request.CreateResponse(HttpStatusCode.OK, apiModels);
            }
        }

        [HttpGet]
        public virtual async Task<HttpResponseMessage> CountAll(int? smSkip = null, int? smTake = null)
        {
            using (new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.No))
            {
                return Request.CreateResponse(HttpStatusCode.OK, await GetPagedItems(smSkip, smTake).CountAsync());
            }
        }

        public virtual async Task<HttpResponseMessage> Get(long id)
        {
            using (new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.No))
            {
                if (id == 0)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                    return Request.CreateResponse(HttpStatusCode.OK, new DetailApiModelT().MapFromObject(new EntityT().ConstructVirtualProperties()));
                }

                var entityItem = await GetItemOrDefaultAsync(id);
                if (entityItem == null)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Entity with id=" + id + " does not exist");
                }
                var apiModelItem = (DetailApiModelT)new DetailApiModelT().MapFromObject(entityItem);

                return Request.CreateResponse(HttpStatusCode.OK, apiModelItem);
            }
        }

        public virtual async Task<HttpResponseMessage> Delete(long id)
        {
            using (var uow = new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.Yes))
            {
                EntityT entityItem;
                try
                {
                    entityItem = await GetItemOrDefaultAsync(id);
                    if (entityItem == null)
                    {
                        UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Entity with id=" + id + " does not exist");
                    }
                    entityItem.Delete();
                }
                catch (UnableToDeleteException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return Request.CreateErrorResponse(HttpStatusCode.Conflict, ex.Message);
                }
                catch (Exception ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }

                await AfterDeleteAsync(id, entityItem);
                
                if (ReferenceEquals(uow.DbContext, UnitOfWorkContext<DbContextT>.CurrentDbContext)) await UnitOfWorkContext<DbContextT>.CurrentDbContext.FinalSaveChangesAsync();
                else await UnitOfWorkContext<DbContextT>.CurrentDbContext.SaveChangesAsync();

                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
        }

        public virtual async Task<HttpResponseMessage> Put(long id, DetailApiModelT apiModelItem)
        {
            using (var uow = new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.Yes))
            {
                if (id == 0)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "ApiCRUDController.Put: id == 0");
                }

                var entityItem = await GetItemOrDefaultAsync(id);
                if (entityItem == null)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Entity with id=" + id + " does not exist");
                }
                try
                {
                    entityItem = TryUpdateEntity(entityItem, apiModelItem);
                    if (id != apiModelItem.Id)
                    {
                        UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "ApiCRUDController.Put: Id in Url must match Id in Json");
                    }
                }
                catch (ModelStateInvalidException)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                    return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, new ValidationErrorsApiModel().MapFrom(ModelState));
                }

                await AfterUpdateAsync(id, entityItem, apiModelItem);

                if (ReferenceEquals(uow.DbContext, UnitOfWorkContext<DbContextT>.CurrentDbContext)) await UnitOfWorkContext<DbContextT>.CurrentDbContext.FinalSaveChangesAsync();
                else await UnitOfWorkContext<DbContextT>.CurrentDbContext.SaveChangesAsync();

                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        public virtual async Task<HttpResponseMessage> Post(DetailApiModelT apiModelItem)
        {
            using (var uow =new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.Yes))
            {
                var entityItem = (EntityT)new EntityT().ConstructVirtualProperties();
                try
                {
                    entityItem = TryUpdateEntity(entityItem, apiModelItem);
                    if (apiModelItem.Id != 0)
                    {
                        UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "ApiCRUDController.Post: Id in Json must be 0 or be blank");
                    }
                }
                catch (ModelStateInvalidException)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                    return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, new ValidationErrorsApiModel().MapFrom(ModelState));
                }
                entityItem.Add();

                await AfterCreateAsync(entityItem, apiModelItem);
                
                if (ReferenceEquals(uow.DbContext, UnitOfWorkContext<DbContextT>.CurrentDbContext)) await UnitOfWorkContext<DbContextT>.CurrentDbContext.FinalSaveChangesAsync();
                else await UnitOfWorkContext<DbContextT>.CurrentDbContext.SaveChangesAsync();

                return Request.CreateResponse(HttpStatusCode.OK, entityItem.Id);
            }
        }
        #endregion

        #region Protected helpers
        protected virtual Task<ValidateLoginResponseApiModel> GetValidateLoginResponseAsync()
        {
            return Task.FromResult(new ValidateLoginResponseApiModel());
        }
        protected virtual Task<EntityT> GetItemOrDefaultAsync(long id)
        {
            return GetItems().SingleOrDefaultAsync(x => x.Id == id);
        }
        protected virtual IQueryable<EntityT> GetItems()
        {
            return ControllerCommon.GetItems<EntityT>();
        }
        protected virtual IQueryable<EntityT> GetPagedItems(int? skip, int? take)
        {
            var items = GetItems();
            if (skip != null || take != null) items = ApplySkipAndTake(items.OrderBy(x => x.Id), skip, take);
            return items;
        }
        protected virtual IQueryable<EntityT> ApplySkipAndTake(IOrderedQueryable<EntityT> orderedItems, int? skip, int? take)
        {
            return ControllerCommon.ApplySkipAndTake(orderedItems, skip, take);
        }

        protected virtual Task AfterDeleteAsync(long id, EntityT entityItem)
        {
            //Do nothing
            return Task.FromResult(0);
        }
        protected virtual Task AfterUpdateAsync(long id, EntityT entityItem, DetailApiModelT apiModelItem)
        {
            //Do nothing
            return Task.FromResult(0);
        }
        protected virtual Task AfterCreateAsync(EntityT entityItem, DetailApiModelT apiModelItem)
        {
            //Do nothing
            return Task.FromResult(0);
        }
        protected virtual EntityT TryUpdateEntity(EntityT entityItem, DetailApiModelT apiModelItem)
        {
            try
            {
                //Validate apiModelItemDelta here
                var vr = apiModelItem.Validate(new ValidationContext(apiModelItem));
                if (vr.Any()) throw new ValidationResultException((ValidationResultList)vr);
                
                entityItem = (EntityT)apiModelItem.MapToObject(entityItem);
                if (ModelState.IsValid != true) throw new ModelStateInvalidException(apiModelItem);
                return entityItem;
            }
            catch (ValidationResultException ex)
            {
                foreach (var validationResult in ex.ValidationResultList)
                {
                    foreach (var memberName in validationResult.MemberNames)
                    {
                        ModelState.AddModelError(memberName, validationResult.ErrorMessage);
                    }
                }
                throw new ModelStateInvalidException(apiModelItem);
            }
        }
        #endregion
    }
}