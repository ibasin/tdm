﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Async
{
    public abstract class AsyncApiCommandController<InputT, OutputT> : ApiController, IAsyncApiCommandController<InputT, OutputT>
        where InputT : class, new()
        where OutputT : class, new()
    {
        #region Action Methods
        public virtual async Task<HttpResponseMessage> Post(InputT input)
        {
            try
            {
                var output = await ExecuteAsync(input);
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }

        }
        #endregion

        #region Abstracts
        protected abstract Task<OutputT> ExecuteAsync(InputT input);
        #endregion

    }
}
