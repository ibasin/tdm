﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Async
{
    // ReSharper disable once UnusedTypeParameter
    interface IAsyncApiCRUDController<in DetailApiModelT, in ListApiModelT>
    {
        [HttpGet, Authorize]Task<HttpResponseMessage> ValidateLogin();   
        Task<HttpResponseMessage> All(int? smSkip = null, int? smTake = null);
        [HttpGet] Task<HttpResponseMessage> CountAll(int? smSkip = null, int? smTake = null);
        Task<HttpResponseMessage> Get(long id);
        Task<HttpResponseMessage> Delete(long id);
        Task<HttpResponseMessage> Put(long id, DetailApiModelT apiModelItem);
        Task<HttpResponseMessage> Post(DetailApiModelT apiModelItem);
    }
}
