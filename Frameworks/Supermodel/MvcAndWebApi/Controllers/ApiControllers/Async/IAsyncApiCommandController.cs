﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Async
{
    interface IAsyncApiCommandController<in InputT, out OutputT>
        where InputT : class, new()
        where OutputT : class, new()
    {
        Task<HttpResponseMessage> Post(InputT input);
    }
}