﻿using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Async
{
    interface IAsyncEnhancedApiCRUDController<in DetailApiModelT, in ListApiModelT, in SearchApiModelT> : IAsyncApiCRUDController<DetailApiModelT, ListApiModelT>
    {
        [HttpGet] Task<HttpResponseMessage> Where(SearchApiModelT smSearchBy, int? smSkip = null, int? smTake = null, string smSortBy = null);
        [HttpGet] Task<HttpResponseMessage> CountWhere(SearchApiModelT smSearchBy, int? smSkip = null, int? smTake = null, string smSortBy = null);
    }
}