using System.Net.Http;
using System.Web.Http;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync
{
    interface ISyncEnhancedApiCRUDController<in DetailApiModelT, in ListApiModelT, in SearchApiModelT> : ISyncApiCRUDController<DetailApiModelT, ListApiModelT>
    {
        [HttpGet] HttpResponseMessage Where(SearchApiModelT smSearchBy, int? smSkip = null, int? smTake = null, string smSortBy = null);
        [HttpGet] HttpResponseMessage CountWhere(SearchApiModelT smSearchBy, int? smSkip = null, int? smTake = null, string smSortBy = null);
    }
}