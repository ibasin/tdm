﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.WebApi;
using Supermodel.DDD.UnitOfWork;
using Supermodel.MvcAndWebApi.WebApiValidation;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync
{
    public abstract class SyncApiCRUDController<EntityT, ApiModelT, DbContextT> : SyncApiCRUDController<EntityT, ApiModelT, ApiModelT, DbContextT>
        where DbContextT : class, IDbContext, new()
        where EntityT : class, IEntity, new()
        where ApiModelT : ApiModelForEntity<EntityT>, new()
    { }

    public abstract class SyncApiCRUDController<EntityT, DetailApiModelT, ListApiModelT, DbContextT> : ApiController, ISyncApiCRUDController<DetailApiModelT, ListApiModelT> 
        where DbContextT : class, IDbContext, new()
        where EntityT : class, IEntity, new()
        where DetailApiModelT : ApiModelForEntity<EntityT>, new()
        where ListApiModelT : ApiModelForEntity<EntityT>, new()
    {
        #region Action Methods
        [HttpGet, Authorize]
        public virtual HttpResponseMessage ValidateLogin()
        {
            //Authorize attribute is the key here
            return Request.CreateResponse(HttpStatusCode.OK, GetValidateLoginResponse());     
        }
        
        [HttpGet]
        //Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
        public virtual HttpResponseMessage All(int? smSkip = null, int? smTake = null)
        {
            using (new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.No))
            {
                var entities = GetPagedItems(smSkip, smTake).ToList();
                var apiModels = new List<ListApiModelT>();
                apiModels = (List<ListApiModelT>)apiModels.MapFromObject(entities);
                return Request.CreateResponse(HttpStatusCode.OK, apiModels);
            }
        }

        [HttpGet]
        public virtual HttpResponseMessage CountAll(int? smSkip = null, int? smTake = null)
        {
            using (new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.No))
            {
                return Request.CreateResponse(HttpStatusCode.OK, GetPagedItems(smSkip, smTake).Count());
            }
        }

        public virtual HttpResponseMessage Get(long id)
        {
            using (new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.No))
            {
                if (id == 0)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                    return Request.CreateResponse(HttpStatusCode.OK, new DetailApiModelT().MapFromObject(new EntityT().ConstructVirtualProperties()));
                }

                var entityItem = GetItemOrDefault(id);
                if (entityItem == null)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Entity with id=" + id + " does not exist");
                }
                var apiModelItem = (DetailApiModelT)new DetailApiModelT().MapFromObject(entityItem);

                return Request.CreateResponse(HttpStatusCode.OK, apiModelItem);
            }
        }

        public virtual HttpResponseMessage Delete(long id)
        {
            using (var uow = new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.Yes))
            {
                EntityT entityItem;
                try
                {
                    entityItem = GetItemOrDefault(id);
                    if (entityItem == null)
                    {
                        UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Entity with id=" + id + " does not exist");
                    }
                    entityItem.Delete();
                }
                catch (UnableToDeleteException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return Request.CreateErrorResponse(HttpStatusCode.Conflict, ex.Message);
                }
                catch (Exception ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                }

                AfterDelete(id, entityItem);
                
                if (ReferenceEquals(uow.DbContext, UnitOfWorkContext<DbContextT>.CurrentDbContext)) UnitOfWorkContext<DbContextT>.CurrentDbContext.FinalSaveChanges();
                else UnitOfWorkContext<DbContextT>.CurrentDbContext.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
        }

        public virtual HttpResponseMessage Put(long id, DetailApiModelT apiModelItem)
        {
            using (var uow = new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.Yes))
            {
                if (id == 0)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "ApiCRUDController.Put: id == 0");
                }

                var entityItem = GetItemOrDefault(id);
                if (entityItem == null)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Entity with id=" + id + " does not exist");
                }
                try
                {
                    entityItem = TryUpdateEntity(entityItem, apiModelItem);
                    if (id != apiModelItem.Id)
                    {
                        UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "ApiCRUDController.Put: Id in Url must match Id in Json");
                    }
                }
                catch (ModelStateInvalidException)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, new ValidationErrorsApiModel().MapFrom(ModelState));
                }

                AfterUpdate(id, entityItem, apiModelItem);
                
                if (ReferenceEquals(uow.DbContext, UnitOfWorkContext<DbContextT>.CurrentDbContext)) UnitOfWorkContext<DbContextT>.CurrentDbContext.FinalSaveChanges();
                else UnitOfWorkContext<DbContextT>.CurrentDbContext.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK);
            }
        }

        public virtual HttpResponseMessage Post(DetailApiModelT apiModelItem)
        {
            using (var uow = new UnitOfWorkIfNoAmbientContext<DbContextT>(MustBeWritable.Yes))
            {
                var entityItem = (EntityT)new EntityT().ConstructVirtualProperties();
                try
                {
                    entityItem = TryUpdateEntity(entityItem, apiModelItem);
                    if (apiModelItem.Id != 0)
                    {
                        UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false;
                        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "ApiCRUDController.Post: Id in Json must be 0 or be blank");
                    }
                }
                catch (ModelStateInvalidException)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, new ValidationErrorsApiModel().MapFrom(ModelState));
                }
                entityItem.Add();

                AfterCreate(entityItem, apiModelItem);
                
                if (ReferenceEquals(uow.DbContext, UnitOfWorkContext<DbContextT>.CurrentDbContext)) UnitOfWorkContext<DbContextT>.CurrentDbContext.FinalSaveChanges();
                else UnitOfWorkContext<DbContextT>.CurrentDbContext.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK, entityItem.Id);
            }
        }
        #endregion

        #region Protected helpers
        protected virtual ValidateLoginResponseApiModel GetValidateLoginResponse()
        {
            return new ValidateLoginResponseApiModel();
        }
        protected virtual EntityT GetItemOrDefault(long id)
        {
            return GetItems().SingleOrDefault(x => x.Id == id);
        }
        protected virtual IQueryable<EntityT> GetItems()
        {
            return ControllerCommon.GetItems<EntityT>();
        }
        protected virtual IQueryable<EntityT> GetPagedItems(int? skip, int? take)
        {
            var items = GetItems();
            if (skip != null || take != null) items = ApplySkipAndTake(items.OrderBy(x => x.Id), skip, take);
            return items;
        }
        protected virtual IQueryable<EntityT> ApplySkipAndTake(IOrderedQueryable<EntityT> orderedItems, int? skip, int? take)
        {
            return ControllerCommon.ApplySkipAndTake(orderedItems, skip, take);
        }

        protected virtual void AfterDelete(long id, EntityT entityItem)
        {
            //Do nothing
        }
        protected virtual void AfterUpdate(long id, EntityT entityItem, DetailApiModelT apiModelItem)
        {
            //Do nothing
        }
        protected virtual void AfterCreate(EntityT entityItem, DetailApiModelT apiModelItem)
        {
            //Do nothing
        }
        protected virtual EntityT TryUpdateEntity(EntityT entityItem, DetailApiModelT apiModelItem)
        {
            try
            {
                //Validate apiModelItemDelta here
                var vr = apiModelItem.Validate(new ValidationContext(apiModelItem));
                if (vr.Any()) throw new ValidationResultException((ValidationResultList)vr);

                entityItem = (EntityT)apiModelItem.MapToObject(entityItem);
                if (ModelState.IsValid != true) throw new ModelStateInvalidException(apiModelItem);
                return entityItem;
            }
            catch (ValidationResultException ex)
            {
                foreach (var validationResult in ex.ValidationResultList)
                {
                    foreach (var memberName in validationResult.MemberNames)
                    {
                        ModelState.AddModelError(memberName, validationResult.ErrorMessage);
                    }
                }
                throw new ModelStateInvalidException(apiModelItem);
            }
        }
        #endregion
    }
}