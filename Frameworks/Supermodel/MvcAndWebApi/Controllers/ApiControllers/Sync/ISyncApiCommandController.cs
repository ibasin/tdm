using System.Net.Http;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync
{
    interface ISyncApiCommandController<in InputT, out OutputT>
        where InputT : class, new()
        where OutputT : class, new()
    {
        HttpResponseMessage Post(InputT input);
    }
}