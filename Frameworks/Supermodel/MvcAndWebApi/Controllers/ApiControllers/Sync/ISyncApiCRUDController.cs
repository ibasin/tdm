﻿using System.Net.Http;
using System.Web.Http;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync
{
    // ReSharper disable once UnusedTypeParameter
    interface ISyncApiCRUDController<in DetailApiModelT, in ListApiModelT>
    {
        [HttpGet, Authorize]HttpResponseMessage ValidateLogin();   
        HttpResponseMessage All(int? smSkip = null, int? smTake = null);
        [HttpGet] HttpResponseMessage CountAll(int? smSkip = null, int? smTake = null);
        HttpResponseMessage Get(long id);
        HttpResponseMessage Delete(long id);
        HttpResponseMessage Put(long id, DetailApiModelT apiModelItem);
        HttpResponseMessage Post(DetailApiModelT apiModelItem);
    }
}
