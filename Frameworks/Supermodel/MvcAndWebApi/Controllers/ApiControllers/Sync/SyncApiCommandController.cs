﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync
{
    public abstract class SyncApiCommandController<InputT, OutputT> : ApiController, ISyncApiCommandController<InputT, OutputT>
        where InputT : class, new()
        where OutputT : class, new()
    {
        #region Action Methods
        public virtual HttpResponseMessage Post(InputT input)
        {
            try
            {
                var output = Execute(input);
                return Request.CreateResponse(HttpStatusCode.OK, output);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
        #endregion

        #region Abstracts
        protected abstract OutputT Execute(InputT input);
        #endregion
    }
}
