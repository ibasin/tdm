﻿namespace Supermodel.MvcAndWebApi.Controllers.ApiControllers
{
    public class ValidateLoginResponseApiModel
    {
        public long? UserId { get; set; }
        public string UserLabel { get; set; }
    }
}
