﻿using System.Linq;
using System.Web.Mvc;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Repository;
using Supermodel.Extensions;
using Supermodel.MvcAndWebApi.Controllers.MvcControllers;

namespace Supermodel.MvcAndWebApi.Controllers
{
    public static class ControllerCommon
    {
        public static IQueryable<EntityT> GetItems<EntityT>() where EntityT : class, IEntity, new()
        {
            var repo = (ILinqDataRepo<EntityT>)RepoFactory.Create<EntityT>();
            return repo.Items;
        }

        public static IOrderedQueryable<EntityT> ApplySortBy<EntityT>(IQueryable<EntityT> items, string sortBy) where EntityT : class, IEntity, new()
        {
            if (string.IsNullOrEmpty(sortBy)) return items.OrderBy(x => x.Id);

            var columnNamesToSortBy = sortBy.Split(',');
            var itemsSorted = false;
            foreach (var trimmedColumnName in columnNamesToSortBy.Select(columnName => columnName.Trim()))
            {
                if (!trimmedColumnName.StartsWith("-"))
                {
                    if (itemsSorted) items = items.Supermodel().ThenBy(trimmedColumnName);
                    else items = items.Supermodel().OrderBy(trimmedColumnName);
                }
                else
                {
                    if (itemsSorted) items = items.Supermodel().ThenByDescending(trimmedColumnName.Substring(1));
                    else items = items.Supermodel().OrderByDescending(trimmedColumnName.Substring(1));
                }
                itemsSorted = true;
            }
            return (IOrderedQueryable<EntityT>)items;
        }
        public static IQueryable<EntityT> ApplySkipAndTake<EntityT>(IOrderedQueryable<EntityT> orderedItems, int? skip, int? take)
        {
            if (skip != null) orderedItems = (IOrderedQueryable<EntityT>)orderedItems.Skip(skip.Value);
            if (take != null) orderedItems = (IOrderedQueryable<EntityT>)orderedItems.Take(take.Value);
            return orderedItems;
        }


        public static SearchMvcModelT GetSearchByUsingTryUpdate<SearchMvcModelT>(SupermodelMvcControllerCore controller) where SearchMvcModelT : MvcModel, new()
        {
            var searchBy = new SearchMvcModelT();
            try
            {
                controller.TryUpdateModelPublic(searchBy);
                if (controller.ModelState.IsValid != true) throw new ModelStateInvalidException(searchBy);
                return searchBy;
            }
            catch (ValidationResultException ex)
            {
                foreach (var validationResult in ex.ValidationResultList)
                {
                    foreach (var memberName in validationResult.MemberNames)
                    {
                        controller.ModelState.AddModelError(memberName, validationResult.ErrorMessage);
                    }
                }
                throw new ModelStateInvalidException(searchBy);
            }
        }

        public static EntityT TryUpdateEntity<EntityT, DetailMvcModelT>(SupermodelMvcControllerCore controller, EntityT entityItem, out DetailMvcModelT mvcModelItem)
            where EntityT : class, IEntity, new()
            where DetailMvcModelT : MvcModelForEntity<EntityT>, new()
        {
            mvcModelItem = (DetailMvcModelT)new DetailMvcModelT().MapFromObject(entityItem);
            try
            {
                controller.TryUpdateModelPublic(mvcModelItem);
                entityItem = (EntityT)mvcModelItem.MapToObject(entityItem);
                if (controller.ModelState.IsValid != true) throw new ModelStateInvalidException(mvcModelItem);
                return entityItem;
            }
            catch (ValidationResultException ex)
            {
                foreach (var validationResult in ex.ValidationResultList)
                {
                    foreach (var memberName in validationResult.MemberNames)
                    {
                        controller.ModelState.AddModelError(memberName, validationResult.ErrorMessage);
                    }
                }
                throw new ModelStateInvalidException(mvcModelItem);
            }
        }
        public static ChildEntityT TryUpdateEntity<ChildEntityT, ParentEntityT, ChildDetailMvcModelT>(SupermodelMvcControllerCore controller, ChildEntityT entityItem, long? parentId, out ChildDetailMvcModelT mvcModelItem)
            where ChildEntityT : class, IEntity, new()
            where ParentEntityT : class, IEntity, new()
            where ChildDetailMvcModelT : ChildMvcModelForEntity<ChildEntityT, ParentEntityT>, new()
        {
            mvcModelItem = (ChildDetailMvcModelT)new ChildDetailMvcModelT().MapFromObject(entityItem);
            if (parentId != null) mvcModelItem.ParentId = parentId;
            try
            {
                controller.TryUpdateModelPublic(mvcModelItem);
                entityItem = (ChildEntityT)mvcModelItem.MapToObject(entityItem);
                if (controller.ModelState.IsValid != true) throw new ModelStateInvalidException(mvcModelItem);
                return entityItem;
            }
            catch (ValidationResultException ex)
            {
                foreach (var validationResult in ex.ValidationResultList)
                {
                    foreach (var memeberName in validationResult.MemberNames)
                    {
                        controller.ModelState.AddModelError(memeberName, validationResult.ErrorMessage);
                    }
                }
                throw new ModelStateInvalidException(mvcModelItem);
            }
        }

        public static ActionResult GoToListScreen(SupermodelMvcControllerCore controller)
        {
            var routeValues = controller.HttpContext.Request.QueryString.Supermodel().ToRouteValueDictionary();
            return controller.RedirectToActionPublic("List", routeValues);
        }
        public static ActionResult GoToListScreen(SupermodelMvcControllerCore controller, long parentId)
        {
            var routeValues = controller.HttpContext.Request.QueryString.Supermodel().ToRouteValueDictionary();
            routeValues.Supermodel().AddOrUpdateWith("parentId", parentId);
            // ReSharper disable once Mvc.ActionNotResolved
            return controller.RedirectToActionPublic("List", routeValues);
        }

        public static ActionResult StayOnDetailScreen(SupermodelMvcControllerCore controller, long id)
        {
            var routeValues = controller.HttpContext.Request.QueryString.Supermodel().ToRouteValueDictionary();
            routeValues.Add("id", id);
            return controller.RedirectToActionPublic("Detail", routeValues);
        }

        public static bool IsGoingToList(SupermodelMvcControllerCore controller)
        {
            //if referrer does not exist, we used to throw exception, but with SSL we can't rely on Referrer
            if (controller.Request.UrlReferrer == null) return false;

            //Figure out referrer's action
            return controller.Request.UrlReferrer.AbsolutePath.ToLower().EndsWith("/list");
        }
    }
}
