﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Supermodel.MvcAndWebApi.Controllers.MvcControllers
{
    public abstract class SupermodelMvcControllerCore : Controller
    {
        //This method is needed for GetSearchByUsingTryUpdate in ControllerCommon
        [NonAction]
        public bool TryUpdateModelPublic<ModelT>(ModelT model) where ModelT : class
        {
            return TryUpdateModel(model);
        }

        [NonAction]
        public RedirectToRouteResult RedirectToActionPublic(string actionName, RouteValueDictionary routeValues)
        {
            return RedirectToAction(actionName, routeValues);
        }
    }
}
