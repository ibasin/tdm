﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.UIComponents;
using Supermodel.DDD.UnitOfWork;
using Supermodel.Extensions;

namespace Supermodel.MvcAndWebApi.Controllers.MvcControllers.Async
{
    public abstract class AsyncMvcCRUDController<EntityT, MvcModelT, DbContextT> : AsyncMvcCRUDController<EntityT, MvcModelT, MvcModelT, DbContextT>
        where EntityT : class, IEntity, new()
        where MvcModelT : MvcModelForEntity<EntityT>, new()
        where DbContextT : class, IDbContext, new()
    { }

    public abstract class AsyncMvcCRUDController<EntityT, DetailMvcModelT, ListMvcModelT, DbContextT> : SupermodelMvcControllerCore
        where EntityT : class, IEntity, new()
        where DetailMvcModelT : MvcModelForEntity<EntityT>, new()
        where ListMvcModelT : MvcModelForEntity<EntityT>, new()
        where DbContextT : class, IDbContext, new()
    {
        #region Action Methods
        public virtual async Task<ActionResult> List()
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                var entities = await GetItems().ToListAsync();
                var mvcModels = new List<ListMvcModelT>();
                mvcModels = (List<ListMvcModelT>)mvcModels.MapFromObject(entities);
                mvcModels = mvcModels.OrderBy(p => p.Label).ToList();
                return View(mvcModels);
            }
        }
        
        [HttpDelete]
        public virtual async Task<ActionResult> Detail(long id, HttpDelete ignore)
        {
            EntityT entityItem = null;
            using (new UnitOfWork<DbContextT>())
            {
                try
                {
                    entityItem = await GetItemAsync(id);
                    entityItem.Delete();
                }
                catch (UnableToDeleteException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    TempData.Supermodel().NextPageModalMessage = ex.Message;
                }
                catch (Exception)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    TempData.Supermodel().NextPageModalMessage = "PROBLEM!!!\\n\\nUnable to delete. Most likely reason: references from other entities.";
                }
                
                var result = await AfterDeleteAsync(id, entityItem);
                await UnitOfWorkContext<DbContextT>.CurrentDbContext.FinalSaveChangesAsync();
                return result;
            }
        }

        [HttpGet]
        public virtual async Task<ActionResult> Detail(long id, HttpGet ignore)
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                if (id == 0) return View(new DetailMvcModelT().MapFromObject(new EntityT().ConstructVirtualProperties()));

                var entityItem = await GetItemAsync(id);
                var mvcModelItem = (DetailMvcModelT)new DetailMvcModelT().MapFromObject(entityItem);

                return View(mvcModelItem);
            }
        }

        [HttpPut]
        public virtual async Task<ActionResult> Detail(long id, HttpPut ignore)
        {
            using (new UnitOfWork<DbContextT>())
            {
                if (id == 0) throw new SupermodelSystemErrorException("CRUDControllerBase.Detail[Put]: id == 0");

                var entityItem = await GetItemAsync(id);
                DetailMvcModelT mvcModelItem;
                try
                {
                    entityItem = TryUpdateEntity(entityItem, out mvcModelItem);
                }
                catch (ModelStateInvalidException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return View(ex.Model);
                }

                var result = await AfterUpdateAsync(id, entityItem, mvcModelItem);
                await UnitOfWorkContext<DbContextT>.CurrentDbContext.FinalSaveChangesAsync();
                return result;
            }
        }

        [HttpPost]
        public virtual async Task<ActionResult> Detail(long id, HttpPost ignore)
        {
            using (new UnitOfWork<DbContextT>())
            {
                if (id != 0) throw new SupermodelSystemErrorException("CRUDControllerBase.Detail[Post]: id != 0");

                var entityItem = (EntityT)new EntityT().ConstructVirtualProperties();
                DetailMvcModelT mvcModelItem;
                try
                {
                    entityItem = TryUpdateEntity(entityItem, out mvcModelItem);
                }
                catch (ModelStateInvalidException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return View(ex.Model);
                }
                entityItem.Add();

                var result = await AfterCreateAsync(id, entityItem, mvcModelItem);
                await UnitOfWorkContext<DbContextT>.CurrentDbContext.FinalSaveChangesAsync();
                return result;
            }
        }

        [HttpGet]
        public virtual async Task<ActionResult> GetBinaryFile(long id, string pn)
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                var mvcModelItem = (DetailMvcModelT)new DetailMvcModelT().MapFromObject(await GetItemAsync(id));
                var file = (BinaryFileMvcModel)mvcModelItem.PropertyGet(pn);
                if (file == null || file.IsEmpty) return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                const string mimeType = "application/octet-stream";
                return File(file.BinaryContent, mimeType, file.Name);
            }
        }

        [HttpGet]
        public virtual async Task<ActionResult> DeleteBinaryFile(long id, string pn)
        {
            using (new UnitOfWork<DbContextT>())
            {
                var entityItem = await GetItemAsync(id);
                var mvcModelItem = (DetailMvcModelT)new DetailMvcModelT().MapFromObject(entityItem);

                //see if pn is a required property
                if (Attribute.GetCustomAttribute(typeof(DetailMvcModelT).GetProperty(pn), typeof(RequiredAttribute), true) != null)
                {
                    TempData.Supermodel().NextPageModalMessage = "Cannot delete required field";
                    var routeValues = new RouteValueDictionary(ControllerContext.RouteData.Values);
                    routeValues["Action"] = "Detail";
                    return RedirectToRoute(routeValues);
                }

                var file = (BinaryFileMvcModel)mvcModelItem.PropertyGet(pn);

                file.Empty();
                entityItem = (EntityT)mvcModelItem.MapToObject(entityItem);

                var result = await AfterBinaryDeleteAsync(id, entityItem, mvcModelItem);
                await UnitOfWorkContext<DbContextT>.CurrentDbContext.FinalSaveChangesAsync();
                return result;
            }
        }
        #endregion

        #region Protected Methods & Properties
        protected virtual Task<ActionResult> AfterUpdateAsync(long id, EntityT entityItem, DetailMvcModelT mvcModelItem)
        {
            return Task.FromResult(GoToListScreen(id, entityItem));
        }
        protected virtual Task<ActionResult> AfterCreateAsync(long id, EntityT entityItem, DetailMvcModelT mvcModelItem)
        {
            return Task.FromResult(GoToListScreen(id, entityItem));
        }
        protected virtual Task<ActionResult> AfterDeleteAsync(long id, EntityT entityItem)
        {
            return Task.FromResult(GoToListScreen(id, entityItem));
        }
        protected virtual Task<ActionResult> AfterBinaryDeleteAsync(long id, EntityT entityItem, DetailMvcModelT mvcModelItem)
        {
            return Task.FromResult(GoToListScreen(id, entityItem));
        }

        protected virtual Task<EntityT> GetItemAsync(long id)
        {
            return GetItems().SingleAsync(x => x.Id == id);
        }
        protected virtual IQueryable<EntityT> GetItems()
        {
            return ControllerCommon.GetItems<EntityT>();
        }

        //this methods will catch validation exceptions that happen during mapping from mvc to domain (when it runs validation for mvc model by creating a domain object)
        protected virtual EntityT TryUpdateEntity(EntityT entityItem, out DetailMvcModelT mvcModelItem)
        {
            return ControllerCommon.TryUpdateEntity(this, entityItem, out mvcModelItem);
        }

        protected virtual ActionResult GoToListScreen(long id, EntityT entityItem)
        {
            return ControllerCommon.GoToListScreen(this);
        }
        protected virtual async Task<ActionResult> StayOnDetailScreenAsync(long id, EntityT entityItem, DetailMvcModelT mvcModelItem)
        {
            await UnitOfWorkContext<DbContextT>.CurrentDbContext.SaveChangesAsync();
            return ControllerCommon.StayOnDetailScreen(this, entityItem.Id);
        }
        #endregion
    }
}
