﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.UIComponents;
using Supermodel.DDD.UnitOfWork;
using Supermodel.Extensions;

namespace Supermodel.MvcAndWebApi.Controllers.MvcControllers.Async
{
    public abstract class AsyncMvcChildCRUDController<ChildEntityT, ParentEntityT, ChildDetailMvcModelT, DbContextT> : SupermodelMvcControllerCore
        where ChildEntityT : class, IEntity, new()
        where ParentEntityT : class, IEntity, new()
        where ChildDetailMvcModelT : ChildMvcModelForEntity<ChildEntityT, ParentEntityT>, new()
        where DbContextT : class, IDbContext, new()
    {
        #region Action Methods
        public abstract Task<ActionResult> List(long? parentId);

        [HttpDelete]
        public virtual async Task<ActionResult> Detail(long id, HttpDelete ignore)
        {
            ChildEntityT entityItem = null;
            using (new UnitOfWork<DbContextT>())
            {
                long? parentId = null;
                try
                {
                    entityItem = await GetItemAsync(id);
                    parentId = new ChildDetailMvcModelT().GetParentEntity(entityItem).Id;
                    entityItem.Delete();
                }
                catch (UnableToDeleteException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    TempData.Supermodel().NextPageModalMessage = ex.Message;
                }
                catch (Exception)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    TempData.Supermodel().NextPageModalMessage = "PROBLEM!!!\\n\\nUnable to delete. Most likely reason: references from other entities.";
                }
                if (parentId == null) throw new SupermodelSystemErrorException("Unknown parentId");
                return await AfterDeleteAsync(id, parentId.Value, entityItem);
            }
        }

        [HttpGet]
        public virtual async Task<ActionResult> Detail(long id, long? parentId, HttpGet ignore)
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                ChildDetailMvcModelT mvcModelItem;
                if (id == 0)
                {
                    if (parentId == null) throw new SupermodelSystemErrorException("parentId == null when id == 0");
                    mvcModelItem = new ChildDetailMvcModelT { ParentId = parentId }; //We set parentID twice, in case we may need it during MapFromObject
                    mvcModelItem = (ChildDetailMvcModelT)mvcModelItem.MapFromObject(new ChildEntityT().ConstructVirtualProperties());
                    mvcModelItem.ParentId = parentId;
                    return View(mvcModelItem);
                }

                var entityItem = await GetItemAsync(id);
                mvcModelItem = (ChildDetailMvcModelT)new ChildDetailMvcModelT().MapFromObject(entityItem);
                return View(mvcModelItem);
            }
        }

        [HttpPut]
        public virtual async Task<ActionResult> Detail(long id, HttpPut ignore)
        {
            using (new UnitOfWork<DbContextT>())
            {
                if (id == 0) throw new SupermodelSystemErrorException("CRUDControllerBase.Detail[Post]: id == 0");

                var entityItem = await GetItemAsync(id);
                ChildDetailMvcModelT mvcModelItem;
                try
                {
                    entityItem = TryUpdateEntity(entityItem, null, out mvcModelItem);
                }
                catch (ModelStateInvalidException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return View(ex.Model);
                }
                return await AfterUpdateAsync(id, entityItem, mvcModelItem);
            }
        }

        [HttpPost]
        public virtual async Task<ActionResult> Detail(long id, long parentId, HttpPost ignore)
        {
            using (new UnitOfWork<DbContextT>())
            {
                if (id != 0) throw new SupermodelSystemErrorException("CRUDControllerBase.Detail[Put]: id != 0");

                var entityItem = (ChildEntityT)new ChildEntityT().ConstructVirtualProperties();
                ChildDetailMvcModelT mvcModelItem;
                try
                {
                    entityItem = TryUpdateEntity(entityItem, parentId, out mvcModelItem);
                }
                catch (ModelStateInvalidException ex)
                {
                    UnitOfWorkContext<DbContextT>.CurrentDbContext.CommitOnDispose = false; //rollback the transaction
                    return View(ex.Model);
                }
                entityItem.Add();
                return await AfterCreateAsync(id, parentId, entityItem, mvcModelItem);
            }
        }

        [HttpGet]
        public virtual async Task<ActionResult> GetBinaryFile(long id, string pn)
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                var mvcModelItem = (ChildDetailMvcModelT)new ChildDetailMvcModelT().MapFromObject(await GetItemAsync(id));
                var file = (BinaryFileMvcModel)mvcModelItem.PropertyGet(pn);
                const string mimeType = "application/octet-stream";
                //var mimeType = Common.GetContentTypeHeader(file.Name);
                if (file == null || file.IsEmpty) return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                return File(file.BinaryContent, mimeType, file.Name);
            }
        }

        [HttpGet]
        public virtual async Task<ActionResult> DeleteBinaryFile(long id, string pn)
        {
            using (new UnitOfWork<DbContextT>())
            {
                var entityItem = await GetItemAsync(id);
                var mvcModelItem = (ChildDetailMvcModelT)new ChildDetailMvcModelT().MapFromObject(entityItem);

                //see if pn is a required property
                if (Attribute.GetCustomAttribute(typeof(ChildDetailMvcModelT).GetProperty(pn), typeof(RequiredAttribute), true) != null)
                {
                    TempData.Supermodel().NextPageModalMessage = "Cannot delete required field";
                    var routeValues = new RouteValueDictionary(ControllerContext.RouteData.Values);
                    routeValues["Action"] = "Detail";
                    return RedirectToRoute(routeValues);
                }

                var file = (BinaryFileMvcModel)mvcModelItem.PropertyGet(pn);

                file.Empty();
                entityItem = (ChildEntityT)mvcModelItem.MapToObject(entityItem);
                return await AfterBinaryDeleteAsync(id, entityItem, mvcModelItem);
            }
        }
        #endregion

        #region Protected Methods & Properties
        protected virtual Task<ActionResult> AfterUpdateAsync(long id, ChildEntityT entityItem, ChildDetailMvcModelT mvcModelItem)
        {
            return Task.FromResult(GoToListScreen(mvcModelItem.GetParentEntity(entityItem).Id));
        }
        protected virtual Task<ActionResult> AfterCreateAsync(long id, long parentId, ChildEntityT entityItem, ChildDetailMvcModelT mvcModelItem)
        {
            return Task.FromResult(GoToListScreen(mvcModelItem.GetParentEntity(entityItem).Id));
        }
        protected virtual Task<ActionResult> AfterDeleteAsync(long id, long parentId, ChildEntityT entityItem)
        {
            return Task.FromResult(GoToListScreen(parentId));
        }
        protected virtual async Task<ActionResult> AfterBinaryDeleteAsync(long id, ChildEntityT entityItem, ChildDetailMvcModelT mvcModelItem)
        {
            return await StayOnDetailScreenAsync(entityItem);
        }

        protected virtual IQueryable<ChildEntityT> GetItems()
        {
            return ControllerCommon.GetItems<ChildEntityT>();
        }
        protected virtual Task<ChildEntityT> GetItemAsync(long id)
        {
            return GetItems().SingleAsync(x => x.Id == id);
        }

        //this methods will catch validation exceptions that happen during mapping from mvc to domain (when it runs validation for mvc model by creating a domain object)
        protected virtual ChildEntityT TryUpdateEntity(ChildEntityT entityItem, long? parentId, out ChildDetailMvcModelT mvcModelItem)
        {
            return ControllerCommon.TryUpdateEntity<ChildEntityT, ParentEntityT, ChildDetailMvcModelT>(this, entityItem, parentId, out mvcModelItem);
        }

        protected ActionResult GoToListScreen(long parentId)
        {
            return ControllerCommon.GoToListScreen(this, parentId);
        }
        protected async Task<ActionResult> StayOnDetailScreenAsync(ChildEntityT entityItem)
        {
            await UnitOfWorkContext<DbContextT>.CurrentDbContext.SaveChangesAsync();
            return ControllerCommon.StayOnDetailScreen(this, entityItem.Id);
        }
        #endregion
    }
}
