﻿using System.Data.Entity;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Repository;
using Supermodel.DDD.UnitOfWork;
using Supermodel.Extensions;

namespace Supermodel.MvcAndWebApi.Controllers.MvcControllers.Async
{
    public abstract class AsyncMvcAuthController<UserEntityT, LoginMvcModelT, DbContextT> : Controller
        where UserEntityT : UserEntity<UserEntityT, DbContextT>, new()
        where LoginMvcModelT : class, ILoginMvcModel, new()
        where DbContextT : class, IDbContext, new()
    {
        #region Action Methods
        [HttpGet]
        public ActionResult LogIn()
        {
            return View(new LoginMvcModelT());
        }

        [HttpPost]
        public async Task<ActionResult> LogIn(LoginMvcModelT login, string returnUrl)
        {
            FormsAuthentication.SignOut();
            HttpContext.User = null;
            UserEntityT user;
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                var repo = (ILinqDataRepo<UserEntityT>)RepoFactory.Create<UserEntityT>();
                user = await repo.Items.SingleOrDefaultAsync(u => u.Username == login.UsernameStr);
                
                //Login
                if (user == null) //if user does not exist
                {
                    SetNextPageMessage("Username and password combination is incorrect!");
                    login.PasswordStr = "";
                    LoginAttempted(login.UsernameStr, login.PasswordStr, false);
                    return View(login);
                }

                if (!user.PasswordEquals(login.PasswordStr)) //if password does not match
                {
                    SetNextPageMessage("Username and password combination is incorrect!");
                    login.PasswordStr = "";
                    LoginAttempted(login.UsernameStr, login.PasswordStr, false);
                    return View(login);
                }

                if (!IsUserAllowedToLogIn(user))
                {
                    SetNextPageMessage("Username and password combination is incorrect!");
                    login.PasswordStr = "";
                    LoginAttempted(login.UsernameStr, login.PasswordStr, false);
                    return View(login);
                }
            }

            LoginAttempted(login.UsernameStr, login.PasswordStr, true);

            //All is good. Log the user in
            //This is intentionally outside of the scope of the Unit of Work. This unit of work is used to create the database and because we overload SaveChanges,
            //we need to make sure user is not authenticated when the db is being created. Otherwise we get into an infinite recursion
            FormsAuthentication.SetAuthCookie(user.Id.ToString(CultureInfo.InvariantCulture), false);
            
            return string.IsNullOrEmpty(returnUrl) ? RedirectToHomeScreen(user) : Redirect(returnUrl);
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            // ReSharper disable once Mvc.ActionNotResolved
            return RedirectToAction("LogIn");
        }
        #endregion

        #region Methods for Overrdies
        protected virtual void LoginAttempted(string username, string password, bool success) {}
        protected virtual bool IsUserAllowedToLogIn(UserEntityT user)
        {
            return true;
        }
        protected virtual void SetNextPageMessage(string message)
        {
            TempData.Supermodel().NextPageAlertMessage = message;
        }
        protected abstract ActionResult RedirectToHomeScreen(UserEntityT user);
        #endregion
    }
}
