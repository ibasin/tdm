﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.UnitOfWork;

namespace Supermodel.MvcAndWebApi.Controllers.MvcControllers.Sync
{
    public abstract class SyncEnhancedMvcCRUDController<EntityT, MvcModelT, SearchMvcModelT, DbContextT> : SyncEnhancedMvcCRUDController<EntityT, MvcModelT, MvcModelT, SearchMvcModelT, DbContextT>
        where EntityT : class, IEntity, new()
        where MvcModelT : MvcModelForEntity<EntityT>, new()
        where SearchMvcModelT : MvcModel, new()
        where DbContextT : class, IDbContext, new()
    {}

    public abstract class SyncEnhancedMvcCRUDController<EntityT, DetailMvcModelT, ListMvcModelT, SearchMvcModelT, DbContextT> : SyncMvcCRUDController<EntityT, DetailMvcModelT, ListMvcModelT, DbContextT>
        where EntityT : class, IEntity, new()
        where DetailMvcModelT : MvcModelForEntity<EntityT>, new()
        where ListMvcModelT : MvcModelForEntity<EntityT>, new()
        where SearchMvcModelT : MvcModel, new()
        where DbContextT : class, IDbContext, new()
    {
        #region Action Methods
        public virtual ActionResult Search(int? smTake = null, string smSortBy = null)
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                SearchMvcModelT searchBy;
                try
                {
                    searchBy = GetSearchByUsingTryUpdate();
                }
                catch (ModelStateInvalidException ex)
                {
                    return View("Search", ex.Model);
                }
                return View(searchBy);
            }
        }
        // ReSharper disable once MethodOverloadWithOptionalParameter
        public virtual ActionResult List(int? smSkip = null, int? smTake = null, string smSortBy = null)
        {
            using (new UnitOfWork<DbContextT>(ReadOnly.Yes))
            {
                SearchMvcModelT searchBy;
                try
                {
                    searchBy = GetSearchByUsingTryUpdate();
                }
                catch (ModelStateInvalidException ex)
                {
                    if (IsGoingToList())
                    {
                        SetUpPagingViewBag(0);
                        return View("List", new ListWithCriteria<ListMvcModelT, SearchMvcModelT> { Criteria = (SearchMvcModelT)ex.Model });
                    }
                    return View("Search", ex.Model);
                }

                var items = GetItems();
                items = ApplySearchBy(items, searchBy);
                items = ApplySortBy(items, smSortBy);
                var itemsBeforeSkipAndTake = items; //save items for count
                items = ApplySkipAndTake((IOrderedQueryable<EntityT>)items, smSkip, smTake);

                var entities = items.ToList();
                var mvcModels = new ListWithCriteria<ListMvcModelT, SearchMvcModelT> { Criteria = searchBy };
                mvcModels = (ListWithCriteria<ListMvcModelT, SearchMvcModelT>)mvcModels.MapFromObject(entities);

                SetUpPagingViewBag(itemsBeforeSkipAndTake.Count());
                return View(mvcModels);
            }
        }
        [NonAction]
        public override ActionResult List()
        {
            return new HttpStatusCodeResult(HttpStatusCode.NotFound);
        }
        #endregion

        #region Protected Helpers
        protected SearchMvcModelT GetSearchByUsingTryUpdate()
        {
            return ControllerCommon.GetSearchByUsingTryUpdate<SearchMvcModelT>(this);
        }
        
        protected virtual IQueryable<EntityT> GetPagedSortedAndSearchedItems(int? skip, int? take, string sortBy, SearchMvcModelT searchBy)
        {
            var items = GetItems();
            items = ApplySearchBy(items, searchBy);
            items = ApplySortBy(items, sortBy);
            items = ApplySkipAndTake((IOrderedQueryable<EntityT>)items, skip, take);

            return items;
        }
        protected virtual IQueryable<EntityT> ApplySearchBy(IQueryable<EntityT> items, SearchMvcModelT searchBy)
        {
            return items;
        }

        protected virtual IOrderedQueryable<EntityT> ApplySortBy(IQueryable<EntityT> items, string sortBy)
        {
            return ControllerCommon.ApplySortBy(items, sortBy);
        }
        protected virtual IQueryable<EntityT> ApplySkipAndTake(IOrderedQueryable<EntityT> orderedItems, int? skip, int? take)
        {
            return ControllerCommon.ApplySkipAndTake(orderedItems, skip, take);
        }

        protected virtual void SetUpPagingViewBag(int totalCount)
        {
            ViewBag.SupermodelTotalCount = totalCount;
        }

        protected virtual bool IsGoingToList()
        {
            return ControllerCommon.IsGoingToList(this);
        }
        #endregion
    }
}
