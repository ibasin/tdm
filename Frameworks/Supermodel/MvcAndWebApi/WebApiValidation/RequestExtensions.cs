﻿using System;
using System.Net;
using System.Net.Http;

namespace Supermodel.MvcAndWebApi.WebApiValidation
{
    public static class RequestExtensions
    {
        public static HttpResponseMessage CreateErrorResponse(this HttpRequestMessage request, HttpStatusCode statusCode, ValidationErrorsApiModel validationErrors)
        {
            if (request == null) throw new ArgumentNullException("request");
            return request.CreateResponse(statusCode, validationErrors);
        }
    }
}