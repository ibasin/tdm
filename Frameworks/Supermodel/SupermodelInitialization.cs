﻿using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Supermodel.DDD.Models.View.Mvc.Metadata;
using Supermodel.DDD.UnitOfWork;
using Supermodel.MvcAndWebApi.Auth.WebApi;
using Supermodel.MvcAndWebApi.DefaultModelBinders;
using Supermodel.MvcAndWebApi.WebApiBatch;

namespace Supermodel
{
    public static class SupermodelInitialization
    {
        public static void WebInit<BatchDbContextT>(ModelMetadataProvider customModelMetadataProvider = null, IModelBinder defaultModelBinder = null, IActionValueBinder defaultActionValueBinder = null, AuthenticateAttribute webApiAuthFilter = null)
            where BatchDbContextT : class, IDbContext, new()
        {
            ModelMetadataProviders.Current = customModelMetadataProvider ?? new SupermodelTemplateMetadataProvider();
            ModelBinders.Binders.DefaultBinder = defaultModelBinder ?? new SupermodelDefaultModelBinder();
            GlobalConfiguration.Configuration.Services.Replace(typeof (IActionValueBinder), defaultActionValueBinder ?? new SupermodelDefaultActionValueBinder());

            //Json.Net config
            var jsonNetSettings = GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings;
            jsonNetSettings.Converters.Add(new StringEnumConverter());
            jsonNetSettings.MissingMemberHandling = MissingMemberHandling.Error;

            //Web Api stuff
            GlobalConfiguration.Configuration.Routes.MapHttpBatchRoute
            (
                routeName: "BatchApi",
                routeTemplate: "api/$batch",
                batchHandler: new SupermodelHttpBatchHandler<BatchDbContextT>(GlobalConfiguration.DefaultServer));

            GlobalConfiguration.Configuration.Routes.MapHttpRoute
            (
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional },
                constraints: new { id = @"(^$)|(\d+$)" }
            );

            GlobalConfiguration.Configuration.Routes.MapHttpRoute
            (
                name: "DefaultApiWithAction",
                routeTemplate: "api/{controller}/{action}"
            );

            //Mvc Stuff
            RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            RouteTable.Routes.MapRoute
            (
                name: "DefaultMvc",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            //Http authentication set up
            if (webApiAuthFilter != null) GlobalConfiguration.Configuration.Filters.Add(webApiAuthFilter);

            //Initialize store if context needs to do it explicitly
            new BatchDbContextT().InitStorageIfApplies();
        }
    }
}
