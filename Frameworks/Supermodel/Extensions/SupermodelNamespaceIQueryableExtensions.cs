﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Supermodel.Extensions
{
    public class SupermodelNamespaceIQueryableExtensions<T>
    {
        #region Constructors
        public SupermodelNamespaceIQueryableExtensions(IQueryable<T> queryableSource)
        {
            _queryableSource = queryableSource;
        }
        #endregion

        #region Dynamic Sort methods
        public IOrderedQueryable<T> OrderBy(string property)
        {
            return ApplyOrder(property, "OrderBy");
        }
        public IOrderedQueryable<T> OrderByDescending(string property)
        {
            return ApplyOrder(property, "OrderByDescending");
        }
        public IOrderedQueryable<T> ThenBy(string property)
        {
            return ApplyOrder(property, "ThenBy");
        }
        public IOrderedQueryable<T> ThenByDescending(string property)
        {
            return ApplyOrder(property, "ThenByDescending");
        }
        private IOrderedQueryable<T> ApplyOrder(string property, string methodName)
        {
            var props = property.Split('.');
            var type = typeof(T);
            var arg = Expression.Parameter(type, "x");
            var expr = (Expression)arg;
            foreach (var prop in props)
            {
                var propertyInfo = type.GetProperty(prop);
                expr = Expression.Property(expr, propertyInfo);
                type = propertyInfo.PropertyType;
            }
            var delegateType = typeof(Func<,>).MakeGenericType(typeof(T), type);
            var lambda = Expression.Lambda(delegateType, expr, arg);

            var result = typeof(Queryable).GetMethods().Single(
                method => method.Name == methodName
                          && method.IsGenericMethodDefinition
                          && method.GetGenericArguments().Length == 2
                          && method.GetParameters().Length == 2)
                .MakeGenericMethod(typeof(T), type)
                .Invoke(null, new object[] { _queryableSource, lambda });
            return (IOrderedQueryable<T>)result;
        } 
        #endregion

        #region Private Context
        private readonly IQueryable<T> _queryableSource;
        #endregion
    }
}