﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Routing;

namespace Supermodel.Extensions
{
    public class SupermodelNamespaceControllerExtensions<TControllerT> where TControllerT : Controller
    {
        #region Constructors
        public SupermodelNamespaceControllerExtensions(Controller controller)
        {
            _controller = controller;
        }
	    #endregion

        #region RedirectToActionStrong
        public RedirectToRouteResult RedirectToActionStrong(Expression<Action<TControllerT>> action, RouteValueDictionary routeValues = null)
        {
            return RedirectToActionStrongInternal(action, routeValues);
        }
        public ActionResult RedirectToActionStrong<T>(Expression<Action<T>> action, RouteValueDictionary routeValues = null)
        {
            return RedirectToActionStrongInternalGeneric(action, routeValues);
        }
        private RedirectToRouteResult RedirectToActionStrongInternalGeneric<T>(Expression<Action<T>> action, RouteValueDictionary routeValues)
        {
            routeValues = routeValues ?? new RouteValueDictionary();

            var methodExpression = action.Body as MethodCallExpression;
            if (methodExpression == null) throw new InvalidOperationException("Expression must be a method call.");
            if (methodExpression.Object != action.Parameters[0]) throw new InvalidOperationException("Method call must target lambda argument.");

            var actionName = methodExpression.Method.Name;
            var controllerName = typeof(T).Supermodel().GetControllerName();

            routeValues.Supermodel().AddOrUpdateWith("controller", controllerName);

            var parameters = methodExpression.Method.GetParameters();
            for (var i = 0; i < parameters.Length; i++)
            {
                var param = parameters[i];
                var argumentExpression = methodExpression.Arguments[i];
                var argument = Expression.Lambda(argumentExpression).Compile().DynamicInvoke();
                if (param.Name != "ignore") routeValues.Supermodel().AddOrUpdateWith(param.Name, argument);
            }
            if (actionName.EndsWith("Async")) actionName = actionName.Substring(0, actionName.Length - 5);
            return new RedirectToRouteResult(MergeRouteValues(actionName, controllerName, null, routeValues, true));
        }
        private RedirectToRouteResult RedirectToActionStrongInternal(Expression<Action<TControllerT>> action, RouteValueDictionary routeValues)
        {
            routeValues = routeValues ?? new RouteValueDictionary();

            var methodExpression = action.Body as MethodCallExpression;
            if (methodExpression == null) throw new InvalidOperationException("Expression must be a method call.");
            if (methodExpression.Object != action.Parameters[0]) throw new InvalidOperationException("Method call must target lambda argument.");

            var actionName = methodExpression.Method.Name;
            var controllerName = typeof(TControllerT).Supermodel().GetControllerName();

            routeValues.Supermodel().AddOrUpdateWith("controller", controllerName);

            var parameters = methodExpression.Method.GetParameters();
            for (var i = 0; i < parameters.Length; i++)
            {
                var param = parameters[i];
                var argumentExpression = methodExpression.Arguments[i];
                var argument = Expression.Lambda(argumentExpression).Compile().DynamicInvoke();

                if (argument != null && argument.GetType().IsClass && argument.GetType() != typeof(string))
                {
                    var argumentAsRouteVlues = HtmlHelper.AnonymousObjectToHtmlAttributes(argument);
                    routeValues.Supermodel().AddOrUpdateWith(argumentAsRouteVlues);
                }
                else
                {
                    routeValues.Supermodel().AddOrUpdateWith(param.Name, argument);
                }
            }
            if (actionName.EndsWith("Async")) actionName = actionName.Substring(0, actionName.Length - 5);
            return new RedirectToRouteResult(MergeRouteValues(actionName, controllerName, null, routeValues, true));
        }
        #endregion

        #region Private helpers
        private static RouteValueDictionary MergeRouteValues(string actionName, string controllerName, RouteValueDictionary implicitRouteValues, RouteValueDictionary routeValues, bool includeImplicitMvcValues)
        {
            var routeValueDictionary = new RouteValueDictionary();
            if (includeImplicitMvcValues)
            {
                object obj;
                if (implicitRouteValues != null && implicitRouteValues.TryGetValue("action", out obj)) routeValueDictionary["action"] = obj;
                if (implicitRouteValues != null && implicitRouteValues.TryGetValue("controller", out obj)) routeValueDictionary["controller"] = obj;
            }
            if (routeValues != null)
            {
                foreach (KeyValuePair<string, object> keyValuePair in GetRouteValues(routeValues)) routeValueDictionary[keyValuePair.Key] = keyValuePair.Value;
            }
            if (actionName != null) routeValueDictionary["action"] = actionName;
            if (controllerName != null) routeValueDictionary["controller"] = controllerName;
            return routeValueDictionary;
        }
        private static RouteValueDictionary GetRouteValues(RouteValueDictionary routeValues)
        {
            return routeValues == null ? new RouteValueDictionary() : new RouteValueDictionary(routeValues);
        }
        #endregion

        #region Private Context
        // ReSharper disable NotAccessedField.Local
        private readonly Controller _controller;
        // ReSharper restore NotAccessedField.Local
        #endregion
    }
}
