﻿using System.Collections.Specialized;
using System.Web.Routing;

namespace Supermodel.Extensions
{
    public class SupermodelNamespaceNameValueCollectionExtensions
    {

        public SupermodelNamespaceNameValueCollectionExtensions(NameValueCollection nvCollection)
        {
            _nvCollection = nvCollection;
        }

        public RouteValueDictionary ToRouteValueDictionary()
        {
            var dict = new RouteValueDictionary();
            foreach (string key in _nvCollection.Keys)
            {
                if (key == null) continue;
                var value = _nvCollection[key].TrimEnd().EndsWith(",") ? _nvCollection[key].Supermodel().RemoveStartingLast(",") : _nvCollection[key];
                dict.Add(key, value);
            }
            return dict;
        }

        public int? GetSkipValue()
        {
            int value;
            if (int.TryParse(_nvCollection["smSkip"], out value)) return value;
            else return null;
        }

        public int? GetTakeValue()
        {
            int value;
            if (int.TryParse(_nvCollection["smTake"], out value)) return value;
            else return null;
        }

        public string GetSortByValue()
        {
            return _nvCollection["smSkipBy"];
        }

        private readonly NameValueCollection _nvCollection;
    }
}