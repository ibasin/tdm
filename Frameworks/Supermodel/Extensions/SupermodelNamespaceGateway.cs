﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace Supermodel.Extensions
{
    public static class SupermodelNamespaceGateway
    {
        #region Supermodel namespace gateway
        public static SupermodelNamespaceControllerTypeExtensions Supermodel(this Type type)
        {
            return new SupermodelNamespaceControllerTypeExtensions(type);
        }
        
        public static SupermodelNamespaceControllerExtensions<ControllerT> Supermodel<ControllerT>(this ControllerT controller) where ControllerT : Controller
        {
            return new SupermodelNamespaceControllerExtensions<ControllerT>(controller);
        }
        public static SupermodelNamespaceControllerExtensions<ControllerT> Supermodel<ControllerT>(this Controller controller) where ControllerT : Controller
        {
            return new SupermodelNamespaceControllerExtensions<ControllerT>(controller);
        }
        
        public static SupermodelNamespaceHtmlHelperExtensions<dynamic> Supermodel(this HtmlHelper html)
        {
            return new SupermodelNamespaceHtmlHelperExtensions<dynamic>(html);
        }
        public static SupermodelNamespaceHtmlHelperExtensions<ModelT> Supermodel<ModelT>(this HtmlHelper<ModelT> html)
        {
            return new SupermodelNamespaceHtmlHelperExtensions<ModelT>(html);
        }

        public static SupermodelNamespaceStringExtensions Supermodel(this string str)
        {
            return new SupermodelNamespaceStringExtensions(str);
        }
        public static SupermodelNamespaceMvcHtmlStringExtensions Supermodel(this MvcHtmlString mvcStr)
        {
            return new SupermodelNamespaceMvcHtmlStringExtensions(mvcStr);
        }

        public static SupermodelNamespaceTempDataDictionaryExtensions Supermodel(this TempDataDictionary tempData)
        {
            return new SupermodelNamespaceTempDataDictionaryExtensions(tempData);
        }

        public static SupermodelNamespaceRouteValueDictionaryExtensions Supermodel(this RouteValueDictionary rvDict)
        {
            return new SupermodelNamespaceRouteValueDictionaryExtensions(rvDict);
        }

        public static SupermodelNamespaceNameValueCollectionExtensions Supermodel(this NameValueCollection nvCollection)
        {
            return new SupermodelNamespaceNameValueCollectionExtensions(nvCollection);
        }

        public static SupermodelNamespaceIQueryableExtensions<T> Supermodel<T>(this IQueryable<T> queryableSource)
        {
            return new SupermodelNamespaceIQueryableExtensions<T>(queryableSource);
        }
        #endregion
    }
}
