﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.JQMobile;
using Supermodel.DDD.Models.View.Mvc.TwitterBS;
using Supermodel.DDD.Models.View.Mvc.UIComponents;

namespace Supermodel.Extensions
{
    public class SupermodelNamespaceHtmlHelperExtensions<ModelT>
    {
        #region Constructors
		public SupermodelNamespaceHtmlHelperExtensions(HtmlHelper html)
        {
            _html = html;
        }
	    #endregion

        #region JQModel Namespace gatweay
        public SupermodelJQMobileNamespaceHtmlHelperExtensions<ModelT> JQMobile => new SupermodelJQMobileNamespaceHtmlHelperExtensions<ModelT>(_html);

        #endregion

        #region TwitterBS Namespace
        public SupermodelTwitterBSNamespaceHtmlHelperExtensions<ModelT> TwitterBS => new SupermodelTwitterBSNamespaceHtmlHelperExtensions<ModelT>(_html);

        #endregion

        #region Strongly-typed ActionLink
        public MvcHtmlString ActionLinkStrong<ControllerT>(string linkText, Expression<Func<ControllerT, Task<ActionResult>>> action, object routeValues, object htmlAttributes) where ControllerT : IController
        {
            return ActionLinkStrong(linkText, action, HtmlHelper.AnonymousObjectToHtmlAttributes(routeValues), HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcHtmlString ActionLinkStrong<ControllerT>(string linkText, Expression<Func<ControllerT, Task<ActionResult>>> action, RouteValueDictionary routeValues = null, IDictionary<string, object> htmlAttributes = null) where ControllerT : IController
        {
            routeValues = routeValues ?? new RouteValueDictionary();

            var methodExpression = action.Body as MethodCallExpression;
            if (methodExpression == null) throw new InvalidOperationException("Expression must be a method call.");

            var actionName = methodExpression.Method.Name;

            var controllerName = typeof(ControllerT).Supermodel().GetControllerName();
            routeValues.Supermodel().AddOrUpdateWith("controller", controllerName);

            var parameters = methodExpression.Method.GetParameters();
            for (var i = 0; i < parameters.Length; i++)
            {
                var param = parameters[i];
                var argumentExpression = methodExpression.Arguments[i];
                var argument = Expression.Lambda(argumentExpression).Compile().DynamicInvoke();

                if (argument != null && argument.GetType().IsClass && argument.GetType() != typeof(string))
                {
                    var argumentAsRouteVlues = HtmlHelper.AnonymousObjectToHtmlAttributes(argument);
                    routeValues.Supermodel().AddOrUpdateWith(argumentAsRouteVlues);
                }
                else
                {
                    routeValues.Supermodel().AddOrUpdateWith(param.Name, argument);
                }
            }
            if (actionName.EndsWith("Async")) actionName = actionName.Substring(0, actionName.Length - 5);
            return _html.ActionLink(linkText, actionName, routeValues, htmlAttributes);
        }
        public MvcHtmlString ActionLinkStrong<ControllerT>(string linkText, Expression<Func<ControllerT, ActionResult>> action, object routeValues, object htmlAttributes) where ControllerT : IController
        {
            return ActionLinkStrong(linkText, action, HtmlHelper.AnonymousObjectToHtmlAttributes(routeValues), HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcHtmlString ActionLinkStrong<ControllerT>(string linkText, Expression<Func<ControllerT, ActionResult>> action, RouteValueDictionary routeValues = null, IDictionary<string, object> htmlAttributes = null) where ControllerT : IController
        {
            routeValues = routeValues ?? new RouteValueDictionary();

            var methodExpression = action.Body as MethodCallExpression;
            if (methodExpression == null) throw new InvalidOperationException("Expression must be a method call.");
            
            var actionName = methodExpression.Method.Name;

            var controllerName = typeof(ControllerT).Supermodel().GetControllerName();
            routeValues.Supermodel().AddOrUpdateWith("controller", controllerName);

            var parameters = methodExpression.Method.GetParameters();
            for (var i = 0; i < parameters.Length; i++)
            {
                var param = parameters[i];
                var argumentExpression = methodExpression.Arguments[i];
                var argument = Expression.Lambda(argumentExpression).Compile().DynamicInvoke();

                if (argument != null && argument.GetType().IsClass && argument.GetType() != typeof(string))
                {
                    var argumentAsRouteVlues = HtmlHelper.AnonymousObjectToHtmlAttributes(argument);
                    routeValues.Supermodel().AddOrUpdateWith(argumentAsRouteVlues);
                }
                else
                {
                    routeValues.Supermodel().AddOrUpdateWith(param.Name, argument);
                }
            }
            if (actionName.EndsWith("Async")) actionName = actionName.Substring(0, actionName.Length - 5);
            return _html.ActionLink(linkText, actionName, routeValues, htmlAttributes);
        }
        #endregion

        #region RESTful ActionLink and ActionLinkHtmlContent helpers
        public MvcHtmlString RESTfulActionLink(HttpVerbs actionVerb, string linkText, object htmlAttributes, string actionName, object routeValues, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, new MvcHtmlString(linkText), htmlAttributes, actionName, routeValues, confMsg);
        }
        public MvcHtmlString RESTfulActionLinkHtmlContent(HttpVerbs actionVerb, MvcHtmlString linkHtml, object htmlAttributes, string actionName, object routeValues, string confMsg)
        {
            var result = new StringBuilder();
            result.AppendLine(_html.Supermodel().BeginMultipartFormMvcHtmlString(actionName, null /*controllerName*/, routeValues).ToString());
            result.AppendLine(RESTfulActionLinkFormContent(_html, linkHtml, actionVerb, htmlAttributes, confMsg).ToString());
            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());
            return MvcHtmlString.Create(result.ToString());
        }
        
        public MvcHtmlString RESTfulActionLink(HttpVerbs actionVerb, string linkText, object htmlAttributes, string actionName, object routeValues, string controllerName, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, new MvcHtmlString(linkText), htmlAttributes, actionName, routeValues, controllerName, confMsg);
        }
        public MvcHtmlString RESTfulActionLinkHtmlContent(HttpVerbs actionVerb, MvcHtmlString linkHtml, object htmlAttributes, string actionName, object routeValues, string controllerName, string confMsg)
        {
            var result = new StringBuilder();
            result.AppendLine(_html.Supermodel().BeginMultipartFormMvcHtmlString(actionName, controllerName, routeValues).ToString());
            result.AppendLine(RESTfulActionLinkFormContent(_html, linkHtml, actionVerb, htmlAttributes, confMsg).ToString());
            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());
            return MvcHtmlString.Create(result.ToString());
        }
        
        public MvcHtmlString RESTfulActionLink(HttpVerbs actionVerb, string linkText, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, new MvcHtmlString(linkText), actionName, routeValues, htmlAttributes, confMsg);
        }
        public MvcHtmlString RESTfulActionLinkHtmlContent(HttpVerbs actionVerb, MvcHtmlString linkHtml, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string confMsg)
        {
            var result = new StringBuilder();
            result.AppendLine(_html.Supermodel().BeginMultipartFormMvcHtmlString(actionName, null /*controllerName*/, routeValues).ToString());
            result.AppendLine(RESTfulActionLinkFormContent(_html, linkHtml, actionVerb, htmlAttributes, confMsg).ToString());
            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());
            return MvcHtmlString.Create(result.ToString());
        }
        
        public MvcHtmlString RESTfulActionLink(HttpVerbs actionVerb, string linkText, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, IDictionary<string, object> formHtmlAttributes, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, new MvcHtmlString(linkText), actionName, routeValues, htmlAttributes, formHtmlAttributes, confMsg);
        }
        public MvcHtmlString RESTfulActionLinkHtmlContent(HttpVerbs actionVerb, MvcHtmlString linkHtml, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, IDictionary<string, object> formHtmlAttributes, string confMsg)
        {
            var result = new StringBuilder();
            result.AppendLine(_html.Supermodel().BeginMultipartFormMvcHtmlString(actionName, null /*controllerName*/, routeValues, FormMethod.Post, formHtmlAttributes).ToString());
            result.AppendLine(RESTfulActionLinkFormContent(_html, linkHtml, actionVerb, htmlAttributes, confMsg).ToString());
            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());
            return MvcHtmlString.Create(result.ToString());
        }
        
        public MvcHtmlString RESTfulActionLink(HttpVerbs actionVerb, string linkText, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, new MvcHtmlString(linkText), actionName, controllerName, routeValues, htmlAttributes, confMsg);
        }
        public MvcHtmlString RESTfulActionLinkHtmlContent(HttpVerbs actionVerb, MvcHtmlString linkHtml, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, linkHtml, actionName, controllerName, routeValues, htmlAttributes, null, confMsg);
        }
        
        public MvcHtmlString RESTfulActionLink(HttpVerbs actionVerb, string linkText, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, IDictionary<string, object> formHtmlAttributes, string confMsg)
        {
            return RESTfulActionLinkHtmlContent(actionVerb, new MvcHtmlString(linkText), actionName, controllerName, routeValues, htmlAttributes, formHtmlAttributes, confMsg);
        }
        public MvcHtmlString RESTfulActionLinkHtmlContent(HttpVerbs actionVerb, MvcHtmlString linkHtml, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, IDictionary<string, object> formHtmlAttributes, string confMsg)
        {
            var result = new StringBuilder();
            result.AppendLine(_html.Supermodel().BeginMultipartFormMvcHtmlString(actionName, controllerName, routeValues, FormMethod.Post, formHtmlAttributes).ToString());
            result.AppendLine(RESTfulActionLinkFormContent(_html, linkHtml, actionVerb, htmlAttributes, confMsg).ToString());
            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());
            return MvcHtmlString.Create(result.ToString());
        }

        private static MvcHtmlString RESTfulActionLinkFormContent(HtmlHelper htmlHelper, MvcHtmlString linkHtml, HttpVerbs actionVerb, object htmlAttributes, string confMsg)
        {
            return RESTfulActionLinkFormContent(htmlHelper, linkHtml, actionVerb, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes), confMsg);
        }
        private static MvcHtmlString RESTfulActionLinkFormContent(HtmlHelper htmlHelper, MvcHtmlString linkHtml, HttpVerbs actionVerb, IDictionary<string, object> htmlAttributes, string confMsg)
        {
            var result = new StringBuilder();

            //HttpMethodOverride
            switch (actionVerb)
            {
                case HttpVerbs.Delete:
                case HttpVerbs.Head:
                case HttpVerbs.Put:
                    result.AppendLine(htmlHelper.HttpMethodOverride(actionVerb).ToHtmlString());
                    break;
                case HttpVerbs.Post:
                    break;
                case HttpVerbs.Get:
                    throw new SupermodelSystemErrorException("HttpVerbs.Get is not supported in ActionLinkFormContent");
            }

            //submit
            var aTag = new TagBuilder("button")
            {
                InnerHtml = linkHtml != null ? linkHtml.ToString() : string.Empty
            };
            aTag.MergeAttribute("type", "submit");
            aTag.MergeAttributes(htmlAttributes);
            if (confMsg != null) aTag.MergeAttribute("data-sm-ConfirmMsg", confMsg);
            result.AppendLine(aTag.ToString());

            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region ActionLinkHtmlContent helpers
        public MvcHtmlString ActionLinkHtmlContent(MvcHtmlString linkHtml, string actionName)
        {
            return ActionLinkHtmlContent(linkHtml, actionName, null /* controllerName */, new RouteValueDictionary(), new RouteValueDictionary());
        }
        public MvcHtmlString ActionLinkHtmlContent(MvcHtmlString linkHtml, string actionName, object routeValues)
        {
            return ActionLinkHtmlContent(linkHtml, actionName, null /* controllerName */, new RouteValueDictionary(routeValues), new RouteValueDictionary());
        }
        public MvcHtmlString ActionLinkHtmlContent(MvcHtmlString linkHtml, string actionName, object routeValues, object htmlAttributes)
        {
            return ActionLinkHtmlContent(linkHtml, actionName, null /* controllerName */, new RouteValueDictionary(routeValues), HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcHtmlString ActionLinkHtmlContent(MvcHtmlString linkHtml, string actionName, RouteValueDictionary routeValues)
        {
            return ActionLinkHtmlContent(linkHtml, actionName, null /* controllerName */, routeValues, new RouteValueDictionary());
        }
        public MvcHtmlString ActionLinkHtmlContent(MvcHtmlString linkHtml, string actionName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            return ActionLinkHtmlContent(linkHtml, actionName, null /* controllerName */, routeValues, htmlAttributes);
        }
        public MvcHtmlString ActionLinkHtmlContent(MvcHtmlString linkHtml, string actionName, string controllerName)
        {
            return ActionLinkHtmlContent(linkHtml, actionName, controllerName, new RouteValueDictionary(), new RouteValueDictionary());
        }
        public MvcHtmlString ActionLinkHtmlContent(MvcHtmlString linkHtml, string actionName, string controllerName, object routeValues, object htmlAttributes)
        {
            return ActionLinkHtmlContent(linkHtml, actionName, controllerName, new RouteValueDictionary(routeValues), HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcHtmlString ActionLinkHtmlContent(MvcHtmlString linkHtml, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            if (linkHtml == null || linkHtml.ToString() == "")
            {
                throw new ArgumentNullException(nameof(linkHtml));
            }
            return MvcHtmlString.Create(GenerateLinkHtmlContent(_html.ViewContext.RequestContext, _html.RouteCollection, linkHtml, null /* routeName */, actionName, controllerName, routeValues, htmlAttributes));
        }
        public MvcHtmlString ActionLinkHtmlContent(MvcHtmlString linkHtml, string actionName, string controllerName, string protocol, string hostName, string fragment, object routeValues, object htmlAttributes)
        {
            return ActionLinkHtmlContent(linkHtml, actionName, controllerName, protocol, hostName, fragment, new RouteValueDictionary(routeValues), HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcHtmlString ActionLinkHtmlContent(MvcHtmlString linkHtml, string actionName, string controllerName, string protocol, string hostName, string fragment, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            if (linkHtml == null || linkHtml.ToString() == "")
            {
                throw new ArgumentNullException(nameof(linkHtml));
            }
            return MvcHtmlString.Create(GenerateLinkHtmlContent(_html.ViewContext.RequestContext, _html.RouteCollection, linkHtml, null /* routeName */, actionName, controllerName, protocol, hostName, fragment, routeValues, htmlAttributes));
        }

        public string GenerateLinkHtmlContent(RequestContext requestContext, RouteCollection routeCollection, MvcHtmlString linkHtml, string routeName, string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            return GenerateLinkHtmlContent(requestContext, routeCollection, linkHtml, routeName, actionName, controllerName, null /* protocol */, null /* hostName */, null /* fragment */, routeValues, htmlAttributes);
        }

        public string GenerateLinkHtmlContent(RequestContext requestContext, RouteCollection routeCollection, MvcHtmlString linkHtml, string routeName, string actionName, string controllerName, string protocol, string hostName, string fragment, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            return GenerateLinkHtmlContentInternal(requestContext, routeCollection, linkHtml, routeName, actionName, controllerName, protocol, hostName, fragment, routeValues, htmlAttributes, true /* includeImplicitMvcValues */);
        }

        private string GenerateLinkHtmlContentInternal(RequestContext requestContext, RouteCollection routeCollection, MvcHtmlString linkHtml, string routeName, string actionName, string controllerName, string protocol, string hostName, string fragment, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes, bool includeImplicitMvcValues)
        {
            string url = UrlHelper.GenerateUrl(routeName, actionName, controllerName, protocol, hostName, fragment, routeValues, routeCollection, requestContext, includeImplicitMvcValues);
            TagBuilder tagBuilder = new TagBuilder("a")
            {
                InnerHtml = linkHtml != null ? linkHtml.ToString() : String.Empty
            };
            tagBuilder.MergeAttributes(htmlAttributes);
            tagBuilder.MergeAttribute("href", url);
            return tagBuilder.ToString(TagRenderMode.Normal);
        }

        #endregion 

        #region Form Method Extensions
        public MvcForm BeginMultipartForm()
        {
            var rawUrl = _html.ViewContext.HttpContext.Request.RawUrl;
            return MultipartFormHelper(rawUrl, FormMethod.Post, new RouteValueDictionary());
        }
        public MvcForm BeginMultipartForm(object htmlAttributes)
        {
            return BeginMultipartForm(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcForm BeginMultipartForm(IDictionary<string, object> htmlAttributes)
        {
            var formAction = _html.ViewContext.HttpContext.Request.RawUrl;
            return MultipartFormHelper(formAction, FormMethod.Post, htmlAttributes);
        }
        public MvcForm BeginMultipartForm(string actionName, string controllerName, object routeValues, object htmlAttributes)
        {
            string formAction = UrlHelper.GenerateUrl(null, actionName, controllerName, new RouteValueDictionary(routeValues), _html.RouteCollection, _html.ViewContext.RequestContext, true);
            return MultipartFormHelper(formAction, FormMethod.Post, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcForm BeginMultipartForm(string actionName, string controllerName, IDictionary<string, object> htmlAttributes)
        {
            string formAction = UrlHelper.GenerateUrl(null, actionName, controllerName, null, _html.RouteCollection, _html.ViewContext.RequestContext, true);
            return MultipartFormHelper(formAction, FormMethod.Post, htmlAttributes);
        }
        public MvcForm BeginMultipartForm(string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            return BeginMultipartForm(actionName, controllerName, routeValues, FormMethod.Post, new RouteValueDictionary());
        }
        public MvcForm BeginMultipartForm(string actionName, string controllerName, object routeValues)
        {
            return BeginMultipartForm(actionName, controllerName, new RouteValueDictionary(routeValues), FormMethod.Post, new RouteValueDictionary());
        }
        public MvcForm BeginMultipartForm(string actionName, string controllerName, RouteValueDictionary routeValues, FormMethod method, IDictionary<string, object> htmlAttributes)
        {
            string formAction = UrlHelper.GenerateUrl(null, actionName, controllerName, routeValues, _html.RouteCollection, _html.ViewContext.RequestContext, true);
            return MultipartFormHelper(formAction, method, htmlAttributes);
        }
        private MvcForm MultipartFormHelper(string formAction, FormMethod method, IDictionary<string, object> htmlAttributes)
        {
            htmlAttributes.Add("enctype", "multipart/form-data");
            return (MvcForm)ReflectionHelper.ExecuteNonPublicStaticMethod(typeof(FormExtensions), "FormHelper", _html, formAction, method, htmlAttributes);
        }

        public MvcHtmlString BeginMultipartFormMvcHtmlString()
        {
            var rawUrl = _html.ViewContext.HttpContext.Request.RawUrl;
            return MultipartFormHelperMvcHtmlString(rawUrl, FormMethod.Post, new RouteValueDictionary());
        }
        public MvcHtmlString BeginMultipartFormMvcHtmlString(IDictionary<string, object> htmlAttributes)
        {
            var rawUrl = _html.ViewContext.HttpContext.Request.RawUrl;
            return MultipartFormHelperMvcHtmlString(rawUrl, FormMethod.Post, htmlAttributes);
        }
        public MvcHtmlString BeginMultipartFormMvcHtmlString(string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            return BeginMultipartFormMvcHtmlString(actionName, controllerName, routeValues, FormMethod.Post, new RouteValueDictionary());
        }
        public MvcHtmlString BeginMultipartFormMvcHtmlString(string actionName, string controllerName, object routeValues)
        {
            return BeginMultipartFormMvcHtmlString(actionName, controllerName, new RouteValueDictionary(routeValues), FormMethod.Post, new RouteValueDictionary());
        }
        public MvcHtmlString BeginMultipartFormMvcHtmlString(string actionName, string controllerName, RouteValueDictionary routeValues, FormMethod method, IDictionary<string, object> htmlAttributes)
        {
            var formAction = UrlHelper.GenerateUrl(null, actionName, controllerName, routeValues, _html.RouteCollection, _html.ViewContext.RequestContext, true);
            return MultipartFormHelperMvcHtmlString(formAction, method, htmlAttributes);
        }
        private MvcHtmlString MultipartFormHelperMvcHtmlString(string formAction, FormMethod method, IDictionary<string, object> htmlAttributes)
        {
            var tagBuilder = new TagBuilder("form");
            tagBuilder.MergeAttributes(htmlAttributes);
            tagBuilder.MergeAttribute("action", formAction);
            tagBuilder.MergeAttribute("method", HtmlHelper.GetFormMethodString(method), true);
            tagBuilder.MergeAttribute("enctype", "multipart/form-data"); //This diffres for standard implemetation but we always want to have an ability to upload files
            var flag = _html.ViewContext.ClientValidationEnabled && !_html.ViewContext.UnobtrusiveJavaScriptEnabled;
            if (flag) tagBuilder.GenerateId((string)_html.ViewContext.ExecuteNonPublicMethod("FormIdGenerator"));
            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.StartTag));
        }

        public MvcForm BeginGetForm()
        {
            var rawUrl = _html.ViewContext.HttpContext.Request.RawUrl;
            return GetFormHelper(rawUrl, new RouteValueDictionary());
        }
        public MvcForm BeginGetForm(object htmlAttributes)
        {
            return BeginMultipartForm(HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcForm BeginGetForm(IDictionary<string, object> htmlAttributes)
        {
            var rawUrl = _html.ViewContext.HttpContext.Request.RawUrl;
            return GetFormHelper(rawUrl, htmlAttributes);
        }
        public MvcForm BeginGetForm(string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            return BeginGetForm(actionName, controllerName, routeValues, new RouteValueDictionary());
        }
        public MvcForm BeginGetForm(string actionName, string controllerName, object routeValues)
        {
            return BeginGetForm(actionName, controllerName, new RouteValueDictionary(routeValues), new RouteValueDictionary());
        }
        public MvcForm BeginGetForm(string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            string formAction = UrlHelper.GenerateUrl(null, actionName, controllerName, routeValues, _html.RouteCollection, _html.ViewContext.RequestContext, true);
            return GetFormHelper(formAction, htmlAttributes);
        }
        private MvcForm GetFormHelper(string formAction, IDictionary<string, object> htmlAttributes)
        {
            htmlAttributes.Add("enctype", "multipart/form-data");
            return (MvcForm)ReflectionHelper.ExecuteNonPublicStaticMethod(typeof(FormExtensions), "FormHelper", _html, formAction, FormMethod.Get, htmlAttributes);
        }

        public MvcHtmlString BeginGetFormMvcHtmlString()
        {
            var rawUrl = _html.ViewContext.HttpContext.Request.RawUrl;
            return GetFormHelperMvcHtmlString(rawUrl, new RouteValueDictionary());
        }
        public MvcHtmlString BegingGetFormMvcHtmlString(IDictionary<string, object> htmlAttributes)
        {
            var rawUrl = _html.ViewContext.HttpContext.Request.RawUrl;
            return GetFormHelperMvcHtmlString(rawUrl, htmlAttributes);
        }
        public MvcHtmlString BeginGetFormMvcHtmlString(string actionName, string controllerName, RouteValueDictionary routeValues)
        {
            return BeginGetFormMvcHtmlString(actionName, controllerName, routeValues, new RouteValueDictionary());
        }
        public MvcHtmlString BeginGetFormMvcHtmlString(string actionName, string controllerName, object routeValues)
        {
            return BeginGetFormMvcHtmlString(actionName, controllerName, new RouteValueDictionary(routeValues), new RouteValueDictionary());
        }
        public MvcHtmlString BeginGetFormMvcHtmlString(string actionName, string controllerName, RouteValueDictionary routeValues, IDictionary<string, object> htmlAttributes)
        {
            var formAction = UrlHelper.GenerateUrl(null, actionName, controllerName, routeValues, _html.RouteCollection, _html.ViewContext.RequestContext, true);
            return GetFormHelperMvcHtmlString(formAction, htmlAttributes);
        }
        private MvcHtmlString GetFormHelperMvcHtmlString(string formAction, IDictionary<string, object> htmlAttributes)
        {
            var tagBuilder = new TagBuilder("form");
            tagBuilder.MergeAttributes(htmlAttributes);
            tagBuilder.MergeAttribute("action", formAction);
            tagBuilder.MergeAttribute("method", HtmlHelper.GetFormMethodString(FormMethod.Get), true);
            var flag = _html.ViewContext.ClientValidationEnabled && !_html.ViewContext.UnobtrusiveJavaScriptEnabled;
            if (flag) tagBuilder.GenerateId((string)_html.ViewContext.ExecuteNonPublicMethod("FormIdGenerator"));
            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.StartTag));
        }

        public MvcHtmlString EndFormMvcHtmlString()
        {
            var result = new StringBuilder();
            result.AppendLine("</form>");
            result.AppendLine(OutputClientValidation(_html.ViewContext).ToString());
            _html.ViewContext.FormContext = null;
            return MvcHtmlString.Create(result.ToString());
        }

        private static MvcHtmlString OutputClientValidation(ViewContext viewContext)
        {
            string result;

            var clientValidation = (FormContext)viewContext.ExecuteNonPublicMethod("GetFormContextForClientValidation");
            if (clientValidation == null || viewContext.UnobtrusiveJavaScriptEnabled) result = "";
            else result = string.Format(CultureInfo.InvariantCulture, "<script type=\"text/javascript\">\r\n//<![CDATA[\r\nif (!window.mvcClientValidationMetadata) {{ window.mvcClientValidationMetadata = []; }}\r\nwindow.mvcClientValidationMetadata.push({0});\r\n//]]>\r\n</script>".Replace("\r\n", Environment.NewLine), clientValidation.GetJsonValidationMetadata());

            return MvcHtmlString.Create(result);
        }
        #endregion

        #region CRUD Search Form Helpers
        public MvcHtmlString CRUDSearchFormForModel(string pageTitle = null, string action = null, string controller = null, bool resetButton = false)
        {
            return CRUDSearchFormForModel(MvcHtmlString.Create(pageTitle), action, controller, resetButton);
        }
        public MvcHtmlString CRUDSearchFormForModel(MvcHtmlString pageTitle, string action, string controller, bool resetButton)
        {
            return CRUDSearchFormHelper((Expression<Func<ModelT, ModelT>>)null, pageTitle, controller, action, resetButton);
        }

        public MvcHtmlString CRUDSearchFormFor<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, string pageTitle = null, string action = null, string controller = null, bool resetButton = false) where ValueT: MvcModel
        {
            return CRUDSearchFormFor(searchByModelExpression, MvcHtmlString.Create(pageTitle), action, controller, resetButton);
        }
        public MvcHtmlString CRUDSearchFormFor<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, MvcHtmlString pageTitle, string action, string controller, bool resetButton) where ValueT : MvcModel
        {
            return CRUDSearchFormHelper(searchByModelExpression, pageTitle, controller, action, resetButton);
        }
        
        public MvcHtmlString CRUDSearchFormHeader(string pageTitle, string action = null, string controller = null)
        {
            return CRUDSearchFormHeader(new MvcHtmlString(pageTitle), action, controller);
        }
        public MvcHtmlString CRUDSearchFormHeader(MvcHtmlString pageTitle, string action, string controller)
        {
            var result = new StringBuilder();

            action = action ?? "List";
            controller = controller ?? _html.ViewContext.RouteData.Values["controller"].ToString();
            result.AppendLine(_html.Supermodel().BeginGetFormMvcHtmlString(action, controller, null, new Dictionary<string, object> { {"class", MvcModel.ScaffoldingSettings.FormId} }).ToString());
            if (!MvcHtmlString.IsNullOrEmpty(pageTitle)) result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(MvcModel.ScaffoldingSettings.SearchTitleCssClass) + ">" + pageTitle + "</h1>");

            return MvcHtmlString.Create(result.ToString());
        }

        public MvcHtmlString CRUDSearchFormFooter(bool resetButton)
        {
            var result = new StringBuilder();

            //result.AppendLine(_html.Hidden("smSkip", 0).ToString());
            result.AppendLine("<input id='smSkip' name='smSkip' type='hidden' value='0'>");
            result.AppendLine(_html.Hidden("smTake", _html.Supermodel().GetTakeValue()).ToString());
            result.AppendLine(_html.Hidden("smSortBy", _html.Supermodel().GetSortByValue()).ToString());

            result.AppendLine("<input type='submit' value='Find' " + UtilsLib.MakeIdAndClassAttribues(MvcModel.ScaffoldingSettings.FindButtonId, MvcModel.ScaffoldingSettings.FindButtonCssClass) + "/>");
            if (resetButton) result.AppendLine("<input type='reset' value='Reset' " + UtilsLib.MakeIdAndClassAttribues(MvcModel.ScaffoldingSettings.ResetButtonId, MvcModel.ScaffoldingSettings.ResetButtonCssClass) + "/>");
            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());

            return MvcHtmlString.Create(result.ToString());
        }

        private MvcHtmlString CRUDSearchFormHelper<ValueT>(Expression<Func<ModelT, ValueT>> searchByModelExpression, MvcHtmlString pageTitle, string controller, string action, bool resetButton)
        {
            var result = new StringBuilder();
            
            result.AppendLine(CRUDSearchFormHeader(pageTitle, action, controller).ToString());
            
            if (searchByModelExpression != null)
            {
                var html = (HtmlHelper<ModelT>)_html;

                var modelMetadata = ModelMetadata.FromLambdaExpression(searchByModelExpression, html.ViewData);
                var htmlFieldName = ExpressionHelper.GetExpressionText(searchByModelExpression);

                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                var nestedHtml = MakeNestedHtmlHelper(modelMetadata, htmlFieldName);
                nestedHtml.ViewData.TemplateInfo.HtmlFieldPrefix = "";
                if (!(modelMetadata.Model is ISupermodelEditorTemplate)) throw new SupermodelSystemErrorException("CRUDSearchFormFor points to property that is not an MvcModel");
                result.AppendLine((modelMetadata.Model as ISupermodelEditorTemplate).EditorTemplate(nestedHtml).ToString());
            }
            else
            {
                result.AppendLine(_html.Supermodel().EditorForModel().ToString());
            }

            result.AppendLine(CRUDSearchFormFooter(resetButton).ToString());

            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region CRUD Edit helpers
        public MvcHtmlString CRUDEdit<EntityT, MvcModelT>(string pageTitle, bool readOnly = false, bool skipBackButton = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            return CRUDEdit<EntityT, MvcModelT>(new MvcHtmlString(pageTitle), readOnly, skipBackButton);
        }

        public MvcHtmlString CRUDEdit<EntityT, MvcModelT>(MvcHtmlString pageTitle = null, bool readOnly = false, bool skipBackButton = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            return CRUDEditHelper<EntityT, MvcModelT>(pageTitle, readOnly, skipBackButton);
        }

        public MvcHtmlString CRUDEditHeader<EntityT, MvcModelT>(string pageTitle, bool readOnly = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            return CRUDEditHeader<EntityT, MvcModelT>(MvcHtmlString.Create(pageTitle), readOnly);
        }

        public MvcHtmlString CRUDEditHeader<EntityT, MvcModelT>(MvcHtmlString pageTitle = null, bool readOnly = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            var result = new StringBuilder();
            var model = (MvcModelT)_html.ViewData.Model;

            result.AppendLine(_html.Supermodel().BeginMultipartFormMvcHtmlString(new Dictionary<string, object> { { "class", MvcModel.ScaffoldingSettings.FormId } }).ToString());
            if (!MvcHtmlString.IsNullOrEmpty(pageTitle)) result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(MvcModel.ScaffoldingSettings.EditTitleCssClass) + ">" + pageTitle + "</h1>");

            if (model.Id != 0) result.AppendLine(_html.HttpMethodOverride(HttpVerbs.Put).ToString());

            return MvcHtmlString.Create(readOnly ? result.ToString().Supermodel().DisableAllControls() : result.ToString());
        }

        public MvcHtmlString CRUDEditFooter<EntityT, MvcModelT>(bool readOnly = false, bool skipBackButton = false)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            var result = new StringBuilder();
            var model = (MvcModelT)_html.ViewData.Model;

            if (!skipBackButton)
            {
                long? parentId = null;
                if (ReflectionHelper.IsClassADerivedFromClassB(model.GetType(), typeof(ChildMvcModelForEntity<,>))) parentId = (long?)model.PropertyGet("ParentId");

                //Make sure we keep quesry string values
                var routeValues = _html.Supermodel().QueryStringRouteValues();
                var newRouteValues = HtmlHelper.AnonymousObjectToHtmlAttributes(new { parentId });
                routeValues.Supermodel().AddOrUpdateWith(newRouteValues);

                //set up htmlAttributes
                var htmlAttributes = HtmlHelper.AnonymousObjectToHtmlAttributes(new { id = MvcModel.ScaffoldingSettings.BackButtonId, @class = MvcModel.ScaffoldingSettings.BackButtonCssClass });
                
                // ReSharper disable Mvc.ActionNotResolved
                result.AppendLine(_html.ActionLink("Back", "List", routeValues, htmlAttributes).ToString());
                // ReSharper restore Mvc.ActionNotResolved
            }
            if (!readOnly) result.AppendLine("<input type='submit' value='Save' " + UtilsLib.MakeIdAndClassAttribues(MvcModel.ScaffoldingSettings.SaveButtonId, MvcModel.ScaffoldingSettings.SaveButtonCssClass) + "/>");
            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());

            return MvcHtmlString.Create(readOnly ? result.ToString().Supermodel().DisableAllControls() : result.ToString());
        }

        private MvcHtmlString CRUDEditHelper<EntityT, MvcModelT>(MvcHtmlString pageTitle, bool readOnly, bool skipBackButton)
            where EntityT : class, IEntity, new()
            where MvcModelT : MvcModelForEntity<EntityT>, new()
        {
            var result = new StringBuilder();

            result.AppendLine(CRUDEditHeader<EntityT, MvcModelT>(pageTitle, readOnly).ToString());
            result.AppendLine(_html.Supermodel().EditorForModel().ToString().Supermodel().DisableAllControlsIf(readOnly));
            result.AppendLine(CRUDEditFooter<EntityT, MvcModelT>(readOnly, skipBackButton).ToString());

            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region CRUD List helpers
        public MvcHtmlString CRUDList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), null, skipAddNew, viewOnly);
        }

        public MvcHtmlString CRUDList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType = null, MvcHtmlString pageTitle = null, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, null, skipAddNew, viewOnly);
        }

        public MvcHtmlString CRUDChildrenList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, long parentId, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), parentId, skipAddNew, viewOnly);
        }

        public MvcHtmlString CRUDChildrenList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long parentId, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, parentId, skipAddNew, viewOnly);
        }

        private MvcHtmlString CRUDListHelper<EntityT, MvcModelT>(IEnumerable<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long? parentId, bool skipAddNew, bool viewOnly)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            var controllerName = controllerType != null ?
                                     controllerType.Supermodel().GetControllerName() :
                                     _html.ViewContext.RouteData.Values["controller"].ToString();

            var result = new StringBuilder();
            if (parentId == null || parentId > 0)
            {
                if (!MvcHtmlString.IsNullOrEmpty(pageTitle))
                {
                    if (parentId == null) result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(MvcModel.ScaffoldingSettings.ListTitleCssClass) + ">" + pageTitle + "</h1>");
                    else result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(MvcModel.ScaffoldingSettings.ChildListTitleCssClass) + ">" + pageTitle + "</h1>");
                }
                result.AppendLine("<div" + UtilsLib.MakeIdAndClassAttribues(MvcModel.ScaffoldingSettings.CRUDListTopDivId, MvcModel.ScaffoldingSettings.CRUDListTopDivCssClass) + ">");
                result.AppendLine("<table" + UtilsLib.MakeIdAndClassAttribues(MvcModel.ScaffoldingSettings.CRUDListTableId, MvcModel.ScaffoldingSettings.CRUDListTableCssClass) + ">");
                result.AppendLine("<thead>");
                result.AppendLine("<tr>");
                result.AppendLine("<th> Name </th>");
                result.AppendLine("<th colspan=2> Actions </th>");
                result.AppendLine("</tr>");
                result.AppendLine("</thead>");
                result.AppendLine("<tbody>");
                foreach (var item in items)
                {
                    result.AppendLine("<tr>");
                    result.AppendLine("<td>" + item.Label + "</td>");

                    //make sure we keep query string
                    var editViewDeleteRouteValues = _html.Supermodel().QueryStringRouteValues();
                    var newEditViewDeleteRouteValues = HtmlHelper.AnonymousObjectToHtmlAttributes(new { id = item.Id, parentId });
                    editViewDeleteRouteValues.Supermodel().AddOrUpdateWith(newEditViewDeleteRouteValues);

                    if (viewOnly)
                    {
                        result.AppendLine("<td>" + _html.ActionLink("View", "Detail", controllerName, editViewDeleteRouteValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = MvcModel.ScaffoldingSettings.CRUDListEditCssClass })) + "</td>");
                    }
                    else
                    {
                        result.AppendLine("<td>" + _html.ActionLink("Edit", "Detail", controllerName, editViewDeleteRouteValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = MvcModel.ScaffoldingSettings.CRUDListEditCssClass })) + "</td>");
                        result.AppendLine("<td>");
                        result.AppendLine(_html.Supermodel().RESTfulActionLink(HttpVerbs.Delete, "Delete", "Detail", controllerName, editViewDeleteRouteValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = MvcModel.ScaffoldingSettings.CRUDListDeleteCssClass }), "Are you sure?").ToString());
                        result.AppendLine("</td>");
                    }
                    result.AppendLine("</tr>");
                }
                result.AppendLine("</tbody>");
                result.AppendLine("</table>");
                if (!skipAddNew)
                {
                    //make sure we keep query string
                    var addRouteValues = _html.Supermodel().QueryStringRouteValues();
                    var newAddRouteValues = HtmlHelper.AnonymousObjectToHtmlAttributes(new { id = 0, parentId });
                    addRouteValues.Supermodel().AddOrUpdateWith(newAddRouteValues);

                    result.AppendLine("<p>" + _html.ActionLink("Add New", "Detail", controllerName, addRouteValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = MvcModel.ScaffoldingSettings.CRUDListAddNewCssClass })) + "</p>");
                }
                result.AppendLine("</div>");
            }
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region CRUD Multi-Column List No Actions helpers
        public MvcHtmlString CRUDMultiColumnListNoActions<T>(ICollection<T> items, string pageTitle) where T : new()
        {
            return CRUDMultiColumnListNoActionsHelper(items, new MvcHtmlString(pageTitle));
        }

        public MvcHtmlString CRUDMultiColumnListNoActions<T>(ICollection<T> items, MvcHtmlString pageTitle = null) where T : new()
        {
            return CRUDMultiColumnListNoActionsHelper(items, pageTitle);
        }

        public MvcHtmlString CRUDChildrenMultiColumnListNoActions<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, long parentId, bool skipAddNew = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), parentId, skipAddNew, false, false);
        }

        public MvcHtmlString CRUDChildrenMultiColumnListNoActions<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long parentId, bool skipAddNew = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, parentId, skipAddNew, false, false);
        }

        private MvcHtmlString CRUDMultiColumnListNoActionsHelper<T>(IEnumerable<T> items, MvcHtmlString pageTitle) where T : new()
        {
            var result = new StringBuilder();
            if (!MvcHtmlString.IsNullOrEmpty(pageTitle)) result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(MvcModel.ScaffoldingSettings.ListTitleCssClass) + ">" + pageTitle + "</h1>");
            result.AppendLine("<div" + UtilsLib.MakeIdAndClassAttribues(MvcModel.ScaffoldingSettings.CRUDListTopDivId, MvcModel.ScaffoldingSettings.CRUDListTopDivCssClass) + ">");
            result.AppendLine("<table" + UtilsLib.MakeIdAndClassAttribues(MvcModel.ScaffoldingSettings.CRUDListTableId, MvcModel.ScaffoldingSettings.CRUDListTableCssClass) + ">");
            result.AppendLine("<thead>");
            result.AppendLine("<tr>");
            result = new T().ToHtmlTableHeader(_html, result);
            result.AppendLine("</tr>");
            result.AppendLine("</thead>");
            result.AppendLine("<tbody>");
            foreach (var item in items)
            {
                result.AppendLine("<tr>");

                //Render list columns using reflection
                result = item.ToHtmlTableRow(_html, result);

                result.AppendLine("</tr>");
            }
            result.AppendLine("</tbody>");
            result.AppendLine("</table>");
            result.AppendLine("</div>");

            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region CRUD MuiliColumn List helpers
        public MvcHtmlString CRUDMultiColumnList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), null, skipAddNew, viewOnly, true);
        }

        public MvcHtmlString CRUDMultiColumnList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType = null, MvcHtmlString pageTitle = null, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, null, skipAddNew, viewOnly, true);
        }

        public MvcHtmlString CRUDMultiColumnChildrenList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, string pageTitle, long parentId, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, new MvcHtmlString(pageTitle), parentId, skipAddNew, viewOnly, true);
        }

        public MvcHtmlString CRUDMultiColumnChildrenList<EntityT, MvcModelT>(ICollection<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long parentId, bool skipAddNew = false, bool viewOnly = false)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            return CRUDMultiColumnListHelper<EntityT, MvcModelT>(items, controllerType, pageTitle, parentId, skipAddNew, viewOnly, true);
        }

        private MvcHtmlString CRUDMultiColumnListHelper<EntityT, MvcModelT>(IEnumerable<MvcModelT> items, Type controllerType, MvcHtmlString pageTitle, long? parentId, bool skipAddNew, bool viewOnly, bool actionsColumn)
            where MvcModelT : MvcModelForEntity<EntityT>, new()
            where EntityT : class, IEntity, new()
        {
            var controllerName = controllerType != null ?
                                     controllerType.Supermodel().GetControllerName() :
                                     _html.ViewContext.RouteData.Values["controller"].ToString();

            var result = new StringBuilder();
            if (parentId == null || parentId > 0)
            {
                if (!MvcHtmlString.IsNullOrEmpty(pageTitle))
                {
                    if (parentId == null) result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(MvcModel.ScaffoldingSettings.ListTitleCssClass) + ">" + pageTitle + "</h1>");
                    else result.AppendLine("<h1 " + UtilsLib.MakeClassAttribue(MvcModel.ScaffoldingSettings.ChildListTitleCssClass) + ">" + pageTitle + "</h1>");
                }
                result.AppendLine("<div" + UtilsLib.MakeIdAndClassAttribues(MvcModel.ScaffoldingSettings.CRUDListTopDivId, MvcModel.ScaffoldingSettings.CRUDListTopDivCssClass) + ">");
                result.AppendLine("<table" + UtilsLib.MakeIdAndClassAttribues(MvcModel.ScaffoldingSettings.CRUDListTableId, MvcModel.ScaffoldingSettings.CRUDListTableCssClass) + ">");
                result.AppendLine("<thead>");
                result.AppendLine("<tr>");
                result = new MvcModelT().ToHtmlTableHeader(_html, result);
                if (actionsColumn) result.AppendLine("<th colspan=2> Actions </th>");
                result.AppendLine("</tr>");
                result.AppendLine("</thead>");
                result.AppendLine("<tbody>");
                foreach (var item in items)
                {
                    result.AppendLine("<tr>");

                    //Render list columns using reflection
                    result = item.ToHtmlTableRow(_html, result);

                    //make sure we keep query string
                    var editViewDeleteRouteValues = _html.Supermodel().QueryStringRouteValues();
                    var newEditViewDeleteRouteValues = HtmlHelper.AnonymousObjectToHtmlAttributes(new { id = item.Id, parentId });
                    editViewDeleteRouteValues.Supermodel().AddOrUpdateWith(newEditViewDeleteRouteValues);

                    if (actionsColumn)
                    {
                        if (viewOnly)
                        {
                            result.AppendLine("<td>" + _html.ActionLink("View", "Detail", controllerName, editViewDeleteRouteValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = MvcModel.ScaffoldingSettings.CRUDListEditCssClass })) + "</td>");
                        }
                        else
                        {
                            result.AppendLine("<td>" + _html.ActionLink("Edit", "Detail", controllerName, editViewDeleteRouteValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = MvcModel.ScaffoldingSettings.CRUDListEditCssClass })) + "</td>");
                            result.AppendLine("<td>");
                            result.AppendLine(_html.Supermodel().RESTfulActionLink(HttpVerbs.Delete, "Delete", "Detail", editViewDeleteRouteValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = MvcModel.ScaffoldingSettings.CRUDListDeleteCssClass }), "Are you sure?").ToString());
                            result.AppendLine("</td>");
                        }
                    }
                    result.AppendLine("</tr>");
                }
                result.AppendLine("</tbody>");
                result.AppendLine("</table>");
                if (!skipAddNew)
                {
                    //make sure we keep query string
                    var addRouteValues = _html.Supermodel().QueryStringRouteValues();
                    var newAddRouteValues = HtmlHelper.AnonymousObjectToHtmlAttributes(new { id = 0, parentId });
                    addRouteValues.Supermodel().AddOrUpdateWith(newAddRouteValues);

                    result.AppendLine("<p>" + _html.ActionLink("Add New", "Detail", controllerName, addRouteValues, HtmlHelper.AnonymousObjectToHtmlAttributes(new { @class = MvcModel.ScaffoldingSettings.CRUDListAddNewCssClass })) + "</p>");
                }
                result.AppendLine("</div>");
            }
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region New Search Action Link
        public MvcHtmlString NewSearchActionLink(MvcHtmlString linkHtml = null)
        {
            linkHtml = linkHtml ?? MvcHtmlString.Create("New Search");
            var routeValues = _html.Supermodel().QueryStringRouteValues();
            var controller = _html.ViewContext.RouteData.Values["controller"].ToString();
            var htmlAttributesDict = HtmlHelper.AnonymousObjectToHtmlAttributes(new {id = MvcModel.ScaffoldingSettings.NewSearchButtonId, @class = MvcModel.ScaffoldingSettings.NewSearchButtonCssClass});
            return ActionLinkHtmlContent(linkHtml, "Search", controller, routeValues, htmlAttributesDict);
        }
        #endregion

        #region CRUD Pagination Helpers
        public MvcHtmlString Pagination(int visiblePages)
        {
            var skip = _html.Supermodel().GetSkipValue();
            var take = _html.Supermodel().GetTakeValue();
            var totalCount = (int?)_html.ViewBag.SupermodelTotalCount;

            if (skip == null || take == null || totalCount == null) throw new ArgumentNullException();

            return Pagination(visiblePages, (int)skip, (int)take, (int)totalCount);
        }
        public MvcHtmlString Pagination(int visiblePages, int skipped, int taken, int totalCount)
        {
            var result = new StringBuilder();
            if (taken < totalCount)
            {
                result.AppendLine("<p " + UtilsLib.MakeClassAttribue(MvcModel.ScaffoldingSettings.PaginationCssClass)  + ">");

                var currentPage = skipped / taken + 1;

                var firstPage = currentPage - visiblePages / 2;
                if (firstPage < 1) firstPage = 1;

                var lastPage = firstPage + visiblePages - 1;
                if (lastPage > totalCount / taken)
                {
                    firstPage -= lastPage - totalCount / taken;
                    if (firstPage < 1) firstPage = 1;
                    lastPage = (int)Math.Ceiling((double)totalCount / taken);
                }

                //Prev page
                if (currentPage > 1) result.AppendLine(GetPageActionLink("«", currentPage - 1, taken));
                else result.AppendLine("<a>«</a>");

                //Neighboring pages
                for (var page = firstPage; page <= lastPage; page++)
                {
                    if (page == currentPage) result.AppendLine("<a>" + page + "</a>");
                    else result.AppendLine(GetPageActionLink(page.ToString(CultureInfo.InvariantCulture), page, taken));
                }

                //Next page
                if (currentPage < lastPage) result.AppendLine(GetPageActionLink("»", currentPage + 1, taken));
                else result.AppendLine("<a>»</a>");

                result.AppendLine("</p>");
            }
            return MvcHtmlString.Create(result.ToString());
        }
        public MvcHtmlString PaginationTotalRecordsCount()
        {
            var totalCount = _html.ViewBag.SupermodelTotalCount;
            if (totalCount == null) throw new ArgumentNullException();
            return new MvcHtmlString(totalCount.ToString());
        }
        private string GetPageActionLink(string linkText, int pageNum, int pageSize)
        {
            return _html.ActionLink(linkText, (string)_html.ViewContext.RouteData.Values["action"], _html.Supermodel().QueryStringRouteValues().Supermodel().AddOrUpdateWith("smSkip", (pageNum - 1) * pageSize)).ToString();
        }
        #endregion

        #region Display Helpers
        //This one handles both display for Radio and Dropdown
        public MvcHtmlString DropDownDisplay()
        {
            if (_html.ViewData.Model == null) throw new NullReferenceException(ReflectionHelper.GetCurrentContext() + " is called for a model that is null");
            if (!(_html.ViewData.Model is SingleSelectMvcModel)) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type diffrent from SingleSelectMvcModelBase.");

            var dropdown = (SingleSelectMvcModel)_html.ViewData.Model;
            return MvcHtmlString.Create(dropdown.SelectedLabel);
        }
        public MvcHtmlString BinaryFileDisplay()
        {
            if (_html.ViewData.Model is BinaryFileMvcModel) throw new InvalidCastException(ReflectionHelper.GetCurrentContext() + " is called for a model of type diffrent from BinaryFileMvcModel.");
            // ReSharper disable PossibleInvalidCastException
            return MvcHtmlString.Create((((BinaryFileMvcModel)_html.ViewData.Model)).Name);
            // ReSharper restore PossibleInvalidCastException
        }
        #endregion

        #region MvcModel Display & Editor & Hidden Helpers
        public MvcHtmlString Editor(string expression, bool forceUsingModelValues = true)
        {
            //if model is valid, we want to grab stuff from the model -- not model state
            if (forceUsingModelValues && _html.ViewData.ModelState.IsValid) _html.ViewData.ModelState.Clear();
            
            var modelMetadata = ModelMetadata.FromStringExpression(expression, _html.ViewData);
            var htmlFieldName = ExpressionHelper.GetExpressionText(expression);

            if (typeof(ISupermodelEditorTemplate).IsAssignableFrom(modelMetadata.ModelType))
            {
                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                // ReSharper disable PossibleNullReferenceException
                return (modelMetadata.Model as ISupermodelEditorTemplate).EditorTemplate(MakeNestedHtmlHelper(modelMetadata, htmlFieldName));
                // ReSharper restore PossibleNullReferenceException
            }

            //Special handling for MvcHtmlString
            if (typeof(MvcHtmlString).IsAssignableFrom(modelMetadata.ModelType))
            {
                return (MvcHtmlString)modelMetadata.Model;
            }

            return _html.Editor(expression);
        }
        public MvcHtmlString EditorFor<ValueT>(Expression<Func<ModelT, ValueT>> expression, bool forceUsingModelValues = true)
        {
            //if model is valid, we want to grab stuff from the model -- not model state
            if (forceUsingModelValues && _html.ViewData.ModelState.IsValid) _html.ViewData.ModelState.Clear();
            
            var html = (HtmlHelper<ModelT>) _html;
            
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            var htmlFieldName = ExpressionHelper.GetExpressionText(expression);

            if (typeof(ISupermodelEditorTemplate).IsAssignableFrom(modelMetadata.ModelType))
            {
                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                // ReSharper disable PossibleNullReferenceException
                return (modelMetadata.Model as ISupermodelEditorTemplate).EditorTemplate(MakeNestedHtmlHelper(modelMetadata, htmlFieldName));
                // ReSharper restore PossibleNullReferenceException
            }

            //Special handling for MvcHtmlString
            if (typeof(MvcHtmlString).IsAssignableFrom(modelMetadata.ModelType))
            {
                return (MvcHtmlString)modelMetadata.Model;
            }

            return html.EditorFor(expression);
        }
        public MvcHtmlString EditorForModel(bool forceUsingModelValues = true)
        {
            //if model is valid, we want to grab stuff from the model -- not model state
            if (forceUsingModelValues && _html.ViewData.ModelState.IsValid) _html.ViewData.ModelState.Clear();
            
            var modelMetadata = _html.ViewData.ModelMetadata;

            if (typeof(ISupermodelEditorTemplate).IsAssignableFrom(modelMetadata.ModelType))
            {
                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                // ReSharper disable PossibleNullReferenceException
                return (modelMetadata.Model as ISupermodelEditorTemplate).EditorTemplate(_html);
                // ReSharper restore PossibleNullReferenceException
            }

            //Special handling for MvcHtmlString
            if (typeof(MvcHtmlString).IsAssignableFrom(modelMetadata.ModelType))
            {
                return (MvcHtmlString)modelMetadata.Model;
            }

            return _html.EditorForModel();
        }

        public MvcHtmlString Display(string expression, bool forceUsingModelValues = true)
        {
            //if model is valid, we want to grab stuff from the model -- not model state
            if (forceUsingModelValues && _html.ViewData.ModelState.IsValid) _html.ViewData.ModelState.Clear();
            
            var modelMetadata = ModelMetadata.FromStringExpression(expression, _html.ViewData);
            var htmlFieldName = ExpressionHelper.GetExpressionText(expression);

            if (typeof(ISupermodelDisplayTemplate).IsAssignableFrom(modelMetadata.ModelType))
            {
                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                // ReSharper disable PossibleNullReferenceException
                return (modelMetadata.Model as ISupermodelDisplayTemplate).DisplayTemplate(MakeNestedHtmlHelper(modelMetadata, htmlFieldName));
                // ReSharper restore PossibleNullReferenceException
            }

            //Special handling for MvcHtmlString
            if (typeof(MvcHtmlString).IsAssignableFrom(modelMetadata.ModelType))
            {
                return (MvcHtmlString)modelMetadata.Model;
            }

            return _html.Display(expression);
        }
        public MvcHtmlString DisplayFor<ValueT>(Expression<Func<ModelT, ValueT>> expression, bool forceUsingModelValues = true)
        {
            //if model is valid, we want to grab stuff from the model -- not model state
            if (forceUsingModelValues && _html.ViewData.ModelState.IsValid) _html.ViewData.ModelState.Clear();

            var html = (HtmlHelper<ModelT>)_html;
            
            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            var htmlFieldName = ExpressionHelper.GetExpressionText(expression);

            if (typeof(ISupermodelDisplayTemplate).IsAssignableFrom(modelMetadata.ModelType))
            {
                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                // ReSharper disable PossibleNullReferenceException
                return (modelMetadata.Model as ISupermodelDisplayTemplate).DisplayTemplate(MakeNestedHtmlHelper(modelMetadata, htmlFieldName));
                // ReSharper restore PossibleNullReferenceException
            }

            //Special handling for MvcHtmlString
            if (typeof(MvcHtmlString).IsAssignableFrom(modelMetadata.ModelType))
            {
                return (MvcHtmlString)modelMetadata.Model;
            }

            return html.DisplayFor(expression);
        }
        public MvcHtmlString DisplayForModel(bool forceUsingModelValues = true)
        {
            //if model is valid, we want to grab stuff from the model -- not model state
            if (forceUsingModelValues && _html.ViewData.ModelState.IsValid) _html.ViewData.ModelState.Clear();
            
            var modelMetadata = _html.ViewData.ModelMetadata;

            if (typeof(ISupermodelDisplayTemplate).IsAssignableFrom(modelMetadata.ModelType))
            {
                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                // ReSharper disable PossibleNullReferenceException
                return (modelMetadata.Model as ISupermodelDisplayTemplate).DisplayTemplate(_html);
                // ReSharper restore PossibleNullReferenceException
            }

            //Special handling for MvcHtmlString
            if (typeof(MvcHtmlString).IsAssignableFrom(modelMetadata.ModelType))
            {
                return (MvcHtmlString)modelMetadata.Model;
            }
            
            return _html.DisplayForModel();
        }

        public MvcHtmlString Hidden(string expression, bool forceUsingModelValues = true)
        {
            //if model is valid, we want to grab stuff from the model -- not model state
            if (forceUsingModelValues && _html.ViewData.ModelState.IsValid) _html.ViewData.ModelState.Clear();
            
            var modelMetadata = ModelMetadata.FromStringExpression(expression, _html.ViewData);
            var htmlFieldName = ExpressionHelper.GetExpressionText(expression);

            if (typeof(ISupermodelHiddenTemplate).IsAssignableFrom(modelMetadata.ModelType))
            {
                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                // ReSharper disable PossibleNullReferenceException
                return (modelMetadata.Model as ISupermodelHiddenTemplate).HiddenTemplate(MakeNestedHtmlHelper(modelMetadata, htmlFieldName));
                // ReSharper restore PossibleNullReferenceException
            }
            return _html.Hidden(expression);
        }
        public MvcHtmlString HiddenFor<ValueT>(Expression<Func<ModelT, ValueT>> expression, bool forceUsingModelValues = true)
        {
            //if model is valid, we want to grab stuff from the model -- not model state
            if (forceUsingModelValues && _html.ViewData.ModelState.IsValid) _html.ViewData.ModelState.Clear();
            
            var html = (HtmlHelper<ModelT>)_html;

            var modelMetadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
            var htmlFieldName = ExpressionHelper.GetExpressionText(expression);

            if (typeof(ISupermodelHiddenTemplate).IsAssignableFrom(modelMetadata.ModelType))
            {
                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                // ReSharper disable PossibleNullReferenceException
                return (modelMetadata.Model as ISupermodelHiddenTemplate).HiddenTemplate(MakeNestedHtmlHelper(modelMetadata, htmlFieldName));
                // ReSharper restore PossibleNullReferenceException
            }

            return html.HiddenFor(expression);
        }
        public MvcHtmlString HiddenForModel(bool forceUsingModelValues = true)
        {
            //if model is valid, we want to grab stuff from the model -- not model state
            if (forceUsingModelValues && _html.ViewData.ModelState.IsValid) _html.ViewData.ModelState.Clear();
            
            var modelMetadata = _html.ViewData.ModelMetadata;

            if (typeof(ISupermodelHiddenTemplate).IsAssignableFrom(modelMetadata.ModelType))
            {
                if (modelMetadata.Model == null) modelMetadata.Model = ReflectionHelper.CreateType(modelMetadata.ModelType);

                // ReSharper disable PossibleNullReferenceException
                return (modelMetadata.Model as ISupermodelHiddenTemplate).HiddenTemplate(_html);
                // ReSharper restore PossibleNullReferenceException
            }

            //Html.HiddenForModel does not exist in MVC so we do custom one here
            var result = new StringBuilder();
            if (_html.ViewData.TemplateInfo.TemplateDepth <= 1)
            {
                var properties = _html.ViewData.ModelMetadata.Properties.Where(pm => pm.ShowForEdit && !_html.ViewData.TemplateInfo.Visited(pm));
                foreach (var prop in properties)
                {
                    result.AppendLine(_html.Supermodel().Hidden(prop.PropertyName).ToString());
                }
            }
            return MvcHtmlString.Create(result.ToString());
        }

        public HtmlHelper MakeNestedHtmlHelper(ModelMetadata metadata, string htmlFieldName)
        {
            var viewData = new ViewDataDictionary(_html.ViewDataContainer.ViewData)
            {
                Model = metadata.Model,
                ModelMetadata = metadata,
                TemplateInfo = new TemplateInfo
                {
                    FormattedModelValue = metadata.Model,
                    HtmlFieldPrefix = _html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlFieldName),
                }
            };
            return new HtmlHelper(new ViewContext(_html.ViewContext, _html.ViewContext.View, viewData, _html.ViewContext.TempData, _html.ViewContext.Writer), new ViewDataContainer(viewData));
        }
        #endregion

        #region Query String Route Values
        public RouteValueDictionary QueryStringRouteValues()
        {
            return _html.ViewContext.RequestContext.HttpContext.Request.QueryString.Supermodel().ToRouteValueDictionary();
        }
        #endregion

        #region Query String Skip, Take and Sort Grabbers
        public int? GetSkipValue()
        {
            return _html.ViewContext.RequestContext.HttpContext.Request.QueryString.Supermodel().GetSkipValue();
        }
        public int? GetTakeValue()
        {
            return _html.ViewContext.RequestContext.HttpContext.Request.QueryString.Supermodel().GetTakeValue();
        }
        public string GetSortByValue()
        {
            return _html.ViewContext.RequestContext.HttpContext.Request.QueryString.Supermodel().GetSortByValue();
        }
        #endregion

        #region SortBy Helpers
        public MvcHtmlString SortByDropdown(Supermodel.SortByOptions sortByOptions, object htmlAttributes = null)
        {
            return SortByDropdown(sortByOptions, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcHtmlString SortByDropdown(Supermodel.SortByOptions sortByOptions, IDictionary<string, object> htmlAttributesDict)
        {
            var result = new StringBuilder();

            //begin form
            result.AppendLine(_html.Supermodel().BeginGetFormMvcHtmlString().ToString());

            //Get all the query string params, except the dropdown
            var queryString = _html.ViewContext.RequestContext.HttpContext.Request.QueryString;
            foreach (var queryStringKey in queryString.AllKeys)
            {
                switch (queryStringKey)
                {
                    case "smSortBy": 
                        continue;

                    case "smSkip":
                        result.AppendLine("<input id='smSkip' name='smSkip' type='hidden' value='0' />");
                        break;

                    case "_":
                        result.AppendLine(_html.Hidden("<input name='_' type='hidden' value='" + DateTime.Now.Ticks).ToString());
                        break;

                    default:
                        result.AppendLine(_html.Hidden(queryStringKey, queryString[queryStringKey]).ToString());
                        break;
                }
            }

            //Get the dropdown
            var sortBySelectList = new List<SelectListItem> { new SelectListItem { Value = "", Text = "Select Sort Order" } };
            foreach (var sortByOption in sortByOptions) sortBySelectList.Add(new SelectListItem { Value = sortByOption.Value, Text = "Sort By: " + sortByOption.Key });

            //Create an empty dict or a copy of it
            htmlAttributesDict = htmlAttributesDict == null ? new Dictionary<string, object>() : new Dictionary<string, object>(htmlAttributesDict);
            
            htmlAttributesDict["onchange"] = "this.form.submit();";
            result.AppendLine(_html.DropDownList("smSortBy", sortBySelectList, htmlAttributesDict).ToString());

            //end form
            result.AppendLine(_html.Supermodel().EndFormMvcHtmlString().ToString());

            return MvcHtmlString.Create(result.ToString());
        }

        public MvcHtmlString SortableColumnHeader(string headerName, string orderBy, string orderByDesc, MvcHtmlString sortedHtml = null, MvcHtmlString sortedHtmlDesc = null, object htmlAttributes = null)
        {
            return SortableColumnHeader(headerName, orderBy, orderByDesc, sortedHtml, sortedHtmlDesc, HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes));
        }
        public MvcHtmlString SortableColumnHeader(string headerName, string orderBy, string orderByDesc, MvcHtmlString sortedHtml, MvcHtmlString sortedHtmlDesc, IDictionary<string, object> htmlAttributesDict)
        {
            sortedHtml = sortedHtml ?? MvcHtmlString.Create("▲");
            sortedHtmlDesc = sortedHtmlDesc ?? MvcHtmlString.Create("▼");
            
            var result = new StringBuilder();
            result.AppendLine("<th>");

            var queryString = _html.ViewContext.RequestContext.HttpContext.Request.QueryString;
            var currentSortbyValue = (queryString["smSortBy"] ?? "").Trim();
            var action = (string)_html.ViewContext.RouteData.Values["action"];

            var routeValues = queryString.Supermodel().ToRouteValueDictionary().Supermodel().
                AddOrUpdateWith("smSkip", 0).Supermodel().
                AddOrUpdateWith("smSortBy", orderBy);

            var routeValuesDesc = queryString.Supermodel().ToRouteValueDictionary().Supermodel().
                AddOrUpdateWith("smSkip", 0).Supermodel().
                AddOrUpdateWith("smSortBy", orderByDesc);

            if (currentSortbyValue == orderBy)
            {
                if (!string.IsNullOrEmpty(orderByDesc)) result.AppendLine(_html.ActionLink(headerName, action, routeValuesDesc, htmlAttributesDict).ToString() + sortedHtml);
                else if (!string.IsNullOrEmpty(orderBy)) result.AppendLine(_html.ActionLink(headerName, action, routeValues, htmlAttributesDict).ToString() + sortedHtml);
                else result.AppendLine(headerName);
            }
            else if (currentSortbyValue == orderByDesc)
            {
                if (!string.IsNullOrEmpty(orderBy)) result.AppendLine(_html.ActionLink(headerName, action, routeValues, htmlAttributesDict).ToString() + sortedHtmlDesc);
                else if (!string.IsNullOrEmpty(orderByDesc)) result.AppendLine(_html.ActionLink(headerName, action, routeValuesDesc, htmlAttributesDict).ToString() + sortedHtmlDesc);
                else result.AppendLine(headerName);
            }
            else
            {
                if (!string.IsNullOrEmpty(orderBy)) result.AppendLine(_html.ActionLink(headerName, action, routeValues, htmlAttributesDict).ToString());
                else if (!string.IsNullOrEmpty(orderByDesc)) result.AppendLine(_html.ActionLink(headerName, action, routeValuesDesc, htmlAttributesDict).ToString());
                else result.AppendLine(headerName);
            }

            result.AppendLine("</th>");
            return MvcHtmlString.Create(result.ToString());
        }
        #endregion

        #region HtmlHelper context
        private readonly HtmlHelper _html;
        #endregion
    }
}