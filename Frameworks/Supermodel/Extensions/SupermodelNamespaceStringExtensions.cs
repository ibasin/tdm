﻿using System;

namespace Supermodel.Extensions
{
    public class SupermodelNamespaceStringExtensions
    {
        #region Constructros
        public SupermodelNamespaceStringExtensions(string str)
        {
            _str = str;
        } 
        #endregion

        #region Methods
        public string DisableAllControls()
        {
            return _str.Replace("<input ", "<input disabled='disabled' ").Replace("<textarea ", "<textarea disabled='disabled' ").Replace("<select ", "<select disabled='disabled' ");
        }

        public string DisableAllControlsIf(bool condition)
        {
            return condition ? DisableAllControls() : _str;
        }

        public string MakeRequired()
        {
            return _str.Replace("class=\"", "class=\"required ");
        }

        public string ToStringHandleNull()
        {
            return _str ?? "";
        }

        public string CapLength(int len)
        {
            if (_str == null) return "";
            if (_str.Length <= len) return _str;
            return _str.Substring(0, len - 3) + "...";
        }

        public string RemoveStartingFirst(string delimiter)
        {
            var dashIndex = _str.IndexOf(delimiter, StringComparison.Ordinal);
            return dashIndex > 0 ? _str.Substring(0, dashIndex) : _str;
        }

        public string RemoveStartingLast(string delimiter)
        {
            var dashIndex = _str.LastIndexOf(delimiter, StringComparison.Ordinal);
            return dashIndex > 0 ? _str.Substring(0, dashIndex) : _str;
        }
        #endregion

        #region Private Context
        private readonly string _str; 
        #endregion
    }
}
