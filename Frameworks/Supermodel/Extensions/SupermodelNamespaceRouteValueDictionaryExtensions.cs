﻿using System.Web.Routing;

namespace Supermodel.Extensions
{
    public class SupermodelNamespaceRouteValueDictionaryExtensions
    {
        public SupermodelNamespaceRouteValueDictionaryExtensions(RouteValueDictionary rvDict)
        {
            _rvDict = rvDict;
        }

        // ReSharper disable ParameterTypeCanBeEnumerable.Global
        public RouteValueDictionary AddOrUpdateWith(RouteValueDictionary another)
        // ReSharper restore ParameterTypeCanBeEnumerable.Global
        {
            foreach (var pair in another) _rvDict[pair.Key] = pair.Value;
            return _rvDict;
        }
        
        public RouteValueDictionary AddOrUpdateWith(string key, object value)
        {
            _rvDict[key] = value;
            return _rvDict;
        }

        public RouteValueDictionary RemoveKey(string key)
        {
            _rvDict.Remove(key);
            return _rvDict;
        }

        public RouteValueDictionary RemoveKeys(params string[] keys)
        {
            foreach (string key in keys) _rvDict.Remove(key);
            return _rvDict;
        }

        private readonly RouteValueDictionary _rvDict;
    }
}