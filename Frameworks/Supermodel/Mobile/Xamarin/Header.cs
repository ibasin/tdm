﻿//   PROJECT SETUP INSTRUCTIONS
//
//   Please add the following references:
//
//   * For all (.NET server/desktop, iOS, Adnroid and UWP) projects
//     - Xamarin.Forms (NuGet package)
//     - PCLCrypto (Nuget package)
//     - Json.NET (NuGet package)
//     - Xam.Plugin.Media (NuGet package)
//
//   * For iOS and Android projects only   
//     - ModernHttpClient (NuGet package) -- this one is only for Mobile
//
//   * For .NET server/desktop, iOS, and Adnroid projects only
//     - System.Data.Sqlite (if using Visual Studio) or Mono.Data.Sqlite (if using Xamarin Studio). 
//       If programming for .NET server/desktop, add it as a Mono.Data.Sqlite.Portable NuGet
//     - System.Data
//     - System.ComponentModel.DataAnnotations
//     - System.Runtime.Serialization
//     - System.Web.Services
//   
//   * For .NET server/desktop project only
//      - System.Net.Http.Formatting
//      - System.Web
//
//   * For UWP project only
//      - Reference SQLite for Universal Windows Platform extension (if not there, follow intructions at:
//        https://developer.xamarin.com/guides/xamarin-forms/working-with/databases/#Adding_SQLite_database_support_to_Windows_Phone_8)
//      - Refernce Visual C++ 2015 Runtime for Universal Windows Platform Apps extension
//
//   * For iOS project only 
//      - Under iOS Build please add "--nolinkaway" to Additional mtouch arguments
//        For more info on this, pleasee see: https://spouliot.wordpress.com/2011/11/01/linker-vs-linked-away-exceptions/
//

//SQLite.Net directives
#if WINDOWS_PHONE && !USE_WP8_NATIVE_SQLITE
#define USE_CSHARP_SQLITE
#endif

#if NETFX_CORE
#define USE_NEW_REFLECTION_API
#endif

//Custom Renderers
#if WINDOWS_UWP
[assembly: Xamarin.Forms.Platform.UWP.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtEntry), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtEntryRenderer))]
[assembly: Xamarin.Forms.Platform.UWP.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtPicker), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtPickerRenderer))]
[assembly: Xamarin.Forms.Platform.UWP.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtDatePicker), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtDatePickerRenderer))]
#elif __MOBILE__
[assembly: Xamarin.Forms.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtEntry), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtEntryRenderer))]
[assembly: Xamarin.Forms.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtPicker), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtPickerRenderer))]
[assembly: Xamarin.Forms.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtDatePicker), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtDatePickerRenderer))]
#endif

//Services
#if __MOBILE__
[assembly: Xamarin.Forms.Dependency(typeof(Supermodel.Mobile.Services.AudioService))]
#endif
