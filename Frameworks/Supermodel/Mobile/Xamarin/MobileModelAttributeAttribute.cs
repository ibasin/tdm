﻿using System;

namespace Supermodel.Mobile.Xamarin
{
    public class MobileModelAttributeAttribute : Attribute
    {
        public MobileModelAttributeAttribute(string attrContent)
        {
            AttrContent = attrContent;
        }
        
        public string AttrContent { get; set;}
    }
}
