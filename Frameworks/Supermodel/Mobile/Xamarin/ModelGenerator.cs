﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Supermodel.Extensions;
using Supermodel.MvcAndWebApi.Controllers.ApiControllers.Async;
using Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync;

namespace Supermodel.Mobile.Xamarin
{
    public class ModelGenerator
    {
        #region Constructors
        public ModelGenerator(Assembly webAssembly, string nameSpace = "Supermodel.Mobile.Runtime.Models") : this(new List<Assembly> { webAssembly }, nameSpace) { }
        public ModelGenerator(IEnumerable<Assembly> assemblies, string nameSpace = "Supermodel.Mobile.Runtime.Models")
        {
            Assemblies = assemblies;
            NameSpace = nameSpace;
        }
        #endregion

        #region Methods
        public CSOutStringBuilder GenerateModelsAndSaveToDisk(string filename = @"..\..\" + "Supermodel.Mobile.ModelsAndRuntime.cs", CSOutStringBuilder sb = null)
        {
            if (sb == null) sb = new CSOutStringBuilder();
            sb = GenerateModels(sb);
            File.WriteAllText(filename, sb.ToString());
            return sb;
        }
        public CSOutStringBuilder GenerateModels(CSOutStringBuilder sb = null)
        {
            _globalCustomTypesDefined.Clear();

            if (sb == null) sb = new CSOutStringBuilder();
            var customTypesDefined = new List<Type>();

            sb.AppendLine("//   DO NOT MAKE CHANGES TO THIS FILE. THEY MIGHT GET OVERWRITTEN!!!");
            sb.AppendLine("//   Autogenrated by Supermodel.Mobile on " + DateTime.Now);
            sb.AppendLine("//");
            sb.AppendLine(EmbeddedResource.ReadTextFile(GetType().Assembly, "Supermodel.Mobile.Xamarin.Header.cs"));
            sb.AppendLine("#region Models");
            sb.AppendLineFormat("namespace {0}", NameSpace);
            sb.AppendLineIndentPlus("{");
            sb.AppendLine("using System;");
            sb.AppendLine("using Mobile.Models;");
            sb.AppendLine("using Attributes;");
            sb.AppendLine("using System.Collections.Generic;");
            sb.AppendLine("using System.ComponentModel;");
            sb.AppendLine("using System.Threading.Tasks;");
            sb.AppendLine("using DataContext.WebApi;");
            

            foreach (var controllerType in FindAllSupermodelControllers())
            {
                if (!controllerType.Name.EndsWith("Controller")) throw new SupermodelSystemErrorException("Controller " + controllerType.Name + ": names by convention must end with 'Controller'");

                var modelTypes = GetModelTypesForController(controllerType, out var controllerKind);
                if (controllerKind == ControllerKindEnum.Command && modelTypes.Count != 2) throw new Exception("controllerKind == ControllerKindEnum.Command && modelTypes.Count != 2");
                if (controllerKind == ControllerKindEnum.CRUD && modelTypes.Count != 2) throw new Exception("controllerKind == ControllerKindEnum.CRUD && modelTypes.Count != 2");
                if (controllerKind == ControllerKindEnum.EnhacedCRUD && modelTypes.Count != 3) throw new Exception("controllerKind == ControllerKindEnum.EnhacedCRUD && modelTypes.Count != 3");

                sb.AppendLine("");
                sb.AppendLineFormat("#region {0}", controllerType.Name);

                if (controllerKind == ControllerKindEnum.Command)
                {
                    //if (!modelTypes[0].Name.EndsWith("ApiModel")) throw new SupermodelSystemErrorException("Model " + modelTypes[0].Name + ": names by convention must end with 'ApiModel'");
                    //if (!modelTypes[1].Name.EndsWith("ApiModel")) throw new SupermodelSystemErrorException("Model " + modelTypes[0].Name + ": names by convention must end with 'ApiModel'");

                    var commandName = controllerType.Name.Replace("Controller", "");
                    var inputType = modelTypes[0].Name.Replace("ApiModel", "");
                    var outputType = modelTypes[1].Name.Replace("ApiModel", "");

                    sb.AppendLine($"//Extension method for {commandName} command");
                    sb.AppendLine($"public static class {commandName}CommandExt");
                    sb.AppendLineIndentPlus("{");
                    sb.AppendLine($"public static async Task<{outputType}> {commandName}Async(this WebApiDataContext me, {inputType} input)");
                    sb.AppendLineIndentPlus("{");
                    sb.AppendLine($"return await me.ExecutePostAsync<{inputType}, {outputType}>(\"{commandName}\", input);");
                    sb.AppendLineIndentMinus("}");
                    sb.AppendLineIndentMinus("}");

                    if (!_globalCustomTypesDefined.Contains(modelTypes[0]))
                    {
                        _globalCustomTypesDefined.Add(modelTypes[0]);
                        sb.AppendLine("// ReSharper disable once PartialTypeWithSinglePart");
                        sb.AppendLineFormat("public partial class {0}", modelTypes[0].Name.Replace("ApiModel", ""));
                        sb = GeneratePropertiesForModel(modelTypes[0], customTypesDefined, false, sb);
                    }
                    else
                    {
                        sb.AppendLineFormat("//Class {0} is defined elsewhere", modelTypes[0].Name.Replace("ApiModel", ""));
                    }

                    if (!_globalCustomTypesDefined.Contains(modelTypes[1]))
                    {
                        _globalCustomTypesDefined.Add(modelTypes[1]);
                        sb.AppendLine("// ReSharper disable once PartialTypeWithSinglePart");
                        sb.AppendLineFormat("public partial class {0}", modelTypes[1].Name.Replace("ApiModel", ""));
                        sb = GeneratePropertiesForModel(modelTypes[1], customTypesDefined, false, sb);
                    }
                    else
                    {
                        sb.AppendLineFormat("//Class {0} is defined elsewhere", modelTypes[1].Name.Replace("ApiModel", ""));
                    }
                }

                if (controllerKind == ControllerKindEnum.CRUD || controllerKind == ControllerKindEnum.EnhacedCRUD)
                {
                    var restUrlAttribute = $"[RestUrl(\"{controllerType.Name.Replace("Controller", "")}\")]";
                    if (modelTypes[0] == modelTypes[1]) //Detail and List are the same model
                    {
                        if (!modelTypes[0].Name.EndsWith("ApiModel")) throw new SupermodelSystemErrorException("Model " + modelTypes[0].Name + ": names by convention must end with 'ApiModel'");
                    }
                    else
                    {
                        if (!modelTypes[0].Name.EndsWith("DetailApiModel")) throw new SupermodelSystemErrorException("Detail Model " + modelTypes[0].Name + ": names by convention must end with 'DetailApiModel'");
                    }

                    if (!_globalCustomTypesDefined.Contains(modelTypes[0]))
                    {
                        _globalCustomTypesDefined.Add(modelTypes[0]);
                        sb.AppendLine(restUrlAttribute);
                        sb.AppendLine("// ReSharper disable once PartialTypeWithSinglePart");
                        sb.AppendLineFormat("public partial class {0} : Model", modelTypes[0].Name.Replace("DetailApiModel", "").Replace("ApiModel", ""));
                        sb = GeneratePropertiesForModel(modelTypes[0], customTypesDefined, false, sb);
                    }
                    else
                    {
                        sb.AppendLineFormat("//Class {0} is defined elsewhere", modelTypes[0].Name.Replace("DetailApiModel", "").Replace("ApiModel", ""));
                    }

                    //if List and Detail types are diffeent, do a separate one here
                    if (modelTypes[0] != modelTypes[1])
                    {
                        sb.AppendLine("");
                        sb.AppendLine(restUrlAttribute);
                        if (!modelTypes[1].Name.EndsWith("ListApiModel")) throw new SupermodelSystemErrorException("List Model " + modelTypes[0].Name + ": names by convention must end with 'ListApiModel'");

                        if (!_globalCustomTypesDefined.Contains(modelTypes[1]))
                        {
                            _globalCustomTypesDefined.Add(modelTypes[1]);
                            sb.AppendLine("// ReSharper disable once PartialTypeWithSinglePart");
                            sb.AppendLineFormat("public partial class {0} : Model", modelTypes[1].Name.Replace("ApiModel", ""));
                            sb = GeneratePropertiesForModel(modelTypes[1], customTypesDefined, false, sb);
                        }
                        else
                        {
                            sb.AppendLineFormat("//Class {0} is defined elsewhere", modelTypes[1].Name.Replace("ApiModel", ""));
                        }
                    }

                    //if we are dealing with an enhanced controller, do the search model
                    if (controllerKind == ControllerKindEnum.EnhacedCRUD)
                    {
                        sb.AppendLine("");
                        if (!modelTypes[2].Name.EndsWith("SearchApiModel")) throw new SupermodelSystemErrorException("Search Model " + modelTypes[0].Name + ": names by convention must end with 'SearchApiModel'");

                        if (!_globalCustomTypesDefined.Contains(modelTypes[2]))
                        {
                            _globalCustomTypesDefined.Add(modelTypes[2]);
                            sb.AppendLine("// ReSharper disable once PartialTypeWithSinglePart");
                            sb.AppendLineFormat("public partial class {0}", modelTypes[2].Name.Replace("ApiModel", ""));
                            sb = GeneratePropertiesForModel(modelTypes[2], customTypesDefined, true, sb);
                        }
                        else
                        {
                            sb.AppendLineFormat("//Class {0} is defined elsewhere", modelTypes[2].Name.Replace("ApiModel", ""));
                        }
                    }
                }
                sb.AppendLine("#endregion");
            }

            //Add types marked by an IncludeInMobileRuntime attribute
            foreach (var assembly in Assemblies)
            {
                var types = assembly.GetTypes().Where(x => Attribute.IsDefined(x, typeof(IncludeInMobileRuntimeAttribute))).ToList();
                foreach (var type in types)
                {
                    if (!_globalCustomTypesDefined.Contains(type))
                    {
                        customTypesDefined.Add(type);
                        _globalCustomTypesDefined.Add(type);
                    }
                }
            }

            sb.AppendLine("");
            sb.AppendLine("#region Types models depend on and types that were specifically marked with [IncludeInMobileRuntime]");
            sb = GenerateCustomTypes(customTypesDefined, sb);
            sb.AppendLine("#endregion");

            sb.AppendLineIndentMinus("}");
            sb.AppendLine("#endregion");

            sb.AppendLine("");

            //sb = GenerateRuntimeLibFromDir(@"..\..\Mobile\Xamarin\Supermodel.Mobile.Runtime", sb);
            var runtimeLib = EmbeddedResource.ReadTextFile(GetType().Assembly, "Supermodel.Mobile.Xamarin.Supermodel.Mobile.Runtime.cs");
            sb.AppendLine(runtimeLib);
            return sb;
        }
        #endregion

        #region Private Helper Methods
        private CSOutStringBuilder GeneratePropertiesForModel(Type modelType, List<Type> customTypesDefined, bool generateIdProp, CSOutStringBuilder sb)
        {
            if (sb == null) sb = new CSOutStringBuilder();

            sb.AppendLineIndentPlus("{");
            sb.AppendLine("#region Properties");
            foreach (var property in GetPublicProperties(modelType))
            {
                //skip if property is marked with [JsonIgnore]
                if (property.GetCustomAttributes().SingleOrDefault(x => x is JsonIgnoreAttribute) != null) continue;

                var customModelAttributeAttribute = (MobileModelAttributeAttribute)property.GetCustomAttributes().SingleOrDefault(x => x is MobileModelAttributeAttribute);
                if (customModelAttributeAttribute != null) sb.Append(customModelAttributeAttribute.AttrContent + " ");

                if (property.PropertyType.IsPrimitive)
                {
                    if (generateIdProp || property.Name != "Id")
                    {
                        sb.AppendFormat("public {0} {1} ", property.PropertyType.Name, property.Name);
                        sb.AppendLine("{ get; set; }");
                    }
                }
                else if (property.PropertyType != typeof(string) && property.PropertyType.GetInterfaces().Any(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)))
                {
                    var genericArgType = property.PropertyType.GetInterfaces().Single(x => x.IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)).GetGenericArguments()[0];
                    if (!_globalCustomTypesDefined.Contains(genericArgType))
                    {
                        customTypesDefined.Add(genericArgType);
                        _globalCustomTypesDefined.Add(genericArgType);
                    }
                    var genericParam = genericArgType.Name.Replace("ApiModel", "");
                    sb.AppendFormat("public List<{0}> {1} ", genericParam, property.Name);
                    sb.AppendLineFormat("{{ get; set; }} = new List<{0}>();", genericParam);
                }
                else if (Nullable.GetUnderlyingType(property.PropertyType) != null)
                {
                    var underlyingPropType = Nullable.GetUnderlyingType(property.PropertyType);
                    if (underlyingPropType == null) throw new Exception("underlyingPropType == null: this should never happen");
                    sb.AppendFormat("public {0}? {1} ", underlyingPropType.Name, property.Name);
                    if (underlyingPropType.IsEnum)
                    {
                        if (!_globalCustomTypesDefined.Contains(underlyingPropType))
                        {
                            customTypesDefined.Add(underlyingPropType);
                            _globalCustomTypesDefined.Add(underlyingPropType);
                        }
                    }
                    sb.AppendLine("{ get; set; }");

                }
                else if (property.PropertyType == typeof(string) || property.PropertyType == typeof(DateTime) || property.PropertyType.Name == "BinaryFileApiModel")
                {
                    var typeName = property.PropertyType.Name.Replace("ApiModel", "");
                    sb.AppendFormat("public {0} {1} ", typeName, property.Name);

                    if (property.PropertyType.Name == "BinaryFileApiModel" || property.PropertyType == typeof(DateTime)) sb.AppendLineFormat("{{ get; set; }} = new {0}();", typeName);
                    else sb.AppendLine("{ get; set; }");
                }
                else if (property.PropertyType.IsEnum || property.PropertyType.IsClass || property.PropertyType.IsValueType)
                {
                    sb.AppendFormat("public {0} {1} ", property.PropertyType.Name.Replace("ApiModel", ""), property.Name);
                    if (!_globalCustomTypesDefined.Contains(property.PropertyType))
                    {
                        customTypesDefined.Add(property.PropertyType);
                        _globalCustomTypesDefined.Add(property.PropertyType);
                    }
                    sb.AppendLine("{ get; set; }");
                }
                else
                {
                    throw new SupermodelSystemErrorException("Unexpected property type '" + property.PropertyType.Name + "'");
                }
            }
            sb.AppendLine("#endregion");
            sb.AppendLineIndentMinus("}");
            return sb;
        }
        private CSOutStringBuilder GenerateCustomTypes(List<Type> customTypesDefined, CSOutStringBuilder sb)
        {
            var additionalCustomTypes = new List<Type>();
            var firstcustomType = true;
            foreach (var customType in customTypesDefined)
            {
                if (firstcustomType) firstcustomType = false;
                else sb.AppendLine("");

                if (customType.IsEnum)
                {
                    sb.AppendLineFormat("public enum {0}", customType.Name);
                    sb.AppendLineIndentPlus("{");
                    var first = true;
                    foreach (var enumValue in Enum.GetValues(customType))
                    {
                        var descriptionAttribute = "";
                        var descriptionStr = enumValue.GetDescription();
                        if (descriptionStr != enumValue.ToString()) descriptionAttribute = $"[Description(\"{descriptionStr}\")] ";

                        var disabledAttribute = "";
                        var isDisabled = enumValue.IsDisabled();
                        if (isDisabled) disabledAttribute = "[Disabled] ";

                        if (first)
                        {
                            sb.AppendFormat("{0}{1}{2} = {3}", descriptionAttribute, disabledAttribute, enumValue.ToString(), Convert.ChangeType(enumValue, Enum.GetUnderlyingType(customType)));
                            first = false;
                        }
                        else
                        {
                            sb.AppendLine(",");
                            sb.AppendFormat("{0}{1}{2} = {3}", descriptionAttribute, disabledAttribute, enumValue.ToString(), Convert.ChangeType(enumValue, Enum.GetUnderlyingType(customType)));
                        }
                    }
                    sb.AppendLine("");
                    sb.AppendLineIndentMinus("}");
                }
                else if (customType.IsClass || customType.IsValueType)
                {
                    sb.AppendLine("// ReSharper disable once PartialTypeWithSinglePart");
                    sb.AppendLineFormat("public partial class {0}", customType.Name.Replace("ApiModel", ""));
                    sb = GeneratePropertiesForModel(customType, additionalCustomTypes, true, sb);
                }
                else
                {
                    throw new SupermodelSystemErrorException("Unexpected Custom Type '" + customType.Name + "'");
                }
            }

            if (additionalCustomTypes.Any()) GenerateCustomTypes(additionalCustomTypes, sb);
            return sb;
        }
        private List<PropertyInfo> GetPublicProperties(Type modelType)
        {
            return modelType.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(x => x.CanRead && x.CanWrite).ToList();
        }
        private List<Type> GetModelTypesForController(Type controllerType, out ControllerKindEnum controllerKind)
        {
            var modelTypes = new List<Type>();

            var crudControllerInterfaceType = controllerType.GetInterfaces().SingleOrDefault(i => i.IsGenericType && (i.GetGenericTypeDefinition() == _syncCRUDInterfaceType || i.GetGenericTypeDefinition() == _asyncCRUDInterfaceType));
            if (crudControllerInterfaceType != null)
            {
                var crudGenericArguments = crudControllerInterfaceType.GetGenericArguments();
                modelTypes.Add(crudGenericArguments[0]);
                modelTypes.Add(crudGenericArguments[1]);

                //if this is an enhanced CRUD controller, grab the third argument (SearchType)
                var controllerEnhancedInterfaceType = controllerType.GetInterfaces().SingleOrDefault(i => i.IsGenericType && (i.GetGenericTypeDefinition() == _synchEnhancedInterfaceType || i.GetGenericTypeDefinition() == _asynchEnhancedInterfaceType));
                if (controllerEnhancedInterfaceType != null)
                {
                    controllerKind = ControllerKindEnum.EnhacedCRUD;
                    modelTypes.Add(controllerEnhancedInterfaceType.GetGenericArguments()[2]);
                }
                else
                {
                    controllerKind = ControllerKindEnum.CRUD;
                }
            }
            else
            {
                var controllerInterfaceType = controllerType.GetInterfaces().Single(i => i.IsGenericType && (i.GetGenericTypeDefinition() == _syncCommandInterfaceType || i.GetGenericTypeDefinition() == _asyncCommandInterfaceType));
                var genericArguments = controllerInterfaceType.GetGenericArguments();
                modelTypes.Add(genericArguments[0]);
                modelTypes.Add(genericArguments[1]);
                controllerKind = ControllerKindEnum.Command;
            }

            return modelTypes;
        }
        private List<Type> FindAllSupermodelControllers()
        {
            var result = new List<Type>();

            foreach (var assembly in Assemblies)
            {
                var assemblyResult = assembly.GetTypes().Where(t => !t.IsAbstract && t.GetInterfaces()
                    .Any(i => i.IsGenericType && (i.GetGenericTypeDefinition() == _syncCRUDInterfaceType || i.GetGenericTypeDefinition() == _asyncCRUDInterfaceType))).ToList();

                result.AddRange(assemblyResult);
            }

            //we do it twice to insure that all Command controllers are after the CRUD controllers
            foreach (var assembly in Assemblies)
            {
                var assemblyResult = assembly.GetTypes().Where(t => !t.IsAbstract && t.GetInterfaces()
                    .Any(i => i.IsGenericType && (i.GetGenericTypeDefinition() == _syncCommandInterfaceType || i.GetGenericTypeDefinition() == _asyncCommandInterfaceType))).ToList();

                result.AddRange(assemblyResult);
            }
            return result;
        }
        #endregion

        #region Proprties & Attributes
        public IEnumerable<Assembly> Assemblies { get; set; }
        public string NameSpace { get; set; }

        private readonly HashSet<Type> _globalCustomTypesDefined = new HashSet<Type>();
        private readonly Type _synchEnhancedInterfaceType = typeof(ISyncEnhancedApiCRUDController<,,>);
        private readonly Type _asynchEnhancedInterfaceType = typeof(IAsyncEnhancedApiCRUDController<,,>);
        private readonly Type _syncCRUDInterfaceType = typeof(ISyncApiCRUDController<,>);
        private readonly Type _asyncCRUDInterfaceType = typeof(IAsyncApiCRUDController<,>);
        private readonly Type _syncCommandInterfaceType = typeof(ISyncApiCommandController<,>);
        private readonly Type _asyncCommandInterfaceType = typeof(IAsyncApiCommandController<,>);
        #endregion
    }
}
