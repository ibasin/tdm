﻿using System;
using System.Text;
using CsvMaker.CsvString;
using CsvMaker.Extensions;
using CsvMaker.Interfaces;
using ReflectionMapper;

namespace CsvMaker.Models
{
    public class CsvModel : ICsvMakerCustom, ICsvReaderCustom, IRMapperCustom
    {
        #region ICsvMakerCustom implementation
        public virtual StringBuilder ToCsvRowCustom(StringBuilder sb = null)
        {
            return this.ToCsvRowBase(sb);
        }
        public virtual StringBuilder ToCsvHeaderCustom(StringBuilder sb = null)
        {
            return this.ToCsvHeaderBase(sb);
        }
        #endregion

        #region ICsvReaderCustom implementation
        public T ValidateCsvHeaderCustom<T>(CsvStringReader sr)
        {
            return (T)(object)this.ValidateCsvHeaderRowBase(sr);
        }

        public T ReadCsvRowCustom<T>(CsvStringReader sr)
        {
            return (T)(object)this.ReadCsvRowBase(sr);
        }
        #endregion

        #region IRMapperCustom implementation
        public virtual object MapFromObjectCustom(object obj, Type objType)
        {
            return this.MapFromObjectCustomBase(obj);
        }
        public virtual object MapToObjectCustom(object obj, Type objType)
        {
            return this.MapToObjectCustomBase(obj);
        }
        #endregion
    }
}
