﻿using System;

namespace CsvMaker.Attributes
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class CsvMakerColumnIgnoreAttribute : Attribute { }
}