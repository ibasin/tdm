﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using CsvMaker.Attributes;
using CsvMaker.CsvString;
using CsvMaker.Interfaces;
using ReflectionMapper;

namespace CsvMaker.Extensions
{
    public static class CsvReader
    {
        public static T ValidateCsvHeaderRow<T>(this T me, CsvStringReader sr)
        {
            if (me is ICsvReaderCustom) return (me as ICsvReaderCustom).ValidateCsvHeaderCustom<T>(sr);
            else return me.ValidateCsvHeaderRowBase(sr);
        }
        public static T ReadCsvRow<T>(this T me, CsvStringReader sr)
        {
            if (me is ICsvReaderCustom) return (me as ICsvReaderCustom).ReadCsvRowCustom<T>(sr);
            else return me.ReadCsvRowBase(sr);
        }

        public static T ValidateCsvHeaderRowBase<T>(this T me, CsvStringReader sr)
        {
            foreach (var property in me.GetType().GetPropertiesInOrder())
            {
                if (typeof(ICsvReaderCustom).IsAssignableFrom(property.PropertyType))
                {
                    //We use existing object if it exists, otherwise we just create a blank object for our purposes
                    var propertyObj = me.PropertyGet(property.Name);
                    if (propertyObj == null)
                    {
                        if (property.PropertyType.GetConstructor(Type.EmptyTypes) == null) throw new Exception($"Property '{property.Name}' is null and no default constructor exists for the type '{property.PropertyType.Name}'");
                        propertyObj = ReflectionHelper.CreateType(property.PropertyType);
                    }
                    
                    // ReSharper disable PossibleNullReferenceException
                    (propertyObj as ICsvReaderCustom).ValidateCsvHeaderCustom<T>(sr);
                    // ReSharper restore PossibleNullReferenceException
                }
                else
                {
                    var expectedHeader = property.GetCustomAttribute<CsvMakerColumnNameAttribute>() != null ?
                        property.GetCustomAttribute<CsvMakerColumnNameAttribute>().Name :
                        property.Name.InsertSpacesBetweenWords();
                    var csvHeader = sr.ReadNextColumn();

                    if (csvHeader != expectedHeader) throw new UnexpectedHeaderException($"'{csvHeader}' while '{expectedHeader}' is expected");
                }
            }
            sr.ReadEOLorEOF();
            return me;
        }
        public static T ReadCsvRowBase<T>(this T me, CsvStringReader sr)
        {
            //var propertyNames = me.GetType().GetPropertiesInOrder().Select(x => x.Name).ToList();
            foreach (var property in me.GetType().GetPropertiesInOrder())
            {
                //We use existing object if it exists, otherwise we just create a blank object for our purposes
                var propertyObj = me.PropertyGet(property.Name);

                if (typeof(ICsvReaderCustom).IsAssignableFrom(property.PropertyType))
                {
                    if (propertyObj == null)
                    {
                        if (property.PropertyType.GetConstructor(Type.EmptyTypes) == null) throw new Exception($"Property '{property.Name}' is null and no dfeault constructor exists for the type '{property.PropertyType.Name}'");
                        propertyObj = ReflectionHelper.CreateType(property.PropertyType);
                    }

                    // ReSharper disable once PossibleNullReferenceException
                    (propertyObj as ICsvReaderCustom).ValidateCsvHeaderCustom<T>(sr);
                }
                else
                {
                    string csvColumnStr;
                    try
                    {
                        csvColumnStr = sr.ReadNextColumn();
                        if (property.GetCustomAttribute<CsvMakerColumnIgnoreAttribute>() != null) csvColumnStr = "";
                    }
                    catch (EOFException)
                    {
                        return me;
                    }
                    
                    try
                    {
                        if (string.IsNullOrWhiteSpace(csvColumnStr)) me.PropertySet(property.Name, null);
                        else if (property.PropertyType == typeof(string)) me.PropertySet(property.Name, csvColumnStr);
                        else if (property.PropertyType == typeof(int) || property.PropertyType == typeof(int?)) me.PropertySet(property.Name, int.Parse(csvColumnStr.Replace(",", "")));
                        else if (property.PropertyType == typeof(long) || property.PropertyType == typeof(long?)) me.PropertySet(property.Name, long.Parse(csvColumnStr.Replace(",", "")));
                        else if (property.PropertyType == typeof(float) || property.PropertyType == typeof(float?)) me.PropertySet(property.Name, float.Parse(csvColumnStr.Replace(",", "")));
                        else if (property.PropertyType == typeof(double) || property.PropertyType == typeof(double?)) me.PropertySet(property.Name, double.Parse(csvColumnStr.Replace(",", "")));
                        else if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(DateTime?)) me.PropertySet(property.Name, DateTime.Parse(csvColumnStr));
                        else if (property.PropertyType == typeof(bool) || property.PropertyType == typeof(bool?)) me.PropertySet(property.Name, ParseBool(csvColumnStr));
                        else if (property.PropertyType.IsEnum) me.PropertySet(property.Name, ParseEnum(property.PropertyType, csvColumnStr));
                        else throw new Exception($"'{property.PropertyType.Name}' type is not supported");
                    }
                    catch (FormatException)
                    {
                        var expectedHeader = property.GetCustomAttribute<CsvMakerColumnNameAttribute>() != null ?
                            property.GetCustomAttribute<CsvMakerColumnNameAttribute>().Name :
                            property.Name.InsertSpacesBetweenWords();
                        throw new FormatException($"Unable to parse value '{csvColumnStr}' for column '{expectedHeader}'");
                    }
                }
            }
            sr.ReadEOLorEOF();
            return me;
        }

        public static List<T> ReadCsv<T>(this List<T> me, CsvStringReader sr) where T: class, new()
        {
            new T().ValidateCsvHeaderRow(sr); //Check file format matches our expectations
            var resultList = new List<T>();

            while(!sr.IsEOF())
            {
                var item = new T();
                item.ReadCsvRow(sr);
                resultList.Add(item);
            }

            me.Clear();
            me.AddRange(resultList);
            return me;
        }

        #region Private Helpers
        private static bool ParseBool(string str)
        {
            var trimmedLowerStr = str.ToLower().Trim();
            if (trimmedLowerStr == "1") return true;
            if (trimmedLowerStr == "x") return true;
            if (trimmedLowerStr == "y") return true;
            if (trimmedLowerStr == "yes") return true;
            if (trimmedLowerStr == "t") return true;
            if (trimmedLowerStr == "true") return true;
            return false;
        }
        private static Enum ParseEnum(Type enumType, string str)
        {
            try
            {
                return (Enum)Enum.Parse(enumType, str);
            }
            catch (Exception)
            {
                var trimmedLowerStr = str.ToLower().Trim();
                var enumValues = Enum.GetValues(enumType);
                foreach (var enumValue in enumValues)
                {
                    if (enumValue.GetDescription().ToLower() == trimmedLowerStr) return (Enum)enumValue;
                }
                throw;
            }
        }
        private static string GetDescription(this object value) //This is the same implemetation as Supermodels
        {
            if (value == null) return "";

            //Tries to find a DescriptionAttribute for a potential friendly name for the enum
            var type = value.GetType();

            var valueToString = value.ToString();

            if (type.IsEnum)
            {
                var memberInfo = type.GetMember(valueToString);
                if (memberInfo.Length > 0)
                {
                    var attr = Attribute.GetCustomAttribute(memberInfo[0], typeof(DescriptionAttribute), true);
                    if (attr != null) return ((DescriptionAttribute)attr).Description;
                }
                //If we have no description attribute, just return the ToString() or ToString().InsertSpacesBetweenWords() for enum
                return value.ToString().InsertSpacesBetweenWords();
            }

            return valueToString;
        }
        #endregion

    }
}
