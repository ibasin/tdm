﻿using System.Text.RegularExpressions;

namespace CsvMaker.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Allows left-to-right invocation of string formatting: <code>"Some format {0}".Format("insert this.");</code>
        /// "Format" conflicts with static method, and single letter method name makes calls more concise.
        /// </summary>
        public static string ExcapeSingleQuotes(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }

            return str.Replace("'", "~");
        }
        public static string ExcapeCommas(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }

            return str.Replace(",", "~");
        }
        public static string ExcapeCommasSingleQuotes(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }

            return str.ExcapeCommas().ExcapeSingleQuotes();
        }
        public static string ExcapeCommasSingleQuotesNewLine(this string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }

            return Regex.Replace(str.ExcapeCommasSingleQuotes(), @"\r\n?|\n", " ");
        }
        public static string PrepareCvsColumn(this string str)
        {
            //handle doublequotes
            str = str.Replace("\"", "\"\"");

            //handle comma
            if (str.Contains(",") || str.Contains("\"")) str = $"\"{str}\"";

            return str;
        }
    }
}
