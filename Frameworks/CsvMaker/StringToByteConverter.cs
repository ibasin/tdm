﻿using System.Text;

namespace CsvMaker
{
    public static class StringToByteConverter
    {
        public static byte[] GetBytes(this string str)
        {
            return Encoding.Default.GetBytes(str);
            
            //var bytes = new byte[str.Length * sizeof(char)];
            //Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            //return bytes;
        }

        public static string GetString(this byte[] bytes)
        {
            return Encoding.Default.GetString(bytes);
            
            //var chars = new char[bytes.Length / sizeof(char)];
            //Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            //return new string(chars);
        }
    }
}
