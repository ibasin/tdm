﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Supersonic.Linq
{
    internal class IndexedListQueryProvider<ItemT> : IQueryProvider where ItemT : class
    {
        #region Constructors
        public IndexedListQueryProvider(SupersonicList<ItemT> supersonicList)
        {
            SupersonicList = supersonicList;
        }
        #endregion

        #region Methods
        public IQueryable CreateQuery(Expression expression)
        {
            try
            {
                //return new IndexedList<ItemT>(this, expression);
                return (IQueryable)Activator.CreateInstance(typeof(SupersonicList<>).MakeGenericType(expression.Type), this, expression);
            }
            catch (TargetInvocationException ex)
            {
                // ReSharper disable once PossibleNullReferenceException
                throw ex.InnerException;
            }
        }
        public IQueryable<ResultT> CreateQuery<ResultT>(Expression expression)
        {
            try
            {
                //return new IndexedList<ResultT>(this, expression);
                return (IQueryable<ResultT>)Activator.CreateInstance(typeof(SupersonicList<>).MakeGenericType(typeof(ResultT)), this, expression);
            }
            catch (TargetInvocationException ex)
            {
                // ReSharper disable once PossibleNullReferenceException
                throw ex.InnerException;
            }
        }
        public object Execute(Expression expression)
        {
            return IndexedListQueryContext<ItemT>.Execute(SupersonicList, expression, false);
        }
        public ResultT Execute<ResultT>(Expression expression)
        {
            var isEnumerable = typeof(ResultT).Name == "IEnumerable`1";
            return (ResultT)IndexedListQueryContext<ItemT>.Execute(SupersonicList, expression, isEnumerable);
        }
        #endregion

        #region Properties
        public SupersonicList<ItemT> SupersonicList { get; }
        #endregion
    }
}