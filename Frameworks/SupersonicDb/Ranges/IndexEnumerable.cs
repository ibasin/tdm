﻿using System.Collections;
using System.Collections.Generic;

namespace Supersonic.Ranges
{
    public class IndexEnumerable<ItemT> : IEnumerable<ItemT>
    {
        #region Constructors
        public IndexEnumerable(Range range, List<ItemT> list)
        {
            Enumerator = new IndexEnumerator<ItemT>(range, list);
        }
        #endregion

        #region IEnumerable implemetation
        public IEnumerator<ItemT> GetEnumerator()
        {
            return Enumerator;
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion

        #region Properties
        protected Range Range { get; set; }
        protected List<ItemT> List { get; set; }
        protected IndexEnumerator<ItemT> Enumerator { get; set; }
        #endregion
    }
}