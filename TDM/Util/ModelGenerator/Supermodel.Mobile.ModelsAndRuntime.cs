//   DO NOT MAKE CHANGES TO THIS FILE. THEY MIGHT GET OVERWRITTEN!!!
//   Autogenrated by Supermodel.Mobile on 10/23/2019 7:27:10 PM
//
//   PROJECT SETUP INSTRUCTIONS
//
//   Please add the following references:
//
//   * For all (.NET server/desktop, iOS, Adnroid and UWP) projects
//     - Xamarin.Forms (NuGet package)
//     - PCLCrypto (Nuget package)
//     - Json.NET (NuGet package)
//     - Xam.Plugin.Media (NuGet package)
//
//   * For iOS and Android projects only   
//     - ModernHttpClient (NuGet package) -- this one is only for Mobile
//
//   * For .NET server/desktop, iOS, and Adnroid projects only
//     - System.Data.Sqlite (if using Visual Studio) or Mono.Data.Sqlite (if using Xamarin Studio). 
//       If programming for .NET server/desktop, add it as a Mono.Data.Sqlite.Portable NuGet
//     - System.Data
//     - System.ComponentModel.DataAnnotations
//     - System.Runtime.Serialization
//     - System.Web.Services
//   
//   * For .NET server/desktop project only
//      - System.Net.Http.Formatting
//      - System.Web
//
//   * For UWP project only
//      - Reference SQLite for Universal Windows Platform extension (if not there, follow intructions at:
//        https://developer.xamarin.com/guides/xamarin-forms/working-with/databases/#Adding_SQLite_database_support_to_Windows_Phone_8)
//      - Refernce Visual C++ 2015 Runtime for Universal Windows Platform Apps extension
//
//   * For iOS project only 
//      - Under iOS Build please add "--nolinkaway" to Additional mtouch arguments
//        For more info on this, pleasee see: https://spouliot.wordpress.com/2011/11/01/linker-vs-linked-away-exceptions/
//

//SQLite.Net directives
#if WINDOWS_PHONE && !USE_WP8_NATIVE_SQLITE
#define USE_CSHARP_SQLITE
#endif

#if NETFX_CORE
#define USE_NEW_REFLECTION_API
#endif

//Custom Renderers
#if WINDOWS_UWP
[assembly: Xamarin.Forms.Platform.UWP.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtEntry), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtEntryRenderer))]
[assembly: Xamarin.Forms.Platform.UWP.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtPicker), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtPickerRenderer))]
[assembly: Xamarin.Forms.Platform.UWP.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtDatePicker), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtDatePickerRenderer))]
#elif __MOBILE__
[assembly: Xamarin.Forms.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtEntry), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtEntryRenderer))]
[assembly: Xamarin.Forms.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtPicker), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtPickerRenderer))]
[assembly: Xamarin.Forms.ExportRenderer (typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtDatePicker), typeof (Supermodel.Mobile.XForms.UIComponents.CustomControls.ExtDatePickerRenderer))]
#endif

//Services
#if __MOBILE__
[assembly: Xamarin.Forms.Dependency(typeof(Supermodel.Mobile.Services.AudioService))]
#endif

#region Models
namespace Supermodel.Mobile.Runtime.Models
{
	using System;
	using Mobile.Models;
	using Attributes;
	using System.Collections.Generic;
	using System.ComponentModel;
	using System.Threading.Tasks;
	using DataContext.WebApi;
	
	#region TDMUserUpdatePasswordController
	[RestUrl("TDMUserUpdatePassword")]
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class TDMUserUpdatePassword : Model
	{
		#region Properties
		public String OldPassword { get; set; }
		public String NewPassword { get; set; }
		public String ConfirmPassword { get; set; }
		#endregion
	}
	#endregion
	
	#region ToDoListController
	[RestUrl("ToDoList")]
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class ToDoList : Model
	{
		#region Properties
		public DateTime CreatedOnUtc { get; set; } = new DateTime();
		public DateTime ModifiedOnUtc { get; set; } = new DateTime();
		public Int64 ListOwnerId { get; set; }
		public String Name { get; set; }
		public List<ToDoItem> ToDoItems { get; set; } = new List<ToDoItem>();
		#endregion
	}
	
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class SimpleSearch
	{
		#region Properties
		public String SearchTerm { get; set; }
		#endregion
	}
	#endregion
	
	#region DeleteAllBeforeController
	//Extension method for DeleteAllBefore command
	public static class DeleteAllBeforeCommandExt
	{
		public static async Task<DeleteAllBeforeOutput> DeleteAllBeforeAsync(this WebApiDataContext me, DeleteAllBeforeInput input)
		{
			return await me.ExecutePostAsync<DeleteAllBeforeInput, DeleteAllBeforeOutput>("DeleteAllBefore", input);
		}
	}
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class DeleteAllBeforeInput
	{
		#region Properties
		public DateTime OlderThanUtc { get; set; } = new DateTime();
		#endregion
	}
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class DeleteAllBeforeOutput
	{
		#region Properties
		public Int64 DeletedCount { get; set; }
		#endregion
	}
	#endregion
	
	#region Types models depend on and types that were specifically marked with [IncludeInMobileRuntime]
	// ReSharper disable once PartialTypeWithSinglePart
	public partial class ToDoItem
	{
		#region Properties
		public String Name { get; set; }
		public PriorityEnum? Priority { get; set; }
		public DateTime? DueOn { get; set; }
		public Boolean Completed { get; set; }
		public Int64 Id { get; set; }
		#endregion
	}
	public enum PriorityEnum
	{
		High = 0,
		Medium = 1,
		Low = 2
	}
	#endregion
}
#endregion

#region Supermodel.Mobile.Runtime
	
	#region Async
		
		#region AsyncHelper
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Async
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public static class AsyncHelper
		    {
		        public static void RunSync(Func<Task> task)
		        {
		            var oldContext = SynchronizationContext.Current;
		            var synch = new ExclusiveSynchronizationContext();
		            SynchronizationContext.SetSynchronizationContext(synch);
		            synch.Post(async _ =>
		            {
		                try
		                {
		                    await task();
		                }
		                catch (Exception e)
		                {
		                    synch.InnerException = e;
		                    throw;
		                }
		                finally
		                {
		                    synch.EndMessageLoop();
		                }
		            }, null);
		            synch.BeginMessageLoop();
		
		            SynchronizationContext.SetSynchronizationContext(oldContext);
		        }
		
		        public static T RunSync<T>(Func<Task<T>> task)
		        {
		            var oldContext = SynchronizationContext.Current;
		            var synch = new ExclusiveSynchronizationContext();
		            SynchronizationContext.SetSynchronizationContext(synch);
		            var ret = default(T);
		            synch.Post(async _ =>
		            {
		                try
		                {
		                    ret = await task();
		                }
		                catch (Exception e)
		                {
		                    synch.InnerException = e;
		                    throw;
		                }
		                finally
		                {
		                    synch.EndMessageLoop();
		                }
		            }, null);
		            synch.BeginMessageLoop();
		            SynchronizationContext.SetSynchronizationContext(oldContext);
		            return ret;
		        }
		
		        private class ExclusiveSynchronizationContext : SynchronizationContext
		        {
		            private bool _done;
		            public Exception InnerException { get; set; }
		            readonly AutoResetEvent _workItemsWaiting = new AutoResetEvent(false);
		            readonly Queue<Tuple<SendOrPostCallback, object>> _items = new Queue<Tuple<SendOrPostCallback, object>>();
		
		            public override void Send(SendOrPostCallback d, object state)
		            {
		                throw new NotSupportedException("We cannot send to our same thread");
		            }
		
		            public override void Post(SendOrPostCallback d, object state)
		            {
		                lock (_items)
		                {
		                    _items.Enqueue(Tuple.Create(d, state));
		                }
		                _workItemsWaiting.Set();
		            }
		
		            public void EndMessageLoop()
		            {
		                Post(_ => _done = true, null);
		            }
		
		            public void BeginMessageLoop()
		            {
		                while (!_done)
		                {
		                    Tuple<SendOrPostCallback, object> task = null;
		                    lock (_items)
		                    {
		                        if (_items.Count > 0) task = _items.Dequeue();
		                    }
		                    if (task != null)
		                    {
		                        task.Item1(task.Item2);
		                        // the method threw an exception
		                        if (InnerException != null) throw new AggregateException("AsyncHelper.Run method threw an exception.", InnerException);
		                    }
		                    else
		                    {
		                        _workItemsWaiting.WaitOne();
		                    }
		                }
		            }
		
		            public override SynchronizationContext CreateCopy()
		            {
		                return this;
		            }
		        }
		    }
		}
		#endregion
		
		#region AsyncLock
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Async
		{
		    using System;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public class AsyncLock
		    {
		        #region Embedded Types
		        public struct Releaser : IDisposable
		        {
		            #region Contructors
		            internal Releaser(AsyncLock toRelease) { _toRelease = toRelease; }
		            #endregion
		
		            #region IDisposable implemetation
		            public void Dispose()
		            {
		                if (_toRelease != null) _toRelease._semaphore.Release();
		            }
		            #endregion
		
		            #region Attributes
		            private readonly AsyncLock _toRelease;
		            #endregion
		        }
		        #endregion
		
		        #region Constructors
		        public AsyncLock()
		        {
		            _semaphore = new AsyncSemaphore(1);
		            _releaser = Task.FromResult(new Releaser(this));
		        }
		        #endregion
		
		        #region Methods
		        public Task<Releaser> LockAsync()
		        {
		            var wait = _semaphore.WaitAsync();
		            return wait.IsCompleted ?
		                _releaser :
		                wait.ContinueWith((_, state) => new Releaser((AsyncLock)state), this, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
		        }
		        #endregion
		
		        #region Attributes
		        private readonly AsyncSemaphore _semaphore;
		        private readonly Task<Releaser> _releaser;
		        #endregion
		    }
		}
		#endregion
		
		#region AsyncSemaphore
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Async
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Threading.Tasks;
		
		    public class AsyncSemaphore
		    {
		        #region Methods
		        public AsyncSemaphore(int initialCount)
		        {
		            if (initialCount < 0) throw new ArgumentOutOfRangeException("initialCount");
		            _currentCount = initialCount;
		        }
		        public Task WaitAsync()
		        {
		            lock (_waiters)
		            {
		                if (_currentCount > 0)
		                {
		                    --_currentCount;
		                    return _completed;
		                }
		                else
		                {
		                    var waiter = new TaskCompletionSource<bool>();
		                    _waiters.Enqueue(waiter);
		                    return waiter.Task;
		                }
		            }
		        }
		        public void Release()
		        {
		            TaskCompletionSource<bool> toRelease = null;
		            lock (_waiters)
		            {
		                if (_waiters.Count > 0) toRelease = _waiters.Dequeue();
		                else ++_currentCount;
		            }
		            if (toRelease != null) toRelease.SetResult(true);
		        }
		        #endregion
		
		        #region Attributes
		        private readonly static Task _completed = Task.FromResult(true);
		        private readonly Queue<TaskCompletionSource<bool>> _waiters = new Queue<TaskCompletionSource<bool>>();
		        private int _currentCount;
		        #endregion
		    }
		}
		#endregion
		
	#endregion
	
	#region Attributes
		
		#region DisabledAttribute
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Attributes
		{
		    using System;
		    
		    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
		    public class DisabledAttribute : Attribute { }
		}
		#endregion
		
		#region ForceRequiredLabelAttribute
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Attributes
		{
		    using System;
		    
		    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
		    public class ForceRequiredLabelAttribute : Attribute { }
		}
		#endregion
		
		#region NoRequiredLabelAttribute
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Attributes
		{
		    using System;
		
		    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
		    public class NoRequiredLabelAttribute : Attribute { }
		}
		#endregion
		
		#region RestUrlAttribute
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Attributes
		{
		    using System;
		
		    public class RestUrlAttribute : Attribute
		    {
		        public RestUrlAttribute(string url)
		        {
		            Url = url;
		        }
		        
		        public string Url { get; set; }
		    }
		}
		#endregion
		
		#region ScreenOrderAttribute
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Attributes
		{
		    using System;
		    
		    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
		    public class ScreenOrderAttribute : Attribute 
		    {
		        public ScreenOrderAttribute(int order)
		        {
		            Order = order;
		        }
		
		        public int Order { get; set; }
		    }
		}
		#endregion
		
	#endregion
	
	#region DataContext
		
		#region CachedWebApi
			
			#region CachedWebApiDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.CachedWebApi
			{
			    using System;
			    using System.Collections.Generic;
			    using System.Linq;
			    using Encryptor;
			    using System.Text;
			    using System.Threading.Tasks;
			    using Core;
			    using Sqlite;
			    using WebApi;
			    using Exceptions;
			    using Models;
			    using Repository;
			    using UnitOfWork;
			    using ReflectionMapper;
			    using SQLite;
			
			    public class CachedWebApiDataContext<WebApiDataContextT, SqlliteDataContextT> : DataContextBase, IWebApiAuthorizationContext, ICachedDataContext
			        where WebApiDataContextT : WebApiDataContext, new()
			        where SqlliteDataContextT : SqliteDataContext, new()
			    {
			        #region Constructors
			        public CachedWebApiDataContext()
			        {
			            CacheAgeToleranceInSeconds = 5 * 60; // 5 min
			        }
			        #endregion
			
			        #region Methods
			        public async Task PurgeCacheAsync(int? cacheExpirationAgeInSeconds = null, Type modelType = null)
			        {
			            var sqlliteContext = new SqlliteDataContextT();
			            await sqlliteContext.InitDbAsync();
			            var db = new SQLiteAsyncConnection(sqlliteContext.DatabaseFilePath);
			            var sb = new StringBuilder();
			            sb.AppendFormat(@"DELETE FROM {0}", sqlliteContext.DataTableName);
			            var first = true;
			                    
			            if (cacheExpirationAgeInSeconds != null)
			            {
			                // ReSharper disable once ConditionIsAlwaysTrueOrFalse
			                if (first)
			                {
			                    sb.Append(" WHERE ");
			                    first = false;
			                }
			                else
			                // ReSharper disable HeuristicUnreachableCode
			                {
			                    sb.Append(" AND ");
			                }
			                // ReSharper restore HeuristicUnreachableCode
			                sb.AppendFormat("BroughtFromMasterDbOnUtcTicks < {0}", DateTime.UtcNow.AddSeconds(cacheExpirationAgeInSeconds.Value).Ticks);
			            }
			
			            if (modelType != null)
			            {
			                if (first)
			                {
			                    sb.Append(" WHERE ");
			                    first = false;
			                }
			                else
			                {
			                    sb.Append(" AND ");
			                }
			                sb.AppendFormat("ModelTypeLogicalName == '{0}'", GetModelTypeLogicalName(modelType));
			            }
			
			            if (first)
			            {
			                sb.Append(" WHERE ");
			                // ReSharper disable once RedundantAssignment
			                first = false;
			            }
			            else
			            {
			                sb.Append(" AND ");
			            }
			            sb.AppendFormat("ModelTypeLogicalName != '{0}'", sqlliteContext.SchemaVersionModelType);
			
			            var commandText = sb.ToString();
			            await db.ExecuteAsync(commandText);
			        }
			        #endregion
			
			        #region ValidateLogin
			        public virtual async Task<LoginResult> ValidateLoginAsync<ModelT>() where ModelT : class, IModel
			        {
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                return await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.ValidateLoginAsync<ModelT>();
			            }
			        }
			        #endregion
			
			        #region DataContext Reads
			        public override async Task<ModelT> GetByIdOrDefaultAsync<ModelT>(long id)
			        {
			            //First check local cache
			            using (new UnitOfWork<SqlliteDataContextT>())
			            {
			                var cachedModel = await RepoFactory.Create<ModelT>().GetByIdOrDefaultAsync(id);
			                if (cachedModel != null)
			                {
			                    if (cachedModel.BroughtFromMasterDbOnUtc != null && cachedModel.BroughtFromMasterDbOnUtc.Value.AddSeconds(CacheAgeToleranceInSeconds) > DateTime.UtcNow)
			                    {
			                        ManagedModels.Add(new ManagedModel(cachedModel));
			                        return cachedModel;
			                    }
			                    else
			                    {
			                        cachedModel.Delete();
			                    }
			                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			                }
			                else
			                {
			                    //Mark done for performance reasons
			                    UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.MakeCompletedAndFinalized();
			                }
			            }
			
			            //if we get here, we need to get the data from web api service
			            ModelT masterModel;
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			                
			                masterModel = await RepoFactory.Create<ModelT>().GetByIdOrDefaultAsync(id);
			            }
			
			            //Now save master model to cache and add it to managed models if we have something to save
			            if (masterModel != null)
			            {
			                using (new UnitOfWork<SqlliteDataContextT>())
			                {
			                    ManagedModels.Add(new ManagedModel(masterModel)); 
			                    UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.AddOrUpdate(masterModel);
			                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			                }
			                
			            }
			
			            return masterModel;
			        }
			        public override async Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            //first delete local cache, we will be refreshing it with new data
			            var sqlliteContext = new SqlliteDataContextT();
			            await sqlliteContext.InitDbAsync();
			            var db = new SQLiteAsyncConnection(sqlliteContext.DatabaseFilePath);
			            var commandText = $@"DELETE FROM {sqlliteContext.DataTableName} WHERE ModelTypeLogicalName == '{GetModelTypeLogicalName(typeof(ModelT))}'";
			            await db.ExecuteAsync(commandText);
			            
			            //get the data from web api service
			            List<ModelT> masterModels;
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                masterModels = await RepoFactory.Create<ModelT>().GetAllAsync(skip, take);
			            }
			
			            //Now save master models to cache if we have something to save
			            if (masterModels.Any())
			            {
			                using (new UnitOfWork<SqlliteDataContextT>())
			                {
			                    foreach (var masterModel in masterModels)
			                    {
			                        ManagedModels.Add(new ManagedModel(masterModel)); 
			                        UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.AddOrUpdate(masterModel);
			                    }
			                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			                }
			            }
			
			            return masterModels;
			        }
			        public override async Task<long> GetCountAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                return await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.GetCountAllAsync<ModelT>(skip, take);
			            }
			        }
			        #endregion
			
			        #region DataContext Queries
			        public override async Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null)
			        {
			            //if we get here, we need to get the data from web api service
			            List<ModelT> masterModels;
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                masterModels = await RepoFactory.Create<ModelT>().GetWhereAsync(searchBy, sortBy, skip, take);
			            }
			
			            //Now save master models to cache if we have something to save
			            if (masterModels.Any())
			            {
			                using (new UnitOfWork<SqlliteDataContextT>())
			                {
			                    foreach (var masterModel in masterModels)
			                    {
			                        ManagedModels.Add(new ManagedModel(masterModel));
			                        UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.AddOrUpdate(masterModel);
			                    }
			                    await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			                }
			            }
			
			            return masterModels;
			        }
			        public override async Task<long> GetCountWhereAsync<ModelT>(object searchBy, int? skip = null, int? take = null)
			        {
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                return await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.GetCountWhereAsync<ModelT>(searchBy, skip, take);
			            }
			        }
			        #endregion
			
			        #region DataContext Save Changes
			        public override async Task SaveChangesInternalAsync(List<PendingAction> pendingActions)
			        {
			            //First we delete all objects in cache that are about to be updated -- we do this for transcational integrity
			            var actionsToLoopThrough = IsReadOnly ? PendingActions.Where(x => x.IsReadOnlyAction) : PendingActions;
			            using (new UnitOfWork<SqlliteDataContextT>())
			            {
			                // ReSharper disable once PossibleMultipleEnumeration
			                foreach (var pendingAction in actionsToLoopThrough)
			                {
			                    switch (pendingAction.Operation)
			                    {
			                        case PendingAction.OperationEnum.AddWithExistingId:
			                        case PendingAction.OperationEnum.Update:
			                        case PendingAction.OperationEnum.Delete:
			                        case PendingAction.OperationEnum.AddOrUpdate:
			                        case PendingAction.OperationEnum.DelayedGetById:
			                        case PendingAction.OperationEnum.DelayedGetByIdOrDefault:
			                        {
			                            UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("Delete", new[] { pendingAction.ModelType }, pendingAction.ModelId);
			                            break;
			                        }
			                        case PendingAction.OperationEnum.DelayedGetAll:
			                        {
			                            var sqlliteContext = new SqlliteDataContextT();
			                            await sqlliteContext.InitDbAsync();
			                            var db = new SQLiteAsyncConnection(sqlliteContext.DatabaseFilePath);
			                            var commandText = $@"DELETE FROM {sqlliteContext.DataTableName} WHERE ModelTypeLogicalName == '{GetModelTypeLogicalName(pendingAction.ModelType)}'";
			                            await db.ExecuteAsync(commandText);
			                            break;
			                        }
			                        case PendingAction.OperationEnum.GenerateIdAndAdd:
			                        case PendingAction.OperationEnum.DelayedGetWhere:
			                        case PendingAction.OperationEnum.DelayedGetCountAll:
			                        case PendingAction.OperationEnum.DelayedGetCountWhere:
			                        {
			                            //for these we do nothing
			                            break;
			                        }
			                        default:
			                        {
			                            throw new SupermodelException("Unsupported Operation");
			                        }
			                    }
			                }
			                await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			            }
			
			            //Then we attempt to save to web api service
			            using (new UnitOfWork<WebApiDataContextT>())
			            {
			                if (IsReadOnly) UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeReadOnly();
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.AuthHeader = AuthHeader;
			
			                await UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.SaveChangesInternalAsync(PendingActions);
			                UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext.MakeCompletedAndFinalized();
			            }
			
			            //Them if we were successful, we update local db
			            using (new UnitOfWork<SqlliteDataContextT>())
			            {
			                // ReSharper disable once PossibleMultipleEnumeration
			                foreach (var pendingAction in actionsToLoopThrough)
			                {
			                    switch (pendingAction.Operation)
			                    {
			                        case PendingAction.OperationEnum.AddWithExistingId:
			                        case PendingAction.OperationEnum.GenerateIdAndAdd:
			                        case PendingAction.OperationEnum.Update:
			                        case PendingAction.OperationEnum.AddOrUpdate:
			                        {
			                            UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("AddOrUpdate", new[] { pendingAction.ModelType }, pendingAction.Model);
			                            break;
			                        }
			                        case PendingAction.OperationEnum.Delete: //we need to delete again becasue we could have read data with delayed read
			                        {
			                            UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("Delete", new[] { pendingAction.ModelType }, pendingAction.ModelId);
			                            break;
			                        }
			                        case PendingAction.OperationEnum.DelayedGetById:
			                        case PendingAction.OperationEnum.DelayedGetByIdOrDefault:
			                        {
			                            var model = (IModel)pendingAction.DelayedValue.GetValue();
			                            if (model != null)
			                            {
			                                UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("AddOrUpdate", new[] { pendingAction.ModelType }, model);
			                            }
			                            break;
			                        }
			                        case PendingAction.OperationEnum.DelayedGetAll:
			                        case PendingAction.OperationEnum.DelayedGetWhere:
			                        {
			                            var models = (IEnumerable<IModel>)pendingAction.DelayedValue.GetValue();
			                            foreach (var model in models)
			                            {
			                                UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.ExecuteGenericMethod("AddOrUpdate", new[] { pendingAction.ModelType }, model);
			                            }
			                            break;
			                        }
			                        case PendingAction.OperationEnum.DelayedGetCountAll:
			                        case PendingAction.OperationEnum.DelayedGetCountWhere:
			                        {
			                            //for these we do nothing
			                            break;
			                        }
			                        default:
			                        {
			                            throw new SupermodelException("Unsupported Operation");
			                        }
			                    }
			                }
			                await UnitOfWorkContext<SqlliteDataContextT>.CurrentDataContext.FinalSaveChangesAsync();
			            }
			        }
			        #endregion
			
			        #region Configuration Properties
			        public AuthHeader AuthHeader { get; set; }
			        public int CacheAgeToleranceInSeconds { get; set; }
			        #endregion
			    }
			}
			#endregion
			
		#endregion
		
		#region Core
			
			#region DataContextBase
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System;
			    using System.Collections.Generic;
			    using System.ComponentModel.DataAnnotations;
			    using System.Linq;
			    using System.Threading.Tasks;
			    using Exceptions;
			    using Models;
			    using Repository;
			    using Attributes;
			    using ReflectionMapper;
			    using System.Reflection; //only needed for UWP
			    
			    public abstract class DataContextBase : IQuerableReadableDataContext, IWriteableDataContext
			    {
			        #region Contructors
			        protected DataContextBase()
			        {
			            CommitOnDispose = true;
			            IsReadOnly = false;
			            IsCompletedAndFinalized = false;
			            
			            ManagedModels = new List<ManagedModel>();
			            PendingActions = new List<PendingAction>();
			
			            CustomValues = new Dictionary<string, object>();
			        }
			        #endregion
			
			        #region Methods
			        public static string GetModelTypeLogicalName<ModelT>() where ModelT : class, IModel
			        {
			            return GetModelTypeLogicalName(typeof(ModelT));
			        }
			        public static string GetModelTypeLogicalName(Type type)
			        {
			            if (!typeof(IModel).IsAssignableFrom(type)) throw new SupermodelException("GetModelTypeLogicalName can only be called for types that implement IModel");
			            //var restUrlAttribute = type.GetCustomAttributes(typeof(RestUrlAttribute), true).FirstOrDefault() as RestUrlAttribute;
			            var restUrlAttribute = type.GetTypeInfo().GetCustomAttributes(typeof(RestUrlAttribute), true).FirstOrDefault() as RestUrlAttribute;
			            return restUrlAttribute == null ? type.Name : restUrlAttribute.Url;
			        }
			        public void DetectAllUpdates()
			        {
			            //remove all updates that are already in PendingActions
			            PendingActions.RemoveAll(x => x.Operation == PendingAction.OperationEnum.Update);
			
			            DetectNewUpdates();
			        }
			        public void DetectNewUpdates()
			        {
			            //detect new updates and put them in PendingActions
			            foreach (var managedModel in ManagedModels.Where(managedModel => managedModel.NeedsUpdating()))
			            {
			                if (!PendingActions.Any(x => x.Operation == PendingAction.OperationEnum.Update && x.ModelType == managedModel.Model.GetType() && x.ModelId == managedModel.Model.Id && x.OriginalModelId == managedModel.OriginalModelId ))
			                {
			                    PendingActions.Add(new PendingAction
			                    {
			                        Operation = PendingAction.OperationEnum.Update,
			                        ModelType = managedModel.Model.GetType(),
			                        ModelId = managedModel.Model.Id,
			                        OriginalModelId = managedModel.OriginalModelId,
			                        Model = managedModel.Model,
			                        DelayedValue = null,
			                        SearchBy = null,
			                        Skip = null,
			                        Take = null,
			                        SortBy = null
			                    }.Validate());
			                }
			            }
			        }
			        #endregion
			
			        #region DataContext Reads
			        public virtual async Task<ModelT> GetByIdAsync<ModelT>(long id) where ModelT : class, IModel, new()
			        {
			            var model = await GetByIdOrDefaultAsync<ModelT>(id);
			            if (model == null) throw new SupermodelException("GetByIdAsync(id): no object exists with id = " + id);
			            return model;
			        }
			        public abstract Task<ModelT> GetByIdOrDefaultAsync<ModelT>(long id) where ModelT : class, IModel, new();
			        public abstract Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        public abstract Task<long> GetCountAllAsync<ModelT>(int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        #endregion
			
			        #region DataContext Delayed Reads
			        public void DelayedGetById<ModelT>(out DelayedModel<ModelT> model, long id) where ModelT : class, IModel, new()
			        {
			            model = new DelayedModel<ModelT>();
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetById,
			                ModelType = typeof(ModelT),
			                ModelId = id,
			                OriginalModelId = 0,
			                Model = null,
			                DelayedValue = model,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        public void DelayedGetByIdOrDefault<ModelT>(out DelayedModel<ModelT> model, long id) where ModelT : class, IModel, new()
			        {
			            model = new DelayedModel<ModelT>();
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetByIdOrDefault,
			                ModelType = typeof(ModelT),
			                ModelId = id,
			                OriginalModelId = 0,
			                Model = null,
			                DelayedValue = model,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        public void DelayedGetAll<ModelT>(out DelayedModels<ModelT> models) where ModelT : class, IModel, new()
			        {
			            models = new DelayedModels<ModelT>();
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetAll,
			                ModelType = typeof(ModelT),
			                ModelId = 0,
			                OriginalModelId = 0,
			                Model = null,
			                DelayedValue = models,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        public void DelayedGetCountAll<ModelT>(out DelayedCount count) where ModelT : class, IModel, new()
			        {
			            count = new DelayedCount();
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetCountAll,
			                ModelType = typeof(ModelT),
			                ModelId = 0,
			                OriginalModelId = 0,
			                Model = null,
			                DelayedValue = count,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        #endregion
			
			        #region DataContext Queries
			        public abstract Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        public abstract Task<long> GetCountWhereAsync<ModelT>(object searchBy, int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        #endregion
			
			        #region DataContext Delayed Queries
			        public void DelayedGetWhere<ModelT>(out DelayedModels<ModelT> models, object searchBy, string sortBy = null, int? skip = null, int? take = null) where ModelT : class, IModel, new()
			        {
			            models = new DelayedModels<ModelT>();
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetWhere,
			                ModelType = typeof(ModelT),
			                ModelId = 0,
			                OriginalModelId = 0,
			                Model = null,
			                DelayedValue = models,
			                SearchBy = searchBy,
			                Skip = skip,
			                Take = take,
			                SortBy = sortBy
			            }.Validate());
			        }
			        public void DelayedGetCountWhere<ModelT>(out DelayedCount count, object searchBy) where ModelT : class, IModel, new()
			        {
			            count = new DelayedCount();
			
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.DelayedGetCountWhere,
			                ModelType = typeof(ModelT),
			                ModelId = 0,
			                OriginalModelId = 0,
			                Model = null,
			                DelayedValue = count,
			                SearchBy = searchBy,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        #endregion
			
			        #region DataContext Writes
			        public void Add<ModelT>(ModelT model) where ModelT : class, IModel, new()
			        {
			            var operation = model.Id == 0 ? PendingAction.OperationEnum.GenerateIdAndAdd : PendingAction.OperationEnum.AddWithExistingId;
			            PendingActions.Add(new PendingAction
			            {
			                Operation = operation,
			                ModelType = typeof(ModelT),
			                ModelId = model.Id,
			                OriginalModelId = 0,
			                Model = model,
			                DelayedValue = null,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        public void Delete<ModelT>(ModelT model) where ModelT : class, IModel, new()
			        {
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.Delete,
			                ModelType = typeof (ModelT),
			                ModelId = model.Id,
			                OriginalModelId = model.Id,
			                Model = model,
			                DelayedValue = null,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        public void ForceUpdate<ModelT>(ModelT model) where ModelT : class, IModel, new()
			        {
			            var managedModel = ManagedModels.SingleOrDefault(x => x.Model == model);
			            
			            if (managedModel == null) ManagedModels.Add(new ManagedModel(model){ ForceUpdate = true });
			            else managedModel.ForceUpdate = true;
			        }
			        #endregion
			
			        #region IDisposable implemetation
			        public void Dispose()
			        {
			            if (IsCompletedAndFinalized) return;
			
			            if (IsReadOnly || !CommitOnDispose) PendingActions.RemoveAll(x => !x.IsReadOnlyAction);
			            else DetectNewUpdates();
			
			            //OptimizePendingActions();
			
			            // ReSharper disable once SimplifyLinqExpression
			            if (!PendingActions.Any()) return;
			
			            throw new SupermodelException("Must run FinalSaveChangesAsync or Finalize otherwise before you are done with the unit of work");
			        }
			        #endregion
			        
			        #region DataContext Configuration
			        public bool CommitOnDispose { get; set; }
			        public bool IsReadOnly { get; protected set; }
			        public void MakeReadOnly()
			        {
			            IsReadOnly = true;
			        }
			        public bool IsCompletedAndFinalized { get; protected set; }
			        public void MakeCompletedAndFinalized()
			        {
			            IsCompletedAndFinalized = true;
			        }
			        #endregion
			
			        #region Context RepoFactory
			        public virtual IDataRepo<ModelT> CreateRepo<ModelT>() where ModelT : class, IModel, new()
			        {
			            if (CustomRepoFactoryList != null)
			            {
			                foreach (var customFactory in CustomRepoFactoryList)
			                {
			                    var repo = customFactory.CreateRepo<ModelT>();
			                    if (repo != null) return repo;
			                }
			            }
			            return new DataRepo<ModelT>();
			        }
			        protected List<IRepoFactory> CustomRepoFactoryList { get { return null; } }
			        #endregion
			
			        #region DataContext Save Changes
			        public async Task FinalSaveChangesAsync()
			        {
			            try
			            {
			                await SaveChangesAsync(true);
			            }
			            finally
			            {
			                MakeCompletedAndFinalized();
			            }
			        }
			        public async Task SaveChangesAsync()
			        {
			            await SaveChangesAsync(false);
			        }
			        protected virtual async Task SaveChangesAsync(bool isFinal)
			        {
			            if (IsCompletedAndFinalized) return;
			
			            if (IsReadOnly || !CommitOnDispose) PendingActions.RemoveAll(x => !x.IsReadOnlyAction);
			            else DetectNewUpdates();
			
			            // ReSharper disable once SimplifyLinqExpression
			            if (!PendingActions.Any(x => !x.Disabled)) return;
			
			            //Run BeforeSave for all Models about to be saved
			            foreach (var pendingAction in PendingActions.Where(x => !x.Disabled))
			            {
			                if (pendingAction.Operation == PendingAction.OperationEnum.AddOrUpdate || 
			                    pendingAction.Operation == PendingAction.OperationEnum.AddWithExistingId ||
			                    pendingAction.Operation == PendingAction.OperationEnum.Update ||
			                    pendingAction.Operation == PendingAction.OperationEnum.Delete ||
			                    pendingAction.Operation == PendingAction.OperationEnum.GenerateIdAndAdd) pendingAction.Model.BeforeSave(pendingAction.Operation);
			            }
			
			            OptimizePendingActions();          
			            ValidatePendingActions();
			
			            await SaveChangesInternalAsync(PendingActions);
			
			            if (!isFinal)
			            {
			                //Update all managed models with the new hash and clear PendingAction, so that we can save multiple times in the same unit of work
			                foreach (var managedModel in ManagedModels) managedModel.UpdateHash();
			                PendingActions.Clear();
			            }
			        }
			        public abstract Task SaveChangesInternalAsync(List<PendingAction> pendingActions);
			        #endregion
			
			        #region Private Helpers
			        protected void PrepareForThrowingException()
			        {
			            foreach (var pendingAction in PendingActions.Where(x => x.Operation == PendingAction.OperationEnum.GenerateIdAndAdd)) pendingAction.Model.Id = 0;
			            MakeCompletedAndFinalized();
			        }
			        protected void ThrowSupermodelValidationException(SupermodelDataContextValidationException.ValidationError validationError)
			        {
			            PrepareForThrowingException();
			            throw new SupermodelDataContextValidationException(validationError);
			        }
			        protected void ThrowSupermodelValidationException(List<SupermodelDataContextValidationException.ValidationError> validationErrors)
			        {
			            PrepareForThrowingException();
			            throw new SupermodelDataContextValidationException(validationErrors);
			        }
			        public virtual void ValidatePendingActions()
			        {
			            //Validate that all disbaled PendingActions have been cleared
			            if (PendingActions.Any(x => x.Disabled)) throw new SupermodelException("ValidatePendingActions: should not have any Disbaled pending actions");
			
			            //Validate that every Pending Action is valid
			            if (PendingActions.Any(x => !x.IsValid())) throw new SupermodelException("One of the PendingActions is invalid. This should never happen");
			            
			            //Validate no duplicate addorupdate, updates, and deletes. For every Id and type there should only be one Update or Delete or AddOrUpdate
			            if (PendingActions.Where(x => x.Operation == PendingAction.OperationEnum.Update || x.Operation == PendingAction.OperationEnum.Delete || x.Operation == PendingAction.OperationEnum.AddOrUpdate).GroupBy(x => new { x.ModelType, x.ModelId }).Any(x => x.Count() > 1))
			            {
			                throw new SupermodelException("Duplicate addorupdates/updates/deletes in PendingActions");
			            }
			
			            //Validate objects themselves
			            var validationErrors = new List<SupermodelDataContextValidationException.ValidationError>();
			            foreach (var pendingAction in PendingActions.Where(x => x.Model != null))
			            {
			                var vr = new ValidationResultList();
			                if (!Validator.TryValidateObject(pendingAction.Model, new ValidationContext(pendingAction.Model), vr))
			                {
			                    var validationError = new SupermodelDataContextValidationException.ValidationError(vr, pendingAction, "There are some Model Validation Errors");
			                    validationErrors.Add(validationError);
			                }
			            }
			            if (validationErrors.Any()) throw new SupermodelDataContextValidationException(validationErrors);
			        }
			        protected virtual void OptimizePendingActions()
			        {
			            //Run the optimization, marking redundand operations Disabled
			            for (var i = PendingActions.Count - 1; i >= 0; i--)
			            {
			                var pendingAction = PendingActions[i];
			                if (pendingAction.Disabled) continue;
			                switch (pendingAction.Operation)
			                {
			                    case PendingAction.OperationEnum.Delete:
			                    {
			                        for (var j = i - 1; j >= 0; j--)
			                        {
			                            var pendingAction2 = PendingActions[j];
			                            if (pendingAction2.Disabled || pendingAction.ModelId != pendingAction2.ModelId || pendingAction.ModelType != pendingAction2.ModelType) continue;
			                            switch (pendingAction2.Operation)
			                            {
			                                case PendingAction.OperationEnum.Delete: pendingAction.Disabled = true; break;
			                                case PendingAction.OperationEnum.AddOrUpdate: pendingAction2.Disabled = true; break;
			                                case PendingAction.OperationEnum.Update: pendingAction2.Disabled = true; break;
			                                //do nothing for all other operations
			                            }
			                        }
			                        break;
			                    }
			                    case PendingAction.OperationEnum.AddOrUpdate:
			                    {
			                        for (var j = i - 1; j >= 0; j--)
			                        {
			                            var pendingAction2 = PendingActions[j];
			                            if (pendingAction2.Disabled || pendingAction.ModelId != pendingAction2.ModelId || pendingAction.ModelType != pendingAction2.ModelType) continue;
			                            switch (pendingAction2.Operation)
			                            {
			                                case PendingAction.OperationEnum.Delete: pendingAction.Disabled = true; break;
			                                case PendingAction.OperationEnum.AddOrUpdate: pendingAction2.Disabled = true; break;
			                                case PendingAction.OperationEnum.Update: pendingAction2.Disabled = true; break;
			                                //do nothing for all other operations
			                            }
			                        }
			                        break;
			                    }
			                    case PendingAction.OperationEnum.Update:
			                    {
			                        for (var j = i - 1; j >= 0; j--)
			                        {
			                            var pendingAction2 = PendingActions[j];
			                            if (pendingAction2.Disabled || pendingAction.ModelId != pendingAction2.ModelId || pendingAction.ModelType != pendingAction2.ModelType) continue;
			                            switch (pendingAction2.Operation)
			                            {
			                                case PendingAction.OperationEnum.Delete: pendingAction.Disabled = true; break;
			                                case PendingAction.OperationEnum.AddOrUpdate: pendingAction2.Disabled = true; pendingAction.Operation = PendingAction.OperationEnum.AddOrUpdate; break;
			                                case PendingAction.OperationEnum.Update: pendingAction2.Disabled = true; break;
			                                //do nothing for all other operations
			                            }
			                        }
			                        break;
			                    }
			                    //do nothing for all other operations
			                }
			            }
			            
			            //Remove disabled operations
			            PendingActions.RemoveAll(x => x.Disabled);
			        }
			        #endregion
			
			        #region Properties & Constants
			        protected List<ManagedModel> ManagedModels { get; set; }
			        protected List<PendingAction> PendingActions { get; set; }
			        public Dictionary<string, object> CustomValues { get; private set; } 
			        #endregion
			    }
			}
			#endregion
			
			#region DelayedValues
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System.Collections.Generic;
			    using Models;
			    
			    public abstract class DelayedValue
			    {
			        public abstract void SetValue(object value);
			        public abstract object GetValue();
			    }
			    
			    public class DelayedModel<ModelT> : DelayedValue where ModelT : class, IModel, new() 
			    {
			        #region Overrides
			        public override void SetValue(object value)
			        {
			            Value = (ModelT)value;
			        }
			
			        public override object GetValue()
			        {
			            return Value;
			        }
			        #endregion
			
			        #region Properties
			        public ModelT Value { get; set; }
			        #endregion
			    }
			
			    public class DelayedModels<ModelT> : DelayedValue where ModelT : class, IModel, new()
			    {
			        #region Overrides
			        public override void SetValue(object value)
			        {
			            Values = (List<ModelT>)value;
			        }
			        public override object GetValue()
			        {
			            return Values;
			        }
			        #endregion
			
			        #region Properties
			        public List<ModelT> Values { get; set; }
			        #endregion
			    }
			
			    public class DelayedCount : DelayedValue
			    {
			        #region Overrides
			        public override void SetValue(object value)
			        {
			            Value = (long?) value;
			        }
			        public override object GetValue()
			        {
			            return Value;
			        }
			        #endregion
			
			        #region Properties
			        public long? Value { get; set; }
			        #endregion
			    }
			}
			#endregion
			
			#region ICachedDataContext
			 // ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System;
			    using System.Threading.Tasks;
			    
			    public interface ICachedDataContext
			    {
			        int CacheAgeToleranceInSeconds { get; set; }
			        Task PurgeCacheAsync(int? cacheExpirationAgeInSeconds = null, Type modelType = null);
			    }
			}
			#endregion
			
			#region IDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System;
			    using Models;
			    using Repository;
			    using System.Collections.Generic;
			    
			    public interface IDataContext : IDisposable
			    {
			        #region Configuration
			        bool CommitOnDispose { get; set; }
			        bool IsReadOnly { get; }
			        void MakeReadOnly();
			        #endregion
			
			        #region Context RepoFactory
			        IDataRepo<ModelT> CreateRepo<ModelT>() where ModelT : class, IModel, new();
			        #endregion
			
			        #region CustomValues
			        Dictionary<string, object> CustomValues { get; } 
			        #endregion
			    }
			}
			
			#endregion
			
			#region IQuerableReadableDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System.Collections.Generic;
			    using System.Threading.Tasks;
			    using Models;
			    
			    public interface IQuerableReadableDataContext : IReadableDataContext
			    {
			        #region Queries
			        Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        Task<long> GetCountWhereAsync<ModelT>(object searchBy, int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        #endregion
			
			        #region Delayed Queries
			        void DelayedGetWhere<ModelT>(out DelayedModels<ModelT> models, object searchBy, string sortBy = null, int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        void DelayedGetCountWhere<ModelT>(out DelayedCount count, object searchBy) where ModelT : class, IModel, new();
			        #endregion
			    }
			}
			#endregion
			
			#region IReadableDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System.Collections.Generic;
			    using System.Threading.Tasks;
			    using Models;
			    
			    public interface IReadableDataContext : IDataContext
			    {
			        #region Reads
			        Task<ModelT> GetByIdAsync<ModelT>(long id) where ModelT : class, IModel, new();
			        Task<ModelT> GetByIdOrDefaultAsync<ModelT>(long id) where ModelT : class, IModel, new();
			        Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        Task<long> GetCountAllAsync<ModelT>(int? skip = null, int? take = null) where ModelT : class, IModel, new();
			        #endregion
			
			        #region Batch Reads
			        void DelayedGetById<ModelT>(out DelayedModel<ModelT> model, long id) where ModelT : class, IModel, new();
			        void DelayedGetByIdOrDefault<ModelT>(out DelayedModel<ModelT> model, long id) where ModelT : class, IModel, new();
			        void DelayedGetAll<ModelT>(out DelayedModels<ModelT> models) where ModelT : class, IModel, new();
			        void DelayedGetCountAll<ModelT>(out DelayedCount count) where ModelT : class, IModel, new();
			        #endregion
			    }
			}
			#endregion
			
			#region IWebApiAuthorizationContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using Encryptor;
			    using System.Threading.Tasks;
			    using Models;
			    using WebApi;
			
			    public interface IWebApiAuthorizationContext
			    {
			        AuthHeader AuthHeader { get; set; }
			        Task<LoginResult> ValidateLoginAsync<ModelT>() where ModelT : class, IModel;
			    }
			}
			#endregion
			
			#region IWriteableDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System.Threading.Tasks;
			    using Models;
			    
			    public interface IWriteableDataContext : IDataContext
			    {
			        #region Writes
			        void Add<ModelT>(ModelT model) where ModelT : class, IModel, new();
			        void Delete<ModelT>(ModelT model) where ModelT : class, IModel, new();
			        void ForceUpdate<ModelT>(ModelT model) where ModelT : class, IModel, new();
			        void DetectAllUpdates();
			        #endregion
			
			        #region Save Changes
			        Task SaveChangesAsync();
			        Task FinalSaveChangesAsync();
			        #endregion
			    }
			}
			#endregion
			
			#region ManagedModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using Models;
			    using Encryptor;
			    using Newtonsoft.Json;
			
			    public class ManagedModel
			    {
			        #region Constructors
			        public ManagedModel(IModel originalModel)
			        {
			            Model = originalModel;
			            OriginalModelId = originalModel.Id;
			            UpdateHash();
			        }
			        #endregion
			
			        #region Methods
			        public void UpdateHash()
			        {
			            OriginalModelHash = JsonConvert.SerializeObject(Model).GetMD5Hash();
			        }
			        public bool NeedsUpdating()
			        {
			            return ForceUpdate || JsonConvert.SerializeObject(Model).GetMD5Hash() != OriginalModelHash;
			        }
			        #endregion
			
			        #region Properties
			        public IModel Model { get; set; }
			        public long OriginalModelId { get; set; }
			        protected string OriginalModelHash { get; set; }
			        public bool ForceUpdate { get; set; }
			        #endregion
			    }
			}
			#endregion
			
			#region PendingAction
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Core
			{
			    using System;
			    using System.Collections.Generic;
			    using System.Net.Http;
			    using System.Text;
			    using Newtonsoft.Json;
			    using Sqlite;
			    using WebApi;
			    using Exceptions;
			    using Models;
			    using ReflectionMapper;
			    using System.Linq;
			
			    
			    public class PendingAction 
			    {
			        #region Embedded Enums
			        public enum OperationEnum { AddWithExistingId, GenerateIdAndAdd, Update, Delete, DelayedGetById, DelayedGetByIdOrDefault, DelayedGetAll, DelayedGetCountAll, DelayedGetWhere, DelayedGetCountWhere, AddOrUpdate }
			        public enum OperationSqlType { SingleResultQuery, ListResultQuery, ScalarResultQuery, NoQueryResult }
			        #endregion
			
			        #region Constructors
			        public PendingAction()
			        {
			            Disabled = false;
			        }
			        public PendingAction(PendingAction other) //copy constructor
			        {
			            Disabled = other.Disabled;
			            Operation = other.Operation;
			            ModelType = other.ModelType;
			            ModelId = other.ModelId;
			            OriginalModelId = other.OriginalModelId;
			            Model = other.Model;
			            DelayedValue = other.DelayedValue;
			            SearchBy = other.SearchBy;
			            Skip = other.Skip;
			            Take = other.Take;
			            SortBy = other.SortBy;
			        }
			        #endregion
			
			        #region Methods
			        public PendingAction Validate()
			        {
			            if (!IsValid()) throw new SupermodelException("Invalid PendingAction");
			            return this;
			        }
			        public bool IsValid()
			        {
			            if (ModelType == null) return false;
			
			            switch (Operation)
			            {
						    case OperationEnum.GenerateIdAndAdd:
						    {
						        if (ModelId != 0) return false;
						        if (Model == null) return false;
						        break;
						    }
						    case OperationEnum.AddWithExistingId:
						    case OperationEnum.AddOrUpdate:
						    {
						        if (ModelId == 0) return false;
						        if (Model == null) return false;
						        break;
						    }
						    case OperationEnum.Delete:
						    {
						        if (ModelId == 0) return false;
						        if (OriginalModelId != ModelId) return false;
						        if (Model == null) return false;
						        break;
						    }
			                case OperationEnum.Update:
			                {
			                    if (ModelId == 0) return false;
			                    if (OriginalModelId == 0) return false;
			                    if (Model == null) return false;
			                    break;
			                }
			                case OperationEnum.DelayedGetById:
			                case OperationEnum.DelayedGetByIdOrDefault:
			                {
			                    if (ModelId == 0) return false;
			                    if (DelayedValue == null) return false;
			                    break;
			                }
			                case OperationEnum.DelayedGetAll:
			                case OperationEnum.DelayedGetWhere:
			                case OperationEnum.DelayedGetCountAll:
			                case OperationEnum.DelayedGetCountWhere:
			                {
			                    if (DelayedValue == null) return false;
			                    break;
			                }
			                default:
			                {
			                    throw new SupermodelException("Unsupported Operation");
			                }
			            }
			            return true;
			        }
			        public bool IsReadOnlyAction 
			        {
			            get
			            {
			                switch (Operation)
			                {
			                    case OperationEnum.AddWithExistingId:
			                    case OperationEnum.GenerateIdAndAdd:
			                    case OperationEnum.Update:
			                    case OperationEnum.Delete:
			                    case OperationEnum.AddOrUpdate:
			                        return false;
			
			                    case OperationEnum.DelayedGetById:
			                    case OperationEnum.DelayedGetByIdOrDefault:
			                    case OperationEnum.DelayedGetAll:
			                    case OperationEnum.DelayedGetWhere:
			                    case OperationEnum.DelayedGetCountAll:
			                    case OperationEnum.DelayedGetCountWhere:
			                        return true;
			
			                    default:
			                        throw new SupermodelException("Unsupported Operation");
			                }
			            }
			        }
			        public OperationSqlType SqlType
			        {
			            get
			            {
			                switch (Operation)
			                {
			                    case OperationEnum.AddWithExistingId:
			                    case OperationEnum.GenerateIdAndAdd:
			                    case OperationEnum.Update:
			                    case OperationEnum.Delete:
			                    case OperationEnum.AddOrUpdate:
			                        return OperationSqlType.NoQueryResult;
			
			                    case OperationEnum.DelayedGetById:
			                    case OperationEnum.DelayedGetByIdOrDefault:
			                        return OperationSqlType.SingleResultQuery;
			
			                    case OperationEnum.DelayedGetAll:
			                    case OperationEnum.DelayedGetWhere:
			                        return OperationSqlType.ListResultQuery;
			
			                    case OperationEnum.DelayedGetCountAll:
			                    case OperationEnum.DelayedGetCountWhere:
			                        return OperationSqlType.ScalarResultQuery;
			
			                    default:
			                        throw new SupermodelException("Unsupported Operation");
			                }
			            }
			        }
			        #endregion
			
			        #region WebApi Methods
			        public HttpRequestMessage GenerateHttpRequest(string baseUrl, IQueryStringProvider queryStringProvider)
			        {
			            switch (Operation)
			            {
			                case OperationEnum.AddWithExistingId:
			                {
			                    throw new SupermodelException("AddWithExistingId is not supported by WebApiContext -- only GenerateIdAndAdd");
			                }
			                case OperationEnum.GenerateIdAndAdd:
			                {
			                    if (ModelId != 0 || Model.Id != 0) throw new SupermodelException("When adding a new Model Id must be equal 0.");
			                    return new HttpRequestMessage(HttpMethod.Post, $"{baseUrl}{DataContextBase.GetModelTypeLogicalName(ModelType)}")
			                    {
			                        //Content = new StringContent(JsonConvert.SerializeObject(Model.PerpareForSerializingForMasterDb()), Encoding.UTF8, ContentType)
			                        Content = new StringContent(JsonConvert.SerializeObject(Model), Encoding.UTF8, ContentType)
			                    };
			                }
			                case OperationEnum.Update:
			                {
			                    if (ModelId == 0 || Model.Id == 0 || ModelId != Model.Id) throw new SupermodelException("When updating a ModelIds must be not equal 0 and equal to each other");
			                    return new HttpRequestMessage(HttpMethod.Put, $"{baseUrl}{DataContextBase.GetModelTypeLogicalName(ModelType)}/{ModelId}")
			                    {
			                        //Content = new StringContent(JsonConvert.SerializeObject(Model.PerpareForSerializingForMasterDb()), Encoding.UTF8, ContentType)
			                        Content = new StringContent(JsonConvert.SerializeObject(Model), Encoding.UTF8, ContentType)
			                    };
			                }
			                case OperationEnum.Delete:
			                {
			                    return new HttpRequestMessage(HttpMethod.Delete, $"{baseUrl}{DataContextBase.GetModelTypeLogicalName(ModelType)}/{ModelId}");
			                }
			                case OperationEnum.DelayedGetById:
			                case OperationEnum.DelayedGetByIdOrDefault:
			                {
			                    return new HttpRequestMessage(HttpMethod.Get, $"{baseUrl}{DataContextBase.GetModelTypeLogicalName(ModelType)}/{ModelId}");
			                }
			                case OperationEnum.DelayedGetAll:
			                {
			                    return new HttpRequestMessage(HttpMethod.Get, $"{baseUrl}{DataContextBase.GetModelTypeLogicalName(ModelType)}/All");
			                }
			                case OperationEnum.DelayedGetCountAll:
			                {
			                    return new HttpRequestMessage(HttpMethod.Get, $"{baseUrl}{DataContextBase.GetModelTypeLogicalName(ModelType)}/CountAll");
			                }
			                case OperationEnum.DelayedGetWhere:
			                {
			                    return new HttpRequestMessage(HttpMethod.Get, $"{baseUrl}{DataContextBase.GetModelTypeLogicalName(ModelType)}/Where?{queryStringProvider.GetQueryString(SearchBy, Skip, Take, SortBy)}");
			                }
			                case OperationEnum.DelayedGetCountWhere:
			                {
			                    return new HttpRequestMessage(HttpMethod.Get, $"{baseUrl}{DataContextBase.GetModelTypeLogicalName(ModelType)}/CountWhere?{queryStringProvider.GetQueryString(SearchBy, Skip, Take, SortBy)}");
			                }
			
			                // ReSharper disable once RedundantCaseLabel
			                case OperationEnum.AddOrUpdate:
			                default:
			                    throw new SupermodelException("Unsupported Operation");
			            }
			        }
			        public void ProcessHttpResponse(string responseContentStr)
			        {
			            switch (Operation)
			            {
			                case OperationEnum.AddWithExistingId:
			                {
			                    throw new SupermodelException("AddWithExistingId is not supported by WebApiContext -- only GenerateIdAndAdd");
			                }
			                case OperationEnum.GenerateIdAndAdd:
			                {
			                    Model.Id = long.Parse(responseContentStr);
			                    Model.BroughtFromMasterDbOnUtc = DateTime.UtcNow;
			                    break;
			                }
			                case OperationEnum.Update:
			                {
			                    Model.BroughtFromMasterDbOnUtc = DateTime.UtcNow;
			                    break;
			                }
			                case OperationEnum.Delete:
			                {
			                    //Do nothing
			                    break;
			                }
			                case OperationEnum.DelayedGetById:
			                {
			                    if (string.IsNullOrEmpty(responseContentStr))
			                    {
			                        throw new SupermodelException("DelayedGetById: no object exists with id = " + ModelId);
			                    }
			                    else
			                    {
			                        var model = (IModel)JsonConvert.DeserializeObject(responseContentStr, ModelType);
			                        model.BroughtFromMasterDbOnUtc = DateTime.UtcNow;
			                        model.AfterLoad();
			                        DelayedValue.SetValue(model);
			                    }
			                    break;
			                }
			                case OperationEnum.DelayedGetByIdOrDefault:
			                {
			                    if (string.IsNullOrEmpty(responseContentStr))
			                    {
			                        DelayedValue.SetValue(null);
			                    }
			                    else
			                    {
			                        var model = (IModel)JsonConvert.DeserializeObject(responseContentStr, ModelType);
			                        model.BroughtFromMasterDbOnUtc = DateTime.UtcNow;
			                        model.AfterLoad();
			                        DelayedValue.SetValue(model);
			                    }
			                    break;
			                }
			                case OperationEnum.DelayedGetAll:
			                case OperationEnum.DelayedGetWhere:
			                {
			                    var models = (IEnumerable<IModel>)JsonConvert.DeserializeObject(responseContentStr, typeof(List<>).MakeGenericType(ModelType));
			                    foreach (var model in models)
			                    {
			                        model.BroughtFromMasterDbOnUtc = DateTime.UtcNow;
			                        model.AfterLoad();
			                    }
			                    DelayedValue.SetValue(models);
			                    break;
			                }
			                case OperationEnum.DelayedGetCountAll:
			                case OperationEnum.DelayedGetCountWhere:
			                {
			                    var count = JsonConvert.DeserializeObject<long>(responseContentStr);
			                    DelayedValue.SetValue(count);
			                    break;
			                }
			                // ReSharper disable once RedundantCaseLabel
			                case OperationEnum.AddOrUpdate:
			                default:
			                {
			                    throw new SupermodelException("Unsupported Operation");
			                }
			            }
			        }
			        #endregion
			
			        #region Sqlite Methods
			        public string GenerateSql(string dataTableName, ISqlQueryProvider sqlQueryProvider)
			        {
			            switch (Operation)
			            {
			                case OperationEnum.AddWithExistingId:
			                case OperationEnum.GenerateIdAndAdd:
			                {
			                    return new DataRow(Model, OriginalModelId, sqlQueryProvider).GenerateSqlInsert(dataTableName);
			                }
			                case OperationEnum.AddOrUpdate:
			                {
			                    return new DataRow(Model, OriginalModelId, sqlQueryProvider).GenerateSqlInsertOrReplace(dataTableName);
			                }
			                case OperationEnum.Update:
			                {
			                    return new DataRow(Model, OriginalModelId, sqlQueryProvider).GenerateSqlUpdate(dataTableName);
			                }
			                case OperationEnum.Delete:
			                {
			                    return $@"DELETE FROM [{dataTableName}] WHERE ModelTypeLogicalName = '{DataContextBase.GetModelTypeLogicalName(ModelType)}' AND ModelId = {ModelId}";
			                }
			                case OperationEnum.DelayedGetById:
			                case OperationEnum.DelayedGetByIdOrDefault:
			                {
			                    return $"SELECT * FROM [{dataTableName}] WHERE ModelTypeLogicalName = '{DataContextBase.GetModelTypeLogicalName(ModelType)}' AND ModelId = {ModelId}";
			                }
			                case OperationEnum.DelayedGetAll:
			                {
			                    return $"SELECT * FROM [{dataTableName}] WHERE ModelTypeLogicalName = '{DataContextBase.GetModelTypeLogicalName(ModelType)}'";
			                }
			                case OperationEnum.DelayedGetWhere:
			                {
			                    //var whereClause = sqlQueryProvider.GetWhereClause(SearchBy, SortBy);
			                    var whereClause = (string)sqlQueryProvider.ExecuteGenericMethod("GetWhereClause", new[] { ModelType }, SearchBy, SortBy);
			
			                    if (whereClause == null) throw new SupermodelException("Must override GetWhereClause before running queries on localDb");
			
			                    //var fullWhereClause = whereClause + sqlQueryProvider.GetSkipAndTakeForWhereClause(Skip, Take);
			                    var fullWhereClause = whereClause + sqlQueryProvider.ExecuteGenericMethod("GetSkipAndTakeForWhereClause", new[] { ModelType }, Skip, Take);
			
			                    return $"SELECT * FROM [{dataTableName}] WHERE ModelTypeLogicalName = '{DataContextBase.GetModelTypeLogicalName(ModelType)}' {fullWhereClause}";
			                }
			                case OperationEnum.DelayedGetCountAll:
			                {
			                    return $"SELECT COUNT(*) FROM [{dataTableName}] WHERE ModelTypeLogicalName = '{DataContextBase.GetModelTypeLogicalName(ModelType)}'";
			                }
			                case OperationEnum.DelayedGetCountWhere:
			                {
			                    //var whereClause = sqlQueryProvider.GetWhereClause(SearchBy, SortBy);
			                    var whereClause = (string)sqlQueryProvider.ExecuteGenericMethod("GetWhereClause", new[] { ModelType }, SearchBy, SortBy);
			
			                    if (whereClause == null) throw new SupermodelException("Must override GetWhereClause before running queries on localDb");
			
			                    //var fullWhereClause = whereClause + sqlQueryProvider.GetSkipAndTakeForWhereClause(Skip, Take);
			                    var fullWhereClause = whereClause + sqlQueryProvider.ExecuteGenericMethod("GetSkipAndTakeForWhereClause", new[] { ModelType }, Skip, Take);
			
			                    return $"SELECT COUNT(*) FROM [{dataTableName}] WHERE ModelTypeLogicalName = '{DataContextBase.GetModelTypeLogicalName(ModelType)}' AND {fullWhereClause}";
			                }
			                default:
			                {
			                    throw new SupermodelException("Unsupported Operation");
			                }
			            }
			        }
			        public void ProcessDatabaseResponseAsync(string dataTableName, object response, ISqlQueryProvider sqlQueryProvider)
			        {
			            switch (Operation)
			            {
			                case OperationEnum.AddWithExistingId:
			                case OperationEnum.GenerateIdAndAdd:
			                case OperationEnum.AddOrUpdate:
			                case OperationEnum.Update:
			                case OperationEnum.Delete:
			                {
			                    //we have nothing to process
			                    return;
			                }
			                case OperationEnum.DelayedGetById:
			                case OperationEnum.DelayedGetByIdOrDefault:
			                {
			                    var results = (List<DataRow>)response;
			                    if (results.Count == 0)
			                    {
			                        if (Operation == OperationEnum.DelayedGetById) throw new SupermodelException("DelayedGetById: no object exists with id = " + ModelId);
			                        DelayedValue.SetValue(null);
			                        return;
			                    }
			                    if (results.Count > 1) throw new Exception("GetByIdOrAsync or GetByIdOrDefaultAsync brought back more than one record");
			                    var model = results.Single().GetModel(ModelType);
			                    DelayedValue.SetValue(model);
			                    return;
			                }
			                case OperationEnum.DelayedGetAll:
			                case OperationEnum.DelayedGetWhere:
			                {
			                    var results = (List<DataRow>)response;
			                    var models = ReflectionHelper.CreateGenericType(typeof (List<>), ModelType);
			                    foreach (var result in results)
			                    {
			                        var model = result.GetModel(ModelType);
			                        models.ExecuteMethod("Add", model);
			                    }
			                    DelayedValue.SetValue(models);
			                    return;
			                }
			                case OperationEnum.DelayedGetCountAll:
			                case OperationEnum.DelayedGetCountWhere:
			                {
			                    var responseLong = (long)response;
			                    DelayedValue.SetValue(responseLong);
			                    return;
			                }
			                default:
			                {
			                    throw new SupermodelException("Unrecognized pendingAction.SqlType");
			                }
			            }
			        }
			        #endregion
			
			        #region Properties and Constants
			        public bool Disabled { get; set; }
			        public OperationEnum Operation { get; set; }
			        public Type ModelType { get; set; }
			        public long ModelId { get; set; }
			        public long OriginalModelId { get; set; }
			        public IModel Model { get; set; }
			        public DelayedValue DelayedValue { get; set; }
			        public object SearchBy { get; set; }
			        public int? Skip { get; set; }
			        public int? Take { get; set; }
			        public string SortBy { get; set; }
			
			        public const string ContentType = "application/json";
			        #endregion
			    }
			}
			#endregion
			
		#endregion
		
		#region Offline
			
			#region Synchronizer
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Offline
			{
			    using System;
			    using System.Collections.Generic;
			    using System.Linq;
			    using System.Threading.Tasks;
			    using Sqlite;
			    using WebApi;
			    using Exceptions;
			    using Models;
			    using ReflectionMapper;
			    using Repository;
			    using Core;
			    using UnitOfWork;
			    using Xamarin.Forms;
			    
			    public abstract class Synchronizer<ModelT, WebApiDataContextT, SqliteDataContextT> 
			        where ModelT : class, IModel, new()
			        where SqliteDataContextT : SqliteDataContext, new()
			        where WebApiDataContextT : WebApiDataContext, new()
			    {
			        #region Constructiors
			        protected Synchronizer()
			        {
			            RefreshFromMasterAfterSynch = true;
			        }
			        #endregion
			
			        #region Methods
			        public async Task SynchronizeAsync()
			        {
			            using(var webApiUOW = new UnitOfWork<WebApiDataContextT>())
			            {
			                SetUpWebApiContext(UnitOfWorkContext<WebApiDataContextT>.CurrentDataContext);
			
			                using(var sqliteUOW = new UnitOfWork<SqliteDataContextT>())
			                {
			                    SetUpSqliteContext(UnitOfWorkContext<SqliteDataContextT>.CurrentDataContext);
			                    
			                    //----------------------------------------------------------------------------------------
			                    //Load all the models to synchronize
			                    //----------------------------------------------------------------------------------------
			                    var masterModels = await LoadAllSynchFromMasterAsync();
			                    var localModels = await LoadAllSynchFromLocalAsync();
			                    
			                    //----------------------------------------------------------------------------------------
			                    //Run the synching algorithm
			                    //----------------------------------------------------------------------------------------
			                    SynchLists(masterModels, localModels);
			
			                    //----------------------------------------------------------------------------------------
			                    //Let's try to validate all the models that are to be saved locally
			                    //----------------------------------------------------------------------------------------
			                    SupermodelDataContextValidationException localValidationException = null;
			                    try
			                    {
			                        UnitOfWorkContext<SqliteDataContextT>.CurrentDataContext.ValidatePendingActions();
			                    }
			                    catch (SupermodelDataContextValidationException ex)
			                    {
			                        localValidationException = ex;
			                    }
			                    catch (Exception)
			                    {
			                        webApiUOW.Context.CommitOnDispose = sqliteUOW.Context.CommitOnDispose = false;
			                        throw;
			                    }
			                    if (localValidationException != null)
			                    {
			                        await ResolveLocalValidationError(localValidationException, webApiUOW, sqliteUOW);
			                    }
			
			                    //----------------------------------------------------------------------------------------
			                    //Register for resresh if needed
			                    //----------------------------------------------------------------------------------------
			
			                    DelayedModels<ModelT> delayedMasterModels = null;
			                    if (RefreshFromMasterAfterSynch)
			                    {
						            var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
			                        UnitOfWorkContext.DetectUpdates();
						            UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
			                        RegisterDelayedLoadAllSynchFromMaster(out delayedMasterModels);
			                    }
			
			                    //----------------------------------------------------------------------------------------
			                    //Then try to save changes to the web api
			                    //----------------------------------------------------------------------------------------
			                    UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
			
			                    SupermodelDataContextValidationException serverValidationException = null;
			                    try
			                    {
			                        await UnitOfWorkContext.FinalSaveChangesAsync();
			                    }
			                    catch (SupermodelDataContextValidationException ex)
			                    {
			                        serverValidationException = ex;
			                    }
			                    catch(Exception)
			                    {
			                        webApiUOW.Context.CommitOnDispose = sqliteUOW.Context.CommitOnDispose = false;
			                        throw;
			                    }
			                    
			                    if (serverValidationException != null)
			                    {
			                        await ResolveServerValidationError(serverValidationException, webApiUOW, sqliteUOW);
			                    }
			                    UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteUOW.Context);
			
			                    //----------------------------------------------------------------------------------------
			                    //Refsresh local models if we have data to refresh with (that is if we registered for it ealrier)
			                    //----------------------------------------------------------------------------------------
			                    if (delayedMasterModels != null)
			                    {
			                        foreach (var localModel in localModels)
			                        {
			                            var matchingDelpayedMasterModel = delayedMasterModels.Values.SingleOrDefault(x => x.Id == localModel.Id);
			                            if (matchingDelpayedMasterModel != null) CopyModel1IntoModel2(matchingDelpayedMasterModel, localModel);
			                        }
			                    }
			
			                    //----------------------------------------------------------------------------------------
			                    //If that succeds, then we save changes to the local db, should not have any more validation errors, since we already checked
			                    //----------------------------------------------------------------------------------------
			                    LastSynchDateTimeUtc = DateTime.UtcNow;
			                    await UnitOfWorkContext.FinalSaveChangesAsync();
			                }
			            }
			        }
			        public bool IsUploadPending(ModelT model)
			        {
			            return LastSynchDateTimeUtc == null || GetModifiedDateTimeUtc(model) > LastSynchDateTimeUtc;
			        }
			        #endregion
			
			        #region Main Algorithm
			        protected virtual void SynchLists(List<ModelT> masterModels, List<ModelT> localModels)
			        {
			            //find updated on the server, update locally
			            //find updated on the client, update on the server
			            //find created on the server, create locally
			            //find deleted locally, delete on the server
			            foreach (var masterModel in masterModels)
			            {
			                //Try to find a matching local model
			                var matchingLocalModel = localModels.SingleOrDefault(x => x.Id == masterModel.Id);
			                if (matchingLocalModel != null) //if we found one
			                {
			                    if (GetModifiedDateTimeUtc(masterModel) > LastSynchDateTimeUtc && GetModifiedDateTimeUtc(matchingLocalModel) > LastSynchDateTimeUtc)
			                    {
			                        //if model was updated on both server and client, call the hook to let the user resolve conflict
			                        HandleModelUpdatedOnServerAndDevice(masterModel, matchingLocalModel);
			                    }
			                    else
			                    {
			                        //otherwise, we just figure out the newer one and copy it into the older one
			                        CopyNewerModelIntoOlder(masterModel, matchingLocalModel);
			                    }
			                }
			                else //if not, there could be two scenarios:
			                {
			                    //If the model on the server was created after our last synch or if we never syhcned before
			                    if (GetCreatedDateTimeUtc(masterModel) > LastSynchDateTimeUtc || LastSynchDateTimeUtc == null)
			                    {
			                        //we need to add the master model to our local storage
			                        //var localModel = new ModelT();
			                        //CopyModel1IntoModel2(masterModel, localModel);
			                        //localModel.Add();
			                        masterModel.Add(); //Add master model to local storage
			                    }
			                    else
			                    {
			                        //otherwise it means that we deleted the model on the client and now we need to delete it from the server
			                        if (GetModifiedDateTimeUtc(masterModel) > LastSynchDateTimeUtc)
			                        {
			                            //if since our last synch the model was modified on the server and deleted on the client, call the hook to let the user resolve conflict
			                            HandleModelUpdatedOnServerDeletedOnDevice(masterModel);
			                        }
			                        else
			                        {
			                            var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
			                            masterModel.Delete();
			                            UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
			                        }
			                    }
			                }
			            }
			
			            //find created locally, create on the server
			            //find deleted on the server, delete locally
			            foreach (var localModel in localModels)
			            {
			                if (localModel.Id < 0)
			                {
			                    //if this model was never created on the server, go ahead and create it
			                    var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
			                    localModel.Id = 0; //make sure when we add it on the server, Id == 0. When it is saved on the server, the local model's id should get updated
			                    localModel.Add(); //add local model to server context
			                    UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
			                }
			                else
			                {
			                    //Try to find a matching server model
			                    var matchingServerModel = masterModels.SingleOrDefault(x => x.Id == localModel.Id);
			                    if (matchingServerModel == null) //if one exists, we already updated it and need not worry
			                    {
			                        //otherwise, it means that the model was deleted on the server
			                        if (GetModifiedDateTimeUtc(localModel) > LastSynchDateTimeUtc)
			                        {
			                            //if since our last synch the model was modified on the device and deleted on the client, call the hook to let the user resolve conflict
			                            HandleModelUpdatedOnDeviceDeletedOnServer(localModel);
			                        }
			                        else
			                        {
			                            localModel.Delete();
			                        }
			                    }
			                }
			            }
			        }
			        #endregion
			
			        #region Conflict Resolution Methods
			        protected virtual Task ResolveLocalValidationError(SupermodelDataContextValidationException validationException, UnitOfWork<WebApiDataContextT> webApiUOW, UnitOfWork<SqliteDataContextT> sqliteUOW)
			        {
			            //default implemetation just rolls back transcations and rethrows the exception
			            webApiUOW.Context.CommitOnDispose = sqliteUOW.Context.CommitOnDispose = false;
			            throw validationException;
			        }
			        protected virtual Task ResolveServerValidationError(SupermodelDataContextValidationException validationException, UnitOfWork<WebApiDataContextT> webApiUOW, UnitOfWork<SqliteDataContextT> sqliteUOW)
			        {
			            //default implemetation just rolls back transcations and rethrows the exception
			            webApiUOW.Context.CommitOnDispose = sqliteUOW.Context.CommitOnDispose = false;
			            throw validationException;
			        }
			        protected virtual void HandleModelUpdatedOnDeviceDeletedOnServer(ModelT localModel)
			        {
			            //default implements "last one wins" approach
			            localModel.Delete();
			        }
			        protected virtual void HandleModelUpdatedOnServerAndDevice(ModelT masterModel, ModelT localModel)
			        {
			            //default implements "last one wins" approach
			            CopyNewerModelIntoOlder(masterModel, localModel);
			        }
			        protected virtual void HandleModelUpdatedOnServerDeletedOnDevice(ModelT masterModel)
			        {
			            //default implements "last one wins" approach
			            var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
			            masterModel.Delete();
			            UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
			        }
			        #endregion
			
			        #region Modified and Created Utc DateTime Resolution
			        public abstract DateTime GetModifiedDateTimeUtc(ModelT model);
			        public abstract DateTime GetCreatedDateTimeUtc(ModelT model);
			        #endregion
			
			        #region Helpers that are Meant to be Overriden for Customization
			        protected virtual void RegisterDelayedLoadAllSynchFromMaster(out DelayedModels<ModelT> delayedMasterModels)
			        {
			            var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
			            RepoFactory.Create<ModelT>().DelayedGetAll(out delayedMasterModels);
			            UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
			        }
			        protected virtual Task<List<ModelT>> LoadAllSynchFromMasterAsync()
			        {
			            var sqliteDataContext = UnitOfWorkContext<SqliteDataContextT>.PopDbContext();
			            var result = RepoFactory.Create<ModelT>().GetAllAsync();
			            UnitOfWorkContext<SqliteDataContextT>.PushDbContext(sqliteDataContext);
			            return result;
			        }
			        protected virtual Task<List<ModelT>> LoadAllSynchFromLocalAsync()
			        {
			            return RepoFactory.Create<ModelT>().GetAllAsync();
			        }
			        public virtual void CopyModel1IntoModel2(ModelT model1, ModelT model2)
			        {
			            if (model1.BroughtFromMasterDbOnUtc == null || model2.BroughtFromMasterDbOnUtc == null) throw new SupermodelException("(model1.BroughtFromMasterDbOnUtc == null || model2.BroughtFromMasterDbOnUtc == null): this should not happen");
			            model1.BroughtFromMasterDbOnUtc = model2.BroughtFromMasterDbOnUtc = (model1.BroughtFromMasterDbOnUtc > model2.BroughtFromMasterDbOnUtc ? model1.BroughtFromMasterDbOnUtc : model2.BroughtFromMasterDbOnUtc);
			            model2.MapFrom(model1);
			        }
			        public virtual void CopyNewerModelIntoOlder(ModelT model1, ModelT model2)
			        {
			            if (GetModifiedDateTimeUtc(model1) > GetModifiedDateTimeUtc(model2)) CopyModel1IntoModel2(model1, model2);
			            else CopyModel1IntoModel2(model2, model1);
			        }
			        protected abstract void SetUpWebApiContext(WebApiDataContextT context);
			        protected abstract void SetUpSqliteContext(SqliteDataContextT context);
			        #endregion
			
			        #region LastSynch DateTime Handling
			        public virtual DateTime? LastSynchDateTimeUtc
			        {
			            get => _lastSynchDateTimeUtc ?? (_lastSynchDateTimeUtc = LastSynchDateTimeUtcInternal);
			            set => _lastSynchDateTimeUtc = LastSynchDateTimeUtcInternal = value;
			        }
			        private DateTime? _lastSynchDateTimeUtc;
			        
			        protected virtual DateTime? LastSynchDateTimeUtcInternal
			        {
			            get
			            {
			                if (!Application.Current.Properties.ContainsKey("smLastSynchDateTimeUtc")) return null;
			                return Application.Current.Properties["smLastSynchDateTimeUtc"] as DateTime?;
			            }
			            set
			            {
			                Application.Current.Properties["smLastSynchDateTimeUtc"] = value;
			                #pragma warning disable 4014
			                Application.Current.SavePropertiesAsync();
			                #pragma warning restore 4014
			            }
			        }
			        #endregion
			
			        #region Properties
			        public bool RefreshFromMasterAfterSynch { get; set; }
			        #endregion
			    }
			}
			#endregion
			
		#endregion
		
		#region Sqlite
			
			#region DataRow
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Sqlite
			{
			    using System;
			    using Newtonsoft.Json;
			    using Core;
			    using Models;
			    
			    public class DataRow<ModelT> : DataRow where ModelT : class, IModel, new()
			    {
			        #region Constructors
			        public DataRow(ModelT model, long originalModelId, ISqlQueryProvider sqlQueryProvider) : base(model, originalModelId, sqlQueryProvider)
			        {
			            var modelTypeLogicalName1 = DataContextBase.GetModelTypeLogicalName<ModelT>();
			            var modelTypeLogicalName2 = DataContextBase.GetModelTypeLogicalName(model.GetType());
			            if (modelTypeLogicalName1 != modelTypeLogicalName2) throw new Exception("DataContextBase.GetModelTypeLogicalName<ModelT>() != DataContextBase.GetModelTypeLogicalName(model.GetType())");
			        }
			        public DataRow(){} //default constructor for SQLite.NET
			        #endregion
			
			        #region Methods
			        public ModelT GetModel()
			        {
			            return (ModelT)GetModel(typeof(ModelT));
			        }
			        #endregion
			    }
			    
			    public class DataRow
			    {
			        #region Constructors
			        public DataRow(IModel model, long originalModelId, ISqlQueryProvider sqlQueryProvider)
			        {
			            ModelTypeLogicalName = DataContextBase.GetModelTypeLogicalName(model.GetType());
			            ModelId = model.Id;
			            OriginalModelId = originalModelId;
			            //Json = JsonConvert.SerializeObject(model.PerpareForSerializingForLocalDb());
			            Json = JsonConvert.SerializeObject(model);
			            BroughtFromMasterDbOnUtc = model.BroughtFromMasterDbOnUtc;       
			            Index0 = (string)sqlQueryProvider.GetIndex(0, model);
			            Index1 = (string)sqlQueryProvider.GetIndex(1, model); 
			            Index2 = (string)sqlQueryProvider.GetIndex(2, model); 
			            Index3 = (string)sqlQueryProvider.GetIndex(3, model);
			            Index4 = (string)sqlQueryProvider.GetIndex(4, model); 
			            Index5 = (string)sqlQueryProvider.GetIndex(5, model);
			            Index6 = (string)sqlQueryProvider.GetIndex(6, model); 
			            Index7 = (string)sqlQueryProvider.GetIndex(7, model); 
			            Index8 = (string)sqlQueryProvider.GetIndex(8, model);
			            Index9 = (string)sqlQueryProvider.GetIndex(9, model); 
			            Index10 = (long?)sqlQueryProvider.GetIndex(10, model);
			            Index11 = (long?)sqlQueryProvider.GetIndex(11, model); 
			            Index12 = (long?)sqlQueryProvider.GetIndex(12, model); 
			            Index13 = (long?)sqlQueryProvider.GetIndex(13, model);
			            Index14 = (long?)sqlQueryProvider.GetIndex(14, model); 
			            Index15 = (long?)sqlQueryProvider.GetIndex(15, model);
			            Index16 = (long?)sqlQueryProvider.GetIndex(16, model); 
			            Index17 = (long?)sqlQueryProvider.GetIndex(17, model); 
			            Index18 = (long?)sqlQueryProvider.GetIndex(18, model);
			            Index19 = (long?)sqlQueryProvider.GetIndex(19, model); 
			            Index20 = (double?)sqlQueryProvider.GetIndex(20, model);
			            Index21 = (double?)sqlQueryProvider.GetIndex(21, model); 
			            Index22 = (double?)sqlQueryProvider.GetIndex(22, model); 
			            Index23 = (double?)sqlQueryProvider.GetIndex(23, model);
			            Index24 = (double?)sqlQueryProvider.GetIndex(24, model); 
			            Index25 = (double?)sqlQueryProvider.GetIndex(25, model);
			            Index26 = (double?)sqlQueryProvider.GetIndex(26, model); 
			            Index27 = (double?)sqlQueryProvider.GetIndex(27, model); 
			            Index28 = (double?)sqlQueryProvider.GetIndex(28, model);
			            Index29 = (double?)sqlQueryProvider.GetIndex(29, model); 
			        }
			        public DataRow(){} //default constructor for SQLite.NET
			        #endregion
			
			        #region Methods
			        public IModel GetModel(Type modelType)
			        {
			            var model = (IModel)JsonConvert.DeserializeObject(Json, modelType);
			            if (model.Id != ModelId || ModelTypeLogicalName != DataContextBase.GetModelTypeLogicalName(modelType)) throw new Exception("Database corruption: (THIS SHOULD NEVER HAPPEN): model.Id != DataRow.ModelId || ModelTypeLogicalName != DataContextBase.GetModelTypeLogicalName<ModelT>()");
			            return model;
			        }
			        public string GenerateSqlInsertOrReplace(string dataTableName)
			        {
			            var sql = string.Format(@"INSERT OR REPLACE INTO [{0}] 
						                          (ModelTypeLogicalName, ModelId, Json, BroughtFromMasterDbOnUtcTicks, Index0, Index1, Index2, Index3, Index4, Index5, Index6, Index7, Index8, Index9, Index10, Index11, Index12, Index13, Index14, Index15, Index16, Index17, Index18, Index19, Index20, Index21, Index22, Index23, Index24, Index25, Index26, Index27, Index28, Index29) 
			                                      VALUES ('{1}', '{2}', '{3}', {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}, {23}, {24}, {25}, {26}, {27}, {28}, {29}, {30}, {31}, {32}, {33}, {34})",
			                                      dataTableName,
			                                      ModelTypeLogicalName,
			                                      ModelId,
			                                      Json.Replace("'", "''"),
			                                      BroughtFromMasterDbOnUtc?.Ticks.ToString() ?? "NULL",
			                                      Index0 != null ? "'" + Index0.Replace("'", "''") + "'" : "NULL",
			                                      Index1 != null ? "'" + Index1.Replace("'", "''") + "'" : "NULL",
			                                      Index2 != null ? "'" + Index2.Replace("'", "''") + "'" : "NULL",
			                                      Index3 != null ? "'" + Index3.Replace("'", "''") + "'" : "NULL",
			                                      Index4 != null ? "'" + Index4.Replace("'", "''") + "'" : "NULL",
			                                      Index5 != null ? "'" + Index5.Replace("'", "''") + "'" : "NULL",
			                                      Index6 != null ? "'" + Index6.Replace("'", "''") + "'" : "NULL",
			                                      Index7 != null ? "'" + Index7.Replace("'", "''") + "'" : "NULL",
			                                      Index8 != null ? "'" + Index8.Replace("'", "''") + "'" : "NULL",
			                                      Index9 != null ? "'" + Index9.Replace("'", "''") + "'" : "NULL",
			                                      Index10 != null ? Index10.ToString() : "NULL",
			                                      Index11 != null ? Index11.ToString() : "NULL",
			                                      Index12 != null ? Index12.ToString() : "NULL",
			                                      Index13 != null ? Index13.ToString() : "NULL",
			                                      Index14 != null ? Index14.ToString() : "NULL",
			                                      Index15 != null ? Index15.ToString() : "NULL",
			                                      Index16 != null ? Index16.ToString() : "NULL",
			                                      Index17 != null ? Index17.ToString() : "NULL",
			                                      Index18 != null ? Index18.ToString() : "NULL",
			                                      Index19 != null ? Index19.ToString() : "NULL",
			                                      Index20 != null ? Index20.ToString() : "NULL",
			                                      Index21 != null ? Index21.ToString() : "NULL",
			                                      Index22 != null ? Index22.ToString() : "NULL",
			                                      Index23 != null ? Index23.ToString() : "NULL",
			                                      Index24 != null ? Index24.ToString() : "NULL",
			                                      Index25 != null ? Index25.ToString() : "NULL",
			                                      Index26 != null ? Index26.ToString() : "NULL",
			                                      Index27 != null ? Index27.ToString() : "NULL",
			                                      Index28 != null ? Index28.ToString() : "NULL",
			                                      Index29 != null ? Index29.ToString() : "NULL");
			            return sql;
			        }
			        public string GenerateSqlInsert(string dataTableName)
			        {
			            var sql = string.Format(@"INSERT INTO [{0}] 
						                          (ModelTypeLogicalName, ModelId, Json, BroughtFromMasterDbOnUtcTicks, Index0, Index1, Index2, Index3, Index4, Index5, Index6, Index7, Index8, Index9, Index10, Index11, Index12, Index13, Index14, Index15, Index16, Index17, Index18, Index19, Index20, Index21, Index22, Index23, Index24, Index25, Index26, Index27, Index28, Index29) 
			                                      VALUES ('{1}', '{2}', '{3}', {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}, {23}, {24}, {25}, {26}, {27}, {28}, {29}, {30}, {31}, {32}, {33}, {34})",
			                                      dataTableName,
			                                      ModelTypeLogicalName, 
			                                      ModelId, 
			                                      Json.Replace("'", "''"),
			                                      BroughtFromMasterDbOnUtc?.Ticks.ToString() ?? "NULL",
			                                      Index0 != null ? "'" + Index0.Replace("'", "''") + "'" : "NULL",
			                                      Index1 != null ? "'" + Index1.Replace("'", "''") + "'" : "NULL",
			                                      Index2 != null ? "'" + Index2.Replace("'", "''") + "'" : "NULL",
			                                      Index3 != null ? "'" + Index3.Replace("'", "''") + "'" : "NULL",
			                                      Index4 != null ? "'" + Index4.Replace("'", "''") + "'" : "NULL",
			                                      Index5 != null ? "'" + Index5.Replace("'", "''") + "'" : "NULL",
			                                      Index6 != null ? "'" + Index6.Replace("'", "''") + "'" : "NULL",
			                                      Index7 != null ? "'" + Index7.Replace("'", "''") + "'" : "NULL",
			                                      Index8 != null ? "'" + Index8.Replace("'", "''") + "'" : "NULL",
			                                      Index9 != null ? "'" + Index9.Replace("'", "''") + "'" : "NULL",
			                                      Index10 != null ? Index10.ToString() : "NULL",
			                                      Index11 != null ? Index11.ToString() : "NULL",
			                                      Index12 != null ? Index12.ToString() : "NULL",
			                                      Index13 != null ? Index13.ToString() : "NULL",
			                                      Index14 != null ? Index14.ToString() : "NULL",
			                                      Index15 != null ? Index15.ToString() : "NULL",
			                                      Index16 != null ? Index16.ToString() : "NULL",
			                                      Index17 != null ? Index17.ToString() : "NULL",
			                                      Index18 != null ? Index18.ToString() : "NULL",
			                                      Index19 != null ? Index19.ToString() : "NULL",
			                                      Index20 != null ? Index20.ToString() : "NULL",
			                                      Index21 != null ? Index21.ToString() : "NULL",
			                                      Index22 != null ? Index22.ToString() : "NULL",
			                                      Index23 != null ? Index23.ToString() : "NULL",
			                                      Index24 != null ? Index24.ToString() : "NULL",
			                                      Index25 != null ? Index25.ToString() : "NULL",
			                                      Index26 != null ? Index26.ToString() : "NULL",
			                                      Index27 != null ? Index27.ToString() : "NULL",
			                                      Index28 != null ? Index28.ToString() : "NULL",
			                                      Index29 != null ? Index29.ToString() : "NULL");
			            return sql;
			        }
					public string GenerateSqlUpdate(string dataTableName)
					{
						var sql = string.Format(@"UPDATE [{0}] 
						                            SET 
						                            ModelId = {35},
						                            Json = '{3}', 
						                            BroughtFromMasterDbOnUtcTicks = {4},
						                            Index0 = {5}, 
						                            Index1 = {6}, 
						                            Index2 = {7}, 
						                            Index3 = {8}, 
						                            Index4 = {9},
						                            Index5 = {10}, 
						                            Index6 = {11}, 
						                            Index7 = {12}, 
						                            Index8 = {13}, 
						                            Index9 = {14},
						                            Index10 = {15}, 
						                            Index11 = {16}, 
						                            Index12 = {17}, 
						                            Index13 = {18}, 
						                            Index14 = {19},
						                            Index15 = {20}, 
						                            Index16 = {21}, 
						                            Index17 = {22}, 
						                            Index18 = {23}, 
						                            Index19 = {24},
						                            Index20 = {25}, 
						                            Index21 = {26}, 
						                            Index22 = {27}, 
						                            Index23 = {28}, 
						                            Index24 = {29},
						                            Index25 = {30}, 
						                            Index26 = {31}, 
						                            Index27 = {32}, 
						                            Index28 = {33}, 
						                            Index29 = {34}
						                            WHERE ModelTypeLogicalName = '{1}' AND ModelId = {2}",
						                            dataTableName,
						                            ModelTypeLogicalName, 
						                            OriginalModelId, 
						                            Json.Replace("'", "''"),
						                            BroughtFromMasterDbOnUtc?.Ticks.ToString() ?? "NULL",
						                            Index0 != null ? "'" + Index0.Replace("'", "''") + "'" : "NULL",
						                            Index1 != null ? "'" + Index1.Replace("'", "''") + "'" : "NULL",
						                            Index2 != null ? "'" + Index2.Replace("'", "''") + "'" : "NULL",
						                            Index3 != null ? "'" + Index3.Replace("'", "''") + "'" : "NULL",
						                            Index4 != null ? "'" + Index4.Replace("'", "''") + "'" : "NULL",
						                            Index5 != null ? "'" + Index5.Replace("'", "''") + "'" : "NULL",
						                            Index6 != null ? "'" + Index6.Replace("'", "''") + "'" : "NULL",
						                            Index7 != null ? "'" + Index7.Replace("'", "''") + "'" : "NULL",
						                            Index8 != null ? "'" + Index8.Replace("'", "''") + "'" : "NULL",
						                            Index9 != null ? "'" + Index9.Replace("'", "''") + "'" : "NULL",
						                            Index10 != null ? Index10.ToString() : "NULL",
						                            Index11 != null ? Index11.ToString() : "NULL",
						                            Index12 != null ? Index12.ToString() : "NULL",
						                            Index13 != null ? Index13.ToString() : "NULL",
						                            Index14 != null ? Index14.ToString() : "NULL",
						                            Index15 != null ? Index15.ToString() : "NULL",
						                            Index16 != null ? Index16.ToString() : "NULL",
						                            Index17 != null ? Index17.ToString() : "NULL",
						                            Index18 != null ? Index18.ToString() : "NULL",
						                            Index19 != null ? Index19.ToString() : "NULL",
						                            Index20 != null ? Index20.ToString() : "NULL",
						                            Index21 != null ? Index21.ToString() : "NULL",
						                            Index22 != null ? Index22.ToString() : "NULL",
						                            Index23 != null ? Index23.ToString() : "NULL",
						                            Index24 != null ? Index24.ToString() : "NULL",
						                            Index25 != null ? Index25.ToString() : "NULL",
						                            Index26 != null ? Index26.ToString() : "NULL",
						                            Index27 != null ? Index27.ToString() : "NULL",
						                            Index28 != null ? Index28.ToString() : "NULL",
						                            Index29 != null ? Index29.ToString() : "NULL",
						                            ModelId);
						return sql;
					}
			        #endregion
			
			        #region Properties
			        public string ModelTypeLogicalName { get; set; }
			        public long ModelId { get; set; }
			        public long OriginalModelId { get; set; }
			        public string Json { get; set; }
			        public DateTime? BroughtFromMasterDbOnUtc { get; set; }
			        public string Index0 { get; set; }
			        public string Index1 { get; set; }
			        public string Index2 { get; set; }
			        public string Index3 { get; set; }
			        public string Index4 { get; set; }
			        public string Index5 { get; set; }
			        public string Index6 { get; set; }
			        public string Index7 { get; set; }
			        public string Index8 { get; set; }
			        public string Index9 { get; set; }
			        public long? Index10 { get; set; }
			        public long? Index11 { get; set; }
			        public long? Index12 { get; set; }
			        public long? Index13 { get; set; }
			        public long? Index14 { get; set; }
			        public long? Index15 { get; set; }
			        public long? Index16 { get; set; }
			        public long? Index17 { get; set; }
			        public long? Index18 { get; set; }
			        public long? Index19 { get; set; }
			        public double? Index20 { get; set; }
			        public double? Index21 { get; set; }
			        public double? Index22 { get; set; }
			        public double? Index23 { get; set; }
			        public double? Index24 { get; set; }
			        public double? Index25 { get; set; }
			        public double? Index26 { get; set; }
			        public double? Index27 { get; set; }
			        public double? Index28 { get; set; }
			        public double? Index29 { get; set; }
			        #endregion
			    }
			}
			#endregion
			
			#region ISqlQueryProvider
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Sqlite
			{
			    using Models;
			    
			    public interface ISqlQueryProvider
			    {
			        object GetIndex<ModelT>(int idxNum0To29, ModelT model);
			        string GetWhereClause<ModelT>(object searchBy, string sortBy);
			        string GetSkipAndTakeForWhereClause<ModelT>(int? skip, int? take);
			    }
			}
			#endregion
			
			#region SQLite
			//
			// Copyright (c) 2009-2015 Krueger Systems, Inc.
			// 
			// Permission is hereby granted, free of charge, to any person obtaining a copy
			// of this software and associated documentation files (the "Software"), to deal
			// in the Software without restriction, including without limitation the rights
			// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
			// copies of the Software, and to permit persons to whom the Software is
			// furnished to do so, subject to the following conditions:
			// 
			// The above copyright notice and this permission notice shall be included in
			// all copies or substantial portions of the Software.
			// 
			// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
			// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
			// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
			// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
			// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
			// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
			// THE SOFTWARE.
			//
			//#if WINDOWS_PHONE && !USE_WP8_NATIVE_SQLITE
			//#define USE_CSHARP_SQLITE
			//#endif
			
			//#if NETFX_CORE
			//#define USE_NEW_REFLECTION_API
			//#endif
			
			// ReSharper disable once CheckNamespace
			namespace SQLite
			{
			    using System;
			    using System.Diagnostics;
			    #if !USE_SQLITEPCL_RAW
			    using System.Runtime.InteropServices;
			    #endif
			    using System.Collections.Generic;
			    #if NO_CONCURRENT
			    using ConcurrentStringDictionary = System.Collections.Generic.Dictionary<string, object>;
			    using SQLite.Extensions;
			    #else
			    using ConcurrentStringDictionary = System.Collections.Concurrent.ConcurrentDictionary<string, object>;
			    #endif
			    using System.Reflection;
			    using System.Linq;
			    using System.Linq.Expressions;
			    using System.Threading;
			
			    #if USE_CSHARP_SQLITE
			    using Sqlite3 = Community.CsharpSqlite.Sqlite3;
			    using Sqlite3DatabaseHandle = Community.CsharpSqlite.Sqlite3.sqlite3;
			    using Sqlite3Statement = Community.CsharpSqlite.Sqlite3.Vdbe;
			    #elif USE_WP8_NATIVE_SQLITE
			    using Sqlite3 = Sqlite.Sqlite3;
			    using Sqlite3DatabaseHandle = Sqlite.Database;
			    using Sqlite3Statement = Sqlite.Statement;
			    #elif USE_SQLITEPCL_RAW
			    using Sqlite3DatabaseHandle = SQLitePCL.sqlite3;
			    using Sqlite3Statement = SQLitePCL.sqlite3_stmt;
			    using Sqlite3 = SQLitePCL.raw;
			    #else
			    using Sqlite3DatabaseHandle = System.IntPtr;
			    using Sqlite3Statement = System.IntPtr;
			    #endif
			
			    public class SQLiteException : Exception
				{
					public SQLite3.Result Result { get; private set; }
			
					protected SQLiteException (SQLite3.Result r,string message) : base(message)
					{
						Result = r;
					}
			
					public static SQLiteException New (SQLite3.Result r, string message)
					{
						return new SQLiteException (r, message);
					}
				}
			
				public class NotNullConstraintViolationException : SQLiteException
				{
					public IEnumerable<TableMapping.Column> Columns { get; protected set; }
			
					protected NotNullConstraintViolationException (SQLite3.Result r, string message)
						: this (r, message, null, null)
					{
			
					}
			
					protected NotNullConstraintViolationException (SQLite3.Result r, string message, TableMapping mapping, object obj)
						: base (r, message)
					{
						if (mapping != null && obj != null) {
							this.Columns = from c in mapping.Columns
										   where c.IsNullable == false && c.GetValue (obj) == null
										   select c;
						}
					}
			
					public static new NotNullConstraintViolationException New (SQLite3.Result r, string message)
					{
						return new NotNullConstraintViolationException (r, message);
					}
			
					public static NotNullConstraintViolationException New (SQLite3.Result r, string message, TableMapping mapping, object obj)
					{
						return new NotNullConstraintViolationException (r, message, mapping, obj);
					}
			
					public static NotNullConstraintViolationException New (SQLiteException exception, TableMapping mapping, object obj)
					{
						return new NotNullConstraintViolationException (exception.Result, exception.Message, mapping, obj);
					}
				}
			
				[Flags]
				public enum SQLiteOpenFlags {
					ReadOnly = 1, ReadWrite = 2, Create = 4,
					NoMutex = 0x8000, FullMutex = 0x10000,
					SharedCache = 0x20000, PrivateCache = 0x40000,
					ProtectionComplete = 0x00100000,
					ProtectionCompleteUnlessOpen = 0x00200000,
					ProtectionCompleteUntilFirstUserAuthentication = 0x00300000,
					ProtectionNone = 0x00400000
				}
			
			    [Flags]
			    public enum CreateFlags
			    {
			        None                = 0x000,
			        ImplicitPK          = 0x001,    // create a primary key for field called 'Id' (Orm.ImplicitPkName)
			        ImplicitIndex       = 0x002,    // create an index for fields ending in 'Id' (Orm.ImplicitIndexSuffix)
			        AllImplicit         = 0x003,    // do both above
			        AutoIncPK           = 0x004,    // force PK field to be auto inc
			        FullTextSearch3     = 0x100,    // create virtual table using FTS3
			        FullTextSearch4     = 0x200     // create virtual table using FTS4
			    }
			
				/// <summary>
				/// Represents an open connection to a SQLite database.
				/// </summary>
				public partial class SQLiteConnection : IDisposable
				{
					private bool _open;
					private TimeSpan _busyTimeout;
					private Dictionary<string, TableMapping> _mappings = null;
					private Dictionary<string, TableMapping> _tables = null;
					private System.Diagnostics.Stopwatch _sw;
					private long _elapsedMilliseconds = 0;
			
					private int _transactionDepth = 0;
					private Random _rand = new Random ();
			
					public Sqlite3DatabaseHandle Handle { get; private set; }
					internal static readonly Sqlite3DatabaseHandle NullHandle = default(Sqlite3DatabaseHandle);
			
					public string DatabasePath { get; private set; }
			
					public bool TimeExecution { get; set; }
			
					public bool Trace { get; set; }
			
					public bool StoreDateTimeAsTicks { get; private set; }
			
					/// <summary>
					/// Constructs a new SQLiteConnection and opens a SQLite database specified by databasePath.
					/// </summary>
					/// <param name="databasePath">
					/// Specifies the path to the database file.
					/// </param>
					/// <param name="storeDateTimeAsTicks">
					/// Specifies whether to store DateTime properties as ticks (true) or strings (false). You
					/// absolutely do want to store them as Ticks in all new projects. The value of false is
					/// only here for backwards compatibility. There is a *significant* speed advantage, with no
					/// down sides, when setting storeDateTimeAsTicks = true.
					/// If you use DateTimeOffset properties, it will be always stored as ticks regardingless
					/// the storeDateTimeAsTicks parameter.
					/// </param>
					public SQLiteConnection (string databasePath, bool storeDateTimeAsTicks = true)
						: this (databasePath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create, storeDateTimeAsTicks)
					{
					}
			
					/// <summary>
					/// Constructs a new SQLiteConnection and opens a SQLite database specified by databasePath.
					/// </summary>
					/// <param name="databasePath">
					/// Specifies the path to the database file.
					/// </param>
					/// <param name="storeDateTimeAsTicks">
					/// Specifies whether to store DateTime properties as ticks (true) or strings (false). You
					/// absolutely do want to store them as Ticks in all new projects. The value of false is
					/// only here for backwards compatibility. There is a *significant* speed advantage, with no
					/// down sides, when setting storeDateTimeAsTicks = true.
					/// If you use DateTimeOffset properties, it will be always stored as ticks regardingless
					/// the storeDateTimeAsTicks parameter.
					/// </param>
					public SQLiteConnection (string databasePath, SQLiteOpenFlags openFlags, bool storeDateTimeAsTicks = true)
					{
						if (string.IsNullOrEmpty (databasePath))
							throw new ArgumentException ("Must be specified", "databasePath");
			
						DatabasePath = databasePath;
			
			#if NETFX_CORE
						SQLite3.SetDirectory(/*temp directory type*/2, Windows.Storage.ApplicationData.Current.TemporaryFolder.Path);
			#endif
			
						Sqlite3DatabaseHandle handle;
			
			#if SILVERLIGHT || USE_CSHARP_SQLITE || USE_SQLITEPCL_RAW
			            var r = SQLite3.Open (databasePath, out handle, (int)openFlags, IntPtr.Zero);
			#else
						// open using the byte[]
						// in the case where the path may include Unicode
						// force open to using UTF-8 using sqlite3_open_v2
						var databasePathAsBytes = GetNullTerminatedUtf8 (DatabasePath);
						var r = SQLite3.Open (databasePathAsBytes, out handle, (int) openFlags, IntPtr.Zero);
			#endif
			
						Handle = handle;
						if (r != SQLite3.Result.OK) {
							throw SQLiteException.New (r, String.Format ("Could not open database file: {0} ({1})", DatabasePath, r));
						}
						_open = true;
			
						StoreDateTimeAsTicks = storeDateTimeAsTicks;
						
						BusyTimeout = TimeSpan.FromSeconds (0.1);
					}
					
			#if __IOS__
					static SQLiteConnection ()
					{
						if (_preserveDuringLinkMagic) {
							var ti = new ColumnInfo ();
							ti.Name = "magic";
						}
					}
			
			   		/// <summary>
					/// Used to list some code that we want the MonoTouch linker
					/// to see, but that we never want to actually execute.
					/// </summary>
					#pragma warning disable 0649
			        static bool _preserveDuringLinkMagic;
			        #pragma warning restore 0649
			#endif
			
			#if !USE_SQLITEPCL_RAW
			        public void EnableLoadExtension(int onoff)
			        {
			            SQLite3.Result r = SQLite3.EnableLoadExtension(Handle, onoff);
						if (r != SQLite3.Result.OK) {
							string msg = SQLite3.GetErrmsg (Handle);
							throw SQLiteException.New (r, msg);
						}
			        }
			#endif
			
			#if !USE_SQLITEPCL_RAW
					static byte[] GetNullTerminatedUtf8 (string s)
					{
						var utf8Length = System.Text.Encoding.UTF8.GetByteCount (s);
						var bytes = new byte [utf8Length + 1];
						utf8Length = System.Text.Encoding.UTF8.GetBytes(s, 0, s.Length, bytes, 0);
						return bytes;
					}
			#endif
			
			        /// <summary>
					/// Sets a busy handler to sleep the specified amount of time when a table is locked.
					/// The handler will sleep multiple times until a total time of <see cref="BusyTimeout"/> has accumulated.
					/// </summary>
					public TimeSpan BusyTimeout {
						get { return _busyTimeout; }
						set {
							_busyTimeout = value;
							if (Handle != NullHandle) {
								SQLite3.BusyTimeout (Handle, (int)_busyTimeout.TotalMilliseconds);
							}
						}
					}
			
					/// <summary>
					/// Returns the mappings from types to tables that the connection
					/// currently understands.
					/// </summary>
					public IEnumerable<TableMapping> TableMappings {
						get {
							return _tables != null ? _tables.Values : Enumerable.Empty<TableMapping> ();
						}
					}
			
					/// <summary>
					/// Retrieves the mapping that is automatically generated for the given type.
					/// </summary>
					/// <param name="type">
					/// The type whose mapping to the database is returned.
					/// </param>         
			        /// <param name="createFlags">
					/// Optional flags allowing implicit PK and indexes based on naming conventions
					/// </param>     
					/// <returns>
					/// The mapping represents the schema of the columns of the database and contains 
					/// methods to set and get properties of objects.
					/// </returns>
			        public TableMapping GetMapping(Type type, CreateFlags createFlags = CreateFlags.None)
					{
						if (_mappings == null) {
							_mappings = new Dictionary<string, TableMapping> ();
						}
						TableMapping map;
						if (!_mappings.TryGetValue (type.FullName, out map)) {
							map = new TableMapping (type, createFlags);
							_mappings [type.FullName] = map;
						}
						return map;
					}
					
					/// <summary>
					/// Retrieves the mapping that is automatically generated for the given type.
					/// </summary>
					/// <returns>
					/// The mapping represents the schema of the columns of the database and contains 
					/// methods to set and get properties of objects.
					/// </returns>
					public TableMapping GetMapping<T> ()
					{
						return GetMapping (typeof (T));
					}
			
					private struct IndexedColumn
					{
						public int Order;
						public string ColumnName;
					}
			
					private struct IndexInfo
					{
						public string IndexName;
						public string TableName;
						public bool Unique;
						public List<IndexedColumn> Columns;
					}
			
					/// <summary>
					/// Executes a "drop table" on the database.  This is non-recoverable.
					/// </summary>
					public int DropTable<T>()
					{
						var map = GetMapping (typeof (T));
			
						var query = string.Format("drop table if exists \"{0}\"", map.TableName);
			
						return Execute (query);
					}
					
					/// <summary>
					/// Executes a "create table if not exists" on the database. It also
					/// creates any specified indexes on the columns of the table. It uses
					/// a schema automatically generated from the specified type. You can
					/// later access this schema by calling GetMapping.
					/// </summary>
					/// <returns>
					/// The number of entries added to the database schema.
					/// </returns>
					public int CreateTable<T>(CreateFlags createFlags = CreateFlags.None)
					{
						return CreateTable(typeof (T), createFlags);
					}
			
					/// <summary>
					/// Executes a "create table if not exists" on the database. It also
					/// creates any specified indexes on the columns of the table. It uses
					/// a schema automatically generated from the specified type. You can
					/// later access this schema by calling GetMapping.
					/// </summary>
					/// <param name="ty">Type to reflect to a database table.</param>
			        /// <param name="createFlags">Optional flags allowing implicit PK and indexes based on naming conventions.</param>  
					/// <returns>
					/// The number of entries added to the database schema.
					/// </returns>
			        public int CreateTable(Type ty, CreateFlags createFlags = CreateFlags.None)
					{
						if (_tables == null) {
							_tables = new Dictionary<string, TableMapping> ();
						}
						TableMapping map;
						if (!_tables.TryGetValue (ty.FullName, out map)) {
							map = GetMapping (ty, createFlags);
							_tables.Add (ty.FullName, map);
						}
			
						// Present a nice error if no columns specified
						if (map.Columns.Length == 0) {
							throw new Exception (string.Format ("Cannot create a table with zero columns (does '{0}' have public properties?)", ty.FullName));
						}
			
			            // Facilitate virtual tables a.k.a. full-text search.
					    bool fts3 = (createFlags & CreateFlags.FullTextSearch3) != 0;
					    bool fts4 = (createFlags & CreateFlags.FullTextSearch4) != 0;
					    bool fts = fts3 || fts4;
			            var @virtual = fts ? "virtual " : string.Empty;
					    var @using = fts3 ? "using fts3 " : fts4 ? "using fts4 " : string.Empty;
			
			            // Build query.
						var query = "create " + @virtual + "table if not exists \"" + map.TableName + "\" " + @using + "(\n";
						var decls = map.Columns.Select (p => Orm.SqlDecl (p, StoreDateTimeAsTicks));
						var decl = string.Join (",\n", decls.ToArray ());
						query += decl;
						query += ")";
						
						var count = Execute (query);
						
						if (count == 0) { //Possible bug: This always seems to return 0?
							// Table already exists, migrate it
							MigrateTable (map);
						}
			
						var indexes = new Dictionary<string, IndexInfo> ();
						foreach (var c in map.Columns) {
							foreach (var i in c.Indices) {
								var iname = i.Name ?? map.TableName + "_" + c.Name;
								IndexInfo iinfo;
								if (!indexes.TryGetValue (iname, out iinfo)) {
									iinfo = new IndexInfo {
										IndexName = iname,
										TableName = map.TableName,
										Unique = i.Unique,
										Columns = new List<IndexedColumn> ()
									};
									indexes.Add (iname, iinfo);
								}
			
								if (i.Unique != iinfo.Unique)
									throw new Exception ("All the columns in an index must have the same value for their Unique property");
			
								iinfo.Columns.Add (new IndexedColumn {
									Order = i.Order,
									ColumnName = c.Name
								});
							}
						}
			
						foreach (var indexName in indexes.Keys) {
							var index = indexes[indexName];
							var columns = index.Columns.OrderBy(i => i.Order).Select(i => i.ColumnName).ToArray();
			                count += CreateIndex(indexName, index.TableName, columns, index.Unique);
						}
						
						return count;
					}
			
			        /// <summary>
			        /// Creates an index for the specified table and columns.
			        /// </summary>
			        /// <param name="indexName">Name of the index to create</param>
			        /// <param name="tableName">Name of the database table</param>
			        /// <param name="columnNames">An array of column names to index</param>
			        /// <param name="unique">Whether the index should be unique</param>
			        public int CreateIndex(string indexName, string tableName, string[] columnNames, bool unique = false)
			        {
			            const string sqlFormat = "create {2} index if not exists \"{3}\" on \"{0}\"(\"{1}\")";
			            var sql = String.Format(sqlFormat, tableName, string.Join ("\", \"", columnNames), unique ? "unique" : "", indexName);
			            return Execute(sql);
			        }
			
			        /// <summary>
			        /// Creates an index for the specified table and column.
			        /// </summary>
			        /// <param name="indexName">Name of the index to create</param>
			        /// <param name="tableName">Name of the database table</param>
			        /// <param name="columnName">Name of the column to index</param>
			        /// <param name="unique">Whether the index should be unique</param>
			        public int CreateIndex(string indexName, string tableName, string columnName, bool unique = false)
			        {
			            return CreateIndex(indexName, tableName, new string[] { columnName }, unique);
			        }
			        
			        /// <summary>
			        /// Creates an index for the specified table and column.
			        /// </summary>
			        /// <param name="tableName">Name of the database table</param>
			        /// <param name="columnName">Name of the column to index</param>
			        /// <param name="unique">Whether the index should be unique</param>
			        public int CreateIndex(string tableName, string columnName, bool unique = false)
			        {
			            return CreateIndex(tableName + "_" + columnName, tableName, columnName, unique);
			        }
			
			        /// <summary>
			        /// Creates an index for the specified table and columns.
			        /// </summary>
			        /// <param name="tableName">Name of the database table</param>
			        /// <param name="columnNames">An array of column names to index</param>
			        /// <param name="unique">Whether the index should be unique</param>
			        public int CreateIndex(string tableName, string[] columnNames, bool unique = false)
			        {
			            return CreateIndex(tableName + "_" + string.Join ("_", columnNames), tableName, columnNames, unique);
			        }
			
			        /// <summary>
			        /// Creates an index for the specified object property.
			        /// e.g. CreateIndex<Client>(c => c.Name);
			        /// </summary>
			        /// <typeparam name="T">Type to reflect to a database table.</typeparam>
			        /// <param name="property">Property to index</param>
			        /// <param name="unique">Whether the index should be unique</param>
			        public void CreateIndex<T>(Expression<Func<T, object>> property, bool unique = false)
			        {
			            MemberExpression mx;
			            if (property.Body.NodeType == ExpressionType.Convert)
			            {
			                mx = ((UnaryExpression)property.Body).Operand as MemberExpression;
			            }
			            else
			            {
			                mx= (property.Body as MemberExpression);
			            }
			            var propertyInfo = mx.Member as PropertyInfo;
			            if (propertyInfo == null)
			            {
			                throw new ArgumentException("The lambda expression 'property' should point to a valid Property");
			            }
			
			            var propName = propertyInfo.Name;
			
			            var map = GetMapping<T>();
			            var colName = map.FindColumnWithPropertyName(propName).Name;
			
			            CreateIndex(map.TableName, colName, unique);
			        }
			
					public class ColumnInfo
					{
			//			public int cid { get; set; }
			
						[Column ("name")]
						public string Name { get; set; }
			
			//			[Column ("type")]
			//			public string ColumnType { get; set; }
			
						public int notnull { get; set; }
			
			//			public string dflt_value { get; set; }
			
			//			public int pk { get; set; }
			
						public override string ToString ()
						{
							return Name;
						}
					}
			
					public List<ColumnInfo> GetTableInfo (string tableName)
					{
						var query = "pragma table_info(\"" + tableName + "\")";			
						return Query<ColumnInfo> (query);
					}
			
					void MigrateTable (TableMapping map)
					{
						var existingCols = GetTableInfo (map.TableName);
						
						var toBeAdded = new List<TableMapping.Column> ();
						
						foreach (var p in map.Columns) {
							var found = false;
							foreach (var c in existingCols) {
								found = (string.Compare (p.Name, c.Name, StringComparison.OrdinalIgnoreCase) == 0);
								if (found)
									break;
							}
							if (!found) {
								toBeAdded.Add (p);
							}
						}
						
						foreach (var p in toBeAdded) {
							var addCol = "alter table \"" + map.TableName + "\" add column " + Orm.SqlDecl (p, StoreDateTimeAsTicks);
							Execute (addCol);
						}
					}
			
					/// <summary>
					/// Creates a new SQLiteCommand. Can be overridden to provide a sub-class.
					/// </summary>
					/// <seealso cref="SQLiteCommand.OnInstanceCreated"/>
					protected virtual SQLiteCommand NewCommand ()
					{
						return new SQLiteCommand (this);
					}
			
					/// <summary>
					/// Creates a new SQLiteCommand given the command text with arguments. Place a '?'
					/// in the command text for each of the arguments.
					/// </summary>
					/// <param name="cmdText">
					/// The fully escaped SQL.
					/// </param>
					/// <param name="args">
					/// Arguments to substitute for the occurences of '?' in the command text.
					/// </param>
					/// <returns>
					/// A <see cref="SQLiteCommand"/>
					/// </returns>
					public SQLiteCommand CreateCommand (string cmdText, params object[] ps)
					{
						if (!_open)
							throw SQLiteException.New (SQLite3.Result.Error, "Cannot create commands from unopened database");
			
						var cmd = NewCommand ();
						cmd.CommandText = cmdText;
						foreach (var o in ps) {
							cmd.Bind (o);
						}
						return cmd;
					}
			
					/// <summary>
					/// Creates a SQLiteCommand given the command text (SQL) with arguments. Place a '?'
					/// in the command text for each of the arguments and then executes that command.
					/// Use this method instead of Query when you don't expect rows back. Such cases include
					/// INSERTs, UPDATEs, and DELETEs.
					/// You can set the Trace or TimeExecution properties of the connection
					/// to profile execution.
					/// </summary>
					/// <param name="query">
					/// The fully escaped SQL.
					/// </param>
					/// <param name="args">
					/// Arguments to substitute for the occurences of '?' in the query.
					/// </param>
					/// <returns>
					/// The number of rows modified in the database as a result of this execution.
					/// </returns>
					public int Execute (string query, params object[] args)
					{
						var cmd = CreateCommand (query, args);
						
						if (TimeExecution) {
							if (_sw == null) {
								_sw = new Stopwatch ();
							}
							_sw.Reset ();
							_sw.Start ();
						}
			
						var r = cmd.ExecuteNonQuery ();
						
						if (TimeExecution) {
							_sw.Stop ();
							_elapsedMilliseconds += _sw.ElapsedMilliseconds;
							Debug.WriteLine (string.Format ("Finished in {0} ms ({1:0.0} s total)", _sw.ElapsedMilliseconds, _elapsedMilliseconds / 1000.0));
						}
						
						return r;
					}
			
					public T ExecuteScalar<T> (string query, params object[] args)
					{
						var cmd = CreateCommand (query, args);
						
						if (TimeExecution) {
							if (_sw == null) {
								_sw = new Stopwatch ();
							}
							_sw.Reset ();
							_sw.Start ();
						}
						
						var r = cmd.ExecuteScalar<T> ();
						
						if (TimeExecution) {
							_sw.Stop ();
							_elapsedMilliseconds += _sw.ElapsedMilliseconds;
							Debug.WriteLine (string.Format ("Finished in {0} ms ({1:0.0} s total)", _sw.ElapsedMilliseconds, _elapsedMilliseconds / 1000.0));
						}
						
						return r;
					}
			
					/// <summary>
					/// Creates a SQLiteCommand given the command text (SQL) with arguments. Place a '?'
					/// in the command text for each of the arguments and then executes that command.
					/// It returns each row of the result using the mapping automatically generated for
					/// the given type.
					/// </summary>
					/// <param name="query">
					/// The fully escaped SQL.
					/// </param>
					/// <param name="args">
					/// Arguments to substitute for the occurences of '?' in the query.
					/// </param>
					/// <returns>
					/// An enumerable with one result for each row returned by the query.
					/// </returns>
					public List<T> Query<T> (string query, params object[] args) where T : new()
					{
						var cmd = CreateCommand (query, args);
						return cmd.ExecuteQuery<T> ();
					}
			
					/// <summary>
					/// Creates a SQLiteCommand given the command text (SQL) with arguments. Place a '?'
					/// in the command text for each of the arguments and then executes that command.
					/// It returns each row of the result using the mapping automatically generated for
					/// the given type.
					/// </summary>
					/// <param name="query">
					/// The fully escaped SQL.
					/// </param>
					/// <param name="args">
					/// Arguments to substitute for the occurences of '?' in the query.
					/// </param>
					/// <returns>
					/// An enumerable with one result for each row returned by the query.
					/// The enumerator will call sqlite3_step on each call to MoveNext, so the database
					/// connection must remain open for the lifetime of the enumerator.
					/// </returns>
					public IEnumerable<T> DeferredQuery<T>(string query, params object[] args) where T : new()
					{
						var cmd = CreateCommand(query, args);
						return cmd.ExecuteDeferredQuery<T>();
					}
			
					/// <summary>
					/// Creates a SQLiteCommand given the command text (SQL) with arguments. Place a '?'
					/// in the command text for each of the arguments and then executes that command.
					/// It returns each row of the result using the specified mapping. This function is
					/// only used by libraries in order to query the database via introspection. It is
					/// normally not used.
					/// </summary>
					/// <param name="map">
					/// A <see cref="TableMapping"/> to use to convert the resulting rows
					/// into objects.
					/// </param>
					/// <param name="query">
					/// The fully escaped SQL.
					/// </param>
					/// <param name="args">
					/// Arguments to substitute for the occurences of '?' in the query.
					/// </param>
					/// <returns>
					/// An enumerable with one result for each row returned by the query.
					/// </returns>
					public List<object> Query (TableMapping map, string query, params object[] args)
					{
						var cmd = CreateCommand (query, args);
						return cmd.ExecuteQuery<object> (map);
					}
			
					/// <summary>
					/// Creates a SQLiteCommand given the command text (SQL) with arguments. Place a '?'
					/// in the command text for each of the arguments and then executes that command.
					/// It returns each row of the result using the specified mapping. This function is
					/// only used by libraries in order to query the database via introspection. It is
					/// normally not used.
					/// </summary>
					/// <param name="map">
					/// A <see cref="TableMapping"/> to use to convert the resulting rows
					/// into objects.
					/// </param>
					/// <param name="query">
					/// The fully escaped SQL.
					/// </param>
					/// <param name="args">
					/// Arguments to substitute for the occurences of '?' in the query.
					/// </param>
					/// <returns>
					/// An enumerable with one result for each row returned by the query.
					/// The enumerator will call sqlite3_step on each call to MoveNext, so the database
					/// connection must remain open for the lifetime of the enumerator.
					/// </returns>
					public IEnumerable<object> DeferredQuery(TableMapping map, string query, params object[] args)
					{
						var cmd = CreateCommand(query, args);
						return cmd.ExecuteDeferredQuery<object>(map);
					}
			
					/// <summary>
					/// Returns a queryable interface to the table represented by the given type.
					/// </summary>
					/// <returns>
					/// A queryable object that is able to translate Where, OrderBy, and Take
					/// queries into native SQL.
					/// </returns>
					public TableQuery<T> Table<T> () where T : new()
					{
						return new TableQuery<T> (this);
					}
			
					/// <summary>
					/// Attempts to retrieve an object with the given primary key from the table
					/// associated with the specified type. Use of this method requires that
					/// the given type have a designated PrimaryKey (using the PrimaryKeyAttribute).
					/// </summary>
					/// <param name="pk">
					/// The primary key.
					/// </param>
					/// <returns>
					/// The object with the given primary key. Throws a not found exception
					/// if the object is not found.
					/// </returns>
					public T Get<T> (object pk) where T : new()
					{
						var map = GetMapping (typeof(T));
						return Query<T> (map.GetByPrimaryKeySql, pk).First ();
					}
			
			        /// <summary>
			        /// Attempts to retrieve the first object that matches the predicate from the table
			        /// associated with the specified type. 
			        /// </summary>
			        /// <param name="predicate">
			        /// A predicate for which object to find.
			        /// </param>
			        /// <returns>
			        /// The object that matches the given predicate. Throws a not found exception
			        /// if the object is not found.
			        /// </returns>
			        public T Get<T> (Expression<Func<T, bool>> predicate) where T : new()
			        {
			            return Table<T> ().Where (predicate).First ();
			        }
			
					/// <summary>
					/// Attempts to retrieve an object with the given primary key from the table
					/// associated with the specified type. Use of this method requires that
					/// the given type have a designated PrimaryKey (using the PrimaryKeyAttribute).
					/// </summary>
					/// <param name="pk">
					/// The primary key.
					/// </param>
					/// <returns>
					/// The object with the given primary key or null
					/// if the object is not found.
					/// </returns>
					public T Find<T> (object pk) where T : new ()
					{
						var map = GetMapping (typeof (T));
						return Query<T> (map.GetByPrimaryKeySql, pk).FirstOrDefault ();
					}
			
					/// <summary>
					/// Attempts to retrieve an object with the given primary key from the table
					/// associated with the specified type. Use of this method requires that
					/// the given type have a designated PrimaryKey (using the PrimaryKeyAttribute).
					/// </summary>
					/// <param name="pk">
					/// The primary key.
					/// </param>
					/// <param name="map">
					/// The TableMapping used to identify the object type.
					/// </param>
					/// <returns>
					/// The object with the given primary key or null
					/// if the object is not found.
					/// </returns>
					public object Find (object pk, TableMapping map)
					{
						return Query (map, map.GetByPrimaryKeySql, pk).FirstOrDefault ();
					}
					
					/// <summary>
			        /// Attempts to retrieve the first object that matches the predicate from the table
			        /// associated with the specified type. 
			        /// </summary>
			        /// <param name="predicate">
			        /// A predicate for which object to find.
			        /// </param>
			        /// <returns>
			        /// The object that matches the given predicate or null
			        /// if the object is not found.
			        /// </returns>
			        public T Find<T> (Expression<Func<T, bool>> predicate) where T : new()
			        {
			            return Table<T> ().Where (predicate).FirstOrDefault ();
			        }
			
					/// <summary>
					/// Attempts to retrieve the first object that matches the query from the table
					/// associated with the specified type. 
					/// </summary>
					/// <param name="query">
					/// The fully escaped SQL.
					/// </param>
					/// <param name="args">
					/// Arguments to substitute for the occurences of '?' in the query.
					/// </param>
					/// <returns>
					/// The object that matches the given predicate or null
					/// if the object is not found.
					/// </returns>
					public T FindWithQuery<T> (string query, params object[] args) where T : new()
					{
						return Query<T> (query, args).FirstOrDefault ();
					}
			
					/// <summary>
					/// Whether <see cref="BeginTransaction"/> has been called and the database is waiting for a <see cref="Commit"/>.
					/// </summary>
					public bool IsInTransaction {
						get { return _transactionDepth > 0; }
					}
			
					/// <summary>
					/// Begins a new transaction. Call <see cref="Commit"/> to end the transaction.
					/// </summary>
					/// <example cref="System.InvalidOperationException">Throws if a transaction has already begun.</example>
					public void BeginTransaction ()
					{
						// The BEGIN command only works if the transaction stack is empty, 
						//    or in other words if there are no pending transactions. 
						// If the transaction stack is not empty when the BEGIN command is invoked, 
						//    then the command fails with an error.
						// Rather than crash with an error, we will just ignore calls to BeginTransaction
						//    that would result in an error.
						if (Interlocked.CompareExchange (ref _transactionDepth, 1, 0) == 0) {
							try {
								Execute ("begin transaction");
							} catch (Exception ex) {
								var sqlExp = ex as SQLiteException;
								if (sqlExp != null) {
									// It is recommended that applications respond to the errors listed below 
									//    by explicitly issuing a ROLLBACK command.
									// TODO: This rollback failsafe should be localized to all throw sites.
									switch (sqlExp.Result) {
									case SQLite3.Result.IOError:
									case SQLite3.Result.Full:
									case SQLite3.Result.Busy:
									case SQLite3.Result.NoMem:
									case SQLite3.Result.Interrupt:
										RollbackTo (null, true);
										break;
									}
								} else {
									// Call decrement and not VolatileWrite in case we've already 
									//    created a transaction point in SaveTransactionPoint since the catch.
									Interlocked.Decrement (ref _transactionDepth);
								}
			
								throw;
							}
						} else { 
							// Calling BeginTransaction on an already open transaction is invalid
							throw new InvalidOperationException ("Cannot begin a transaction while already in a transaction.");
						}
					}
			
					/// <summary>
					/// Creates a savepoint in the database at the current point in the transaction timeline.
					/// Begins a new transaction if one is not in progress.
					/// 
					/// Call <see cref="RollbackTo"/> to undo transactions since the returned savepoint.
					/// Call <see cref="Release"/> to commit transactions after the savepoint returned here.
					/// Call <see cref="Commit"/> to end the transaction, committing all changes.
					/// </summary>
					/// <returns>A string naming the savepoint.</returns>
					public string SaveTransactionPoint ()
					{
						int depth = Interlocked.Increment (ref _transactionDepth) - 1;
						string retVal = "S" + _rand.Next (short.MaxValue) + "D" + depth;
			
						try {
							Execute ("savepoint " + retVal);
						} catch (Exception ex) {
							var sqlExp = ex as SQLiteException;
							if (sqlExp != null) {
								// It is recommended that applications respond to the errors listed below 
								//    by explicitly issuing a ROLLBACK command.
								// TODO: This rollback failsafe should be localized to all throw sites.
								switch (sqlExp.Result) {
								case SQLite3.Result.IOError:
								case SQLite3.Result.Full:
								case SQLite3.Result.Busy:
								case SQLite3.Result.NoMem:
								case SQLite3.Result.Interrupt:
									RollbackTo (null, true);
									break;
								}
							} else {
								Interlocked.Decrement (ref _transactionDepth);
							}
			
							throw;
						}
			
						return retVal;
					}
			
					/// <summary>
					/// Rolls back the transaction that was begun by <see cref="BeginTransaction"/> or <see cref="SaveTransactionPoint"/>.
					/// </summary>
					public void Rollback ()
					{
						RollbackTo (null, false);
					}
			
					/// <summary>
					/// Rolls back the savepoint created by <see cref="BeginTransaction"/> or SaveTransactionPoint.
					/// </summary>
					/// <param name="savepoint">The name of the savepoint to roll back to, as returned by <see cref="SaveTransactionPoint"/>.  If savepoint is null or empty, this method is equivalent to a call to <see cref="Rollback"/></param>
					public void RollbackTo (string savepoint)
					{
						RollbackTo (savepoint, false);
					}
			
					/// <summary>
					/// Rolls back the transaction that was begun by <see cref="BeginTransaction"/>.
					/// </summary>
					/// <param name="noThrow">true to avoid throwing exceptions, false otherwise</param>
					void RollbackTo (string savepoint, bool noThrow)
					{
						// Rolling back without a TO clause rolls backs all transactions 
						//    and leaves the transaction stack empty.   
						try {
							if (String.IsNullOrEmpty (savepoint)) {
								if (Interlocked.Exchange (ref _transactionDepth, 0) > 0) {
									Execute ("rollback");
								}
							} else {
								DoSavePointExecute (savepoint, "rollback to ");
							}   
						} catch (SQLiteException) {
							if (!noThrow)
								throw;
			            
						}
						// No need to rollback if there are no transactions open.
					}
			
					/// <summary>
					/// Releases a savepoint returned from <see cref="SaveTransactionPoint"/>.  Releasing a savepoint 
					///    makes changes since that savepoint permanent if the savepoint began the transaction,
					///    or otherwise the changes are permanent pending a call to <see cref="Commit"/>.
					/// 
					/// The RELEASE command is like a COMMIT for a SAVEPOINT.
					/// </summary>
					/// <param name="savepoint">The name of the savepoint to release.  The string should be the result of a call to <see cref="SaveTransactionPoint"/></param>
					public void Release (string savepoint)
					{
						DoSavePointExecute (savepoint, "release ");
					}
			
					void DoSavePointExecute (string savepoint, string cmd)
					{
						// Validate the savepoint
						int firstLen = savepoint.IndexOf ('D');
						if (firstLen >= 2 && savepoint.Length > firstLen + 1) {
							int depth;
							if (Int32.TryParse (savepoint.Substring (firstLen + 1), out depth)) {
								// TODO: Mild race here, but inescapable without locking almost everywhere.
								if (0 <= depth && depth < _transactionDepth) {
			#if NETFX_CORE || USE_SQLITEPCL_RAW
			                        Volatile.Write (ref _transactionDepth, depth);
			#elif SILVERLIGHT
									_transactionDepth = depth;
			#else
			                        Thread.VolatileWrite (ref _transactionDepth, depth);
			#endif
			                        Execute (cmd + savepoint);
									return;
								}
							}
						}
			
						throw new ArgumentException ("savePoint is not valid, and should be the result of a call to SaveTransactionPoint.", "savePoint");
					}
			
					/// <summary>
					/// Commits the transaction that was begun by <see cref="BeginTransaction"/>.
					/// </summary>
					public void Commit ()
					{
						if (Interlocked.Exchange (ref _transactionDepth, 0) != 0) {
							Execute ("commit");
						}
						// Do nothing on a commit with no open transaction
					}
			
					/// <summary>
					/// Executes <param name="action"> within a (possibly nested) transaction by wrapping it in a SAVEPOINT. If an
					/// exception occurs the whole transaction is rolled back, not just the current savepoint. The exception
					/// is rethrown.
					/// </summary>
					/// <param name="action">
					/// The <see cref="Action"/> to perform within a transaction. <param name="action"> can contain any number
					/// of operations on the connection but should never call <see cref="BeginTransaction"/> or
					/// <see cref="Commit"/>.
					/// </param>
					public void RunInTransaction (Action action)
					{
						try {
							var savePoint = SaveTransactionPoint ();
							action ();
							Release (savePoint);
						} catch (Exception) {
							Rollback ();
							throw;
						}
					}
			
					/// <summary>
					/// Inserts all specified objects.
					/// </summary>
					/// <param name="objects">
					/// An <see cref="IEnumerable"/> of the objects to insert.
					/// <param name="runInTransaction"/>
					/// A boolean indicating if the inserts should be wrapped in a transaction.
					/// </param>
					/// <returns>
					/// The number of rows added to the table.
					/// </returns>
					public int InsertAll (System.Collections.IEnumerable objects, bool runInTransaction=true)
					{
						var c = 0;
						if (runInTransaction) {
							RunInTransaction(() => {
								foreach (var r in objects) {
									c += Insert (r);
								}
							});
						}
						else {
							foreach (var r in objects) {
								c += Insert (r);
							}
						}
						return c;
					}
			
					/// <summary>
					/// Inserts all specified objects.
					/// </summary>
					/// <param name="objects">
					/// An <see cref="IEnumerable"/> of the objects to insert.
					/// </param>
					/// <param name="extra">
					/// Literal SQL code that gets placed into the command. INSERT {extra} INTO ...
					/// </param>
					/// <param name="runInTransaction"/>
					/// A boolean indicating if the inserts should be wrapped in a transaction.
					/// </param>
					/// <returns>
					/// The number of rows added to the table.
					/// </returns>
					public int InsertAll (System.Collections.IEnumerable objects, string extra, bool runInTransaction=true)
					{
						var c = 0;
						if (runInTransaction) {
							RunInTransaction (() => {
								foreach (var r in objects) {
									c += Insert (r, extra);
								}
							});
						}
						else {
							foreach (var r in objects) {
								c+= Insert (r);
							}
						}
						return c;
					}
			
					/// <summary>
					/// Inserts all specified objects.
					/// </summary>
					/// <param name="objects">
					/// An <see cref="IEnumerable"/> of the objects to insert.
					/// </param>
					/// <param name="objType">
					/// The type of object to insert.
					/// </param>
					/// <param name="runInTransaction"/>
					/// A boolean indicating if the inserts should be wrapped in a transaction.
					/// </param>
					/// <returns>
					/// The number of rows added to the table.
					/// </returns>
					public int InsertAll (System.Collections.IEnumerable objects, Type objType, bool runInTransaction=true)
					{
						var c = 0;
						if (runInTransaction) {
							RunInTransaction (() => {
								foreach (var r in objects) {
									c += Insert (r, objType);
								}
							});
						}
						else {
							foreach (var r in objects) {
								c += Insert (r, objType);
							}
						}
						return c;
					}
					
					/// <summary>
					/// Inserts the given object and retrieves its
					/// auto incremented primary key if it has one.
					/// </summary>
					/// <param name="obj">
					/// The object to insert.
					/// </param>
					/// <returns>
					/// The number of rows added to the table.
					/// </returns>
					public int Insert (object obj)
					{
						if (obj == null) {
							return 0;
						}
						return Insert (obj, "", obj.GetType ());
					}
			
					/// <summary>
					/// Inserts the given object and retrieves its
					/// auto incremented primary key if it has one.
					/// If a UNIQUE constraint violation occurs with
					/// some pre-existing object, this function deletes
					/// the old object.
					/// </summary>
					/// <param name="obj">
					/// The object to insert.
					/// </param>
					/// <returns>
					/// The number of rows modified.
					/// </returns>
					public int InsertOrReplace (object obj)
					{
						if (obj == null) {
							return 0;
						}
						return Insert (obj, "OR REPLACE", obj.GetType ());
					}
			
					/// <summary>
					/// Inserts the given object and retrieves its
					/// auto incremented primary key if it has one.
					/// </summary>
					/// <param name="obj">
					/// The object to insert.
					/// </param>
					/// <param name="objType">
					/// The type of object to insert.
					/// </param>
					/// <returns>
					/// The number of rows added to the table.
					/// </returns>
					public int Insert (object obj, Type objType)
					{
						return Insert (obj, "", objType);
					}
			
					/// <summary>
					/// Inserts the given object and retrieves its
					/// auto incremented primary key if it has one.
					/// If a UNIQUE constraint violation occurs with
					/// some pre-existing object, this function deletes
					/// the old object.
					/// </summary>
					/// <param name="obj">
					/// The object to insert.
					/// </param>
					/// <param name="objType">
					/// The type of object to insert.
					/// </param>
					/// <returns>
					/// The number of rows modified.
					/// </returns>
					public int InsertOrReplace (object obj, Type objType)
					{
						return Insert (obj, "OR REPLACE", objType);
					}
					
					/// <summary>
					/// Inserts the given object and retrieves its
					/// auto incremented primary key if it has one.
					/// </summary>
					/// <param name="obj">
					/// The object to insert.
					/// </param>
					/// <param name="extra">
					/// Literal SQL code that gets placed into the command. INSERT {extra} INTO ...
					/// </param>
					/// <returns>
					/// The number of rows added to the table.
					/// </returns>
					public int Insert (object obj, string extra)
					{
						if (obj == null) {
							return 0;
						}
						return Insert (obj, extra, obj.GetType ());
					}
			
				    /// <summary>
				    /// Inserts the given object and retrieves its
				    /// auto incremented primary key if it has one.
				    /// </summary>
				    /// <param name="obj">
				    /// The object to insert.
				    /// </param>
				    /// <param name="extra">
				    /// Literal SQL code that gets placed into the command. INSERT {extra} INTO ...
				    /// </param>
				    /// <param name="objType">
				    /// The type of object to insert.
				    /// </param>
				    /// <returns>
				    /// The number of rows added to the table.
				    /// </returns>
				    public int Insert (object obj, string extra, Type objType)
					{
						if (obj == null || objType == null) {
							return 0;
						}
						
			            
						var map = GetMapping (objType);
			
			#if USE_NEW_REFLECTION_API
			            if (map.PK != null && map.PK.IsAutoGuid)
			            {
			                // no GetProperty so search our way up the inheritance chain till we find it
			                PropertyInfo prop;
			                while (objType != null)
			                {
			                    var info = objType.GetTypeInfo();
			                    prop = info.GetDeclaredProperty(map.PK.PropertyName);
			                    if (prop != null) 
			                    {
			                        if (prop.GetValue(obj, null).Equals(Guid.Empty))
			                        {
			                            prop.SetValue(obj, Guid.NewGuid(), null);
			                        }
			                        break; 
			                    }
			
			                    objType = info.BaseType;
			                }
			            }
			#else
			            if (map.PK != null && map.PK.IsAutoGuid) {
			                var prop = objType.GetProperty(map.PK.PropertyName);
			                if (prop != null) {
			                    if (prop.GetValue(obj, null).Equals(Guid.Empty)) {
			                        prop.SetValue(obj, Guid.NewGuid(), null);
			                    }
			                }
			            }
			#endif
			
			
						var replacing = string.Compare (extra, "OR REPLACE", StringComparison.OrdinalIgnoreCase) == 0;
						
						var cols = replacing ? map.InsertOrReplaceColumns : map.InsertColumns;
						var vals = new object[cols.Length];
						for (var i = 0; i < vals.Length; i++) {
							vals [i] = cols [i].GetValue (obj);
						}
						
						var insertCmd = map.GetInsertCommand (this, extra);
						int count;
			
						lock (insertCmd) {
							// We lock here to protect the prepared statement returned via GetInsertCommand.
							// A SQLite prepared statement can be bound for only one operation at a time.
							try {
								count = insertCmd.ExecuteNonQuery (vals);
							} catch (SQLiteException ex) {
								if (SQLite3.ExtendedErrCode (this.Handle) == SQLite3.ExtendedResult.ConstraintNotNull) {
									throw NotNullConstraintViolationException.New (ex.Result, ex.Message, map, obj);
								}
								throw;
							}
			
							if (map.HasAutoIncPK) {
								var id = SQLite3.LastInsertRowid (Handle);
								map.SetAutoIncPK (obj, id);
							}
						}
						if (count > 0)
							OnTableChanged (map, NotifyTableChangedAction.Insert);
			
						return count;
					}
			
					/// <summary>
					/// Updates all of the columns of a table using the specified object
					/// except for its primary key.
					/// The object is required to have a primary key.
					/// </summary>
					/// <param name="obj">
					/// The object to update. It must have a primary key designated using the PrimaryKeyAttribute.
					/// </param>
					/// <returns>
					/// The number of rows updated.
					/// </returns>
					public int Update (object obj)
					{
						if (obj == null) {
							return 0;
						}
						return Update (obj, obj.GetType ());
					}
			
					/// <summary>
					/// Updates all of the columns of a table using the specified object
					/// except for its primary key.
					/// The object is required to have a primary key.
					/// </summary>
					/// <param name="obj">
					/// The object to update. It must have a primary key designated using the PrimaryKeyAttribute.
					/// </param>
					/// <param name="objType">
					/// The type of object to insert.
					/// </param>
					/// <returns>
					/// The number of rows updated.
					/// </returns>
					public int Update (object obj, Type objType)
					{
						int rowsAffected = 0;
						if (obj == null || objType == null) {
							return 0;
						}
						
						var map = GetMapping (objType);
						
						var pk = map.PK;
						
						if (pk == null) {
							throw new NotSupportedException ("Cannot update " + map.TableName + ": it has no PK");
						}
						
						var cols = from p in map.Columns
							where p != pk
							select p;
						var vals = from c in cols
							select c.GetValue (obj);
						var ps = new List<object> (vals);
						ps.Add (pk.GetValue (obj));
						var q = string.Format ("update \"{0}\" set {1} where {2} = ? ", map.TableName, string.Join (",", (from c in cols
							select "\"" + c.Name + "\" = ? ").ToArray ()), pk.Name);
			
						try {
							rowsAffected = Execute (q, ps.ToArray ());
						}
						catch (SQLiteException ex) {
			
							if (ex.Result == SQLite3.Result.Constraint && SQLite3.ExtendedErrCode (this.Handle) == SQLite3.ExtendedResult.ConstraintNotNull) {
								throw NotNullConstraintViolationException.New (ex, map, obj);
							}
			
							throw ex;
						}
			
						if (rowsAffected > 0)
							OnTableChanged (map, NotifyTableChangedAction.Update);
			
						return rowsAffected;
					}
			
					/// <summary>
					/// Updates all specified objects.
					/// </summary>
					/// <param name="objects">
					/// An <see cref="IEnumerable"/> of the objects to insert.
					/// </param>
					/// <param name="runInTransaction"/>
					/// A boolean indicating if the inserts should be wrapped in a transaction
					/// </param>
					/// <returns>
					/// The number of rows modified.
					/// </returns>
					public int UpdateAll (System.Collections.IEnumerable objects, bool runInTransaction=true)
					{
						var c = 0;
						if (runInTransaction) {
							RunInTransaction (() => {
								foreach (var r in objects) {
									c += Update (r);
								}
							});
						}
						else {
							foreach (var r in objects) {
								c += Update (r);
							}
						}
						return c;
					}
			
					/// <summary>
					/// Deletes the given object from the database using its primary key.
					/// </summary>
					/// <param name="objectToDelete">
					/// The object to delete. It must have a primary key designated using the PrimaryKeyAttribute.
					/// </param>
					/// <returns>
					/// The number of rows deleted.
					/// </returns>
					public int Delete (object objectToDelete)
					{
						var map = GetMapping (objectToDelete.GetType ());
						var pk = map.PK;
						if (pk == null) {
							throw new NotSupportedException ("Cannot delete " + map.TableName + ": it has no PK");
						}
						var q = string.Format ("delete from \"{0}\" where \"{1}\" = ?", map.TableName, pk.Name);
						var count = Execute (q, pk.GetValue (objectToDelete));
						if (count > 0)
							OnTableChanged (map, NotifyTableChangedAction.Delete);
						return count;
					}
			
					/// <summary>
					/// Deletes the object with the specified primary key.
					/// </summary>
					/// <param name="primaryKey">
					/// The primary key of the object to delete.
					/// </param>
					/// <returns>
					/// The number of objects deleted.
					/// </returns>
					/// <typeparam name='T'>
					/// The type of object.
					/// </typeparam>
					public int Delete<T> (object primaryKey)
					{
						var map = GetMapping (typeof (T));
						var pk = map.PK;
						if (pk == null) {
							throw new NotSupportedException ("Cannot delete " + map.TableName + ": it has no PK");
						}
						var q = string.Format ("delete from \"{0}\" where \"{1}\" = ?", map.TableName, pk.Name);
						var count = Execute (q, primaryKey);
						if (count > 0)
							OnTableChanged (map, NotifyTableChangedAction.Delete);
						return count;
					}
			
					/// <summary>
					/// Deletes all the objects from the specified table.
					/// WARNING WARNING: Let me repeat. It deletes ALL the objects from the
					/// specified table. Do you really want to do that?
					/// </summary>
					/// <returns>
					/// The number of objects deleted.
					/// </returns>
					/// <typeparam name='T'>
					/// The type of objects to delete.
					/// </typeparam>
					public int DeleteAll<T> ()
					{
						var map = GetMapping (typeof (T));
						var query = string.Format("delete from \"{0}\"", map.TableName);
						var count = Execute (query);
						if (count > 0)
							OnTableChanged (map, NotifyTableChangedAction.Delete);
						return count;
					}
			
					~SQLiteConnection ()
					{
						Dispose (false);
					}
			
					public void Dispose ()
					{
						Dispose(true);
						GC.SuppressFinalize(this);
					}
			
					public void Close()
					{
						Dispose(true);
					}
			
					protected virtual void Dispose(bool disposing)
					{
						if (_open && Handle != NullHandle) {
							try {
								if (disposing) {
									if (_mappings != null) {
										foreach (var sqlInsertCommand in _mappings.Values){
											sqlInsertCommand.Dispose();
										}
									}
			
									var r = SQLite3.Close(Handle);
									if (r != SQLite3.Result.OK)
									{
										string msg = SQLite3.GetErrmsg(Handle);
										throw SQLiteException.New(r, msg);
									}
								} else {
									SQLite3.Close2(Handle);
								}
							}
							finally {
								Handle = NullHandle;
								_open = false;
							}
						}
					}
			
					void OnTableChanged (TableMapping table, NotifyTableChangedAction action)
					{
						var ev = TableChanged;
						if (ev != null)
							ev (this, new NotifyTableChangedEventArgs (table, action));
					}
			
					public event EventHandler<NotifyTableChangedEventArgs> TableChanged;
				}
			
				public class NotifyTableChangedEventArgs : EventArgs
				{
					public TableMapping Table { get; private set; }
					public NotifyTableChangedAction Action { get; private set; }
			
					public NotifyTableChangedEventArgs (TableMapping table, NotifyTableChangedAction action)
					{
						Table = table;
						Action = action;		
					}
				}
			
				public enum NotifyTableChangedAction
				{
					Insert,
					Update,
					Delete,
				}
			
				/// <summary>
				/// Represents a parsed connection string.
				/// </summary>
				class SQLiteConnectionString
				{
					public string ConnectionString { get; private set; }
					public string DatabasePath { get; private set; }
					public bool StoreDateTimeAsTicks { get; private set; }
			
			#if NETFX_CORE
					static readonly string MetroStyleDataPath = Windows.Storage.ApplicationData.Current.LocalFolder.Path;
			#endif
			
					public SQLiteConnectionString (string databasePath, bool storeDateTimeAsTicks)
					{
						ConnectionString = databasePath;
						StoreDateTimeAsTicks = storeDateTimeAsTicks;
			
			#if NETFX_CORE
						DatabasePath = System.IO.Path.Combine (MetroStyleDataPath, databasePath);
			#else
						DatabasePath = databasePath;
			#endif
					}
				}
			
			    [AttributeUsage (AttributeTargets.Class)]
				public class TableAttribute : Attribute
				{
					public string Name { get; set; }
			
					public TableAttribute (string name)
					{
						Name = name;
					}
				}
			
				[AttributeUsage (AttributeTargets.Property)]
				public class ColumnAttribute : Attribute
				{
					public string Name { get; set; }
			
					public ColumnAttribute (string name)
					{
						Name = name;
					}
				}
			
				[AttributeUsage (AttributeTargets.Property)]
				public class PrimaryKeyAttribute : Attribute
				{
				}
			
				[AttributeUsage (AttributeTargets.Property)]
				public class AutoIncrementAttribute : Attribute
				{
				}
			
				[AttributeUsage (AttributeTargets.Property)]
				public class IndexedAttribute : Attribute
				{
					public string Name { get; set; }
					public int Order { get; set; }
					public virtual bool Unique { get; set; }
					
					public IndexedAttribute()
					{
					}
					
					public IndexedAttribute(string name, int order)
					{
						Name = name;
						Order = order;
					}
				}
			
				[AttributeUsage (AttributeTargets.Property)]
				public class IgnoreAttribute : Attribute
				{
				}
			
				[AttributeUsage (AttributeTargets.Property)]
				public class UniqueAttribute : IndexedAttribute
				{
					public override bool Unique {
						get { return true; }
						set { /* throw?  */ }
					}
				}
			
				[AttributeUsage (AttributeTargets.Property)]
				public class MaxLengthAttribute : Attribute
				{
					public int Value { get; private set; }
			
					public MaxLengthAttribute (int length)
					{
						Value = length;
					}
				}
			
				[AttributeUsage (AttributeTargets.Property)]
				public class CollationAttribute: Attribute
				{
					public string Value { get; private set; }
			
					public CollationAttribute (string collation)
					{
						Value = collation;
					}
				}
			
				[AttributeUsage (AttributeTargets.Property)]
				public class NotNullAttribute : Attribute
				{
				}
			
				public class TableMapping
				{
					public Type MappedType { get; private set; }
			
					public string TableName { get; private set; }
			
					public Column[] Columns { get; private set; }
			
					public Column PK { get; private set; }
			
					public string GetByPrimaryKeySql { get; private set; }
			
					Column _autoPk;
					Column[] _insertColumns;
					Column[] _insertOrReplaceColumns;
			
			        public TableMapping(Type type, CreateFlags createFlags = CreateFlags.None)
					{
						MappedType = type;
			
			#if USE_NEW_REFLECTION_API
						var tableAttr = (TableAttribute)System.Reflection.CustomAttributeExtensions
			                .GetCustomAttribute(type.GetTypeInfo(), typeof(TableAttribute), true);
			#else
						var tableAttr = (TableAttribute)type.GetCustomAttributes (typeof (TableAttribute), true).FirstOrDefault ();
			#endif
			
						TableName = tableAttr != null ? tableAttr.Name : MappedType.Name;
			
			#if !USE_NEW_REFLECTION_API
						var props = MappedType.GetProperties (BindingFlags.Public | BindingFlags.Instance | BindingFlags.SetProperty);
			#else
						var props = from p in MappedType.GetRuntimeProperties()
									where ((p.GetMethod != null && p.GetMethod.IsPublic) || (p.SetMethod != null && p.SetMethod.IsPublic) || (p.GetMethod != null && p.GetMethod.IsStatic) || (p.SetMethod != null && p.SetMethod.IsStatic))
									select p;
			#endif
						var cols = new List<Column> ();
						foreach (var p in props) {
			#if !USE_NEW_REFLECTION_API
							var ignore = p.GetCustomAttributes (typeof(IgnoreAttribute), true).Length > 0;
			#else
							var ignore = p.GetCustomAttributes (typeof(IgnoreAttribute), true).Count() > 0;
			#endif
							if (p.CanWrite && !ignore) {
								cols.Add (new Column (p, createFlags));
							}
						}
						Columns = cols.ToArray ();
						foreach (var c in Columns) {
							if (c.IsAutoInc && c.IsPK) {
								_autoPk = c;
							}
							if (c.IsPK) {
								PK = c;
							}
						}
						
						HasAutoIncPK = _autoPk != null;
			
						if (PK != null) {
							GetByPrimaryKeySql = string.Format ("select * from \"{0}\" where \"{1}\" = ?", TableName, PK.Name);
						}
						else {
							// People should not be calling Get/Find without a PK
							GetByPrimaryKeySql = string.Format ("select * from \"{0}\" limit 1", TableName);
						}
						_insertCommandMap = new ConcurrentStringDictionary ();
					}
			
					public bool HasAutoIncPK { get; private set; }
			
					public void SetAutoIncPK (object obj, long id)
					{
						if (_autoPk != null) {
							_autoPk.SetValue (obj, Convert.ChangeType (id, _autoPk.ColumnType, null));
						}
					}
			
					public Column[] InsertColumns {
						get {
							if (_insertColumns == null) {
								_insertColumns = Columns.Where (c => !c.IsAutoInc).ToArray ();
							}
							return _insertColumns;
						}
					}
			
					public Column[] InsertOrReplaceColumns {
						get {
							if (_insertOrReplaceColumns == null) {
								_insertOrReplaceColumns = Columns.ToArray ();
							}
							return _insertOrReplaceColumns;
						}
					}
			
					public Column FindColumnWithPropertyName (string propertyName)
					{
						var exact = Columns.FirstOrDefault (c => c.PropertyName == propertyName);
						return exact;
					}
			
					public Column FindColumn (string columnName)
					{
						var exact = Columns.FirstOrDefault (c => c.Name == columnName);
						return exact;
					}
			
			        ConcurrentStringDictionary _insertCommandMap;
			
					public PreparedSqlLiteInsertCommand GetInsertCommand(SQLiteConnection conn, string extra)
					{
						object prepCmdO;
			            
						if (!_insertCommandMap.TryGetValue (extra, out prepCmdO)) {
							var prepCmd = CreateInsertCommand (conn, extra);
							prepCmdO = prepCmd;
							if (!_insertCommandMap.TryAdd (extra, prepCmd)) {
								// Concurrent add attempt beat us.
								prepCmd.Dispose ();
								_insertCommandMap.TryGetValue (extra, out prepCmdO);
							}
						}
						return (PreparedSqlLiteInsertCommand)prepCmdO;
					}
					
					PreparedSqlLiteInsertCommand CreateInsertCommand(SQLiteConnection conn, string extra)
					{
						var cols = InsertColumns;
					    string insertSql;
			            if (!cols.Any() && Columns.Count() == 1 && Columns[0].IsAutoInc)
			            {
			                insertSql = string.Format("insert {1} into \"{0}\" default values", TableName, extra);
			            }
			            else
			            {
							var replacing = string.Compare (extra, "OR REPLACE", StringComparison.OrdinalIgnoreCase) == 0;
			
							if (replacing) {
								cols = InsertOrReplaceColumns;
							}
			
			                insertSql = string.Format("insert {3} into \"{0}\"({1}) values ({2})", TableName,
			                                   string.Join(",", (from c in cols
			                                                     select "\"" + c.Name + "\"").ToArray()),
			                                   string.Join(",", (from c in cols
			                                                     select "?").ToArray()), extra);
			                
			            }
						
						var insertCommand = new PreparedSqlLiteInsertCommand(conn);
						insertCommand.CommandText = insertSql;
						return insertCommand;
					}
					
					protected internal void Dispose()
					{
						foreach (var pair in _insertCommandMap) {
			                ((PreparedSqlLiteInsertCommand)pair.Value).Dispose ();
						}
						_insertCommandMap = null;
					}
			
					public class Column
					{
						PropertyInfo _prop;
			
						public string Name { get; private set; }
			
						public string PropertyName { get { return _prop.Name; } }
			
						public Type ColumnType { get; private set; }
			
						public string Collation { get; private set; }
			
			            public bool IsAutoInc { get; private set; }
			            public bool IsAutoGuid { get; private set; }
			
						public bool IsPK { get; private set; }
			
						public IEnumerable<IndexedAttribute> Indices { get; set; }
			
						public bool IsNullable { get; private set; }
			
						public int? MaxStringLength { get; private set; }
			
			            public Column(PropertyInfo prop, CreateFlags createFlags = CreateFlags.None)
			            {
			                var colAttr = (ColumnAttribute)prop.GetCustomAttributes(typeof(ColumnAttribute), true).FirstOrDefault();
			
			                _prop = prop;
			                Name = colAttr == null ? prop.Name : colAttr.Name;
			                //If this type is Nullable<T> then Nullable.GetUnderlyingType returns the T, otherwise it returns null, so get the actual type instead
			                ColumnType = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
			                Collation = Orm.Collation(prop);
			
			                IsPK = Orm.IsPK(prop) ||
								(((createFlags & CreateFlags.ImplicitPK) == CreateFlags.ImplicitPK) &&
								 	string.Compare (prop.Name, Orm.ImplicitPkName, StringComparison.OrdinalIgnoreCase) == 0);
			
			                var isAuto = Orm.IsAutoInc(prop) || (IsPK && ((createFlags & CreateFlags.AutoIncPK) == CreateFlags.AutoIncPK));
			                IsAutoGuid = isAuto && ColumnType == typeof(Guid);
			                IsAutoInc = isAuto && !IsAutoGuid;
			
			                Indices = Orm.GetIndices(prop);
			                if (!Indices.Any()
			                    && !IsPK
			                    && ((createFlags & CreateFlags.ImplicitIndex) == CreateFlags.ImplicitIndex)
			                    && Name.EndsWith (Orm.ImplicitIndexSuffix, StringComparison.OrdinalIgnoreCase)
			                    )
			                {
			                    Indices = new IndexedAttribute[] { new IndexedAttribute() };
			                }
			                IsNullable = !(IsPK || Orm.IsMarkedNotNull(prop));
			                MaxStringLength = Orm.MaxStringLength(prop);
			            }
			
						public void SetValue (object obj, object val)
						{
							_prop.SetValue (obj, val, null);
						}
			
						public object GetValue (object obj)
						{
							return _prop.GetValue (obj, null);
						}
					}
				}
			
				public static class Orm
				{
			        public const int DefaultMaxStringLength = 140;
			        public const string ImplicitPkName = "Id";
			        public const string ImplicitIndexSuffix = "Id";
			
					public static string SqlDecl (TableMapping.Column p, bool storeDateTimeAsTicks)
					{
						string decl = "\"" + p.Name + "\" " + SqlType (p, storeDateTimeAsTicks) + " ";
						
						if (p.IsPK) {
							decl += "primary key ";
						}
						if (p.IsAutoInc) {
							decl += "autoincrement ";
						}
						if (!p.IsNullable) {
							decl += "not null ";
						}
						if (!string.IsNullOrEmpty (p.Collation)) {
							decl += "collate " + p.Collation + " ";
						}
						
						return decl;
					}
			
					public static string SqlType (TableMapping.Column p, bool storeDateTimeAsTicks)
					{
						var clrType = p.ColumnType;
			            if (clrType == typeof(Boolean) || clrType == typeof(Byte) || clrType == typeof(UInt16) || clrType == typeof(SByte) || clrType == typeof(Int16) || clrType == typeof(Int32) || clrType == typeof(UInt32) || clrType == typeof(Int64))
			            {
							return "integer";
						} else if (clrType == typeof(Single) || clrType == typeof(Double) || clrType == typeof(Decimal)) {
							return "float";
						} else if (clrType == typeof(String)) {
							int? len = p.MaxStringLength;
			
							if (len.HasValue)
								return "varchar(" + len.Value + ")";
			
							return "varchar";
						} else if (clrType == typeof(TimeSpan)) {
			                return "bigint";
						} else if (clrType == typeof(DateTime)) {
							return storeDateTimeAsTicks ? "bigint" : "datetime";
						} else if (clrType == typeof(DateTimeOffset)) {
							return "bigint";
			#if !USE_NEW_REFLECTION_API
						} else if (clrType.IsEnum) {
			#else
						} else if (clrType.GetTypeInfo().IsEnum) {
			#endif
							return "integer";
						} else if (clrType == typeof(byte[])) {
							return "blob";
			            } else if (clrType == typeof(Guid)) {
			                return "varchar(36)";
			            } else {
							throw new NotSupportedException ("Don't know about " + clrType);
						}
					}
			
					public static bool IsPK (MemberInfo p)
					{
						var attrs = p.GetCustomAttributes (typeof(PrimaryKeyAttribute), true);
			#if !USE_NEW_REFLECTION_API
						return attrs.Length > 0;
			#else
						return attrs.Count() > 0;
			#endif
					}
			
					public static string Collation (MemberInfo p)
					{
						var attrs = p.GetCustomAttributes (typeof(CollationAttribute), true);
			#if !USE_NEW_REFLECTION_API
						if (attrs.Length > 0) {
							return ((CollationAttribute)attrs [0]).Value;
			#else
						if (attrs.Count() > 0) {
			                return ((CollationAttribute)attrs.First()).Value;
			#endif
						} else {
							return string.Empty;
						}
					}
			
					public static bool IsAutoInc (MemberInfo p)
					{
						var attrs = p.GetCustomAttributes (typeof(AutoIncrementAttribute), true);
			#if !USE_NEW_REFLECTION_API
						return attrs.Length > 0;
			#else
						return attrs.Count() > 0;
			#endif
					}
			
					public static IEnumerable<IndexedAttribute> GetIndices(MemberInfo p)
					{
						var attrs = p.GetCustomAttributes(typeof(IndexedAttribute), true);
						return attrs.Cast<IndexedAttribute>();
					}
					
					public static int? MaxStringLength(PropertyInfo p)
					{
						var attrs = p.GetCustomAttributes (typeof(MaxLengthAttribute), true);
			#if !USE_NEW_REFLECTION_API
						if (attrs.Length > 0)
							return ((MaxLengthAttribute)attrs [0]).Value;
			#else
						if (attrs.Count() > 0)
							return ((MaxLengthAttribute)attrs.First()).Value;
			#endif
			
						return null;
					}
			
					public static bool IsMarkedNotNull(MemberInfo p)
					{
						var attrs = p.GetCustomAttributes (typeof (NotNullAttribute), true);
			#if !USE_NEW_REFLECTION_API
						return attrs.Length > 0;
			#else
				return attrs.Count() > 0;
			#endif
					}
				}
			
				public partial class SQLiteCommand
				{
					SQLiteConnection _conn;
					private List<Binding> _bindings;
			
					public string CommandText { get; set; }
			
					internal SQLiteCommand (SQLiteConnection conn)
					{
						_conn = conn;
						_bindings = new List<Binding> ();
						CommandText = "";
					}
			
					public int ExecuteNonQuery ()
					{
						if (_conn.Trace) {
							Debug.WriteLine ("Executing: " + this);
						}
						
						var r = SQLite3.Result.OK;
						var stmt = Prepare ();
						r = SQLite3.Step (stmt);
						Finalize (stmt);
						if (r == SQLite3.Result.Done) {
							int rowsAffected = SQLite3.Changes (_conn.Handle);
							return rowsAffected;
						} else if (r == SQLite3.Result.Error) {
							string msg = SQLite3.GetErrmsg (_conn.Handle);
							throw SQLiteException.New (r, msg);
						}
						else if (r == SQLite3.Result.Constraint) {
							if (SQLite3.ExtendedErrCode (_conn.Handle) == SQLite3.ExtendedResult.ConstraintNotNull) {
								throw NotNullConstraintViolationException.New (r, SQLite3.GetErrmsg (_conn.Handle));
							}
						}
			
						throw SQLiteException.New(r, r.ToString());
					}
			
					public IEnumerable<T> ExecuteDeferredQuery<T> ()
					{
						return ExecuteDeferredQuery<T>(_conn.GetMapping(typeof(T)));
					}
			
					public List<T> ExecuteQuery<T> ()
					{
						return ExecuteDeferredQuery<T>(_conn.GetMapping(typeof(T))).ToList();
					}
			
					public List<T> ExecuteQuery<T> (TableMapping map)
					{
						return ExecuteDeferredQuery<T>(map).ToList();
					}
			
					/// <summary>
					/// Invoked every time an instance is loaded from the database.
					/// </summary>
					/// <param name='obj'>
					/// The newly created object.
					/// </param>
					/// <remarks>
					/// This can be overridden in combination with the <see cref="SQLiteConnection.NewCommand"/>
					/// method to hook into the life-cycle of objects.
					///
					/// Type safety is not possible because MonoTouch does not support virtual generic methods.
					/// </remarks>
					protected virtual void OnInstanceCreated (object obj)
					{
						// Can be overridden.
					}
			
					public IEnumerable<T> ExecuteDeferredQuery<T> (TableMapping map)
					{
						if (_conn.Trace) {
							Debug.WriteLine ("Executing Query: " + this);
						}
			
						var stmt = Prepare ();
						try
						{
							var cols = new TableMapping.Column[SQLite3.ColumnCount (stmt)];
			
							for (int i = 0; i < cols.Length; i++) {
								var name = SQLite3.ColumnName16 (stmt, i);
								cols [i] = map.FindColumn (name);
							}
						
							while (SQLite3.Step (stmt) == SQLite3.Result.Row) {
								var obj = Activator.CreateInstance(map.MappedType);
								for (int i = 0; i < cols.Length; i++) {
									if (cols [i] == null)
										continue;
									var colType = SQLite3.ColumnType (stmt, i);
									var val = ReadCol (stmt, i, colType, cols [i].ColumnType);
									cols [i].SetValue (obj, val);
			 					}
								OnInstanceCreated (obj);
								yield return (T)obj;
							}
						}
						finally
						{
							SQLite3.Finalize(stmt);
						}
					}
			
					public T ExecuteScalar<T> ()
					{
						if (_conn.Trace) {
							Debug.WriteLine ("Executing Query: " + this);
						}
						
						T val = default(T);
						
						var stmt = Prepare ();
			
			            try
			            {
			                var r = SQLite3.Step (stmt);
			                if (r == SQLite3.Result.Row) {
			                    var colType = SQLite3.ColumnType (stmt, 0);
			                    val = (T)ReadCol (stmt, 0, colType, typeof(T));
			                }
			                else if (r == SQLite3.Result.Done) {
			                }
			                else
			                {
			                    throw SQLiteException.New (r, SQLite3.GetErrmsg (_conn.Handle));
			                }
			            }
			            finally
			            {
			                Finalize (stmt);
			            }
						
						return val;
					}
			
					public void Bind (string name, object val)
					{
						_bindings.Add (new Binding {
							Name = name,
							Value = val
						});
					}
			
					public void Bind (object val)
					{
						Bind (null, val);
					}
			
					public override string ToString ()
					{
						var parts = new string[1 + _bindings.Count];
						parts [0] = CommandText;
						var i = 1;
						foreach (var b in _bindings) {
							parts [i] = string.Format ("  {0}: {1}", i - 1, b.Value);
							i++;
						}
						return string.Join (Environment.NewLine, parts);
					}
			
					Sqlite3Statement Prepare()
					{
						var stmt = SQLite3.Prepare2 (_conn.Handle, CommandText);
						BindAll (stmt);
						return stmt;
					}
			
					void Finalize (Sqlite3Statement stmt)
					{
						SQLite3.Finalize (stmt);
					}
			
					void BindAll (Sqlite3Statement stmt)
					{
						int nextIdx = 1;
						foreach (var b in _bindings) {
							if (b.Name != null) {
								b.Index = SQLite3.BindParameterIndex (stmt, b.Name);
							} else {
								b.Index = nextIdx++;
							}
							
							BindParameter (stmt, b.Index, b.Value, _conn.StoreDateTimeAsTicks);
						}
					}
			
					internal static IntPtr NegativePointer = new IntPtr (-1);
			
					internal static void BindParameter (Sqlite3Statement stmt, int index, object value, bool storeDateTimeAsTicks)
					{
						if (value == null) {
							SQLite3.BindNull (stmt, index);
						} else {
							if (value is Int32) {
								SQLite3.BindInt (stmt, index, (int)value);
							} else if (value is String) {
								SQLite3.BindText (stmt, index, (string)value, -1, NegativePointer);
							} else if (value is Byte || value is UInt16 || value is SByte || value is Int16) {
								SQLite3.BindInt (stmt, index, Convert.ToInt32 (value));
							} else if (value is Boolean) {
								SQLite3.BindInt (stmt, index, (bool)value ? 1 : 0);
							} else if (value is UInt32 || value is Int64) {
								SQLite3.BindInt64 (stmt, index, Convert.ToInt64 (value));
							} else if (value is Single || value is Double || value is Decimal) {
								SQLite3.BindDouble (stmt, index, Convert.ToDouble (value));
							} else if (value is TimeSpan) {
								SQLite3.BindInt64(stmt, index, ((TimeSpan)value).Ticks);
							} else if (value is DateTime) {
								if (storeDateTimeAsTicks) {
									SQLite3.BindInt64 (stmt, index, ((DateTime)value).Ticks);
								}
								else {
									SQLite3.BindText (stmt, index, ((DateTime)value).ToString ("yyyy-MM-dd HH:mm:ss"), -1, NegativePointer);
								}
							} else if (value is DateTimeOffset) {
								SQLite3.BindInt64 (stmt, index, ((DateTimeOffset)value).UtcTicks);
			#if !USE_NEW_REFLECTION_API
							} else if (value.GetType().IsEnum) {
			#else
							} else if (value.GetType().GetTypeInfo().IsEnum) {
			#endif
								SQLite3.BindInt (stmt, index, Convert.ToInt32 (value));
			                } else if (value is byte[]){
			                    SQLite3.BindBlob(stmt, index, (byte[]) value, ((byte[]) value).Length, NegativePointer);
			                } else if (value is Guid) {
			                    SQLite3.BindText(stmt, index, ((Guid)value).ToString(), 72, NegativePointer);
			                } else {
			                    throw new NotSupportedException("Cannot store type: " + value.GetType());
			                }
						}
					}
			
					class Binding
					{
						public string Name { get; set; }
			
						public object Value { get; set; }
			
						public int Index { get; set; }
					}
			
					object ReadCol (Sqlite3Statement stmt, int index, SQLite3.ColType type, Type clrType)
					{
						if (type == SQLite3.ColType.Null) {
							return null;
						} else {
							if (clrType == typeof(String)) {
								return SQLite3.ColumnString (stmt, index);
							} else if (clrType == typeof(Int32)) {
								return (int)SQLite3.ColumnInt (stmt, index);
							} else if (clrType == typeof(Boolean)) {
								return SQLite3.ColumnInt (stmt, index) == 1;
							} else if (clrType == typeof(double)) {
								return SQLite3.ColumnDouble (stmt, index);
							} else if (clrType == typeof(float)) {
								return (float)SQLite3.ColumnDouble (stmt, index);
							} else if (clrType == typeof(TimeSpan)) {
								return new TimeSpan(SQLite3.ColumnInt64(stmt, index));
							} else if (clrType == typeof(DateTime)) {
								if (_conn.StoreDateTimeAsTicks) {
									return new DateTime (SQLite3.ColumnInt64 (stmt, index));
								}
								else {
									var text = SQLite3.ColumnString (stmt, index);
									return DateTime.Parse (text);
								}
							} else if (clrType == typeof(DateTimeOffset)) {
								return new DateTimeOffset(SQLite3.ColumnInt64 (stmt, index),TimeSpan.Zero);
			#if !USE_NEW_REFLECTION_API
							} else if (clrType.IsEnum) {
			#else
							} else if (clrType.GetTypeInfo().IsEnum) {
			#endif
								return SQLite3.ColumnInt (stmt, index);
							} else if (clrType == typeof(Int64)) {
								return SQLite3.ColumnInt64 (stmt, index);
							} else if (clrType == typeof(UInt32)) {
								return (uint)SQLite3.ColumnInt64 (stmt, index);
							} else if (clrType == typeof(decimal)) {
								return (decimal)SQLite3.ColumnDouble (stmt, index);
							} else if (clrType == typeof(Byte)) {
								return (byte)SQLite3.ColumnInt (stmt, index);
							} else if (clrType == typeof(UInt16)) {
								return (ushort)SQLite3.ColumnInt (stmt, index);
							} else if (clrType == typeof(Int16)) {
								return (short)SQLite3.ColumnInt (stmt, index);
							} else if (clrType == typeof(sbyte)) {
								return (sbyte)SQLite3.ColumnInt (stmt, index);
							} else if (clrType == typeof(byte[])) {
								return SQLite3.ColumnByteArray (stmt, index);
							} else if (clrType == typeof(Guid)) {
			                  var text = SQLite3.ColumnString(stmt, index);
			                  return new Guid(text);
			                } else{
								throw new NotSupportedException ("Don't know how to read " + clrType);
							}
						}
					}
				}
			
				/// <summary>
				/// Since the insert never changed, we only need to prepare once.
				/// </summary>
				public class PreparedSqlLiteInsertCommand : IDisposable
				{
					public bool Initialized { get; set; }
			
					protected SQLiteConnection Connection { get; set; }
			
					public string CommandText { get; set; }
			
					protected Sqlite3Statement Statement { get; set; }
					internal static readonly Sqlite3Statement NullStatement = default(Sqlite3Statement);
			
					internal PreparedSqlLiteInsertCommand (SQLiteConnection conn)
					{
						Connection = conn;
					}
			
					public int ExecuteNonQuery (object[] source)
					{
						if (Connection.Trace) {
							Debug.WriteLine ("Executing: " + CommandText);
						}
			
						var r = SQLite3.Result.OK;
			
						if (!Initialized) {
							Statement = Prepare ();
							Initialized = true;
						}
			
						//bind the values.
						if (source != null) {
							for (int i = 0; i < source.Length; i++) {
								SQLiteCommand.BindParameter (Statement, i + 1, source [i], Connection.StoreDateTimeAsTicks);
							}
						}
						r = SQLite3.Step (Statement);
			
						if (r == SQLite3.Result.Done) {
							int rowsAffected = SQLite3.Changes (Connection.Handle);
							SQLite3.Reset (Statement);
							return rowsAffected;
						} else if (r == SQLite3.Result.Error) {
							string msg = SQLite3.GetErrmsg (Connection.Handle);
							SQLite3.Reset (Statement);
							throw SQLiteException.New (r, msg);
						} else if (r == SQLite3.Result.Constraint && SQLite3.ExtendedErrCode (Connection.Handle) == SQLite3.ExtendedResult.ConstraintNotNull) {
							SQLite3.Reset (Statement);
							throw NotNullConstraintViolationException.New (r, SQLite3.GetErrmsg (Connection.Handle));
						} else {
							SQLite3.Reset (Statement);
							throw SQLiteException.New (r, r.ToString ());
						}
					}
			
					protected virtual Sqlite3Statement Prepare ()
					{
						var stmt = SQLite3.Prepare2 (Connection.Handle, CommandText);
						return stmt;
					}
			
					public void Dispose ()
					{
						Dispose (true);
						GC.SuppressFinalize (this);
					}
			
					private void Dispose (bool disposing)
					{
						if (Statement != NullStatement) {
							try {
								SQLite3.Finalize (Statement);
							} finally {
								Statement = NullStatement;
								Connection = null;
							}
						}
					}
			
					~PreparedSqlLiteInsertCommand ()
					{
						Dispose (false);
					}
				}
			
				public abstract class BaseTableQuery
				{
					protected class Ordering
					{
						public string ColumnName { get; set; }
						public bool Ascending { get; set; }
					}
				}
			
				public class TableQuery<T> : BaseTableQuery, IEnumerable<T>
				{
					public SQLiteConnection Connection { get; private set; }
			
					public TableMapping Table { get; private set; }
			
					Expression _where;
					List<Ordering> _orderBys;
					int? _limit;
					int? _offset;
			
					BaseTableQuery _joinInner;
					Expression _joinInnerKeySelector;
					BaseTableQuery _joinOuter;
					Expression _joinOuterKeySelector;
					Expression _joinSelector;
							
					Expression _selector;
			
					TableQuery (SQLiteConnection conn, TableMapping table)
					{
						Connection = conn;
						Table = table;
					}
			
					public TableQuery (SQLiteConnection conn)
					{
						Connection = conn;
						Table = Connection.GetMapping (typeof(T));
					}
			
					public TableQuery<U> Clone<U> ()
					{
						var q = new TableQuery<U> (Connection, Table);
						q._where = _where;
						q._deferred = _deferred;
						if (_orderBys != null) {
							q._orderBys = new List<Ordering> (_orderBys);
						}
						q._limit = _limit;
						q._offset = _offset;
						q._joinInner = _joinInner;
						q._joinInnerKeySelector = _joinInnerKeySelector;
						q._joinOuter = _joinOuter;
						q._joinOuterKeySelector = _joinOuterKeySelector;
						q._joinSelector = _joinSelector;
						q._selector = _selector;
						return q;
					}
			
					public TableQuery<T> Where (Expression<Func<T, bool>> predExpr)
					{
						if (predExpr.NodeType == ExpressionType.Lambda) {
							var lambda = (LambdaExpression)predExpr;
							var pred = lambda.Body;
							var q = Clone<T> ();
							q.AddWhere (pred);
							return q;
						} else {
							throw new NotSupportedException ("Must be a predicate");
						}
					}
			
					public int Delete(Expression<Func<T, bool>> predExpr)
					{
						if (predExpr.NodeType == ExpressionType.Lambda) {
							var lambda = (LambdaExpression)predExpr;
							var pred = lambda.Body;
							var args = new List<object> ();
							var w = CompileExpr (pred, args);
							var cmdText = "delete from \"" + Table.TableName + "\"";
							cmdText += " where " + w.CommandText;
							var command = Connection.CreateCommand (cmdText, args.ToArray ());
			
							int result = command.ExecuteNonQuery();
							return result;
						} else {
							throw new NotSupportedException ("Must be a predicate");
						}
					}
			
					public TableQuery<T> Take (int n)
					{
						var q = Clone<T> ();
						q._limit = n;
						return q;
					}
			
					public TableQuery<T> Skip (int n)
					{
						var q = Clone<T> ();
						q._offset = n;
						return q;
					}
			
					public T ElementAt (int index)
					{
						return Skip (index).Take (1).First ();
					}
			
					bool _deferred;
					public TableQuery<T> Deferred ()
					{
						var q = Clone<T> ();
						q._deferred = true;
						return q;
					}
			
					public TableQuery<T> OrderBy<U> (Expression<Func<T, U>> orderExpr)
					{
						return AddOrderBy<U> (orderExpr, true);
					}
			
					public TableQuery<T> OrderByDescending<U> (Expression<Func<T, U>> orderExpr)
					{
						return AddOrderBy<U> (orderExpr, false);
					}
			
					public TableQuery<T> ThenBy<U>(Expression<Func<T, U>> orderExpr)
					{
						return AddOrderBy<U>(orderExpr, true);
					}
			
					public TableQuery<T> ThenByDescending<U>(Expression<Func<T, U>> orderExpr)
					{
						return AddOrderBy<U>(orderExpr, false);
					}
			
					private TableQuery<T> AddOrderBy<U> (Expression<Func<T, U>> orderExpr, bool asc)
					{
						if (orderExpr.NodeType == ExpressionType.Lambda) {
							var lambda = (LambdaExpression)orderExpr;
							
							MemberExpression mem = null;
							
							var unary = lambda.Body as UnaryExpression;
							if (unary != null && unary.NodeType == ExpressionType.Convert) {
								mem = unary.Operand as MemberExpression;
							}
							else {
								mem = lambda.Body as MemberExpression;
							}
							
							if (mem != null && (mem.Expression.NodeType == ExpressionType.Parameter)) {
								var q = Clone<T> ();
								if (q._orderBys == null) {
									q._orderBys = new List<Ordering> ();
								}
								q._orderBys.Add (new Ordering {
									ColumnName = Table.FindColumnWithPropertyName(mem.Member.Name).Name,
									Ascending = asc
								});
								return q;
							} else {
								throw new NotSupportedException ("Order By does not support: " + orderExpr);
							}
						} else {
							throw new NotSupportedException ("Must be a predicate");
						}
					}
			
					private void AddWhere (Expression pred)
					{
						if (_where == null) {
							_where = pred;
						} else {
							_where = Expression.AndAlso (_where, pred);
						}
					}
							
					public TableQuery<TResult> Join<TInner, TKey, TResult> (
						TableQuery<TInner> inner,
						Expression<Func<T, TKey>> outerKeySelector,
						Expression<Func<TInner, TKey>> innerKeySelector,
						Expression<Func<T, TInner, TResult>> resultSelector)
					{
						var q = new TableQuery<TResult> (Connection, Connection.GetMapping (typeof (TResult))) {
							_joinOuter = this,
							_joinOuterKeySelector = outerKeySelector,
							_joinInner = inner,
							_joinInnerKeySelector = innerKeySelector,
							_joinSelector = resultSelector,
						};
						return q;
					}
							
					public TableQuery<TResult> Select<TResult> (Expression<Func<T, TResult>> selector)
					{
						var q = Clone<TResult> ();
						q._selector = selector;
						return q;
					}
			
					private SQLiteCommand GenerateCommand (string selectionList)
					{
						if (_joinInner != null && _joinOuter != null) {
							throw new NotSupportedException ("Joins are not supported.");
						}
						else {
							var cmdText = "select " + selectionList + " from \"" + Table.TableName + "\"";
							var args = new List<object> ();
							if (_where != null) {
								var w = CompileExpr (_where, args);
								cmdText += " where " + w.CommandText;
							}
							if ((_orderBys != null) && (_orderBys.Count > 0)) {
								var t = string.Join (", ", _orderBys.Select (o => "\"" + o.ColumnName + "\"" + (o.Ascending ? "" : " desc")).ToArray ());
								cmdText += " order by " + t;
							}
							if (_limit.HasValue) {
								cmdText += " limit " + _limit.Value;
							}
							if (_offset.HasValue) {
								if (!_limit.HasValue) {
									cmdText += " limit -1 ";
								}
								cmdText += " offset " + _offset.Value;
							}
							return Connection.CreateCommand (cmdText, args.ToArray ());
						}
					}
			
					class CompileResult
					{
						public string CommandText { get; set; }
			
						public object Value { get; set; }
					}
			
					private CompileResult CompileExpr (Expression expr, List<object> queryArgs)
					{
						if (expr == null) {
							throw new NotSupportedException ("Expression is NULL");
						} else if (expr is BinaryExpression) {
							var bin = (BinaryExpression)expr;
			
							// VB turns 'x=="foo"' into 'CompareString(x,"foo",true/false)==0', so we need to unwrap it
							// http://blogs.msdn.com/b/vbteam/archive/2007/09/18/vb-expression-trees-string-comparisons.aspx
							if (bin.Left.NodeType == ExpressionType.Call) { 
								var call = (MethodCallExpression)bin.Left;
								if (call.Method.DeclaringType.FullName == "Microsoft.VisualBasic.CompilerServices.Operators"
									&& call.Method.Name == "CompareString")
									bin = Expression.MakeBinary(bin.NodeType, call.Arguments[0], call.Arguments[1]);
							}
			
			
							var leftr = CompileExpr (bin.Left, queryArgs);
							var rightr = CompileExpr (bin.Right, queryArgs);
			
							//If either side is a parameter and is null, then handle the other side specially (for "is null"/"is not null")
							string text;
							if (leftr.CommandText == "?" && leftr.Value == null)
								text = CompileNullBinaryExpression(bin, rightr);
							else if (rightr.CommandText == "?" && rightr.Value == null)
								text = CompileNullBinaryExpression(bin, leftr);
							else
								text = "(" + leftr.CommandText + " " + GetSqlName(bin) + " " + rightr.CommandText + ")";
							return new CompileResult { CommandText = text };
						} else if (expr.NodeType == ExpressionType.Not) {
			                var operandExpr = ((UnaryExpression)expr).Operand;
			                var opr = CompileExpr(operandExpr, queryArgs);
			                object val = opr.Value;
			                if (val is bool)
			                    val = !((bool) val);
			                return new CompileResult
			                {
			                    CommandText = "NOT(" + opr.CommandText + ")",
			                    Value = val
			                };
			            } else if (expr.NodeType == ExpressionType.Call) {
							
							var call = (MethodCallExpression)expr;
							var args = new CompileResult[call.Arguments.Count];
							var obj = call.Object != null ? CompileExpr (call.Object, queryArgs) : null;
							
							for (var i = 0; i < args.Length; i++) {
								args [i] = CompileExpr (call.Arguments [i], queryArgs);
							}
							
							var sqlCall = "";
							
							if (call.Method.Name == "Like" && args.Length == 2) {
								sqlCall = "(" + args [0].CommandText + " like " + args [1].CommandText + ")";
							}
							else if (call.Method.Name == "Contains" && args.Length == 2) {
								sqlCall = "(" + args [1].CommandText + " in " + args [0].CommandText + ")";
							}
							else if (call.Method.Name == "Contains" && args.Length == 1) {
								if (call.Object != null && call.Object.Type == typeof(string)) {
									sqlCall = "(" + obj.CommandText + " like ('%' || " + args [0].CommandText + " || '%'))";
								}
								else {
									sqlCall = "(" + args [0].CommandText + " in " + obj.CommandText + ")";
								}
							}
							else if (call.Method.Name == "StartsWith" && args.Length == 1) {
								sqlCall = "(" + obj.CommandText + " like (" + args [0].CommandText + " || '%'))";
							}
							else if (call.Method.Name == "EndsWith" && args.Length == 1) {
								sqlCall = "(" + obj.CommandText + " like ('%' || " + args [0].CommandText + "))";
							}
							else if (call.Method.Name == "Equals" && args.Length == 1) {
								sqlCall = "(" + obj.CommandText + " = (" + args[0].CommandText + "))";
							} else if (call.Method.Name == "ToLower") {
								sqlCall = "(lower(" + obj.CommandText + "))"; 
							} else if (call.Method.Name == "ToUpper") {
								sqlCall = "(upper(" + obj.CommandText + "))"; 
							} else {
								sqlCall = call.Method.Name.ToLower () + "(" + string.Join (",", args.Select (a => a.CommandText).ToArray ()) + ")";
							}
							return new CompileResult { CommandText = sqlCall };
							
						} else if (expr.NodeType == ExpressionType.Constant) {
							var c = (ConstantExpression)expr;
							queryArgs.Add (c.Value);
							return new CompileResult {
								CommandText = "?",
								Value = c.Value
							};
						} else if (expr.NodeType == ExpressionType.Convert) {
							var u = (UnaryExpression)expr;
							var ty = u.Type;
							var valr = CompileExpr (u.Operand, queryArgs);
							return new CompileResult {
								CommandText = valr.CommandText,
								Value = valr.Value != null ? ConvertTo (valr.Value, ty) : null
							};
						} else if (expr.NodeType == ExpressionType.MemberAccess) {
							var mem = (MemberExpression)expr;
							
							if (mem.Expression!=null && mem.Expression.NodeType == ExpressionType.Parameter) {
								//
								// This is a column of our table, output just the column name
								// Need to translate it if that column name is mapped
								//
								var columnName = Table.FindColumnWithPropertyName (mem.Member.Name).Name;
								return new CompileResult { CommandText = "\"" + columnName + "\"" };
							} else {
								object obj = null;
								if (mem.Expression != null) {
									var r = CompileExpr (mem.Expression, queryArgs);
									if (r.Value == null) {
										throw new NotSupportedException ("Member access failed to compile expression");
									}
									if (r.CommandText == "?") {
										queryArgs.RemoveAt (queryArgs.Count - 1);
									}
									obj = r.Value;
								}
								
								//
								// Get the member value
								//
								object val = null;
								
			#if !USE_NEW_REFLECTION_API
								if (mem.Member.MemberType == MemberTypes.Property) {
			#else
								if (mem.Member is PropertyInfo) {
			#endif
									var m = (PropertyInfo)mem.Member;
									val = m.GetValue (obj, null);
			#if !USE_NEW_REFLECTION_API
								} else if (mem.Member.MemberType == MemberTypes.Field) {
			#else
								} else if (mem.Member is FieldInfo) {
			#endif
			#if SILVERLIGHT
									val = Expression.Lambda (expr).Compile ().DynamicInvoke ();
			#else
									var m = (FieldInfo)mem.Member;
									val = m.GetValue (obj);
			#endif
								} else {
			#if !USE_NEW_REFLECTION_API
									throw new NotSupportedException ("MemberExpr: " + mem.Member.MemberType);
			#else
									throw new NotSupportedException ("MemberExpr: " + mem.Member.DeclaringType);
			#endif
								}
								
								//
								// Work special magic for enumerables
								//
								if (val != null && val is System.Collections.IEnumerable && !(val is string) && !(val is System.Collections.Generic.IEnumerable<byte>)) {
									var sb = new System.Text.StringBuilder();
									sb.Append("(");
									var head = "";
									foreach (var a in (System.Collections.IEnumerable)val) {
										queryArgs.Add(a);
										sb.Append(head);
										sb.Append("?");
										head = ",";
									}
									sb.Append(")");
									return new CompileResult {
										CommandText = sb.ToString(),
										Value = val
									};
								}
								else {
									queryArgs.Add (val);
									return new CompileResult {
										CommandText = "?",
										Value = val
									};
								}
							}
						}
						throw new NotSupportedException ("Cannot compile: " + expr.NodeType.ToString ());
					}
			
					static object ConvertTo (object obj, Type t)
					{
						Type nut = Nullable.GetUnderlyingType(t);
						
						if (nut != null) {
							if (obj == null) return null;				
							return Convert.ChangeType (obj, nut);
						} else {
							return Convert.ChangeType (obj, t);
						}
					}
			
					/// <summary>
					/// Compiles a BinaryExpression where one of the parameters is null.
					/// </summary>
					/// <param name="parameter">The non-null parameter</param>
					private string CompileNullBinaryExpression(BinaryExpression expression, CompileResult parameter)
					{
						if (expression.NodeType == ExpressionType.Equal)
							return "(" + parameter.CommandText + " is ?)";
						else if (expression.NodeType == ExpressionType.NotEqual)
							return "(" + parameter.CommandText + " is not ?)";
						else
							throw new NotSupportedException("Cannot compile Null-BinaryExpression with type " + expression.NodeType.ToString());
					}
			
					string GetSqlName (Expression expr)
					{
						var n = expr.NodeType;
						if (n == ExpressionType.GreaterThan)
							return ">"; else if (n == ExpressionType.GreaterThanOrEqual) {
							return ">=";
						} else if (n == ExpressionType.LessThan) {
							return "<";
						} else if (n == ExpressionType.LessThanOrEqual) {
							return "<=";
						} else if (n == ExpressionType.And) {
							return "&";
						} else if (n == ExpressionType.AndAlso) {
							return "and";
						} else if (n == ExpressionType.Or) {
							return "|";
						} else if (n == ExpressionType.OrElse) {
							return "or";
						} else if (n == ExpressionType.Equal) {
							return "=";
						} else if (n == ExpressionType.NotEqual) {
							return "!=";
						} else {
							throw new NotSupportedException ("Cannot get SQL for: " + n);
						}
					}
					
					public int Count ()
					{
						return GenerateCommand("count(*)").ExecuteScalar<int> ();			
					}
			
					public int Count (Expression<Func<T, bool>> predExpr)
					{
						return Where (predExpr).Count ();
					}
			
					public IEnumerator<T> GetEnumerator ()
					{
						if (!_deferred)
							return GenerateCommand("*").ExecuteQuery<T>().GetEnumerator();
			
						return GenerateCommand("*").ExecuteDeferredQuery<T>().GetEnumerator();
					}
			
					System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator ()
					{
						return GetEnumerator ();
					}
			
					public T First ()
					{
						var query = Take (1);
						return query.ToList<T>().First ();
					}
			
					public T FirstOrDefault ()
					{
						var query = Take (1);
						return query.ToList<T>().FirstOrDefault ();
					}
			    }
			
				public static class SQLite3
				{
					public enum Result : int
					{
						OK = 0,
						Error = 1,
						Internal = 2,
						Perm = 3,
						Abort = 4,
						Busy = 5,
						Locked = 6,
						NoMem = 7,
						ReadOnly = 8,
						Interrupt = 9,
						IOError = 10,
						Corrupt = 11,
						NotFound = 12,
						Full = 13,
						CannotOpen = 14,
						LockErr = 15,
						Empty = 16,
						SchemaChngd = 17,
						TooBig = 18,
						Constraint = 19,
						Mismatch = 20,
						Misuse = 21,
						NotImplementedLFS = 22,
						AccessDenied = 23,
						Format = 24,
						Range = 25,
						NonDBFile = 26,
						Notice = 27,
						Warning = 28,
						Row = 100,
						Done = 101
					}
			
					public enum ExtendedResult : int
					{
						IOErrorRead = (Result.IOError | (1 << 8)),
						IOErrorShortRead = (Result.IOError | (2 << 8)),
						IOErrorWrite = (Result.IOError | (3 << 8)),
						IOErrorFsync = (Result.IOError | (4 << 8)),
						IOErrorDirFSync = (Result.IOError | (5 << 8)),
						IOErrorTruncate = (Result.IOError | (6 << 8)),
						IOErrorFStat = (Result.IOError | (7 << 8)),
						IOErrorUnlock = (Result.IOError | (8 << 8)),
						IOErrorRdlock = (Result.IOError | (9 << 8)),
						IOErrorDelete = (Result.IOError | (10 << 8)),
						IOErrorBlocked = (Result.IOError | (11 << 8)),
						IOErrorNoMem = (Result.IOError | (12 << 8)),
						IOErrorAccess = (Result.IOError | (13 << 8)),
						IOErrorCheckReservedLock = (Result.IOError | (14 << 8)),
						IOErrorLock = (Result.IOError | (15 << 8)),
						IOErrorClose = (Result.IOError | (16 << 8)),
						IOErrorDirClose = (Result.IOError | (17 << 8)),
						IOErrorSHMOpen = (Result.IOError | (18 << 8)),
						IOErrorSHMSize = (Result.IOError | (19 << 8)),
						IOErrorSHMLock = (Result.IOError | (20 << 8)),
						IOErrorSHMMap = (Result.IOError | (21 << 8)),
						IOErrorSeek = (Result.IOError | (22 << 8)),
						IOErrorDeleteNoEnt = (Result.IOError | (23 << 8)),
						IOErrorMMap = (Result.IOError | (24 << 8)),
						LockedSharedcache = (Result.Locked | (1 << 8)),
						BusyRecovery = (Result.Busy | (1 << 8)),
						CannottOpenNoTempDir = (Result.CannotOpen | (1 << 8)),
						CannotOpenIsDir = (Result.CannotOpen | (2 << 8)),
						CannotOpenFullPath = (Result.CannotOpen | (3 << 8)),
						CorruptVTab = (Result.Corrupt | (1 << 8)),
						ReadonlyRecovery = (Result.ReadOnly | (1 << 8)),
						ReadonlyCannotLock = (Result.ReadOnly | (2 << 8)),
						ReadonlyRollback = (Result.ReadOnly | (3 << 8)),
						AbortRollback = (Result.Abort | (2 << 8)),
						ConstraintCheck = (Result.Constraint | (1 << 8)),
						ConstraintCommitHook = (Result.Constraint | (2 << 8)),
						ConstraintForeignKey = (Result.Constraint | (3 << 8)),
						ConstraintFunction = (Result.Constraint | (4 << 8)),
						ConstraintNotNull = (Result.Constraint | (5 << 8)),
						ConstraintPrimaryKey = (Result.Constraint | (6 << 8)),
						ConstraintTrigger = (Result.Constraint | (7 << 8)),
						ConstraintUnique = (Result.Constraint | (8 << 8)),
						ConstraintVTab = (Result.Constraint | (9 << 8)),
						NoticeRecoverWAL = (Result.Notice | (1 << 8)),
						NoticeRecoverRollback = (Result.Notice | (2 << 8))
					}
			        
			
					public enum ConfigOption : int
					{
						SingleThread = 1,
						MultiThread = 2,
						Serialized = 3
					}
			
					const string LibraryPath = "sqlite3";
			
			#if !USE_CSHARP_SQLITE && !USE_WP8_NATIVE_SQLITE && !USE_SQLITEPCL_RAW
					[DllImport(LibraryPath, EntryPoint = "sqlite3_threadsafe", CallingConvention=CallingConvention.Cdecl)]
					public static extern int Threadsafe ();
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_open", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result Open ([MarshalAs(UnmanagedType.LPStr)] string filename, out IntPtr db);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_open_v2", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result Open ([MarshalAs(UnmanagedType.LPStr)] string filename, out IntPtr db, int flags, IntPtr zvfs);
					
					[DllImport(LibraryPath, EntryPoint = "sqlite3_open_v2", CallingConvention = CallingConvention.Cdecl)]
					public static extern Result Open(byte[] filename, out IntPtr db, int flags, IntPtr zvfs);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_open16", CallingConvention = CallingConvention.Cdecl)]
					public static extern Result Open16([MarshalAs(UnmanagedType.LPWStr)] string filename, out IntPtr db);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_enable_load_extension", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result EnableLoadExtension (IntPtr db, int onoff);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_close", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result Close (IntPtr db);
					
					[DllImport(LibraryPath, EntryPoint = "sqlite3_close_v2", CallingConvention = CallingConvention.Cdecl)]
					public static extern Result Close2(IntPtr db);
					
					[DllImport(LibraryPath, EntryPoint = "sqlite3_initialize", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result Initialize();
									
					[DllImport(LibraryPath, EntryPoint = "sqlite3_shutdown", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result Shutdown();
					
					[DllImport(LibraryPath, EntryPoint = "sqlite3_config", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result Config (ConfigOption option);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_win32_set_directory", CallingConvention=CallingConvention.Cdecl, CharSet=CharSet.Unicode)]
					public static extern int SetDirectory (uint directoryType, string directoryPath);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_busy_timeout", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result BusyTimeout (IntPtr db, int milliseconds);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_changes", CallingConvention=CallingConvention.Cdecl)]
					public static extern int Changes (IntPtr db);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_prepare_v2", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result Prepare2 (IntPtr db, [MarshalAs(UnmanagedType.LPStr)] string sql, int numBytes, out IntPtr stmt, IntPtr pzTail);
			
			#if NETFX_CORE
					[DllImport (LibraryPath, EntryPoint = "sqlite3_prepare_v2", CallingConvention = CallingConvention.Cdecl)]
					public static extern Result Prepare2 (IntPtr db, byte[] queryBytes, int numBytes, out IntPtr stmt, IntPtr pzTail);
			#endif
			
					public static IntPtr Prepare2 (IntPtr db, string query)
					{
						IntPtr stmt;
			#if NETFX_CORE
			            byte[] queryBytes = System.Text.UTF8Encoding.UTF8.GetBytes (query);
			            var r = Prepare2 (db, queryBytes, queryBytes.Length, out stmt, IntPtr.Zero);
			#else
			            var r = Prepare2 (db, query, System.Text.UTF8Encoding.UTF8.GetByteCount (query), out stmt, IntPtr.Zero);
			#endif
						if (r != Result.OK) {
							throw SQLiteException.New (r, GetErrmsg (db));
						}
						return stmt;
					}
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_step", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result Step (IntPtr stmt);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_reset", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result Reset (IntPtr stmt);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_finalize", CallingConvention=CallingConvention.Cdecl)]
					public static extern Result Finalize (IntPtr stmt);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_last_insert_rowid", CallingConvention=CallingConvention.Cdecl)]
					public static extern long LastInsertRowid (IntPtr db);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_errmsg16", CallingConvention=CallingConvention.Cdecl)]
					public static extern IntPtr Errmsg (IntPtr db);
			
					public static string GetErrmsg (IntPtr db)
					{
						return Marshal.PtrToStringUni (Errmsg (db));
					}
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_bind_parameter_index", CallingConvention=CallingConvention.Cdecl)]
					public static extern int BindParameterIndex (IntPtr stmt, [MarshalAs(UnmanagedType.LPStr)] string name);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_bind_null", CallingConvention=CallingConvention.Cdecl)]
					public static extern int BindNull (IntPtr stmt, int index);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_bind_int", CallingConvention=CallingConvention.Cdecl)]
					public static extern int BindInt (IntPtr stmt, int index, int val);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_bind_int64", CallingConvention=CallingConvention.Cdecl)]
					public static extern int BindInt64 (IntPtr stmt, int index, long val);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_bind_double", CallingConvention=CallingConvention.Cdecl)]
					public static extern int BindDouble (IntPtr stmt, int index, double val);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_bind_text16", CallingConvention=CallingConvention.Cdecl, CharSet = CharSet.Unicode)]
					public static extern int BindText (IntPtr stmt, int index, [MarshalAs(UnmanagedType.LPWStr)] string val, int n, IntPtr free);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_bind_blob", CallingConvention=CallingConvention.Cdecl)]
					public static extern int BindBlob (IntPtr stmt, int index, byte[] val, int n, IntPtr free);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_column_count", CallingConvention=CallingConvention.Cdecl)]
					public static extern int ColumnCount (IntPtr stmt);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_column_name", CallingConvention=CallingConvention.Cdecl)]
					public static extern IntPtr ColumnName (IntPtr stmt, int index);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_column_name16", CallingConvention=CallingConvention.Cdecl)]
					static extern IntPtr ColumnName16Internal (IntPtr stmt, int index);
					public static string ColumnName16(IntPtr stmt, int index)
					{
						return Marshal.PtrToStringUni(ColumnName16Internal(stmt, index));
					}
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_column_type", CallingConvention=CallingConvention.Cdecl)]
					public static extern ColType ColumnType (IntPtr stmt, int index);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_column_int", CallingConvention=CallingConvention.Cdecl)]
					public static extern int ColumnInt (IntPtr stmt, int index);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_column_int64", CallingConvention=CallingConvention.Cdecl)]
					public static extern long ColumnInt64 (IntPtr stmt, int index);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_column_double", CallingConvention=CallingConvention.Cdecl)]
					public static extern double ColumnDouble (IntPtr stmt, int index);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_column_text", CallingConvention=CallingConvention.Cdecl)]
					public static extern IntPtr ColumnText (IntPtr stmt, int index);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_column_text16", CallingConvention=CallingConvention.Cdecl)]
					public static extern IntPtr ColumnText16 (IntPtr stmt, int index);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_column_blob", CallingConvention=CallingConvention.Cdecl)]
					public static extern IntPtr ColumnBlob (IntPtr stmt, int index);
			
					[DllImport(LibraryPath, EntryPoint = "sqlite3_column_bytes", CallingConvention=CallingConvention.Cdecl)]
					public static extern int ColumnBytes (IntPtr stmt, int index);
			
					public static string ColumnString (IntPtr stmt, int index)
					{
						return Marshal.PtrToStringUni (SQLite3.ColumnText16 (stmt, index));
					}
			
					public static byte[] ColumnByteArray (IntPtr stmt, int index)
					{
						int length = ColumnBytes (stmt, index);
						var result = new byte[length];
						if (length > 0)
							Marshal.Copy (ColumnBlob (stmt, index), result, 0, length);
						return result;
					}
			
					[DllImport (LibraryPath, EntryPoint = "sqlite3_extended_errcode", CallingConvention = CallingConvention.Cdecl)]
					public static extern ExtendedResult ExtendedErrCode (IntPtr db);
			
					[DllImport (LibraryPath, EntryPoint = "sqlite3_libversion_number", CallingConvention = CallingConvention.Cdecl)]
					public static extern int LibVersionNumber ();
			#else
					public static Result Open(string filename, out Sqlite3DatabaseHandle db)
					{
						return (Result) Sqlite3.sqlite3_open(filename, out db);
					}
			
					public static Result Open(string filename, out Sqlite3DatabaseHandle db, int flags, IntPtr zVfs)
					{
			#if USE_WP8_NATIVE_SQLITE
						return (Result)Sqlite3.sqlite3_open_v2(filename, out db, flags, "");
			#else
						return (Result)Sqlite3.sqlite3_open_v2(filename, out db, flags, null);
			#endif
					}
			
					public static Result Close(Sqlite3DatabaseHandle db)
					{
						return (Result)Sqlite3.sqlite3_close(db);
					}
			
					public static Result Close2(Sqlite3DatabaseHandle db)
					{
						return (Result)Sqlite3.sqlite3_close_v2(db);
					}
			
					public static Result BusyTimeout(Sqlite3DatabaseHandle db, int milliseconds)
					{
						return (Result)Sqlite3.sqlite3_busy_timeout(db, milliseconds);
					}
			
					public static int Changes(Sqlite3DatabaseHandle db)
					{
						return Sqlite3.sqlite3_changes(db);
					}
			
					public static Sqlite3Statement Prepare2(Sqlite3DatabaseHandle db, string query)
					{
						Sqlite3Statement stmt = default(Sqlite3Statement);
			#if USE_WP8_NATIVE_SQLITE || USE_SQLITEPCL_RAW
						var r = Sqlite3.sqlite3_prepare_v2(db, query, out stmt);
			#else
						stmt = new Sqlite3Statement();
						var r = Sqlite3.sqlite3_prepare_v2(db, query, -1, ref stmt, 0);
			#endif
						if (r != 0)
						{
							throw SQLiteException.New((Result)r, GetErrmsg(db));
						}
						return stmt;
					}
			
					public static Result Step(Sqlite3Statement stmt)
					{
						return (Result)Sqlite3.sqlite3_step(stmt);
					}
			
					public static Result Reset(Sqlite3Statement stmt)
					{
						return (Result)Sqlite3.sqlite3_reset(stmt);
					}
			
					public static Result Finalize(Sqlite3Statement stmt)
					{
						return (Result)Sqlite3.sqlite3_finalize(stmt);
					}
			
					public static long LastInsertRowid(Sqlite3DatabaseHandle db)
					{
						return Sqlite3.sqlite3_last_insert_rowid(db);
					}
			
					public static string GetErrmsg(Sqlite3DatabaseHandle db)
					{
						return Sqlite3.sqlite3_errmsg(db);
					}
			
					public static int BindParameterIndex(Sqlite3Statement stmt, string name)
					{
						return Sqlite3.sqlite3_bind_parameter_index(stmt, name);
					}
			
					public static int BindNull(Sqlite3Statement stmt, int index)
					{
						return Sqlite3.sqlite3_bind_null(stmt, index);
					}
			
					public static int BindInt(Sqlite3Statement stmt, int index, int val)
					{
						return Sqlite3.sqlite3_bind_int(stmt, index, val);
					}
			
					public static int BindInt64(Sqlite3Statement stmt, int index, long val)
					{
						return Sqlite3.sqlite3_bind_int64(stmt, index, val);
					}
			
					public static int BindDouble(Sqlite3Statement stmt, int index, double val)
					{
						return Sqlite3.sqlite3_bind_double(stmt, index, val);
					}
			
					public static int BindText(Sqlite3Statement stmt, int index, string val, int n, IntPtr free)
					{
			#if USE_WP8_NATIVE_SQLITE
						return Sqlite3.sqlite3_bind_text(stmt, index, val, n);
			#elif USE_SQLITEPCL_RAW
						return Sqlite3.sqlite3_bind_text(stmt, index, val);
			#else
						return Sqlite3.sqlite3_bind_text(stmt, index, val, n, null);
			#endif
					}
			
					public static int BindBlob(Sqlite3Statement stmt, int index, byte[] val, int n, IntPtr free)
					{
			#if USE_WP8_NATIVE_SQLITE
						return Sqlite3.sqlite3_bind_blob(stmt, index, val, n);
			#elif USE_SQLITEPCL_RAW
						return Sqlite3.sqlite3_bind_blob(stmt, index, val);
			#else
						return Sqlite3.sqlite3_bind_blob(stmt, index, val, n, null);
			#endif
					}
			
					public static int ColumnCount(Sqlite3Statement stmt)
					{
						return Sqlite3.sqlite3_column_count(stmt);
					}
			
					public static string ColumnName(Sqlite3Statement stmt, int index)
					{
						return Sqlite3.sqlite3_column_name(stmt, index);
					}
			
					public static string ColumnName16(Sqlite3Statement stmt, int index)
					{
						return Sqlite3.sqlite3_column_name(stmt, index);
					}
			
					public static ColType ColumnType(Sqlite3Statement stmt, int index)
					{
						return (ColType)Sqlite3.sqlite3_column_type(stmt, index);
					}
			
					public static int ColumnInt(Sqlite3Statement stmt, int index)
					{
						return Sqlite3.sqlite3_column_int(stmt, index);
					}
			
					public static long ColumnInt64(Sqlite3Statement stmt, int index)
					{
						return Sqlite3.sqlite3_column_int64(stmt, index);
					}
			
					public static double ColumnDouble(Sqlite3Statement stmt, int index)
					{
						return Sqlite3.sqlite3_column_double(stmt, index);
					}
			
					public static string ColumnText(Sqlite3Statement stmt, int index)
					{
						return Sqlite3.sqlite3_column_text(stmt, index);
					}
			
					public static string ColumnText16(Sqlite3Statement stmt, int index)
					{
						return Sqlite3.sqlite3_column_text(stmt, index);
					}
			
					public static byte[] ColumnBlob(Sqlite3Statement stmt, int index)
					{
						return Sqlite3.sqlite3_column_blob(stmt, index);
					}
			
					public static int ColumnBytes(Sqlite3Statement stmt, int index)
					{
						return Sqlite3.sqlite3_column_bytes(stmt, index);
					}
			
					public static string ColumnString(Sqlite3Statement stmt, int index)
					{
						return Sqlite3.sqlite3_column_text(stmt, index);
					}
			
					public static byte[] ColumnByteArray(Sqlite3Statement stmt, int index)
					{
						return ColumnBlob(stmt, index);
					}
			
			#if !USE_SQLITEPCL_RAW
					public static Result EnableLoadExtension(Sqlite3DatabaseHandle db, int onoff)
					{
						return (Result)Sqlite3.sqlite3_enable_load_extension(db, onoff);
					}
			#endif
			
					public static ExtendedResult ExtendedErrCode(Sqlite3DatabaseHandle db)
					{
						return (ExtendedResult)Sqlite3.sqlite3_extended_errcode(db);
					}
			#endif
			
					public enum ColType : int
					{
						Integer = 1,
						Float = 2,
						Text = 3,
						Blob = 4,
						Null = 5
					}
				}
			}
			
			#if NO_CONCURRENT
			namespace SQLite.Extensions
			{
			    public static class ListEx
			    {
			        public static bool TryAdd<TKey, TValue> (this IDictionary<TKey, TValue> dict, TKey key, TValue value)
			        {
			            try {
			                dict.Add (key, value);
			                return true;
			            }
			            catch (ArgumentException) {
			                return false;
			            }
			        }
			    }
			}
			#endif
			
			
			#endregion
			
			#region SQLiteAsync
			//
			// Copyright (c) 2012-2015 Krueger Systems, Inc.
			// 
			// Permission is hereby granted, free of charge, to any person obtaining a copy
			// of this software and associated documentation files (the "Software"), to deal
			// in the Software without restriction, including without limitation the rights
			// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
			// copies of the Software, and to permit persons to whom the Software is
			// furnished to do so, subject to the following conditions:
			// 
			// The above copyright notice and this permission notice shall be included in
			// all copies or substantial portions of the Software.
			// 
			// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
			// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
			// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
			// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
			// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
			// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
			// THE SOFTWARE.
			//
			
			
			// ReSharper disable once CheckNamespace
			namespace SQLite
			{
			    using System;
			    using System.Collections;
			    using System.Collections.Generic;
			    using System.Linq;
			    using System.Linq.Expressions;
			    using System.Threading;
			    using System.Threading.Tasks;
			
			    public partial class SQLiteAsyncConnection
				{
					SQLiteConnectionString _connectionString;
			        SQLiteOpenFlags _openFlags;
			
			        public SQLiteAsyncConnection(string databasePath, bool storeDateTimeAsTicks = true)
			            : this(databasePath, SQLiteOpenFlags.ReadWrite | SQLiteOpenFlags.Create, storeDateTimeAsTicks)
			        {
			        }
			        
			        public SQLiteAsyncConnection(string databasePath, SQLiteOpenFlags openFlags, bool storeDateTimeAsTicks = true)
			        {
			            _openFlags = openFlags;
			            _connectionString = new SQLiteConnectionString(databasePath, storeDateTimeAsTicks);
			        }
			
					public static void ResetPool()
					{
						SQLiteConnectionPool.Shared.Reset();
					}
			
					SQLiteConnectionWithLock GetConnection ()
					{
						return SQLiteConnectionPool.Shared.GetConnection (_connectionString, _openFlags);
					}
			
					public Task<CreateTablesResult> CreateTableAsync<T> (CreateFlags createFlags = CreateFlags.None)
						where T : new ()
					{
						return CreateTablesAsync (createFlags, typeof(T));
					}
			
					public Task<CreateTablesResult> CreateTablesAsync<T, T2> (CreateFlags createFlags = CreateFlags.None)
						where T : new ()
						where T2 : new ()
					{
						return CreateTablesAsync (createFlags, typeof (T), typeof (T2));
					}
			
					public Task<CreateTablesResult> CreateTablesAsync<T, T2, T3> (CreateFlags createFlags = CreateFlags.None)
						where T : new ()
						where T2 : new ()
						where T3 : new ()
					{
						return CreateTablesAsync (createFlags, typeof (T), typeof (T2), typeof (T3));
					}
			
					public Task<CreateTablesResult> CreateTablesAsync<T, T2, T3, T4> (CreateFlags createFlags = CreateFlags.None)
						where T : new ()
						where T2 : new ()
						where T3 : new ()
						where T4 : new ()
					{
						return CreateTablesAsync (createFlags, typeof (T), typeof (T2), typeof (T3), typeof (T4));
					}
			
					public Task<CreateTablesResult> CreateTablesAsync<T, T2, T3, T4, T5> (CreateFlags createFlags = CreateFlags.None)
						where T : new ()
						where T2 : new ()
						where T3 : new ()
						where T4 : new ()
						where T5 : new ()
					{
						return CreateTablesAsync (createFlags, typeof (T), typeof (T2), typeof (T3), typeof (T4), typeof (T5));
					}
			
					public Task<CreateTablesResult> CreateTablesAsync (CreateFlags createFlags = CreateFlags.None, params Type[] types)
					{
						return Task.Factory.StartNew (() => {
							CreateTablesResult result = new CreateTablesResult ();
							var conn = GetConnection ();
							using (conn.Lock ()) {
								foreach (Type type in types) {
									int aResult = conn.CreateTable (type, createFlags);
									result.Results[type] = aResult;
								}
							}
							return result;
						});
					}
			
					public Task<int> DropTableAsync<T> ()
						where T : new ()
					{
						return Task.Factory.StartNew (() => {
							var conn = GetConnection ();
							using (conn.Lock ()) {
								return conn.DropTable<T> ();
							}
						});
					}
			
					public Task<int> InsertAsync (object item)
					{
						return Task.Factory.StartNew (() => {
							var conn = GetConnection ();
							using (conn.Lock ()) {
								return conn.Insert (item);
							}
						});
					}
			
			        public Task<int> InsertOrReplaceAsync(object item)
			        {
			            return Task.Factory.StartNew(() =>
			            {
			                var conn = GetConnection();
			                using (conn.Lock())
			                {
			                    return conn.InsertOrReplace(item);
			                }
			            });
			        }
			
					public Task<int> UpdateAsync (object item)
					{
						return Task.Factory.StartNew (() => {
							var conn = GetConnection ();
							using (conn.Lock ()) {
								return conn.Update (item);
							}
						});
					}
			
					public Task<int> DeleteAsync (object item)
					{
						return Task.Factory.StartNew (() => {
							var conn = GetConnection ();
							using (conn.Lock ()) {
								return conn.Delete (item);
							}
						});
					}
			
			        public Task<T> GetAsync<T>(object pk)
			            where T : new()
			        {
			            return Task.Factory.StartNew(() =>
			            {
			                var conn = GetConnection();
			                using (conn.Lock())
			                {
			                    return conn.Get<T>(pk);
			                }
			            });
			        }
			
					public Task<T> FindAsync<T> (object pk)
						where T : new ()
					{
						return Task.Factory.StartNew (() => {
							var conn = GetConnection ();
							using (conn.Lock ()) {
								return conn.Find<T> (pk);
							}
						});
					}
					
					public Task<T> GetAsync<T> (Expression<Func<T, bool>> predicate)
			            where T : new()
			        {
			            return Task.Factory.StartNew(() =>
			            {
			                var conn = GetConnection();
			                using (conn.Lock())
			                {
			                    return conn.Get<T> (predicate);
			                }
			            });
			        }
			
					public Task<T> FindAsync<T> (Expression<Func<T, bool>> predicate)
						where T : new ()
					{
						return Task.Factory.StartNew (() => {
							var conn = GetConnection ();
							using (conn.Lock ()) {
								return conn.Find<T> (predicate);
							}
						});
					}
			
					public Task<int> ExecuteAsync (string query, params object[] args)
					{
						return Task<int>.Factory.StartNew (() => {
							var conn = GetConnection ();
							using (conn.Lock ()) {
								return conn.Execute (query, args);
							}
						});
					}
			
					public Task<int> InsertAllAsync (IEnumerable items)
					{
						return Task.Factory.StartNew (() => {
							var conn = GetConnection ();
							using (conn.Lock ()) {
								return conn.InsertAll (items);
							}
						});
					}
					
					public Task<int> UpdateAllAsync (IEnumerable items)
					{
						return Task.Factory.StartNew (() => {
							var conn = GetConnection ();
							using (conn.Lock ()) {
								return conn.UpdateAll (items);
							}
						});
					}
			
			  //      [Obsolete("Will cause a deadlock if any call in action ends up in a different thread. Use RunInTransactionAsync(Action<SQLiteConnection>) instead.")]
					//public Task RunInTransactionAsync (Action<SQLiteAsyncConnection> action)
					//{
					//	return Task.Factory.StartNew (() => {
					//		var conn = this.GetConnection ();
					//		using (conn.Lock ()) {
					//			conn.BeginTransaction ();
					//			try {
					//				action (this);
					//				conn.Commit ();
					//			}
					//			catch (Exception) {
					//				conn.Rollback ();
					//				throw;
					//			}
					//		}
					//	});
					//}
			
			        public Task RunInTransactionAsync(Action<SQLiteConnection> action)
			        {
			            return Task.Factory.StartNew(() =>
			            {
			                var conn = this.GetConnection();
			                using (conn.Lock())
			                {
			                    conn.BeginTransaction();
			                    try
			                    {
			                        action(conn);
			                        conn.Commit();
			                    }
			                    catch (Exception)
			                    {
			                        conn.Rollback();
			                        throw;
			                    }
			                }
			            });
			        }
			
					public AsyncTableQuery<T> Table<T> ()
						where T : new ()
					{
						//
						// This isn't async as the underlying connection doesn't go out to the database
						// until the query is performed. The Async methods are on the query iteself.
						//
						var conn = GetConnection ();
						return new AsyncTableQuery<T> (conn.Table<T> ());
					}
			
					public Task<T> ExecuteScalarAsync<T> (string sql, params object[] args)
					{
						return Task<T>.Factory.StartNew (() => {
							var conn = GetConnection ();
							using (conn.Lock ()) {
								var command = conn.CreateCommand (sql, args);
								return command.ExecuteScalar<T> ();
							}
						});
					}
			
					public Task<List<T>> QueryAsync<T> (string sql, params object[] args)
						where T : new ()
					{
						return Task<List<T>>.Factory.StartNew (() => {
							var conn = GetConnection ();
							using (conn.Lock ()) {
								return conn.Query<T> (sql, args);
							}
						});
					}
				}
			
				//
				// TODO: Bind to AsyncConnection.GetConnection instead so that delayed
				// execution can still work after a Pool.Reset.
				//
				public class AsyncTableQuery<T>
					where T : new ()
				{
					TableQuery<T> _innerQuery;
			
					public AsyncTableQuery (TableQuery<T> innerQuery)
					{
						_innerQuery = innerQuery;
					}
			
					public AsyncTableQuery<T> Where (Expression<Func<T, bool>> predExpr)
					{
						return new AsyncTableQuery<T> (_innerQuery.Where (predExpr));
					}
			
					public AsyncTableQuery<T> Skip (int n)
					{
						return new AsyncTableQuery<T> (_innerQuery.Skip (n));
					}
			
					public AsyncTableQuery<T> Take (int n)
					{
						return new AsyncTableQuery<T> (_innerQuery.Take (n));
					}
			
					public AsyncTableQuery<T> OrderBy<U> (Expression<Func<T, U>> orderExpr)
					{
						return new AsyncTableQuery<T> (_innerQuery.OrderBy<U> (orderExpr));
					}
			
					public AsyncTableQuery<T> OrderByDescending<U> (Expression<Func<T, U>> orderExpr)
					{
						return new AsyncTableQuery<T> (_innerQuery.OrderByDescending<U> (orderExpr));
					}
			
					public Task<List<T>> ToListAsync ()
					{
						return Task.Factory.StartNew (() => {
							using (((SQLiteConnectionWithLock)_innerQuery.Connection).Lock ()) {
								return _innerQuery.ToList ();
							}
						});
					}
			
					public Task<int> CountAsync ()
					{
						return Task.Factory.StartNew (() => {
							using (((SQLiteConnectionWithLock)_innerQuery.Connection).Lock ()) {
								return _innerQuery.Count ();
							}
						});
					}
			
					public Task<T> ElementAtAsync (int index)
					{
						return Task.Factory.StartNew (() => {
							using (((SQLiteConnectionWithLock)_innerQuery.Connection).Lock ()) {
								return _innerQuery.ElementAt (index);
							}
						});
					}
			
					public Task<T> FirstAsync ()
					{
						return Task<T>.Factory.StartNew(() => {
							using (((SQLiteConnectionWithLock)_innerQuery.Connection).Lock ()) {
								return _innerQuery.First ();
							}
						});
					}
			
					public Task<T> FirstOrDefaultAsync ()
					{
						return Task<T>.Factory.StartNew(() => {
							using (((SQLiteConnectionWithLock)_innerQuery.Connection).Lock ()) {
								return _innerQuery.FirstOrDefault ();
							}
						});
					}
			    }
			
				public class CreateTablesResult
				{
					public Dictionary<Type, int> Results { get; private set; }
			
					internal CreateTablesResult ()
					{
						this.Results = new Dictionary<Type, int> ();
					}
				}
			
				class SQLiteConnectionPool
				{
					class Entry
					{
						public SQLiteConnectionString ConnectionString { get; private set; }
						public SQLiteConnectionWithLock Connection { get; private set; }
			
			            public Entry (SQLiteConnectionString connectionString, SQLiteOpenFlags openFlags)
						{
							ConnectionString = connectionString;
							Connection = new SQLiteConnectionWithLock (connectionString, openFlags);
						}
			
						public void OnApplicationSuspended ()
						{
							Connection.Dispose ();
							Connection = null;
						}
					}
			
					readonly Dictionary<string, Entry> _entries = new Dictionary<string, Entry> ();
					readonly object _entriesLock = new object ();
			
					static readonly SQLiteConnectionPool _shared = new SQLiteConnectionPool ();
			
					/// <summary>
					/// Gets the singleton instance of the connection tool.
					/// </summary>
					public static SQLiteConnectionPool Shared
					{
						get
						{
							return _shared;
						}
					}
			
					public SQLiteConnectionWithLock GetConnection (SQLiteConnectionString connectionString, SQLiteOpenFlags openFlags)
					{
						lock (_entriesLock) {
							Entry entry;
							string key = connectionString.ConnectionString;
			
							if (!_entries.TryGetValue (key, out entry)) {
								entry = new Entry (connectionString, openFlags);
								_entries[key] = entry;
							}
			
							return entry.Connection;
						}
					}
			
					/// <summary>
					/// Closes all connections managed by this pool.
					/// </summary>
					public void Reset ()
					{
						lock (_entriesLock) {
							foreach (var entry in _entries.Values) {
								entry.OnApplicationSuspended ();
							}
							_entries.Clear ();
						}
					}
			
					/// <summary>
					/// Call this method when the application is suspended.
					/// </summary>
					/// <remarks>Behaviour here is to close any open connections.</remarks>
					public void ApplicationSuspended ()
					{
						Reset ();
					}
				}
			
				class SQLiteConnectionWithLock : SQLiteConnection
				{
					readonly object _lockPoint = new object ();
			
			        public SQLiteConnectionWithLock (SQLiteConnectionString connectionString, SQLiteOpenFlags openFlags)
						: base (connectionString.DatabasePath, openFlags, connectionString.StoreDateTimeAsTicks)
					{
					}
			
					public IDisposable Lock ()
					{
						return new LockWrapper (_lockPoint);
					}
			
					private class LockWrapper : IDisposable
					{
						object _lockPoint;
			
						public LockWrapper (object lockPoint)
						{
							_lockPoint = lockPoint;
							Monitor.Enter (_lockPoint);
						}
			
						public void Dispose ()
						{
							Monitor.Exit (_lockPoint);
						}
					}
				}
			}
			
			#endregion
			
			#region SqliteDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.Sqlite
			{
			    using System;
			    using System.Collections.Generic;
			    using System.IO;
			    using System.Linq;
			    using System.Text;
			    using System.Threading.Tasks;
			    using Core;
			    using Exceptions;
			    using Models;
			    using System.Runtime.Remoting.Messaging;
			    using SQLite;
			    using UnitOfWork;
			    using Xamarin.Forms;
			
			    #if WINDOWS_UWP
			    using Windows.Storage;
			    #endif
			    
			    public abstract class SqliteDataContext : DataContextBase, ISqlQueryProvider
			    {
			        #region ISqlQueryProvider implemetation
			        public virtual object GetIndex<ModelT>(int idxNum0To29, ModelT model)
			        {
			            if (idxNum0To29 < 0 || idxNum0To29 > 29) throw new SupermodelException("Only Indexes 0-29 are allowed");
			            if (idxNum0To29 >= 0 && idxNum0To29 <= 9) return GetStringIndex(idxNum0To29, model);
			            if (idxNum0To29 >= 10 && idxNum0To29 <= 19) return GetLongIndex(idxNum0To29 - 10, model);
			            if (idxNum0To29 >= 20 && idxNum0To29 <= 29) return GetDoubleIndex(idxNum0To29 - 20, model);
			            return null;
			        }
			        public virtual string GetStringIndex<ModelT>(int idxNum0To9, ModelT model)
			        {
			            if (idxNum0To9 < 0 || idxNum0To9 > 9) throw new SupermodelException("Only Indexes 0-9 are allowed");
			            return null;
			        }
			        public virtual long? GetLongIndex<ModelT>(int idxNum0To9, ModelT model)
			        {
			            if (idxNum0To9 < 0 || idxNum0To9 > 9) throw new SupermodelException("Only Indexes 0-9 are allowed");
			            return null;
			        }
			        public virtual double? GetDoubleIndex<ModelT>(int idxNum0To9, ModelT model)
			        {
			            if (idxNum0To9 < 0 || idxNum0To9 > 9) throw new SupermodelException("Only Indexes 0-9 are allowed");
			            return null;
			        }
			        public virtual string GetWhereClause<ModelT>(object searchBy, string sortBy)
			        {
			            return null;
			        }
			        public virtual string GetSkipAndTakeForWhereClause<ModelT>(int? skip, int? take)
			        {
			            var sb = new StringBuilder();
			            if (take != null) sb.Append(" LIMIT " + take);
			            if (skip != null) sb.Append(" OFFSET " + skip);
			            return sb.ToString();
			        }
			        #endregion
			
			        #region DataContext Reads
			        public override async Task<ModelT> GetByIdOrDefaultAsync<ModelT>(long id)
			        {
			            if (await InitDbAsync()) return null;
			
			            var db = new SQLiteAsyncConnection(DatabaseFilePath);
			            var modelTypeLogicalName = GetModelTypeLogicalName(typeof(ModelT));
			            var commandText = $"SELECT * FROM [{DataTableName}] WHERE ModelTypeLogicalName = '{modelTypeLogicalName}' AND ModelId = {id}";
			            var results = await db.QueryAsync<DataRow<ModelT>>(commandText);
			            if (results.Count == 0) return null;
			            if (results.Count > 1) throw new Exception("GetByIdOrDefaultAsync brought back more than one record");
			            var model = results.Single().GetModel();
			            ManagedModels.Add(new ManagedModel(model));
			            return model;
			        }
			        public override async Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            if (await InitDbAsync()) return new List<ModelT>();
			
			            var db = new SQLiteAsyncConnection(DatabaseFilePath);
			            var modelTypeLogicalName = GetModelTypeLogicalName(typeof(ModelT));
			            var commandText = $"SELECT * FROM [{DataTableName}] WHERE ModelTypeLogicalName = '{modelTypeLogicalName}'";
			            if (take != null) commandText += " LIMIT " + take;
			            if (skip != null) commandText += " OFFSET " + skip;
			            var results = await db.QueryAsync<DataRow<ModelT>>(commandText);
			            var models = new List<ModelT>();
			            foreach (var result in results)
			            {
			                var model = result.GetModel();
			                ManagedModels.Add(new ManagedModel(model));
			                models.Add(model);
			            }
			            return models;
			        }
			        public override async Task<long> GetCountAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            if (await InitDbAsync()) return 0;
			
			            var db = new SQLiteAsyncConnection(DatabaseFilePath);
			            var modelTypeLogicalName = GetModelTypeLogicalName(typeof(ModelT));
			            var commandText = $"SELECT COUNT(*) FROM [{DataTableName}] WHERE ModelTypeLogicalName='{modelTypeLogicalName}'";
			            if (take != null) commandText += " LIMIT " + take;
			            if (skip != null) commandText += " OFFSET " + skip;
			            var count = await db.ExecuteScalarAsync<long>(commandText);
			            return count;
			        }
			        #endregion
			
			        #region DataContext Queries
			        public override async Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null)
			        {
			            if (await InitDbAsync()) return new List<ModelT>();
			
			            var db = new SQLiteAsyncConnection(DatabaseFilePath);
			            var modelTypeLogicalName = GetModelTypeLogicalName(typeof(ModelT));
			            var whereClause = GetWhereClause<ModelT>(searchBy, sortBy);
			            if (whereClause == null) throw new SupermodelException("Must override GetWhereClause before running queries on localDb");
			            var fullWhereClause = whereClause + GetSkipAndTakeForWhereClause<ModelT>(skip, take);
			            var commandText = $"SELECT * FROM [{DataTableName}] WHERE ModelTypeLogicalName = '{modelTypeLogicalName}' {fullWhereClause}";
			
			            var results = await db.QueryAsync<DataRow<ModelT>>(commandText);
			            var models = new List<ModelT>();
			            foreach (var result in results)
			            {
			                var model = result.GetModel();
			                ManagedModels.Add(new ManagedModel(model));
			                models.Add(model);
			            }
			            return models;
			        }
			        public override async Task<long> GetCountWhereAsync<ModelT>(object searchBy, int? skip = null, int? take = null)
			        {
			            if (await InitDbAsync()) return 0;
			
			            var db = new SQLiteAsyncConnection(DatabaseFilePath);
			            var modelTypeLogicalName = GetModelTypeLogicalName(typeof(ModelT));
			            var whereClause = GetWhereClause<ModelT>(searchBy, null);
			            if (whereClause == null) throw new SupermodelException("Must override GetWhereClause before running queries on localDb");
			            var fullWhereClause = whereClause + GetSkipAndTakeForWhereClause<ModelT>(skip, take);
			            var commandText = $"SELECT COUNT(*) FROM [{DataTableName}] WHERE ModelTypeLogicalName = '{modelTypeLogicalName}' {fullWhereClause}";
			            var count = await db.ExecuteScalarAsync<long>(commandText);
			            return count;
			        }
			        #endregion
			
			        #region DataContext sqlite-specific writes
			        public void AddOrUpdate<ModelT>(ModelT model) where ModelT : class, IModel, new()
			        {
			            if (model.Id == 0) throw new SupermodelException("AddOrUpdate operation does not allow model id to be 0");
			            PendingActions.Add(new PendingAction
			            {
			                Operation = PendingAction.OperationEnum.AddOrUpdate,
			                ModelType = typeof(ModelT),
			                ModelId = model.Id,
			                OriginalModelId = model.Id,
			                Model = model,
			                DelayedValue = null,
			                SearchBy = null,
			                Skip = null,
			                Take = null,
			                SortBy = null
			            }.Validate());
			        }
			        #endregion
			
			        #region DataContext Save Changes
			        protected override async Task SaveChangesAsync(bool isFinal)
			        {
			            await InitDbAsync();
			            await base.SaveChangesAsync(isFinal);
			        }
			        public override async Task SaveChangesInternalAsync(List<PendingAction> pendingActions)
			        {
			            await InitDbAsync();
			
			            var db = new SQLiteAsyncConnection(DatabaseFilePath);
			            try
			            {
			                await db.RunInTransactionAsync(transaction => 
			                {
			                    for (var i = 0; i < pendingActions.Count; i++)
			                    {
			                        var pendingAction = pendingActions[i];
			                        
			                        //if we have multiple deletes in a row
			                        if (pendingAction.Operation == PendingAction.OperationEnum.Delete && i + 1 < pendingActions.Count && pendingActions[i + 1].Operation == PendingAction.OperationEnum.Delete)
			                        {
			                            var sb = new StringBuilder();
			                            sb.Append(pendingAction.GenerateSql(DataTableName, this));
			                            
			                            int j;
			                            var brokeFromLoop = false;
			                            for (j = 1; i + j < pendingActions.Count; j++)
			                            {
			                                var pendingActionIPlusJ = pendingActions[i + j];
			                                if (pendingActionIPlusJ.Operation != PendingAction.OperationEnum.Delete)
			                                {
			                                    brokeFromLoop = true;
			                                    break;
			                                }
			                                sb.AppendFormat(@" OR ModelTypeLogicalName = '{0}' AND ModelId = {1}", GetModelTypeLogicalName(pendingActionIPlusJ.ModelType), pendingActionIPlusJ.ModelId);
			                            }
			
			                            var bulkDeleteCommandText = sb.ToString();
			                            transaction.Execute(bulkDeleteCommandText);
			
			                            if (brokeFromLoop) i = i + j - 1;
			                            else i = i + j;
			                            
			                            continue;
			                        }
			
			                        //if we generate Id, we need to read the nextId first
			                        if (pendingAction.Operation == PendingAction.OperationEnum.GenerateIdAndAdd)
			                        {
			                            var nextIdCommandText = $"SELECT IFNULL(MIN(ModelId), 0)-1 FROM {DataTableName} WHERE ModelId < 0 AND ModelTypeLogicalName='{GetModelTypeLogicalName(pendingAction.ModelType)}'";
			                            var nextId = transaction.ExecuteScalar<long>(nextIdCommandText);
			                            pendingAction.Model.Id = nextId;
			                        }
			
			                        var commandText = pendingAction.GenerateSql(DataTableName, this);
			                        switch (pendingAction.SqlType)
			                        {
			                            case PendingAction.OperationSqlType.NoQueryResult:
			                            {
			                                transaction.Execute(commandText);
			                                pendingAction.ProcessDatabaseResponseAsync(DataTableName, null, this);
			                                break;
			                            }
			                            case PendingAction.OperationSqlType.SingleResultQuery:
			                            case PendingAction.OperationSqlType.ListResultQuery:
			                            {
			                                var results = transaction.Query<DataRow>(commandText);
			                                pendingAction.ProcessDatabaseResponseAsync(DataTableName, results, this);
			                                break;
			                            }
			                            case PendingAction.OperationSqlType.ScalarResultQuery:
			                            {
			                                var scalar = transaction.ExecuteScalar<long>(commandText);
			                                pendingAction.ProcessDatabaseResponseAsync(DataTableName, scalar, this);
			                                break;
			                            }
			                            default:
			                            {
			                                throw new SupermodelException("Unrecognized pendingAction.SqlType");
			                            }
			                        }
			                    }
			                });
			            }
			            catch (Exception)
			            {
			                //Rollback already happened by now
			                foreach (var pendingAction in pendingActions.Where(x => x.Operation == PendingAction.OperationEnum.GenerateIdAndAdd)) pendingAction.Model.Id = 0;
			                throw;
			            }
			        }
			        #endregion
			
			        #region Db Initialization and Migration
					public virtual int ContextSchemaVersion => 1;
			        public virtual async Task UpdateDbSchemaVersionAsync(int schemaVersion)
			        {
			            var db = new SQLiteAsyncConnection(DatabaseFilePath);
			            var commandText = string.Format(@"UPDATE [{0}]
			                                              SET 
			                                                  Json = '{3}', 
			                                                  BroughtFromMasterDbOnUtcTicks = {4},
			                                                  Index0 = NULL, 
			                                                  Index1 = NULL, 
			                                                  Index2 = NULL, 
			                                                  Index3 = NULL, 
			                                                  Index4 = NULL,
			                                                  Index5 = NULL, 
			                                                  Index6 = NULL, 
			                                                  Index7 = NULL, 
			                                                  Index8 = NULL, 
			                                                  Index9 = NULL,
			                                                  Index10 = NULL, 
			                                                  Index11 = NULL, 
			                                                  Index12 = NULL, 
			                                                  Index13 = NULL, 
			                                                  Index14 = NULL,
			                                                  Index15 = NULL, 
			                                                  Index16 = NULL, 
			                                                  Index17 = NULL, 
			                                                  Index18 = NULL, 
			                                                  Index19 = NULL,
			                                                  Index20 = NULL, 
			                                                  Index21 = NULL, 
			                                                  Index22 = NULL, 
			                                                  Index23 = NULL, 
			                                                  Index24 = NULL,
			                                                  Index25 = NULL, 
			                                                  Index26 = NULL, 
			                                                  Index27 = NULL, 
			                                                  Index28 = NULL, 
			                                                  Index29 = NULL
			                                              WHERE ModelTypeLogicalName = '{1}' AND ModelId = {2}",
			                                              DataTableName, SchemaVersionModelType, 0, schemaVersion, DateTime.UtcNow.Ticks);
			            await db.ExecuteAsync(commandText);
			        }
			        public virtual async Task<int?> GetDbSchemaVersionAsync()
			        {
			            var db = new SQLiteAsyncConnection(DatabaseFilePath);
			            var commandText = $"SELECT * FROM [{DataTableName}] WHERE ModelTypeLogicalName = '{SchemaVersionModelType}'";
			            var results = await db.QueryAsync<DataRow>(commandText);
			            if (results.Count == 0) return null;
			            if (results.Count > 1) return null;
			            var dbSchemaVersion = int.Parse(results.Single().Json);
			            return dbSchemaVersion;
			        }
			        public virtual async Task<bool> InitDbAsync()
			        {
			            //If this is called from inside of migration, don't initiaize or we get a deadlock
			            var supermodelMigrationInProgressOnThisThread = (bool?)CallContext.LogicalGetData("SupermodelSqliteMigrationInProgressOnThisThread") ?? false;
			            if (supermodelMigrationInProgressOnThisThread) return false;
			            
			            //check if we already initialized this run, if we did, don't bother
			            if (_finishedInitializingSqlLiteContext.Contains(GetType())) return false;
			            if (_startedInitializingSqlLiteContext.Contains(GetType()))
			            {
			                const int checkEveryMilliseconds = 250;
			                var totalWait = 0;
			                while (!_finishedInitializingSqlLiteContext.Contains(GetType()))
			                {
			                    await Task.Delay(checkEveryMilliseconds);
			                    totalWait += checkEveryMilliseconds;
			                    //If this was not resolved in 12 seconds, we fail (this should be nearly instanteneouse)
			                    if (totalWait >= 12 * 1000) throw new SupermodelException("Deadlock in InitDbAsync");
			                }
			                return false;
			            }
			            _startedInitializingSqlLiteContext.Add(GetType());
			            try
			            {
			                var newDb = await CreateDatabaseIfNotExistsAsync();
			                
			                //If the database is not new, check for migrations
			                if (!newDb)
			                {
			                    var dbSchemaVersion = await GetDbSchemaVersionAsync();
			                    if (ContextSchemaVersion != dbSchemaVersion)
			                    {
			                        CallContext.LogicalSetData("SupermodelSqliteMigrationInProgressOnThisThread", true);
			                        await MigrateDbAsync(dbSchemaVersion, ContextSchemaVersion);
			                        await UpdateDbSchemaVersionAsync(ContextSchemaVersion);
			                        CallContext.LogicalSetData("SupermodelSqliteMigrationInProgressOnThisThread", false);
			                    }
			                }
			                _finishedInitializingSqlLiteContext.Add(GetType());
			                return newDb;
			
			            }
			            catch (Exception)
			            {
			                _startedInitializingSqlLiteContext.Remove(GetType());
			                throw;
			            }
			        }
			        public virtual async Task ResetDatabaseAsync()
			        {
			            try
			            {
			                var db = new SQLiteAsyncConnection(DatabaseFilePath);
			
			                var commandText1 = $"DROP TABLE IF EXISTS [{DataTableName}]";
			
			                var commandText2 =
			                    $@"CREATE TABLE [{DataTableName}] (
			                    ModelTypeLogicalName text NOT NULL,
						        ModelId integer NOT NULL,
						        Json text NOT NULL,
						        BroughtFromMasterDbOnUtcTicks integer,
						        Index0 text,
						        Index1 text,
						        Index2 text,
						        Index3 text,
						        Index4 text,
						        Index5 text,
						        Index6 text,
						        Index7 text,
						        Index8 text,
						        Index9 text,
						        Index10 integer,
						        Index11 integer,
						        Index12 integer,
						        Index13 integer,
						        Index14 integer,
						        Index15 integer,
						        Index16 integer,
						        Index17 integer,
						        Index18 integer,
						        Index19 integer,
						        Index20 real,
						        Index21 real,
						        Index22 real,
						        Index23 real,
						        Index24 real,
						        Index25 real,
						        Index26 real,
						        Index27 real,
						        Index28 real,
						        Index29 real);";
			
			                var commandText3 = "CREATE UNIQUE INDEX UIX_Data_Data_ModelId ON Data (ModelTypeLogicalName ASC, ModelId ASC);";
						    var commandText4 = "CREATE INDEX IX_Data_BroughtFromMasterDbOnUtcTicks_ModelTypeLogicalName ON Data (BroughtFromMasterDbOnUtcTicks ASC, ModelTypeLogicalName ASC);";
						    var commandText5 = "CREATE INDEX IX_Data_Index0 ON Data (ModelTypeLogicalName ASC, Index0 ASC);";
						    var commandText6 = "CREATE INDEX IX_Data_Index1 ON Data (ModelTypeLogicalName ASC, Index1 ASC);";
						    var commandText7 = "CREATE INDEX IX_Data_Index2 ON Data (ModelTypeLogicalName ASC, Index2 ASC);";
						    var commandText8 = "CREATE INDEX IX_Data_Index3 ON Data (ModelTypeLogicalName ASC, Index3 ASC);";
						    var commandText9 = "CREATE INDEX IX_Data_Index4 ON Data (ModelTypeLogicalName ASC, Index4 ASC);";
						    var commandText10 = "CREATE INDEX IX_Data_Index5 ON Data (ModelTypeLogicalName ASC, Index5 ASC);";
						    var commandText11 = "CREATE INDEX IX_Data_Index6 ON Data (ModelTypeLogicalName ASC, Index6 ASC);";
						    var commandText12 = "CREATE INDEX IX_Data_Index7 ON Data (ModelTypeLogicalName ASC, Index7 ASC);";
						    var commandText13 = "CREATE INDEX IX_Data_Index8 ON Data (ModelTypeLogicalName ASC, Index8 ASC);";
						    var commandText14 = "CREATE INDEX IX_Data_Index9 ON Data (ModelTypeLogicalName ASC, Index9 ASC);";
						    var commandText15 = "CREATE INDEX IX_Data_Index10 ON Data (ModelTypeLogicalName ASC, Index10 ASC);";
						    var commandText16 = "CREATE INDEX IX_Data_Index11 ON Data (ModelTypeLogicalName ASC, Index11 ASC);";
						    var commandText17 = "CREATE INDEX IX_Data_Index12 ON Data (ModelTypeLogicalName ASC, Index12 ASC);";
						    var commandText18 = "CREATE INDEX IX_Data_Index13 ON Data (ModelTypeLogicalName ASC, Index13 ASC);";
						    var commandText19 = "CREATE INDEX IX_Data_Index14 ON Data (ModelTypeLogicalName ASC, Index14 ASC);";
						    var commandText20 = "CREATE INDEX IX_Data_Index15 ON Data (ModelTypeLogicalName ASC, Index15 ASC);";
						    var commandText21 = "CREATE INDEX IX_Data_Index16 ON Data (ModelTypeLogicalName ASC, Index16 ASC);";
						    var commandText22 = "CREATE INDEX IX_Data_Index17 ON Data (ModelTypeLogicalName ASC, Index17 ASC);";
						    var commandText23 = "CREATE INDEX IX_Data_Index18 ON Data (ModelTypeLogicalName ASC, Index18 ASC);";
						    var commandText24 = "CREATE INDEX IX_Data_Index19 ON Data (ModelTypeLogicalName ASC, Index19 ASC);";
						    var commandText25 = "CREATE INDEX IX_Data_Index20 ON Data (ModelTypeLogicalName ASC, Index20 ASC);";
						    var commandText26 = "CREATE INDEX IX_Data_Index21 ON Data (ModelTypeLogicalName ASC, Index21 ASC);";
						    var commandText27 = "CREATE INDEX IX_Data_Index22 ON Data (ModelTypeLogicalName ASC, Index22 ASC);";
						    var commandText28 = "CREATE INDEX IX_Data_Index23 ON Data (ModelTypeLogicalName ASC, Index23 ASC);";
						    var commandText29 = "CREATE INDEX IX_Data_Index24 ON Data (ModelTypeLogicalName ASC, Index24 ASC);";
						    var commandText30 = "CREATE INDEX IX_Data_Index25 ON Data (ModelTypeLogicalName ASC, Index25 ASC);";
						    var commandText31 = "CREATE INDEX IX_Data_Index26 ON Data (ModelTypeLogicalName ASC, Index26 ASC);";
						    var commandText32 = "CREATE INDEX IX_Data_Index27 ON Data (ModelTypeLogicalName ASC, Index27 ASC);";
						    var commandText33 = "CREATE INDEX IX_Data_Index28 ON Data (ModelTypeLogicalName ASC, Index28 ASC);";
						    var commandText34 = "CREATE INDEX IX_Data_Index29 ON Data (ModelTypeLogicalName ASC, Index29 ASC);";
			
			                var commandText35 =
			                    $@"INSERT INTO [{DataTableName}] (
			                    ModelTypeLogicalName, 
			                    ModelId, 
			                    Json, 
			                    BroughtFromMasterDbOnUtcTicks, 
			                    Index0, 
			                    Index1, 
			                    Index2, 
			                    Index3, 
			                    Index4,
			                    Index5, 
			                    Index6, 
			                    Index7, 
			                    Index8, 
			                    Index9,
			                    Index10, 
			                    Index11, 
			                    Index12, 
			                    Index13, 
			                    Index14,
			                    Index15, 
			                    Index16, 
			                    Index17, 
			                    Index18, 
			                    Index19,
			                    Index20, 
			                    Index21, 
			                    Index22, 
			                    Index23, 
			                    Index24,
			                    Index25, 
			                    Index26, 
			                    Index27, 
			                    Index28, 
			                    Index29) 
						        VALUES (
			                    '{SchemaVersionModelType}', 
			                    0, 
			                    '{ContextSchemaVersion}', 
			                    {DateTime.UtcNow.Ticks}, 
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL,
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL,
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL,
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL,
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL,
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL, 
			                    NULL)"; 
			                           
			                await db.ExecuteAsync(commandText1);
			                await db.ExecuteAsync(commandText2);
			                await db.ExecuteAsync(commandText3);
			                await db.ExecuteAsync(commandText4);
			                await db.ExecuteAsync(commandText5);
			                await db.ExecuteAsync(commandText6);
			                await db.ExecuteAsync(commandText7);
			                await db.ExecuteAsync(commandText8);
			                await db.ExecuteAsync(commandText9);
			                await db.ExecuteAsync(commandText10);
			                await db.ExecuteAsync(commandText11);
			                await db.ExecuteAsync(commandText12);
			                await db.ExecuteAsync(commandText13);
			                await db.ExecuteAsync(commandText14);
			                await db.ExecuteAsync(commandText15);
			                await db.ExecuteAsync(commandText16);
			                await db.ExecuteAsync(commandText17);
			                await db.ExecuteAsync(commandText18);
			                await db.ExecuteAsync(commandText19);
			                await db.ExecuteAsync(commandText20);
			                await db.ExecuteAsync(commandText21);
			                await db.ExecuteAsync(commandText22);
			                await db.ExecuteAsync(commandText23);
			                await db.ExecuteAsync(commandText24);
			                await db.ExecuteAsync(commandText25);
			                await db.ExecuteAsync(commandText26);
			                await db.ExecuteAsync(commandText27);
			                await db.ExecuteAsync(commandText28);
			                await db.ExecuteAsync(commandText29);
			                await db.ExecuteAsync(commandText30);
			                await db.ExecuteAsync(commandText31);
			                await db.ExecuteAsync(commandText32);
			                await db.ExecuteAsync(commandText33);
			                await db.ExecuteAsync(commandText34);
			                await db.ExecuteAsync(commandText35);
			            }
			            catch (Exception)
			            {
						    throw new SupermodelException("Fatal error: unable to intialize the database");
			            }
			        }
			        public virtual async Task<bool> DataTableExistsAsync()
			        {
			            var db = new SQLiteAsyncConnection(DatabaseFilePath);
			            var commandText = $"SELECT COUNT(*) FROM [sqlite_master] WHERE type='table' AND name='{DataTableName}';";
			            var count = await db.ExecuteScalarAsync<long>(commandText);
			            return count != 0;
			        }
			        public virtual async Task<bool> CreateDatabaseIfNotExistsAsync()
					{
			            if (await DataTableExistsAsync()) return false;
			            await ResetDatabaseAsync();
			
					    //we do this because, in a case when db is deleted but LastSynchDateTimeUtc is set, all records on the server will be deleted
					    Application.Current.Properties.Remove("smLastSynchDateTimeUtc");
					    await Application.Current.SavePropertiesAsync();
			
			            return true;
					}
			        public virtual async Task MigrateDbAsync(int? fromVersion, int toVersion)
					{
			            await UnitOfWorkContext.ResetDatabaseAsync();
			        }
			        #endregion
			
			        #region Properties and Contants
			        public string SchemaVersionModelType => DataTableName + ".LocalDb.SchemaVersion";
			
			        public virtual string DbFileName => "Supermodel.Mobile.db3";
			        public virtual string DataTableName => "Data";
			        public virtual string DatabaseFilePath
			        {
			            get
			            {
			                // ReSharper disable once RedundantAssignment
			                string libraryPath = null;
			                #if __IOS__
			                var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			                libraryPath = Path.Combine(documentsPath, "../Documents/"); // Documents folder
			                #endif
			
			                #if __ANDROID__
			                // Just use whatever directory SpecialFolder.Personal returns
			                libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			                #endif
			
			                #if WINDOWS_UWP
			                // Just use whatever directory ApplicationData.Current.LocalFolder.Path returns
			                libraryPath = ApplicationData.Current.LocalFolder.Path;
			                #endif
			
			                // ReSharper disable once AssignNullToNotNullAttribute
			                var path = Path.Combine(libraryPath, DbFileName);
			
			                return path;
			            }
			        }
			
			        // ReSharper disable InconsistentNaming
			        private static readonly HashSet<Type> _finishedInitializingSqlLiteContext = new HashSet<Type>();
			        private static readonly HashSet<Type> _startedInitializingSqlLiteContext = new HashSet<Type>();
			        // ReSharper restore InconsistentNaming
			        #endregion
			    }
			}
			#endregion
			
		#endregion
		
		#region WebApi
			
			#region IQueryStringProvider
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.WebApi
			{
			    public interface IQueryStringProvider
			    {
			        string GetQueryString(object searchBy, int? skip, int? take, string sortBy);
			    }
			}
			#endregion
			
			#region LoginResult
			 // ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.WebApi
			{
			    public class LoginResult
			    {
			        #region Contructors
			        public LoginResult(bool loginSuccessful, long? userId, string userLabel)
			        {
			            LoginSuccessful = loginSuccessful;
			            if (loginSuccessful)
			            {
			                UserId = userId;
			                UserLabel = userLabel;
			
			            }
			            else
			            {
			                UserId = null;
			                UserLabel = null;
			            }
			        }
			        #endregion
			
			        #region Properties
			        public bool LoginSuccessful { get; set; }
			        public long? UserId { get; set; }
			        public string UserLabel { get; set; }
			        #endregion
			    }
			}
			#endregion
			
			#region ValidateLoginResponse
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.WebApi
			{
			    public class ValidateLoginResponse
			    {
			        public long? UserId { get; set; }
			        public string UserLabel { get; set; }
			    }
			}
			#endregion
			
			#region WebApiDataContext
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.DataContext.WebApi
			{
			    using System;
			    using System.Collections.Generic;
			    using System.Linq;
			    using System.Net;
			    using System.Net.Http;
			    using System.Net.Http.Headers;
			    using System.Text;
			    using System.Threading.Tasks;
			    using Newtonsoft.Json;
			    using Core;
			    using Exceptions;
			    using UnitOfWork;
			    using Models;
			    using Encryptor;
			    using Utils;
			
			    #if __ANDROID__ || __IOS__ || WINDOWS_UWP
			    using Multipart;
			    using MultipartContent = Multipart.MultipartContent;
			    #endif
			
			    #if __ANDROID__ || __IOS__
			    using ModernHttpClient;
			    #endif
			
			    public abstract class WebApiDataContext : DataContextBase, IQueryStringProvider, IWebApiAuthorizationContext
			    {
			        #region Contructors
			        protected WebApiDataContext(AuthHeader authHeader = null)
			        {
			            AuthHeader = authHeader;
			        }
			        #endregion
			
			        #region IQueryStringProvider implemetation
			        public string GetQueryString(object searchBy, int? skip, int? take, string sortBy)
			        {
			            var qs = new StringBuilder();
			            qs.Append(searchBy.ToQueryString());
			            if (sortBy != null) qs.Append("&smSortBy=" + sortBy);
			            if (skip != null) qs.Append("&smSkip=" + skip);
			            if (take != null) qs.Append("&smTake=" + take);
			            return qs.ToString();
			        }
			        #endregion
			
			        #region ValidateLogin
			        public virtual async Task<LoginResult> ValidateLoginAsync<ModelT>() where ModelT : class, IModel
			        {
			            using (var httpClient = CreateHttpClient())
						{
			                var url = $"{GetModelTypeLogicalName<ModelT>()}/ValidateLogin";
			                var dataResponse = await httpClient.GetAsyncAndPreserveContext(url);
						    var dataResponseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync () : "";
						    if (dataResponse.IsSuccessStatusCode)
			                {
			                    var validateLoginResponse = JsonConvert.DeserializeObject<ValidateLoginResponse>(dataResponseContentStr);
			                    return new LoginResult(true, validateLoginResponse.UserId, validateLoginResponse.UserLabel);
			                }
			                if (dataResponse.StatusCode == HttpStatusCode.Unauthorized) return new LoginResult(false, null, null);
			                throw new SupermodelWebApiException(dataResponse.StatusCode, dataResponseContentStr);
						}
			        }
			        #endregion
			
			        #region DataContext Commands
			        public async Task<OutputT> ExecutePostAsync<InputT, OutputT>(string url, InputT input)
			            where InputT : class, new()
			            where OutputT : class, new()
			        {
			            using (var httpClient = CreateHttpClient())
			            {
			                var request = new HttpRequestMessage(HttpMethod.Post, $"{BaseUrl}{url}")
			                {
			                    Content = new StringContent(JsonConvert.SerializeObject(input), Encoding.UTF8, "application/json")
			                };
			                var response = await httpClient.SendAsyncAndPreserveContext(request);
			                var responseContentStr = response.Content != null ? await response.Content.ReadAsStringAsync() : "";
			                if (!response.IsSuccessStatusCode) ThrowSupermodelWebApiException(response.StatusCode, responseContentStr);
			                var output = JsonConvert.DeserializeObject<OutputT>(responseContentStr);
			                return output;
			            }
			        }
			        #endregion
			
			        #region DataContext Reads
			        public override async Task<ModelT> GetByIdOrDefaultAsync<ModelT>(long id)
			        {
			            var url = $"{GetModelTypeLogicalName<ModelT>()}/{id}";
			            return await GetJsonObjectAsync<ModelT>(url);
			        }
			        public override async Task<List<ModelT>> GetAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            var url = $"{GetModelTypeLogicalName<ModelT>()}/All?";
			            if (skip != null) url += "&smSkip=" + skip;
			            if (take != null) url += "&smTake=" + take;
			            return await GetJsonObjectsAsync<ModelT>(url);
			        }
			        public override async Task<long> GetCountAllAsync<ModelT>(int? skip = null, int? take = null)
			        {
			            var url = $"{GetModelTypeLogicalName<ModelT>()}/CountAll?";
			            if (skip != null) url += "&smSkip=" + skip;
			            if (take != null) url += "&smTake=" + take;
			            return await GetCountAsync(url);
			        }
			        #endregion
			
			        #region DataContext Queries
			        public override async Task<List<ModelT>> GetWhereAsync<ModelT>(object searchBy, string sortBy = null, int? skip = null, int? take = null)
			        {
			            var url = $"{GetModelTypeLogicalName<ModelT>()}/Where?{searchBy.ToQueryString()}";
			            if (sortBy != null) url += "&smSortBy=" + sortBy;
			            if (skip != null) url += "&smSkip=" + skip;
			            if (take != null) url += "&smTake=" + take;
			            return await GetJsonObjectsAsync<ModelT>(url);
			        }
			        public override async Task<long> GetCountWhereAsync<ModelT>(object searchBy, int? skip = null, int? take = null)
			        {
			            var url = $"{GetModelTypeLogicalName<ModelT>()}/CountWhere?{searchBy.ToQueryString()}";
			            if (skip != null) url += "&smSkip=" + skip;
			            if (take != null) url += "&smTake=" + take;
			            return await GetCountAsync(url);
			        }
			        #endregion
			
			        #region DataContext Save Changes
			        public override async Task SaveChangesInternalAsync(List<PendingAction> pendingActions)
			        {
			            using (var httpClient = CreateHttpClient())
			            {
			                HttpRequestMessage request;
			                if (pendingActions.Count == 1)
			                {
			                    request = pendingActions.Single().GenerateHttpRequest(BaseUrl, this);
			                }
			                else
			                {
			                    request = new HttpRequestMessage(HttpMethod.Post, BaseUrl + "$batch") { Content = new MultipartContent("mixed") };
			                    foreach (var pendingAction in pendingActions) ((MultipartContent)request.Content).Add(new HttpMessageContent(pendingAction.GenerateHttpRequest(BaseUrl, this)));
			                }
			
			                var response = await httpClient.SendAsyncAndPreserveContext(request);
			                var responseContentStr = response.Content != null ? await response.Content.ReadAsStringAsync() : "";
			                if (!response.IsSuccessStatusCode)
			                {
			                    //this is a special case for when an object does not exists and it's ok
			                    if (response.StatusCode == HttpStatusCode.NotFound && pendingActions.Count == 1 && pendingActions.Single().Operation == PendingAction.OperationEnum.DelayedGetByIdOrDefault)
			                    {
			                        //by this we indicate that this is not an transport error - it's an indication that object with id does not exists
			                        responseContentStr = null;
			                    }
			                    else if (response.StatusCode == HttpStatusCode.ExpectationFailed) //If validation error(s)
			                    {
			                        var validationError = JsonConvert.DeserializeObject<SupermodelDataContextValidationException.ValidationError>(responseContentStr);
			                        validationError.FailedAction = pendingActions.Single();
			                        ThrowSupermodelValidationException(validationError);
			                    }
			                    else
			                    {
			                        ThrowSupermodelWebApiException(response.StatusCode, responseContentStr);
			                    }
			                }
			
			                if (pendingActions.Count == 1)
			                {
			                    pendingActions.Single().ProcessHttpResponse(responseContentStr);
			                }
			                else
			                {
			                    var streamProvider = await response.Content.ReadAsMultipartAsync();
			                    if (pendingActions.Count != streamProvider.Contents.Count) throw new SupermodelException("Response does not match the request");
			                    var validationErrors = new List<SupermodelDataContextValidationException.ValidationError>();
			                    for (var i = 0; i < pendingActions.Count; i++)
			                    {
			                        var dataResponse = await streamProvider.Contents[i].ReadAsHttpResponseMessageAsync();
			                        var dataResponseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync() : "";
			                        if (!dataResponse.IsSuccessStatusCode)
			                        {
			                            //this is a special case for when an object does not exists and it's ok
			                            if (dataResponse.StatusCode == HttpStatusCode.NotFound && pendingActions[i].Operation == PendingAction.OperationEnum.DelayedGetByIdOrDefault)
			                            {
			                                //by this we indicate that this is not an transport error - it's an indication that object with id does not exists
			                                pendingActions[i].ProcessHttpResponse(null);
			                            }
			                            else if (dataResponse.StatusCode == HttpStatusCode.ExpectationFailed) //If validation error(s)
			                            {
			                                var validationError = JsonConvert.DeserializeObject<SupermodelDataContextValidationException.ValidationError>(dataResponseContentStr);
			                                validationErrors.Add(validationError);
			                            }
			                            else
			                            {
			                                ThrowSupermodelWebApiException(dataResponse.StatusCode, dataResponseContentStr);
			                            }
			                        }
			                        else
			                        {
			                            pendingActions[i].ProcessHttpResponse(dataResponseContentStr);
			                        }
			                    }
			                    if (validationErrors.Any()) ThrowSupermodelValidationException(validationErrors);
			                }
			            }
			        }
			        #endregion
			
			        #region Helpers
			        protected virtual HttpClient CreateHttpClient()
			        {
			            #if __ANDROID__ || __IOS__
			            var httpClient = new HttpClient(new NativeMessageHandler()) { BaseAddress = new Uri(BaseUrl) };
			            #else
			            var httpClient = new HttpClient { BaseAddress = new Uri(BaseUrl) };
			            #endif
			
						httpClient.DefaultRequestHeaders.Accept.Add(MediaTypeWithQualityHeaderValue.Parse(AcceptHeader));
			            if (AuthHeader != null) httpClient.DefaultRequestHeaders.Add(AuthHeader.HeaderName, AuthHeader.AuthToken);
			            return httpClient;
			        }
					protected void ThrowSupermodelWebApiException(HttpStatusCode statusCode, string content)
					{
						PrepareForThrowingException();
						throw new SupermodelWebApiException(statusCode, content);
					}
			        protected async Task<List<ModelT>> GetJsonObjectsAsync<ModelT>(string url) where ModelT : class, IModel, new()
			        {
			            var models = await GetJsonAnyObjectsAsync<ModelT>(url);
			            foreach (var model in models)
			            {
			                model.BroughtFromMasterDbOnUtc = DateTime.UtcNow;
			                model.AfterLoad();
			                ManagedModels.Add(new ManagedModel(model));
			            }
			            return models;
			        }
			        protected async Task<ModelT> GetJsonObjectAsync<ModelT>(string url) where ModelT : class, IModel, new()
			        {
			            var model = await GetJsonAnyObjectAsync<ModelT>(url);
			            if (model != null)
			            {
			                model.BroughtFromMasterDbOnUtc = DateTime.UtcNow;
			                model.AfterLoad();
			                ManagedModels.Add(new ManagedModel(model));
			            }
			            return model;
			        }
			        protected async Task<long> GetCountAsync(string url)
			        {
			            using (var httpClient = CreateHttpClient())
			            {
			                var dataResponse = await httpClient.GetAsyncAndPreserveContext(url);
							var dataResponseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync () : "";
							if (!dataResponse.IsSuccessStatusCode) throw new SupermodelWebApiException(dataResponse.StatusCode, dataResponseContentStr);
			                var count = JsonConvert.DeserializeObject<long>(dataResponseContentStr);
			                return count;
			            }
			        }
			        protected async Task<List<T>> GetJsonAnyObjectsAsync<T>(string url) where T: class
			        {
			            using (var httpClient = CreateHttpClient())
			            {
			                var dataResponse = await httpClient.GetAsyncAndPreserveContext(url);
			                var responseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync () : "";
			                if (!dataResponse.IsSuccessStatusCode) throw new SupermodelWebApiException(dataResponse.StatusCode, responseContentStr);
			                var models = JsonConvert.DeserializeObject<List<T>>(responseContentStr);
			                return models;
			            }
			        }
			        protected async Task<T> GetJsonAnyObjectAsync<T>(string url)  where T: class
			        {
			            using (var httpClient = CreateHttpClient())
			            {
			                var dataResponse = await httpClient.GetAsyncAndPreserveContext(url);
			                if (dataResponse.StatusCode == HttpStatusCode.NotFound) return null;
			                var dataResponseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync () : "";
			                if (!dataResponse.IsSuccessStatusCode) throw new SupermodelWebApiException (dataResponse.StatusCode, dataResponseContentStr);
			                var model = JsonConvert.DeserializeObject<T>(dataResponseContentStr);
			                return model;
			            }
			        }
			        protected async Task<ScalarValueT> GetScalarValueAsync<ScalarValueT>(string url)
			        {
			            using (var httpClient = CreateHttpClient())
			            {
			                var dataResponse = await httpClient.GetAsyncAndPreserveContext(url);
							var dataResponseContentStr = dataResponse.Content != null ? await dataResponse.Content.ReadAsStringAsync () : "";
							if (!dataResponse.IsSuccessStatusCode) throw new SupermodelWebApiException(dataResponse.StatusCode, dataResponseContentStr);
			                var count = JsonConvert.DeserializeObject<ScalarValueT>(dataResponseContentStr);
			                return count;
			            }
			        }
			        #endregion
			
			        #region Properties & Constants
			        public const string AcceptHeader = "application/json";
			        public AuthHeader AuthHeader { get; set; }
			        public abstract string BaseUrl { get; }
			        #endregion
			    }
			}
			#endregion
			
		#endregion
		
	#endregion
	
	#region Encryptor
		
		#region AuthHeader
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    public class AuthHeader
		    {
		        #region Constructors
		        public AuthHeader(string headerName, string authToken)
		        {
		            HeaderName = headerName;
		            AuthToken = authToken;
		        }
		        public AuthHeader(string authToken) : this("Authorization", authToken){}
		        #endregion
		
		        #region Properties
		        public string HeaderName { get; set; }
		        public string AuthToken { get; set; }
		        #endregion
		    }
		}
		#endregion
		
		#region Converter
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    using System;
		    using System.Text;
		
		    public static class Converter
		    {
		        public static byte[] StringToByteArr(string str)
		        {
		            return Encoding.Unicode.GetBytes(str);
		        }
		        public static string ByteArrToString(byte[] bytes)
		        {
		            return Encoding.Unicode.GetString(bytes);
		        }
		        
		        public static string ByteArrToBase64String(byte[] bytes)
		        {
		            return Convert.ToBase64String(bytes);
		        }
		        public static byte[] Base64StringToByteArr(string str)
		        {
		            return Convert.FromBase64String(str);
		        }
		        
		        public static string BinaryToHex(byte[] data)
		        {
		            if (data == null) return null;
		            var hex = new char[checked(data.Length * 2)];
		            for (var i = 0; i < data.Length; i++)
		            {
		                var thisByte = data[i];
		                hex[2 * i] = NibbleToHex((byte)(thisByte >> 4));        // high nibble
		                hex[2 * i + 1] = NibbleToHex((byte)(thisByte & 0xf));   // low nibble
		            }
		            return new string(hex);
		        }
		        public static char NibbleToHex(byte nibble)
		        {
		            return (char)((nibble < 10) ? (nibble + '0') : (nibble - 10 + 'A'));
		        }
		    }
		}
		#endregion
		
		#region EncryptorAgent
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    using System.Collections.Generic;
		    using System.IO;
		    using System.Text;
		    using PCLCrypto;
		
		    public static class EncryptorAgent
		    {
				public static byte[] Lock(byte[] key, string str, out byte[] iv)
				{
		            var aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
		            var cryptoKey = aes.CreateSymmetricKey(key);
		            iv = WinRTCrypto.CryptographicBuffer.GenerateRandom(16); //16 bytes is the stnadrd size for IV lenth for AES 256
		            var encryptor = WinRTCrypto.CryptographicEngine.CreateEncryptor(cryptoKey, iv);
		
		            using (var memstrm = new MemoryStream())
		            {
		                using (var csw = new CryptoStream(memstrm, encryptor, CryptoStreamMode.Write))
		                {
		                    csw.Write(Encoding.ASCII.GetBytes(str), 0, str.Length); //This breaks old code
		                    csw.FlushFinalBlock();
		                    var cryptdata = memstrm.ToArray();
		                    return cryptdata;
		                }
		            }
				}
		
		        public static byte[] Lock(byte[] key, string str, byte[] iv)
				{
				    var aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
				    var cryptoKey = aes.CreateSymmetricKey(key);
				    //iv = WinRTCrypto.CryptographicBuffer.GenerateRandom(16); //16 bytes is the stnadrd size for IV lenth for AES 256
				    var encryptor = WinRTCrypto.CryptographicEngine.CreateEncryptor(cryptoKey, iv);
				
				    using (var memstrm = new MemoryStream())
				    {
				        using (var csw = new CryptoStream(memstrm, encryptor, CryptoStreamMode.Write))
				        {
				            csw.Write(Encoding.ASCII.GetBytes(str), 0, str.Length); //This breaks old code
				            csw.FlushFinalBlock();
				            var cryptdata = memstrm.ToArray();
				            return cryptdata;
				        }
				    }
				}
		
		        public static byte[] LockUnicode(byte[] key, string str, out byte[] iv)
		        {
		            var aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
		            var cryptoKey = aes.CreateSymmetricKey(key);
		            iv = WinRTCrypto.CryptographicBuffer.GenerateRandom(16); //16 bytes is the stnadrd size for IV lenth for AES 256
		            var encryptor = WinRTCrypto.CryptographicEngine.CreateEncryptor(cryptoKey, iv);
		
		            using (var memstrm = new MemoryStream())
		            {
		                using (var csw = new CryptoStream(memstrm, encryptor, CryptoStreamMode.Write))
		                {
		                    csw.Write(Converter.StringToByteArr(str), 0, str.Length); //This breaks old code
		                    csw.FlushFinalBlock();
		                    var cryptdata = memstrm.ToArray();
		                    return cryptdata;
		                }
		            }
		        }
		
		        public static string Unlock(byte[] key, byte[] code, byte[] iv)
		        {
		            var aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
		            var cryptoKey = aes.CreateSymmetricKey(key);
		            var decryptor = WinRTCrypto.CryptographicEngine.CreateDecryptor(cryptoKey, iv);
		
		            using (var memstrm = new MemoryStream(code) { Position = 0 })
		            {
		                using (var csr = new CryptoStream(memstrm, decryptor, CryptoStreamMode.Read))
		                {
		                    var dataFragments = new List<byte[]>();
		                    var recv = 0;
		                    while (true)
		                    {
		                        var dataFragment = new byte[1024];
		                        var recvdThisFragment = csr.Read(dataFragment, 0, dataFragment.Length);
		                        if (recvdThisFragment == 0) break;
		                        dataFragments.Add(dataFragment);
		                        recv = recv + recvdThisFragment;
		                    }
		
		                    var data = new byte[dataFragments.Count * 1024];
		                    var idx = 0;
		
		                    //now let's build the entire data
		                    foreach (var t in dataFragments)
		                    {
		                        for (var j = 0; j < 1024; j++) data[idx++] = t[j];
		                    }
		
		                    var newphrase = Encoding.ASCII.GetString(data, 0, recv);
		                    return newphrase;
		                }
		            }
		        }
		
		        public static string UnlockUnicode(byte[] key, byte[] code, byte[] iv)
		        {
		            var aes = WinRTCrypto.SymmetricKeyAlgorithmProvider.OpenAlgorithm(SymmetricAlgorithm.AesCbcPkcs7);
		            var cryptoKey = aes.CreateSymmetricKey(key);
		            var decryptor = WinRTCrypto.CryptographicEngine.CreateDecryptor(cryptoKey, iv);
		
		            using (var memstrm = new MemoryStream(code) { Position = 0 })
		            {
		                using (var csr = new CryptoStream(memstrm, decryptor, CryptoStreamMode.Read))
		                {
		                    var dataFragments = new List<byte[]>();
		                    var recv = 0;
		                    while (true)
		                    {
		                        var dataFragment = new byte[1024];
		                        var recvdThisFragment = csr.Read(dataFragment, 0, dataFragment.Length);
		                        if (recvdThisFragment == 0) break;
		                        dataFragments.Add(dataFragment);
		                        recv = recv + recvdThisFragment;
		                    }
		
		                    var data = new byte[dataFragments.Count * 1024];
		                    var idx = 0;
		
		                    //now let's build the entire data
		                    foreach (var t in dataFragments)
		                    {
		                        for (var j = 0; j < 1024; j++) data[idx++] = t[j];
		                    }
		
		                    var newphrase = Encoding.Unicode.GetString(data, 0, recv);
		                    return newphrase;
		                }
		            }
		        }
		
		        //Sample key, 16 bytes
		        //private readonly static byte[] _key = { 0xA6, 0x46, 0x10, 0xF1, 0xEA, 0x16, 0x51, 0xA0, 0xB2, 0x41, 0x27, 0x5C, 0x23, 0x9C, 0xF0, 0xDD };
		    }
		}
		#endregion
		
		#region HashAgent
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    using System;
		    using System.Text;
		    using PCLCrypto;
		
		    public static class HashAgent
		    {
				public static string HashByteArray(byte[] data)
				{
		            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(HashAlgorithm.Sha1);
		            return Convert.ToBase64String(hasher.HashData(data));
				}
		
		        public static string GenerateGuidSalt()
		        {
		            return Guid.NewGuid().ToString();
		        }
		        public static string Generate256BitSalt()
				{
					return Convert.ToBase64String(GenerateBinary256BitSalt()); 
				}
		
		        public static byte[] GenerateBinaryGuidSalt()
		        {
		            return Converter.StringToByteArr(GenerateGuidSalt());
		        }
		        public static byte[] GenerateBinary256BitSalt ()
				{
					return WinRTCrypto.CryptographicBuffer.GenerateRandom(32); 
				}
		
		        public static string Generate5MinTimeStampSalt(DateTime dt, string format = "{0:|yyyy|M|d|H|m}")
		        {
		            //return $"{RoundUp5Min(dt):|yyyy|M|d|H|m}";
		            return string.Format(format, RoundUp5Min(dt));
		        }
		
		        public static string HashPasswordSHA1(string password, string salt)
		        {
		            return HashPassword(password, salt, HashAlgorithm.Sha1);
		        }
		        public static string HashPasswordSHA1Unicode(string password, string salt)
		        {
		            return HashPasswordUnicode(password, salt, HashAlgorithm.Sha1);
		        }
		        public static byte[] HashPasswordSHA1(string password, byte[] salt)
		        {
		            return HashPassword(password, salt, HashAlgorithm.Sha1);
		        }
		        public static byte[] HashPasswordSHA1Unicode(string password, byte[] salt)
		        {
		            return HashPasswordUnicode(password, salt, HashAlgorithm.Sha1);
		        }
		        
		        public static string HashPasswordMD5(string password, string salt)
		        {
		            return HashPassword(password, salt, HashAlgorithm.Md5);            
		        }
		        public static string HashPasswordMD5Unicode(string password, string salt)
		        {
		            return HashPasswordUnicode(password, salt, HashAlgorithm.Md5);
		        }
		        public static byte[] HashPasswordMD5(string password, byte[] salt)
		        {
		            return HashPassword(password, salt, HashAlgorithm.Md5);
		        }
		        public static byte[] HashPasswordMD5Unicode(string password, byte[] salt)
		        {
		            return HashPasswordUnicode(password, salt, HashAlgorithm.Md5);
		        }
		        
		        public static string HashPasswordSHA256(string password, string salt)
		        {
		            return HashPassword(password, salt, HashAlgorithm.Sha256);            
		        }
		        public static string HashPasswordSHA256Unicode(string password, string salt)
		        {
		            return HashPasswordUnicode(password, salt, HashAlgorithm.Sha256);
		        }
		        public static byte[] HashPasswordSHA256(string password, byte[] salt)
		        {
		            return HashPassword(password, salt, HashAlgorithm.Sha256);            
		        }
		        public static byte[] HashPasswordSHA256Unicode(string password, byte[] salt)
		        {
		            return HashPasswordUnicode(password, salt, HashAlgorithm.Sha256);
		        }
		
		        public static string HashPasswordSHA512(string password, string salt)
		        {
		            return HashPassword(password, salt, HashAlgorithm.Sha512);            
		        }
		        public static string HashPasswordSHA512Unicode(string password, string salt)
		        {
		            return HashPasswordUnicode(password, salt, HashAlgorithm.Sha512);
		        }
		        public static byte[] HashPasswordSHA512(string password, byte[] salt)
		        {
		            return HashPassword(password, salt, HashAlgorithm.Sha512);            
		        }
		        public static byte[] HashPasswordSHA512Unicode(string password, byte[] salt)
		        {
		            return HashPassword(password, salt, HashAlgorithm.Sha512);            
		        }
		
		        private static string HashPassword(string password, string salt, HashAlgorithm hashAlgorithm)
		        {
		            if (password == null) throw new ArgumentNullException(nameof(password));
		            if (salt == null) throw new ArgumentNullException(nameof(salt));
		
		            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(hashAlgorithm);
		            return Converter.BinaryToHex(hasher.HashData(Encoding.UTF8.GetBytes(password + salt))); //This breaks old code
		        }
		        private static string HashPasswordUnicode(string password, string salt, HashAlgorithm hashAlgorithm)
		        {
		            if (password == null) throw new ArgumentNullException(nameof(password));
		            if (salt == null) throw new ArgumentNullException(nameof(salt));
		
		            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(hashAlgorithm);
		            return Converter.BinaryToHex(hasher.HashData(Converter.StringToByteArr(password + salt)));
		        }
		        private static byte[] HashPassword(string password, byte[] salt, HashAlgorithm hashAlgorithm)
		        {
		            if (password == null) throw new ArgumentNullException(nameof(password));
		            if (salt == null) throw new ArgumentNullException(nameof(salt));
		
		            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(hashAlgorithm);
		            return hasher.HashData(Encoding.UTF8.GetBytes(password + salt)); //This breaks old code
		        }
		        private static byte[] HashPasswordUnicode(string password, byte[] salt, HashAlgorithm hashAlgorithm)
		        {
		            if (password == null) throw new ArgumentNullException(nameof(password));
		            if (salt == null) throw new ArgumentNullException(nameof(salt));
		
		            var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(hashAlgorithm);
		            return hasher.HashData(Converter.StringToByteArr(password + salt));
		        }
		        
		        private static DateTime RoundUp5Min(DateTime dt)
		        {
		            var d = TimeSpan.FromMinutes(5);
		            return new DateTime(((dt.Ticks + d.Ticks - 1) / d.Ticks) * d.Ticks);
		        }
		    }
		}
		#endregion
		
		#region HashExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    using System;
		    using System.IO;
		    using System.Runtime.Serialization;
		    using PCLCrypto;
		
		    public static class HashExtensions
		    {
		        public static string GetHash(this object instance, PCLCrypto.HashAlgorithm hashAlgorithm)
		        {
		            return ComputeHash(instance, hashAlgorithm);
		        }
		        //public static string GetKeyedHash<T>(this object instance, byte[] key) where T : KeyedHashAlgorithm, new()
		        //{
		        //    T cryptoServiceProvider = new T { Key = key };
		        //    return ComputeHash(instance, cryptoServiceProvider);
		        //}
		        public static string GetMD5Hash(this object instance)
		        {
		            return instance.GetHash(PCLCrypto.HashAlgorithm.Md5);
		        }
		        public static string GetSHA1Hash(this object instance)
		        {
		            return instance.GetHash(PCLCrypto.HashAlgorithm.Sha1);
		        }        
		        public static string GetSHA256Hash(this object instance)
		        {
		            return instance.GetHash(PCLCrypto.HashAlgorithm.Sha256);
		        }        
		        public static string GetSHA512Hash(this object instance)
		        {
		            return instance.GetHash(PCLCrypto.HashAlgorithm.Sha512);
		        }        
		        private static string ComputeHash(object instance, PCLCrypto.HashAlgorithm hashAlgorithm)
		        {
		            var serializer = new DataContractSerializer(instance.GetType());
		            using (var memoryStream = new MemoryStream())
		            {
		                serializer.WriteObject(memoryStream, instance);
		                var hasher = WinRTCrypto.HashAlgorithmProvider.OpenAlgorithm(hashAlgorithm);
		                return Convert.ToBase64String(hasher.HashData(memoryStream.ToArray()));
		            }
		        }
		    }
		}
		#endregion
		
		#region HttpAuthAgent
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Encryptor
		{
		    using System;
		    using System.Text;
		
			public static class HttpAuthAgent
		    {
		        #region Header Creation Methods
		        //example Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
		        public static AuthHeader CreateBasicAuthHeader(string username, string password)
		        {
		            return new AuthHeader("Basic " + CreateBasicAuthTokenCredentials(username, password));
		        }
		        public static AuthHeader CreateSMCustomEncryptedAuthHeader(byte[] key, params string[] args)
		        {
		            return new AuthHeader("SMCustomEncrypted " + CreateSMCustomEncryptedAuthTokenCredentials(key, args));
		        }
		
				public static string CreateBasicAuthToken(string username, string password)
				{
		            return "Basic " + CreateBasicAuthTokenCredentials(username, password);
				}
				public static string CreateSMCustomEncryptedAuthToken(byte[] key = null, params string[] args)
				{
		            return "SMCustomEncrypted " + CreateSMCustomEncryptedAuthTokenCredentials(key, args);
				}
		        #endregion
		
		        #region Header Read Methods
				public static void ReadBasicAuthToken(string authorizeHeader, out string username, out string password)
				{
				    if (authorizeHeader == null) throw new ArgumentNullException("authorizeHeader");
		            if (!authorizeHeader.StartsWith("Basic ")) throw new ArgumentException("'Basic' authorization scheme is expected");
				    
				    var credentials = authorizeHeader.Replace("Basic ", "");
		            ReadBasicAuthTokenCredentials(credentials, out username, out password);
				}
		        public static void ReadSMCustomEncryptedAuthToken(byte[] key, string authorizeHeader, out string[] args)
				{
				    if (authorizeHeader == null) throw new ArgumentNullException("authorizeHeader");
				    if (!authorizeHeader.StartsWith("SMCustomEncrypted ")) throw new ArgumentException("'SMCustomEncrypted' authorization scheme is expected");
		
		            var encryptedCredentials = authorizeHeader.Replace("SMCustomEncrypted ", "");
		            ReadSMCustomEncryptedAuthTokenCredentials(key, encryptedCredentials, out args);
				}
		        #endregion
		
		        #region Lower-level credential methods
		        public static string CreateBasicAuthTokenCredentials(string username, string password)
		        {
		            var payloadStr = string.Join(":", username, password);
		    		if (username.Contains(":") || password.Contains(":")) throw new ArgumentException("Username and Password cannot contain ':' character for Basic non-encrypted auth");
		            return Convert.ToBase64String(Encoding.UTF8.GetBytes(payloadStr));
		        }
		        public static string CreateSMCustomEncryptedAuthTokenCredentials(byte[] key, params string[] args)
		        {
		            var payloadStrBldr = new StringBuilder();
		            var first = true;
		            foreach (var param in args)
		            {
		                if (first)
		                {
		                    first = false;
		                    payloadStrBldr.Append(Converter.ByteArrToBase64String(Converter.StringToByteArr(param)));
		                }
		                else
		                {
		                    payloadStrBldr.Append(":" + Converter.ByteArrToBase64String(Converter.StringToByteArr(param)));
		                }
		            }
		            byte[] payloadIV;
		            var payloadCode = EncryptorAgent.Lock(key, payloadStrBldr.ToString(), out payloadIV);
		            
		            return Convert.ToBase64String(payloadCode) + "|" + Convert.ToBase64String(payloadIV);
		        }
		        
		        public static void ReadBasicAuthTokenCredentials(string credentials, out string username, out string password)
		        {
		            var parts = Encoding.UTF8.GetString(Convert.FromBase64String(credentials)).Split(':');   
				    if (parts.Length != 2) throw new ArgumentException("Authorization header is badly formatted");
				    username = parts[0].Trim();
				    password = parts[1].Trim();
		        }
		        public static void ReadSMCustomEncryptedAuthTokenCredentials(byte[] key, string encryptedCredentials, out string[] args)
		        {
		            if (key == null) throw new ArgumentException("key is required for parsing an encrypted auth header");
		            if (string.IsNullOrWhiteSpace(encryptedCredentials)) throw new ArgumentException("encryptedCredentials is required for parsing an encrypted auth header");
		
				    var encryptionParts = encryptedCredentials.Split('|');
				    if (encryptionParts.Length != 2) throw new ArgumentException("Authorization header is badly formatted");
		
		            var code = encryptionParts[0];
		            var iv = encryptionParts[1];
		
		            var payloadStr = EncryptorAgent.Unlock(key, Convert.FromBase64String(code), Convert.FromBase64String(iv));
		
		            var argsStr = payloadStr.Split(':');
		            args = new string[argsStr.Length];
		            for (var i=0; i < argsStr.Length; i++) args[i] = Converter.ByteArrToString(Converter.Base64StringToByteArr(argsStr[i]));
		        }
		        #endregion
		    }
		}
		#endregion
		
	#endregion
	
	#region Exceptions
		
		#region SupermodelDataContextValidationException
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Exceptions
		{
		    using System.Collections.Generic;
		    using System.ComponentModel.DataAnnotations;
		    using System.Linq;
		    using Newtonsoft.Json;
		    using DataContext.Core;
		    using ReflectionMapper;
		    
		    public class SupermodelDataContextValidationException : SupermodelException
		    {
		        #region Embedded Classes
		        public class ValidationError : List<ValidationError.Error>
		        {
		            #region Constructors
		            public ValidationError() { }
		            public ValidationError(IEnumerable<ValidationResult> validationResults, PendingAction failedAction, string message)
		            {
		                foreach (var validationResult in validationResults)
		                {
		                    foreach (var memberName in validationResult.MemberNames)
		                    {
		                        var existingError = this.SingleOrDefault(x => x.Name == memberName);
		                        if (existingError != null) existingError.ErrorMessages.Add(validationResult.ErrorMessage);
		                        else Add(new Error (memberName, validationResult.ErrorMessage) );
		                    }
		                }
		                FailedAction = failedAction;
		                Message = message;
		            }
		            #endregion
		
		            #region Embedded Classes
		            public class Error
		            {
		                #region Constructors
		                public Error(string name, List<string> errorMessages)
		                {
		                    Name = name;
		                    ErrorMessages = errorMessages;
		                }
		                public Error(string name, string errorMessage)
		                {
		                    Name = name;
		                    ErrorMessages = new List<string>{ errorMessage };
		                }
		                public Error()
		                {
		                    ErrorMessages = new List<string>();
		                }
		                #endregion
		
		                #region Properties
		                public string Name { get; set; }
		                public List<string> ErrorMessages { get; set; }
		                #endregion
		            }
		            #endregion
		
		            #region Properties
		            [JsonIgnore] public PendingAction FailedAction { get; set; }
		            public string Message { get; set; }
		            #endregion
		        }
		        #endregion
		
		        #region Constructors
		        public SupermodelDataContextValidationException(ValidationError validationError)
		        {
		            _validationErrors = new List<ValidationError>{validationError};
		        }
		        public SupermodelDataContextValidationException(List<ValidationError> validationErrors)
		        {
		            _validationErrors = validationErrors;
		        }
		        #endregion
		
		        #region Methods
		        protected virtual List<ValidationResultList> GetListOfValidationResultLists()
		        {
					var lvrl = new List<ValidationResultList>();    
		            foreach (var validationError in _validationErrors)
					{
		                var vrl = new ValidationResultList();
		                foreach (var error in validationError)
						{
		                    if (error.Name == "id") continue;
		                    if (error.Name.StartsWith("apiModelItem.")) error.Name = error.Name.Split('.').Last();
						    foreach (var errorMessage in error.ErrorMessages)
						    {
		                        var vr = new ValidationResult(errorMessage, new[] { error.Name });
		                        if (!VrlContainsVr(vrl, vr)) vrl.Add(vr);
						    }
						}
		                lvrl.Add(vrl);
		            }
		            return lvrl;
		        }
		        private bool VrlContainsVr(ValidationResultList vrl, ValidationResult newVr)
		        {
		            foreach (var vr in vrl)
		            {
		                if (AreVrsEqual(vr, newVr)) return true;
		            }
		            return false;
		        }
		        private bool AreVrsEqual(ValidationResult vr1, ValidationResult vr2)
		        {
		            if (vr1.ErrorMessage != vr2.ErrorMessage) return false;
		            var memberNames1 = vr1.MemberNames.ToArray();
		            var memberNames2 = vr2.MemberNames.ToArray();
		            if (memberNames1.Length != memberNames2.Length) return false;
		            for (var i = 0; i < memberNames1.Length; i++)
		            {
		                if (memberNames1[i] != memberNames2[i]) return false;
		            }
		            return true;
		        }
		        #endregion
		
		        #region Properties
		        public List<ValidationResultList> ValidationErrors => GetListOfValidationResultLists();
		        private readonly List<ValidationError> _validationErrors;
		        #endregion
		    }
		}
		#endregion
		
		#region SupermodelException
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Exceptions
		{
		    using System;
		    
		    public class SupermodelException : Exception
		    {
		        public SupermodelException() : this("N/A") { }
		        public SupermodelException(string msg) : base(/*ReflectionHelper.GetThrowingContext() + ": " + */msg) { }
		    }
		}
		#endregion
		
		#region SupermodelWebApiException
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Exceptions
		{
		    using Newtonsoft.Json;
		    using System.Net;
		    
		    public class SupermodelWebApiException : SupermodelException
		    {
		        #region Embedded Types
		        // ReSharper disable once ClassNeverInstantiated.Local
		        private class JsonMessage
		        {
		            public string Message { get; set; }
		        }
		        #endregion
		
		        public SupermodelWebApiException(HttpStatusCode statusCode, string content) : base((int)statusCode + ":" + statusCode + ". Content: " + content)
		        {
		            StatusCode = statusCode;
		            Content = content;
		        }
		
				public HttpStatusCode StatusCode { get; }
				public string Content { get; }
		        public string ContentJsonMessage => JsonConvert.DeserializeObject<JsonMessage>(Content).Message;
		    }
		}
		#endregion
		
	#endregion
	
	#region Misc
		
		#region DeviceInformation
		 // ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Misc
		{
		    #if __IOS__
		    using Foundation; 
		    using System.IO;
		    using System;
		    using UIKit;
		    using MonoTouch;
		    using Security;
		    #endif
		
		    #if __ANDROID__
		    using Java.Lang;
		    using Java.IO;
		    using Android.OS;
		    using Android.App;
		    #endif
		
		    #if WINDOWS_UWP
		    using System;
		    #endif
		    
		    public static class DeviceInformation
		    {
				public static bool IsRunningOnEmulator()
		        {
		            // ReSharper disable once ConvertToConstant.Local
		            var result = false;
		
		            #if __IOS__
		            if (ObjCRuntime.Runtime.Arch == ObjCRuntime.Arch.SIMULATOR) result = true;
		            #endif
		
		            #if __ANDROID__
		            if (Build.Fingerprint != null && (Build.Fingerprint.Contains("vbox") || Build.Fingerprint.Contains("generic"))) result = true;
		            #endif
		
		            #if WINDOWS_UWP
		            var deviceInfo = new Windows.Security.ExchangeActiveSyncProvisioning.EasClientDeviceInformation();
		            result = (deviceInfo.SystemProductName == "Virtual" && deviceInfo.SystemManufacturer == "Microsoft");
		            #endif
		
		            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
		            return result;
		        }
		        public static bool? IsJailbroken(bool returnNullIfNotSupported)
				{
		            if (IsRunningOnEmulator()) return null; //if we are not running on physical hardware return null (unknown)
		
		            // ReSharper disable once ConvertToConstant.Local
		            var result = false;
		
		            #if __IOS__
		            var paths = new [] {@"/Applications/Cydia.app", @"/Library/MobileSubstrate/MobileSubstrate.dylib", @"/bin/bash", @"/usr/sbin/sshd", @"/etc/apt" };
		            foreach (var path in paths)
		            {
		                if (NSFileManager.DefaultManager.FileExists(path)) result = true;
		            }
		
		            try
		            {
		                const string filename = @"/private/jailbreak.txt";
		                File.WriteAllText(filename, "This is a test.");
		                result = true;
		                File.Delete(filename);
		            }
		            // ReSharper disable once EmptyGeneralCatchClause
		            catch (Exception){} //if exception is thrown, we are not jailbroken
		
		            if (UIApplication.SharedApplication.CanOpenUrl(new NSUrl("cydia://package/com.exapmle.package"))) result = true;
		            #endif
		
		            #if __ANDROID__
		            var buildTags = Android.OS.Build.Tags;
		            if (buildTags != null && buildTags.Contains("test-keys")) result = true;
		
		            if (new File("/system/app/Superuser.apk").Exists()) result = true;
		
		            var paths = new[] { "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su" };
		            foreach(var path in paths) 
		            {
		                if (new File(path).Exists()) result = true;
		            }
		
		            try
		            {
		                using (var process = Runtime.GetRuntime().Exec(new [] { "/system/xbin/which", "su" }))
		                {
		                    var br = new BufferedReader(new InputStreamReader(process.InputStream));
		                    if (br.ReadLine() != null) result = true;
		                }
		            }
		            catch{}
		            #endif
		
		            #if WINDOWS_UWP
		            if (returnNullIfNotSupported) return null;
		            else throw new NotImplementedException("IsJailbroken() not implemented on Winodws Mobile");
		            #endif
		
				    // ReSharper disable once ConditionIsAlwaysTrueOrFalse
		            return result;
				}
		        public static bool? IsDeviceSecuredByPassocde(bool returnNullIfNotSupported)
		        {
		            if (IsRunningOnEmulator()) return null; //if we are not running on physical hardware return null (unknown)
		
		            // ReSharper disable once ConvertToConstant.Local
		            var result = false;
		            
		            #if __IOS__
		            const string text = "Supermodel.Mobile Passcode Test";
		            var record = new SecRecord(SecKind.GenericPassword) { Generic = NSData.FromString (text), Accessible = SecAccessible.WhenPasscodeSetThisDeviceOnly };
		            var status = SecKeyChain.Add(record);
		            if (status == SecStatusCode.Success || status == SecStatusCode.DuplicateItem)
		            {
		                result = true;
		                SecKeyChain.Remove(record);
		            }
		            #endif
		
		            #if __ANDROID__
		            var km = (KeyguardManager)Android.App.Application.Context.GetSystemService(Android.Content.Context.KeyguardService);
				    if (km.IsKeyguardSecure) result = true;
		            #endif
		
		            #if WINDOWS_UWP
		            if (returnNullIfNotSupported) return null;
		            else throw new NotImplementedException("IsDeviceSecuredByPassocde() not implemented on Winodws Mobile");
		            #endif
		            
		            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
		            return result;
		        }
		    }
		}
		#endregion
		
		#region RegexHelper
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Misc
		{
		    using System;
		    using System.Globalization;
		    using System.Text.RegularExpressions;
		
		    public class RegexHelper
		    {
		        // ReSharper disable RedundantDefaultFieldInitializer
		        private bool _invalid = false;
		        // ReSharper restore RedundantDefaultFieldInitializer
		
		        public bool IsValidEmail(string strIn)
		        {
		            _invalid = false;
		            if (String.IsNullOrEmpty(strIn)) return false;
		
		            // Use IdnMapping class to convert Unicode domain names.
		            strIn = Regex.Replace(strIn, @"(@)(.+)$", DomainMapper);
		            if (_invalid) return false;
		
		            // Return true if strIn is in valid e-mail format.
		            return Regex.IsMatch(strIn, EmailRegex, RegexOptions.IgnoreCase);
		        }
		
		        public static string EmailRegex
		        {
		            get { return @"^(?("")(""[^""]+?""@)|(([0-9A-Za-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9A-Za-z])@))(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9A-Za-z][-\w]*[0-9A-Za-z]*\.)+[a-zA-Z0-9]{2,17}))$"; }
		        }
		
		        private string DomainMapper(Match match)
		        {
		            // IdnMapping class with default property values.
		            var idn = new IdnMapping();
		
		            var domainName = match.Groups[2].Value;
		            try
		            {
		                domainName = idn.GetAscii(domainName);
		            }
		            catch (ArgumentException)
		            {
		                _invalid = true;
		            }
		            return match.Groups[1].Value + domainName;
		        }
		    }
		}
		#endregion
		
	#endregion
	
	#region Models
		
		#region BinaryFile
		 // ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Models
		{
			using System;
		    
		    public class BinaryFile
			{
				public String Name { get; set; }
				public Byte[] BinaryContent { get; set; }
			}
		}
		#endregion
		
		#region ChildModel
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Models
		{
		    using ReflectionMapper;
		    using XForms.ViewModels;
		    using System;
		    using System.ComponentModel;
		    using Xamarin.Forms;
		    using Newtonsoft.Json;
		
		    public abstract class ChildModel: ISupermodelListTemplate
		    {
		        #region Overrdies
		        [JsonIgnore, NotRCompared] public virtual Guid[] ParentGuidIdentities { get; set; }
		        [JsonIgnore, NotRCompared] public virtual Guid ChildGuidIdentity { get; set; } = Guid.NewGuid();
		
		        public virtual DataTemplate GetListCellDataTemplate(EventHandler deleteItemHandler)
		        {
		            var dataTemplate = new DataTemplate(() =>
		            {
		                var cell = ReturnACell();
		                if (deleteItemHandler != null)
		                {
		                    var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true, Parent = cell };
		                    deleteAction.SetBinding(MenuItem.CommandParameterProperty, new Binding("."));
		                    cell.ContextActions.Add(deleteAction);
		                    deleteAction.Clicked += deleteItemHandler;
		                }
		                return cell;
		            });
		            SetUpBindings(dataTemplate);
		            return dataTemplate;
		        }
		        public virtual Cell ReturnACell()
		        {
		            var msg = $"In order to use '{GetType().Name}' class with CRUD features, you must override ReturnACell() and SetUpBindings() methods!";
		            throw new NotImplementedException(msg);
		        }
		        public virtual void SetUpBindings(DataTemplate dataTemplate)
		        {
		            var msg = $"In order to use '{GetType().Name}' class with CRUD features, you must override ReturnACell() and SetUpBindings() methods!";
		            throw new NotImplementedException(msg);
		        }
		
		        public event PropertyChangedEventHandler PropertyChanged;
		        public virtual void OnPropertyChanged(string propertyName)
		        {
		            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		        }
		        #endregion
		    }
		}
		#endregion
		
		#region DirectChildModel
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Models
		{
		    using System;
		
		    public abstract class DirectChildModel : ChildModel
		    {
		        protected DirectChildModel()
		        {
		            // ReSharper disable once VirtualMemberCallInConstructor
		            ParentGuidIdentities = new Guid[0];
		        }
		    }
		}
		#endregion
		
		#region IModel
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Models
		{
		    using System;
		    using System.ComponentModel.DataAnnotations;
		    using DataContext.Core;
		    using System.Collections.Generic;
		
		    public interface IModel : /*IObjectWithIdentity,*/ IValidatableObject
		    {
		        long Id { get; set; }
		        
		        DateTime? BroughtFromMasterDbOnUtc { get; set; }
		
		        bool IsNew { get; }
		        
		        //IModel PerpareForSerializingForMasterDb();
		        //IModel PerpareForSerializingForLocalDb();
		
		        void Add();
		        void Delete();
		        void Update();
		
		        void BeforeSave(PendingAction.OperationEnum operation);
		        void AfterLoad();
		
		        List<ChildModelT> GetChildList<ChildModelT>(params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new();
		        ChildModelT GetChild<ChildModelT>(Guid childGuidIdentity, params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new();
		        ChildModelT GetChildOrDefault<ChildModelT>(Guid childGuidIdentity, params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new();
		        void AddChild<ChildModelT>(ChildModelT child, int? index = null) where ChildModelT : ChildModel, new();
		        int DeleteChild<ChildModelT>(ChildModelT child) where ChildModelT : ChildModel, new();
		    }
		}
		
		#endregion
		
		#region Model
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Models
		{
		    using System;
		    using System.Collections.Generic;
		    using System.ComponentModel.DataAnnotations;
		    using Newtonsoft.Json;
		    using Repository;
		    using DataContext.Core;
		    using ReflectionMapper;
		    using XForms.ViewModels;
		    using System.ComponentModel;
		    using Xamarin.Forms;
		    using DataContext.Sqlite;
		    using Exceptions;
		    using UnitOfWork;
		    using System.Linq;
		
		    public abstract class Model : IModel, ISupermodelListTemplate
		    {
		        #region Methods
		        public virtual List<ChildModelT> GetChildList<ChildModelT>(params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new()
		        {
		            throw new NotImplementedException("If Model has children, you must overrdie GetChildList<ChildModelT>");
		        }
		        public virtual ChildModelT GetChild<ChildModelT>(Guid childGuidIdentity, params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new()
		        {
		            if (parentGuidIdentities == null) throw new ArgumentNullException(nameof(parentGuidIdentities), "Overrdie AfterLoad() on root Model and assign parent adentities for each child");
		            var child = GetChildOrDefault<ChildModelT>(childGuidIdentity, parentGuidIdentities);
		            if (child == null) throw new InvalidOperationException("No element satisfies the condition in predicate.");
		            return child;
		        }
		        public virtual ChildModelT GetChildOrDefault<ChildModelT>(Guid childGuidIdentity, params Guid[] parentGuidIdentities) where ChildModelT : ChildModel, new()
		        {
		            if (parentGuidIdentities == null) throw new ArgumentNullException(nameof(parentGuidIdentities), "Overrdie AfterLoad() on root Model and assign parent adentities for each child");
		            var child = GetChildList<ChildModelT>(parentGuidIdentities).SingleOrDefault(x => x.ChildGuidIdentity == childGuidIdentity);
		            return child;
		        }
		        public virtual void AddChild<ChildModelT>(ChildModelT child, int? index = null) where ChildModelT : ChildModel, new()
		        {
		            if (child == null) throw new ArgumentNullException(nameof(child));
		            if (child.ParentGuidIdentities == null) throw new ArgumentNullException(nameof(child.ParentGuidIdentities), "Overrdie AfterLoad() on root Model and assign ParentIdentities for each child");
		            if (GetChildOrDefault<ChildModelT>(child.ChildGuidIdentity, child.ParentGuidIdentities) != null) throw new SupermodelException("Model.AddChild<ChildModelT>(): Attempting to add a duplicate child");
		            GetChildList<ChildModelT>(child.ParentGuidIdentities).Add(child);
		        }
		        public virtual int DeleteChild<ChildModelT>(ChildModelT child) where ChildModelT : ChildModel, new()
		        {
		            if (child == null) throw new ArgumentNullException(nameof(child));
		            if (child.ParentGuidIdentities == null) throw new ArgumentNullException(nameof(child.ParentGuidIdentities), "Overrdie AfterLoad() on root Model and assign ParentIdentities for each child");
		            var index = GetChildList<ChildModelT>(child.ParentGuidIdentities).IndexOf(child);
		            if (index < 0) throw new SupermodelException("DeleteChild(): Element not foubnd");
		            GetChildList<ChildModelT>(child.ParentGuidIdentities).RemoveAt(index);
		            return index;
		        }
		
		        public virtual void Add()
		        {
		            CreateRepo().ExecuteMethod("Add", this);
		        }
		        public virtual void Delete()
		        {
		            CreateRepo().ExecuteMethod("Delete", this);
		        }
		        public virtual void Update()
		        {
		            CreateRepo().ExecuteMethod("ForceUpdate", this);
		        }
		
		        public virtual void BeforeSave(PendingAction.OperationEnum operation)
		        {
		            //default is doing nothing
		        }
		        public virtual void AfterLoad()
		        {
		            //default is doing nothing
		        }
		
		        public virtual object CreateRepo()
		        {
		            return RepoFactory.CreateForRuntimeType(GetType());
		        }
		        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		        {
		            return new ValidationResultList();
		        }
		        #endregion
		
		        #region ISupermodelListTemplate implemetation
		        public virtual DataTemplate GetListCellDataTemplate(EventHandler deleteItemHandler)
		        {
		            var dataTemplate = new DataTemplate(() =>
		            {
		                var cell = ReturnACell();
		                if (deleteItemHandler != null)
		                {
		                    var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true };
		                    deleteAction.SetBinding(MenuItem.CommandParameterProperty, new Binding("."));
		                    cell.ContextActions.Add(deleteAction);
		                    deleteAction.Clicked += deleteItemHandler;
		                }
		                return cell;
		            });
		            SetUpBindings(dataTemplate);
		            return dataTemplate;
		        }
		        public virtual Cell ReturnACell()
		        {
		            var msg = $"In order to use '{GetType().Name}' class with CRUD features, you must override ReturnACell() and SetUpBindings() methods!";
		            throw new NotImplementedException(msg);
		        }
		        public virtual void SetUpBindings(DataTemplate dataTemplate)
		        {
		            var msg = $"In order to use '{GetType().Name}' class with CRUD features, you must override ReturnACell() and SetUpBindings() methods!";
		            throw new NotImplementedException(msg);
		        }
		
		        public event PropertyChangedEventHandler PropertyChanged;
		        public virtual void OnPropertyChanged(string propertyName)
		        {
		            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		        }
		        #endregion
		
		        #region Properties
		        public long Id { get; set; }
		
		        [NotRMapped] public DateTime? BroughtFromMasterDbOnUtc { get; set; }
		        public bool ShouldSerializeBroughtFromMasterDbOnUtc()
		        {
		            if (UnitOfWorkContextCore.StackCount == 0) return false;
		            return UnitOfWorkContextCore.CurrentDataContext is SqliteDataContext;
		        }
		        //public bool ShouldSerializeBroughtFromMasterDbOnUtc() { return !SerializingForMasterDb; }
		        //public IModel PerpareForSerializingForMasterDb() { SerializingForMasterDb = true; return this; }
		        //public IModel PerpareForSerializingForLocalDb() { SerializingForMasterDb = false; return this; }
		        //[JsonIgnore] protected bool SerializingForMasterDb { get; set; }
		
		        [JsonIgnore, NotRMapped] public virtual bool IsNew => Id == 0;
		        #endregion
		    }
		}
		#endregion
		
		#region TableSectionDefinition
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Models
		{
		    public class TableSectionDefinition
		    {
		        public TableSectionDefinition() { }
		        public TableSectionDefinition(string title, int screenOrderFrom, int screenOrderTo)
		        {
		            Title = title;
		            ScreenOrderFrom = screenOrderFrom;
		            ScreenOrderTo = screenOrderTo;
		        }
		
		
		        public string Title { get; set; }
		        public int ScreenOrderFrom { get; set; }
		        public int ScreenOrderTo { get; set; }
		    }
		}
		#endregion
		
	#endregion
	
	#region Multipart
		
		#region FormattingUtilities
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Globalization;
		    using System.Linq;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using Newtonsoft.Json.Linq;
		    using System.Reflection;
		    
		    public static class FormattingUtilities
		    {
		        // Supported date formats for input.
		        private static readonly string[] _dateFormats =
		        {
		            // "r", // RFC 1123, required output format but too strict for input
		            "ddd, d MMM yyyy H:m:s 'GMT'", // RFC 1123 (r, except it allows both 1 and 01 for date and time)
		            "ddd, d MMM yyyy H:m:s", // RFC 1123, no zone - assume GMT
		            "d MMM yyyy H:m:s 'GMT'", // RFC 1123, no day-of-week
		            "d MMM yyyy H:m:s", // RFC 1123, no day-of-week, no zone
		            "ddd, d MMM yy H:m:s 'GMT'", // RFC 1123, short year
		            "ddd, d MMM yy H:m:s", // RFC 1123, short year, no zone
		            "d MMM yy H:m:s 'GMT'", // RFC 1123, no day-of-week, short year
		            "d MMM yy H:m:s", // RFC 1123, no day-of-week, short year, no zone
		
		            "dddd, d'-'MMM'-'yy H:m:s 'GMT'", // RFC 850
		            "dddd, d'-'MMM'-'yy H:m:s", // RFC 850 no zone
		            "ddd MMM d H:m:s yyyy", // ANSI C's asctime() format
		
		            "ddd, d MMM yyyy H:m:s zzz", // RFC 5322
		            "ddd, d MMM yyyy H:m:s", // RFC 5322 no zone
		            "d MMM yyyy H:m:s zzz", // RFC 5322 no day-of-week
		            "d MMM yyyy H:m:s" // RFC 5322 no day-of-week, no zone
		        };
		
		        // Valid header token characters are within the range 0x20 < c < 0x7F excluding the following characters
		        private const string NonTokenChars = "()<>@,;:\\\"/[]?={}";
		
		        public const double Match = 1.0;
		        public const double NoMatch = 0.0;
		        public const int DefaultMaxDepth = 256;
		        public const int DefaultMinDepth = 1;
		        public const string HttpRequestedWithHeader = @"x-requested-with";
		        public const string HttpRequestedWithHeaderValue = @"XMLHttpRequest";
		        public const string HttpHostHeader = "Host";
		        public const string HttpVersionToken = "HTTP";
		        public static readonly Type HttpRequestMessageType = typeof(HttpRequestMessage);
		        public static readonly Type HttpResponseMessageType = typeof(HttpResponseMessage);
		        public static readonly Type HttpContentType = typeof(HttpContent);
		        //public static readonly Type DelegatingEnumerableGenericType = typeof(DelegatingEnumerable<>);
		        public static readonly Type EnumerableInterfaceGenericType = typeof(IEnumerable<>);
		        public static readonly Type QueryableInterfaceGenericType = typeof(IQueryable<>);
		        public static bool IsJTokenType(Type type)
		        {
		            return typeof(JToken).IsAssignableFrom(type);
		        }
		        public static HttpContentHeaders CreateEmptyContentHeaders()
		        {
		            HttpContent tempContent = null;
		            HttpContentHeaders contentHeaders;
		            try
		            {
		                tempContent = new StringContent(String.Empty);
		                contentHeaders = tempContent.Headers;
		                contentHeaders.Clear();
		            }
		            finally
		            {
		                // We can dispose the content without touching the headers
		                if (tempContent != null) tempContent.Dispose();
		            }
		
		            return contentHeaders;
		        }
		        public static string UnquoteToken(string token)
		        {
		            if (String.IsNullOrWhiteSpace(token)) return token;
		            if (token.StartsWith("\"", StringComparison.Ordinal) && token.EndsWith("\"", StringComparison.Ordinal) && token.Length > 1) return token.Substring(1, token.Length - 2);
		            return token;
		        }
		
		        public static bool ValidateHeaderToken(string token)
		        {
		            if (token == null) return false;
		            foreach (var c in token)
		            {
		                if (c < 0x21 || c > 0x7E || NonTokenChars.IndexOf(c) != -1) return false;
		            }
		            return true;
		        }
		
		        public static string DateToString(DateTimeOffset dateTime)
		        {
		            // Format according to RFC1123; 'r' uses invariant info (DateTimeFormatInfo.InvariantInfo)
		            return dateTime.ToUniversalTime().ToString("r", CultureInfo.InvariantCulture);
		        }
		
		        public static bool TryParseDate(string input, out DateTimeOffset result)
		        {
		            return DateTimeOffset.TryParseExact(input, _dateFormats, DateTimeFormatInfo.InvariantInfo,
		                                                DateTimeStyles.AllowWhiteSpaces | DateTimeStyles.AssumeUniversal,
		                                                out result);
		        }
		
		        public static bool TryParseInt32(string value, out int result)
		        {
		            return Int32.TryParse(value, NumberStyles.None, NumberFormatInfo.InvariantInfo, out result);
		        }    
		    }
		}
		#endregion
		
		#region HttpContentMessageExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.Globalization;
		    using System.IO;
		    using System.Linq;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public static class HttpContentMessageExtensions
		    {
		        private const int MinBufferSize = 256;
		        private const int DefaultBufferSize = 32 * 1024;
		
		        public static bool IsHttpRequestMessageContent(this HttpContent content)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		
		            try
		            {
		                return HttpMessageContent.ValidateHttpMessageContent(content, true, false);
		            }
		            catch (Exception)
		            {
		                return false;
		            }
		        }
		
		        public static bool IsHttpResponseMessageContent(this HttpContent content)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            try
		            {
		                return HttpMessageContent.ValidateHttpMessageContent(content, false, false);
		            }
		            catch (Exception)
		            {
		                return false;
		            }
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content)
		        {
		            return ReadAsHttpRequestMessageAsync(content, "http", DefaultBufferSize);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, CancellationToken cancellationToken)
		        {
		            return ReadAsHttpRequestMessageAsync(content, "http", DefaultBufferSize, cancellationToken);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, string uriScheme)
		        {
		            return ReadAsHttpRequestMessageAsync(content, uriScheme, DefaultBufferSize);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, string uriScheme, CancellationToken cancellationToken)
		        {
		            return ReadAsHttpRequestMessageAsync(content, uriScheme, DefaultBufferSize, cancellationToken);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, string uriScheme, int bufferSize, CancellationToken cancellationToken)
		        {
		            return ReadAsHttpRequestMessageAsync(content, uriScheme, bufferSize, HttpRequestHeaderParser.DefaultMaxHeaderSize, cancellationToken);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, string uriScheme, int bufferSize, int maxHeaderSize = HttpRequestHeaderParser.DefaultMaxHeaderSize)
		        {
		            return ReadAsHttpRequestMessageAsync(content, uriScheme, bufferSize, maxHeaderSize, CancellationToken.None);
		        }
		
		        public static Task<HttpRequestMessage> ReadAsHttpRequestMessageAsync(this HttpContent content, string uriScheme, int bufferSize, int maxHeaderSize, CancellationToken cancellationToken)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            if (uriScheme == null) throw new ArgumentNullException("uriScheme");
		            if (!Uri.CheckSchemeName(uriScheme)) throw new ArgumentException("HttpMessageParserInvalidUriScheme", "uriScheme");
		            if (bufferSize < MinBufferSize) throw new ArgumentOutOfRangeException("bufferSize");
		            if (maxHeaderSize < InternetMessageFormatHeaderParser.MinHeaderSize) throw new ArgumentOutOfRangeException("maxHeaderSize");
		
		            HttpMessageContent.ValidateHttpMessageContent(content, true, true);
		
		            return content.ReadAsHttpRequestMessageAsyncCore(uriScheme, bufferSize, maxHeaderSize, cancellationToken);
		        }
		
		        private static async Task<HttpRequestMessage> ReadAsHttpRequestMessageAsyncCore(this HttpContent content, string uriScheme, int bufferSize, int maxHeaderSize, CancellationToken cancellationToken)
		        {
		            cancellationToken.ThrowIfCancellationRequested();
		            Stream stream = await content.ReadAsStreamAsync();
		
		            var httpRequest = new HttpUnsortedRequest();
		            var parser = new HttpRequestHeaderParser(httpRequest, HttpRequestHeaderParser.DefaultMaxRequestLineSize, maxHeaderSize);
		
		            var buffer = new byte[bufferSize];
		            var headerConsumed = 0;
		
		            while (true)
		            {
		                int bytesRead;
		                try
		                {
		                    bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length, cancellationToken);
		                }
		                catch (Exception e)
		                {
		                    throw new IOException("HttpMessageErrorReading", e);
		                }
		
		                ParserState parseStatus;
		                try
		                {
		                    parseStatus = parser.ParseBuffer(buffer, bytesRead, ref headerConsumed);
		                }
		                catch (Exception)
		                {
		                    parseStatus = ParserState.Invalid;
		                }
		
		                if (parseStatus == ParserState.Done) return CreateHttpRequestMessage(uriScheme, httpRequest, stream, bytesRead - headerConsumed);
		                if (parseStatus != ParserState.NeedMoreData) throw new InvalidOperationException("HttpMessageParserError");
		                if (bytesRead == 0) throw new IOException("ReadAsHttpMessageUnexpectedTermination");
		            }
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content)
		        {
		            return ReadAsHttpResponseMessageAsync(content, DefaultBufferSize);
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content, CancellationToken cancellationToken)
		        {
		            return ReadAsHttpResponseMessageAsync(content, DefaultBufferSize, cancellationToken);
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content, int bufferSize)
		        {
		            return ReadAsHttpResponseMessageAsync(content, bufferSize, HttpResponseHeaderParser.DefaultMaxHeaderSize);
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content, int bufferSize,
		            CancellationToken cancellationToken)
		        {
		            return ReadAsHttpResponseMessageAsync(content, bufferSize, HttpResponseHeaderParser.DefaultMaxHeaderSize, cancellationToken);
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content, int bufferSize, int maxHeaderSize)
		        {
		            return ReadAsHttpResponseMessageAsync(content, bufferSize, maxHeaderSize, CancellationToken.None);
		        }
		
		        public static Task<HttpResponseMessage> ReadAsHttpResponseMessageAsync(this HttpContent content, int bufferSize, int maxHeaderSize, CancellationToken cancellationToken)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            if (bufferSize < MinBufferSize) throw new ArgumentOutOfRangeException("bufferSize");
		            if (maxHeaderSize < InternetMessageFormatHeaderParser.MinHeaderSize) throw new ArgumentOutOfRangeException("maxHeaderSize");
		            HttpMessageContent.ValidateHttpMessageContent(content, false, true);
		
		            return content.ReadAsHttpResponseMessageAsyncCore(bufferSize, maxHeaderSize, cancellationToken);
		        }
		
		        private static async Task<HttpResponseMessage> ReadAsHttpResponseMessageAsyncCore(this HttpContent content, int bufferSize, int maxHeaderSize, CancellationToken cancellationToken)
		        {
		            cancellationToken.ThrowIfCancellationRequested();
		            Stream stream = await content.ReadAsStreamAsync();
		
		            var httpResponse = new HttpUnsortedResponse();
		            var parser = new HttpResponseHeaderParser(httpResponse, HttpResponseHeaderParser.DefaultMaxStatusLineSize, maxHeaderSize);
		            ParserState parseStatus;
		
		            var buffer = new byte[bufferSize];
		            int bytesRead;
		            var headerConsumed = 0;
		
		            while (true)
		            {
		                try
		                {
		                    bytesRead = await stream.ReadAsync(buffer, 0, buffer.Length, cancellationToken);
		                }
		                catch (Exception e)
		                {
		                    throw new IOException("HttpMessageErrorReading", e);
		                }
		
		                try
		                {
		                    parseStatus = parser.ParseBuffer(buffer, bytesRead, ref headerConsumed);
		                }
		                catch (Exception)
		                {
		                    parseStatus = ParserState.Invalid;
		                }
		
		                if (parseStatus == ParserState.Done) return CreateHttpResponseMessage(httpResponse, stream, bytesRead - headerConsumed); // Create and return parsed HttpResponseMessage
		                else if (parseStatus != ParserState.NeedMoreData) throw new InvalidOperationException("HttpMessageParserError");
		                else if (bytesRead == 0) throw new IOException("ReadAsHttpMessageUnexpectedTermination");
		            }
		        }
		
		        private static Uri CreateRequestUri(string uriScheme, HttpUnsortedRequest httpRequest)
		        {
		            Contract.Assert(httpRequest != null, "httpRequest cannot be null.");
		            Contract.Assert(uriScheme != null, "uriScheme cannot be null");
		
		            IEnumerable<string> hostValues;
		            if (httpRequest.HttpHeaders.TryGetValues(FormattingUtilities.HttpHostHeader, out hostValues))
		            {
		                // ReSharper disable once PossibleMultipleEnumeration
		                var hostCount = hostValues.Count();
		                if (hostCount != 1) throw new InvalidOperationException("HttpMessageParserInvalidHostCount");
		            }
		            else
		            {
		                throw new InvalidOperationException("HttpMessageParserInvalidHostCount");
		            }
		
		            // We don't use UriBuilder as hostValues.ElementAt(0) contains 'host:port' and UriBuilder needs these split out into separate host and port.
		            string requestUri = String.Format(CultureInfo.InvariantCulture, "{0}://{1}{2}", uriScheme, hostValues.ElementAt(0), httpRequest.RequestUri);
		            return new Uri(requestUri);
		        }
		
		        // ReSharper disable once ParameterTypeCanBeEnumerable.Local
		        private static HttpContent CreateHeaderFields(HttpHeaders source, HttpHeaders destination, Stream contentStream, int rewind)
		        {
		            Contract.Assert(source != null, "source headers cannot be null");
		            Contract.Assert(destination != null, "destination headers cannot be null");
		            Contract.Assert(contentStream != null, "contentStream must be non null");
		            HttpContentHeaders contentHeaders = null;
		            HttpContent content = null;
		
		            // Set the header fields
		            foreach (KeyValuePair<string, IEnumerable<string>> header in source)
		            {
		                if (!destination.TryAddWithoutValidation(header.Key, header.Value))
		                {
		                    if (contentHeaders == null) contentHeaders = FormattingUtilities.CreateEmptyContentHeaders();
		                    contentHeaders.TryAddWithoutValidation(header.Key, header.Value);
		                }
		            }
		
		            // If we have content headers then create an HttpContent for this Response
		            if (contentHeaders != null)
		            {
		                // Need to rewind the input stream to be at the position right after the HTTP header
		                // which we may already have parsed as we read the content stream.
		                if (!contentStream.CanSeek) throw new InvalidOperationException("HttpMessageContentStreamMustBeSeekable");
		
		                contentStream.Seek(0 - rewind, SeekOrigin.Current);
		                content = new StreamContent(contentStream);
		                contentHeaders.CopyTo(content.Headers);
		            }
		
		            return content;
		        }
		
		        private static HttpRequestMessage CreateHttpRequestMessage(string uriScheme, HttpUnsortedRequest httpRequest, Stream contentStream, int rewind)
		        {
		            Contract.Assert(uriScheme != null, "URI scheme must be non null");
		            Contract.Assert(httpRequest != null, "httpRequest must be non null");
		            Contract.Assert(contentStream != null, "contentStream must be non null");
		
		            HttpRequestMessage httpRequestMessage = new HttpRequestMessage();
		
		            // Set method, requestURI, and version
		            httpRequestMessage.Method = httpRequest.Method;
		            httpRequestMessage.RequestUri = CreateRequestUri(uriScheme, httpRequest);
		            httpRequestMessage.Version = httpRequest.Version;
		
		            // Set the header fields and content if any
		            httpRequestMessage.Content = CreateHeaderFields(httpRequest.HttpHeaders, httpRequestMessage.Headers, contentStream, rewind);
		
		            return httpRequestMessage;
		        }
		
		        private static HttpResponseMessage CreateHttpResponseMessage(HttpUnsortedResponse httpResponse, Stream contentStream, int rewind)
		        {
		            Contract.Assert(httpResponse != null, "httpResponse must be non null");
		            Contract.Assert(contentStream != null, "contentStream must be non null");
		
		            HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
		
		            // Set version, status code and reason phrase
		            httpResponseMessage.Version = httpResponse.Version;
		            httpResponseMessage.StatusCode = httpResponse.StatusCode;
		            httpResponseMessage.ReasonPhrase = httpResponse.ReasonPhrase;
		
		            // Set the header fields and content if any
		            httpResponseMessage.Content = CreateHeaderFields(httpResponse.HttpHeaders, httpResponseMessage.Headers, contentStream, rewind);
		
		            return httpResponseMessage;
		        }
		    }
		}
		#endregion
		
		#region HttpContentMultipartExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.IO;
		    using System.Net.Http;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public static class HttpContentMultipartExtensions
		    {
		        private const int MinBufferSize = 256;
		        private const int DefaultBufferSize = 32 * 1024;
		
		        public static bool IsMimeMultipartContent(this HttpContent content)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            return MimeMultipartBodyPartParser.IsMimeMultipartContent(content);
		        }
		
		        public static bool IsMimeMultipartContent(this HttpContent content, string subtype)
		        {
		            if (String.IsNullOrWhiteSpace(subtype)) throw new ArgumentNullException("subtype");
		            if (IsMimeMultipartContent(content))
		            {
		                if (content.Headers.ContentType.MediaType.Equals("multipart/" + subtype, StringComparison.OrdinalIgnoreCase)) return true;
		            }
		
		            return false;
		        }
		
		        public static Task<MultipartMemoryStreamProvider> ReadAsMultipartAsync(this HttpContent content)
		        {
		            return ReadAsMultipartAsync(content, new MultipartMemoryStreamProvider(), DefaultBufferSize);
		        }
		
		        public static Task<MultipartMemoryStreamProvider> ReadAsMultipartAsync(this HttpContent content, CancellationToken cancellationToken)
		        {
		            return ReadAsMultipartAsync(content, new MultipartMemoryStreamProvider(), DefaultBufferSize, cancellationToken);
		        }
		
		        public static Task<T> ReadAsMultipartAsync<T>(this HttpContent content, T streamProvider) where T : MultipartStreamProvider
		        {
		            return ReadAsMultipartAsync(content, streamProvider, DefaultBufferSize);
		        }
		
		        public static Task<T> ReadAsMultipartAsync<T>(this HttpContent content, T streamProvider, CancellationToken cancellationToken) where T : MultipartStreamProvider
		        {
		            return ReadAsMultipartAsync(content, streamProvider, DefaultBufferSize, cancellationToken);
		        }
		
		        public static Task<T> ReadAsMultipartAsync<T>(this HttpContent content, T streamProvider, int bufferSize) where T : MultipartStreamProvider
		        {
		            return ReadAsMultipartAsync(content, streamProvider, bufferSize, CancellationToken.None);
		        }
		
		        public static async Task<T> ReadAsMultipartAsync<T>(this HttpContent content, T streamProvider, int bufferSize, CancellationToken cancellationToken) where T : MultipartStreamProvider
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            if (streamProvider == null) throw new ArgumentNullException("streamProvider");
		
		            if (bufferSize < MinBufferSize) throw new ArgumentOutOfRangeException("bufferSize");
		
		            Stream stream;
		            try
		            {
		                stream = await content.ReadAsStreamAsync();
		            }
		            catch (Exception e)
		            {
		                throw new IOException("ReadAsMimeMultipartErrorReading", e);
		            }
		
		            using (var parser = new MimeMultipartBodyPartParser(content, streamProvider))
		            {
		                byte[] data = new byte[bufferSize];
		                MultipartAsyncContext context = new MultipartAsyncContext(stream, parser, data, streamProvider.Contents);
		
		                // Start async read/write loop
		                await MultipartReadAsync(context, cancellationToken);
		
		                // Let the stream provider post-process when everything is complete
		                await streamProvider.ExecutePostProcessingAsync(cancellationToken);
		                return streamProvider;
		            }
		        }
		
		        private static async Task MultipartReadAsync(MultipartAsyncContext context, CancellationToken cancellationToken)
		        {
		            Contract.Assert(context != null, "context cannot be null");
		            while (true)
		            {
		                int bytesRead;
		                try
		                {
		                    bytesRead = await context.ContentStream.ReadAsync(context.Data, 0, context.Data.Length, cancellationToken);
		                }
		                catch (Exception e)
		                {
		                    throw new IOException("ReadAsMimeMultipartErrorReading", e);
		                }
		
		                IEnumerable<MimeBodyPart> parts = context.MimeParser.ParseBuffer(context.Data, bytesRead);
		
		                foreach (MimeBodyPart part in parts)
		                {
		                    foreach (ArraySegment<byte> segment in part.Segments)
		                    {
		                        try
		                        {
		                            await part.WriteSegment(segment, cancellationToken);
		                        }
		                        catch (Exception e)
		                        {
		                            part.Dispose();
		                            throw new IOException("ReadAsMimeMultipartErrorWriting", e);
		                        }
		                    }
		
		                    if (CheckIsFinalPart(part, context.Result)) return;
		                }
		            }
		        }
		
		        private static bool CheckIsFinalPart(MimeBodyPart part, ICollection<HttpContent> result)
		        {
		            Contract.Assert(part != null, "part cannot be null.");
		            Contract.Assert(result != null, "result cannot be null.");
		            if (part.IsComplete)
		            {
		                var partContent = part.GetCompletedHttpContent();
		                if (partContent != null)
		                {
		                    result.Add(partContent);
		                }
		
		                bool isFinal = part.IsFinal;
		                part.Dispose();
		                return isFinal;
		            }
		
		            return false;
		        }
		
		        private class MultipartAsyncContext
		        {
		            public MultipartAsyncContext(Stream contentStream, MimeMultipartBodyPartParser mimeParser, byte[] data, ICollection<HttpContent> result)
		            {
		                Contract.Assert(contentStream != null);
		                Contract.Assert(mimeParser != null);
		                Contract.Assert(data != null);
		
		                ContentStream = contentStream;
		                Result = result;
		                MimeParser = mimeParser;
		                Data = data;
		            }
		
		            public Stream ContentStream { get; private set; }
		
		            public ICollection<HttpContent> Result { get; private set; }
		
		            public byte[] Data { get; private set; }
		
		            public MimeMultipartBodyPartParser MimeParser { get; private set; }
		        }
		    }
		}
		#endregion
		
		#region HttpHeaderExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.Net.Http.Headers;
		    
		    public static class HttpHeaderExtensions
		    {
		        public static void CopyTo(this HttpContentHeaders fromHeaders, HttpContentHeaders toHeaders)
		        {
		            Contract.Assert(fromHeaders != null, "fromHeaders cannot be null.");
		            Contract.Assert(toHeaders != null, "toHeaders cannot be null.");
		
		            foreach (KeyValuePair<string, IEnumerable<string>> header in fromHeaders)
		            {
		                toHeaders.TryAddWithoutValidation(header.Key, header.Value);
		            }
		        }
		    }
		}
		#endregion
		
		#region HttpMessageContent
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.IO;
		    using System.Net;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using System.Text;
		    using System.Threading.Tasks;
		    
		    public class HttpMessageContent : HttpContent
		    {
		        // ReSharper disable InconsistentNaming
		        private const string SP = " ";
		        private const string ColonSP = ": ";
		        private const string CRLF = "\r\n";
		        // ReSharper restore InconsistentNaming
		        private const string CommaSeparator = ", ";
		
		        private const int DefaultHeaderAllocation = 2 * 1024;
		
		        private const string DefaultMediaType = "application/http";
		
		        private const string MsgTypeParameter = "msgtype";
		        private const string DefaultRequestMsgType = "request";
		        private const string DefaultResponseMsgType = "response";
		
		        //private const string DefaultRequestMediaType = DefaultMediaType + "; " + MsgTypeParameter + "=" + DefaultRequestMsgType;
		        //private const string DefaultResponseMediaType = DefaultMediaType + "; " + MsgTypeParameter + "=" + DefaultResponseMsgType;
		
		        // Set of header fields that only support single values such as Set-Cookie.
		        private static readonly HashSet<string> _singleValueHeaderFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
		        {
		            "Cookie",
		            "Set-Cookie",
		            "X-Powered-By",
		        };
		
		        // Set of header fields that should get serialized as space-separated values such as User-Agent.
		        private static readonly HashSet<string> _spaceSeparatedValueHeaderFields = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
		        {
		            "User-Agent",
		        };
		
		        private bool _contentConsumed;
		        private Lazy<Task<Stream>> _streamTask;
		
		        public HttpMessageContent(HttpRequestMessage httpRequest)
		        {
		            if (httpRequest == null) throw new ArgumentNullException("httpRequest");
		
		            HttpRequestMessage = httpRequest;
		            Headers.ContentType = new MediaTypeHeaderValue(DefaultMediaType);
		            Headers.ContentType.Parameters.Add(new NameValueHeaderValue(MsgTypeParameter, DefaultRequestMsgType));
		
		            InitializeStreamTask();
		        }
		
		        public HttpMessageContent(HttpResponseMessage httpResponse)
		        {
		            if (httpResponse == null) throw new ArgumentNullException("httpResponse");
		
		            HttpResponseMessage = httpResponse;
		            Headers.ContentType = new MediaTypeHeaderValue(DefaultMediaType);
		            Headers.ContentType.Parameters.Add(new NameValueHeaderValue(MsgTypeParameter, DefaultResponseMsgType));
		
		            InitializeStreamTask();
		        }
		
		        private HttpContent Content
		        {
		            get { return HttpRequestMessage != null ? HttpRequestMessage.Content : HttpResponseMessage.Content; }
		        }
		
		        public HttpRequestMessage HttpRequestMessage { get; private set; }
		
		        public HttpResponseMessage HttpResponseMessage { get; private set; }
		
		        private void InitializeStreamTask()
		        {
		            _streamTask = new Lazy<Task<Stream>>(() => Content == null ? null : Content.ReadAsStreamAsync());
		        }
		
		        public static bool ValidateHttpMessageContent(HttpContent content, bool isRequest, bool throwOnError)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		
		            var contentType = content.Headers.ContentType;
		            if (contentType != null)
		            {
		                if (!contentType.MediaType.Equals(DefaultMediaType, StringComparison.OrdinalIgnoreCase))
		                {
		                    if (throwOnError) throw new ArgumentException("content");
		                    return false;
		                }
		
		                foreach (NameValueHeaderValue parameter in contentType.Parameters)
		                {
		                    if (parameter.Name.Equals(MsgTypeParameter, StringComparison.OrdinalIgnoreCase))
		                    {
		                        var msgType = FormattingUtilities.UnquoteToken(parameter.Value);
		                        if (!msgType.Equals(isRequest ? DefaultRequestMsgType : DefaultResponseMsgType, StringComparison.OrdinalIgnoreCase))
		                        {
		                            if (throwOnError) throw new ArgumentException("content");
		                            return false;
		                        }
		
		                        return true;
		                    }
		                }
		            }
		
		            if (throwOnError) throw new ArgumentException("content");
		            return false;
		        }
		
		        protected override async Task SerializeToStreamAsync(Stream stream, TransportContext context)
		        {
		            if (stream == null) throw new ArgumentNullException("stream");
		
		            var header = SerializeHeader();
		            await stream.WriteAsync(header, 0, header.Length);
		
		            if (Content != null)
		            {
		                var readStream = await _streamTask.Value;
		                ValidateStreamForReading(readStream);
		                await Content.CopyToAsync(stream);
		            }
		        }
		
		        protected override bool TryComputeLength(out long length)
		        {
		            // We have four states we could be in:
		            //   1. We have content, but the task is still running or finished without success
		            //   2. We have content, the task has finished successfully, and the stream came back as a null or non-seekable
		            //   3. We have content, the task has finished successfully, and the stream is seekable, so we know its length
		            //   4. We don't have content (streamTask.Value == null)
		            //
		            // For #1 and #2, we return false.
		            // For #3, we return true & the size of our headers + the content length
		            // For #4, we return true & the size of our headers
		
		            var hasContent = _streamTask.Value != null;
		            length = 0;
		
		            // Cases #1, #2, #3
		            if (hasContent)
		            {
		                Stream readStream;
		                if (!_streamTask.Value.TryGetResult(out readStream) /* Case #1 */ || readStream == null || !readStream.CanSeek /* Case #2 */) 
		                {
		                    length = -1;
		                    return false;
		                }
		                length = readStream.Length; // Case #3
		            }
		
		            // We serialize header to a StringBuilder so that we can determine the length
		            // following the pattern for HttpContent to try and determine the message length.
		            // The perf overhead is no larger than for the other HttpContent implementations.
		            var header = SerializeHeader();
		            length += header.Length;
		            return true;
		        }
		
		        protected override void Dispose(bool disposing)
		        {
		            if (disposing)
		            {
		                if (HttpRequestMessage != null)
		                {
		                    HttpRequestMessage.Dispose();
		                    HttpRequestMessage = null;
		                }
		
		                if (HttpResponseMessage != null)
		                {
		                    HttpResponseMessage.Dispose();
		                    HttpResponseMessage = null;
		                }
		            }
		
		            base.Dispose(disposing);
		        }
		
		        private static void SerializeRequestLine(StringBuilder message, HttpRequestMessage httpRequest)
		        {
		            Contract.Assert(message != null, "message cannot be null");
		            message.Append(httpRequest.Method + SP);
		            message.Append(httpRequest.RequestUri.PathAndQuery + SP);
		            message.Append(FormattingUtilities.HttpVersionToken + "/" + (httpRequest.Version != null ? httpRequest.Version.ToString(2) : "1.1") + CRLF);
		
		            // Only insert host header if not already present.
		            if (httpRequest.Headers.Host == null)
		            {
		                message.Append(FormattingUtilities.HttpHostHeader + ColonSP + httpRequest.RequestUri.Authority + CRLF);
		            }
		        }
		
		        private static void SerializeStatusLine(StringBuilder message, HttpResponseMessage httpResponse)
		        {
		            Contract.Assert(message != null, "message cannot be null");
		            message.Append(FormattingUtilities.HttpVersionToken + "/" + (httpResponse.Version != null ? httpResponse.Version.ToString(2) : "1.1") + SP);
		            message.Append((int)httpResponse.StatusCode + SP);
		            message.Append(httpResponse.ReasonPhrase + CRLF);
		        }
		
		        private static void SerializeHeaderFields(StringBuilder message, HttpHeaders headers)
		        {
		            Contract.Assert(message != null, "message cannot be null");
		            if (headers != null)
		            {
		                foreach (KeyValuePair<string, IEnumerable<string>> header in headers)
		                {
		                    if (_singleValueHeaderFields.Contains(header.Key))
		                    {
		                        foreach (string value in header.Value) message.Append(header.Key + ColonSP + value + CRLF);
		                    }
		                    else if (_spaceSeparatedValueHeaderFields.Contains(header.Key))
		                    {
		                        message.Append(header.Key + ColonSP + String.Join(SP, header.Value) + CRLF);
		                    }
		                    else
		                    {
		                        message.Append(header.Key + ColonSP + String.Join(CommaSeparator, header.Value) + CRLF);
		                    }
		                }
		            }
		        }
		
		        private byte[] SerializeHeader()
		        {
		            var message = new StringBuilder(DefaultHeaderAllocation);
		            HttpHeaders headers;
		            HttpContent content;
		            if (HttpRequestMessage != null)
		            {
		                SerializeRequestLine(message, HttpRequestMessage);
		                headers = HttpRequestMessage.Headers;
		                content = HttpRequestMessage.Content;
		            }
		            else
		            {
		                SerializeStatusLine(message, HttpResponseMessage);
		                headers = HttpResponseMessage.Headers;
		                content = HttpResponseMessage.Content;
		            }
		
		            SerializeHeaderFields(message, headers);
		            if (content != null)
		            {
		                SerializeHeaderFields(message, content.Headers);
		            }
		
		            message.Append(CRLF);
		            return Encoding.UTF8.GetBytes(message.ToString());
		        }
		
		        private void ValidateStreamForReading(Stream stream)
		        {
		            // If the content needs to be written to a target stream a 2nd time, then the stream must support
		            // seeking (e.g. a FileStream), otherwise the stream can't be copied a second time to a target 
		            // stream (e.g. a NetworkStream).
		            if (_contentConsumed)
		            {
		                if (stream != null && stream.CanRead) stream.Position = 0;
		                else throw new InvalidOperationException();
		            }
		
		            _contentConsumed = true;
		        }
		    }
		}
		#endregion
		
		#region HttpRequestHeaderParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		
		    public class HttpRequestHeaderParser
		    {
		        internal const int DefaultMaxRequestLineSize = 2 * 1024;
		        internal const int DefaultMaxHeaderSize = 16 * 1024; // Same default size as IIS has for regular requests
		
		        private HttpRequestState _requestStatus = HttpRequestState.RequestLine;
		
		        private readonly HttpRequestLineParser _requestLineParser;
		        private readonly InternetMessageFormatHeaderParser _headerParser;
		
		        public HttpRequestHeaderParser(HttpUnsortedRequest httpRequest, int maxRequestLineSize = DefaultMaxRequestLineSize, int maxHeaderSize = DefaultMaxHeaderSize)
		        {
		            if (httpRequest == null) throw new ArgumentNullException("httpRequest");
		
		            HttpUnsortedRequest httpRequest1 = httpRequest;
		
		            // Create request line parser
		            _requestLineParser = new HttpRequestLineParser(httpRequest1, maxRequestLineSize);
		
		            // Create header parser
		            _headerParser = new InternetMessageFormatHeaderParser(httpRequest1.HttpHeaders, maxHeaderSize);
		        }
		
		        private enum HttpRequestState
		        {
		            RequestLine = 0, // parsing request line
		            RequestHeaders // reading headers
		        }
		
		        public ParserState ParseBuffer(byte[] buffer, int bytesReady, ref int bytesConsumed)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		
		            var parseStatus = ParserState.NeedMoreData;
		            ParserState subParseStatus;
		
		            switch (_requestStatus)
		            {
		                case HttpRequestState.RequestLine:
		                    try
		                    {
		                        subParseStatus = _requestLineParser.ParseBuffer(buffer, bytesReady, ref bytesConsumed);
		                    }
		                    catch (Exception)
		                    {
		                        subParseStatus = ParserState.Invalid;
		                    }
		
		                    if (subParseStatus == ParserState.Done)
		                    {
		                        _requestStatus = HttpRequestState.RequestHeaders;
		                        goto case HttpRequestState.RequestHeaders;
		                    }
		                    else if (subParseStatus != ParserState.NeedMoreData)
		                    {
		                        // Report error - either Invalid or DataTooBig
		                        parseStatus = subParseStatus;
		                        // ReSharper disable once RedundantJumpStatement
		                        break;
		                    }
		
		                    break; // read more data
		
		                case HttpRequestState.RequestHeaders:
		                    if (bytesConsumed >= bytesReady) break; // we already can tell we need more data
		                    try
		                    {
		                        subParseStatus = _headerParser.ParseBuffer(buffer, bytesReady, ref bytesConsumed);
		                    }
		                    catch (Exception)
		                    {
		                        subParseStatus = ParserState.Invalid;
		                    }
		
		                    if (subParseStatus == ParserState.Done) parseStatus = subParseStatus;
		                    else if (subParseStatus != ParserState.NeedMoreData) parseStatus = subParseStatus;
		
		                    break; // need more data
		            }
		
		            return parseStatus;
		        }
		    }
		}
		#endregion
		
		#region HttpRequestLineParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Diagnostics.Contracts;
		    using System.Net.Http;
		    using System.Text;
		    
		    public class HttpRequestLineParser
		    {
		        public const int MinRequestLineSize = 14;
		        private const int DefaultTokenAllocation = 2 * 1024;
		
		        private int _totalBytesConsumed;
		        private readonly int _maximumHeaderLength;
		
		        private HttpRequestLineState _requestLineState;
		        private HttpUnsortedRequest _httpRequest;
		        private readonly StringBuilder _currentToken = new StringBuilder(DefaultTokenAllocation);
		
		        public HttpRequestLineParser(HttpUnsortedRequest httpRequest, int maxRequestLineSize)
		        {
		            // The minimum length which would be an empty header terminated by CRLF
		            if (maxRequestLineSize < MinRequestLineSize) throw new ArgumentOutOfRangeException("maxRequestLineSize");
		            if (httpRequest == null) throw new ArgumentNullException("httpRequest");
		
		            _httpRequest = httpRequest;
		            _maximumHeaderLength = maxRequestLineSize;
		        }
		
		        private enum HttpRequestLineState
		        {
		            RequestMethod = 0,
		            RequestUri,
		            BeforeVersionNumbers,
		            MajorVersionNumber,
		            MinorVersionNumber,
		            AfterCarriageReturn
		        }
		
		        public ParserState ParseBuffer(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		
		            ParserState parseStatus = ParserState.NeedMoreData;
		
		            if (bytesConsumed >= bytesReady) return parseStatus;  // We already can tell we need more data
		
		            try
		            {
		                parseStatus = ParseRequestLine(buffer, bytesReady, ref bytesConsumed, ref _requestLineState, _maximumHeaderLength, ref _totalBytesConsumed, _currentToken, _httpRequest);
		            }
		            catch (Exception)
		            {
		                parseStatus = ParserState.Invalid;
		            }
		
		            return parseStatus;
		        }
		
		        private static ParserState ParseRequestLine(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed,
		            ref HttpRequestLineState requestLineState,
		            int maximumHeaderLength,
		            ref int totalBytesConsumed,
		            StringBuilder currentToken,
		            HttpUnsortedRequest httpRequest)
		        {
		            Contract.Assert((bytesReady - bytesConsumed) >= 0, "ParseRequestLine()|(bytesReady - bytesConsumed) < 0");
		            Contract.Assert(maximumHeaderLength <= 0 || totalBytesConsumed <= maximumHeaderLength, "ParseRequestLine()|Headers already read exceeds limit.");
		
		            // Remember where we started.
		            var initialBytesParsed = bytesConsumed;
		            int segmentStart;
		
		            // Set up parsing status with what will happen if we exceed the buffer.
		            var parseStatus = ParserState.DataTooBig;
		            var effectiveMax = maximumHeaderLength <= 0 ? Int32.MaxValue : (maximumHeaderLength - totalBytesConsumed + bytesConsumed);
		            if (bytesReady < effectiveMax)
		            {
		                parseStatus = ParserState.NeedMoreData;
		                effectiveMax = bytesReady;
		            }
		
		            Contract.Assert(bytesConsumed < effectiveMax, "We have already consumed more than the max header length.");
		
		            switch (requestLineState)
		            {
		                case HttpRequestLineState.RequestMethod:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != ' ')
		                    {
		                        if (buffer[bytesConsumed] < 0x21 || buffer[bytesConsumed] > 0x7a)
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            var method = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(method);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        var method = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(method);
		                    }
		
		                    // Copy value out
		                    httpRequest.Method = new HttpMethod(currentToken.ToString());
		                    currentToken.Clear();
		
		                    // Move past the SP
		                    requestLineState = HttpRequestLineState.RequestUri;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpRequestLineState.RequestUri;
		
		                case HttpRequestLineState.RequestUri:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != ' ')
		                    {
		                        if (buffer[bytesConsumed] == '\r')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string addr = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(addr);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string addr = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(addr);
		                    }
		
		                    // URI validation happens when we create the URI later.
		                    if (currentToken.Length == 0) throw new FormatException("HttpMessageParserEmptyUri");
		
		                    // Copy value out
		                    httpRequest.RequestUri = currentToken.ToString();
		                    currentToken.Clear();
		
		                    // Move past the SP
		                    requestLineState = HttpRequestLineState.BeforeVersionNumbers;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HttpRequestLineState.BeforeVersionNumbers;
		
		                case HttpRequestLineState.BeforeVersionNumbers:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '/')
		                    {
		                        if (buffer[bytesConsumed] < 0x21 || buffer[bytesConsumed] > 0x7a)
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string token = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(token);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string token = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(token);
		                    }
		
		                    // Validate value
		                    var version = currentToken.ToString();
		                    if (String.CompareOrdinal(FormattingUtilities.HttpVersionToken, version) != 0) throw new FormatException("HttpInvalidVersion");
		                    currentToken.Clear();
		
		                    // Move past the '/'
		                    requestLineState = HttpRequestLineState.MajorVersionNumber;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpRequestLineState.MajorVersionNumber;
		
		                case HttpRequestLineState.MajorVersionNumber:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '.')
		                    {
		                        if (buffer[bytesConsumed] < '0' || buffer[bytesConsumed] > '9')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string major = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(major);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        var major = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(major);
		                    }
		
		                    // Move past the "."
		                    currentToken.Append('.');
		                    requestLineState = HttpRequestLineState.MinorVersionNumber;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HttpRequestLineState.MinorVersionNumber;
		
		                case HttpRequestLineState.MinorVersionNumber:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '\r')
		                    {
		                        if (buffer[bytesConsumed] < '0' || buffer[bytesConsumed] > '9')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string minor = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(minor);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string minor = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(minor);
		                    }
		
		                    // Copy out value
		                    httpRequest.Version = Version.Parse(currentToken.ToString());
		                    currentToken.Clear();
		
		                    // Move past the CR
		                    requestLineState = HttpRequestLineState.AfterCarriageReturn;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		                    goto case HttpRequestLineState.AfterCarriageReturn;
		
		                case HttpRequestLineState.AfterCarriageReturn:
		                    if (buffer[bytesConsumed] != '\n')
		                    {
		                        parseStatus = ParserState.Invalid;
		                        goto quit;
		                    }
		
		                    parseStatus = ParserState.Done;
		                    bytesConsumed++;
		                    break;
		            }
		
		        quit:
		            totalBytesConsumed += bytesConsumed - initialBytesParsed;
		            return parseStatus;
		        }
		    }
		}
		#endregion
		
		#region HttpResponseHeaderParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		
		    public class HttpResponseHeaderParser
		    {
		        public const int DefaultMaxStatusLineSize = 2 * 1024;
		        public const int DefaultMaxHeaderSize = 16 * 1024; // Same default size as IIS has for HTTP requests
		
		        private HttpResponseState _responseStatus = HttpResponseState.StatusLine;
		
		        private readonly HttpStatusLineParser _statusLineParser;
		        private readonly InternetMessageFormatHeaderParser _headerParser;
		
		        public HttpResponseHeaderParser(HttpUnsortedResponse httpResponse, int maxResponseLineSize = DefaultMaxStatusLineSize, int maxHeaderSize = DefaultMaxHeaderSize)
		        {
		            if (httpResponse == null) throw new ArgumentNullException("httpResponse");
		
		            HttpUnsortedResponse httpResponse1 = httpResponse;
		
		            // Create status line parser
		            _statusLineParser = new HttpStatusLineParser(httpResponse1, maxResponseLineSize);
		
		            // Create header parser
		            _headerParser = new InternetMessageFormatHeaderParser(httpResponse1.HttpHeaders, maxHeaderSize);
		        }
		
		        private enum HttpResponseState
		        {
		            StatusLine = 0, // parsing status line
		            ResponseHeaders // reading headers
		        }
		
		        public ParserState ParseBuffer(byte[] buffer, int bytesReady, ref int bytesConsumed)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		
		            var parseStatus = ParserState.NeedMoreData;
		            ParserState subParseStatus;
		
		            switch (_responseStatus)
		            {
		                case HttpResponseState.StatusLine:
		                    try
		                    {
		                        subParseStatus = _statusLineParser.ParseBuffer(buffer, bytesReady, ref bytesConsumed);
		                    }
		                    catch (Exception)
		                    {
		                        subParseStatus = ParserState.Invalid;
		                    }
		
		                    if (subParseStatus == ParserState.Done)
		                    {
		                        _responseStatus = HttpResponseState.ResponseHeaders;
		                        goto case HttpResponseState.ResponseHeaders;
		                    }
		                    else if (subParseStatus != ParserState.NeedMoreData)
		                    {
		                        // Report error - either Invalid or DataTooBig
		                        parseStatus = subParseStatus;
		                    }
		
		                    break; // read more data
		
		                case HttpResponseState.ResponseHeaders:
		                    if (bytesConsumed >= bytesReady) break;  // we already can tell we need more data
		
		                    try
		                    {
		                        subParseStatus = _headerParser.ParseBuffer(buffer, bytesReady, ref bytesConsumed);
		                    }
		                    catch (Exception)
		                    {
		                        subParseStatus = ParserState.Invalid;
		                    }
		
		                    if (subParseStatus == ParserState.Done)
		                    {
		                        parseStatus = subParseStatus;
		                    }
		                    else if (subParseStatus != ParserState.NeedMoreData)
		                    {
		                        parseStatus = subParseStatus;
		                        // ReSharper disable once RedundantJumpStatement
		                        break;
		                    }
		
		                    break; // need more data
		            }
		
		            return parseStatus;
		        }
		    }
		}
		#endregion
		
		#region HttpStatusLineParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Diagnostics.Contracts;
		    using System.Globalization;
		    using System.Net;
		    using System.Text;
		    
		    public class HttpStatusLineParser
		    {
		        public const int MinStatusLineSize = 15;
		        private const int DefaultTokenAllocation = 2 * 1024;
		        //private const int MaxStatusCode = 1000;
		
		        private int _totalBytesConsumed;
		        private readonly int _maximumHeaderLength;
		
		        private HttpStatusLineState _statusLineState;
		        private HttpUnsortedResponse _httpResponse;
		        private readonly StringBuilder _currentToken = new StringBuilder(DefaultTokenAllocation);
		
		        public HttpStatusLineParser(HttpUnsortedResponse httpResponse, int maxStatusLineSize)
		        {
		            // The minimum length which would be an empty header terminated by CRLF
		            if (maxStatusLineSize < MinStatusLineSize) throw new ArgumentOutOfRangeException("maxStatusLineSize");
		            if (httpResponse == null) throw new ArgumentNullException("httpResponse");
		
		            _httpResponse = httpResponse;
		            _maximumHeaderLength = maxStatusLineSize;
		        }
		
		        private enum HttpStatusLineState
		        {
		            BeforeVersionNumbers = 0,
		            MajorVersionNumber,
		            MinorVersionNumber,
		            StatusCode,
		            ReasonPhrase,
		            AfterCarriageReturn
		        }
		
		        public ParserState ParseBuffer(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		
		            var parseStatus = ParserState.NeedMoreData;
		
		            if (bytesConsumed >= bytesReady) return parseStatus; // We already can tell we need more data
		
		            try
		            {
		                parseStatus = ParseStatusLine(buffer, bytesReady, ref bytesConsumed, ref _statusLineState, _maximumHeaderLength, ref _totalBytesConsumed, _currentToken, _httpResponse);
		            }
		            catch (Exception)
		            {
		                parseStatus = ParserState.Invalid;
		            }
		
		            return parseStatus;
		        }
		
		        private static ParserState ParseStatusLine(byte[] buffer, int bytesReady, ref int bytesConsumed, ref HttpStatusLineState statusLineState, int maximumHeaderLength, ref int totalBytesConsumed, StringBuilder currentToken, HttpUnsortedResponse httpResponse)
		        {
		            Contract.Assert((bytesReady - bytesConsumed) >= 0, "ParseRequestLine()|(bytesReady - bytesConsumed) < 0");
		            Contract.Assert(maximumHeaderLength <= 0 || totalBytesConsumed <= maximumHeaderLength, "ParseRequestLine()|Headers already read exceeds limit.");
		
		            // Remember where we started.
		            var initialBytesParsed = bytesConsumed;
		            int segmentStart;
		
		            // Set up parsing status with what will happen if we exceed the buffer.
		            var parseStatus = ParserState.DataTooBig;
		            int effectiveMax = maximumHeaderLength <= 0 ? Int32.MaxValue : (maximumHeaderLength - totalBytesConsumed + bytesConsumed);
		            if (bytesReady < effectiveMax)
		            {
		                parseStatus = ParserState.NeedMoreData;
		                effectiveMax = bytesReady;
		            }
		
		            Contract.Assert(bytesConsumed < effectiveMax, "We have already consumed more than the max header length.");
		
		            switch (statusLineState)
		            {
		                case HttpStatusLineState.BeforeVersionNumbers:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '/')
		                    {
		                        if (buffer[bytesConsumed] < 0x21 || buffer[bytesConsumed] > 0x7a)
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            var token = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(token);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        var token = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(token);
		                    }
		
		                    // Validate value
		                    var version = currentToken.ToString();
		                    if (String.CompareOrdinal(FormattingUtilities.HttpVersionToken, version) != 0) throw new FormatException("HttpInvalidVersion");
		
		                    currentToken.Clear();
		
		                    // Move past the '/'
		                    statusLineState = HttpStatusLineState.MajorVersionNumber;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HttpStatusLineState.MajorVersionNumber;
		
		                case HttpStatusLineState.MajorVersionNumber:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '.')
		                    {
		                        if (buffer[bytesConsumed] < '0' || buffer[bytesConsumed] > '9')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string major = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(major);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string major = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(major);
		                    }
		
		                    // Move past the "."
		                    currentToken.Append('.');
		                    statusLineState = HttpStatusLineState.MinorVersionNumber;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpStatusLineState.MinorVersionNumber;
		
		                case HttpStatusLineState.MinorVersionNumber:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != ' ')
		                    {
		                        if (buffer[bytesConsumed] < '0' || buffer[bytesConsumed] > '9')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string minor = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(minor);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string minor = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(minor);
		                    }
		
		                    // Copy out value
		                    httpResponse.Version = Version.Parse(currentToken.ToString());
		                    currentToken.Clear();
		
		                    // Move past the SP
		                    statusLineState = HttpStatusLineState.StatusCode;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpStatusLineState.StatusCode;
		
		                case HttpStatusLineState.StatusCode:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != ' ')
		                    {
		                        if (buffer[bytesConsumed] < '0' || buffer[bytesConsumed] > '9')
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string method = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(method);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string method = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(method);
		                    }
		
		                    // Copy value out
		                    int statusCode = Int32.Parse(currentToken.ToString(), CultureInfo.InvariantCulture);
		                    if (statusCode < 100 || statusCode > 1000)
		                    {
		                        throw new FormatException("HttpInvalidStatusCode");
		                    }
		
		                    httpResponse.StatusCode = (HttpStatusCode)statusCode;
		                    currentToken.Clear();
		
		                    // Move past the SP
		                    statusLineState = HttpStatusLineState.ReasonPhrase;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpStatusLineState.ReasonPhrase;
		
		                case HttpStatusLineState.ReasonPhrase:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '\r')
		                    {
		                        if (buffer[bytesConsumed] < 0x20 || buffer[bytesConsumed] > 0x7a)
		                        {
		                            parseStatus = ParserState.Invalid;
		                            goto quit;
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string addr = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentToken.Append(addr);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string addr = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentToken.Append(addr);
		                    }
		
		                    // Copy value out
		                    httpResponse.ReasonPhrase = currentToken.ToString();
		                    currentToken.Clear();
		
		                    // Move past the CR
		                    statusLineState = HttpStatusLineState.AfterCarriageReturn;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HttpStatusLineState.AfterCarriageReturn;
		
		                case HttpStatusLineState.AfterCarriageReturn:
		                    if (buffer[bytesConsumed] != '\n')
		                    {
		                        parseStatus = ParserState.Invalid;
		                        goto quit;
		                    }
		
		                    parseStatus = ParserState.Done;
		                    bytesConsumed++;
		                    break;
		            }
		
		        quit:
		            totalBytesConsumed += bytesConsumed - initialBytesParsed;
		            return parseStatus;
		        }
		    }
		
		}
		#endregion
		
		#region HttpUnsortedHeaders
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System.Net.Http.Headers;
		    
		    public class HttpUnsortedHeaders : HttpHeaders {}
		}
		#endregion
		
		#region HttpUnsortedRequest
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    
		    public class HttpUnsortedRequest
		    {
		        public HttpUnsortedRequest()
		        {
		            // Collection of unsorted headers. Later we will sort it into the appropriate
		            // HttpContentHeaders, HttpRequestHeaders, and HttpResponseHeaders.
		            HttpHeaders = new HttpUnsortedHeaders();
		        }
		
		        public HttpMethod Method { get; set; }
		
		        public string RequestUri { get; set; }
		
		        public Version Version { get; set; }
		
		        public HttpHeaders HttpHeaders { get; private set; }
		    }
		}
		#endregion
		
		#region HttpUnsortedResponse
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Net;
		    using System.Net.Http.Headers;
		
		    public class HttpUnsortedResponse
		    {
		        public HttpUnsortedResponse()
		        {
		            // Collection of unsorted headers. Later we will sort it into the appropriate
		            // HttpContentHeaders, HttpRequestHeaders, and HttpResponseHeaders.
		            HttpHeaders = new HttpUnsortedHeaders();
		        }
		
		        public Version Version { get; set; }
		
		        public HttpStatusCode StatusCode { get; set; }
		
		        public string ReasonPhrase { get; set; }
		
		        public HttpHeaders HttpHeaders { get; private set; }
		    }
		}
		#endregion
		
		#region HttpUtilities
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Net;
		    using System.Threading;
		    using System.Threading.Tasks;
		
		    public static class HttpUtilities
		    {
		        //internal static readonly Version DefaultVersion = HttpVersion.Version11;
		        internal static readonly byte[] EmptyByteArray = new byte[0];
		
		        static HttpUtilities() { }
		
		        public static bool IsHttpUri(Uri uri)
		        {
		            var scheme = uri.Scheme;
		            if (string.Compare("http", scheme, StringComparison.OrdinalIgnoreCase) != 0) return string.Compare("https", scheme, StringComparison.OrdinalIgnoreCase) == 0;
		            else return true;
		        }
		
		        public static bool HandleFaultsAndCancelation<T>(Task task, TaskCompletionSource<T> tcs)
		        {
		            if (task.IsFaulted)
		            {
		                // ReSharper disable once PossibleNullReferenceException
		                tcs.TrySetException(task.Exception.GetBaseException());
		                return true;
		            }
		
		            if (!task.IsCanceled) return false;
		            tcs.TrySetCanceled();
		            return true;
		        }
		
		        public static Task ContinueWithStandard(this Task task, Action<Task> continuation)
		        {
		            return task.ContinueWith(continuation, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
		        }
		
		        public static Task ContinueWithStandard<T>(this Task<T> task, Action<Task<T>> continuation)
		        {
		            return task.ContinueWith(continuation, CancellationToken.None, TaskContinuationOptions.ExecuteSynchronously, TaskScheduler.Default);
		        }
		    }
		}
		#endregion
		
		#region InternetMessageFormatHeaderParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Diagnostics.Contracts;
		    using System.Net.Http.Headers;
		    using System.Text;
		    
		    public enum ParserState
		    {
		        NeedMoreData,
		        Done,
		        Invalid,
		        DataTooBig,
		    }
		    
		    public class InternetMessageFormatHeaderParser
		    {
		        internal const int MinHeaderSize = 2;
		
		        private int _totalBytesConsumed;
		        private readonly int _maxHeaderSize;
		
		        private HeaderFieldState _headerState;
		        private readonly HttpHeaders _headers;
		        private readonly CurrentHeaderFieldStore _currentHeader;
		
		        public InternetMessageFormatHeaderParser(HttpHeaders headers, int maxHeaderSize)
		        {
		            // The minimum length which would be an empty header terminated by CRLF
		            if (maxHeaderSize < MinHeaderSize) throw new ArgumentOutOfRangeException("maxHeaderSize");
		            if (headers == null) throw new ArgumentNullException("headers");
		            _headers = headers;
		            _maxHeaderSize = maxHeaderSize;
		            _currentHeader = new CurrentHeaderFieldStore();
		        }
		
		        private enum HeaderFieldState
		        {
		            Name = 0,
		            Value,
		            AfterCarriageReturn,
		            FoldingLine
		        }
		
		        public ParserState ParseBuffer(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		            var parseStatus = ParserState.NeedMoreData;
		
		            if (bytesConsumed >= bytesReady) return parseStatus;  // We already can tell we need more data
		
		            try
		            {
		                parseStatus = ParseHeaderFields(
		                    buffer,
		                    bytesReady,
		                    ref bytesConsumed,
		                    ref _headerState,
		                    _maxHeaderSize,
		                    ref _totalBytesConsumed,
		                    _currentHeader,
		                    _headers);
		            }
		            catch (Exception)
		            {
		                parseStatus = ParserState.Invalid;
		            }
		
		            return parseStatus;
		        }
		
		        private static ParserState ParseHeaderFields(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed,
		            ref HeaderFieldState requestHeaderState,
		            int maximumHeaderLength,
		            ref int totalBytesConsumed,
		            CurrentHeaderFieldStore currentField,
		            HttpHeaders headers)
		        {
		            Contract.Assert((bytesReady - bytesConsumed) >= 0, "ParseHeaderFields()|(inputBufferLength - bytesParsed) < 0");
		            Contract.Assert(maximumHeaderLength <= 0 || totalBytesConsumed <= maximumHeaderLength, "ParseHeaderFields()|Headers already read exceeds limit.");
		
		            // Remember where we started.
		            var initialBytesParsed = bytesConsumed;
		            int segmentStart;
		
		            // Set up parsing status with what will happen if we exceed the buffer.
		            ParserState parseStatus = ParserState.DataTooBig;
		            var effectiveMax = maximumHeaderLength <= 0 ? Int32.MaxValue : maximumHeaderLength - totalBytesConsumed + initialBytesParsed;
		            if (bytesReady < effectiveMax)
		            {
		                parseStatus = ParserState.NeedMoreData;
		                effectiveMax = bytesReady;
		            }
		
		            Contract.Assert(bytesConsumed < effectiveMax, "We have already consumed more than the max header length.");
		
		            switch (requestHeaderState)
		            {
		                case HeaderFieldState.Name:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != ':')
		                    {
		                        if (buffer[bytesConsumed] == '\r')
		                        {
		                            if (!currentField.IsEmpty())
		                            {
		                                parseStatus = ParserState.Invalid;
		                                goto quit;
		                            }
		                            else
		                            {
		                                // Move past the '\r'
		                                requestHeaderState = HeaderFieldState.AfterCarriageReturn;
		                                if (++bytesConsumed == effectiveMax)
		                                {
		                                    goto quit;
		                                }
		
		                                goto case HeaderFieldState.AfterCarriageReturn;
		                            }
		                        }
		
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string headerFieldName = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentField.Name.Append(headerFieldName);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string headerFieldName = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentField.Name.Append(headerFieldName);
		                    }
		
		                    // Move past the ':'
		                    requestHeaderState = HeaderFieldState.Value;
		                    if (++bytesConsumed == effectiveMax)
		                    {
		                        goto quit;
		                    }
		
		                    goto case HeaderFieldState.Value;
		
		                case HeaderFieldState.Value:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != '\r')
		                    {
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            string headerFieldValue = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                            currentField.Value.Append(headerFieldValue);
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        string headerFieldValue = Encoding.UTF8.GetString(buffer, segmentStart, bytesConsumed - segmentStart);
		                        currentField.Value.Append(headerFieldValue);
		                    }
		
		                    // Move past the CR
		                    requestHeaderState = HeaderFieldState.AfterCarriageReturn;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HeaderFieldState.AfterCarriageReturn;
		
		                case HeaderFieldState.AfterCarriageReturn:
		                    if (buffer[bytesConsumed] != '\n')
		                    {
		                        parseStatus = ParserState.Invalid;
		                        goto quit;
		                    }
		
		                    if (currentField.IsEmpty())
		                    {
		                        parseStatus = ParserState.Done;
		                        bytesConsumed++;
		                        goto quit;
		                    }
		
		                    requestHeaderState = HeaderFieldState.FoldingLine;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HeaderFieldState.FoldingLine;
		
		                case HeaderFieldState.FoldingLine:
		                    if (buffer[bytesConsumed] != ' ' && buffer[bytesConsumed] != '\t')
		                    {
		                        currentField.CopyTo(headers);
		                        requestHeaderState = HeaderFieldState.Name;
		                        if (bytesConsumed == effectiveMax)
		                        {
		                            goto quit;
		                        }
		
		                        goto case HeaderFieldState.Name;
		                    }
		
		                    // Unfold line by inserting SP instead
		                    currentField.Value.Append(' ');
		
		                    // Continue parsing header field value
		                    requestHeaderState = HeaderFieldState.Value;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case HeaderFieldState.Value;
		            }
		
		        quit:
		            totalBytesConsumed += bytesConsumed - initialBytesParsed;
		            return parseStatus;
		        }
		
		        private class CurrentHeaderFieldStore
		        {
		            private const int DefaultFieldNameAllocation = 128;
		            private const int DefaultFieldValueAllocation = 2 * 1024;
		
		            private static readonly char[] _linearWhiteSpace = { ' ', '\t' };
		
		            private readonly StringBuilder _name = new StringBuilder(DefaultFieldNameAllocation);
		            private readonly StringBuilder _value = new StringBuilder(DefaultFieldValueAllocation);
		
		            public StringBuilder Name
		            {
		                get { return _name; }
		            }
		
		            public StringBuilder Value
		            {
		                get { return _value; }
		            }
		
		            public void CopyTo(HttpHeaders headers)
		            {
		                headers.Add(_name.ToString(), _value.ToString().Trim(_linearWhiteSpace));
		                Clear();
		            }
		
		            public bool IsEmpty()
		            {
		                return _name.Length == 0 && _value.Length == 0;
		            }
		
		            private void Clear()
		            {
		                _name.Clear();
		                _value.Clear();
		            }
		        }
		    }
		}
		#endregion
		
		#region MimeBodyPart
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.IO;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public class MimeBodyPart : IDisposable
		    {
		        //private static readonly Type _streamType = typeof(Stream);
		        private Stream _outputStream;
		        private MultipartStreamProvider _streamProvider;
		        private HttpContent _parentContent;
		        private HttpContent _content;
		        private HttpContentHeaders _headers;
		
		        public MimeBodyPart(MultipartStreamProvider streamProvider, int maxBodyPartHeaderSize, HttpContent parentContent)
		        {
		            Contract.Assert(streamProvider != null);
		            Contract.Assert(parentContent != null);
		            _streamProvider = streamProvider;
		            _parentContent = parentContent;
		            Segments = new List<ArraySegment<byte>>(2);
		            _headers = FormattingUtilities.CreateEmptyContentHeaders();
		            HeaderParser = new InternetMessageFormatHeaderParser(_headers, maxBodyPartHeaderSize);
		        }
		
		        public InternetMessageFormatHeaderParser HeaderParser { get; private set; }
		
		        public HttpContent GetCompletedHttpContent()
		        {
		            Contract.Assert(IsComplete);
		            if (_content == null) return null;
		            _headers.CopyTo(_content.Headers);
		            return _content;
		        }
		
		        public List<ArraySegment<byte>> Segments { get; private set; }
		
		        public bool IsComplete { get; set; }
		
		        public bool IsFinal { get; set; }
		
		        public async Task WriteSegment(ArraySegment<byte> segment, CancellationToken cancellationToken)
		        {
		            var stream = GetOutputStream();
		            await stream.WriteAsync(segment.Array, segment.Offset, segment.Count, cancellationToken);
		        }
		
		        private Stream GetOutputStream()
		        {
		            if (_outputStream == null)
		            {
		                try
		                {
		                    _outputStream = _streamProvider.GetStream(_parentContent, _headers);
		                }
		                catch (Exception)
		                {
		                    throw new InvalidOperationException("ReadAsMimeMultipartStreamProviderException");
		                }
		
		                if (_outputStream == null)
		                {
		                    throw new InvalidOperationException("ReadAsMimeMultipartStreamProviderNull");
		                }
		
		                if (!_outputStream.CanWrite)
		                {
		                    throw new InvalidOperationException("ReadAsMimeMultipartStreamProviderReadOnly");
		                }
		                _content = new StreamContent(_outputStream);
		            }
		
		            return _outputStream;
		        }
		
		        public void Dispose()
		        {
		            Dispose(true);
		            GC.SuppressFinalize(this);
		        }
		
		        protected void Dispose(bool disposing)
		        {
		            if (disposing)
		            {
		                CleanupOutputStream();
		                CleanupHttpContent();
		                _parentContent = null;
		                HeaderParser = null;
		                Segments.Clear();
		            }
		        }
		
		        private void CleanupHttpContent()
		        {
		            if (!IsComplete && _content != null) _content.Dispose();
		            _content = null;
		        }
		
		        private void CleanupOutputStream()
		        {
		            if (_outputStream != null)
		            {
		                var output = _outputStream as MemoryStream;
		                if (output != null)
		                {
		                    output.Position = 0;
		                }
		                else
		                {
		                    //_outputStream.Close(); 
		                    _outputStream.Dispose(); //Changed to accomodate WinRT
		                }
		
		                _outputStream = null;
		            }
		        }
		    }
		}
		#endregion
		
		#region MimeMultipartBodyPartParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Diagnostics.Contracts;
		    using System.IO;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    
		    public class MimeMultipartBodyPartParser : IDisposable
		    {
		        public const long DefaultMaxMessageSize = Int64.MaxValue;
		        private const int DefaultMaxBodyPartHeaderSize = 4 * 1024;
		
		        // MIME parser
		        private MimeMultipartParser _mimeParser;
		        private MimeMultipartParser.State _mimeStatus = MimeMultipartParser.State.NeedMoreData;
		        private readonly ArraySegment<byte>[] _parsedBodyPart = new ArraySegment<byte>[2];
		        private MimeBodyPart _currentBodyPart;
		        private bool _isFirst = true;
		
		        // Header field parser
		        private ParserState _bodyPartHeaderStatus = ParserState.NeedMoreData;
		        private readonly int _maxBodyPartHeaderSize;
		
		        // Stream provider
		        private readonly MultipartStreamProvider _streamProvider;
		
		        private readonly HttpContent _content;
		
		        public MimeMultipartBodyPartParser(HttpContent content, MultipartStreamProvider streamProvider) : this(content, streamProvider, DefaultMaxMessageSize, DefaultMaxBodyPartHeaderSize) {}
		
		        public MimeMultipartBodyPartParser(
		            HttpContent content,
		            MultipartStreamProvider streamProvider,
		            long maxMessageSize,
		            int maxBodyPartHeaderSize)
		        {
		            Contract.Assert(content != null, "content cannot be null.");
		            Contract.Assert(streamProvider != null, "streamProvider cannot be null.");
		
		            string boundary = ValidateArguments(content, maxMessageSize, true);
		
		            _mimeParser = new MimeMultipartParser(boundary, maxMessageSize);
		            _currentBodyPart = new MimeBodyPart(streamProvider, maxBodyPartHeaderSize, content);
		            _content = content;
		            _maxBodyPartHeaderSize = maxBodyPartHeaderSize;
		
		            _streamProvider = streamProvider;
		        }
		
		        public static bool IsMimeMultipartContent(HttpContent content)
		        {
		            Contract.Assert(content != null, "content cannot be null.");
		            try
		            {
		                string boundary = ValidateArguments(content, DefaultMaxMessageSize, false);
		                return boundary != null;
		            }
		            catch (Exception)
		            {
		                return false;
		            }
		        }
		
		        public void Dispose()
		        {
		            Dispose(true);
		            GC.SuppressFinalize(this);
		        }
		
		        public IEnumerable<MimeBodyPart> ParseBuffer(byte[] data, int bytesRead)
		        {
		            var bytesConsumed = 0;
		            // There's a special case here - if we've reached the end of the message and there's no optional
		            // CRLF, then we're out of bytes to read, but we have finished the message. 
		            //
		            // If IsWaitingForEndOfMessage is true and we're at the end of the stream, then we're going to 
		            // call into the parser again with an empty array as the buffer to signal the end of the parse. 
		            // Then the final boundary segment will be marked as complete. 
		            if (bytesRead == 0 && !_mimeParser.IsWaitingForEndOfMessage)
		            {
		                CleanupCurrentBodyPart();
		                throw new IOException("ReadAsMimeMultipartUnexpectedTermination");
		            }
		
		            // Make sure we remove an old array segments.
		            _currentBodyPart.Segments.Clear();
		
		            while (_mimeParser.CanParseMore(bytesRead, bytesConsumed))
		            {
		                bool isFinal;
		                _mimeStatus = _mimeParser.ParseBuffer(data, bytesRead, ref bytesConsumed, out _parsedBodyPart[0], out _parsedBodyPart[1], out isFinal);
		                if (_mimeStatus != MimeMultipartParser.State.BodyPartCompleted && _mimeStatus != MimeMultipartParser.State.NeedMoreData)
		                {
		                    CleanupCurrentBodyPart();
		                    throw new InvalidOperationException("ReadAsMimeMultipartParseError");
		                }
		
		                // First body is empty preamble which we just ignore
		                if (_isFirst)
		                {
		                    if (_mimeStatus == MimeMultipartParser.State.BodyPartCompleted) _isFirst = false;
		                    continue;
		                }
		
		                // Parse the two array segments containing parsed body parts that the MIME parser gave us
		                foreach (ArraySegment<byte> part in _parsedBodyPart)
		                {
		                    if (part.Count == 0) continue;
		                    if (_bodyPartHeaderStatus != ParserState.Done)
		                    {
		                        int headerConsumed = part.Offset;
		                        _bodyPartHeaderStatus = _currentBodyPart.HeaderParser.ParseBuffer(part.Array, part.Count + part.Offset, ref headerConsumed);
		                        if (_bodyPartHeaderStatus == ParserState.Done)
		                        {
		                            // Add the remainder as body part content
		                            _currentBodyPart.Segments.Add(new ArraySegment<byte>(part.Array, headerConsumed, part.Count + part.Offset - headerConsumed));
		                        }
		                        else if (_bodyPartHeaderStatus != ParserState.NeedMoreData)
		                        {
		                            CleanupCurrentBodyPart();
		                            throw new InvalidOperationException("ReadAsMimeMultipartHeaderParseError");
		                        }
		                    }
		                    else
		                    {
		                        // Add the data as body part content
		                        _currentBodyPart.Segments.Add(part);
		                    }
		                }
		
		                if (_mimeStatus == MimeMultipartParser.State.BodyPartCompleted)
		                {
		                    // If body is completed then swap current body part
		                    MimeBodyPart completed = _currentBodyPart;
		                    completed.IsComplete = true;
		                    completed.IsFinal = isFinal;
		
		                    _currentBodyPart = new MimeBodyPart(_streamProvider, _maxBodyPartHeaderSize, _content);
		
		                    _mimeStatus = MimeMultipartParser.State.NeedMoreData;
		                    _bodyPartHeaderStatus = ParserState.NeedMoreData;
		                    yield return completed;
		                }
		                else
		                {
		                    // Otherwise return what we have 
		                    yield return _currentBodyPart;
		                }
		            }
		        }
		
		        /// <summary>
		        /// Releases unmanaged and - optionally - managed resources
		        /// </summary>
		        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		        protected void Dispose(bool disposing)
		        {
		            if (disposing)
		            {
		                _mimeParser = null;
		                CleanupCurrentBodyPart();
		            }
		        }
		
		        private static string ValidateArguments(HttpContent content, long maxMessageSize, bool throwOnError)
		        {
		            Contract.Assert(content != null, "content cannot be null.");
		            if (maxMessageSize < MimeMultipartParser.MinMessageSize)
		            {
		                if (throwOnError) throw new ArgumentOutOfRangeException("maxMessageSize");
		                else return null;
		            }
		
		            MediaTypeHeaderValue contentType = content.Headers.ContentType;
		            if (contentType == null)
		            {
		                if (throwOnError) throw new ArgumentException("ReadAsMimeMultipartArgumentNoContentType", "content");
		                else return null;
		            }
		
		            if (!contentType.MediaType.StartsWith("multipart", StringComparison.OrdinalIgnoreCase))
		            {
		                if (throwOnError) throw new ArgumentException("ReadAsMimeMultipartArgumentNoMultipart", "content");
		                else return null;
		            }
		
		            string boundary = null;
		            foreach (NameValueHeaderValue p in contentType.Parameters)
		            {
		                if (p.Name.Equals("boundary", StringComparison.OrdinalIgnoreCase))
		                {
		                    boundary = FormattingUtilities.UnquoteToken(p.Value);
		                    break;
		                }
		            }
		
		            if (boundary == null)
		            {
		                if (throwOnError) throw new ArgumentException("ReadAsMimeMultipartArgumentNoBoundary", "content"); 
		                else return null;
		            }
		
		            return boundary;
		        }
		
		        private void CleanupCurrentBodyPart()
		        {
		            if (_currentBodyPart != null)
		            {
		                _currentBodyPart.Dispose();
		                _currentBodyPart = null;
		            }
		        }
		    }
		}
		#endregion
		
		#region MimeMultipartParser
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Diagnostics;
		    using System.Diagnostics.CodeAnalysis;
		    using System.Diagnostics.Contracts;
		    using System.Globalization;
		    using System.Text;
		
		    public class MimeMultipartParser
		    {
		        public const int MinMessageSize = 10;
		
		        private const int MaxBoundarySize = 256;
		
		        // ReSharper disable InconsistentNaming
		        private const byte HTAB = 0x09;
		        private const byte SP = 0x20;
		        private const byte CR = 0x0D;
		        private const byte LF = 0x0A;
		        // ReSharper restore InconsistentNaming
		        private const byte Dash = 0x2D;
		        private static readonly ArraySegment<byte> _emptyBodyPart = new ArraySegment<byte>(new byte[0]);
		
		        private long _totalBytesConsumed;
		        private readonly long _maxMessageSize;
		
		        private BodyPartState _bodyPartState;
		        private readonly CurrentBodyPartStore _currentBoundary;
		
		        public MimeMultipartParser(string boundary, long maxMessageSize)
		        {
		            // The minimum length which would be an empty message terminated by CRLF
		            if (maxMessageSize < MinMessageSize) throw new ArgumentOutOfRangeException("maxMessageSize");
		            if (String.IsNullOrWhiteSpace(boundary)) throw new ArgumentNullException("boundary");
		            if (boundary.Length > MaxBoundarySize - 10) throw new ArgumentOutOfRangeException("boundary");
		            if (boundary.EndsWith(" ", StringComparison.Ordinal)) throw new ArgumentException("MimeMultipartParserBadBoundary", "boundary");
		
		            _maxMessageSize = maxMessageSize;
		            _currentBoundary = new CurrentBodyPartStore(boundary);
		            _bodyPartState = BodyPartState.AfterFirstLineFeed;
		        }
		
		        public bool IsWaitingForEndOfMessage
		        {
		            get
		            {
		                return
		                    _bodyPartState == BodyPartState.AfterBoundary &&
		                    _currentBoundary != null &&
		                    _currentBoundary.IsFinal;
		            }
		        }
		
		        private enum BodyPartState
		        {
		            BodyPart = 0,
		            AfterFirstCarriageReturn,
		            AfterFirstLineFeed,
		            AfterFirstDash,
		            Boundary,
		            AfterBoundary,
		            AfterSecondDash,
		            AfterSecondCarriageReturn
		        }
		
		        // ReSharper disable once UnusedMember.Local
		        private enum MessageState
		        {
		            Boundary = 0, // about to parse boundary
		            BodyPart, // about to parse body-part
		            CloseDelimiter // about to read close-delimiter
		        }
		
		        public enum State
		        {
		            /// <summary>
		            /// Need more data
		            /// </summary>
		            NeedMoreData = 0,
		
		            /// <summary>
		            /// Parsing of a complete body part succeeded.
		            /// </summary>
		            BodyPartCompleted,
		
		            /// <summary>
		            /// Bad data format
		            /// </summary>
		            Invalid,
		
		            /// <summary>
		            /// Data exceeds the allowed size
		            /// </summary>
		            DataTooBig,
		        }
		
		        public bool CanParseMore(int bytesRead, int bytesConsumed)
		        {
		            if (bytesConsumed < bytesRead)
		            {
		                // If there's more bytes we haven't parsed, then we can parse more
		                return true;
		            }
		
		            if (bytesRead == 0 && IsWaitingForEndOfMessage)
		            {
		                // If we're waiting for the end of the message and we've arrived there, we want parse to be called
		                // again so we can mark the parse as complete.
		                //
		                // This can happen when the last boundary segment doesn't have a trailing CRLF. We need to wait until
		                // the end of the message to complete the parse because we need to consume any trailing whitespace that's
		                //present.
		                return true;
		            }
		
		            return false;
		        }
		
		        public State ParseBuffer(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed,
		            out ArraySegment<byte> remainingBodyPart,
		            out ArraySegment<byte> bodyPart,
		            out bool isFinalBodyPart)
		        {
		            if (buffer == null) throw new ArgumentNullException("buffer");
		
		            State parseStatus;
		            isFinalBodyPart = false;
		
		            try
		            {
		                parseStatus = ParseBodyPart(
		                    buffer,
		                    bytesReady,
		                    ref bytesConsumed,
		                    ref _bodyPartState,
		                    _maxMessageSize,
		                    ref _totalBytesConsumed,
		                    _currentBoundary);
		            }
		            catch (Exception)
		            {
		                parseStatus = State.Invalid;
		            }
		
		            remainingBodyPart = _currentBoundary.GetDiscardedBoundary();
		            bodyPart = _currentBoundary.BodyPart;
		            if (parseStatus == State.BodyPartCompleted)
		            {
		                isFinalBodyPart = _currentBoundary.IsFinal;
		                _currentBoundary.ClearAll();
		            }
		            else
		            {
		                _currentBoundary.ClearBodyPart();
		            }
		
		            return parseStatus;
		        }
		
		        [SuppressMessage("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity", Justification = "This is a parser which cannot be split up for performance reasons.")]
		        private static State ParseBodyPart(
		            byte[] buffer,
		            int bytesReady,
		            ref int bytesConsumed,
		            ref BodyPartState bodyPartState,
		            long maximumMessageLength,
		            ref long totalBytesConsumed,
		            CurrentBodyPartStore currentBodyPart)
		        {
		            Contract.Assert((bytesReady - bytesConsumed) >= 0, "ParseBodyPart()|(bytesReady - bytesConsumed) < 0");
		            Contract.Assert(maximumMessageLength <= 0 || totalBytesConsumed <= maximumMessageLength, "ParseBodyPart()|Message already read exceeds limit.");
		
		            // Remember where we started.
		            int segmentStart;
		            var initialBytesParsed = bytesConsumed;
		
		            if (bytesReady == 0 && bodyPartState == BodyPartState.AfterBoundary && currentBodyPart.IsFinal)
		            {
		                // We've seen the end of the stream - the final body part has no trailing CRLF
		                return State.BodyPartCompleted;
		            }
		
		            // Set up parsing status with what will happen if we exceed the buffer.
		            State parseStatus = State.DataTooBig;
		            var effectiveMax = maximumMessageLength <= 0 ? Int64.MaxValue : (maximumMessageLength - totalBytesConsumed + bytesConsumed);
		            if (effectiveMax == 0)
		            {
		                // effectiveMax is based on our max message size - if we've arrrived at the max size, then we need
		                // to stop parsing.
		                return State.DataTooBig;
		            }
		
		            if (bytesReady <= effectiveMax)
		            {
		                parseStatus = State.NeedMoreData;
		                effectiveMax = bytesReady;
		            }
		
		            currentBodyPart.ResetBoundaryOffset();
		
		            Contract.Assert(bytesConsumed < effectiveMax, "We have already consumed more than the max header length.");
		
		            switch (bodyPartState)
		            {
		                case BodyPartState.BodyPart:
		                    while (buffer[bytesConsumed] != CR)
		                    {
		                        if (++bytesConsumed == effectiveMax) goto quit;
		                    }
		
		                    // Remember potential boundary
		                    currentBodyPart.AppendBoundary(CR);
		
		                    // Move past the CR
		                    bodyPartState = BodyPartState.AfterFirstCarriageReturn;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case BodyPartState.AfterFirstCarriageReturn;
		
		                case BodyPartState.AfterFirstCarriageReturn:
		                    if (buffer[bytesConsumed] != LF)
		                    {
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                    // Remember potential boundary
		                    currentBodyPart.AppendBoundary(LF);
		
		                    // Move past the CR
		                    bodyPartState = BodyPartState.AfterFirstLineFeed;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case BodyPartState.AfterFirstLineFeed;
		
		                case BodyPartState.AfterFirstLineFeed:
		                    if (buffer[bytesConsumed] == CR)
		                    {
		                        // Remember potential boundary
		                        currentBodyPart.ResetBoundary();
		                        currentBodyPart.AppendBoundary(CR);
		
		                        // Move past the CR
		                        bodyPartState = BodyPartState.AfterFirstCarriageReturn;
		                        if (++bytesConsumed == effectiveMax) goto quit;
		
		                        goto case BodyPartState.AfterFirstCarriageReturn;
		                    }
		
		                    if (buffer[bytesConsumed] != Dash)
		                    {
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                    // Remember potential boundary
		                    currentBodyPart.AppendBoundary(Dash);
		
		                    // Move past the Dash
		                    bodyPartState = BodyPartState.AfterFirstDash;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case BodyPartState.AfterFirstDash;
		
		                case BodyPartState.AfterFirstDash:
		                    if (buffer[bytesConsumed] != Dash)
		                    {
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                    // Remember potential boundary
		                    currentBodyPart.AppendBoundary(Dash);
		
		                    // Move past the Dash
		                    bodyPartState = BodyPartState.Boundary;
		                    if (++bytesConsumed == effectiveMax) goto quit;
		
		                    goto case BodyPartState.Boundary;
		
		                case BodyPartState.Boundary:
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != CR)
		                    {
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            if (currentBodyPart.AppendBoundary(buffer, segmentStart, bytesConsumed - segmentStart))
		                            {
		                                if (currentBodyPart.IsBoundaryComplete())
		                                {
		                                    // At this point we've seen the end of a boundary segment that is aligned at the end
		                                    // of the buffer - this might be because we have another segment coming or it might
		                                    // truly be the end of the message.
		                                    bodyPartState = BodyPartState.AfterBoundary;
		                                }
		                            }
		                            else
		                            {
		                                currentBodyPart.ResetBoundary();
		                                bodyPartState = BodyPartState.BodyPart;
		                            }
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        if (!currentBodyPart.AppendBoundary(buffer, segmentStart, bytesConsumed - segmentStart))
		                        {
		                            currentBodyPart.ResetBoundary();
		                            bodyPartState = BodyPartState.BodyPart;
		                            goto case BodyPartState.BodyPart;
		                        }
		                    }
		
		                    goto case BodyPartState.AfterBoundary;
		
		                case BodyPartState.AfterBoundary:
		
		                    // This state means that we just saw the end of a boundary. It might by a 'normal' boundary, in which
		                    // case it's followed by optional whitespace and a CRLF. Or it might be the 'final' boundary and will 
		                    // be followed by '--', optional whitespace and an optional CRLF.
		                    if (buffer[bytesConsumed] == Dash && !currentBodyPart.IsFinal)
		                    {
		                        currentBodyPart.AppendBoundary(Dash);
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            bodyPartState = BodyPartState.AfterSecondDash;
		                            goto quit;
		                        }
		
		                        goto case BodyPartState.AfterSecondDash;
		                    }
		
		                    // Capture optional whitespace
		                    segmentStart = bytesConsumed;
		                    while (buffer[bytesConsumed] != CR)
		                    {
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            if (!currentBodyPart.AppendBoundary(buffer, segmentStart, bytesConsumed - segmentStart))
		                            {
		                                // It's an unexpected character
		                                currentBodyPart.ResetBoundary();
		                                bodyPartState = BodyPartState.BodyPart;
		                            }
		
		                            goto quit;
		                        }
		                    }
		
		                    if (bytesConsumed > segmentStart)
		                    {
		                        if (!currentBodyPart.AppendBoundary(buffer, segmentStart, bytesConsumed - segmentStart))
		                        {
		                            currentBodyPart.ResetBoundary();
		                            bodyPartState = BodyPartState.BodyPart;
		                            goto case BodyPartState.BodyPart;
		                        }
		                    }
		
		                    if (buffer[bytesConsumed] == CR)
		                    {
		                        currentBodyPart.AppendBoundary(CR);
		                        if (++bytesConsumed == effectiveMax)
		                        {
		                            bodyPartState = BodyPartState.AfterSecondCarriageReturn;
		                            goto quit;
		                        }
		
		                        goto case BodyPartState.AfterSecondCarriageReturn;
		                    }
		                    else
		                    {
		                        // It's an unexpected character
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                case BodyPartState.AfterSecondDash:
		                    if (buffer[bytesConsumed] == Dash)
		                    {
		                        currentBodyPart.AppendBoundary(Dash);
		                        bytesConsumed++;
		
		                        if (currentBodyPart.IsBoundaryComplete())
		                        {
		                            Debug.Assert(currentBodyPart.IsFinal);
		
		                            // If we get in here, it means we've see the trailing '--' of the last boundary - in order to consume all of the 
		                            // remaining bytes, we don't mark the parse as complete again - wait until this method is called again with the 
		                            // empty buffer to do that.
		                            bodyPartState = BodyPartState.AfterBoundary;
		                            parseStatus = State.NeedMoreData;
		                            goto quit;
		                        }
		                        else
		                        {
		                            currentBodyPart.ResetBoundary();
		                            if (bytesConsumed == effectiveMax) goto quit;
		
		                            goto case BodyPartState.BodyPart;
		                        }
		                    }
		                    else
		                    {
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                case BodyPartState.AfterSecondCarriageReturn:
		                    if (buffer[bytesConsumed] != LF)
		                    {
		                        currentBodyPart.ResetBoundary();
		                        bodyPartState = BodyPartState.BodyPart;
		                        goto case BodyPartState.BodyPart;
		                    }
		
		                    currentBodyPart.AppendBoundary(LF);
		                    bytesConsumed++;
		
		                    bodyPartState = BodyPartState.BodyPart;
		                    if (currentBodyPart.IsBoundaryComplete())
		                    {
		                        parseStatus = State.BodyPartCompleted;
		                        goto quit;
		                    }
		                    else
		                    {
		                        currentBodyPart.ResetBoundary();
		                        if (bytesConsumed == effectiveMax) goto quit;
		
		                        goto case BodyPartState.BodyPart;
		                    }
		            }
		
		        quit:
		            if (initialBytesParsed < bytesConsumed)
		            {
		                int boundaryLength = currentBodyPart.BoundaryDelta;
		                if (boundaryLength > 0 && parseStatus != State.BodyPartCompleted)
		                {
		                    currentBodyPart.HasPotentialBoundaryLeftOver = true;
		                }
		
		                int bodyPartEnd = bytesConsumed - initialBytesParsed - boundaryLength;
		
		                currentBodyPart.BodyPart = new ArraySegment<byte>(buffer, initialBytesParsed, bodyPartEnd);
		            }
		
		            totalBytesConsumed += bytesConsumed - initialBytesParsed;
		            return parseStatus;
		        }
		
		        private class CurrentBodyPartStore
		        {
		            private const int InitialOffset = 2;
		
		            private readonly byte[] _boundaryStore = new byte[MaxBoundarySize];
		            private int _boundaryStoreLength;
		
		            private readonly byte[] _referenceBoundary = new byte[MaxBoundarySize];
		            private readonly int _referenceBoundaryLength;
		
		            private readonly byte[] _boundary = new byte[MaxBoundarySize];
		            private int _boundaryLength;
		
		            private ArraySegment<byte> _bodyPart = _emptyBodyPart;
		            private bool _isFinal;
		            private bool _isFirst = true;
		            private bool _releaseDiscardedBoundary;
		            private int _boundaryOffset;
		
		            public CurrentBodyPartStore(string referenceBoundary)
		            {
		                Contract.Assert(referenceBoundary != null);
		
		                _referenceBoundary[0] = CR;
		                _referenceBoundary[1] = LF;
		                _referenceBoundary[2] = Dash;
		                _referenceBoundary[3] = Dash;
		                _referenceBoundaryLength = 4 + Encoding.UTF8.GetBytes(referenceBoundary, 0, referenceBoundary.Length, _referenceBoundary, 4);
		
		                _boundary[0] = CR;
		                _boundary[1] = LF;
		                _boundaryLength = InitialOffset;
		            }
		
		            public bool HasPotentialBoundaryLeftOver { get; set; }
		
		            public int BoundaryDelta
		            {
		                get { return (_boundaryLength - _boundaryOffset > 0) ? _boundaryLength - _boundaryOffset : _boundaryLength; }
		            }
		
		            public ArraySegment<byte> BodyPart
		            {
		                get { return _bodyPart; }
		                set { _bodyPart = value; }
		            }
		
		            public bool IsFinal
		            {
		                get { return _isFinal; }
		            }
		
		            public void ResetBoundaryOffset()
		            {
		                _boundaryOffset = _boundaryLength;
		            }
		
		            public void ResetBoundary()
		            {
		                // If we had a potential boundary left over then store it so that we don't loose it
		                if (HasPotentialBoundaryLeftOver)
		                {
		                    Buffer.BlockCopy(_boundary, 0, _boundaryStore, 0, _boundaryOffset);
		                    _boundaryStoreLength = _boundaryOffset;
		                    HasPotentialBoundaryLeftOver = false;
		                    _releaseDiscardedBoundary = true;
		                }
		
		                _boundaryLength = 0;
		                _boundaryOffset = 0;
		            }
		
		            public void AppendBoundary(byte data)
		            {
		                _boundary[_boundaryLength++] = data;
		            }
		
		            public bool AppendBoundary(byte[] data, int offset, int count)
		            {
		                // Check that potential boundary is not bigger than our reference boundary. 
		                // Allow for 2 extra characters to include the final boundary which ends with 
		                // an additional "--" sequence + plus up to 4 LWS characters (which are allowed). 
		                if (_boundaryLength + count > _referenceBoundaryLength + 6) return false;
		
		                var cnt = _boundaryLength;
		                Buffer.BlockCopy(data, offset, _boundary, _boundaryLength, count);
		                _boundaryLength += count;
		
		                // Verify that boundary matches so far
		                var maxCount = Math.Min(_boundaryLength, _referenceBoundaryLength);
		                for (; cnt < maxCount; cnt++)
		                {
		                    if (_boundary[cnt] != _referenceBoundary[cnt]) return false;
		                }
		
		                return true;
		            }
		
		            public ArraySegment<byte> GetDiscardedBoundary()
		            {
		                if (_boundaryStoreLength > 0 && _releaseDiscardedBoundary)
		                {
		                    ArraySegment<byte> discarded = new ArraySegment<byte>(_boundaryStore, 0, _boundaryStoreLength);
		                    _boundaryStoreLength = 0;
		                    return discarded;
		                }
		
		                return _emptyBodyPart;
		            }
		
		            public bool IsBoundaryValid()
		            {
		                var offset = 0;
		                if (_isFirst)
		                {
		                    offset = InitialOffset;
		                }
		
		                var count = offset;
		                for (; count < _referenceBoundaryLength; count++)
		                {
		                    if (_boundary[count] != _referenceBoundary[count])
		                    {
		                        return false;
		                    }
		                }
		
		                // Check for final
		                var boundaryIsFinal = false;
		                if (_boundary[count] == Dash &&
		                    _boundary[count + 1] == Dash)
		                {
		                    boundaryIsFinal = true;
		                    count += 2;
		                }
		
		                // Rest of boundary must be ignorable whitespace in order for it to match
		                for (; count < _boundaryLength - 2; count++)
		                {
		                    if (_boundary[count] != SP && _boundary[count] != HTAB)
		                    {
		                        return false;
		                    }
		                }
		
		                // We have a valid boundary so whatever we stored in the boundary story is no longer needed
		                _isFinal = boundaryIsFinal;
		                _isFirst = false;
		
		                return true;
		            }
		
		            public bool IsBoundaryComplete()
		            {
		                if (!IsBoundaryValid()) return false;
		                if (_boundaryLength < _referenceBoundaryLength) return false;
		                if (_boundaryLength == _referenceBoundaryLength + 1 && _boundary[_referenceBoundaryLength] == Dash) return false;
		                return true;
		            }
		
		            public void ClearBodyPart()
		            {
		                BodyPart = _emptyBodyPart;
		            }
		
		            public void ClearAll()
		            {
		                _releaseDiscardedBoundary = false;
		                HasPotentialBoundaryLeftOver = false;
		                _boundaryLength = 0;
		                _boundaryOffset = 0;
		                _boundaryStoreLength = 0;
		                _isFinal = false;
		                ClearBodyPart();
		            }
		
		            private string DebuggerToString()
		            {
		                var referenceBoundary = Encoding.UTF8.GetString(_referenceBoundary, 0, _referenceBoundaryLength);
		                var boundary = Encoding.UTF8.GetString(_boundary, 0, _boundaryLength);
		
		                return String.Format(
		                    CultureInfo.InvariantCulture,
		                    "Expected: {0} *** Current: {1}",
		                    referenceBoundary,
		                    boundary);
		            }
		        }
		    }
		}
		#endregion
		
		#region MultipartContent
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.Collections;
		    using System.Collections.Generic;
		    using System.IO;
		    using System.Linq;
		    using System.Net;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using System.Reflection;
		    using System.Text;
		    using System.Threading.Tasks;
		
		    public class MultipartContent : HttpContent, IEnumerable<HttpContent>
		    {
		        private readonly List<HttpContent> _nestedContent;
		        private readonly string _boundary;
		        private int _nextContentIndex;
		        private Stream _outputStream;
		        private TaskCompletionSource<object> _tcs;
		        //private const string crlf = "\r\n";
		
		        public MultipartContent() : this("mixed", GetDefaultBoundary()){}
		
		        public MultipartContent(string subtype) : this(subtype, GetDefaultBoundary()) {}
		
		        public MultipartContent(string subtype, string boundary)
		        {
		            if (string.IsNullOrWhiteSpace(subtype)) throw new ArgumentException("subtype");
		            ValidateBoundary(boundary);
		            _boundary = boundary;
		            var str = boundary;
		            if (!str.StartsWith("\"", StringComparison.Ordinal)) str = "\"" + str + "\"";
		            Headers.ContentType = new MediaTypeHeaderValue("multipart/" + subtype)
		            {
		                Parameters = { new NameValueHeaderValue("boundary", str) }
		            };
		            _nestedContent = new List<HttpContent>();
		        }
		
		        public virtual void Add(HttpContent content)
		        {
		            if (content == null) throw new ArgumentNullException("content");
		            _nestedContent.Add(content);
		        }
		
		        protected override void Dispose(bool disposing)
		        {
		            if (disposing)
		            {
		                foreach (HttpContent httpContent in _nestedContent) httpContent.Dispose();
		                _nestedContent.Clear();
		            }
		            base.Dispose(disposing);
		        }
		
		        public IEnumerator<HttpContent> GetEnumerator()
		        {
		            return _nestedContent.GetEnumerator();
		        }
		
		        IEnumerator IEnumerable.GetEnumerator()
		        {
		            return _nestedContent.GetEnumerator();
		        }
		
		        protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
		        {
		            TaskCompletionSource<object> completionSource = new TaskCompletionSource<object>();
		            _tcs = completionSource;
		            _outputStream = stream;
		            _nextContentIndex = 0;
		            EncodeStringToStreamAsync(_outputStream, "--" + _boundary + "\r\n").ContinueWithStandard(WriteNextContentHeadersAsync);
		            return completionSource.Task;
		        }
		
		        protected override bool TryComputeLength(out long length)
		        {
		            const long num1 = 0L;
		            var num2 = (long) GetEncodedLength("\r\n--" + _boundary + "\r\n");
		            var num3 = num1 + GetEncodedLength("--" + _boundary + "\r\n");
		            var flag = true;
		            foreach (var httpContent in _nestedContent)
		            {
		                if (flag) flag = false;
		                else num3 += num2;
		
		                foreach (var keyValuePair in httpContent.Headers) num3 += GetEncodedLength(keyValuePair.Key + ": " + string.Join(", ", keyValuePair.Value) + "\r\n");
		                num3 += "\r\n".Length;
		            
		                // ReSharper disable once ConvertToConstant.Local
		                var length1 = 0L;
		                var methodInfo = httpContent.GetType().GetMethod("TryComputeLength", BindingFlags.Instance | BindingFlags.NonPublic);
		                var result = (bool)methodInfo.Invoke(httpContent, new[] {/*out*/(object)length1}); 
		                if (result)
		                {
		                    length = 0L;
		                    return false;
		                }
		
		                num3 += length1;
		            }
		            var num4 = num3 + GetEncodedLength("\r\n--" + _boundary + "--\r\n");
		            length = num4;
		            return true;
		        }
		
		        private static void ValidateBoundary(string boundary)
		        {
		            if (string.IsNullOrWhiteSpace(boundary)) throw new ArgumentException("boundary");
		            if (boundary.Length > 70) throw new ArgumentOutOfRangeException("boundary", "boundary is longer than 70 characters");
		            if (boundary.EndsWith(" ", StringComparison.Ordinal)) throw new ArgumentException("boundary ends with blank", "boundary");
		            if (boundary.Any(ch => (48 > ch || ch > 57) && (97 > ch || ch > 122) && ((65 > ch || ch > 90) && "'()+_,-./:=? ".IndexOf(ch) < 0))) throw new ArgumentException("boundary contains invalid characters", "boundary");
		        }
		
		        private static string GetDefaultBoundary()
		        {
		            return Guid.NewGuid().ToString();
		        }
		
		        private void WriteNextContentHeadersAsync(Task task)
		        {
		            if (task.IsFaulted)
		            {
		                // ReSharper disable once PossibleNullReferenceException
		                HandleAsyncException(task.Exception.GetBaseException());
		            }
		            else
		            {
		                try
		                {
		                    if (_nextContentIndex >= _nestedContent.Count)
		                    {
		                        WriteTerminatingBoundaryAsync();
		                    }
		                    else
		                    {
		                        var str = "\r\n--" + _boundary + "\r\n";
		                        var stringBuilder = new StringBuilder();
		                        if (_nextContentIndex != 0) stringBuilder.Append(str);
		                        foreach (var keyValuePair in _nestedContent[_nextContentIndex].Headers) stringBuilder.Append(keyValuePair.Key + ": " + string.Join(", ", keyValuePair.Value) + "\r\n");
		                        stringBuilder.Append("\r\n");
		                        EncodeStringToStreamAsync(_outputStream, stringBuilder.ToString()).ContinueWithStandard(WriteNextContentAsync);
		                    }
		                }
		                catch (Exception ex)
		                {
		                  HandleAsyncException(ex);
		                }
		            }
		        }
		
		        private void WriteNextContentAsync(Task task)
		        {
		            if (task.IsFaulted)
		            {
		                // ReSharper disable once PossibleNullReferenceException
		                HandleAsyncException(task.Exception.GetBaseException());
		            }
		            else
		            {
		                try
		                {
		                    var httpContent = _nestedContent[_nextContentIndex];
		                    ++_nextContentIndex;
		                    httpContent.CopyToAsync(_outputStream).ContinueWithStandard(WriteNextContentHeadersAsync);
		                }
		                catch (Exception ex)
		                {
		                    HandleAsyncException(ex);
		                }
		            }
		        }
		
		        private void WriteTerminatingBoundaryAsync()
		        {
		            try
		            {
		                EncodeStringToStreamAsync(_outputStream, "\r\n--" + _boundary + "--\r\n").ContinueWithStandard(task =>
		                {
		                    // ReSharper disable once PossibleNullReferenceException
		                    if (task.IsFaulted) HandleAsyncException(task.Exception.GetBaseException());
		                    else CleanupAsync().TrySetResult(null);
		                });
		            }
		            catch (Exception ex)
		            {
		                HandleAsyncException(ex);
		            }
		        }
		
		        private static Task EncodeStringToStreamAsync(Stream stream, string input)
		        {
		            var bytes = Encoding.GetEncoding(28591).GetBytes(input);
		            //return Task.Factory.FromAsync(stream.BeginWrite, stream.EndWrite, bytes, 0, bytes.Length, null); 
		            return stream.WriteAsync(bytes, 0, bytes.Length); //Changed to accomodate WinRT
		        }
		
		        private TaskCompletionSource<object> CleanupAsync()
		        {
		            var completionSource = _tcs;
		            _outputStream = null;
		            _nextContentIndex = 0;
		            _tcs = null;
		            return completionSource;
		        }
		
		        private void HandleAsyncException(Exception ex)
		        {
		            CleanupAsync().TrySetException(ex);
		        }
		
		        private static int GetEncodedLength(string input)
		        {
		            return Encoding.GetEncoding(28591).GetByteCount(input);
		        }
		    }
		}
		
		#endregion
		
		#region MultipartMemoryStreamProvider
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System;
		    using System.IO;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		
		    public class MultipartMemoryStreamProvider : MultipartStreamProvider
		    {
		        public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
		        {
		            if (parent == null) throw new ArgumentNullException("parent");
		            if (headers == null) throw new ArgumentNullException("headers");
		            return new MemoryStream();
		        }
		    }
		}
		#endregion
		
		#region MultipartStreamProvider
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System.Collections.ObjectModel;
		    using System.IO;
		    using System.Net.Http;
		    using System.Net.Http.Headers;
		    using System.Threading;
		    using System.Threading.Tasks;
		    
		    public abstract class MultipartStreamProvider
		    {
		        private struct AsyncVoid{}
		        
		        private readonly Collection<HttpContent> _contents = new Collection<HttpContent>();
		
		        public Collection<HttpContent> Contents
		        {
		            get { return _contents; }
		        }
		
		        public abstract Stream GetStream(HttpContent parent, HttpContentHeaders headers);
		
		        public virtual Task ExecutePostProcessingAsync()
		        {
		            return Task.FromResult(default(AsyncVoid));
		        }
		
		        public virtual Task ExecutePostProcessingAsync(CancellationToken cancellationToken)
		        {
		            // Call the other overload to maintain backward compatibility.
		            // ReSharper disable once MethodSupportsCancellation
		            return ExecutePostProcessingAsync();
		        }
		    }
		}
		#endregion
		
		#region TaskHelperExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Multipart
		{
		    using System.Threading.Tasks;
		    
		    public static class TaskHelperExtensions
		    {
		        public static async Task<object> CastToObject(this Task task)
		        {
		            await task;
		            return null;
		        }
		
		        public static async Task<object> CastToObject<T>(this Task<T> task)
		        {
		            return (object)await task;
		        }
		
		        public static void ThrowIfFaulted(this Task task)
		        {
		            task.GetAwaiter().GetResult();
		        }
		
		        public static bool TryGetResult<ResultT>(this Task<ResultT> task, out ResultT result)
		        {
		            if (task.Status == TaskStatus.RanToCompletion)
		            {
		                result = task.Result;
		                return true;
		            }
		
		            result = default(ResultT);
		            return false;
		        }
		    }
		}
		#endregion
		
	#endregion
	
	#region ReflectionMapper
		
		#region Attributes
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System;
		    using System.Linq;
		    using System.Text;
		
		    [AttributeUsage(AttributeTargets.Property)]
		    public class NotRComparedAttribute : Attribute { }
		
		    [AttributeUsage(AttributeTargets.Property)]
		    public class NotRMappedAttribute : Attribute { }
		
		    [AttributeUsage(AttributeTargets.Property)]
		    public class NotRMappedToAttribute : Attribute { }
		
		    [AttributeUsage(AttributeTargets.Property)]
		    public class NotRMappedFromAttribute : Attribute { }
		    
		    [AttributeUsage(AttributeTargets.Property)]
		    public class RMapToAttribute : Attribute
		    {
		        #region Constructors
		        public RMapToAttribute() { }
		        public RMapToAttribute(string fullPath)
		        {
		            FullPath = fullPath;
		        }
		        #endregion
		
		        #region Properties
		        public string FullPath
		        {
		            set
		            {
		                if (!value.StartsWith(".")) throw new ReflectionMapperException("Path must always start with a '.'");
		                var pathParts = value.Split('.');
		                var sb = new StringBuilder();
		                for (var i = 1; i < pathParts.Length - 1; i++)
		                {
		                    if (i == 1) sb.Append($"{pathParts[i]}");
		                    else sb.Append($".{pathParts[i]}");
		                }
		                ObjectPath = sb.ToString();
		                PropertyName = value.EndsWith("*") ? null : pathParts.Last();
		            }
		        }
		        public string ObjectPath { get; protected set; }
		        public string PropertyName { get; protected set; }
		        #endregion
		    }    
		}
		#endregion
		
		#region Exceptions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System;
		    using System.Collections.Generic;
		    using System.ComponentModel.DataAnnotations;
		
		    public class ReflectionMapperException : Exception
		    {
		        public ReflectionMapperException() { }
		        public ReflectionMapperException(string msg) : base(msg) { }
		    }
		
		    public class ReflectionPropertyCantBeInvoked : ReflectionMapperException
		    {
		        public ReflectionPropertyCantBeInvoked(Type type, string propertyName)
		            : base($"Property '{propertyName}' does not exist in Type '{type.Name}'") { }
		    }
		
		    public class ReflectionMethodCantBeInvoked : ReflectionMapperException
		    {
		        public ReflectionMethodCantBeInvoked(Type type, string methodName)
		            : base($"Method '{methodName}' does not exist in Type '{type.Name}'") { }
		    }    
		    
		    public class PropertyCantBeAutomappedException : ReflectionMapperException
		    {
		        public PropertyCantBeAutomappedException(string msg) : base(msg) { }
		    }
		
		    public class ValidationResultException : ReflectionMapperException
		    {
		        public ValidationResultException(string errorMessage, IEnumerable<string> memberNames)
		        {
		            ValidationResultList = new ValidationResultList { new ValidationResult(errorMessage, memberNames) };
		        }
		
		        public ValidationResultException(ValidationResultList validationResultList)
		        {
		            ValidationResultList = validationResultList;
		        }
		
		        public ValidationResultException(string errorMessage) : this(errorMessage, new List<string> {" "}) { }
		                
		        public ValidationResultList ValidationResultList { get; protected set; }
		    }
		}
		#endregion
		
		#region IRMapperCustom
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System;
		
		    public interface IRMapperCustom
		    {
		        //must also have a default constructor!
		        object MapFromObjectCustom(object obj, Type objType);
		        object MapToObjectCustom(object obj, Type objType);    
		    }
		}
		#endregion
		
		#region ReflectionHelper
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System;
		    using System.ComponentModel;
		    using System.Diagnostics;
		    using System.Linq;
		    using System.Reflection;
		    using System.Text.RegularExpressions;
		
		    public static class ReflectionHelper
		    {
		        #region Methods
		        public static string GetThrowingContext()
		        {
		            #if WINDOWS_UWP
		            var stackFrame = new StackTrace(new Exception(), false).GetFrames()[2];
		            #else
		            var stackFrame = new StackTrace().GetFrame(2);
		            #endif
		            return stackFrame.GetMethod().DeclaringType + "::" + stackFrame.GetMethod().Name + "()";
		        }
		        public static string GetCurrentContext()
		        {
		            #if WINDOWS_UWP
		            var stackFrame = new StackTrace(new Exception(), false).GetFrames()[1];
		            #else
		            var stackFrame = new StackTrace().GetFrame(1);
		            #endif
		            return stackFrame.GetMethod().DeclaringType + "::" + stackFrame.GetMethod().Name + "()";
		        }
		        public static object CreateType(Type type, params object[] args)
		        {
		            //if (type.IsGenericType)
		            if (type.GetTypeInfo().IsGenericType)
		            {
		                return CreateGenericType(type.GetGenericTypeDefinition(), type.GetGenericArguments());
		            }
		            else
		            {
		                return Activator.CreateInstance(type, args);
		            }
		        }
		        public static object CreateGenericType(Type genericType, Type[] innerTypes, params object[] args)
		        {
		            var specificType = genericType.MakeGenericType(innerTypes);
		            return Activator.CreateInstance(specificType, args);
		        }
		        public static object CreateGenericType(Type genericType, Type innerType, params object[] args)
		        {
		            return CreateGenericType(genericType, new[] {innerType}, args);
		        }
		        public static object ExecuteStaticMethod(Type typeofClassWithStaticMethod, string methodName, params object[] args)
		        {
		            var methodInfo = typeofClassWithStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.Public).Single(m => m.Name == methodName && !m.IsGenericMethod && m.GetParameters().Length == args.Length);
		            return methodInfo.Invoke(null, args);
		        }
		        
		        public static object ExecuteNonPublicStaticMethod(Type typeofClassWithStaticMethod, string methodName, params object[] args)
		        {
		            var methodInfo = typeofClassWithStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.NonPublic).Single(m => m.Name == methodName && !m.IsGenericMethod && m.GetParameters().Length == args.Length);
		            return methodInfo.Invoke(null, args);
		        }
		        
		        public static object ExecuteStaticGenericMethod(Type typeofClassWithGenericStaticMethod, string methodName, Type[] genericArguments, params object[] args)
		        {
		            var methodInfo = typeofClassWithGenericStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.Public).Single(m => m.Name == methodName && m.IsGenericMethod&& m.GetParameters().Length == args.Length);
		            var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
		            return genericMethodInfo.Invoke(null, args);
		        }
		
		        public static object ExecuteNonPublicStaticGenericMethod(Type typeofClassWithGenericStaticMethod, string methodName, Type[] genericArguments, params object[] args)
		        {
		            var methodInfo = typeofClassWithGenericStaticMethod.GetMethods(BindingFlags.Static | BindingFlags.NonPublic).Single(m => m.Name == methodName && m.IsGenericMethod&& m.GetParameters().Length == args.Length);
		            var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
		            return genericMethodInfo.Invoke(null, args);
		        }
		
		        public static bool IsClassADerivedFromClassB(Type a, Type b)
		        {
		            return IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(a, b) != null;
		        }
		        public static Type IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(Type a, Type b)
		        {
		            if (a == b) return a;
		
		            //var aBaseType = a.BaseType;
		            var aBaseType = a.GetTypeInfo().BaseType;
		
		            if (aBaseType == null) return null;
		
		            //if (b.IsGenericTypeDefinition && aBaseType.IsGenericType)
		            if (b.GetTypeInfo().IsGenericTypeDefinition && aBaseType.GetTypeInfo().IsGenericType)
		            {
		                if (aBaseType.GetGenericTypeDefinition() == b) return aBaseType;
		            }
		            else
		            {
		                if (aBaseType == b) return aBaseType;
		            }
		            return IfClassADerivedFromClassBGetFullGenericBaseTypeOfB(aBaseType, b);
		        }
		        #endregion
		
		        #region Extnesions for Fluent interface
		        public static string InsertSpacesBetweenWords(this string str)
		        {
		            var result = Regex.Replace(str, @"(\B[A-Z][^A-Z]+)|\B(?<=[^A-Z]+)([A-Z]+)(?![^A-Z])", " $1$2");
		            return result
		                .Replace(" Or ", " or ")
		                .Replace(" And ", " and ")
		                .Replace(" Of ", " of ")
		                .Replace(" On ", " on ")
		                .Replace(" The ", " the ")
		                .Replace(" For ", " for ")
		                .Replace(" At ", " at ")
		                .Replace(" A ", " a ")
		                .Replace(" In ", " in ")
		                .Replace(" By ", " by ")
		                .Replace(" About ", " about ")
		                .Replace(" To ", " to ")
		                .Replace(" From ", " from ")
		                .Replace(" With ", " with ")
		                .Replace(" Over ", " over ")
		                .Replace(" Into ", " into ")
		                .Replace(" Without ", " without ");
		        }
		
		        public static string GetTypeDescription(this Type type)
		        {
		            //var attr = MyAttribute.GetCustomAttribute(type, typeof(DescriptionAttribute), true);
		            var attr = type.GetTypeInfo().GetCustomAttribute(typeof(DescriptionAttribute), true);
		            return attr != null ? ((DescriptionAttribute)attr).Description : type.ToString().InsertSpacesBetweenWords();
		        }
		        public static string GetTypeFriendlyDescription(this Type type)
		        {
		            //var attr = MyAttribute.GetCustomAttribute(type, typeof(DescriptionAttribute), true);
		            var attr = type.GetTypeInfo().GetCustomAttribute(typeof(DescriptionAttribute), true);
		            return attr != null ? ((DescriptionAttribute)attr).Description : type.Name.InsertSpacesBetweenWords();
		        }
		        public static string GetDisplayNameForProperty(this Type type, string propertyName)
		        {
		            var propertyInfo = type.GetProperty(propertyName);
		            var attr = propertyInfo.GetCustomAttribute(typeof (DisplayNameAttribute), true);
		            return attr != null ? ((DisplayNameAttribute)attr).DisplayName : propertyName.InsertSpacesBetweenWords();
		        }
		        public static object ExecuteGenericMethod(this object me, string methodName, Type[] genericArguments, params object[] args)
		        {
		            try
		            {
		                var methodInfo = me.GetType().GetMethods().Single(x => x.Name == methodName && x.IsGenericMethod);
		                var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
		                return genericMethodInfo.Invoke(me, args);
		            }
		            catch (Exception)
		            {
		                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
		            }
		        }
		        public static object ExecuteNonPublicGenericMethod(this object me, string methodName, Type[] genericArguments, params object[] args)
		        {
		            try
		            {
		                var methodInfo = me.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Single(x => x.Name == methodName && x.IsGenericMethod);
		                var genericMethodInfo = methodInfo.MakeGenericMethod(genericArguments);
		                return genericMethodInfo.Invoke(me, args);
		            }
		            catch (Exception)
		            {
		                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
		            }
		        }
		        public static object ExecuteMethod(this object me, string methodName, params object[] args)
		        {
		            var method = me.GetType().GetMethods().SingleOrDefault(x => x.Name == methodName);
		            if (method == null)
		            {
		                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
		            }
		            return method.Invoke(me, args);
		        }
		        public static object ExecuteNonPublicMethod(this object me, string methodName, params object[] args)
		        {
		            try
		            {
		                return me.GetType().GetMethods(BindingFlags.NonPublic | BindingFlags.Instance).Single(x => x.Name == methodName).Invoke(me, args);
		            }
		            catch (Exception)
		            {
		                throw new ReflectionMethodCantBeInvoked(me.GetType(), methodName);
		            }
		        }
		
		        public static object PropertyGet(this object me, string propertyName, object[] index = null)
		        {
		            return me.GetPropertyInfo(propertyName).GetValue(me, index);
		        }
		        public static object PropertyGetNonPublic(this object me, string propertyName, object[] index = null)
		        {
		            return me.GetPropertyInfoNonPublic(propertyName).GetValue(me, index);
		        }
		
		        public static void PropertySet(this object me, string propertyName, object newValue, bool ignoreNoSetMethod = false, object[] index = null)
		        {
		            var myProperty = me.GetPropertyInfo(propertyName);
		            var setMethod = myProperty.GetSetMethod(true);
		            if (setMethod != null)
		            {
		                setMethod.Invoke(me, new[] { newValue });
		            }
		            else
		            {
		                if (!ignoreNoSetMethod) myProperty.SetValue(me, newValue, index);
		            }
		        }
		        public static void PropertySetNonPublic(this object me, string propertyName, object newValue, bool ignoreNoSetMethod = false, object[] index = null)
		        {
		            var myProperty = me.GetPropertyInfoNonPublic(propertyName);
		            if (ignoreNoSetMethod)
		            {
		                if (myProperty.GetSetMethod() != null) myProperty.SetValue(me, newValue, index);
		            }
		            else
		            {
		                myProperty.SetValue(me, newValue, index);
		            }
		        }
		
		        //public static object StaticPropertyGet(this Type myType, string propertyName, object[] index = null)
		        //{
		        //    return myType.GetStaticPropertyInfoWithType(propertyName).GetValue(null, index);
		        //}
		        //public static void StaticPropertySet(this Type myType, string propertyName, object newValue, bool ignoreNoSetMethod = false, object[] index = null)
		        //{
		        //    var myProperty = myType.GetStaticPropertyInfoWithType(propertyName);
		        //    var setMethod = myProperty.GetSetMethod(true);
		        //    if (setMethod != null)
		        //    {
		        //        setMethod.Invoke(null, new[] { newValue });
		        //    }
		        //    else
		        //    {
		        //        if (!ignoreNoSetMethod) myProperty.SetValue(null, newValue, index);
		        //    }
		        //}
		
		        #endregion
		
		        #region Private Helpers
		        private static PropertyInfo GetPropertyInfoNonPublic(this object me, string propertyName)
		        {
		            return me.GetType().GetPropertyInfoNonPublicWithType(propertyName);
		        }
		        //private static PropertyInfo GetStaticPropertyInfoNonPublic(this object me, string propertyName)
		        //{
		        //    return me.GetType().GetStaticPropertyInfoNonPublicWithType(propertyName);
		        //}
		        private static PropertyInfo GetPropertyInfoNonPublicWithType(this Type myType, string propertyName)
		        {
		            try
		            {
		                return myType.GetProperties(BindingFlags.NonPublic | BindingFlags.Instance).Single(p => p.Name == propertyName);
		            }
		            catch (Exception)
		            {
		                throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
		            }
		        }
		        //private static PropertyInfo GetStaticPropertyInfoNonPublicWithType(this Type myType, string propertyName)
		        //{
		        //    try
		        //    {
		        //        return myType.GetProperties(BindingFlags.NonPublic | BindingFlags.Static).Single(p => p.Name == propertyName);
		        //    }
		        //    catch (Exception)
		        //    {
		        //        throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
		        //    }
		        //}
		
		
		        private static PropertyInfo GetPropertyInfo(this object me, string propertyName)
		        {
		            return me.GetType().GetPropertyInfoWithType(propertyName);
		        }
		        //private static PropertyInfo GetStaticPropertyInfo(this object me, string propertyName)
		        //{
		        //    return me.GetType().GetStaticPropertyInfoWithType(propertyName);
		        //}
		        private static PropertyInfo GetPropertyInfoWithType(this Type myType, string propertyName)
		        {
		            try
		            {
		                return myType.GetProperty(propertyName);
		            }
		            catch(Exception)
		            {
		                throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
		            }
		        }
		        //private static PropertyInfo GetStaticPropertyInfoWithType(this Type myType, string propertyName)
		        //{
		        //    try
		        //    {
		        //        //return myType.GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy).Single(p => p.Name == propertyName);
		        //        var x = myType.GetProperties(BindingFlags.Public | BindingFlags.Static);
		        //        return myType.GetProperties().Single(p => p.Name == propertyName);
		        //    }
		        //    catch (Exception ex)
		        //    {
		        //        throw new ReflectionPropertyCantBeInvoked(myType, propertyName);
		        //    }
		        //}
		        #endregion
		    }
		}
		#endregion
		
		#region ReflectionMapperExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Linq;
		    using System.Reflection;
		    using System.Collections;
		    using System.ComponentModel.DataAnnotations;
		
		    public static class ReflectionMapperExtensions
		    {
		        public static MeT MapFrom<MeT, ObjT>(this MeT me, ObjT obj)
		        {
		            return (MeT)me.MapFromObject(obj);
		        }
		        public static ObjT MapTo<MeT, ObjT>(this MeT me, ObjT obj)
		        {
		            return (ObjT)me.MapToObject(obj);
		        }
		        public static object MapFromObject(this object me, object obj)
		        {
		            //first check if me implements IRMapperCustomMapper, if it does we use that. If custom mapper does not exist, we go property by property by calling the base method
		            if (me is IRMapperCustom) me = (me as IRMapperCustom).MapFromObjectCustom(obj, obj.GetType());
		            else me = me.MapFromObjectCustomBase(obj);
		            return me;
		        }
		        public static object MapToObject(this object me, object obj)
		        {
		            //first check if me implements ICustomMapper, if it does we use that. If custom mapper does not exist, we go property by property by calling the base method
		            if (me is IRMapperCustom) obj = (me as IRMapperCustom).MapToObjectCustom(obj, obj.GetType());
		            else obj = me.MapToObjectCustomBase(obj);
		            return obj;
		        }
		
		        public static object MapFromObjectCustomBase(this object me, object obj)
		        {
		            //if obj == null, we set me to null too, the caller must assign the result of this method
		            if (obj == null) return null;
		
		            //if me is null, report error
		            if (me == null) throw new ReflectionMapperException("MapFromObjectCustomBase(): me is null");
		
		            //ICollection<ICustomeMapper>
		            if (AreCompatibleCollections(me.GetType(), obj.GetType()))
		            {
		                var objIEnumerable = (IEnumerable)obj;
		
		                var myIEnumerableGenericArg = me.GetType().GetInterfaces().Single(x => x.Name == typeof(IEnumerable<>).Name).GetGenericArguments()[0];
		
		                foreach (var objItemObj in objIEnumerable)
		                {
		                    me.ExecuteMethod("Add", objItemObj != null ? ReflectionHelper.CreateType(myIEnumerableGenericArg).ExecuteMethod("MapFromObjectCustom", objItemObj, objItemObj.GetType()) : null);
		                }
		                return me;
		            }
		
		            foreach (var property in me.GetType().GetProperties())
		            {
		                //If property is marked NotRMappedAttribute, don't worry about this one
		                //if (MyAttribute.GetCustomAttribute(property, typeof(NotRMappedAttribute), true) != null || MyAttribute.GetCustomAttribute(property, typeof(NotRMappedFromAttribute), true) != null) continue;
		                if (property.GetCustomAttribute(typeof(NotRMappedAttribute), true) != null || property.GetCustomAttribute(typeof(NotRMappedFromAttribute), true) != null) continue;
		
		                //Find matching properties
		                var myProperty = property;
		                var objPropertyMeta = GetMatchingProperty(me, myProperty, obj);
		
		                //ICustomMapper
		                if (typeof(IRMapperCustom).IsAssignableFrom(myProperty.PropertyType))
		                {
		                    try
		                    {
		                        //Get the property we are mapping to
		                        var objPropertyObj = objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name);
		
		                        var myPropertyObj = me.PropertyGet(myProperty.Name);
		                        if (myPropertyObj == null)
		                        {
		                            //we create blank object for the property in question
		                            myPropertyObj = !myProperty.PropertyType.GetTypeInfo().IsGenericType ? ReflectionHelper.CreateType(myProperty.PropertyType) : ReflectionHelper.CreateGenericType(myProperty.PropertyType.GetGenericTypeDefinition(), myProperty.PropertyType.GetGenericArguments());
		                        }
		
		                        //then we ask that property to map itself to the matching property of the object
		                        // ReSharper disable PossibleNullReferenceException
		                        (myPropertyObj as IRMapperCustom).MapFromObjectCustom(objPropertyObj, objPropertyMeta.PropertyInfo.PropertyType);
		                        // ReSharper restore PossibleNullReferenceException
		
		                        me.PropertySet(myProperty.Name, myPropertyObj);
		                    }
		                    catch (ValidationResultException ex)
		                    {
		                        var vr = new ValidationResultList();
		                        foreach (var validationResult in ex.ValidationResultList)
		                        {
		                            if (validationResult.MemberNames.Any(x => !string.IsNullOrWhiteSpace(x))) vr.Add(validationResult);
		                            else vr.Add(new ValidationResult(validationResult.ErrorMessage, new[] { property.Name }));
		                        }
		                        throw new ValidationResultException(vr);
		                    }
		                    catch (Exception ex)
		                    {
		                        throw new PropertyCantBeAutomappedException($"Property '{myProperty.Name}' of class '{me.GetType().Name}' can't be automapped to type '{obj.GetType().Name}' property '{objPropertyMeta.PropertyInfo.Name}' because ICustomMapper implementation threw an exception: {ex.Message}.");
		                    }
		                }
		
		                //ICollection<ICustomMapper>
		                else if (AreCompatibleCollections(myProperty.PropertyType, objPropertyMeta.PropertyInfo.PropertyType))
		                {
		                    var objIEnumerable = (IEnumerable)objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name);
		
		                    var myIEnumerableGenericArg = myProperty.PropertyType.GetTypeInfo().ImplementedInterfaces.Single(x => x.GetTypeInfo().IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)).GetGenericArguments()[0];
		
		                    if (objIEnumerable != null)
		                    {
		                        if (me.PropertyGet(myProperty.Name) == null)
		                        {
		                            me.PropertySet(myProperty.Name, ReflectionHelper.CreateGenericType(objIEnumerable.GetType().GetGenericTypeDefinition(), myIEnumerableGenericArg));
		                        }
		                        else
		                        {
		                            me.PropertyGet(myProperty.Name).ExecuteMethod("Clear");
		                        }
		
		                        foreach (var objItemObj in objIEnumerable)
		                        {
		                            me.PropertyGet(myProperty.Name).ExecuteMethod("Add", objItemObj != null ? ReflectionHelper.CreateType(myIEnumerableGenericArg).ExecuteMethod("MapFromObjectCustom", objItemObj, objItemObj.GetType()) : null);
		                        }
		                    }
		                }
		
		                //This is the default case of when the types match exactly
		                else if (myProperty.PropertyType == objPropertyMeta.PropertyInfo.PropertyType)
		                {
		                    me.PropertySet(myProperty.Name, objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name), true);
		                }
		
		                //If all fails
		                else
		                {
		                    throw new PropertyCantBeAutomappedException($"Property '{myProperty.Name}' of class '{me.GetType().Name}' can't be automapped to type '{obj.GetType().Name}' property '{objPropertyMeta.PropertyInfo.Name}' because their types are incompatible.");
		                }
		            }
		            return me;
		        }
		        public static object MapToObjectCustomBase(this object me, object obj)
		        {
		            //if me == null, we set obj to null too, the caller must assign the result of this method
		            if (me == null) return null;
		
		            //if me is null, report error
		            if (obj == null) throw new ReflectionMapperException("MapToObjectCustomBase(): obj is null");
		
		            //ICollection<ICustomeMapper>
		            if (AreCompatibleCollections(me.GetType(), obj.GetType()))
		            {
		                var myICollection = (ICollection)me;
		
		                var objICollectionGenericArg = obj.GetType().GetTypeInfo().ImplementedInterfaces.Single(x => x.GetTypeInfo().IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)).GetGenericArguments()[0];
		
		                foreach (var myItemObj in myICollection)
		                {
		                    obj.ExecuteMethod("Add", myItemObj != null ? myItemObj.ExecuteMethod("MapToObjectCustom", ReflectionHelper.CreateType(objICollectionGenericArg), objICollectionGenericArg) : null);
		                }
		                return obj;
		            }
		
		            foreach (var property in me.GetType().GetProperties())
		            {
		                //If property is marked NotRMappedAttribute, don't worry about this one
		                //if (MyAttribute.GetCustomAttribute(property, typeof(NotRMappedAttribute), true) != null || MyAttribute.GetCustomAttribute(property, typeof(NotRMappedToAttribute), true) != null) continue;
		                if (property.GetCustomAttribute(typeof(NotRMappedAttribute), true) != null || property.GetCustomAttribute(typeof(NotRMappedToAttribute), true) != null) continue;
		
		                //Find matching properties
		                var myProperty = property;
		                var objPropertyMeta = GetMatchingProperty(me, myProperty, obj);
		
		                //ICustomMapper
		                if (typeof(IRMapperCustom).IsAssignableFrom(myProperty.PropertyType))
		                {
		                    try
		                    {
		                        // ReSharper disable PossibleNullReferenceException
		                        var propertyValue = (me.PropertyGet(myProperty.Name) as IRMapperCustom).MapToObjectCustom(objPropertyMeta.Obj.PropertyGet(objPropertyMeta.PropertyInfo.Name), objPropertyMeta.PropertyInfo.PropertyType);
		                        // ReSharper restore PossibleNullReferenceException
		                        objPropertyMeta.Obj.PropertySet(objPropertyMeta.PropertyInfo.Name, propertyValue);
		                    }
		                    catch (ValidationResultException ex)
		                    {
		                        var vr = new ValidationResultList();
		                        foreach (var validationResult in ex.ValidationResultList)
		                        {
		                            if (validationResult.MemberNames.Any(x => !string.IsNullOrWhiteSpace(x))) vr.Add(validationResult);
		                            else vr.Add(new ValidationResult(validationResult.ErrorMessage, new[] { property.Name }));
		                        }
		                        throw new ValidationResultException(vr);
		                    }
		                    catch (Exception ex)
		                    {
		                        throw new PropertyCantBeAutomappedException($"Property '{myProperty.Name}' of class '{me.GetType().Name}' can't be automapped to type '{obj.GetType().Name}' property '{objPropertyMeta.PropertyInfo.Name}' because ICsutomMapper threw an exception: {ex.Message}.");
		                    }
		                }
		
		                //IEnumerable<ICustomMapper>
		                else if (AreCompatibleCollections(myProperty.PropertyType, objPropertyMeta.PropertyInfo.PropertyType))
		                {
		                    var myIEnumerable = (IEnumerable)me.PropertyGet(myProperty.Name);
		                    var objIEnumerableGenericArg = objPropertyMeta.PropertyInfo.PropertyType.GetTypeInfo().ImplementedInterfaces.Single(x => x.GetTypeInfo().IsGenericType && x.GetGenericTypeDefinition() == typeof(IEnumerable<>)).GetGenericArguments()[0];
		
		
		                    if (myIEnumerable != null)
		                    {
		                        if (objPropertyMeta.Obj.PropertyGet(myProperty.Name) == null)
		                        {
		                            objPropertyMeta.Obj.PropertySet(objPropertyMeta.PropertyInfo.Name, ReflectionHelper.CreateGenericType(myIEnumerable.GetType().GetGenericTypeDefinition(), objIEnumerableGenericArg));
		                        }
		                        else
		                        {
		                            objPropertyMeta.Obj.PropertyGet(myProperty.Name).ExecuteMethod("Clear");
		                        }
		
		                        foreach (var myItemObj in myIEnumerable)
		                        {
		                            objPropertyMeta.Obj.PropertyGet(myProperty.Name).ExecuteMethod("Add", (myItemObj != null) ? myItemObj.ExecuteMethod("MapToObjectCustom", ReflectionHelper.CreateType(objIEnumerableGenericArg), objIEnumerableGenericArg) : null);
		                        }
		                    }
		                }
		
		                //This is the default case of when the types match exactly
		                else if (myProperty.PropertyType == objPropertyMeta.PropertyInfo.PropertyType)
		                {
		                    objPropertyMeta.Obj.PropertySet(objPropertyMeta.PropertyInfo.Name, me.PropertyGet(myProperty.Name), true);
		                }
		
		                //If all fails
		                else
		                {
		                    throw new PropertyCantBeAutomappedException($"Property '{myProperty.Name}' of class '{me.GetType().Name}' can't be automapped to type '{obj.GetType().Name}' property '{objPropertyMeta.PropertyInfo.Name}' because their types are incompatible.");
		                }
		            }
		
		            return obj;
		        }
		
		        public static bool IsEqualToObject(this object me, object obj)
		        {
		            //Handle nulls
		            if (me == null && obj == null) return true;
		            if (me == null || obj == null) return false;
		
		            //We only compare identical types or when obj is derived from me (this is needed for EF support)
		            if (!ReflectionHelper.IsClassADerivedFromClassB(obj.GetType(), me.GetType())) throw new ArgumentException($"Cannot compare incompatible types {me.GetType().Name} and {obj.GetType().Name}");
		
		            //IEnumerables
		            // ReSharper disable CanBeReplacedWithTryCastAndCheckForNull
		            if (me is IEnumerable)
		                // ReSharper restore CanBeReplacedWithTryCastAndCheckForNull
		            {
		                var objIEnumerable = (IEnumerable)obj;
		                var myIEnumerable = (IEnumerable)me;
		
		                var objIEnumerableEnumerator = objIEnumerable.GetEnumerator();
		                var myIEnumerableEnumerator = myIEnumerable.GetEnumerator();
		
		                while (true)
		                {
		                    var objEnumeratorDone = !objIEnumerableEnumerator.MoveNext();
		                    var myEnumeratorDone = !myIEnumerableEnumerator.MoveNext();
		
		                    //if both are done and we have not found a difference, return true
		                    if (objEnumeratorDone && myEnumeratorDone) return true;
		
		                    //If both are not done but one is done, enumerators are of diffrenet length, return false
		                    if (objEnumeratorDone || myEnumeratorDone) return false;
		
		                    var objItem = objIEnumerableEnumerator.Current;
		                    var myItem = myIEnumerableEnumerator.Current;
		
		                    if (!myItem.IsEqualToObject(objItem)) return false;
		                }
		            }
		
		            //Go through all properties that are both readable and writable
		            foreach (var property in me.GetType().GetProperties().Where(x => x.CanRead && x.CanWrite))
		            {
		                //If property is marked NotRComparedAttribute, don't worry about this one
		                if (property.GetCustomAttribute(typeof(NotRComparedAttribute), true) != null) continue;
		
		                //Find matching properties
		                var myProperty = property;
		                var objProperty = obj.GetType().GetProperty(myProperty.Name);
		
		                //Get the property values
		                var objPropertyObj = obj.PropertyGet(objProperty.Name);
		                var myPropertyObj = me.PropertyGet(myProperty.Name);
		
		                //Handle nulls
		                if (myPropertyObj == null)
		                {
		                    if (objPropertyObj == null) continue;
		                    else return false;
		                }
		
		                //IComparer
		                var myPropertyObjAsComparable = myPropertyObj as IComparable;
		                if (myPropertyObjAsComparable != null)
		                {
		                    try
		                    {
		                        if (myPropertyObjAsComparable.CompareTo(objPropertyObj) != 0) return false;
		                    }
		                    catch (Exception ex)
		                    {
		                        throw new ArgumentException($"Property '{myProperty.Name}' of type '{me.GetType().Name}' can't be compared to type '{obj.GetType().Name}' property '{objProperty.Name}' because IComparable implementation threw an exception: {ex.Message}.");
		                    }
		                }
		
		                //Everything else
		                else
		                {
		                    try
		                    {
		                        if (!myPropertyObj.IsEqualToObject(objPropertyObj)) return false;
		                    }
		                    catch (Exception ex)
		                    {
		                        throw new ArgumentException($"Property '{myProperty.Name}' of type '{me.GetType().Name}' can't be compared to type '{obj.GetType().Name}' property '{objProperty.Name}' because IsEqualToObject() method threw an exception: {ex.Message}.");
		                    }
		                }
		            }
		            return true;
		        }
		
		        #region Private Helper Methods
		        private static bool AreCompatibleCollections(Type activeType, Type passiveType)
		        {
		            //both types are generic
		            //if (!activeType.IsGenericType) return false;
		            if (!activeType.GetTypeInfo().IsGenericType) return false;
		            //if (!passiveType.IsGenericType) return false;
		            if (!passiveType.GetTypeInfo().IsGenericType) return false;
		
		            //get generic type defintions
		            var activeTypeGenericDefinition = activeType.GetGenericTypeDefinition();
		            var passiveTypeGenericDefinition = passiveType.GetGenericTypeDefinition();
		
		            //make sure the types are the same except for the generic parameter
		            if (activeTypeGenericDefinition != passiveTypeGenericDefinition) return false;
		
		            //set up ICollection inetrafce in a variable
		            //var activeICollectionInterface = activeType.GetInterface(typeof(ICollection<>).Name);
		            var activeICollectionInterface = activeType.GetTypeInfo().ImplementedInterfaces.SingleOrDefault(x => x.GetTypeInfo().IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>));
		            if (activeICollectionInterface == null && activeTypeGenericDefinition == typeof(ICollection<>)) activeICollectionInterface = activeType;
		
		            //var passiveICollectionInterface = passiveType.GetInterface(typeof(ICollection<>).Name);
		            var passiveICollectionInterface = passiveType.GetTypeInfo().ImplementedInterfaces.SingleOrDefault(x => x.GetTypeInfo().IsGenericType && x.GetGenericTypeDefinition() == typeof(ICollection<>));
		            if (passiveICollectionInterface == null && passiveTypeGenericDefinition == typeof(ICollection<>)) passiveICollectionInterface = passiveType;
		
		            //both types are ICollection
		            if (activeICollectionInterface == null) return false;
		            if (passiveICollectionInterface == null) return false;
		
		            //both are generic types (this might be generic but just in case)
		            //if (!activeICollectionInterface.IsGenericType) return false;
		            if (!activeICollectionInterface.GetTypeInfo().IsGenericType) return false;
		            //if (!passiveICollectionInterface.IsGenericType) return false;
		            if (!passiveICollectionInterface.GetTypeInfo().IsGenericType) return false;
		
		            //my active generic ICollection<> type argument implements ICustomMapper
		            if (!typeof(IRMapperCustom).IsAssignableFrom(activeICollectionInterface.GetGenericArguments()[0])) return false;
		
		            return true;
		        }
		        private static ReflectionMapperPropertyMetadata GetMatchingProperty(object me, PropertyInfo myProperty, object obj)
		        {
		            // ReSharper disable PossibleMultipleEnumeration
		            // ReSharper disable AccessToForEachVariableInClosure
		
		            var myPropertyName = myProperty.Name;
		            var parentObj = obj;
		
		            //var myReflectionMappedToAttribute = MyAttribute.GetCustomAttribute(myProperty, typeof(RMapToAttribute), true);
		            var myReflectionMappedToAttribute = myProperty.GetCustomAttribute(typeof(RMapToAttribute), true);
		            if (myReflectionMappedToAttribute != null)
		            {
		                var attrPropertyName = ((RMapToAttribute)myReflectionMappedToAttribute).PropertyName;
		                if (!string.IsNullOrEmpty(attrPropertyName)) myPropertyName = attrPropertyName;
		
		                var attrObjectPath = ((RMapToAttribute)myReflectionMappedToAttribute).ObjectPath;
		                if (!string.IsNullOrEmpty(attrObjectPath))
		                {
		                    var mappedToNameComponents = attrObjectPath.Split('.');
		                    if (mappedToNameComponents.Length < 1) throw new ReflectionMapperException("Invalid path in ReflectionMappedTo Attribute");
		
		                    foreach (var subPropertyName in mappedToNameComponents)
		                    {
		                        var subObjProperties = parentObj.GetType().GetProperties().Where(x => x.Name == subPropertyName);
		                        if (subObjProperties.Count() != 1) throw new ReflectionMapperException("Invalid path in ReflectionMappedTo Attribute");
		                        parentObj = parentObj.PropertyGet(subObjProperties.Single().Name);
		                        if (parentObj == null) throw new ReflectionMapperException($"{subPropertyName} in ReflectionMappedTo Attribute path is null");
		                    }
		                }
		            }
		
		            var objProperties = parentObj.GetType().GetProperties().Where(x => x.Name == myPropertyName);
		            if (objProperties.Count() != 1) throw new PropertyCantBeAutomappedException(string.Format("Property '{0}' of class '{1}' can't be automapped to type '{2}' because '{3}' property does not exist in type '{2}'.", myProperty.Name, me.GetType().Name, parentObj.GetType().Name, myPropertyName));
		            var propertyInfo = objProperties.Single();
		
		            return new ReflectionMapperPropertyMetadata { Obj = parentObj, PropertyInfo = propertyInfo };
		
		            // ReSharper restore AccessToForEachVariableInClosure
		            // ReSharper restore PossibleMultipleEnumeration
		        }
		        #endregion
		    }
		}
		#endregion
		
		#region ReflectionMapperPropertyMetadata
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System.Reflection;
		    
		    public class ReflectionMapperPropertyMetadata
		    {
		        public PropertyInfo PropertyInfo { get; set; }
		        public object Obj { get; set; }
		    }
		}
		#endregion
		
		#region ValidationResultList
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.ReflectionMapper
		{
		    using System;
		    using System.Collections.Generic;
		    using System.ComponentModel.DataAnnotations;
		    using System.Linq.Expressions;
		    using System.Reflection;
		    
		    public class ValidationResultList : List<ValidationResult>
		    {
		        public ValidationResult AddValidationResult<ModelT, ValueT>(ModelT model, Expression<Func<ModelT, ValueT>> expression, string message)
		        {
		            var validationResult = CreateValidationResult(model, expression, message);
		            Add(validationResult);
		            return validationResult;
		        }
		        public ValidationResult AddValidationResult<ModelT, Value1T, Value2T>(ModelT model, Expression<Func<ModelT, Value1T>> expression1, Expression<Func<ModelT, Value2T>> expression2, string message)
		        {
		            var validationResult = CreateValidationResult(model, expression1, expression2, message);
		            Add(validationResult);
		            return validationResult;
		        }
		        public ValidationResult AddValidationResult<ModelT, Value1T, Value2T, Value3T>(ModelT model, Expression<Func<ModelT, Value1T>> expression1, Expression<Func<ModelT, Value2T>> expression2, Expression<Func<ModelT, Value3T>> expression3, string message)
		        {
		            var validationResult = CreateValidationResult(model, expression1, expression2, expression2, message);
		            Add(validationResult);
		            return validationResult;
		        }
		        public ValidationResultList AddValidationResultList(ValidationResultList vrl)
		        {
		            foreach (var vr in vrl) Add(vr);
		            return this;
		        }
		
		        public static ValidationResult CreateValidationResult<ModelT, ValueT>(ModelT model, Expression<Func<ModelT, ValueT>> expression, string message)
		        {
		            return new ValidationResult(message, new[] { GetPropertyName(model, expression) });
		        }
		        public static ValidationResult CreateValidationResult<ModelT, Value1T, Value2T>(ModelT model, Expression<Func<ModelT, Value1T>> expression1, Expression<Func<ModelT, Value2T>> expression2, string message)
		        {
		            return new ValidationResult(message, new[] { GetPropertyName(model, expression1), GetPropertyName(model, expression2) });
		        }
		        public static ValidationResult CreateValidationResult<ModelT, Value1T, Value2T, Value3T>(ModelT model, Expression<Func<ModelT, Value1T>> expression1, Expression<Func<ModelT, Value2T>> expression2, Expression<Func<ModelT, Value3T>> expression3, string message)
		        {
		            return new ValidationResult(message, new[] { GetPropertyName(model, expression1), GetPropertyName(model, expression2), GetPropertyName(model, expression3) });
		        }
		
		        public static string GetPropertyName<ModelT, ValueT>(ModelT model, Expression<Func<ModelT, ValueT>> expression)
		        {
		            if (expression.Body.NodeType != ExpressionType.MemberAccess) throw new ReflectionMapperException("Expression must describe a property");
		            var memberExpression = (MemberExpression)expression.Body;
		            var propertyName = memberExpression.Member is PropertyInfo ? memberExpression.Member.Name : null;
		            return GetExpressionName(memberExpression.Expression) + propertyName;
		        }
		
		        public static string GetExpressionName(Expression expression)
		        {
		            if (expression.NodeType == ExpressionType.Parameter) return "";
		
		            if (expression.NodeType == ExpressionType.MemberAccess)
		            {
		                var memberExpression = (MemberExpression)expression;
		                return GetExpressionName(memberExpression.Expression) + memberExpression.Member.Name + ".";
		            }
		
		            throw new Exception("Invalid Expression '" + expression + "'");
		        }
		    }
		}
		#endregion
		
	#endregion
	
	#region Repository
		
		#region DataRepo
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Repository
		{
		    using System.Collections.Generic;
		    using System.Threading.Tasks;
		    using DataContext.Core;
		    using Exceptions;
		    using Models;
		    using UnitOfWork;
		    
		    public class DataRepo<ModelT> : IDataRepo<ModelT> where ModelT : class, IModel, new()
		    {
		        #region Reads
		        public virtual Task<ModelT> GetByIdAsync(long id)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetByIdAsync operation");
		            return ((IReadableDataContext) context).GetByIdAsync<ModelT>(id);
		        }
		        public virtual Task<ModelT> GetByIdOrDefaultAsync(long id)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetByIdOrDefaultAsync operation");
		            return ((IReadableDataContext) context).GetByIdOrDefaultAsync<ModelT>(id);
		        }
		        public virtual Task<List<ModelT>> GetAllAsync(int? skip = null, int? take = null)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetAllAsync operation");
		            return ((IReadableDataContext) context).GetAllAsync<ModelT>(skip, take);
		        }
		        public virtual Task<long> GetCountAllAsync(int? skip = null, int? take = null)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetCountAllAsync operation");
		            return ((IReadableDataContext) context).GetCountAllAsync<ModelT>(skip, take);
		        }
		        #endregion
		
		        #region Batch Reads
		        public virtual void DelayedGetById(out DelayedModel<ModelT> model, long id)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetById operation");
		            ((IReadableDataContext) context).DelayedGetById(out model, id);
		        }
		        public virtual void DelayedGetByIdOrDefault(out DelayedModel<ModelT> model, long id)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetByIdOrDefault operation");
		            ((IReadableDataContext) context).DelayedGetByIdOrDefault(out model, id);
		        }
		        public virtual void DelayedGetAll(out DelayedModels<ModelT> models)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetAll operation");
		            ((IReadableDataContext) context).DelayedGetAll(out models);
		        }
		        public virtual void DelayedGetCountAll(out DelayedCount count)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetCountAll operation");
		            ((IReadableDataContext) context).DelayedGetCountAll<ModelT>(out count);
		        }
		        #endregion
		
		        #region Queries
		        public virtual Task<List<ModelT>> GetWhereAsync(object searchBy, string sortBy = null, int? skip = null, int? take = null)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetWhereAsync operation");
		            return ((IQuerableReadableDataContext) context).GetWhereAsync<ModelT>(searchBy, sortBy, skip, take);
		        }
		        public virtual Task<long> GetCountWhereAsync(object searchBy)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support GetCountWhereAsync operation");
		            return ((IQuerableReadableDataContext) context).GetCountWhereAsync<ModelT>(searchBy);
		        }
		        #endregion
		
		        #region Delayed Queries
		        public virtual void DelayedGetWhere(out DelayedModels<ModelT> models, object searchBy, string sortBy = null, int? skip = null, int? take = null)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetWhere operation");
		            ((IQuerableReadableDataContext) context).DelayedGetWhere(out models, searchBy, sortBy, skip, take);
		        }
		        public virtual void DelayedGetCountWhere(out DelayedCount count, object searchBy)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IQuerableReadableDataContext)) throw new SupermodelException("Curent DataContext does not support DelayedGetCountWhere operation");
		            ((IQuerableReadableDataContext) context).DelayedGetCountWhere<ModelT>(out count, searchBy);
		        }
		        #endregion
		
		        #region Writes
		        public virtual void Add(ModelT model)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IWriteableDataContext)) throw new SupermodelException("Curent DataContext does not support Add operation");
		            ((IWriteableDataContext) context).Add(model);
		        }
		        public virtual void Delete(ModelT model)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IWriteableDataContext)) throw new SupermodelException("Curent DataContext does not support Delete operation");
		            ((IWriteableDataContext) context).Delete(model);
		        }
		        public virtual void ForceUpdate(ModelT model)
		        {
		            var context = UnitOfWorkContextCore.CurrentDataContext;
		            if (!(context is IWriteableDataContext)) throw new SupermodelException("Curent DataContext does not support ForceUpdate operation");
		            ((IWriteableDataContext) context).ForceUpdate(model);
		        }
		        #endregion
		    }
		}
		#endregion
		
		#region IDataRepo
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Repository
		{
		    using System.Collections.Generic;
		    using System.Threading.Tasks;
		    using DataContext.Core;
		    using Models;
		    
		    public interface IDataRepo<ModelT> where ModelT : class, IModel, new()
		    {
		        #region Reads
		        Task<ModelT> GetByIdAsync(long id);
		        Task<ModelT> GetByIdOrDefaultAsync(long id);
		        Task<List<ModelT>> GetAllAsync(int? skip = null, int? take = null);
		        Task<long> GetCountAllAsync(int? skip = null, int? take = null);
		        #endregion
		
		        #region Batch Reads
		        void DelayedGetById(out DelayedModel<ModelT> model, long id);
		        void DelayedGetByIdOrDefault(out DelayedModel<ModelT> model, long id);
		        void DelayedGetAll(out DelayedModels<ModelT> models);
		        void DelayedGetCountAll(out DelayedCount count);
		        #endregion
		
		        #region Queries
		        Task<List<ModelT>> GetWhereAsync(object searchBy, string sortBy = null, int? skip = null, int? take = null);
		        Task<long> GetCountWhereAsync(object searchBy);
		        #endregion
		
		        #region Batch Queries
		        void DelayedGetWhere(out DelayedModels<ModelT> models, object searchBy, string sortBy = null, int? skip = null, int? take = null);
		        void DelayedGetCountWhere(out DelayedCount count, object searchBy);
		        #endregion
		
		        #region Writes
		        void Add(ModelT model);
		        void Delete(ModelT model);
		        void ForceUpdate(ModelT model);
		        #endregion
		    }
		}
		#endregion
		
		#region IRepoFactory
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Repository
		{
		    using Models;
		
		    public interface IRepoFactory
		    {
		        IDataRepo<ModelT> CreateRepo<ModelT>() where ModelT : class, IModel, new();
		    }
		}
		#endregion
		
		#region RepoFactory
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Repository
		{
		    using System;
		    using Models;
		    using ReflectionMapper;
		    using UnitOfWork;
		    
		    public static class RepoFactory
		    {
		        public static IDataRepo<ModelT> Create<ModelT>() where ModelT : class, IModel, new()
		        {
		            return UnitOfWorkContextCore.CurrentDataContext.CreateRepo<ModelT>();
		        }
		        public static object CreateForRuntimeType(Type modelType)
		        {
		            return ReflectionHelper.ExecuteStaticGenericMethod(typeof(RepoFactory), "Create", new[] { modelType });
		        }
		    }
		}
		#endregion
		
	#endregion
	
	#region Services
		
		#region AudioService
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Services
		{
		    #if __IOS__
		    using AVFoundation;
		    using Foundation;
		
		    public class AudioService : IAudioService
		    {
		        public void Play(byte[] wavSound)
		        {
		            if (_player == null || !_player.Playing)
		            {
		                _player = AVAudioPlayer.FromData(NSData.FromArray(wavSound));
		                _player.Play();
		            }
		        }
		
		        //public void StartRecording()
		        //{
		        //    throw new System.NotImplementedException();
		        //    //var recorder = new AVAudioRecorder();
		        //    //recorder.
		        //}
		
		        //public byte[] StopRecording()
		        //{
		        //    throw new System.NotImplementedException();
		        //}
		
		        private static AVAudioPlayer _player;
		    }
		    #endif
		
		    #if __ANDROID__
		    using Services;
		    using Android.Media;
		    using Java.IO;
		    using Android.App;
		    using XForms.App;
		
			public class AudioService : IAudioService
			{
				public void Play(byte[] wavSound)
				{
				    if (_player == null || !_player.IsPlaying)
				    {
				        if (_player != null) _player.Release();
		                var tempWav = File.CreateTempFile("temp", "wav", FormsApplication.MainActivity.CacheDir);
				        tempWav.DeleteOnExit();
				        var fos = new FileOutputStream(tempWav);
				        fos.Write(wavSound);
				        fos.Close();
				                
				        _player = new MediaPlayer();
				        var fis = new FileInputStream(tempWav);
				        _player.SetDataSource(fis.FD);
				
				        _player.Prepare();
				        _player.Start();
				    }        
				}
				
				private static MediaPlayer _player; //AudioTrack
			}
		    #endif
		
		    #if WINDOWS_UWP
		    using System.IO;
		    using Windows.UI.Xaml.Controls;
		
		    public class AudioService : IAudioService
		    {
		        public void Play(byte[] wavSound)
		        {
		            var player = new MediaElement();
		            var stream = new MemoryStream(wavSound);
		            player.SetSource(stream.AsRandomAccessStream(), "wav");
		            player.Play();
		        }
		    }
		    #endif
		}
		#endregion
		
		#region IAudioService
		 // ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Services
		{
		    public interface IAudioService
		    {
		        void Play(byte[] wavSound);
		        
		        //void StartRecording();
		        //byte[] StopRecording();
		    }
		}
		#endregion
		
	#endregion
	
	#region UnitOfWork
		
		#region HttpClientExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.UnitOfWork
		{
		    // ReSharper disable once RedundantUsingDirective
		    using System.Runtime.Remoting.Messaging;
		    using System.Net.Http;
		    using System.Threading.Tasks;
		    
		    public static class HttpClientExtensions
		    {
		        public static async Task<HttpResponseMessage> GetAsyncAndPreserveContext(this HttpClient httpClient, string url)
		        {
		            //This is to account for a Mono problem in Android
		            #if __ANDROID__
					var contextStack = CallContext.LogicalGetData("SupermodelDataContextStack");
		            var migrationInProgressFlag = CallContext.LogicalGetData("SupermodelSqliteMigrationInProgressOnThisThread");
		            #endif
		
		            var dataResponse = await httpClient.GetAsync(url);
		            
		            #if __ANDROID__
		            CallContext.LogicalSetData("SupermodelSqliteMigrationInProgressOnThisThread", migrationInProgressFlag);
		            CallContext.LogicalSetData("SupermodelDataContextStack", contextStack);
		            #endif
		
		            return dataResponse;
		        }
		
		        public static async Task<HttpResponseMessage> SendAsyncAndPreserveContext(this HttpClient httpClient, HttpRequestMessage request)
		        {
		            //This is to account for a Mono problem in Android
		            #if __ANDROID__
					var contextStack = CallContext.LogicalGetData("SupermodelDataContextStack");
		            var migrationInProgressFlag = CallContext.LogicalGetData("SupermodelSqliteMigrationInProgressOnThisThread");
		            #endif
		
		            var dataResponse = await httpClient.SendAsync(request);
		            
		            #if __ANDROID__
		            CallContext.LogicalSetData("SupermodelSqliteMigrationInProgressOnThisThread", migrationInProgressFlag);
		            CallContext.LogicalSetData("SupermodelDataContextStack", contextStack);
		            #endif
		
		            return dataResponse;
		        }
		    }
		}
		#endregion
		
		#region ImmutableStack
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.UnitOfWork
		{
		    using System.Collections.Generic;
		    using Validation;
		    using System;
		    using System.Collections;
		
		    public interface IImmutableStack<T> : IEnumerable<T>
		    {
		        bool IsEmpty { get; }
		        IImmutableStack<T> Clear();
		        IImmutableStack<T> Push(T value);
		        IImmutableStack<T> Pop();
		        T Peek();
		    }
		
		    public static class ImmutableStack
		    {
		        public static ImmutableStack<T> Create<T>()
		        {
		            return ImmutableStack<T>.Empty;
		        }
		
		        public static ImmutableStack<T> Create<T>(T item)
		        {
		            return ImmutableStack<T>.Empty.Push(item);
		        }
		
		        public static ImmutableStack<T> CreateRange<T>(IEnumerable<T> items)
		        {
		            // ReSharper disable PossibleMultipleEnumeration
		            Requires.NotNull(items, "items");
		            ImmutableStack<T> immutableStack = ImmutableStack<T>.Empty;
		            foreach (T obj in items)
		            immutableStack = immutableStack.Push(obj);
		            return immutableStack;
		            // ReSharper restore PossibleMultipleEnumeration
		        }
		
		        public static ImmutableStack<T> Create<T>(params T[] items)
		        {
		            Requires.NotNull(items, "items");
		            ImmutableStack<T> immutableStack = ImmutableStack<T>.Empty;
		            foreach (T obj in items)
		            immutableStack = immutableStack.Push(obj);
		            return immutableStack;
		        }
		
		        public static IImmutableStack<T> Pop<T>(this IImmutableStack<T> stack, out T value)
		        {
		            Requires.NotNull(stack, "stack");
		            value = stack.Peek();
		            return stack.Pop();
		        }
		    }
		
		    public sealed class ImmutableStack<T> : IImmutableStack<T>
		    {
		        // ReSharper disable once InconsistentNaming
		        private static readonly ImmutableStack<T> _emptyField = new ImmutableStack<T>();
		        private readonly T _head;
		        private readonly ImmutableStack<T> _tail;
		
		        public static ImmutableStack<T> Empty => _emptyField;
		        public bool IsEmpty => _tail == null;
		
		        private ImmutableStack(){}
		
		        private ImmutableStack(T head, ImmutableStack<T> tail)
		        {
		            Requires.NotNull(tail, "tail");
		            _head = head;
		            _tail = tail;
		        }
		
		        public ImmutableStack<T> Clear()
		        {
		            return Empty;
		        }
		
		        IImmutableStack<T> IImmutableStack<T>.Clear()
		        {
		            return Clear();
		        }
		
		        public T Peek()
		        {
		            if (IsEmpty) throw new InvalidOperationException("Unabled to Peek when ImmutableStack is Empty");
		            return _head;
		        }
		
		        public ImmutableStack<T> Push(T value)
		        {
		            return new ImmutableStack<T>(value, this);
		        }
		
		        IImmutableStack<T> IImmutableStack<T>.Push(T value)
		        {
		            return Push(value);
		        }
		
		        public ImmutableStack<T> Pop()
		        {
		            if (IsEmpty) throw new InvalidOperationException("Unabled to Pop when ImmutableStack is Empty");
		            return _tail;
		        }
		
		        public ImmutableStack<T> Pop(out T value)
		        {
		            value = Peek();
		            return Pop();
		        }
		
		        IImmutableStack<T> IImmutableStack<T>.Pop()
		        {
		            return Pop();
		        }
		
		        public Enumerator GetEnumerator()
		        {
		            return new Enumerator(this);
		        }
		
		        IEnumerator<T> IEnumerable<T>.GetEnumerator()
		        {
		            return new EnumeratorObject(this);
		        }
		
		        IEnumerator IEnumerable.GetEnumerator()
		        {
		            return new EnumeratorObject(this);
		        }
		
		        public ImmutableStack<T> Reverse()
		        {
		            var immutableStack1 = Clear();
		            for (var immutableStack2 = this; !immutableStack2.IsEmpty; immutableStack2 = immutableStack2.Pop())
		            {
		                immutableStack1 = immutableStack1.Push(immutableStack2.Peek());
		            }
		            return immutableStack1;
		        }
		
		        public struct Enumerator
		        {
		            private readonly ImmutableStack<T> _originalStack;
		            private ImmutableStack<T> _remainingStack;
		
		            public T Current
		            {
		                get
		                {
		                    if (_remainingStack == null || _remainingStack.IsEmpty)
		                    throw new InvalidOperationException();
		                    return _remainingStack.Peek();
		                }
		            }
		
		            internal Enumerator(ImmutableStack<T> stack)
		            {
		                Requires.NotNull(stack, "stack");
		                _originalStack = stack;
		                _remainingStack = null;
		            }
		
		            public bool MoveNext()
		            {
		                if (_remainingStack == null) _remainingStack = _originalStack;
		                else if (!_remainingStack.IsEmpty) _remainingStack = _remainingStack.Pop();
		                return !_remainingStack.IsEmpty;
		            }
		        }
		
		        private class EnumeratorObject : IEnumerator<T>
		        {
		            private readonly ImmutableStack<T> _originalStack;
		            private ImmutableStack<T> _remainingStack;
		            private bool _disposed;
		
		            public T Current
		            {
		                get
		                {
		                    ThrowIfDisposed();
		                    if (_remainingStack == null || _remainingStack.IsEmpty)
		                    throw new InvalidOperationException();
		                    return _remainingStack.Peek();
		                }
		            }
		
		            object IEnumerator.Current => Current;
		
		            internal EnumeratorObject(ImmutableStack<T> stack)
		            {
		                Requires.NotNull(stack, "stack");
		                _originalStack = stack;
		            }
		
		            public bool MoveNext()
		            {
		                ThrowIfDisposed();
		                if (_remainingStack == null) _remainingStack = _originalStack;
		                else if (!_remainingStack.IsEmpty) _remainingStack = _remainingStack.Pop();
		                return !_remainingStack.IsEmpty;
		            }
		
		            public void Reset()
		            {
		                ThrowIfDisposed();
		                _remainingStack = null;
		            }
		
		            public void Dispose()
		            {
		                _disposed = true;
		            }
		
		            private void ThrowIfDisposed()
		            {
		                if (!_disposed) return;
		                throw new ObjectDisposedException(GetType().FullName);
		            }
		        }
		    }
		}
		#endregion
		
		#region ReadOnly
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.UnitOfWork
		{
		    public enum ReadOnly
		    {
		        No,
		        Yes,
		    }
		}
		#endregion
		
		#region UnitOfWork
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.UnitOfWork
		{
		    using System;
		    using DataContext.Core;
		    using Exceptions;
		    using Async;
		
		    public class UnitOfWork<DataContextT> : IDisposable where DataContextT : class, IDataContext, new()
		    {
		        #region Constructors
		        public UnitOfWork(ReadOnly readOnly = ReadOnly.No)
		        {
		            Releaser = null;
		            Context = new DataContextT();
		            if (readOnly == ReadOnly.Yes) Context.MakeReadOnly();
		            UnitOfWorkContext<DataContextT>.PushDbContext(Context);
		        }
		        #endregion
		
		        #region IDisposable implemetation
		        public virtual void Dispose()
		        {
		            Context.Dispose();
		
		            var context = UnitOfWorkContext<DataContextT>.PopDbContext();
		
		            // ReSharper disable PossibleUnintendedReferenceComparison
		            if (context != Context) throw new SupermodelException("POP on Dispose popped mismatched Data Context.");
		            // ReSharper restore PossibleUnintendedReferenceComparison
		
		            if (Releaser != null) Releaser.Value.Dispose();
		        }
		        #endregion
		
		        #region Properties
		        public DataContextT Context { get; private set; }
		        public AsyncLock.Releaser? Releaser { get; set; }
		        #endregion
		    }
		}
		#endregion
		
		#region UnitOfWorkContext
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.UnitOfWork
		{
		    using System;
		    using Exceptions;
		    using Encryptor;
		    using System.Threading.Tasks;
		    using Models;
		    using System.Collections.Generic;
		    using DataContext.Core;
		    using DataContext.WebApi;
		    using System.Runtime.Remoting.Messaging;
		    using System.Linq;
		    using DataContext.Sqlite;
		
		    //Shortcuts for the most often used Context methods
		    public static class UnitOfWorkContext
		    {
		        #region Methods and Properties
		        public static Dictionary<string, object> CustomValues => UnitOfWorkContextCore.CurrentDataContext.CustomValues;
		        public static Task ResetDatabaseAsync()
		        {
		            if (!(UnitOfWorkContextCore.CurrentDataContext is SqliteDataContext)) throw new SupermodelException("ResetDatabaseAsync() is only valid for SqliteDataContext");
		            return ((SqliteDataContext)UnitOfWorkContextCore.CurrentDataContext).ResetDatabaseAsync();
		        }
		        public static Task SaveChangesAsync()
		        {
		            if (!(UnitOfWorkContextCore.CurrentDataContext is IWriteableDataContext)) throw new SupermodelException("SaveChangesAsync() is only valid for IWriteableDataContext");
		            return ((IWriteableDataContext)UnitOfWorkContextCore.CurrentDataContext).SaveChangesAsync();
		        }
		        public static Task FinalSaveChangesAsync()
		        {
		            if (!(UnitOfWorkContextCore.CurrentDataContext is IWriteableDataContext)) throw new SupermodelException("FinalSaveChangesAsync() is only valid for IWriteableDataContext");
		            return ((IWriteableDataContext)UnitOfWorkContextCore.CurrentDataContext).FinalSaveChangesAsync();
		        }
		        public static void DetectUpdates()
		        {
		            if (!(UnitOfWorkContextCore.CurrentDataContext is IWriteableDataContext)) throw new SupermodelException("DetectUpdates() is only valid for IWriteableDataContext");
		            ((IWriteableDataContext)UnitOfWorkContextCore.CurrentDataContext).DetectAllUpdates();
		        }
		        public static bool CommitOnDispose
		        {
		            get => UnitOfWorkContextCore.CurrentDataContext.CommitOnDispose;
		            set => UnitOfWorkContextCore.CurrentDataContext.CommitOnDispose = value;
		        }
		        public static AuthHeader AuthHeader
		        {
		            get
		            {
		                if (!(UnitOfWorkContextCore.CurrentDataContext is IWebApiAuthorizationContext)) throw new SupermodelException("AuthHeader is only accesable for IWebApiAuthorizationContext");
		                return ((IWebApiAuthorizationContext)UnitOfWorkContextCore.CurrentDataContext).AuthHeader;
		            }
		            set
		            {
		                if (!(UnitOfWorkContextCore.CurrentDataContext is IWebApiAuthorizationContext)) throw new SupermodelException("AuthorizationHeader is only accesable for IWebApiAuthorizationContext");
		                ((IWebApiAuthorizationContext)UnitOfWorkContextCore.CurrentDataContext).AuthHeader = value;
		            }
		        }
		        public static Task<LoginResult> ValidateLoginAsync<ModelT>() where ModelT : class, IModel
		        {
		            if (!(UnitOfWorkContextCore.CurrentDataContext is IWebApiAuthorizationContext)) throw new SupermodelException("AuthorizationHeader is only accesable for IWebApiAuthorizationContext");
		            return ((IWebApiAuthorizationContext)UnitOfWorkContextCore.CurrentDataContext).ValidateLoginAsync<ModelT>();
		        }
		        public static int CacheAgeToleranceInSeconds
		        {
		            get
		            {
		                if (!(UnitOfWorkContextCore.CurrentDataContext is ICachedDataContext)) throw new SupermodelException("CacheAgeToleranceInSeconds is only accesable for ICachedDataContext");
		                return ((ICachedDataContext)UnitOfWorkContextCore.CurrentDataContext).CacheAgeToleranceInSeconds;
		            }
		            set
		            {
		                if (!(UnitOfWorkContextCore.CurrentDataContext is ICachedDataContext)) throw new SupermodelException("CacheAgeToleranceInSeconds is only accesable for ICachedDataContext");
		                ((ICachedDataContext)UnitOfWorkContextCore.CurrentDataContext).CacheAgeToleranceInSeconds = value;
		            }
		        }
		        public static Task PurgeCacheAsync(int? cacheExpirationAgeInSeconds = null, Type modelType = null)
		        {
		            if (!(UnitOfWorkContextCore.CurrentDataContext is ICachedDataContext)) throw new SupermodelException("CacheAgeToleranceInSeconds is only accesable for ICachedDataContext");
		            return ((ICachedDataContext)UnitOfWorkContextCore.CurrentDataContext).PurgeCacheAsync(cacheExpirationAgeInSeconds, modelType);
		        }
		        #endregion
		    }
		    
		    public static class UnitOfWorkContext<DataContextT> where DataContextT : class, IDataContext, new()
		    {
		        #region Methods and Properties
		        public static DataContextT PopDbContext()
		        {
		            return (DataContextT)UnitOfWorkContextCore.PopDbContext();
		        }
		        public static void PushDbContext(DataContextT context)
		        {
		            UnitOfWorkContextCore.PushDbContext(context);
		        }
		        public static int StackCount => UnitOfWorkContextCore.StackCount;
		        public static DataContextT CurrentDataContext => (DataContextT)UnitOfWorkContextCore.CurrentDataContext;
		        public static bool HasDbContext()
		        {
		            return StackCount > 0;
		        }
		        #endregion
		    }
		
		    public static class UnitOfWorkContextCore
		    {
		        #region Methods and Properties
		        public static IDataContext PopDbContext()
		        {
		            try
		            {
		                IDataContext context;
		                _contextStackImmutable = _contextStackImmutable.Pop(out context);
		                return context;
		
		            }
		            catch (InvalidOperationException)
		            {
		                throw new InvalidOperationException("Stack is empty");
		            }
		        }
		        public static void PushDbContext(IDataContext context)
		        {
		            _contextStackImmutable = _contextStackImmutable.Push(context);
		        }
		        public static int StackCount => _contextStackImmutable.Count();
		        public static IDataContext CurrentDataContext
		        {
		            get
		            {
		                try
		                {
		                    return _contextStackImmutable.Peek();
		                }
		                catch (InvalidOperationException)
		                {
		                    throw new SupermodelException("Current UnitOfWork does not exist. All database access oprations must be wrapped in 'using(new UnitOfWork())'");
		                }
		            }
		        }
		        #endregion
		
		        #region Private variables
		        //private sealed class StackWrapper : MarshalByRefObject
		        //{
		        //    public ImmutableStack<IDataContext> Value { get; set; }
		        //}
		
		        // ReSharper disable once InconsistentNaming
		        private static ImmutableStack<IDataContext> _contextStackImmutable
		        {
		            get
		            {
		                var contextStack = CallContext.LogicalGetData("SupermodelDataContextStack") as ImmutableStack<IDataContext>;
		                return contextStack ?? ImmutableStack.Create<IDataContext>();
		            }
		            set
		            {
		                CallContext.LogicalSetData("SupermodelDataContextStack", value);
		            }
		        }
		        #endregion
		    }
		}
		#endregion
		
	#endregion
	
	#region Utils
		
		#region EmbeddedResource
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Utils
		{
		    using System;
		    using System.IO;
		    using System.Reflection;    
		    
		    public static class EmbeddedResource
		    {
		        public static byte[] ReadBinaryFile(Assembly assembly, string resourceName)
		        {
		            using (var stream = assembly.GetManifestResourceStream(resourceName))
		            {
		                if (stream == null) throw new ArgumentException(resourceName + " is not found.");
		                return ReadBytesToEnd(stream);
		            }
		        }
		        public static string ReadTextFile(Assembly assembly, string resourceName)
		        {
		            using (var stream = assembly.GetManifestResourceStream(resourceName))
		            {
		                if (stream == null) throw new ArgumentException(resourceName + " is not found.");
		                using (var reader = new StreamReader(stream)) { return reader.ReadToEnd(); }
		            }
		        }
		        private static byte[] ReadBytesToEnd(Stream input)
		        {
		            byte[] buffer = new byte[16*1024];
		            using (var ms = new MemoryStream())
		            {
		                int read;
		                while ((read = input.Read(buffer, 0, buffer.Length)) > 0) ms.Write(buffer, 0, read);
		                return ms.ToArray();
		            }
		        } 
		    }
		}
		#endregion
		
		#region ImageResizer
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Utils
		{
		    using System;
		    using System.Collections.Generic;
		    using System.Text;
		    using System.IO;
		    using System.Threading.Tasks;
		
		    // Usings je Platform
		
		    #if __IOS__
		    using System.Drawing;
		    using UIKit;
		    using CoreGraphics;
		    #endif
		
		
		    #if __ANDROID__
		    using Android.Graphics;
		    #endif
		
		    #if WINDOWS_UWP
		    using Windows.Graphics.Imaging;
		    using Windows.Storage.Streams;
		    using System.Runtime.InteropServices.WindowsRuntime;
		    #endif    
		    
		    #pragma warning disable 1998
		    public static class ImageResizer
		    {
		        public static async Task<byte[]> ResizeImageAsync(byte[] imageData, float maxWidth, float maxHeight)
		        {
		            byte[] result = null;
		            #if __IOS__
		            result = await ResizeImageIOSAsync(imageData, maxWidth, maxHeight);
		            #endif
		            #if __ANDROID__
		            result = await ResizeImageAndroidAsync(imageData, maxWidth, maxHeight);
		            #endif
		            #if WINDOWS_UWP
		            result = await ResizeImageWinPhoneAsync(imageData, maxWidth, maxHeight);
		            #endif
		            // ReSharper disable once ExpressionIsAlwaysNull
		            return result;
		        }
		        
		        #if __IOS__
		        public static async Task<byte[]> ResizeImageIOSAsync(byte[] imageData, float width, float height)
		        {
		            // Load the bitmap
		            UIImage originalImage = ImageFromByteArray(imageData);
		            //
		            var Hoehe = originalImage.Size.Height;
		            var Breite = originalImage.Size.Width;
		            //
		            nfloat ZielHoehe = 0;
		            nfloat ZielBreite = 0;
		            //
		
		            if (Hoehe > Breite) // Höhe (71 für Avatar) ist Master
		            {
		                ZielHoehe = height;
		                nfloat teiler = Hoehe / height;
		                ZielBreite = Breite / teiler;
		            }
		            else // Breite (61 for Avatar) ist Master
		            {
		                ZielBreite = width;
		                nfloat teiler = Breite / width;
		                ZielHoehe = Hoehe / teiler;
		            }
		            //
		            width = (float)ZielBreite;
		            height = (float)ZielHoehe;
		            //
		            UIGraphics.BeginImageContext(new SizeF(width, height));
		            originalImage.Draw(new RectangleF(0, 0, width, height));
		            var resizedImage = UIGraphics.GetImageFromCurrentImageContext();
		            UIGraphics.EndImageContext();
		            //
		            var bytesImagen = resizedImage.AsJPEG().ToArray();
		            resizedImage.Dispose();
		            return bytesImagen;
		        }
		        public static UIKit.UIImage ImageFromByteArray(byte[] data)
		        {
		            if (data == null)
		            {
		                return null;
		            }
		            //
		            UIKit.UIImage image;
		            try
		            {
		                image = new UIKit.UIImage(Foundation.NSData.FromArray(data));
		            }
		            catch (Exception e)
		            {
		                Console.WriteLine("Image load failed: " + e.Message);
		                return null;
		            }
		            return image;
		        }
		        #endif
		        #if __ANDROID__
		        public static async Task<byte[]> ResizeImageAndroidAsync(byte[] imageData, float width, float height)
		        {
		            // Load the bitmap 
		            Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);
		            //
		            float ZielHoehe = 0;
		            float ZielBreite = 0;
		            //
		            var Hoehe = originalImage.Height;
		            var Breite = originalImage.Width;
		            //
		            if (Hoehe > Breite) // Höhe (71 für Avatar) ist Master
		            {
		                ZielHoehe = height;
		                float teiler = Hoehe / height;
		                ZielBreite = Breite / teiler;
		            }
		            else // Breite (61 für Avatar) ist Master
		            {
		                ZielBreite = width;
		                float teiler = Breite / width;
		                ZielHoehe = Hoehe / teiler;
		            }
		            //
		            Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)ZielBreite, (int)ZielHoehe, false);
		            // 
		            using (MemoryStream ms = new MemoryStream())
		            {
		                resizedImage.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);
		                return ms.ToArray();
		            }
		        }
		        #endif
		        #if WINDOWS_UWP
				public static async Task<byte[]> ResizeImageWinPhoneAsync(byte[] imageData, float width, float height)
				{
				    using (var streamIn = new MemoryStream(imageData))
				    {
		                using (var imageStream = streamIn.AsRandomAccessStream())
		                {
		                    var decoder = await BitmapDecoder.CreateAsync(imageStream);
		                            
		                    var resizedStream = new InMemoryRandomAccessStream();
		                    var encoder = await BitmapEncoder.CreateForTranscodingAsync(resizedStream, decoder);
		
				            double targetHeight, targetWidth;
				            if (decoder.PixelHeight > decoder.PixelWidth)
				            {
				                targetHeight = height;
				                double scaleRatio = decoder.PixelHeight / height;
				                targetWidth = decoder.PixelWidth / scaleRatio;
				            }
				            else 
				            {
				                targetWidth = width;
				                double scaleRatio = decoder.PixelWidth / width;
				                targetHeight = decoder.PixelHeight / scaleRatio;
				            }
		
		                    encoder.BitmapTransform.InterpolationMode = BitmapInterpolationMode.Linear;
		                    encoder.BitmapTransform.ScaledHeight = (uint)targetHeight;
		                    encoder.BitmapTransform.ScaledWidth = (uint)targetWidth;
		
		                    await encoder.FlushAsync();
		                    resizedStream.Seek(0);
		                    var resizedData = new byte[resizedStream.Size];
		                    await resizedStream.ReadAsync(resizedData.AsBuffer(), (uint)resizedStream.Size, InputStreamOptions.None);
		                    return resizedData;
		                }
				    }
				}        
		        #endif
		    }
		    #pragma warning restore 1998
		}
		#endregion
		
		#region ObjectAttributeReaderExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Utils
		{
		    using System;
		    using System.ComponentModel;
		    using Attributes;
		    using ReflectionMapper;
		    using System.Reflection;
		    
		    public static class ObjectAttributeReaderExtensions
		    {
		        public static string GetDescription(this object value)
		        {
		            if (value == null) return "";
		            
		            //Tries to find a DescriptionAttribute for a potential friendly name for the enum
		            var type = value.GetType();
		
		            var valueToString = value.ToString();
		
		            //if (type.IsEnum)
		            if (type.GetTypeInfo().IsEnum)
		            {
		                var memberInfo = type.GetMember(valueToString);
		                if (memberInfo.Length > 0)
		                {
		                    //var attr = MyAttribute.GetCustomAttribute(memberInfo[0], typeof(DescriptionAttribute), true);
		                    var attr = memberInfo[0].GetCustomAttribute(typeof(DescriptionAttribute), true);
		                    if (attr != null) return ((DescriptionAttribute)attr).Description;
		                }
		                //If we have no description attribute, just return the ToString() or ToString().InsertSpacesBetweenWords() for enum
		                return value.ToString().InsertSpacesBetweenWords();
		            }
		
		            return valueToString;
		        }
		
		        public static int GetScreenOrder(this object value)
		        {
		            //Tries to find a ScreenOrderAttribute for a potential friendly name for the enum
		            var type = value.GetType();
		            var memberInfo = type.GetMember(value.ToString());
		            if (memberInfo.Length > 0)
		            {
		                //var attr = MyAttribute.GetCustomAttribute(memberInfo[0], typeof(ScreenOrderAttribute), true);
		                var attr = memberInfo[0].GetCustomAttribute(typeof(ScreenOrderAttribute), true);
		                if (attr != null) return ((ScreenOrderAttribute)attr).Order;
		            }
		            //If we have no order, default is 100
		            return 100;
		        }
		
		        public static bool IsDisabled(this object value)
		        {
		            if (value == null) return false;
		            
		            var type = value.GetType();
		            var memberInfo = type.GetMember(value.ToString());
		            if (memberInfo.Length > 0)
		            {
		                //var attr = MyAttribute.GetCustomAttribute(memberInfo[0], typeof(DisabledAttribute), true);
		                var attr = memberInfo[0].GetCustomAttribute(typeof(DisabledAttribute), true);
		                if (attr != null) return true;
		            }
		            //If we have no disabled attribute, we assume active
		            return false;
		        }
		    }
		}
		#endregion
		
		#region ObservableCollectionExtensions
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Utils
		{
		    using System;
		    using System.Collections.ObjectModel;
		    using System.Linq;
		    
		    public static class ObservableCollectionExtensions
		    {
		        public static int RemoveAll<T>(this ObservableCollection<T> coll, Func<T, bool> condition)
		        {
		            var itemsToRemove = coll.Where(condition).ToList();
		            foreach (var itemToRemove in itemsToRemove) coll.Remove(itemToRemove);
		            return itemsToRemove.Count;
		        }
		    }
		}
		#endregion
		
		#region SearchByHelper
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.Utils
		{
		    using System.Linq;
		    using System.Reflection;
		    using System.Text;
		    using System.Net;
		
		    public static class SearchByHelper
		    {
		        public static object PropertyGetWithNullIfNoProperty(this object me, string propertyName)
		        {
		            var propertyInfo = me.GetType().GetTypeInfo().DeclaredProperties.SingleOrDefault(x => x.Name == propertyName);
		            return propertyInfo?.GetValue(me);
		        }
		
		        public static string ToQueryString<T>(this T me)
		        {
		            var sb = new StringBuilder();
		
		            var firstColumn = true;
		            foreach (var property in me.GetType().GetTypeInfo().DeclaredProperties)
		            {
		                if (firstColumn) firstColumn = false;
		                else sb.Append("&");
		
		                var propertyObj = me.GetType().GetProperty(property.Name).GetValue(me);
		                var propertyValue = "";
		                if (propertyObj != null) propertyValue = propertyObj.ToString();
		
		                sb.Append($"{property.Name}={WebUtility.UrlEncode(propertyValue)}");
		            }
		            return sb.ToString();
		        }
		    }
		}
		#endregion
		
	#endregion
	
	#region UWPDeficiencies
		
		#region Attributes
			
			#region DecsriptionAttribue
			#if WINDOWS_UWP
			// ReSharper disable once CheckNamespace
			namespace System.ComponentModel
			{
			    using System; 
			
			    [AttributeUsage(AttributeTargets.All)]
			    public class DescriptionAttribute : Attribute
			    { 
			        #region Constructors
			        public DescriptionAttribute() : this (string.Empty) {}
			        public DescriptionAttribute(string description) { this.description = description;  }
			        #endregion
			        
			        #region Methods
			        public virtual string Description => DescriptionValue;
			
			        protected string DescriptionValue
			        {
			            get { return description; }
			            set { description = value; }
			        } 
			 
			        public override bool Equals(object obj)
			        {
			            if (obj == this)  return true; 
			            var other = obj as DescriptionAttribute; 
			            return (other != null) && other.Description == Description; 
			        } 
			
			        public override int GetHashCode()
			        { 
			            return Description.GetHashCode();
			        }
			        #endregion
			
			        #region Properties
			        public static readonly DescriptionAttribute Default = new DescriptionAttribute();
			        private string description;
			        #endregion
			    } 
			}
			#endif
			#endregion
			
			#region DisplayNameAttribute
			#if WINDOWS_UWP
			
			// ReSharper disable once CheckNamespace
			namespace System.ComponentModel
			{
			    using System; 
			
			    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Event | AttributeTargets.Class | AttributeTargets.Method)]
			    public class DisplayNameAttribute : Attribute
			    {
			        #region Constructors
			        public DisplayNameAttribute() : this (string.Empty) {}
			        public DisplayNameAttribute(string displayName)
			        {
			            _displayName = displayName; 
			        }
			        #endregion
			
			        #region Methods
			        public virtual string DisplayName => DisplayNameValue;
			
			        protected string DisplayNameValue
			        {
			            get { return _displayName; }
			            set { _displayName = value; }
			        } 
			 
			        public override bool Equals(object obj) 
			        {
			            if (obj == this) return true; 
			            var other = obj as DisplayNameAttribute; 
			            return (other != null) && other.DisplayName == DisplayName; 
			        } 
			
			        public override int GetHashCode()
			        { 
			            return DisplayName.GetHashCode();
			        }
			
			        public bool IsDefaultAttribute()
			        { 
			            return (Equals(Default));
			        } 
			        #endregion
			
			        #region Properties
			        public static readonly DisplayNameAttribute Default = new DisplayNameAttribute();
			        private string _displayName;
			        #endregion
			    }
			}
			#endif
			#endregion
			
		#endregion
		
		#region CallContext
		#if WINDOWS_UWP
		// ReSharper disable once CheckNamespace
		namespace System.Runtime.Remoting.Messaging
		{
		    using Collections.Generic;
		    using Threading;
		
		    public static class CallContext
		    {
		        #region Methods
				public static object LogicalGetData(string key)
				{
		            if (!_logicalContextDictionary.ContainsKey(key)) return null;
				    return _logicalContextDictionary[key];
				}
				public static void LogicalSetData(string key, object value)
				{
				    _logicalContextDictionary[key] = value;
				}
		        private static Dictionary<string, object> _logicalContextDictionary
		        {
		            get
		            {
		                if (_logicalData.Value == null) _logicalData.Value = new Dictionary<string, object>();
		                return _logicalData.Value;
		            }
		        }
		        #endregion
		
		        #region Private Variables
		        private static AsyncLocal<Dictionary<string, object>> _logicalData = new AsyncLocal<Dictionary<string, object>>();
		        #endregion
		    }
		}
		#endif
		#endregion
		
	#endregion
	
	#region XForms
		
		#region App
			
			#region FormsApplication
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.App
			{
			    using Xamarin.Forms;
			
			    // ReSharper disable UnusedAutoPropertyAccessor.Local
			    #if __IOS__
				using Xamarin.Forms.Platform.iOS;
			    using UIKit;
			    using Foundation;
						    
				public abstract class FormsApplication<AppT> : FormsApplication where AppT : SupermodelXamarinFormsApp, new()
				{
			        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
			        {
			            AppDelegate = this;
			            Forms.Init();
			            LoadApplication(new AppT());
			            return base.FinishedLaunching(app, options);
			        }        
			        
			        public static AppT RunningApp => (AppT)_runningApp;
				}
			    public abstract class FormsApplication : FormsApplicationDelegate
			    {
			        public static FormsApplicationDelegate AppDelegate { get; protected set; }
			        public static void SetRunningApp(SupermodelXamarinFormsApp runningApp) { _runningApp = runningApp; }
			        public static SupermodelXamarinFormsApp GetRunningApp() { return _runningApp; }
			
			        // ReSharper disable once InconsistentNaming
			        protected static SupermodelXamarinFormsApp _runningApp;
			    }
			    #elif __ANDROID__
			    using Xamarin.Forms.Platform.Android;
			    using Android.OS;
			
			    public abstract class FormsApplication<AppT> : FormsApplication where AppT : SupermodelXamarinFormsApp, new()
			    {
			        protected override void OnCreate(Bundle bundle)
			        {
			            MainActivity = this;
			            base.OnCreate(bundle);
			            Forms.Init(this, bundle);
			            LoadApplication(new AppT());
			        }        
			        public static AppT RunningApp => (AppT)_runningApp;
			    }
			    public abstract class FormsApplication : FormsApplicationActivity
			    {
			        public static FormsApplicationActivity MainActivity { get; protected set; }
			        public static void SetRunningApp(SupermodelXamarinFormsApp runningApp) { _runningApp = runningApp; }
			        public static SupermodelXamarinFormsApp GetRunningApp() { return _runningApp; }
			        
			        // ReSharper disable once InconsistentNaming
			        protected static SupermodelXamarinFormsApp _runningApp;
			    }
			    #else
			    public abstract class FormsApplication<AppT> : FormsApplication where AppT : SupermodelXamarinFormsApp, new()
			    {
			        public static AppT RunningApp => (AppT)_runningApp;
			    }
			    public abstract class FormsApplication
			    {
			        public static void SetRunningApp(SupermodelXamarinFormsApp runningApp) { _runningApp = runningApp; }
			        public static SupermodelXamarinFormsApp GetRunningApp() { return _runningApp; }
			        
			        // ReSharper disable once InconsistentNaming
			        protected static SupermodelXamarinFormsApp _runningApp;
			    }
			    #endif
			    // ReSharper restore UnusedAutoPropertyAccessor.Local
			}
			#endregion
			
			#region SupermodelXamarinFormsApp
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.App
			{
			    using Xamarin.Forms;
			    using Pages.Login;
			    using UnitOfWork;
			    using DataContext.Core;
			
			    public abstract class SupermodelXamarinFormsApp : Application
			    {
			        protected SupermodelXamarinFormsApp()
			        {
			            FormsApplication.SetRunningApp(this); 
			        }
			
			        public IAuthHeaderGenerator AuthHeaderGenerator { get; set; }
			        public virtual UnitOfWork<DataContextT> NewUnitOfWork<DataContextT>(ReadOnly readOnly = ReadOnly.No) where DataContextT : class, IDataContext, new()
			        {
			            var unitOfWork = new UnitOfWork<DataContextT>(readOnly);
			            if (unitOfWork.Context is IWebApiAuthorizationContext)
			            {
			                if (AuthHeaderGenerator != null) UnitOfWorkContext.AuthHeader = AuthHeaderGenerator.CreateAuthHeader();
			            }
			            return unitOfWork;
			        }
			
			        public abstract void HandleUnauthorized();
			        public abstract byte[] LocalStorageEncryptionKey { get; }
			    }
			}
			#endregion
			
		#endregion
		
		#region Pages
			
			#region CRUDDetail
				
				#region CRUDChildDetailPage
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
				{
				    using ViewModels;
				    using Models;
				    using DataContext.Core;
				    using System;
				    using System.Collections.ObjectModel;
				    using ReflectionMapper;
				    using System.Threading.Tasks;
				
				    public abstract class CRUDChildDetailPage<ModelT, ChildModelT, XFModelT, DataContextT> : CRUDDetailPageBase<ModelT, XFModelT, DataContextT>
				        where ModelT : class, ISupermodelNotifyPropertyChanged, IModel, new()
				        where ChildModelT : ChildModel, new()
				        where XFModelT : XFModelForChildModel<ModelT, ChildModelT>, new()
				        where DataContextT : class, IDataContext, new()
				    {
				        #region Initializers
				        public virtual CRUDChildDetailPage<ModelT, ChildModelT, XFModelT, DataContextT> Init(ObservableCollection<ModelT> models, string title, ModelT model, Guid childGuidIdentity, params Guid[] parentGuidIdentities)
				        {
				            var xfModel = new XFModelT();
				            xfModel.Init(model, childGuidIdentity, parentGuidIdentities);
				            xfModel = xfModel.MapFrom(model);
				
				            var originalXFModel = new XFModelT();
				            originalXFModel.Init(model, childGuidIdentity, parentGuidIdentities);
				            originalXFModel = originalXFModel.MapFrom(model);
				
				            XFModel = xfModel;
				            OriginalXFModel = originalXFModel;
				
				            return (CRUDChildDetailPage<ModelT, ChildModelT, XFModelT, DataContextT>)base.Init(models, title, model, xfModel, originalXFModel);
				        }
				        #endregion
				
				        #region Overrdies
				        protected override XFModelT GetBlankXFModel()
				        {
				            var blankXfModel = (XFModelT)new XFModelT().Init(Model, OriginalXFModel.ChildGuidIdentity, OriginalXFModel.ParentGuidIdentities);
				            return blankXfModel;
				        }
				        #endregion
				    }
				}
				#endregion
				
				#region CRUDDetailPage
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
				{
				    using System.Collections.ObjectModel;
				    using ViewModels;
				    using ReflectionMapper;
				    using Models;
				    using DataContext.Core;
				
				    public abstract class CRUDDetailPage<ModelT, XFModelT, DataContextT> : CRUDDetailPageBase<ModelT, XFModelT, DataContextT>
				        where ModelT : class, ISupermodelNotifyPropertyChanged, IModel, new()
				        where XFModelT : XFModelForModel<ModelT>, new()
				        where DataContextT : class, IDataContext, new()
				    {
				        #region Initializers
				        public virtual CRUDDetailPage<ModelT, XFModelT, DataContextT> Init(ObservableCollection<ModelT> models, string title, ModelT model)
				        {
				            var xfModel = new XFModelT();
				            xfModel.Init(model);
				            xfModel = xfModel.MapFrom(model);
				
				            var originalXFModel = new XFModelT();
				            originalXFModel.Init(model);
				            originalXFModel = originalXFModel.MapFrom(model);
				
				            return (CRUDDetailPage<ModelT, XFModelT, DataContextT>)base.Init(models, title, model, xfModel, originalXFModel);
				        }
				        #endregion
				
				        #region Overrdies
				        protected override XFModelT GetBlankXFModel()
				        {
				            var blankModel = new ModelT();
				            var blankXfModel = (XFModelT)new XFModelT().Init(blankModel);
				            return blankXfModel;
				        }
				        #endregion
				    }
				}
				#endregion
				
				#region CRUDDetailPageBase
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
				{
				    using System.Collections.Generic;
				    using System.Collections.ObjectModel;
				    using ViewModels;
				    using ReflectionMapper;
				    using System;
				    using System.Net;
				    using System.Threading.Tasks;
				    using Exceptions;
				    using System.ComponentModel.DataAnnotations;
				    using System.Linq;
				    using Views;
				    using Utils;
				    using Models;
				    using System.Reflection;
				    using App;
				    using DataContext.Core;
				    using UnitOfWork;
				    using System.Collections;
				
				    public abstract class CRUDDetailPageBase<ModelT, XFModelT, DataContextT> : CRUDDetailPageCore<ModelT, XFModelT>, IHaveActivityIndicator
				        where ModelT : class, ISupermodelNotifyPropertyChanged, IModel, new()
				        where XFModelT : XFModel, new()
				        where DataContextT : class, IDataContext, new()
				    {
				        #region Initializers
				        protected virtual CRUDDetailPageBase<ModelT, XFModelT, DataContextT> Init(ObservableCollection<ModelT> models, string title, ModelT model, XFModelT xfModel, XFModelT originalXFModel)
				        {
				            Title = title;
				
				            if (CancelButton) AddCancelButton();
				
				            Models = models;
				            Model = model;
				            XFModel = xfModel;
				            OriginalXFModel = originalXFModel;
				
				            return this;
				        }
				        #endregion
				        
				        #region IHaveActivityIndicator implementation
				        public async Task WaitForPageToBecomeActiveAsync()
				        {
				            while(!PageActive) await Task.Delay(25);
				        }
				        public bool ActivityIndicatorOn
				        {
				            get => DetailView.ActivityIndicatorOn;
				            set => DetailView.ActivityIndicatorOn = value;
				        }
				        public string Message
				        {
				            get => DetailView.Message;
				            set => DetailView.Message = value;
				        }
				        #endregion
				
				        #region Overrides
				        protected abstract XFModelT GetBlankXFModel();
				        protected override async void OnAppearing()
				        {
				            base.OnAppearing();
				            InitContent();
				            PageActive = true;
				            if (XFModel.ContainsValidationErrros()) await DisplayAlert("Validation Errors", "Please correct problems with fields marked with '!'", "Ok");
				        }
				        protected override async void OnDisappearing()
				        {
				            PageActive = false;
				            await OnDisappearingInternalAsync();
				        }
				        protected virtual async Task OnDisappearingInternalAsync()
				        {
				            var navigationStack = Navigation.NavigationStack;
				            //Store current state of NavStack
				            var navStackCount = navigationStack.Count;
				
				            var goingBack = navigationStack.Last() == this;
				            var parentPage = navigationStack[navigationStack.Count - (goingBack ? 2 : 3)];
				            var childPage = goingBack ? null : navigationStack.Last();
				            var pageShowing = goingBack ? parentPage : childPage;
				
				            //Finish disapearing :)
				            base.OnDisappearing();
				
				            //Let the page finish dissapearing from NavStack
				            while (navigationStack.Count == navStackCount) await Task.Delay(100);
				
				            if (DissaperingBecasueOfCancellation || (goingBack && XFModel.AreWritableFieldsEqual(GetBlankXFModel()))) return;
				
				            //Try to validate locally. We validate even if the hash did not chnage since we only calculate hash to persisitant fields
				            var localVr = new ValidationResultList();
				            if (!Validator.TryValidateObject(XFModel, new ValidationContext(XFModel, new Dictionary<object,object> { { "CanBeCancellation", goingBack } }), localVr))
				            {
				                //if we had local validation errors
				                XFModel.ShowValidationErrors(localVr);
				                if (goingBack)
				                {
				                    var page = (CRUDDetailPageBase<ModelT, XFModelT, DataContextT>)ReflectionHelper.CreateType(GetType());
				                    await parentPage.Navigation.PushAsync(page.Init(Models, Title, Model, XFModel, OriginalXFModel));
				                }
				                else
				                {
				                    await parentPage.Navigation.PopAsync();     
				                }
				                return;
				            }
				
				            //Map to Model
				            var originalModelHash = ComputeModelHash(Model);
				            ValidationResultList mappingVr = null;
				            try
				            {
				                Model = XFModel.MapTo(Model);
				                if (pageShowing is IBasicCRUDDetailPage basicCRUDPageShowing) basicCRUDPageShowing.InitContent(); //we do this becasue on Android page appears before page dissapears
				            }
				            catch (ValidationResultException ex)
				            {
				                mappingVr = ex.ValidationResultList;
				            }
				            if (mappingVr != null && mappingVr.Any())
				            {
								//if we had mapping validation errors
								Model = OriginalXFModel.MapTo(Model);
				                if (pageShowing is IBasicCRUDDetailPage basicCRUDPageShowing) basicCRUDPageShowing.InitContent(); //we do this becasue on Android page appears before page dissapears
				                XFModel.ShowValidationErrors(mappingVr);
								if (goingBack)
								{
				                    var page = (CRUDDetailPageBase<ModelT, XFModelT, DataContextT>)ReflectionHelper.CreateType(GetType());
								    await parentPage.Navigation.PushAsync(page.Init(Models, Title, Model, XFModel, OriginalXFModel));
								}
				                else
								{
								    await parentPage.Navigation.PopAsync();     
								}
								return;
				            }
				
							//if Model did not change, we don't need to save it
							if (ComputeModelHash(Model) == originalModelHash) 
							{
								//This is in case we had vaication errors in the XFModel to begin with
				                XFModel.ClearValidationErrors();
				                return;
							}
				
				            //Try to save to DataContext
				            bool connectionLost;
				            ValidationResultList serverVr = null;
				            do
				            {
				                connectionLost = false;
				                try
				                {
				                    var pageShowingActivityIndicator = pageShowing as IHaveActivityIndicator;
				                    if (pageShowingActivityIndicator != null)
				                    {
				                        using (await ActivityIndicatorFor.CreateAsync(pageShowingActivityIndicator))
				                        {
				                            //var t1 = Task.Delay(250); //short delay so that saving indicator could be shown
				                            //var t2 = SaveItemInternalAsync(Model);
				                            //await Task.WhenAll(t1, t2);
				                            await SaveItemInternalAsync(Model);
				                        }
				                    }
				                    else
				                    {
				                        await SaveItemInternalAsync(Model);
				                    }
								    if (!Model.IsNew && Models.All(x => x.Id != Model.Id)) Models.Add(Model);
				                }
				                catch (SupermodelWebApiException ex1)
				                {
				                    if (ex1.StatusCode == HttpStatusCode.Unauthorized)
				                    {
				                        UnauthorizedHandler();
				                    }
				                    else if (ex1.StatusCode == HttpStatusCode.NotFound)
				                    {
				                        Models.RemoveAll(x => x == Model);
				                        await pageShowing.DisplayAlert("Not Found", "Item you are trying to update no longer exists.", "Ok");
				                    }
				                    else if (ex1.StatusCode == HttpStatusCode.Conflict)
				                    {
				                        await parentPage.DisplayAlert("Unable to Delete", ex1.ContentJsonMessage, "Ok");
				                    }
				                    else if (ex1.StatusCode == HttpStatusCode.InternalServerError)
				                    {
				                        connectionLost = true;
				                        await parentPage.DisplayAlert("Internal Server Error", ex1.ContentJsonMessage, "Ok");
				                    }
				                    else
				                    {
				                        connectionLost = true;
				                        await pageShowing.DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
				                    }
				                }
				                catch (SupermodelDataContextValidationException ex2)
				                {
				                    var vrl = ex2.ValidationErrors;
				                    if (vrl.Count != 1) throw new SupermodelException("vrl.Count != 1. This should never happen!");
				                    if (!vrl[0].Any()) throw new SupermodelException("!vrl[0].Any(): Server returned validation error with no validation results");
				                    serverVr = vrl[0];
				                }
				                catch (WebException)
				                {
				                    connectionLost = true;
				                    await pageShowing.DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
				                }
				                catch (Exception ex3)
				                {
				                    await DisplayAlert("Unexpected Error", ex3.Message, "Try again");
				                    connectionLost = true;
				                }
				            } 
				            while (connectionLost);  
				
				            if (serverVr != null && serverVr.Any())
				            {
				                //if we had any validation errors while trying to save to DataContext
				                Model = OriginalXFModel.MapTo(Model); 
				                XFModel.ShowValidationErrors(serverVr);
				
				                if (goingBack)
				                {
				                    var page = (CRUDDetailPageBase<ModelT, XFModelT, DataContextT>)ReflectionHelper.CreateType(GetType());
				                    await parentPage.Navigation.PushAsync(page.Init(Models, Title, Model, XFModel, OriginalXFModel));
				                }
				                else
				                {
								    await parentPage.Navigation.PopAsync();   
				                }
				                return;
				            }
				            //If no validation issues, mark all properteis as changed. This way the list will always update
				            //foreach (var property in Model.GetType().GetTypeInfo().DeclaredProperties) Model.OnPropertyChanged(property.Name);
				            MarkAllPropertiesChanged(Model);
				
				            //This is in case we had vaication errors in the XFModel to begin with
				            XFModel.ClearValidationErrors();
				        }
				        protected void MarkAllPropertiesChanged(ISupermodelNotifyPropertyChanged model)
				        {
				            foreach (var property in model.GetType().GetTypeInfo().DeclaredProperties)
				            {
				                Model.OnPropertyChanged(property.Name);
				                var propertyValue = model.PropertyGet(property.Name);
				                if (propertyValue is ISupermodelNotifyPropertyChanged propertyValueChangedObj) MarkAllPropertiesChanged(propertyValueChangedObj);
				                if (propertyValue is IEnumerable propertyValueIEnumerableChanged)
				                {
				                    foreach (var propertyValueInIEnumerable in propertyValueIEnumerableChanged)
				                    {
				                        if (propertyValueInIEnumerable is ISupermodelNotifyPropertyChanged propertyValueInIEnumerableChangedObj) MarkAllPropertiesChanged(propertyValueInIEnumerableChangedObj);
				                    }
				                }
				            }
				        }
				        protected virtual async Task SaveItemInternalAsync(ModelT model)
				        {
				            using (FormsApplication.GetRunningApp().NewUnitOfWork<DataContextT>())
				            {
				                if (model.IsNew) model.Add();
				                else model.Update();
				                await UnitOfWorkContext.FinalSaveChangesAsync();
				            }
				        }
				        #endregion
				
				        #region Properties
				        protected bool PageActive { get; set; }
				        #endregion
				    }
				}
				#endregion
				
				#region CRUDDetailPageCore
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
				{
				    using System.Collections.ObjectModel;
				    using Newtonsoft.Json;
				    using Encryptor;
				    using Models;
				    using App;
				    using ViewModels;
				    using Xamarin.Forms;
				    using System.Linq;
				
				    public abstract class CRUDDetailPageCore<ModelT, XFModelT> : ContentPage, IBasicCRUDDetailPage
				        where ModelT : class, ISupermodelNotifyPropertyChanged, IModel, new()
				        where XFModelT : XFModel, new()
				    {
				        #region Overrides
				        protected virtual void AddCancelButton()
				        {
				            var cancelToolbarItem = new ToolbarItem("Cancel", CancelBtnIconFilename, async () => {
				                DissaperingBecasueOfCancellation = true;
				                await Navigation.PopAsync(true);
				            });
				            ToolbarItems.Add(cancelToolbarItem);
				        }
				        protected virtual void UnauthorizedHandler()
				        {
				            FormsApplication.GetRunningApp().HandleUnauthorized();
				        }
				        protected virtual string ComputeModelHash(ModelT model)
				        {
				            //We hash Json to ignore changes that do not get persisted
				            return JsonConvert.SerializeObject(Model).GetMD5Hash();
				        }
				        #endregion
				
				        #region Methods
				        //override this method to affect the entire view
				        public virtual void InitContent()
				        {
				            //if (DetailView == null)
				            //{
				            //    DetailView = new CRUDDetailView();
				            //    Content = StackLayout = new StackLayout { Children = { DetailView } };
				
				            //    OnLoad();
				            //    InitDetailView();
				            //}
				            DetailView = new CRUDDetailView();
				            Content = StackLayout = new StackLayout { Children = { DetailView } };
				
				            OnLoad();
				            InitDetailView();
				        }
				        //Override this method to create sections
				        public virtual void InitDetailView()
				        {
				            // ReSharper disable once SuspiciousTypeConversion.Global
				            var sectionResolver = XFModel as IHaveSectionNames;
				
				            var lastCell = XFModel.RenderDetail(this)?.LastOrDefault();
				            if (lastCell != null)
				            {
				                var sectionNum = 0;
				                while (true)
				                {
				                    var cells = XFModel.RenderDetail(this, sectionNum, sectionNum + 99);
				                    if (cells.Any())
				                    {
				                        var sectionName = sectionResolver?.GetSectionName(sectionNum);
				                        var section = string.IsNullOrEmpty(sectionName) ? new TableSection() : new TableSection(sectionName);
				                        DetailView.ContentView.Root.Add(section);
				                        foreach (var cell in cells) section.Add(cell);
				                    }
				                    if (cells.Contains(lastCell)) break;
				                    sectionNum += 100;
				                }
				            }
				        }
				        public virtual void OnLoad(){}
				        #endregion
				
				        #region Properties
				        public StackLayout StackLayout { get; set; }
				        public CRUDDetailView DetailView { get; set; }
				        public ObservableCollection<ModelT> Models { get; set; } 
				        public ModelT Model { get; set; }
				        public XFModelT XFModel { get; set; }
				        public XFModel GetXFModel() { return XFModel; }
				        public T GetXFModel<T>() where T : XFModel { return (T)(XFModel)XFModel; }
				
				
				        public XFModelT OriginalXFModel { get; set; }
				
				        protected virtual bool CancelButton => false;
				        protected virtual string CancelBtnIconFilename => null;
				        protected bool DissaperingBecasueOfCancellation { get; set; } //default is false
				        #endregion
				    }
				}
				#endregion
				
				#region CRUDDetailView
				 // ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
				{
				    using Xamarin.Forms;
				    using Views;
				
				    public class CRUDDetailView : ViewWithActivityIndicator<TableView>
				    {
				        public CRUDDetailView() : base(new TableView { Intent = TableIntent.Form, HasUnevenRows = true, Root = new TableRoot()}){}
				    }
				}
				#endregion
				
				#region IBasicCRUDDetailPage
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
				{
				    using ViewModels;
				    using Xamarin.Forms;
				    using System.Threading.Tasks;
				
				    public interface IBasicCRUDDetailPage : ILayout, IPageController, IElementConfiguration<Page>
				    {
				        void InitContent();
				
				        CRUDDetailView DetailView { get; set; }
				        XFModel GetXFModel();
				        T GetXFModel<T>() where T : XFModel;
				
				        Task DisplayAlert(string title, string message, string cancel);
				        Task<bool> DisplayAlert(string title, string message, string accept, string cancel);
				    }
				}
				#endregion
				
				#region IHaveSectionNames
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.CRUDDetail
				{
				    public interface IHaveSectionNames
				    {
				        string GetSectionName(int sectionScreenNumber);
				    }
				}
				#endregion
				
			#endregion
			
			#region CRUDList
				
				#region CRUDListPage
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.CRUDList
				{
				    using System.Linq;
				    using System;
				    using System.Collections.ObjectModel;
				    using System.Net;
				    using System.Threading.Tasks;
				    using Exceptions;
				    using Xamarin.Forms;
				    using ViewModels;
				    using Views;
				    using System.Collections.Generic;
				    using Models;
				    using DataContext.Core;
				    using App;
				    using UnitOfWork;
				    using Repository;
				
				    public abstract class CRUDListPage<ModelT, DataContextT> : CRUDListPageCore<ModelT> 
				        where ModelT : class, ISupermodelListTemplate, IModel, new()
				        where DataContextT : class, IDataContext, new()
				    {
				        #region Event Handlers
				        public override async void ItemAppearingHandler(object sender, ItemVisibilityEventArgs args)
				        {
				            if (LoadedAll || LoadingInProgress) return;
				            if (Models.Last() == args.Item) await LoadListContentAsync(Models.Count, Take);
				        }
				        public virtual async void RefreshingHandler(object sender, EventArgs args)
				        {
				            Models = null;
				            await LoadListContentAsync(showActivityIndicator: false);
				            ListView.ListPanel.ContentView.IsRefreshing = false;
				        }
				        #endregion
				
				        #region Overrides
				        protected override void InitContent(bool readOnly)
				        {
				            Content = StackLayout = new StackLayout();
				            if (readOnly) ListView = new CRUDListView<ModelT>(null, false);
				            else Content = ListView = new CRUDListView<ModelT>(DeleteItemHandler, false);
				            StackLayout.Children.Add(ListView);
				
				            ListView.ListPanel.ContentView.Refreshing += RefreshingHandler;
				        }
				        protected override async Task<bool> DeleteItemInternalAsync(ModelT model)
				        {
				            using (FormsApplication.GetRunningApp().NewUnitOfWork<DataContextT>())
				            {
				                model.Delete();
				                await UnitOfWorkContext.FinalSaveChangesAsync();
				                return true;
				            }
				        }
				        protected virtual async Task<List<ModelT>> GetItemsInternalAsync(int skip, int? take)
				        {
				            using (FormsApplication.GetRunningApp().NewUnitOfWork<DataContextT>(ReadOnly.Yes))
				            {
				                var repo = RepoFactory.Create<ModelT>();
				                return await repo.GetAllAsync(skip, take);
				            }
				        }
				        #endregion
				
				        #region Methods
				        public virtual async Task LoadListContentAsync(int skip = 0, int? take = -1, bool showActivityIndicator = true)
				        {
				            var navStackCurrentPage = Application.Current.MainPage.Navigation.NavigationStack.LastOrDefault();
				            var modalStackCurrentPage = Application.Current.MainPage.Navigation.ModalStack.LastOrDefault();
				
				            if (this != navStackCurrentPage && this != modalStackCurrentPage) throw new Exception("LoadListContentAsync() can only be called when the Page is active");
				
				            if (take < 0) take = Take;
				            bool connectionLost;
				            do
				            {
				                connectionLost = false;
				                try
				                {
				                    LoadingInProgress = true;
				                    LoadedAll = false;
				                    
				                    if (Models == null) Models = new ObservableCollection<ModelT>();
				                    if (skip == 0) Models.Clear();
				                    
				                    if (showActivityIndicator)
				                    {
				                        using(new ActivityIndicatorFor(ListView.ListPanel))
				                        {
				                            var models = await GetItemsInternalAsync(skip, take);
				                            if (take == null || models.Count < take) LoadedAll = true;
				
				                            foreach (var model in models)
				                            {
				                                if (Models.All(x => x.Id != model.Id)) Models.Add(model);
				                            }
				                            ListView.ListPanel.ContentView.ItemsSource = Models;
				                        }
				                    }
				                    else
				                    {
				                        var models = await GetItemsInternalAsync(skip, take);
				                        if (take == null || models.Count < take) LoadedAll = true;
				
				                        foreach (var model in models)
				                        {
				                            if (Models.All(x => x.Id != model.Id)) Models.Add(model);
				                        }
				                        ListView.ListPanel.ContentView.ItemsSource = Models;
				                    }
				                }
				                catch (SupermodelWebApiException ex1)
				                {
				                    if (ex1.StatusCode == HttpStatusCode.Unauthorized)
				                    {
				                        UnauthorizedHandler();
				                    }
				                    else if (ex1.StatusCode == HttpStatusCode.InternalServerError)
				                    {
				                        connectionLost = true;
				                        await DisplayAlert("Internal Server Error", ex1.ContentJsonMessage, "Ok");
				                    }
				                    else
				                    {
				                        connectionLost = true;
				                        await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
				                    }
				                }
				                catch (WebException)
				                {
				                    connectionLost = true;
				                    await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
				                }
				                catch (Exception ex2)
				                {
				                    connectionLost = true;
				                    await DisplayAlert("Unexpected Error", ex2.Message, "Try again");
				                }
				                finally
				                {
				                    LoadingInProgress = false;
				                }
				            } 
				            while (connectionLost);
				        }
				        #endregion
				    }
				}
				#endregion
				
				#region CRUDListPageCore
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.CRUDList
				{
				    using ViewModels;
				    using Xamarin.Forms;
				    using System.Threading.Tasks;
				    using System;
				    using System.Net;
				    using Exceptions;
				    using Views;
				    using System.Collections.ObjectModel;
				    using Utils;
				    using Models;
				    using App;
				
				    public abstract class CRUDListPageCore<ModelT> : ContentPage, IHaveActivityIndicator where ModelT : class, ISupermodelListTemplate, IModel, new()
				    {
				        #region Initializers
				        public virtual CRUDListPageCore<ModelT> Init(string title, int take = 25, bool readOnly = false)
						{
				            InitContent(readOnly);
				
				            Title = title;
							Take = take;
				            
				            ListView.ListPanel.ContentView.ItemSelected += ItemSelectedHandler;
				            ListView.ListPanel.ContentView.ItemAppearing += ItemAppearingHandler;
				
				            if (!readOnly) ToolbarItems.Add(new ToolbarItem("New", NewBtnIconFilename, NewBtnClickedHandler));
				
				            return this;
				        }
				        #endregion
				
				        #region Event Handlers
				        public abstract void ItemAppearingHandler(object sender, ItemVisibilityEventArgs args);
				        public virtual async void ItemSelectedHandler(object sender, SelectedItemChangedEventArgs args)
				        {
				            if (args.SelectedItem == null) return;
				            var model = (ModelT)args.SelectedItem;
				            await OpenDetailInternalAsync(model);
				            ListView.ListPanel.ContentView.SelectedItem = null; //deselect row
				        }
				        public virtual async void DeleteItemHandler(object sender, EventArgs args)
				        {
				            bool connectionLost;
				            var model = (ModelT)((MenuItem)sender).CommandParameter;
				
				            do
				            {
				                connectionLost = false;
				                try
				                {
				                    using(new ActivityIndicatorFor(ListView.ListPanel))
				                    {
				                        if (await DeleteItemInternalAsync(model)) Models.RemoveAll(x => x.Id == model.Id);
				                    }
				                }
				                catch (SupermodelWebApiException ex1)
				                {
				                    if (ex1.StatusCode == HttpStatusCode.Unauthorized)
				                    {
				                        UnauthorizedHandler();
				                    }
				                    else if (ex1.StatusCode == HttpStatusCode.NotFound)
				                    {
				                        Models.RemoveAll(x => x.Id == model.Id);
				                        await DisplayAlert("Not Found", "Item you are trying to delete no longer exists.", "Ok");
				                    }
				                    else if (ex1.StatusCode == HttpStatusCode.Conflict)
				                    {
				                        await DisplayAlert("Unable to Delete", ex1.ContentJsonMessage, "Ok");
				                    }
				                    else if (ex1.StatusCode == HttpStatusCode.InternalServerError)
				                    {
				                        connectionLost = true;
				                        await DisplayAlert("Internal Server Error", ex1.ContentJsonMessage, "Ok");
				                    }
				                    else
				                    {
				                        connectionLost = true;
				                        await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
				                    }
				                }
				                catch (WebException)
				                {
				                    connectionLost = true;
				                    await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
				                }
				                catch (Exception ex2)
				                {
				                    connectionLost = true;
				                    await DisplayAlert("Unexpected Error", ex2.Message, "Try again");
				                }
				            } 
				            while (connectionLost);            
				        }
				        public virtual async void NewBtnClickedHandler()
				        {
				            var blankModel = new ModelT();
				            await OpenDetailInternalAsync(blankModel);
				        }
				        #endregion
				
				        #region IHaveActivityIndicator implementation
				        public async Task WaitForPageToBecomeActiveAsync()
				        {
				            while(!PageActive) await Task.Delay(25);
				        }
				        public bool ActivityIndicatorOn
				        {
				            get => ListView.ListPanel.ActivityIndicatorOn;
				            set => ListView.ListPanel.ActivityIndicatorOn = value;
				        }
				        public string Message
				        {
				            get => ListView.ListPanel.Message;
				            set => ListView.ListPanel.Message = value;
				        }
				        #endregion
				
				        #region Overrides
				        protected abstract void InitContent(bool readOnly);
				        protected abstract Task OpenDetailInternalAsync(ModelT model);
				        protected abstract Task<bool> DeleteItemInternalAsync(ModelT model);
				        protected virtual void UnauthorizedHandler()
				        {
				            FormsApplication.GetRunningApp().HandleUnauthorized();
				        }
				        protected virtual string NewBtnIconFilename => null;
				
				        protected override void OnAppearing()
				        {
				            base.OnAppearing();
				            PageActive = true;
				        }
				        protected override void OnDisappearing()
				        {
				            PageActive = false;
				            base.OnDisappearing();
				        }
				        #endregion
				
				        #region Properties
				        public StackLayout StackLayout { get; set; }
				        public CRUDListView<ModelT> ListView { get; set; }
				        public ObservableCollection<ModelT> Models { get; set; }
				        public int? Take { get; set; }
				
				        protected bool LoadingInProgress { get; set; }
				        protected bool LoadedAll { get; set; }
				
				        protected bool PageActive { get; set;}
				        #endregion
				    }
				}
				#endregion
				
				#region CRUDListView
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.CRUDList
				{
				    using System;
				    using Views;
				    using ViewModels;
				    using Xamarin.Forms;
				    
				    public class CRUDListView<ModelT> : StackLayout where ModelT : class, ISupermodelListTemplate, new()
				    {
				        #region Constructors
				        public CRUDListView(EventHandler deleteHandler, bool searchBar)
				        {
				            if (searchBar)
				            {
				                Spacing = 1;
				                SearchBar = new SearchBar { Placeholder = "Type your search term here" };
				                Children.Add(SearchBar);
				            }
				
				            ListPanel = new ViewWithActivityIndicator<ListView>(new ListView
				            {
				                ItemTemplate = new ModelT().GetListCellDataTemplate(deleteHandler),
				                VerticalOptions = LayoutOptions.FillAndExpand,
				                HorizontalOptions = LayoutOptions.FillAndExpand,
				                IsPullToRefreshEnabled = true
				            });
				            Children.Add(ListPanel);
				        }
				        #endregion
				
				        #region Properties
				        public readonly SearchBar SearchBar;
				        public readonly ViewWithActivityIndicator<ListView> ListPanel;
				        #endregion
				     }
				}
				#endregion
				
				#region EnhancedCRUDListPage
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.CRUDList
				{
				    using ViewModels;
				    using Xamarin.Forms;    
				    using System.Collections.ObjectModel;
				    using System.Threading.Tasks;
				    using System.Linq;
				    using System;
				    using System.Net;
				    using Exceptions;
				    using Views;
				    using System.Collections.Generic;
				    using Models;
				    using DataContext.Core;
				    using UnitOfWork;
				    using App;
				    using Repository;
				
				    public abstract class EnhancedCRUDListPage<ModelT, DataContextT> : CRUDListPageCore<ModelT> 
				        where ModelT : class, ISupermodelListTemplate, IModel, new()
				        where DataContextT : class, IDataContext, new()
				    {
				        #region Event Handlers
				        public virtual async void RunSearchHandler(object sender, TextChangedEventArgs args)
				        {
				            var searchTerm = ListView.SearchBar.Text;
				            await Task.Delay(750);
				            if (searchTerm != ListView.SearchBar.Text) return; //we must still be typing, let's wait for it to finish
				
				            await LoadListContentAsync(0, Take, searchTerm);
				        }
				        public override async void ItemAppearingHandler(object sender, ItemVisibilityEventArgs args)
				        {
				            if (LoadedAll || LoadingInProgress) return;
				            var searchTerm = ListView.SearchBar.Text;
				            if (Models.Last() == args.Item) await LoadListContentAsync(Models.Count, Take, searchTerm);
				        }
				        public virtual async void RefreshingHandler(object sender, EventArgs args)
				        {
				            Models = null;
				            await LoadListContentAsync(searchTerm: ListView.SearchBar.Text, showActivityIndicator: false);
				            ListView.ListPanel.ContentView.IsRefreshing = false;
				        }
				        #endregion
				
				        #region Overrides
				        protected override void InitContent(bool readOnly)
				        {
				            Content = StackLayout = new StackLayout();
				            if (readOnly) ListView = new CRUDListView<ModelT>(null, true);
				            else Content = ListView = new CRUDListView<ModelT>(DeleteItemHandler, true);
				            ListView.SearchBar.TextChanged += RunSearchHandler;
				            ListView.ListPanel.ContentView.Refreshing += RefreshingHandler;
				            StackLayout.Children.Add(ListView);
				        }
				        protected override async Task<bool> DeleteItemInternalAsync(ModelT model)
				        {
				            using (FormsApplication.GetRunningApp().NewUnitOfWork<DataContextT>())
				            {
				                model.Delete();
				                await UnitOfWorkContext.FinalSaveChangesAsync();
				                return true;
				            }
				        }
				        protected virtual async Task<List<ModelT>> GetItemsInternalAsync(int skip, int? take, string searchTerm)
				        {
				            using (FormsApplication.GetRunningApp().NewUnitOfWork<DataContextT>(ReadOnly.Yes))
				            {
				                var repo = RepoFactory.Create<ModelT>();
				                return await repo.GetWhereAsync(new { SearchTerm = searchTerm }, null, skip, take); //We asume SimpleSearchApiModel is used here
				            }
				        }
				        #endregion
				
				        #region Methods
				        public virtual async Task LoadListContentAsync(int skip = 0, int? take = -1, string searchTerm = null, bool showActivityIndicator = true)
				        {
				            if (take < 0) take = Take;
				            if (searchTerm == null) searchTerm = "";
				            
				            bool connectionLost;
				            do
				            {
				                connectionLost = false;
				                try
				                {
				                    LoadingInProgress = true;
				                    LoadedAll = false;
				                    
				                    if (Models == null) Models = new ObservableCollection<ModelT>();
				                    if (skip == 0) Models.Clear();
				                    
				                    if (showActivityIndicator)
				                    {
				                        using(new ActivityIndicatorFor(ListView.ListPanel))
				                        {
				                            var models = await GetItemsInternalAsync(skip, take, searchTerm);
				                            if (take == null || models.Count < take) LoadedAll = true;
				
				                            foreach (var model in models)
				                            {
				                                if (Models.All(x => x.Id != model.Id)) Models.Add(model);
				                            }
				                            ListView.ListPanel.ContentView.ItemsSource = Models;
				                        }
				                    }
				                    else
				                    {
				                        var models = await GetItemsInternalAsync(skip, take, searchTerm);
				                        if (take == null || models.Count < take) LoadedAll = true;
				
				                        foreach (var model in models)
				                        {
				                            if (Models.All(x => x.Id != model.Id)) Models.Add(model);
				                        }
				                        ListView.ListPanel.ContentView.ItemsSource = Models;
				                    }
				                }
				                catch (SupermodelWebApiException ex1)
				                {
				                    if (ex1.StatusCode == HttpStatusCode.Unauthorized)
				                    {
				                        UnauthorizedHandler();
				                    }
				                    else if (ex1.StatusCode == HttpStatusCode.InternalServerError)
				                    {
				                        connectionLost = true;
				                        await DisplayAlert("Internal Server Error", ex1.ContentJsonMessage, "Ok");
				                    }
				                    else
				                    {
				                        connectionLost = true;
				                        await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
				                    }
				                }
				                catch (WebException)
				                {
				                    connectionLost = true;
				                    await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
				                }
				                catch (Exception ex2)
				                {
				                    connectionLost = true;
				                    await DisplayAlert("Unexpected Error", ex2.Message, "Try again");
				                }
				                finally
				                {
				                    LoadingInProgress = false;
				                }
				            } 
				            while (connectionLost);
				        }
				        #endregion
				    }
				}
				#endregion
				
			#endregion
			
			#region Login
				
				#region BasicAuthHeaderGenerator
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.Login
				{
				    using Encryptor;
				    using System.Threading.Tasks;
				    using Exceptions;
				    using Xamarin.Forms;	
				    
				    public class BasicAuthHeaderGenerator : IAuthHeaderGenerator
				    {
				        #region Constructors
				        public BasicAuthHeaderGenerator(string username, string password, byte[] localStorageEncryptionKey = null)
				        {
				            Username = username;
				            Password = password;
				            LocalStorageEncryptionKey = localStorageEncryptionKey;
				        }
				        #endregion
								
				        #region Methods
				        public virtual AuthHeader CreateAuthHeader()
				        {
				            return HttpAuthAgent.CreateBasicAuthHeader(Username, Password);
				        }
				
				        public virtual void Clear()
				        {
				            Username = Password = "";
				        }
				        public virtual async Task ClearAndSaveToPropertiesAsync()
				        {
				            if (LocalStorageEncryptionKey == null) throw new SupermodelException("ClearAndSaveToPropertiesAsync(): LocalStorageEncryptionKey = null");
				
				            Clear();
				            Application.Current.Properties["smUsername"] = null;
				            Application.Current.Properties["smPasswordCode"] = null;
				            Application.Current.Properties["smPasswordIV"] = null;
				            await Application.Current.SavePropertiesAsync();
				        }
				        public virtual async Task SaveToAppPropertiesAsync()
				        {
				            if (LocalStorageEncryptionKey == null) throw new SupermodelException("SaveToAppPropertiesAsync(): LocalStorageEncryptionKey = null");
				
				            if  (string.IsNullOrWhiteSpace(Username) || string.IsNullOrWhiteSpace(Password)) throw new SupermodelException("string.IsNullOrWhiteSpace(Username) || string.IsNullOrWhiteSpace(Password)");
				
				            var passwordCode = EncryptorAgent.Lock(LocalStorageEncryptionKey, Password, out var passwordIV);
				
				            Application.Current.Properties["smUsername"] = Username;
				            Application.Current.Properties["smPasswordCode"] = passwordCode;
				            Application.Current.Properties["smPasswordIV"] = passwordIV;
				            Application.Current.Properties["smUserLabel"] = UserLabel;
				            Application.Current.Properties["smUserId"] = UserId;
				            await Application.Current.SavePropertiesAsync();
				        }
				        public virtual bool LoadFromAppProperties()
				        {
				            if (LocalStorageEncryptionKey == null) throw new SupermodelException("LoadFromAppProperties(): LocalStorageEncryptionKey = null");
				
				            if (Application.Current.Properties.ContainsKey("smUsername") && 
				                Application.Current.Properties.ContainsKey("smPasswordCode") && 
				                Application.Current.Properties.ContainsKey("smPasswordIV") && 
				                Application.Current.Properties.ContainsKey("smUserLabel") && 
				                Application.Current.Properties.ContainsKey("smUserId"))
				            {
				                if (!(Application.Current.Properties["smUsername"] is string username)) return false;
				                if (!(Application.Current.Properties["smPasswordCode"] is byte[] passwordCode)) return false;
				                if (!(Application.Current.Properties["smPasswordIV"] is byte[] passwordIV)) return false;
				
				                //User label and userId can be null
				                var userLabel = Application.Current.Properties["smUserLabel"] as string;
				                var userId = Application.Current.Properties["smUserId"] as long?;
				
				                Password = EncryptorAgent.Unlock(LocalStorageEncryptionKey, passwordCode, passwordIV);
				                Username = username;
				                UserLabel = userLabel;
				                UserId = userId;
				                return true;
				            }
				            else
				            {
				                return false;
				            }
				        }
				        #endregion
								
				        #region Properties
				        public long? UserId { get; set; }
				        public string UserLabel { get; set; }
				        public string Username { get; set; }
				        public string Password { get; set; }
				
				        private byte[] LocalStorageEncryptionKey { get; }
				        #endregion
				    }
				}
				#endregion
				
				#region IAuthHeaderGenerator
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.Login
				{
				    using System.Threading.Tasks;
				    using Encryptor;    
				    
				    public interface IAuthHeaderGenerator
				    {
				        long? UserId { get; set; }
				        string UserLabel { get; set; }
				        AuthHeader CreateAuthHeader();
				
				        void Clear();
				        Task ClearAndSaveToPropertiesAsync();
				        bool LoadFromAppProperties();
				        Task SaveToAppPropertiesAsync();
				    }
				}
				#endregion
				
				#region ILoginViewModel
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.Login
				{
				    using System.ComponentModel;
				
				    public interface ILoginViewModel : INotifyPropertyChanged
				    {
				        string GetValidationError();
				    }
				}
				#endregion
				
				#region LoginPageBase
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.Login
				{
				    using System.Threading.Tasks;
				    using DataContext.WebApi; 
				    using Models;
				    using UnitOfWork;
				    using App;
				
				    public abstract class LoginPageBase<LoginViewModelT, LoginViewT, LoginValidationModelT, WebApiDataContextT> : LoginPageCore<LoginViewModelT, LoginViewT>
				        where LoginViewModelT: ILoginViewModel, new()
				        where LoginViewT : LoginViewBase<LoginViewModelT>, new()
				        where LoginValidationModelT : class, IModel
				        where WebApiDataContextT : WebApiDataContext, new()
				    {
				        #region Overrides
				        public override async Task<LoginResult> TryLoginAsync()
				        {
				            using(FormsApplication.GetRunningApp().NewUnitOfWork<WebApiDataContextT>(ReadOnly.Yes))
				            {
				                return await UnitOfWorkContext.ValidateLoginAsync<LoginValidationModelT>();
				            }
				        }
				        #endregion
				    }
				}
				#endregion
				
				#region LoginPageCore
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.Login
				{
				    using System;
				    using System.Net;
				    using System.Threading.Tasks;
				    using DataContext.WebApi;
				    using Exceptions;
				    using Views;
				    using Xamarin.Forms;   
				    using App;
				
				    public abstract class LoginPageCore<LoginViewModelT, LoginViewT> : ContentPage, IHaveActivityIndicator
				        where LoginViewModelT: ILoginViewModel, new()
				        where LoginViewT : LoginViewBase<LoginViewModelT>, new()
				    {
				        #region Contructors
				        protected LoginPageCore()
				        {
				            Content = LoginView = new ViewWithActivityIndicator<LoginViewT>(new LoginViewT());
				            AutologinIfConnectionLost = false;
				            LoginView.ContentView.SetUpSignInClickedHandler(SignInClicked);
				        }
				        #endregion
				
				        #region Methods
				        public abstract Task<LoginResult> TryLoginAsync();
				        public abstract IAuthHeaderGenerator GetAuthHeaderGenerator(LoginViewModelT loginViewModel);
				        public IAuthHeaderGenerator GetBlankAuthHeaderGenerator()
				        {
				            var authHeader = GetAuthHeaderGenerator(new LoginViewModelT());
				            authHeader.Clear();
				            return authHeader;
				        }
				        public async Task AutologinIfPossibleAsync()
				        {
				            var authHeaderGenerator = GetBlankAuthHeaderGenerator();
				            if (authHeaderGenerator.LoadFromAppProperties())
				            {
				                bool connectionLost;
				                do
				                {
				                    connectionLost = false;
				                    try
				                    {
				                        using(var activityIndicator = new ActivityIndicatorFor(LoginView, "Logging you in..."))
				                        {
				                            FormsApplication.GetRunningApp().AuthHeaderGenerator = authHeaderGenerator;
				                            var loginResult = await TryLoginAsync();
				                            if (loginResult.LoginSuccessful)
				                            {
				                                if (!string.IsNullOrEmpty(loginResult.UserLabel)) activityIndicator.Element.Message = "Welcome, " + loginResult.UserLabel + "!";
				
				                                FormsApplication.GetRunningApp().AuthHeaderGenerator.UserId = loginResult.UserId; 
				                                FormsApplication.GetRunningApp().AuthHeaderGenerator.UserLabel = loginResult.UserLabel;
				
				                                await Task.Delay(800); //short delay so that the message can be read
				                                if (await DoLoginAsync(true, false)) await FormsApplication.GetRunningApp().AuthHeaderGenerator.SaveToAppPropertiesAsync();
				                            }
				                            else
				                            {
				                                await authHeaderGenerator.ClearAndSaveToPropertiesAsync();
				                                FormsApplication.GetRunningApp().AuthHeaderGenerator = null;
				                            }
				                        }
				                    }
				                    catch (SupermodelWebApiException ex1)
				                    {
				                        connectionLost = true;
				                        var result = await DisplayAlert("Server Error", ex1.ContentJsonMessage, "Cancel", "Try again");
				                        if (result) return;
				                    }
				                    catch (WebException)
				                    {
				                        if (AutologinIfConnectionLost)
				                        {
				                            await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Work Offline");
				                            using (new ActivityIndicatorFor(LoginView))
				                            {
				                                await DoLoginAsync(true, false);
				                            }
				                            return;
				                        }
				                        
				                        var result = await DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Cancel", "Try again");
				                        if (result) return;
				                    }
				                    catch (Exception ex3)
				                    {
				                        connectionLost = true;
				                        var result = await DisplayAlert("Unexpected Error", ex3.Message, "Cancel", "Try again");
				                        if (result) return;
				                    }
				                }
				                while(connectionLost);            
				            }
				        }
				        #endregion
				
				        #region Overrides
				        protected virtual void OnDisappearingBase()
				        {
				            // ReSharper disable once RedundantBaseQualifier
				            base.OnDisappearing();
				        }
				        protected virtual void OnAppearingBase()
				        {
				            // ReSharper disable once RedundantBaseQualifier
				            base.OnAppearing();
				        }
				        protected override async void OnAppearing()
				        {
				            PageActive = true;
				
				            if (_trueTitle != null)
				            {
				                Title = _trueTitle;
				                _trueTitle = null;
				            }
				            
				            //If Sign Out is clicked
				            if (_loggedIn)
				            {
				                var answer = await DisplayAlert("Alert", "Are you sure you want to sign out?", "Yes", "No");
				
				                if (answer)
				                {
				                    if (!await OnConfirmedLogOutAsync()) await DoLoginAsync(true, true);
				                }
				                else
				                {
				                    await DoLoginAsync(true, true);
				                }
				            }
				            OnAppearingBase();
				        }
				        protected override void OnDisappearing()
				        {
				            base.OnDisappearing();
				            PageActive = false;
				        }
				        protected virtual async Task<bool> OnConfirmedLogOutAsync()
				        {
				            await FormsApplication.GetRunningApp().AuthHeaderGenerator.ClearAndSaveToPropertiesAsync();
				            FormsApplication.GetRunningApp().AuthHeaderGenerator = null;
				            _loggedIn = false;
				            return true;
				        }
				        protected virtual async Task<bool> DoLoginAsync(bool autologin, bool isJumpBack)
				        {
				            _trueTitle = Title;
				            Title = "Sign Out";
				
				            var result = await OnSuccessfulLoginAsync(autologin, isJumpBack);
				
				            if (result) _loggedIn = true;
				            else Title = _trueTitle;
				
				            return result;
				        }
				        public abstract Task<bool> OnSuccessfulLoginAsync(bool autologin, bool isJumpBack);
				        #endregion
				
				        #region IHaveActivityIndicator implementation
				        public async Task WaitForPageToBecomeActiveAsync()
				        {
				            while(!PageActive) await Task.Delay(25);
				        }
				        public bool ActivityIndicatorOn
				        {
				            get => LoginView.ActivityIndicatorOn;
				            set => LoginView.ActivityIndicatorOn = value;
				        }
				        public string Message
				        {
				            get { return LoginView.Message; }
				            set { LoginView.Message = value; }
				        }
				        #endregion
				
				        #region Event Handlers
				        public virtual async void SignInClicked(object sender, EventArgs args)
				        {
				            var loginViewModel = LoginView.ContentView.ViewModel;
				            var validationError = loginViewModel.GetValidationError();
				            if (validationError != null)
				            {
				                FormsApplication.GetRunningApp().AuthHeaderGenerator = null;
				                await DisplayAlert("Login", validationError, "Ok");
				                return;
				            }
				
				            bool connectionLost;
				            do
				            {
				                connectionLost = false;
				                try
				                {
				                    using(var activityIndicator = new ActivityIndicatorFor(LoginView, "Logging you in..."))
				                    {
				                        FormsApplication.GetRunningApp().AuthHeaderGenerator = GetAuthHeaderGenerator(loginViewModel);
				                        var loginResult = await TryLoginAsync();
				                        if (loginResult.LoginSuccessful)
				                        {
				                            if (!string.IsNullOrEmpty(loginResult.UserLabel)) activityIndicator.Element.Message = "Welcome, " + loginResult.UserLabel + "!";
				
				                            FormsApplication.GetRunningApp().AuthHeaderGenerator.UserId = loginResult.UserId; 
				                            FormsApplication.GetRunningApp().AuthHeaderGenerator.UserLabel = loginResult.UserLabel;
				
				                            await Task.Delay(800); //short delay so that the message can be read
				                            if (await DoLoginAsync(false, false)) await FormsApplication.GetRunningApp().AuthHeaderGenerator.SaveToAppPropertiesAsync();
				                        }
				                        else
				                        {
				                            FormsApplication.GetRunningApp().AuthHeaderGenerator = null;
				                            await DisplayAlert("Unable to sign in", "Username and password combination provided is invalid. Please try again.", "Ok");
				                        }
				                    }
				                }
				                catch (SupermodelWebApiException ex1)
				                {
				                    connectionLost = true;
				                    var result = await DisplayAlert("Server Error", ex1.ContentJsonMessage, "Cancel", "Try again");
				                    if (result) return;
				                }
				                catch (WebException)
				                {
				                    connectionLost = true;
				                    var result = await DisplayAlert("Server Error", "Connection to the cloud cannot be established.", "Cancel", "Try again");
				                    if (result) return;
				                }
				                catch (Exception ex3)
				                {
				                    connectionLost = true;
				                    var result = await DisplayAlert("Unexpected Error", ex3.Message, "Cancel", "Try again");
				                    if (result) return;
				                }
				            }
				            while(connectionLost);
				        }
				        #endregion
				
				        #region Properties
				        public ViewWithActivityIndicator<LoginViewT> LoginView { get; set; }
				        public bool AutologinIfConnectionLost { get; set; }
				        
				        // ReSharper disable InconsistentNaming
				        protected bool _loggedIn;
				        protected string _trueTitle = "Sign In";
				        // ReSharper restore InconsistentNaming
				
				        protected bool PageActive { get; set; }
				        #endregion        
				    }
				}
				#endregion
				
				#region LoginViewBase
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.Login
				{
				    using System;
				    using Xamarin.Forms;
				    
				    public abstract class LoginViewBase<LoginViewModelT> : StackLayout where LoginViewModelT: ILoginViewModel, new()
				    {
				        #region Methods
				        public abstract void SetUpSignInClickedHandler(EventHandler handler);
				        #endregion
				
				        #region Properties
				        public LoginViewModelT ViewModel { get; set; }
				        #endregion
				    }
				}
				#endregion
				
				#region UsernameAndPasswordLoginPage
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.Login
				{
				    using DataContext.WebApi;
				    using Models;    
				    
				    public abstract class UsernameAndPasswordLoginPage<LoginValidationModelT, WebApiDataContextT> : LoginPageBase<UsernameAndPasswordLoginViewModel, UsernameAndPasswordLoginView, LoginValidationModelT, WebApiDataContextT>
				        where LoginValidationModelT : class, IModel
				        where WebApiDataContextT : WebApiDataContext, new()
				    {}
				}
				#endregion
				
				#region UsernameAndPasswordLoginView
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.Login
				{
				    using System;
				    using Xamarin.Forms;    
				    
				    public class UsernameAndPasswordLoginView : LoginViewBase<UsernameAndPasswordLoginViewModel>
				    {
				        #region Constructors
				        public UsernameAndPasswordLoginView()
				        {
				            VerticalOptions = LayoutOptions.FillAndExpand;
				            HorizontalOptions = LayoutOptions.FillAndExpand;
				            Padding = new Thickness(15, 15);
				
				            BindingContext = ViewModel = new UsernameAndPasswordLoginViewModel();
				
				            Username = new Entry { Placeholder = "Username", Keyboard = Keyboard.Email };
				            Username.SetBinding(Entry.TextProperty, "Username");
				            Children.Add(Username);
				
				            Password = new Entry { Placeholder = "Password", IsPassword = true };
				            Password.SetBinding(Entry.TextProperty, "Password");
				            Children.Add(Password);
				
				            SignInButton = new Button { Text = "Sign In" };
				            
				            Children.Add(SignInButton);
				        }
				        #endregion
				
				        #region Methods
				        public virtual void SetUpLoginImage(Image image)
				        {
				            Children.Insert(0, image);
				        }
				        public override void SetUpSignInClickedHandler(EventHandler handler)
				        {
				            SignInButton.Clicked += handler;
				        }
				        #endregion
				
				        #region Properties
				        public Entry Username { get; set; }
				        public Entry Password { get; set; }
				        public Button SignInButton { get; set; }
				        #endregion
				    }
				}
				#endregion
				
				#region UsernameAndPasswordLoginViewModel
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.Pages.Login
				{
				    using App;
				    using System.ComponentModel;
				    using System.Runtime.CompilerServices;
				    
				    public class UsernameAndPasswordLoginViewModel : ILoginViewModel	
				    {
				        #region InotifyPropertyChanged implementation
				        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
				        {
				            var handler = PropertyChanged;
				            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
				        }
				        public event PropertyChangedEventHandler PropertyChanged;
				        #endregion
				
				        #region Overrdies
				        public virtual IAuthHeaderGenerator GetAuthHeaderGenerator()
				        {
				            return new BasicAuthHeaderGenerator(Username, Password, FormsApplication.GetRunningApp().LocalStorageEncryptionKey);
				        }
				        #endregion
				
				        #region Methods
				        public virtual string GetValidationError()
				        {
				            if  (string.IsNullOrWhiteSpace(Username) || string.IsNullOrWhiteSpace(Password)) return "Please enter valid login credentials.";
				            else return null;
				        }
				        #endregion
				
				        #region Properties
				        public string Username
				        {
				            get { return _username; }
				            set
				            {
				                if (value == _username) return;
				                _username = value;
				                OnPropertyChanged();
				            }
				        }
				        private string _username;
				
				        public string Password
				        {
				            get { return _password; }
				            set
				            {
				                if (value == _password) return;
				                _password = value;
				                OnPropertyChanged();
				            }
				        }
				        private string _password;
				
				        public string UserLabel
				        {
				            get { return _userLabel; }
				            set
				            {
				                if (value == _userLabel) return;
				                _userLabel = value;
				                OnPropertyChanged();
				            }
				        }
				        private string _userLabel;
				
				        public long? UserId
				        {
				            get { return _userId; }
				            set
				            {
				                if (value == _userId) return;
				                _userId = value;
				                OnPropertyChanged();
				            }
				        }
				        private long? _userId;
				        #endregion
				    }
				}
				#endregion
				
			#endregion
			
		#endregion
		
		#region UIComponents
			
			#region Base
				
				#region BinaryFilesReadOnlyXFModel
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using System;
				    using System.Collections.Generic;
				    using System.Collections.ObjectModel;
				    using Exceptions;
				    using ReflectionMapper;
				    using Xamarin.Forms;
				    using ViewModels;
				
				    public abstract class BinaryFilesReadOnlyXFModel : IReadOnlyUIComponentXFModel, IRMapperCustom
				    {
				        #region Constructors
				        protected BinaryFilesReadOnlyXFModel()
				        {
				            ModelsWithBinaryFileXFModels = new ObservableCollection<ModelWithBinaryFileXFModel>();
				        }
				        #endregion
				
				        #region Custom Mapper implementation
				        public virtual object MapFromObjectCustom(object obj, Type objType)
				        {
				            if (!(obj is IEnumerable<IModelWithBinaryFile>)) throw new SupermodelException(GetType().Name + " can only map to Lists of type that implements IModelWithBinaryFile");
				            var modelsWithImage = (IEnumerable<IModelWithBinaryFile>)obj;
				            
				            ModelsWithBinaryFileXFModels = ModelsWithBinaryFileXFModels ?? new ObservableCollection<ModelWithBinaryFileXFModel>();
				            ModelsWithBinaryFileXFModels.Clear();
				            foreach (var modelWithImage in modelsWithImage) ModelsWithBinaryFileXFModels.Add(new ModelWithBinaryFileXFModel().MapFrom(modelWithImage));
				            return this;
				        }
				        public virtual object MapToObjectCustom(object obj, Type objType)
				        {
				            //its read-only, so we do nothing
				            return obj; 
				        }
				        #endregion
				
				        #region Methods
				        public abstract List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue);
				        #endregion
				
				        #region Properties
				        public ObservableCollection<ModelWithBinaryFileXFModel> ModelsWithBinaryFileXFModels { get; private set; }
				
				        public abstract bool ShowDisplayNameIfApplies { get; set; }
				        public abstract string DisplayNameIfApplies { get; set; }
				        public abstract TextAlignment TextAlignmentIfApplies { get; set; }
				        #endregion
				    }
				}
				#endregion
				
				#region BinaryFilesWritableXFModel
				 // ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using System;
				    using System.Collections.Generic;
				    using System.Linq;
				    using ReflectionMapper;
				    
				    public abstract class BinaryFilesWritableXFModel : BinaryFilesReadOnlyXFModel, IWritableUIComponentXFModel
				    {
				        #region Custom Mapper implementation
				        public override object MapToObjectCustom(object obj, Type objType)
				        {
				            var modelsWithImage = (IEnumerable<IModelWithBinaryFile>)obj;
				
				            // modelsWithImages == null during validation
				            if (modelsWithImage != null)
				            {
				                //remove all
				                modelsWithImage.ExecuteMethod("Clear");
				
				                //add all the images for which an exact match was not found
				                foreach (var modelWithBinaryFileXFModel in ModelsWithBinaryFileXFModels)
				                {
				                    var modelsWithImageUnderlyingType = obj.GetType().GenericTypeArguments[0];
				                    var modelWithImage = (IModelWithBinaryFile)ReflectionHelper.CreateType(modelsWithImageUnderlyingType);
				                    modelsWithImage.ExecuteMethod("Add", modelWithImage);
				
				                    //update BinaryFile
				                    modelWithBinaryFileXFModel.MapTo(modelWithImage);
				                }
				            }
				            return obj;
				        }
				        #endregion
				
				        #region Properties
				        public abstract string ErrorMessage { get; set; }
				        public abstract bool Required { get; set; }
				        public object WrappedValue => ModelsWithBinaryFileXFModels.FirstOrDefault();
				        #endregion
				    }
				}
				#endregion
				
				#region IHaveTextProperty
				 // ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    public interface IHaveTextProperty
				    {
				        string Text { get; set; }
				    }
				}
				#endregion
				
				#region IModelWithBinaryFile
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using Models;
				
				    public interface IModelWithBinaryFile
				    {
				        long Id { get; set; }
				        string GetTitle();
				        void SetTitle(string value);
				        BinaryFile GetBinaryFile();
				        void SetBinaryFile(BinaryFile value);
				    }
				}
				#endregion
				
				#region IReadOnlyUIComponentXFModel
				 // ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using ViewModels;
				    using Xamarin.Forms;    
				    
				    public interface  IReadOnlyUIComponentXFModel : ISupermodelMobileDetailTemplate
				    {
				        bool ShowDisplayNameIfApplies { get; set; }
				        string DisplayNameIfApplies { get; set; }
				        TextAlignment TextAlignmentIfApplies { get; set; }
				    }
				}
				#endregion
				
				#region IWritableUIComponentXFModel
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using Xamarin.Forms;
				
				    public interface IWritableUIComponentXFModel : IReadOnlyUIComponentXFModel
				    {
				        string ErrorMessage { get; set; }
				        bool Required { get; set; }
				        object WrappedValue { get; }
				    }
				}
				#endregion
				
				#region ModelWithBinaryFileXFModel
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using ReflectionMapper;
				    using ViewModels;
				    using System;
				    using Models;
				    
				    public class ModelWithBinaryFileXFModel : IRMapperCustom
				    {
				        #region IRMapperCustom implementation
				        public object MapFromObjectCustom(object obj, Type objType)
				        {
				            var modelWithBinaryFile = (IModelWithBinaryFile)obj;
				            Title = modelWithBinaryFile.GetTitle();
				            BinaryFile = new BinaryFileXFModel { Name = modelWithBinaryFile.GetBinaryFile().Name, BinaryContent = modelWithBinaryFile.GetBinaryFile().BinaryContent };
				            return this.MapFromObjectCustomBase(obj);
				        }
				        public object MapToObjectCustom(object obj, Type objType)
				        {
				            var modelWithBinaryFile = (IModelWithBinaryFile)obj;
				            modelWithBinaryFile.SetTitle(Title);
				            modelWithBinaryFile.SetBinaryFile(new BinaryFile { Name = BinaryFile.Name, BinaryContent = BinaryFile.BinaryContent });
				            return this.MapToObjectCustomBase(obj);
				        }
				        #endregion
				
				        #region Properties
				        public long Id { get; set; }
				        [NotRMapped] public string Title { get; set; }
				        [NotRMapped] public BinaryFileXFModel BinaryFile { get; set; }
				        #endregion
				    }
				}
				#endregion
				
				#region SingleCellReadOnlyUIComponentForTextXFModel
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using System;
				
				    public abstract class SingleCellReadOnlyUIComponentForTextXFModel : SingleCellReadOnlyUIComponentXFModel, IHaveTextProperty
				    {
				        #region ICustomMapper implemtation
				        public override object MapFromObjectCustom(object obj, Type objType)
				        {
				            return SingleCellUIComponentForTextXFModelCommonLib.MapFromObjectCustom(this, obj, objType);
				        }
				        public override object MapToObjectCustom(object obj, Type objType)
				        {
				            return SingleCellUIComponentForTextXFModelCommonLib.MapToObjectCustom(this, obj, objType);
				        }
				        #endregion
				        
				        #region Properties
				        public abstract string Text { get; set; }
				        #endregion
				    }
				}
				#endregion
				
				#region SingleCellReadOnlyUIComponentWithoutBackingXFModel
				 // ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using XForms;
				    using System.Collections.Generic;
				    using Xamarin.Forms;    
				    
				    public abstract class SingleCellReadOnlyUIComponentWithoutBackingXFModel : ViewCell, IReadOnlyUIComponentXFModel
				    {
				        #region Constructors
				        protected SingleCellReadOnlyUIComponentWithoutBackingXFModel()
				        {
				            View = new StackLayout
				            {
				                Padding = new Thickness(8, 0, 8, 0), 
				                Orientation = StackOrientation.Horizontal, 
				                VerticalOptions = LayoutOptions.CenterAndExpand, 
				                HorizontalOptions = LayoutOptions.FillAndExpand,
				                HeightRequest = 40,
				            };
				
				            DisplayNameLabel = new Label { HorizontalOptions = LayoutOptions.Start, VerticalOptions = LayoutOptions.Center, TextColor = XFormsSettings.LabelTextColor, LineBreakMode = LineBreakMode.NoWrap, FontSize = XFormsSettings.LabelFontSize };
				
				            LabelView = new StackLayout
				            {
				                Padding = new Thickness(0, 0, 0, 0),
				                Orientation = StackOrientation.Horizontal,
				                VerticalOptions = LayoutOptions.CenterAndExpand,
				                HorizontalOptions = LayoutOptions.Start,
				                HeightRequest = 40,
				                Children = { DisplayNameLabel }
				            };
				
				            if (ShowDisplayNameIfApplies) StackLayoutView.Children.Add(LabelView);
				        }
				        #endregion
				
				        #region ISupermodelMobileDetailTemplate implemetation
				        public List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue)
				        {
				            ParentPage = parentPage;
				            return new List<Cell> { this };
				        }
				        #endregion
				
				        #region Properties
				        public StackLayout StackLayoutView
				        {
				            get => (StackLayout)View;
				            set => View = value;
				        }
				        public StackLayout LabelView { get; set; }
				
				        public bool ShowDisplayNameIfApplies
				        {
				            get => _showDisplayNameIfApplies;
				            set
				            {
				                if (_showDisplayNameIfApplies == value) return;
				                if (value) StackLayoutView.Children.Insert(0, DisplayNameLabel);
				                else StackLayoutView.Children.RemoveAt(StackLayoutView.Children.IndexOf(DisplayNameLabel));
				                _showDisplayNameIfApplies = value;
				            }
				        }
				        private bool _showDisplayNameIfApplies = true;
				
				        public string DisplayNameIfApplies
				        {
				            get => DisplayNameLabel.Text;
				            set => DisplayNameLabel.Text = value;
				        }
				        public Label DisplayNameLabel { get; set; }
				
				        public Page ParentPage { get; set; }
				
				        public abstract TextAlignment TextAlignmentIfApplies { get; set; }
				        #endregion    
				    }
				}
				#endregion
				
				#region SingleCellReadOnlyUIComponentXFModel
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using System;
				    using ReflectionMapper;
				    
				    public abstract class SingleCellReadOnlyUIComponentXFModel : SingleCellReadOnlyUIComponentWithoutBackingXFModel, IRMapperCustom
				    {
				        #region ICustomMapper implemtation
				        public abstract object MapFromObjectCustom(object obj, Type objType);
				        public abstract object MapToObjectCustom(object obj, Type objType);
				        #endregion        
				    }
				}
				#endregion
				
				#region SingleCellUIComponentForTextXFModelCommonLib
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using System;
				    using ReflectionMapper;    
				    
				    public static class SingleCellUIComponentForTextXFModelCommonLib
				    {
				        #region ICustomMapper Common Implemtation for Text
				        public static object MapFromObjectCustom(IHaveTextProperty me, object obj, Type objType)
				        {
				            if (objType != typeof(string) && objType != typeof(int)  && objType != typeof(long)  && objType != typeof(double)  && objType != typeof(float)
				                && objType != typeof(int?) && objType != typeof(long?) && objType != typeof(double?) && objType != typeof(float?))
				            {
				                throw new PropertyCantBeAutomappedException($"{me.GetType().Name} can't be automapped to {objType.Name}");
				            }
				
				            var domainObjStr = obj?.ToString();
				            if (domainObjStr != null) me.Text = domainObjStr;
				
				            return me;
				        }
				        public static object MapToObjectCustom(IHaveTextProperty me, object obj, Type objType)
				        {
				            //If both Text and domain property are blank, return the current state
				            var domainObjStr = obj?.ToString();
				            if (string.IsNullOrEmpty(me.Text) && string.IsNullOrEmpty(domainObjStr)) return obj;
				            
				            if (objType == typeof(string)) return me.Text;
				
				            try
				            {
				                if (objType == typeof(int)) return int.Parse(me.Text);
				                if (objType == typeof(int?)) return string.IsNullOrEmpty(me.Text) ? (int?)null : int.Parse(me.Text);
				
				                if (objType == typeof(long)) return long.Parse(me.Text);
				                if (objType == typeof(long?)) return string.IsNullOrEmpty(me.Text) ? (long?)null : long.Parse(me.Text);
				            
				                if (objType == typeof(double)) return double.Parse(me.Text);
				                if (objType == typeof(double?)) return string.IsNullOrEmpty(me.Text) ? (double?)null : double.Parse(me.Text);
				
				                if (objType == typeof(float)) return float.Parse(me.Text);
				                if (objType == typeof(float?)) return string.IsNullOrEmpty(me.Text) ? (float?)null : float.Parse(me.Text);
				            }
				            catch (FormatException)
				            {
				                throw new ValidationResultException("Invalid Format");
				            }
				            catch(OverflowException)
				            {
				                throw new ValidationResultException("Invalid Format");
				            }
				
				            throw new PropertyCantBeAutomappedException($"{me.GetType().Name} can't be automapped to {objType.Name}");
				        }
				        #endregion
				    }
				}
				#endregion
				
				#region SingleCellWritableUIComponentForTextXFModel
				 // ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using System;
				
				    public abstract class SingleCellWritableUIComponentForTextXFModel : SingleCellWritableUIComponentXFModel, IHaveTextProperty
				    {
				        #region ICustomMapper implemtation
				        public override object MapFromObjectCustom(object obj, Type objType)
				        {
				            return SingleCellUIComponentForTextXFModelCommonLib.MapFromObjectCustom(this, obj, objType);
				        }
				        public override object MapToObjectCustom(object obj, Type objType)
				        {
				            return SingleCellUIComponentForTextXFModelCommonLib.MapToObjectCustom(this, obj, objType);
				        }
				        #endregion
				
				        #region Properties
				        public abstract string Text { get; set; }
				        public override object WrappedValue => Text;
				        #endregion
				    }
				}
				#endregion
				
				#region SingleCellWritableUIComponentWithoutBackingXFModel
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using System;
				    using Xamarin.Forms;
				
				    public abstract class SingleCellWritableUIComponentWithoutBackingXFModel : SingleCellReadOnlyUIComponentWithoutBackingXFModel, IWritableUIComponentXFModel
				    {
				        #region Constructors
				        protected SingleCellWritableUIComponentWithoutBackingXFModel()
				        {
				            ValidationErrorIndicator = new Button{ Text = "!", TextColor = Color.Red };
				            ValidationErrorIndicator.Clicked += ValidationIndicatorClicked;
				
				            RequiredFieldIndicator = new Label { HorizontalOptions = LayoutOptions.Start, VerticalOptions = LayoutOptions.Center, TextColor = XFormsSettings.RequiredAsteriskColor, Text = "*" };
				        }
				        #endregion
				
				        #region Event Handlers
				        public async void ValidationIndicatorClicked(object sender, EventArgs args)
				        {
				            if (ParentPage != null) await ParentPage.DisplayAlert("", ErrorMessage, "Ok");
				        }
				        #endregion
				
				        #region Properties
				        public Button ValidationErrorIndicator { get; set; }
				        public Label RequiredFieldIndicator { get; set; }
				        public bool Required
				        {
				            get => _required;
				            set
				            {
				                if (!_required && value) LabelView.Children.Insert(1, RequiredFieldIndicator);
				                if (_required && !value) LabelView.Children.Remove(RequiredFieldIndicator);
				                _required = value;
				            }
				        }
				        private bool _required;
				        
				        public string ErrorMessage
				        {
				            get
				            {
				                return _errorMessage;
				            }
				            set
				            {
				                if (_errorMessage == null & value != null) StackLayoutView.Children.Add(ValidationErrorIndicator);
				                if (_errorMessage != null & value == null)
				                {
				                    //this throws NullRefernceException on Andorid, ignore it
				                    try { StackLayoutView.Children.Remove(ValidationErrorIndicator); } catch (NullReferenceException) { }
				                }
				                _errorMessage = value;
				            }
				        }
				        private string _errorMessage;
				
				        public abstract object WrappedValue { get; }
				        #endregion    
				    }
				}
				#endregion
				
				#region SingleCellWritableUIComponentXFModel
				 // ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.Base
				{
				    using System;
				    using ReflectionMapper;
				
				    public abstract class SingleCellWritableUIComponentXFModel : SingleCellWritableUIComponentWithoutBackingXFModel, IRMapperCustom
				    {
				        #region ICustomMapper implemtation
				        public abstract object MapFromObjectCustom(object obj, Type objType);
				        public abstract object MapToObjectCustom(object obj, Type objType);
				        #endregion        
				    }
				}
				#endregion
				
			#endregion
			
			#region CustomCells
				
				#region AddNewCell
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomCells
				{
				    using System;
				    using Xamarin.Forms;
				    
				    public class AddNewCell : ViewCell
				    {
				        #region Constructors
				        public AddNewCell(string imageFileName)
				        {
				            View = new StackLayout
				            {
				                Padding = new Thickness(8, 0, 8, 0), 
				                Orientation = StackOrientation.Horizontal, 
				                VerticalOptions = LayoutOptions.CenterAndExpand, 
				                HorizontalOptions = LayoutOptions.FillAndExpand,
				                HeightRequest = 40,
				            };
				
				            if (imageFileName != null)
				            {
				                AddNewImage = new Image { Source = imageFileName };
				                StackLayoutView.Children.Add(AddNewImage);
				            }
				
				            AddNewLabel = new Label { HorizontalOptions = LayoutOptions.Start, VerticalOptions = LayoutOptions.Center, Text = "Add New", FontSize = XFormsSettings.LabelFontSize }; 
				            if (Device.OS == TargetPlatform.iOS) AddNewLabel.TextColor = Color.FromHex("#007AFF");
				            StackLayoutView.Children.Add(AddNewLabel);
				
				            ValidationErrorIndicator = new Button{ Text = "!", TextColor = Color.Red, HorizontalOptions = LayoutOptions.EndAndExpand };
				            ValidationErrorIndicator.Clicked += ValidationIndicatorClicked;
				
				            RequiredFieldIndicator = new Label { HorizontalOptions = LayoutOptions.Start, VerticalOptions = LayoutOptions.Center, TextColor = Color.Red, Text = "*" };
				        }
				        #endregion
				
				        #region Event Handlers
				        public async void ValidationIndicatorClicked(object sender, EventArgs args)
				        {
				            if (ParentPage != null) await ParentPage.DisplayAlert("", ErrorMessage, "Ok");
				        }
				        #endregion
				
				        #region Properties
				        public Page ParentPage { get; set; }
				        
				        public StackLayout StackLayoutView
				        {
				            get => (StackLayout)View;
				            set => View = value;
				        }
				
				        public string Text
				        {
				            get => AddNewLabel.Text;
				            set => AddNewLabel.Text = value;
				        }
				
				        public Label AddNewLabel { get; }
				        public Image AddNewImage { get; }
				
				        public Button ValidationErrorIndicator { get; set; }
				        public Label RequiredFieldIndicator { get; set; }
				        public bool Required
				        {
				            get => _required;
				            set
				            {
				                if (!_required && value) StackLayoutView.Children.Insert(0, RequiredFieldIndicator);
				                if (_required && !value) StackLayoutView.Children.Remove(RequiredFieldIndicator);
				                _required = value;
				            }
				        }
				        private bool _required;
				        
				        public string ErrorMessage
				        {
				            get => _errorMessage;
				            set
				            {
				                if (_errorMessage == null & value != null) StackLayoutView.Children.Add(ValidationErrorIndicator);
				                if (_errorMessage != null & value == null) StackLayoutView.Children.Remove(ValidationErrorIndicator);
				                _errorMessage = value;
				            }
				        }
				        private string _errorMessage;
				        #endregion
				    }
				}
				#endregion
				
				#region ImageCellWithEditableText
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomCells
				{
				    using Xamarin.Forms;
				    using CustomControls;
				    using XForms;
				
				    public class ImageCellWithEditableText : ViewCell
				    {
				        #region Contructors
				        public ImageCellWithEditableText()
				        {
				            View = new StackLayout
				            {
				                Padding = new Thickness(8, 0, 8, 0), 
				                Orientation = StackOrientation.Horizontal, 
				                VerticalOptions = LayoutOptions.CenterAndExpand, 
				                HorizontalOptions = LayoutOptions.FillAndExpand,
				                HeightRequest = 40,
				            };
				            
				            Image = new Image { HeightRequest = 40, Aspect = Aspect.AspectFit };
				            
				            TextEntry = new ExtEntry { HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, Border = false, TextAlignment = TextAlignment.End, WidthRequest = 1, FontSize = XFormsSettings.LabelFontSize, TextColor = XFormsSettings.ValueTextColor };
				            TextEntry.SetBinding(Entry.TextProperty, "Text");
				            
				            StackLayoutView.Children.Add(Image);
				            StackLayoutView.Children.Add(TextEntry);
				        }
				        #endregion
				
				        #region Properties
				        public StackLayout StackLayoutView
				        {
				            get => (StackLayout)View;
				            set => View = value;
				        }
				        
				        public ImageSource ImageSource
				        {
				            get => Image.Source;
				            set => Image.Source = value;
				        }
				        public Image Image { get; set; }
				
				        public virtual string Text
				        {
				            get => TextEntry.Text;
				            set
				            {
				                if (value == TextEntry.Text) return;
				                TextEntry.Text = value;
				                OnPropertyChanged();
				            }
				        }
				        public ExtEntry TextEntry { get; set; }
				
				        public string Placeholder
				        {
				            get => TextEntry.Placeholder;
				            set => TextEntry.Placeholder = value;
				        }
				        #endregion
				    }
				}
				#endregion
				
			#endregion
			
			#region CustomControls
				
				#region ExtDatePicker
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
				    using Xamarin.Forms;
				    
				    public class ExtDatePicker : DatePicker
				    {
				        #region Contructors
				        public ExtDatePicker()
				        {
				            Border = true;
				        }
				        #endregion
				        
				        #region Properties
				        public TextAlignment TextAlignment
				        {
				            get => _textAlign;
				            set
				            {
				                if ( value == _textAlign) return;
				                _textAlign = value;
				                OnPropertyChanged();
				            }
				        }
				        private TextAlignment _textAlign;
				        
				        public bool Border
				        {
				            get => _border;
				            set
				            {
				                if ( value == _border) return;
				                _border = value;
				                OnPropertyChanged();
				            }
				        }
				        private bool _border;
				        #endregion    
				    }
				}
				#endregion
				
				#region ExtDatePickerRenderer
				#if __IOS__
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
				    using Xamarin.Forms.Platform.iOS;
				    using Xamarin.Forms;
				    using UIKit;
				    
				    public class ExtDatePickerRenderer : DatePickerRenderer
				    {
				        protected void UpdateControl()
				        {
				            if (Control != null && Element != null)
				            {
				                Control.BorderStyle = UITextBorderStyle.None;
				                var element = (ExtDatePicker)Element;
				                switch (element.TextAlignment)
				                {
				                    case TextAlignment.Start: Control.TextAlignment = UITextAlignment.Left; break;
				                    case TextAlignment.End: Control.TextAlignment = UITextAlignment.Right; break;
				                    case TextAlignment.Center: Control.TextAlignment = UITextAlignment.Center; break;
				                }
				            }
				        }
				        
				        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
				        {
				            base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
				            UpdateControl();
				        }
				
				        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
				        {
				            base.OnElementChanged(e);
				            UpdateControl();
				        }
				    }
				}
				#endif
				
				#if __ANDROID__
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
				    using Xamarin.Forms.Platform.Android;
				    using Xamarin.Forms;
				    using Android.Views;
				    
				    public class ExtDatePickerRenderer : DatePickerRenderer
				    {
				        protected void UpdateControl()
				        {
				            if (Control != null && Element != null)
				            {
				                //Android elements are already borderless by default
				                var element = (ExtDatePicker)Element;
				                switch (element.TextAlignment)
				                {
				                    case Xamarin.Forms.TextAlignment.Start: Control.Gravity = GravityFlags.Left; break;
				                    case Xamarin.Forms.TextAlignment.End: Control.Gravity = GravityFlags.Right; break;
				                    case Xamarin.Forms.TextAlignment.Center: Control.Gravity = GravityFlags.Center; break;
				                }
				            }
				        }
				        
				        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
				        {
				            base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
				            UpdateControl();
				        }
				
				        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
				        {
				            base.OnElementChanged(e);
				            UpdateControl();
				        }
				    }
				}
				#endif
				
				#if WINDOWS_UWP
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
				    using Xamarin.Forms.Platform.UWP;
				    using Windows.UI.Xaml;
				    using Xamarin.Forms;
				
				    public class ExtDatePickerRenderer : DatePickerRenderer
				    {
				        protected void UpdateControl()
				        {
				            if (Control != null && Element != null)
				            {
				                //Android elements are already borderless by default
				                var element = (ExtDatePicker)Element;
				                //switch (element.TextAlignment)
				                //{
				                //    case Xamarin.Forms.TextAlignment.Start: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Left; break;
				                //    case Xamarin.Forms.TextAlignment.End: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Right; break;
				                //    case Xamarin.Forms.TextAlignment.Center: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Center; break;
				                //}
				                Control.BorderThickness = new Windows.UI.Xaml.Thickness(element.Border ? 1 : 0);
				            }
				        }
				        
				        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
				        {
				            base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
				            UpdateControl();
				        }
				
				        protected override void OnElementChanged(ElementChangedEventArgs<DatePicker> e)
				        {
				            base.OnElementChanged(e);
				            UpdateControl();
				        }
				    }
				}
				#endif
				
				
				
				#endregion
				
				#region ExtEntry
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
				    using Xamarin.Forms;
				
				    public class ExtEntry : Entry
				    {
				        #region Contructors
				        public ExtEntry()
				        {
				            Border = true;
				        }
				        #endregion
				        
				        #region Properties
				        public TextAlignment TextAlignment
				        {
				            get => _textAlign;
				            set
				            {
				                if ( value == _textAlign) return;
				                _textAlign = value;
				                OnPropertyChanged();
				            }
				        }
				        private TextAlignment _textAlign;
				        
				        public bool Border
				        {
				            get => _border;
				            set
				            {
				                if ( value == _border) return;
				                _border = value;
				                OnPropertyChanged();
				            }
				        }
				        private bool _border;
				        #endregion
				    }
				}
				#endregion
				
				#region ExtEntryRenderer
				#if __IOS__
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
				    using Xamarin.Forms.Platform.iOS;
				    using Xamarin.Forms;
				    using UIKit;
				    
				    public class ExtEntryRenderer : EntryRenderer
				    {
				        protected void UpdateControl()
				        {
				            if (Control != null && Element != null)
				            {
				                Control.BorderStyle = UITextBorderStyle.None;
				                var element = (ExtEntry)Element;
				                switch (element.TextAlignment)
				                {
				                    case TextAlignment.Start: Control.TextAlignment = UITextAlignment.Left; break;
				                    case TextAlignment.End: Control.TextAlignment = UITextAlignment.Right; break;
				                    case TextAlignment.Center: Control.TextAlignment = UITextAlignment.Center; break;
				                }
				            }
				        }
				        
				        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
				        {
				            base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
				            UpdateControl();
				        }
				
				        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
				        {
				            base.OnElementChanged(e);
				            UpdateControl();
				        }
				    }
				}
				#endif
				
				#if __ANDROID__
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
				    using Xamarin.Forms.Platform.Android;
				    using Xamarin.Forms;
				    using Android.Views;
				    
				    public class ExtEntryRenderer : EntryRenderer
				    {
				        protected void UpdateControl()
				        {
				            if (Control != null && Element != null)
				            {
				                //Android elements are already borderless by default
				                var element = (ExtEntry)Element;
				                switch (element.TextAlignment)
				                {
				                    case Xamarin.Forms.TextAlignment.Start: Control.Gravity = GravityFlags.Left; break;
				                    case Xamarin.Forms.TextAlignment.End: Control.Gravity = GravityFlags.Right; break;
				                    case Xamarin.Forms.TextAlignment.Center: Control.Gravity = GravityFlags.Center; break;
				                }
				            }
				        }
				        
				        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
				        {
				            base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
				            UpdateControl();
				        }
				
				        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
				        {
				            base.OnElementChanged(e);
				            UpdateControl();
				        }
				    }
				}
				#endif
				
				#if WINDOWS_UWP
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
				    using Xamarin.Forms.Platform.UWP;
				    using Windows.UI.Xaml;
				    using Xamarin.Forms;
				
				    public class ExtEntryRenderer : EntryRenderer
				    {
				        protected void UpdateControl()
				        {
				            if (Control != null && Element != null)
				            {
				                //Android elements are already borderless by default
				                var element = (ExtEntry)Element;
				                switch (element.TextAlignment)
				                {
				                    case Xamarin.Forms.TextAlignment.Start: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Left; break;
				                    case Xamarin.Forms.TextAlignment.End: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Right; break;
				                    case Xamarin.Forms.TextAlignment.Center: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Center; break;
				                }
				                Control.BorderThickness = new Windows.UI.Xaml.Thickness(element.Border ? 1 : 0);
				            }
				        }
				        
				        protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
				        {
				            base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
				            UpdateControl();
				        }
				
				        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
				        {
				            base.OnElementChanged(e);
				            UpdateControl();
				        }
				    }
				}
				#endif
				#endregion
				
				#region ExtPicker
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
				    using Xamarin.Forms;
				    
				    public class ExtPicker : Picker
				    {
				        #region Contructors
				        public ExtPicker()
				        {
				            Border = true;
				        }
				        #endregion
				        
				        #region Properties
				        public TextAlignment TextAlignment
				        {
				            get => _textAlign;
				            set
				            {
				                if ( value == _textAlign) return;
				                _textAlign = value;
				                OnPropertyChanged();
				            }
				        }
				        private TextAlignment _textAlign;
				        
				        public bool Border
				        {
				            get => _border;
				            set
				            {
				                if ( value == _border) return;
				                _border = value;
				                OnPropertyChanged();
				            }
				        }
				        private bool _border;
				        #endregion
				    }
				}
				#endregion
				
				#region ExtPickerRenderer
				#if __IOS__
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
					using Xamarin.Forms.Platform.iOS;
					using Xamarin.Forms;
					using UIKit;
								    
					public class ExtPickerRenderer : PickerRenderer
					{
						protected void UpdateControl()
						{
							if (Control != null && Element != null)
							{
								Control.BorderStyle = UITextBorderStyle.None;
								var element = (ExtPicker)Element;
								//Control.TextColor = element.TextColor.ToUIColor();
								switch (element.TextAlignment)
								{
								    case TextAlignment.Start: Control.TextAlignment = UITextAlignment.Left; break;
								    case TextAlignment.End: Control.TextAlignment = UITextAlignment.Right; break;
								    case TextAlignment.Center: Control.TextAlignment = UITextAlignment.Center; break;
								}
							}
						}
								        
						protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
						{
							base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
							UpdateControl();
						}
								
						protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
						{
							base.OnElementChanged(e);
							UpdateControl();
						}
					}
				}
				#endif
								
				#if __ANDROID__
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
					using Xamarin.Forms.Platform.Android;
					using Xamarin.Forms;
					using Android.Views;
								    
					public class ExtPickerRenderer : PickerRenderer
					{
						protected void UpdateControl()
						{
							if (Control != null && Element != null)
							{
								//Android elements are already borderless by default
								var element = (ExtPicker)Element;
								//Control.SetTextColor(element.TextColor.ToAndroid());
								switch (element.TextAlignment)
								{
								    case Xamarin.Forms.TextAlignment.Start: Control.Gravity = GravityFlags.Left; break;
								    case Xamarin.Forms.TextAlignment.End: Control.Gravity = GravityFlags.Right; break;
								    case Xamarin.Forms.TextAlignment.Center: Control.Gravity = GravityFlags.Center; break;
								}
							}
						}
								        
						protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
						{
							base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
							UpdateControl();
						}
								
						protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
						{
							base.OnElementChanged(e);
							UpdateControl();
						}
					}
				}
				#endif
				
				#if WINDOWS_UWP
				// ReSharper disable once CheckNamespace
				namespace Supermodel.Mobile.XForms.UIComponents.CustomControls
				{
				    using Xamarin.Forms.Platform.UWP;
				    using Windows.UI.Xaml;
				    using Xamarin.Forms;
				    using Windows.UI.Xaml.Media;
				
					public class ExtPickerRenderer : PickerRenderer
					{
						protected void UpdateControl()
						{
							if (Control != null && Element != null)
							{
								//Android elements are already borderless by default
								var element = (ExtPicker)Element;
								//Control.Foreground = (SolidColorBrush)new ColorConverter().Convert(element.TextColor, null, null, null);
								//switch (element.TextAlignment)
								//{
				                //    case Xamarin.Forms.TextAlignment.Start: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Left; break;
				                //    case Xamarin.Forms.TextAlignment.End: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Right; break;
				                //    case Xamarin.Forms.TextAlignment.Center: Control.TextAlignment = Windows.UI.Xaml.TextAlignment.Center; break;
								//}
				                Control.BorderThickness = new Windows.UI.Xaml.Thickness(element.Border ? 1 : 0);
							}
						}
								        
						protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs)
						{
							base.OnElementPropertyChanged(sender, propertyChangedEventArgs);
							UpdateControl();
						}
								
						protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
						{
							base.OnElementChanged(e);
							UpdateControl();
						}
					}
				}
				#endif
				#endregion
				
			#endregion
			
			#region ButtonXFModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
			    using Pages.CRUDDetail;
			    using System;
			    using System.Collections.Generic;
			    using XForms;
			    using ViewModels;
			    using Xamarin.Forms;
			
			    public class ButtonXFModel : ViewCell, ISupermodelMobileDetailTemplate
			    {
			        #region Constructors
			        public ButtonXFModel(string text, Action<IBasicCRUDDetailPage> onClicked) : this(text, null, onClicked) {}
			        public ButtonXFModel(string text, string imageFileName, Action<IBasicCRUDDetailPage> onClicked) : this(text, imageFileName, imageFileName, onClicked) { }
			        public ButtonXFModel(string text, string imageFileName, string disabledImageFileName, Action<IBasicCRUDDetailPage> onClicked)
			        {
			            View = new StackLayout
			            {
			                Padding = new Thickness(8, 0, 8, 0),
			                Orientation = StackOrientation.Horizontal,
			                VerticalOptions = LayoutOptions.CenterAndExpand,
			                HorizontalOptions = LayoutOptions.FillAndExpand,
			                HeightRequest = 40,
			            };
			
			            if (imageFileName != null)
			            {
			                ImageFileName = imageFileName;
			                DisabledImageFileName = disabledImageFileName;
			
			                Image = new Image { Source = ImageFileName };
			                StackLayoutView.Children.Add(Image);
			            }
			
			            Label = new Label { HorizontalOptions = LayoutOptions.Start, VerticalOptions = LayoutOptions.Center, Text = text, FontSize = XFormsSettings.LabelFontSize };
			            if (Device.OS == TargetPlatform.iOS) Label.TextColor = Color.FromHex("#007AFF");
			            StackLayoutView.Children.Add(Label);
			
			            OnClicked = onClicked;
			
			            Tapped += (sender, args) => { OnClicked?.Invoke((IBasicCRUDDetailPage)ParentPage); };
			        }
			        #endregion
			
			        #region ISupermodelMobileDetailTemplate implemetation
			        public List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue)
			        {
			            ParentPage = parentPage;
			            return new List<Cell> { this };
			        }
			        #endregion
			
			        #region EventHandling
			        public Action<IBasicCRUDDetailPage> OnClicked { get; set; }
			        #endregion
			
			        #region Properties
			        public bool Active
			        {
			            get => _active;
			            set
			            {
			                _active = IsEnabled = value;
			
			                if (_active)
			                {
			                    Image.Source = ImageFileName;
			                    if (Device.OS == TargetPlatform.iOS) Label.TextColor = Color.FromHex("#007AFF");
			                }
			                else
			                {
			                    Image.Source = DisabledImageFileName;
			                    Label.TextColor = XFormsSettings.DisabledTextColor;
			                }
			
			            }
			        }
			        private bool _active;
			
			        public StackLayout StackLayoutView
			        {
			            get => (StackLayout)View;
			            set => View = value;
			        }
			
			        public string Text
			        {
			            get => Label.Text;
			            set => Label.Text = value;
			        }
			
			        public Label Label { get; }
			        public Image Image { get; }
			        public string ImageFileName { get; }
			        public string DisabledImageFileName { get; }
			
			        public Page ParentPage { get; set; }
			        #endregion
			    }
			}
			#endregion
			
			#region DateReadOnlyXFModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
			    using System;
			    using Base;
			    using Xamarin.Forms;    
			    using ReflectionMapper;
			    using XForms;
			
			    public class DateReadOnlyXFModel : SingleCellReadOnlyUIComponentForTextXFModel
			    {
			        #region Constructors
			        public DateReadOnlyXFModel()
			        {
			            TextLabel = new Label { HorizontalOptions = LayoutOptions.EndAndExpand, VerticalOptions = LayoutOptions.Center, LineBreakMode = LineBreakMode.TailTruncation, FontSize = XFormsSettings.LabelFontSize, TextColor = XFormsSettings.ValueTextColor };
			            StackLayoutView.Children.Add(TextLabel);
			        }
			        #endregion
			
			        #region ICstomMapper implementations
			        public override object MapFromObjectCustom(object obj, Type objType)
			        {
			            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
			            
			            if (obj == null) Value = null;
			            else Value = (DateTime)obj; 
			
			            return this;
			        }
			        // ReSharper disable once RedundantAssignment
			        public override object MapToObjectCustom(object obj, Type objType)
			        {
			            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
			            if (objType == typeof(DateTime) && Value == null) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1} because {0} is null but {1} is not nullable", GetType().Name, objType.Name));
			            obj = Value; //This assignment does not do anything but we still do it for consistency
			            return obj;
			        }
			        #endregion
			
			        #region Properties
			        public DateTime? Value
			        {
			            get
			            {
			                if (DateTime.TryParse(Text, out var result)) return result;
			                else return null;
			            }
			            set => Text = value == null ? "" : value.Value.ToString("d");
			        }
			        public override string Text
			        {
			            get => TextLabel.Text;
			            set => TextLabel.Text = value;
			        }
			        public Label TextLabel { get; }
			        public override TextAlignment TextAlignmentIfApplies
			        {
			            get => TextLabel.HorizontalTextAlignment;
			            set => TextLabel.HorizontalTextAlignment = value;
			        }
			        #endregion
			    }
			}
			#endregion
			
			#region DateXFModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
			    using CustomControls;
			    using System;
			    using Base;
			    using Xamarin.Forms;    
			    using ReflectionMapper;
			    using XForms;
			    using Pages.CRUDDetail;
			
			    public class DateXFModel : SingleCellWritableUIComponentWithoutBackingXFModel, IRMapperCustom
			    {
			        #region Constructors
					public DateXFModel()
					{
						DatePicker = new ExtDatePicker{ HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, Border = false, TextAlignment = TextAlignment.End, FontSize = XFormsSettings.LabelFontSize, TextColor = XFormsSettings.ValueTextColor };
					    DatePicker.PropertyChanged += (sender, args) =>
					    {
					        if (_currentValue != DatePicker.Date)
					        {
					            _currentValue = DatePicker.Date;
					            OnChanged?.Invoke((IBasicCRUDDetailPage)ParentPage);
					        }
					    };
			
			            StackLayoutView.Children.Add(DatePicker);
			            Tapped += (sender, args) => DatePicker.Focus();
					}
			        #endregion
			
			        #region ICstomMapper implementations
			        public virtual object MapFromObjectCustom(object obj, Type objType)
			        {
			            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
			            
			            if (obj == null) Value = null;
			            else Value = (DateTime)obj; 
			
			            return this;
			        }
			        // ReSharper disable once RedundantAssignment
			        public virtual object MapToObjectCustom(object obj, Type objType)
			        {
			            if (objType != typeof(DateTime) && objType != typeof(DateTime?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
			            if (objType == typeof(DateTime) && Value == null) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1} because {0} is null but {1} is not nullable", GetType().Name, objType.Name));
			            obj = Value; //This assignment does not do anything but we still do it for consistency
			            return obj;
			        }
			        #endregion
			
			        #region EventHandling
			        public Action<IBasicCRUDDetailPage> OnChanged { get; set; }
			        #endregion
			
			        #region Properties
			        public DateTime? Value
			        {
			            get => DatePicker.Date;
			            set
			            {
			                _currentValue = value;
			                if (value == null) DatePicker.Date = DateTime.Today;
			                else DatePicker.Date = value.Value;
			            }
			        }
			        private DateTime? _currentValue;
			
			        public ExtDatePicker DatePicker { get; }
			
			        public override object WrappedValue => Value;
			
			        public override TextAlignment TextAlignmentIfApplies
			        {
			            get => DatePicker.TextAlignment;
			            set => DatePicker.TextAlignment = value;
			        }
			
			        public bool Active
			        {
			            get => _active;
			            set
			            {
			                if (_active == value) return;
			                _active = IsEnabled = value;
			                DisplayNameLabel.TextColor = value ? XFormsSettings.LabelTextColor : XFormsSettings.DisabledTextColor;
			                DatePicker.TextColor = value ? XFormsSettings.ValueTextColor : XFormsSettings.DisabledTextColor;
			                RequiredFieldIndicator.TextColor = value ? XFormsSettings.RequiredAsteriskColor : XFormsSettings.DisabledTextColor;
			            }
			        }
			        private bool _active = true;
			        #endregion
			    }
			}
			#endregion
			
			#region DropdownXFModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
				using Xamarin.Forms;    
				using CustomControls;
				using System.Collections.ObjectModel;
				using System.Collections.Specialized;
				using System.Linq;
				using System.Collections.Generic;
				using Base;
				using XForms;
				using Pages.CRUDDetail;
			    using System;
			
			    public class DropdownXFModel : SingleCellWritableUIComponentWithoutBackingXFModel
				{
					#region Option nested class
					public class Option
					{
						public Option(string value, string label, bool isDisabled = false)
						{
						    Value = value;
						    Label = label;
						    IsDisabled = isDisabled;
						}
						public string Value { get; private set; }
						public string Label { get; private set; }
						public bool IsDisabled { get; private set; }
					}
					public ObservableCollection<Option> Options = new ObservableCollection<Option>();
					public List<Option> DisplayedOptions = new List<Option>();
					#endregion
						        
					#region Constructors
					public DropdownXFModel()
					{
						Picker = new ExtPicker
						{
							HorizontalOptions = LayoutOptions.FillAndExpand, 
			                VerticalOptions = LayoutOptions.FillAndExpand, 
			                Border = false, 
			                TextAlignment = TextAlignment.End,
						    FontSize = XFormsSettings.LabelFontSize,
						    TextColor = XFormsSettings.ValueTextColor
			            };
					    Picker.PropertyChanged += (sender, args) =>
					    {
					        if (_currentValue != Picker.SelectedIndex)
					        {
					            _currentValue = Picker.SelectedIndex;
					            OnChanged?.Invoke((IBasicCRUDDetailPage)ParentPage);
					        }
					    };
			
			            StackLayoutView.Children.Add(Picker);
						Tapped += (sender, args) => Picker.Focus();
						Options.CollectionChanged += OptionsChangedHandler;
						SelectedValue = "";
			
					    Picker.SelectedIndexChanged += (source, args) => { UpdateTextColor(); };
					}
			        #endregion
			
			        #region Event Handlers
			        protected virtual void OptionsChangedHandler(object sender, NotifyCollectionChangedEventArgs args)
					{
						ResetList(SelectedValue, true);
					}
					protected virtual void ResetList(string selectedValue, bool setValue)
					{
						DisplayedOptions = new List<Option> { new Option("", BlankOptionLabel) };
						foreach (var option in Options)
						{
						    if (option.IsDisabled)
						    {
						        if (option.Value == selectedValue) DisplayedOptions.Add(option);
						    }
						    else
						    {
						        DisplayedOptions.Add(option);
						    }
						}
						            
						Picker.Items.Clear();
						foreach (var option in DisplayedOptions)
						{
							if (option.IsDisabled) Picker.Items.Add(option.Label + " [DISABLED]");
						    else Picker.Items.Add(option.Label);
						}
						if (setValue) SelectedValue = selectedValue;
					}
			
				    public Action<IBasicCRUDDetailPage> OnChanged { get; set; }
			        #endregion
			
			        #region Properties
			        public string BlankOptionLabel { get; set; } = " ";
					public string SelectedValue
					{
						get
						{
						    if (SelectedIndex == null) return "";
						    var selectedOption = DisplayedOptions[SelectedIndex.Value];
						    return selectedOption.Value;
						} 
						set
						{
			                if (value == null)
						    {
						        SelectedIndex = null;
						    }
						    else
						    {
						        var originalItemDisabled = false;
						        if (SelectedIndex != null) originalItemDisabled = DisplayedOptions[SelectedIndex.Value].IsDisabled;
						                    
						        var selectedOption = DisplayedOptions.FirstOrDefault(x => x.Value == value);
								if (selectedOption == null)
								{
									SelectedIndex = null;
								}
								else
								{
						            //SelectedIndex = Options.Where(x => !x.IsDisabled || x.Value == value).ToList().IndexOf(selectedOption);
									if (selectedOption.IsDisabled || originalItemDisabled) ResetList(selectedOption.Value, false);
						            SelectedIndex = DisplayedOptions.IndexOf(selectedOption);
								}
						    }
			            }
					}
			
			        public string SelectedLabel
					{
						get
						{
						    if (SelectedIndex == null) return "";
						    var selectedOption = Options[SelectedIndex.Value];
						    return selectedOption.Label;
						}
					}
					public bool IsEmpty => string.IsNullOrEmpty(SelectedValue);
			
				    protected int? SelectedIndex
					{
						get => Picker.SelectedIndex == -1 ? (int?)null : Picker.SelectedIndex;
				        set
						{
						    _currentValue = value;
						    if (value == null) Picker.SelectedIndex = -1;
						    else Picker.SelectedIndex = value.Value;
			
			                UpdateTextColor();
						}
					}
				    private int? _currentValue;
			
			        public void UpdateTextColor()
				    {
			            if (SelectedIndex == 0) Picker.TextColor = Color.Gray;
			            else Picker.TextColor = Device.OnPlatform(Color.Black, Color.White, Color.Black);
				    }
			
					public ExtPicker Picker { get; }
						
					public override object WrappedValue => SelectedValue;
			
				    public override TextAlignment TextAlignmentIfApplies
					{
						get => Picker.TextAlignment;
				        set => Picker.TextAlignment = value;
				    }
			
				    public bool Active
				    {
				        get => _active;
				        set
				        {
				            if (_active == value) return;
				            _active = IsEnabled = value;
				            DisplayNameLabel.TextColor = value ? XFormsSettings.LabelTextColor : XFormsSettings.DisabledTextColor;
				            Picker.TextColor = value ? XFormsSettings.ValueTextColor : XFormsSettings.DisabledTextColor;
				            RequiredFieldIndicator.TextColor = value ? XFormsSettings.RequiredAsteriskColor : XFormsSettings.DisabledTextColor;
			            }
			        }
				    private bool _active = true;
			        #endregion
			    }
			}
			#endregion
			
			#region DropdownXFModelUsingEnum
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
			    using Utils;
			    using System.Linq;
			    using System;
			    using ReflectionMapper;
			    using System.Collections.Generic;
			    using System.Globalization;    
			    
			    public class DropdownXFModelUsingEnum<EnumT> : DropdownXFModel, IRMapperCustom where EnumT : struct, IConvertible
			    {
			        #region Constructors
			        public DropdownXFModelUsingEnum()
			        {
			            var enumValues = new List<object>();
			            foreach (var item in Enum.GetValues(typeof(EnumT))) enumValues.Add(item);
			            enumValues = enumValues.OrderBy(x => x.GetScreenOrder()).ToList();
			
			            foreach (var option in enumValues) Options.Add(new EnumOption((EnumT)option));
			            SelectedValue = null;
			        }
			        public DropdownXFModelUsingEnum(EnumT selectedEnum) : this()
			        {
			            SelectedEnum = selectedEnum;
			        }
			        #endregion
			
			        #region Nested Options class
			        public class EnumOption : Option
			        {
			            public EnumOption(EnumT value, string label, bool isDisabled) : base(value.ToString(CultureInfo.InvariantCulture), label, isDisabled) { }
			            public EnumOption(EnumT value) : this(value, value.GetDescription(), value.IsDisabled()) { }
			            public EnumT EnumValue => (EnumT)Enum.Parse(typeof(EnumT), Value);
			        }
			        #endregion
			
			        #region ICstomMapper implementations
			        public virtual object MapFromObjectCustom(object obj, Type objType)
			        {
			            if (objType != typeof(EnumT) && objType != typeof(EnumT?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
			            SelectedEnum = (EnumT?)obj;
			            return this;
			        }
			        // ReSharper disable once RedundantAssignment
			        public virtual object MapToObjectCustom(object obj, Type objType)
			        {
			            if (objType != typeof(EnumT) && objType != typeof(EnumT?)) throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
			            if (objType == typeof(EnumT) && SelectedEnum == null) throw new PropertyCantBeAutomappedException(string.Format("{0} can't be automapped to {1} because {0} is null but {1} is not nullable", GetType().Name, objType.Name));
			            obj = SelectedEnum; //This assignment does not do anyhting but we still do it for consistency
			            return obj;
			        }
			        #endregion
			
			        #region Properties
			        public EnumT? SelectedEnum
			        {
			            get
			            {
			                if (string.IsNullOrEmpty(SelectedValue)) return null;
			                return (EnumT)Enum.Parse(typeof(EnumT), SelectedValue);
			            }
			            set => SelectedValue = value == null ? "" : ((EnumT)value).ToString(CultureInfo.InvariantCulture);
			        }
			        #endregion
			    }
			}
			#endregion
			
			#region ImagesReadOnlyXFModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
			    using System.IO;
			    using System;
			    using Xamarin.Forms;
			    using System.Collections.Generic;
			    using Base;
			
			    public class ImagesReadOnlyXFModel : BinaryFilesReadOnlyXFModel
			    {
			        #region ISupermodelMobileDetailTemplate implementation
			        public override List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue)
			        {
			            ParentPage = parentPage;
			            var cells = new List<Cell>();
			            var imageIndex = 0;
			            foreach (var imageFile in ModelsWithBinaryFileXFModels)
			            {
			                var file = imageFile;
			                var index = imageIndex;
			
			                var cell = new ImageCell
			                {
			                    ImageSource = ImageSource.FromStream(() => new MemoryStream(file.BinaryFile.BinaryContent)),
			                    Text = file.Title
			                };
			                cell.Tapped += (sender, args) => { ImageTappedHandler(index);};
			                cells.Add(cell);
			                imageIndex ++;
			            }
			            return cells;
			        }
			        public async void ImageTappedHandler(int imageIndex)
			        {
			            var imagesCarouselPage = new CarouselPage();
			            foreach (var imageFile in ModelsWithBinaryFileXFModels)
			            {
			                // ReSharper disable once AccessToForEachVariableInClosure
			                imagesCarouselPage.Children.Add(new ContentPage { Content = new Image { Source = ImageSource.FromStream(() => new MemoryStream(imageFile.BinaryFile.BinaryContent)), HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand } });
			            }
			            imagesCarouselPage.CurrentPage = imagesCarouselPage.Children[imageIndex];
			            await ParentPage.Navigation.PushAsync(imagesCarouselPage);
			        }
			        #endregion
			
			        #region Properties
			        public override bool ShowDisplayNameIfApplies { get; set; }
			        public override string DisplayNameIfApplies { get; set; }
			        public override TextAlignment TextAlignmentIfApplies { get; set; }
			        
			        protected Page ParentPage { get; set; }
			        #endregion
			    }
			}
			#endregion
			
			#region ImagesXFModel
			//ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
			    using System.Linq;
			    using ViewModels;
			    using System.IO;
			    using System;
			    using Xamarin.Forms;
			    using System.Collections.Generic;
			    using Base;
			    using ReflectionMapper;
			    using Views;
			    using CustomCells;
			    using Utils;
			    using Plugin.Media;
			    using Plugin.Media.Abstractions;
			
			    public class ImagesXFModel : BinaryFilesWritableXFModel
			    {
			        #region EmbeddedTypes
			        public enum ImageViewModeEnum { ImageZoomEnabled, SwipingThroughImagesEnabled }
			        #endregion
			
			        #region Constructors
			        public ImagesXFModel()
			        {
			            AddNewCell = new AddNewCell(XFormsSettings.AddNewImageFileName) { Text = "Add New Image" };
			        }
			        #endregion
			
			        #region ISupermodelMobileDetailTemplate implementation
			        public override List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue)
			        {
			            ParentPage = parentPage;
			            Cells.Clear();
			            var imageIndex = 0;
			            foreach (var imageFile in ModelsWithBinaryFileXFModels)
			            {
			                var file = imageFile;
			                var index = imageIndex;
			
			                var cell = new ImageCellWithEditableText
			                {
			                    ImageSource = ImageSource.FromStream(() => new MemoryStream(file.BinaryFile.BinaryContent)),
			                    Text = file.Title,
			                    Placeholder = "Image Title",
			                };
			                cell.TextEntry.TextChanged += (sender, args) => { file.Title = cell.Text; };
			
			                var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true };
			                deleteAction.Clicked += (sender, e) => { ImageDeletedHandler(file, cell); };
			                cell.ContextActions.Add(deleteAction);
			
			                //This we do for Android -- otherwise the tap is not recognized
			                //This is instead of cell.Tapped += (sender, args) => { ImageTappedHandler(index); };
			                var tapGestureRecognizer = new TapGestureRecognizer();
			                tapGestureRecognizer.Tapped += (s, e) => { ImageTappedHandler(index); };
			                cell.Image.GestureRecognizers.Add(tapGestureRecognizer);
			
			                Cells.Add(cell);
			                imageIndex++;
			            }
			            AddNewCell.ParentPage = parentPage;
			            AddNewCell.Tapped += (sender, args) => { AddNewTapped(); };
			            Cells.Add(AddNewCell);
			            return Cells;
			        }
			        public virtual async void AddNewTapped()
			        {
			            if (_actionSheetOpen) return;
			            _actionSheetOpen = true;
			
			            await CrossMedia.Current.Initialize();
			
			            var options = new List<string>();
			            if (CrossMedia.Current.IsPickPhotoSupported) options.Add("Photo Library");
			            if (CrossMedia.Current.IsTakePhotoSupported) options.Add("Take Photo");
			            var action = await ParentPage.DisplayActionSheet(null, "Cancel", null, options.ToArray());
			            _actionSheetOpen = false;
			            try
			            {
			                MediaFile mediaFile;
			                switch (action)
			                {
			                    case "Photo Library":
			                    {
			                        mediaFile = await CrossMedia.Current.PickPhotoAsync();
			                        break;
			                    }
			                    case "Take Photo":
			                    {
			                        var storeCameraMediaOptions = new StoreCameraMediaOptions
			                        {
			                            DefaultCamera = CameraDevice.Rear,
			                            SaveToAlbum = false,
			                            Directory = "Media",
			                            Name = "pic.jpg"
			                        };
			                        mediaFile = await CrossMedia.Current.TakePhotoAsync(storeCameraMediaOptions);
			                        break;
			                    }
			                    case "Cancel":
			                    {
			                        mediaFile = null;
			                        break;
			                    }
			                    default: throw new Exception("Invalid photo option. This should never happen");
			                }
			                if (mediaFile != null)
			                {
			                    var binaryContent = ReadFully(mediaFile.GetStream());
			                    binaryContent = await ImageResizer.ResizeImageAsync(binaryContent, 1024, 1024);
			
			                    //Insert image into ModelsWithBinaryFileXFModels list
			                    var file = new ModelWithBinaryFileXFModel { Id = 0, Title = "", BinaryFile = new BinaryFileXFModel { BinaryContent = binaryContent, Name = "image.png" } };
			                    ModelsWithBinaryFileXFModels.Add(file);
			
			                    //Insert image cell
			                    var cell = new ImageCellWithEditableText
			                    {
			                        ImageSource = ImageSource.FromStream(() => new MemoryStream(file.BinaryFile.BinaryContent)),
			                        Text = file.Title,
			                        Placeholder = "Image Title",
			                    };
			                    cell.TextEntry.TextChanged += (sender, args) => { file.Title = cell.Text; };
			
			                    var deleteAction = new MenuItem { Text = "Delete", IsDestructive = true };
			                    deleteAction.Clicked += (sender, e) => { ImageDeletedHandler(file, cell); };
			                    cell.ContextActions.Add(deleteAction);
			
			                    var tableView = (ViewWithActivityIndicator<TableView>)ParentPage.PropertyGet("DetailView");
			                    var section = tableView.ContentView.Root.Single(x => x.Contains(AddNewCell));
			                    var index = section.IndexOf(AddNewCell);
			
			                    //This we do for Android -- otherwise the tap is not recognized
			                    //This is instead of cell.Tapped += (sender, args) => { ImageTappedHandler(index); };
			                    var tapGestureRecognizer = new TapGestureRecognizer();
			                    tapGestureRecognizer.Tapped += (s, e) => { ImageTappedHandler(index); };
			                    cell.Image.GestureRecognizers.Add(tapGestureRecognizer);
			
			                    section.Insert(index, cell);
			                }
			            }
			            // ReSharper disable once EmptyGeneralCatchClause
			            catch (Exception) { }
			        }
			        public virtual void ImageDeletedHandler(ModelWithBinaryFileXFModel file, Cell cell)
			        {
			            ModelsWithBinaryFileXFModels.Remove(file);
			
			            var tableView = (ViewWithActivityIndicator<TableView>)ParentPage.PropertyGet("DetailView");
			            foreach (var section in tableView.ContentView.Root) section.Remove(cell);
			        }
			        public virtual async void ImageTappedHandler(int imageIndex)
			        {
			            if (ImageViewMode == ImageViewModeEnum.SwipingThroughImagesEnabled)
			            {
			                var imagesCarouselPage = new CarouselPage();
			                foreach (var imageFile in ModelsWithBinaryFileXFModels)
			                {
			                    var imagePage = CreateSingleImagePage(imageFile, false);
			                    imagesCarouselPage.Children.Add(imagePage);
			                }
			                imagesCarouselPage.CurrentPage = imagesCarouselPage.Children[imageIndex];
			                await ParentPage.Navigation.PushModalAsync(imagesCarouselPage);
			            }
			            else
			            {
			                var imagePage = CreateSingleImagePage(ModelsWithBinaryFileXFModels[imageIndex], true);
			                await ParentPage.Navigation.PushModalAsync(imagePage);
			            }
			        }
			
			        public static byte[] ReadFully(Stream input)
			        {
			            var buffer = new byte[16 * 1024];
			            using (var ms = new MemoryStream())
			            {
			                int read;
			                while ((read = input.Read(buffer, 0, buffer.Length)) > 0) ms.Write(buffer, 0, read);
			                return ms.ToArray();
			            }
			        }
			
			        protected virtual ContentPage CreateSingleImagePage(ModelWithBinaryFileXFModel imageFile, bool withZoom)
			        {
			            Image image;
			            if (withZoom)
			            {
			                image = new ZoomImage
			                {
			                    Source = ImageSource.FromStream(() => new MemoryStream(imageFile.BinaryFile.BinaryContent)),
			                    HorizontalOptions = LayoutOptions.FillAndExpand,
			                    VerticalOptions = LayoutOptions.FillAndExpand,
			                };
			            }
			            else
			            {
			                image = new Image
			                {
			                    Source = ImageSource.FromStream(() => new MemoryStream(imageFile.BinaryFile.BinaryContent)),
			                    HorizontalOptions = LayoutOptions.FillAndExpand,
			                    VerticalOptions = LayoutOptions.FillAndExpand,
			                };
			            }
			
			            AbsoluteLayout.SetLayoutFlags(image, AbsoluteLayoutFlags.All);
			            AbsoluteLayout.SetLayoutBounds(image, new Rectangle(0f, 0f, 1f, 1f));
			
			            var button = new Button
			            {
			                VerticalOptions = LayoutOptions.Start,
			                HorizontalOptions = LayoutOptions.End
			            };
			
			            if (!string.IsNullOrEmpty(CloseIconFileName)) button.Image = new FileImageSource { File = CloseIconFileName };
			            else button.Text = "✖";
			
			            button.Clicked += async (sender, args) =>
			            {
			                await ParentPage.Navigation.PopModalAsync(true);
			            };
			            AbsoluteLayout.SetLayoutFlags(button, AbsoluteLayoutFlags.All);
			            AbsoluteLayout.SetLayoutBounds(button, new Rectangle(0.975f, 0.025f, 0.1f, 0.1f));
			
			            var imagePage = new ContentPage
			            {
			                Content = new AbsoluteLayout
			                {
			                    Children = { image, button }
			                }
			            };
			
			            return imagePage;
			        }
			        #endregion
			
			        #region Properties
			        public string CloseIconFileName { get; set; }
			
			        public override bool ShowDisplayNameIfApplies { get; set; }
			        public override string DisplayNameIfApplies { get; set; }
			        public override TextAlignment TextAlignmentIfApplies { get; set; }
			
			        protected Page ParentPage { get; set; }
			        protected AddNewCell AddNewCell { get; }
			
			        public override string ErrorMessage
			        {
			            get => AddNewCell.ErrorMessage;
			            set => AddNewCell.ErrorMessage = value;
			        }
			        public override bool Required
			        {
			            get => AddNewCell.Required;
			            set => AddNewCell.Required = value;
			        }
			
			        public readonly List<Cell> Cells = new List<Cell>();
			
			        public ImageViewModeEnum ImageViewMode { get; set; } = ImageViewModeEnum.ImageZoomEnabled;
			        protected bool _actionSheetOpen;
			        #endregion
			    }
			}
			#endregion
			
			#region MultiLineTextBoxReadOnlyXFModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
			    using Xamarin.Forms;
			    using Base;
			    using XForms;
			
			    public class MultiLineTextBoxReadOnlyXFModel : SingleCellReadOnlyUIComponentForTextXFModel
			    {
			        #region Constructors
					public MultiLineTextBoxReadOnlyXFModel()
					{
			            TextLabel = new Label
			            {
			                FontSize = XFormsSettings.LabelFontSize,
			                TextColor = XFormsSettings.ValueTextColor,
			                HeightRequest = XFormsSettings.MultiLineTextBoxReadOnlyCellHeight - XFormsSettings.MultiLineTextLabelHeight
			            };
			            ScrollView = new ScrollView{ Content = TextLabel, HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand };
			            
					    TextLabel.SetBinding(Entry.TextProperty, "Text");
			            StackLayoutView.Orientation = StackOrientation.Vertical;
			            StackLayoutView.Padding = Device.OnPlatform(8, new Thickness(8, 10), 8);
			            StackLayoutView.Children.Add(ScrollView);
			
					    SetHeight(XFormsSettings.MultiLineTextBoxReadOnlyCellHeight);
					    StackLayoutView.HeightRequest = XFormsSettings.MultiLineTextBoxReadOnlyCellHeight;
					}
			        #endregion
			
			        #region Properties
			        public void SetHeight(int newHeight)
			        {
			            Height = newHeight;
			            ScrollView.HeightRequest = ShowDisplayNameIfApplies ? newHeight - XFormsSettings.MultiLineTextLabelHeight : newHeight - 20;
			        }
			        public override string Text
			        {
			            get => TextLabel.Text;
			            set
			            {
			                if (value == TextLabel.Text) return;
			                TextLabel.Text = value;
			                OnPropertyChanged();
			            }
			        }
			        public ScrollView ScrollView { get; }
			        public Label TextLabel { get; }
			        public override TextAlignment TextAlignmentIfApplies { get; set; }
			        #endregion
			
			    }
			}
			#endregion
			
			#region MultiLineTextBoxXFModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
			    using Xamarin.Forms;
			    using Base;
			    using XForms;
			    using System;
			    using Pages.CRUDDetail;
			
			    public class MultiLineTextBoxXFModel : SingleCellWritableUIComponentForTextXFModel
			    {
			        #region Constructors
			        public MultiLineTextBoxXFModel(Keyboard keyboard) : this()
			        {
			            Editor.Keyboard = keyboard;
			        }
			        public MultiLineTextBoxXFModel()
					{
			            Editor = new Editor
					    {
					        HorizontalOptions = LayoutOptions.FillAndExpand,
					        VerticalOptions = LayoutOptions.FillAndExpand,
					        HeightRequest = XFormsSettings.MultiLineTextBoxCellHeight - XFormsSettings.MultiLineTextLabelHeight,
					        FontSize = XFormsSettings.LabelFontSize,
					        TextColor = XFormsSettings.ValueTextColor
			            };
					    Editor.SetBinding(Entry.TextProperty, "Text");
					    Editor.PropertyChanged += (sender, args) =>
					    {
					        if (_currentValue != Editor.Text)
					        {
					            _currentValue = Editor.Text;
					            OnChanged?.Invoke((IBasicCRUDDetailPage)ParentPage);
					        }
					    };
			            StackLayoutView.Orientation = StackOrientation.Vertical;
			            #pragma warning disable 618
					    StackLayoutView.Padding = Device.OnPlatform(new Thickness(8, 10), new Thickness(8, 0), new Thickness(8, 10));
			            #pragma warning restore 618
			            StackLayoutView.Children.Add(Editor);
					    SetHeight(XFormsSettings.MultiLineTextBoxCellHeight);
					    StackLayoutView.HeightRequest = XFormsSettings.MultiLineTextBoxCellHeight;
					    Tapped += (sender, args) => Editor.Focus();
					}
			        #endregion
			
			        #region EventHandling
			        public Action<IBasicCRUDDetailPage> OnChanged { get; set; }
			        #endregion
			
			        #region Properties
			        public void SetHeight(int newHeight)
			        {
			            Height = newHeight;
			            Editor.HeightRequest = ShowDisplayNameIfApplies ? newHeight - XFormsSettings.MultiLineTextLabelHeight : newHeight - 20;
			        }
			        public override string Text
			        {
			            get => Editor.Text;
			            set
			            {
			                _currentValue = value;
			                if (value == Editor.Text) return;
			                Editor.Text = value;
			                OnPropertyChanged();
			            }
			        }
			        private string _currentValue;
			
			        public Editor Editor { get; }
			        public override TextAlignment TextAlignmentIfApplies { get; set; }
			
			        public bool Active
			        {
			            get => _active;
			            set
			            {
			                if (_active == value) return;
			                _active = IsEnabled = value;
			                DisplayNameLabel.TextColor = value ? XFormsSettings.LabelTextColor : XFormsSettings.DisabledTextColor;
			                Editor.TextColor = value ? XFormsSettings.ValueTextColor : XFormsSettings.DisabledTextColor;
			                RequiredFieldIndicator.TextColor = value ? XFormsSettings.RequiredAsteriskColor : XFormsSettings.DisabledTextColor;
			            }
			        }
			        private bool _active = true;
			        #endregion
			    }
			}
			#endregion
			
			#region TextBoxReadOnlyXFModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
			    using Xamarin.Forms;
			    using System;
			    using Base;
			    using XForms;
			
			    public class TextBoxReadOnlyXFModel : SingleCellReadOnlyUIComponentForTextXFModel
			    {
			        #region Constructors
			        public TextBoxReadOnlyXFModel()
			        {
			            TextLabel = new Label { HorizontalOptions = LayoutOptions.EndAndExpand, VerticalOptions = LayoutOptions.Center, LineBreakMode = LineBreakMode.TailTruncation, FontSize = XFormsSettings.LabelFontSize, TextColor = XFormsSettings.ValueTextColor };
			            StackLayoutView.Children.Add(TextLabel);
			        }
			        #endregion
			
			        #region ICustomMapper implemtation
			        public override object MapToObjectCustom(object obj, Type objType)
			        {
			            return obj;
			        }
			        #endregion
			
			        #region Properties
			        public override string Text
			        {
			            get => TextLabel.Text;
			            set => TextLabel.Text = value;
			        }
			        public Label TextLabel { get; }
			        public override TextAlignment TextAlignmentIfApplies
			        {
			            get => TextLabel.HorizontalTextAlignment;
			            set => TextLabel.HorizontalTextAlignment = value;
			        }
			        #endregion
			    }
			}
			#endregion
			
			#region TextBoxXFModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
			    using Xamarin.Forms;
			    using CustomControls;
			    using Base;
			    using XForms;
			    using System;
			    using Pages.CRUDDetail;
			
			    public class TextBoxXFModel : SingleCellWritableUIComponentForTextXFModel
			    {
			        #region Constructors
			        public TextBoxXFModel(Keyboard keyboard):this()
			        {
			            TextEntry.Keyboard = keyboard;
			        }
			        public TextBoxXFModel()
			        {
			            TextEntry = new ExtEntry { HorizontalOptions = LayoutOptions.FillAndExpand, VerticalOptions = LayoutOptions.FillAndExpand, Border = false, TextAlignment = TextAlignment.End, WidthRequest = 1, FontSize = XFormsSettings.LabelFontSize, TextColor = XFormsSettings.ValueTextColor };
			            TextEntry.SetBinding(Entry.TextProperty, "Text");
			            TextEntry.PropertyChanged += (sender, args) =>
			            {
			                if (_currentValue != TextEntry.Text)
			                {
			                    _currentValue = TextEntry.Text;
			                    OnChanged?.Invoke((IBasicCRUDDetailPage)ParentPage);
			                }
			            };
			            StackLayoutView.Children.Add(TextEntry);
			            Tapped += (sender, args) => TextEntry.Focus();
			        }
			        #endregion
			
			        #region EventHandling
			        public Action<IBasicCRUDDetailPage> OnChanged { get; set; }
			        #endregion
			
			        #region Properties
			        public override string Text
			        {
			            get => TextEntry.Text;
			            set
			            {
			                _currentValue = value;
			                if (value == TextEntry.Text) return;
			                TextEntry.Text = value;
			                OnPropertyChanged();
			            }
			        }
			        private string _currentValue;
			
			        public ExtEntry TextEntry { get; }
			        public override TextAlignment TextAlignmentIfApplies
			        {
			            get => TextEntry.TextAlignment;
			            set => TextEntry.TextAlignment = value;
			        }
			
			        public bool Active
			        {
			            get => _active;
			            set
			            {
			                if (_active == value) return;
			                _active = IsEnabled = value;
			                DisplayNameLabel.TextColor = value ? XFormsSettings.LabelTextColor : XFormsSettings.DisabledTextColor;
			                TextEntry.TextColor = value ? XFormsSettings.ValueTextColor : XFormsSettings.DisabledTextColor;
			                RequiredFieldIndicator.TextColor = value ? XFormsSettings.RequiredAsteriskColor : XFormsSettings.DisabledTextColor;
			            }
			        }
			        private bool _active = true;
			        #endregion
			    }
			}
			#endregion
			
			#region ToggleSwitchXFModel
			 // ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.UIComponents
			{
			    using System;
			    using ReflectionMapper;
			    using Xamarin.Forms;
			    using Exceptions;
			    using Base;
			    using Pages.CRUDDetail;
			
			    public class ToggleSwitchXFModel : SingleCellWritableUIComponentXFModel
			    {
			        #region Constructors
					public ToggleSwitchXFModel()
					{
						Switch = new Switch { HorizontalOptions = LayoutOptions.EndAndExpand, VerticalOptions = LayoutOptions.Center, OnColor = XFormsSettings.SwitchOnColor };
						Switch.SetBinding(Switch.IsToggledProperty, "IsToggled");
					    Switch.PropertyChanged += (sender, args) =>
					    {
					        if (_currentValue != Switch.IsToggled)
					        {
					            _currentValue = Switch.IsToggled;
					            OnChanged?.Invoke((IBasicCRUDDetailPage)ParentPage);
					        }
					    };
			            StackLayoutView.Children.Add(Switch);
			            Tapped += (sender, args) => Switch.Focus();
					}
			        #endregion
			
			        #region ICustomMapper implemtation
			        public override object MapFromObjectCustom(object obj, Type objType)
			        {
			            if (objType != typeof(bool) && objType != typeof(bool?))
			            {
			                throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
			            }
			
			            if (obj is bool) IsToggled = (bool)obj;
			            else IsToggled = (bool?)obj ?? false;
			
			            return this;
			        }
			        public override object MapToObjectCustom(object obj, Type objType)
			        {
			            if (objType != typeof(bool) && objType != typeof(bool?))
			            {
			                throw new PropertyCantBeAutomappedException($"{GetType().Name} can't be automapped to {objType.Name}");
			            }
			            
			            return IsToggled;
			        }
			        #endregion
			
			        #region EventHandling
			        public Action<IBasicCRUDDetailPage> OnChanged { get; set; }
			        #endregion
			
			        #region Properties
			        public bool IsToggled
			        {
			            get => Switch.IsToggled;
			            set
			            {
			                _currentValue = value;
			                if (value == Switch.IsToggled) return;
			                Switch.IsToggled = value;
			                OnPropertyChanged();
			            }
			        }
			        private bool _currentValue;
			        public Switch Switch { get; }
			
			        public override object WrappedValue => Switch.IsToggled;
			
			        public override TextAlignment TextAlignmentIfApplies
			        {
			            get
			            {
			                switch(Switch.HorizontalOptions.Alignment)
			                {
			                    case LayoutAlignment.Start: return TextAlignment.Start;
			                    case LayoutAlignment.Center: return TextAlignment.Center;
			                    case LayoutAlignment.End: return TextAlignment.End;
			                    default: throw new SupermodelException("Invalid value for Switch.HorizontalOptions.Alignment. This should never happen");
			                }
			            }
			            set
			            {
			                switch(value)
			                {
			                    case TextAlignment.Start: 
			                        Switch.HorizontalOptions = LayoutOptions.StartAndExpand;
			                        break;
			                    case TextAlignment.Center: 
			                        Switch.HorizontalOptions = LayoutOptions.CenterAndExpand;
			                        break;
			                    case TextAlignment.End: 
			                        Switch.HorizontalOptions = LayoutOptions.EndAndExpand;
			                        break;
			                    default: throw new SupermodelException("Invalid value for TextAlignmentIfApplies. This should never happen");
			                }
			            }
			        }
			
			        public bool Active
			        {
			            get => _active;
			            set
			            {
			                if (_active == value) return;
			                _active = IsEnabled = value;
			                DisplayNameLabel.TextColor = value ? XFormsSettings.LabelTextColor : XFormsSettings.DisabledTextColor;
			                Switch.OnColor = value ? XFormsSettings.SwitchOnColor : XFormsSettings.DisabledTextColor;
			                RequiredFieldIndicator.TextColor = value ? XFormsSettings.RequiredAsteriskColor : XFormsSettings.DisabledTextColor;
			            }
			        }
			        private bool _active = true;
			        #endregion
			    }
			}
			#endregion
			
		#endregion
		
		#region ViewModels
			
			#region BinaryFileXFModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.ViewModels
			{
			    using System;
			    
			    public class BinaryFileXFModel
				{
					public String Name { get; set; }
					public Byte[] BinaryContent { get; set; }
				}
			
			}
			#endregion
			
			#region ISupermodelListTemplate
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.ViewModels
			{
			    using Xamarin.Forms;
			    using System;
			
			    public interface ISupermodelListTemplate : ISupermodelNotifyPropertyChanged
			    {
			        DataTemplate GetListCellDataTemplate(EventHandler deleteHandler);
			    }
			}
			#endregion
			
			#region ISupermodelMobileDetailTemplate
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.ViewModels
			{
			    using System.Collections.Generic;
			    using Xamarin.Forms;
			
			    public interface ISupermodelMobileDetailTemplate
			    {
			        List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue);
			    }
			}
			#endregion
			
			#region ISupermodelNotifyPropertyChanged
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.ViewModels
			{
			    using System.ComponentModel;
			
			    public interface ISupermodelNotifyPropertyChanged : INotifyPropertyChanged
			    {
			        void OnPropertyChanged(string propertyName);
			    }
			}
			#endregion
			
			#region XFModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.ViewModels
			{
			    using ReflectionMapper;
			    using Attributes;
			    using UIComponents;
			    using Xamarin.Forms;
			    using System;
			    using System.Collections.Generic;
			    using System.ComponentModel.DataAnnotations;
			    using System.Linq;
			    using System.Reflection;
			    using UIComponents.Base;
			
			    public abstract class XFModel : ISupermodelMobileDetailTemplate, IValidatableObject 
			    {
			        #region ISupermodelMobileDetailTemplate
			        public virtual List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue)
			        {
			            var cells = new List<Cell>();
			            foreach (var property in GetPropertiesInOrder(screenOrderFrom, screenOrderTo))
			            {
			                var propertyObj = this.PropertyGet(property.Name);
			                if (typeof(ISupermodelMobileDetailTemplate).IsAssignableFrom(property.PropertyType))
			                {
			                    //We use existing object if it exists, otherwise we just create a blank object for our purposes
			                    if (propertyObj == null)
			                    {
			                        if (property.PropertyType.GetConstructor(Type.EmptyTypes) == null) throw new Exception($"Property '{property.Name}' is null and no dfeault constructor exists for the type '{property.PropertyType.Name}'");
			                        propertyObj = ReflectionHelper.CreateType(property.PropertyType);
			                    }
			
			                    //Set up display name and Required
			                    if (propertyObj is IReadOnlyUIComponentXFModel uiReadOnlyComponent)
			                    {
			                        //Set up display name if not already set
			                        if (string.IsNullOrEmpty(uiReadOnlyComponent.DisplayNameIfApplies)) uiReadOnlyComponent.DisplayNameIfApplies = GetType().GetDisplayNameForProperty(property.Name);
			
			                        //Set required asterisk
			                        var requiredAttribute = property.GetCustomAttributes(typeof(RequiredAttribute), true).SingleOrDefault() != null;
			                        var noRequiredLabelAttribute = property.GetCustomAttributes(typeof(NoRequiredLabelAttribute), true).SingleOrDefault() != null;
			                        var forceRequiredLabelAttribute = property.GetCustomAttributes(typeof(ForceRequiredLabelAttribute), true).SingleOrDefault() != null;
			
			                        if (uiReadOnlyComponent is IWritableUIComponentXFModel uiComponent)
			                        {
			                            uiComponent.Required = (requiredAttribute || forceRequiredLabelAttribute) && ! noRequiredLabelAttribute;
			                        }
			                    }
			
			                    // ReSharper disable once PossibleNullReferenceException
			                    cells.AddRange((propertyObj as ISupermodelMobileDetailTemplate).RenderDetail(parentPage));
			                }
			                else
			                {
			                    var genericCell = new TextBoxReadOnlyXFModel
			                    {
			                        DisplayNameIfApplies = GetType().GetDisplayNameForProperty(property.Name),
			                        Text = propertyObj.ToString()
			                    };
			                    cells.Add(genericCell);
			                }
			            }
			            return cells;
			        }
			        #endregion
			
			        #region Methods
			        public virtual bool AreWritableFieldsEqual(XFModel xfModel)
			        {
			            foreach (var property in GetPropertiesInOrder())
			            {
			                if (this.PropertyGet(property.Name) is IWritableUIComponentXFModel uiComponent)
			                {
			                    var mine = uiComponent.WrappedValue;
			                    // ReSharper disable once SuspiciousTypeConversion.Global
			                    var theirs = ((IWritableUIComponentXFModel)xfModel.PropertyGet(property.Name)).WrappedValue;
			                    if (mine != null)
			                    {
			                        if (!mine.Equals(theirs)) return false;
			                    }
			                    else
			                    {
			                        if (theirs != null) return false;
			                    }
			                }
			            }
			            return true;
			        }
			        public virtual void ClearValidationErrors()
			        {
			            foreach (var property in GetPropertiesInOrder())
			            {
			                var uiComponent = this.PropertyGet(property.Name) as IWritableUIComponentXFModel;
			                if (uiComponent != null) uiComponent.ErrorMessage = null;
			            }
			        }
			        public virtual void ShowValidationErrors(IEnumerable<ValidationResult> vr)
			        {
			            if (vr == null) vr = new ValidationResultList();
			            foreach (var property in GetPropertiesInOrder())
			            {
			                // ReSharper disable PossibleMultipleEnumeration
			                var uiComponent = this.PropertyGet(property.Name) as IWritableUIComponentXFModel;
			                if (uiComponent != null)
			                {
			                    uiComponent.ErrorMessage = null;
			                    var errors = vr.Where(x => x.MemberNames.Any(y => y == property.Name)).ToList();
			                    
			                    var first = true;
			                    foreach (var error in errors)
			                    {
			                        if (first)
			                        {
			                            first = false;
			                            uiComponent.ErrorMessage = error.ErrorMessage;
			                        }
			                        else
			                        {
			                            uiComponent.ErrorMessage += Environment.NewLine + error.ErrorMessage;
			                        }
			                    }
			                }
			                // ReSharper restore PossibleMultipleEnumeration
			            }
			        }
			        public virtual bool ContainsValidationErrros()
			        {
			            foreach (var property in GetPropertiesInOrder())
			            {
			                var uiComponent = this.PropertyGet(property.Name) as IWritableUIComponentXFModel;
			                if (uiComponent?.ErrorMessage != null) return true;
			            }
			            return false;
			        }
			        #endregion
			
			        #region Validation
			        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
			        {
			            var vr = new ValidationResultList();
			            foreach (var property in GetPropertiesInOrder())
			            {
			                var propertyObj = this.PropertyGet(property.Name);
			                var uiComponent = propertyObj as IWritableUIComponentXFModel;
			                
			                //Check all required UIComponents
			                if (uiComponent != null && property.GetCustomAttributes(typeof(RequiredAttribute), true).SingleOrDefault() != null)
			                {
			                    var preparedError = new ValidationResult("The " + GetType().GetDisplayNameForProperty(property.Name) + " field is required", new [] { property.Name });
			
			                    var value = uiComponent.WrappedValue;
			                    if (value == null)
			                    {
			                        vr.Add(preparedError);    
			                    }
			                    else
			                    {
			                        var strValue = value as string;
			                        if (strValue != null && string.IsNullOrWhiteSpace(strValue)) vr.Add(preparedError);    
			                    }
			                }
			            }
			            return vr;
			        }
			        #endregion
			
			        #region Private Helpers
			        protected virtual IEnumerable<PropertyInfo> GetPropertiesInOrder(int screenOrderFrom = int.MinValue, int screenOrderTo = int.MaxValue)
			        {
			            return GetType().GetProperties()
			                .Where(x => x.GetCustomAttribute<ScaffoldColumnAttribute>() == null || x.GetCustomAttribute<ScaffoldColumnAttribute>().Scaffold)
			                .Where(x => (x.GetCustomAttribute<ScreenOrderAttribute>() != null ? x.GetCustomAttribute<ScreenOrderAttribute>().Order : 100) >= screenOrderFrom)
			                .Where(x => (x.GetCustomAttribute<ScreenOrderAttribute>() != null ? x.GetCustomAttribute<ScreenOrderAttribute>().Order : 100) <= screenOrderTo)
			                .OrderBy(x => x.GetCustomAttribute<ScreenOrderAttribute>() != null ? x.GetCustomAttribute<ScreenOrderAttribute>().Order : 100);
			        }
			        #endregion
			    }
			}
			#endregion
			
			#region XFModelForChildModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.ViewModels
			{
			    using Models;
			    using System;
			    using System.ComponentModel.DataAnnotations;
			    using ReflectionMapper;
			
			    public abstract class XFModelForChildModel<ModelT, ChildModelT> : XFModelForModelBase<ModelT> 
			        where ModelT : class, IModel, ISupermodelNotifyPropertyChanged, new()
			        where ChildModelT : ChildModel, new()
			    {
			        #region Constructors
			        public virtual XFModelForChildModel<ModelT, ChildModelT> Init(ModelT model, Guid childGuidIdentity, params Guid[] parentGuidIdentities)
			        {
			            ParentGuidIdentities = parentGuidIdentities ?? throw new ArgumentNullException(nameof(parentGuidIdentities), "You may need tp override OnLoad on root Model and set up ParentIdentities for all ChildModels there");
			            ChildGuidIdentity = childGuidIdentity;
			            Model = model;
			            return this;
			        }
			        #endregion
			
			        #region Overrdies
			        public override object MapFromObjectCustom(object obj, Type objType)
			        {
			            var model = (ModelT)obj;
			            var childModel = model.GetChildOrDefault<ChildModelT>(ChildGuidIdentity, ParentGuidIdentities) ?? new ChildModelT { ChildGuidIdentity = ChildGuidIdentity, ParentGuidIdentities = ParentGuidIdentities };
			            this.MapFromObjectCustomBase(childModel);
			            return this;
			        }
			        public override object MapToObjectCustom(object obj, Type objType)
			        {
			            var model = (ModelT)obj;
			            var childModel = model.GetChildOrDefault<ChildModelT>(ChildGuidIdentity, ParentGuidIdentities);
			            if (childModel == null)
			            {
			                childModel = new ChildModelT { ParentGuidIdentities = ParentGuidIdentities };
			                this.MapToObjectCustomBase(childModel);
			                model.AddChild(childModel);
			            }
			            else
			            {
			                this.MapToObjectCustomBase(childModel);
			            }
			            return obj;
			        }
			        #endregion
			
			        #region Properties
			        [ScaffoldColumn(false), NotRMapped, NotRCompared] public virtual Guid[] ParentGuidIdentities { get; set; }
			        [ScaffoldColumn(false), NotRMapped, NotRCompared] public virtual Guid ChildGuidIdentity { get; set; } = Guid.NewGuid();
			        #endregion
			    }
			}
			#endregion
			
			#region XFModelForModel
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.ViewModels
			{
			    using Models;
			
			    public abstract class XFModelForModel<ModelT> : XFModelForModelBase<ModelT> where ModelT : class, IModel, ISupermodelNotifyPropertyChanged, new()
			    {
			        #region Constructors
			        public virtual XFModelForModel<ModelT> Init(ModelT model)
			        {
			            Model = model;
			            return this;
			        }
			        #endregion
			    }
			}
			#endregion
			
			#region XFModelForModelBase
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.ViewModels
			{
			    using System;
			    using System.Collections.Generic;
			    using System.Collections.ObjectModel;
			    using System.ComponentModel.DataAnnotations;
			    using System.Linq;
			    using System.Net;
			    using System.Reflection;
			    using System.Threading.Tasks;
			    using DataContext.Core;
			    using Exceptions;
			    using Models;
			    using ReflectionMapper;
			    using UnitOfWork;
			    using Utils;
			    using App;
			    using Pages.CRUDDetail;
			    using Views;
			    using Xamarin.Forms;
			
			    public abstract class XFModelForModelBase<ModelT> : XFModel, IRMapperCustom where ModelT : class, IModel, ISupermodelNotifyPropertyChanged, new()
			    {
			        #region ICustom mapper implementation
			        public virtual object MapFromObjectCustom(object obj, Type objType)
			        {
			            return this.MapFromObjectCustomBase(obj);
			        }
			        public virtual object MapToObjectCustom(object obj, Type objType)
			        {
			            return this.MapToObjectCustomBase(obj);
			        }
			        #endregion
			
			        #region Methods
			        public virtual List<Cell> RenderChildCells<ChildModelT>(Page page, List<ChildModelT> childModels, Func<Page, ChildModelT, Task> onTappedAsync = null)
			            where ChildModelT : ChildModel, new()
			        {
			            var cells = new List<Cell>();
			            foreach (var childModel in childModels)
			            {
			                var dataTemplate = childModel.GetListCellDataTemplate(null);
			
			                var cell = dataTemplate.CreateContent() as Cell;
			                // ReSharper disable once PossibleNullReferenceException
			                cell.BindingContext = childModel;
			
			                if (onTappedAsync != null)
			                {
			                    cell.Tapped += async (sender, args) =>
			                    {
			                        await onTappedAsync(page, childModel);
			                    };
			                }
			
			                cells.Add(cell);
			            }
			            return cells;
			        }
			        public virtual List<Cell> RenderDeletableChildCells<ChildModelT, DataContextT>(Page page, List<ChildModelT> childModels, ObservableCollection<ModelT> parentModels, Func<Page, ChildModelT, Task> onTappedAsync = null)
			            where ChildModelT : ChildModel, new()
			            where DataContextT : class, IDataContext, new()
			        {
			            var crudPage = (IBasicCRUDDetailPage)page;
			
			            var cells = new List<Cell>();
			            foreach (var childModel in childModels)
			            {
			                var dataTemplate = childModel.GetListCellDataTemplate(async (sender, args) => 
			                {
			                    bool connectionLost;
			                    do
			                    {
			                        connectionLost = false;
			                        try
			                        {
			                            using (new ActivityIndicatorFor(crudPage.DetailView))
			                            {
			                                var deletingCell = (Cell)((MenuItem)sender).Parent;
						
			                                using (FormsApplication.GetRunningApp().NewUnitOfWork<DataContextT>())
			                                {
			                                    //var oldToDoItems = Model.ToDoItems;
			                                    var index = Model.DeleteChild(childModel);
			                                    Model.Update();
			                                    try
			                                    {
			                                        await UnitOfWorkContext.FinalSaveChangesAsync();
			                                    }
			                                    catch(Exception)
			                                    {
			                                        Model.AddChild(childModel, index);
			                                        throw;
			                                    }
			                                }
			
			                                //If no issues, Remove the cell from the screen (from all sections)
			                                foreach (var section in crudPage.DetailView.ContentView.Root) section.Remove(deletingCell);
			
			                                //And mark all properteis as changed. This way the list will always update
			                                foreach (var property in Model.GetType().GetTypeInfo().DeclaredProperties) Model.OnPropertyChanged(property.Name);
			                            }
			                        }
			                        catch (SupermodelWebApiException ex1)
			                        {
			                            if (ex1.StatusCode == HttpStatusCode.Unauthorized)
			                            {
			                                FormsApplication.GetRunningApp().HandleUnauthorized();
			                            }
			                            else if (ex1.StatusCode == HttpStatusCode.NotFound)
			                            {
			                                parentModels.RemoveAll(x => x == Model);
			                                await crudPage.DisplayAlert("Not Found", "Item you are trying to update no longer exists.", "Ok");
			                            }
			                            else if (ex1.StatusCode == HttpStatusCode.Conflict)
			                            {
			                                await crudPage.DisplayAlert("Unable to Delete", ex1.ContentJsonMessage, "Ok");
			                            }
			                            else if (ex1.StatusCode == HttpStatusCode.InternalServerError)
			                            {
			                                await crudPage.DisplayAlert("Internal Server Error", ex1.ContentJsonMessage, "Ok");
			                            }
			                            else
			                            {
			                                connectionLost = true;
			                                await crudPage.DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
			                            }
			                        }
			                        catch (SupermodelDataContextValidationException ex2)
			                        {
			                            var vrl = ex2.ValidationErrors;
			                            if (vrl.Count != 1) throw new SupermodelException("vrl.Count != 1. This should never happen!");
			                            //Model = OriginalXFModel.MapTo(Model); //This is where we would normally restore model to the original, but not here
			                            if (!vrl[0].Any()) throw new SupermodelException("!vrl[0].Any(): Server returned validation error with no validation results");
			                            crudPage.GetXFModel().ShowValidationErrors(vrl[0]);
			                        }
			                        catch (WebException)
			                        {
			                            connectionLost = true;
			                            await crudPage.DisplayAlert("Connection Lost", "Connection to the cloud cannot be established.", "Try again");
			                        }
			                        catch (Exception ex3)
			                        {
			                            await crudPage.DisplayAlert("Unexpected Error", ex3.Message, "Try again");
			                            connectionLost = true;
			                        }
			                    }
			                    while (connectionLost);
			                });
			
			                var cell = dataTemplate.CreateContent() as Cell;
			                // ReSharper disable once PossibleNullReferenceException
			                cell.BindingContext = childModel;
			
			                if (onTappedAsync != null)
			                {
			                    cell.Tapped += async (sender, args) =>
			                    {
			                        await onTappedAsync((Page)crudPage, childModel);
			                    };
			                }
			
			                cells.Add(cell);
			            }
			            return cells;
			        }
			        #endregion
			
			        #region Validation
			        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
			        {
			            // ReSharper disable once ConstantNullCoalescingCondition
			            var vr = (ValidationResultList)base.Validate(validationContext) ?? new ValidationResultList();
			            var tempEntityForValidation = CreateTempValidationEntity();
			            Validator.TryValidateObject(tempEntityForValidation, new ValidationContext(tempEntityForValidation), vr); 
			            return vr;
			        }
			        #endregion
			
			        #region Private Helper Methods
			        protected virtual ModelT CreateTempValidationEntity()
			        {
			            return (ModelT)this.MapToObject(new ModelT());
			        }
			        #endregion
			
			        #region Standard Properties
			        //[ScaffoldColumn(false), NotRMapped, NotRCompared] public virtual long Id { get; set; }
			        [ScaffoldColumn(false), NotRMapped, NotRCompared] public ModelT Model { get; protected set; }
			        //[ScaffoldColumn(false), NotRMapped, NotRCompared] public virtual bool IsNew => Id == 0;
			        #endregion
			    }
			}
			#endregion
			
			#region XFModelList
			//// ReSharper disable once CheckNamespace
			//namespace Supermodel.Mobile.XForms.ViewModels
			//{
			//    using System;
			//    using System.Collections.Generic;
			//    using System.Linq;
			//    using ReflectionMapper;
			//    using Models;
			
			//    public class XFModelList<XFModelForModelT, ModelT> : List<XFModelForModelT>, IRMapperCustom
			//        where XFModelForModelT : XFModelForModel<ModelT>, new()
			//        where ModelT : class, IObjectWithIdentity, ISupermodelNotifyPropertyChanged, IModel, new()
			//    {
			//        #region IRMapperCustom implemtation
			//        public virtual object MapFromObjectCustom(object obj, Type objType)
			//        {
			//            Clear();
			//            var modelList = (ICollection<ModelT>)obj;
			//            foreach (var model in modelList.ToList())
			//            {
			//                var xfModel = new XFModelForModelT().MapFrom(model);
			//                Add(xfModel);
			//            }
			//            return this;
			//        }
			//        public virtual object MapToObjectCustom(object obj, Type objType)
			//        {
			//            var modelList = (ICollection<ModelT>)obj;
			
			//            //Add or Update
			//            foreach (var xfModel in this)
			//            {
			//                var modelMatch = modelList.SingleOrDefault(x => x.Identity == xfModel.Identity);
			//                if (modelMatch != null)
			//                {
			//                    xfModel.MapTo(modelMatch);
			//                }
			//                else
			//                {
			//                    var newModel = xfModel.MapTo(new ModelT());
			//                    modelList.Add(newModel);
			//                }
			//            }
			
			//            //Delete
			//            foreach (var toDoItem in modelList.ToList())
			//            {
			//                if (this.All(x => x.Identity != toDoItem.Identity)) toDoItem.Delete();
			//            }
			
			//            //Set Parent for All
			//            //foreach (var model in modelList)
			//            //{
			//            //    model.ParentToDoListId = Id;
			//            //}
			
			//            return modelList;
			//        }
			//        #endregion
			//    }
			//}
			#endregion
			
		#endregion
		
		#region Views
			
			#region ActivityIndicatorFor
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.Views
			{
			    using System;
			    using System.Threading.Tasks;
			    
			    public class ActivityIndicatorFor : IDisposable
			    {
			        public ActivityIndicatorFor(IHaveActivityIndicator element, string message = null, bool showActivityIndicator = true)
			        {
			            Element = element;
			            Element.ActivityIndicatorOn = showActivityIndicator;
			            Element.Message = message;
			        }
			        public static async Task<ActivityIndicatorFor> CreateAsync(IHaveActivityIndicator element, string message = null, bool showActivityIndicator = true)
			        {
			            await element.WaitForPageToBecomeActiveAsync();
			            return new ActivityIndicatorFor(element, message, showActivityIndicator);
			        }
			        
			        public void Dispose()
			        {
			             Element.ActivityIndicatorOn = false;
			        }
			
			        // ReSharper disable once InconsistentNaming
			        public IHaveActivityIndicator Element { get; set; }
			    }
			}
			#endregion
			
			#region IHaveActivityIndicator
			 // ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.Views
			{
			    using System.Threading.Tasks;
			
			    public interface IHaveActivityIndicator
			    {
			        Task WaitForPageToBecomeActiveAsync();
			        bool ActivityIndicatorOn { get; set; }
			        string Message { get; set; }
			    }
			}
			#endregion
			
			#region ViewWithActivityIndicator
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.Views
			{
			    using Xamarin.Forms;
			    using System.Threading.Tasks;
			
			    public class ViewWithActivityIndicator<ContentViewT> : AbsoluteLayout, IHaveActivityIndicator where ContentViewT : View
			    {
			        #region Constructors
			        public ViewWithActivityIndicator(ContentViewT contentView)
			        {
			            ContentView = contentView;
			            ActivityIndicator = new ActivityIndicator();
			            MessageLabel = new Label { TextColor = Device.OnPlatform(Color.Black, Color.White, Color.Black), Text = Message };
			            ActivityIndicatorAndMessageStackLayout = new StackLayout
			            {
			                Orientation = StackOrientation.Vertical,
			                HorizontalOptions = LayoutOptions.CenterAndExpand
			            };
			            ActivityIndicatorAndMessageStackLayout.Children.Add(ActivityIndicator);
			            ActivityIndicatorAndMessageStackLayout.Children.Add(MessageLabel);
			
			            GrayOutOverlay = new BoxView { Color = new Color (0, 0, 0, 0.4) };
			
			            HorizontalOptions = LayoutOptions.FillAndExpand;
			            VerticalOptions = LayoutOptions.FillAndExpand;
			
			            SetLayoutFlags(ContentView, AbsoluteLayoutFlags.All);
			            SetLayoutBounds(ContentView, new Rectangle(0, 0, 1f, 1f));
			
			            SetLayoutFlags(GrayOutOverlay, AbsoluteLayoutFlags.All);
			            SetLayoutBounds(GrayOutOverlay, new Rectangle(0, 0, 1f, 1f));
			
			            SetLayoutFlags(ActivityIndicatorAndMessageStackLayout, AbsoluteLayoutFlags.PositionProportional);
			            SetLayoutBounds(ActivityIndicatorAndMessageStackLayout, new Rectangle(0.5, 0.5, AutoSize, AutoSize));
			            
			            Children.Add(ContentView);
			            Children.Add(GrayOutOverlay);
			            Children.Add(ActivityIndicatorAndMessageStackLayout);
			            ActivityIndicatorOn = false;
			        }
			        #endregion
			
			        #region IHaveActivityIndicator implementation
			        public async Task WaitForPageToBecomeActiveAsync()
			        {
			            while(ContentView == null) await Task.Delay(25);
			        }
			
			        public virtual bool ActivityIndicatorOn
			        {
			            get => _activityIndicatorOn;
			            set
			            {
			                _activityIndicatorOn = value;
			                ActivityIndicator.IsEnabled = ActivityIndicator.IsRunning = ActivityIndicator.IsVisible = GrayOutOverlay.IsEnabled = GrayOutOverlay.IsVisible = value;
			                MessageLabel.IsVisible = value;
			            }
			        }
			        private bool _activityIndicatorOn;
			
			        public virtual string Message 
			        {
			            get => _message;
			            set => _message = MessageLabel.Text = value;
			        }
			        private string _message;
			
			        #endregion
			
			        #region Properties
			        public BoxView GrayOutOverlay { get; set; }
			        public StackLayout ActivityIndicatorAndMessageStackLayout { get; set; }
			        public ActivityIndicator ActivityIndicator { get; set; }
			        public Label MessageLabel { get; set; }
			        
			        public ContentViewT ContentView { get; set; }
			        #endregion
			    }
			}
			#endregion
			
			#region ZoomImage
			// ReSharper disable once CheckNamespace
			namespace Supermodel.Mobile.XForms.Views
			{
			    using System;
			    using Xamarin.Forms;
			
			    public class ZoomImage : Image
			    {
			        #region Constructiors
			        public ZoomImage()
			        {
			            var pinch = new PinchGestureRecognizer();
			            pinch.PinchUpdated += OnPinchUpdated;
			            GestureRecognizers.Add(pinch);
			
			            var pan = new PanGestureRecognizer();
			            pan.PanUpdated += OnPanUpdated;
			            GestureRecognizers.Add(pan);
			
			            var tap = new TapGestureRecognizer { NumberOfTapsRequired = 2 };
			            tap.Tapped += OnTapped;
			            GestureRecognizers.Add(tap);
			
			            Scale = MinScale;
			            TranslationX = TranslationY = 0;
			            AnchorX = AnchorY = 0.5;
			        }
			        #endregion
			
			        #region Overrides
			        protected override SizeRequest OnMeasure(double widthConstraint, double heightConstraint)
			        {
			            Scale = MinScale;
			            TranslationX = TranslationY = 0;
			            AnchorX = AnchorY = 0;
			            return base.OnMeasure(widthConstraint, heightConstraint);
			        }
			
			        protected override void OnSizeAllocated(double width, double height)
			        {
			            base.OnSizeAllocated(width, height);
			
			            // ReSharper disable CompareOfFloatsByEqualityOperator
			            if (_width != width || _height != height)
			            {
			                _width = width;
			                _height = height;
			
			                Scale = MinScale;
			                TranslationX = TranslationY = 0;
			                AnchorX = AnchorY = 0.5;
			
			                this.ScaleTo(MinScale, 250, Easing.CubicInOut);
			                this.TranslateTo(0.5, 0.5, 250, Easing.CubicInOut);
			            }
			            // ReSharper restore CompareOfFloatsByEqualityOperator
			        }
			        #endregion
			
			        #region Methods
			        private void OnTapped(object sender, EventArgs e)
			        {
			            if (Scale > MinScale)
			            {
			                this.ScaleTo(MinScale, 250, Easing.CubicInOut);
			                this.TranslateTo(0, 0, 250, Easing.CubicInOut);
			            }
			            else
			            {
			                AnchorX = AnchorY = 0.5; //TODO tapped position
			                this.ScaleTo(MaxScale, 250, Easing.CubicInOut);
			            }
			        }
			        private void OnPanUpdated(object sender, PanUpdatedEventArgs e)
			        {
			            switch (e.StatusType)
			            {
			                case GestureStatus.Started:
			                {
			                    _lastX = (1 - AnchorX) * Width;
			                    _lastY = (1 - AnchorY) * Height;
			                    break;
			                }
			                case GestureStatus.Running:
			                {
			                    AnchorX = Clamp(1 - (_lastX + e.TotalX) / Width, 0, 1);
			                    AnchorY = Clamp(1 - (_lastY + e.TotalY) / Height, 0, 1);
			                    break;
			                }
			            }
			        }
			        private void OnPinchUpdated(object sender, PinchGestureUpdatedEventArgs e)
			        {
			            switch (e.Status)
			            {
			                case GestureStatus.Started:
			                {
			                    _startScale = Scale;
			                    AnchorX = e.ScaleOrigin.X;
			                    AnchorY = e.ScaleOrigin.Y;
			                    break;
			                }
			                case GestureStatus.Running:
			                {
			                    var current = Scale + (e.Scale - 1) * _startScale;
			                    Scale = Clamp(current, MinScale * (1 - Overshoot), MaxScale * (1 + Overshoot));
			                    break;
			                }
			                case GestureStatus.Completed:
			                {
			                    if (Scale > MaxScale) this.ScaleTo(MaxScale, 250, Easing.SpringOut);
			                    else if (Scale < MinScale) this.ScaleTo(MinScale, 250, Easing.SpringOut);
			                    break;
			                }
			            }
			        }
			
			        private T Clamp<T>(T value, T minimum, T maximum) where T : IComparable
			        {
			            if (value.CompareTo(minimum) < 0) return minimum;
			            else if (value.CompareTo(maximum) > 0) return maximum;
			            else return value;
			        }
			        #endregion
			
			        #region Properties
			        private const double MinScale = 1;
			        private const double MaxScale = 4;
			        private const double Overshoot = 0.15;
			        private double _startScale;
			        private double _lastX, _lastY;
			
			        private double _width;
			        private double _height;
			        #endregion
			    }
			}
			#endregion
			
		#endregion
		
		#region XFormsSettings
		// ReSharper disable once CheckNamespace
		namespace Supermodel.Mobile.XForms
		{
		    using Xamarin.Forms;
		
		    public static class XFormsSettings
		    {
		        public static int LabelFontSize { get; set; } = 14;
		        public static Color LabelTextColor { get; set; } = Color.RoyalBlue;
		
		        public static int ValueFontSize { get; set; } = 14;
		        public static Color ValueTextColor { get; set; } = Device.OnPlatform(Color.Black, Color.White, Color.Black);
		
		        public static Color DisabledTextColor { get; set; } = Color.LightGray;
		        public static Color SwitchOnColor { get; set; } = Color.RoyalBlue;
		        public static Color RequiredAsteriskColor { get; set; } = Color.Red;
		
		        public static int MultiLineTextBoxCellHeight { get; set; } = 120;
		        public static int MultiLineTextBoxReadOnlyCellHeight { get; set; } = 120;
		
		        public static string AddNewImageFileName { get; set; }
		
		        public static int MultiLineTextLabelHeight = 40;
		    }
		}
		#endregion
		
	#endregion
	
#endregion

