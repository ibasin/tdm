using System;
using System.IO;
using Domain.Supermodel.Persistance;

namespace ModelGenerator
{
    class Program
    {
        static void Main()
        {
            var modelGenerator = new Supermodel.Mobile.Xamarin.ModelGenerator(new[] { typeof(Web.Global).Assembly, typeof(DataInfoInitializer).Assembly });
            var sb = modelGenerator.GenerateModels();
            File.WriteAllText(@"..\..\" + "Supermodel.Mobile.ModelsAndRuntime.cs", sb.ToString());
            File.WriteAllText(@"..\..\..\..\Mobile\TDM.Shared.Online\Supermodel\Runtime\Supermodel.Mobile.ModelsAndRuntime.cs", sb.ToString());
            File.WriteAllText(@"..\..\..\..\Mobile\TDM.Shared.Offline\Supermodel\Runtime\Supermodel.Mobile.ModelsAndRuntime.cs", sb.ToString());
            File.WriteAllText(@"..\..\..\..\Mobile\TDMClient.Cmd\Supermodel\Runtime\Supermodel.Mobile.ModelsAndRuntime.cs", sb.ToString());

            Console.WriteLine("All done! Press enter...");
            Console.ReadLine();
        }
    }
}
