using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using Encryptor;
using Supermodel.DDD.Repository;
using Supermodel.MvcAndWebApi;

namespace TDM.Cmd
{
    class Program
    {
        static void Main(string[] args)
        {
            //var token = HttpAuthAgent.CreateBasicAuthToken("ilya.basin@noblis.org", "1234");
            //Console.WriteLine($"Authorization: {token}");
            //Console.WriteLine("Press enter");
            //Console.ReadLine();
            //return;

            InitializerManager.InitPerConfig(new List<IDatabaseInitializer<TDMDbContext>>
            {
                new TDMCreateDatabaseIfNotExists(),
                new TDMDropCreateDatabaseIfModelChanges(),
                new TDMDropCreateDatabaseAlways()
            });

            using (new TDMUnitOfWork())
            {
                var repo = (ISqlLinqEFDataRepo<TDMUser>)RepoFactory.Create<TDMUser>();
                var user = repo.Items.Single(x => x.Username == "ilya.basin@noblis.org");

                user.ToDoLists.Add(new ToDoList
                {
                    Name = "List #1",
                    ToDoItems = new List<ToDoItem>
                    {
                        new ToDoItem { Name = "Item 1" },
                        new ToDoItem { Name = "Item 2" }
                    }
                });
                user.ToDoLists.Add(new ToDoList
                {
                    Name = "Groceries",
                    ToDoItems = new List<ToDoItem>
                    {
                        new ToDoItem { Name = "Bread" },
                        new ToDoItem { Name = "Eggs" },
                        new ToDoItem { Name = "Milk" }
                    }
                });
                user.ToDoLists.Add(new ToDoList
                {
                    Name = "Supplies",
                    ToDoItems = new List<ToDoItem>
                    {
                        new ToDoItem { Name = "Pens" },
                        new ToDoItem { Name = "Pencils" },
                        new ToDoItem { Name = "Staplers" }
                    }
                });

                Console.WriteLine("Press Any Key...");
                Console.ReadKey(true);
            }
        }
    }
}
