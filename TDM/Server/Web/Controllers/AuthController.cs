using System.Web.Mvc;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.Extensions;
using Supermodel.MvcAndWebApi.Controllers.MvcControllers.Sync;

namespace Web.Controllers
{
    public class AuthController : SyncMvcAuthController<TDMUser, LoginMvcModel, TDMDbContext>
    {
        #region Action Method Overrdies
        protected override ActionResult RedirectToHomeScreen(TDMUser user)
        {
            return this.Supermodel().RedirectToActionStrong<HomeController>(x => x.Index());
        }
        #endregion
    }
}