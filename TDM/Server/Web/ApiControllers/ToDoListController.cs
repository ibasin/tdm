﻿using System;
using System.Linq;
using System.Web;
using System.Web.Http;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using Supermodel.DDD.UnitOfWork;
using Supermodel.MvcAndWebApi.Controllers.ApiControllers;
using Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync;

namespace Web.ApiControllers
{
    [Authorize]
    public class ToDoListController : SyncEnhancedApiCRUDController<ToDoList, ToDoListApiModel, SimpleSearchApiModel, TDMDbContext>
    {
        protected override IQueryable<ToDoList> GetItems()
        {
            var currentUserId = TDMUser.GetCurrentUserId(HttpContext.Current);
            return base.GetItems().Where(x => x.ListOwnerId == currentUserId);
        }
        protected override IQueryable<ToDoList> ApplySearchBy(IQueryable<ToDoList> items, SimpleSearchApiModel searchBy)
        {
            items = base.ApplySearchBy(items, searchBy);
            var searchTerm = searchBy.SearchTerm;
            if (!string.IsNullOrEmpty(searchTerm)) items = items.Where(l => l.Name.Contains(searchTerm) || l.ToDoItems.Any(i => i.Name.Contains(searchTerm)));
            return items;
        }

        protected override void AfterCreate(ToDoList entityItem, ToDoListApiModel apiModelItem)
        {
            ValidateListOwnerId(entityItem);
            base.AfterCreate(entityItem, apiModelItem);
        }

        protected override void AfterUpdate(long id, ToDoList entityItem, ToDoListApiModel apiModelItem)
        {
            ValidateListOwnerId(entityItem);
            base.AfterUpdate(id, entityItem, apiModelItem);
        }

        protected override void AfterDelete(long id, ToDoList entityItem)
        {
            ValidateListOwnerId(entityItem);
            base.AfterDelete(id, entityItem);
        }

        protected void ValidateListOwnerId(ToDoList entityItem)
        {
            if (entityItem.ListOwnerId != TDMUser.GetCurrentUserId(HttpContext.Current))
            {
                UnitOfWorkContext.CommitOnDispose = false;
                throw new Exception("entityItem.ListOwnerId != TDMUser.GetCurrentUserId(HttpContext.Current)");
            }
        }
    }
}
