﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using EntityFramework.Extensions;
using Supermodel.DDD.Repository;
using Supermodel.DDD.UnitOfWork;
using Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync;

namespace Web.ApiControllers
{
    public class DeleteAllBeforeInput
    {
        public DateTime OlderThanUtc { get; set; }
    }

    public class DeleteAllBeforeOutput
    {
        public long DeletedCount { get; set; }
    }

    [Authorize]
    public class DeleteAllBeforeController : SyncApiCommandController<DeleteAllBeforeInput, DeleteAllBeforeOutput>
    {
        protected override DeleteAllBeforeOutput Execute(DeleteAllBeforeInput input)
        {
            using (new TDMUnitOfWork(ReadOnly.Yes))
            {
                var repo = (ISqlLinqEFDataRepo<ToDoList>)RepoFactory.Create<ToDoList>();
                var query = repo.Items.Where(x => x.ModifiedOnUtc <= input.OlderThanUtc);
                var count = query.Delete();
                return new DeleteAllBeforeOutput { DeletedCount = count };
            }
        }
    }
}
