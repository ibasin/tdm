using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using Supermodel.MvcAndWebApi.Controllers.ApiControllers;
using Supermodel.MvcAndWebApi.Controllers.ApiControllers.Sync;
using Supermodel.MvcAndWebApi.WebApiValidation;

namespace Web.ApiControllers
{
    public class TDMUserUpdatePasswordController : SyncApiCRUDController<TDMUser, TDMUserUpdatePasswordApiModel, TDMDbContext>
    {
        #region Action Methods
        public override HttpResponseMessage Put(long id, TDMUserUpdatePasswordApiModel apiModelItem)
        {
            //Check if the old password matches
            var user = TDMUser.GetCurrentUser(HttpContext.Current);
            if (user.Id != id  || !user.PasswordEquals(apiModelItem.OldPassword))
            {
                var error = new ValidationErrorsApiModel.Error
                {
                    Name = "OldPassword",
                    ErrorMessages = new List<string> {"Incorrect Old Password!"}
                };
                var validationErrors = new ValidationErrorsApiModel { error };
                return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, validationErrors);
            }
            return base.Put(id, apiModelItem);
        }
        #endregion

        #region Overrides
        protected override ValidateLoginResponseApiModel GetValidateLoginResponse()
        {
            var currentUser = TDMUser.GetCurrentUser(HttpContext.Current);
            return new ValidateLoginResponseApiModel { UserId = currentUser.Id, UserLabel = currentUser.FirstName + " " + currentUser.LastName };
        }
        #endregion

        #region Disabled Action Methods (we just need put and validate login)
        public override HttpResponseMessage All(int? smSkip = null, int? smTake = null) { throw new InvalidOperationException(); }
        public override HttpResponseMessage CountAll(int? smSkip = null, int? smTake = null) { throw new InvalidOperationException(); }
        public override HttpResponseMessage Get(long id)  { throw new InvalidOperationException(); }
        public override HttpResponseMessage Delete(long id)  { throw new InvalidOperationException(); }
        public override HttpResponseMessage Post(TDMUserUpdatePasswordApiModel apiModelItem) { throw new InvalidOperationException(); }
        #endregion
    }
}