using System.Web.Mvc;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using Supermodel.DDD.Models.View.Mvc.JQMobile;
using Supermodel.Extensions;
using Supermodel.MvcAndWebApi.Controllers.MvcControllers.Sync;

namespace Web.JQM.Controllers
{
    public class AuthController : SyncMvcAuthController<TDMUser, JQMobile.LoginMvcModel, TDMDbContext>
    {
        #region Action Method Overrides
        protected override ActionResult RedirectToHomeScreen(TDMUser user)
        {
            return this.Supermodel().RedirectToActionStrong<HomeController>(x => x.Index());
        }
        #endregion
    }
}