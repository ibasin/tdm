﻿using System.Web.Mvc;

namespace Web.JQM.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        #region Action Methods
        public ActionResult Index()
        {
            return View();
        }
        #endregion
    }
}