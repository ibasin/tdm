﻿using System;
using System.Linq;
using System.Web.Mvc;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using Supermodel.DDD.Repository;
using Supermodel.Extensions;
using Supermodel.MvcAndWebApi;
using Supermodel.MvcAndWebApi.Controllers.MvcControllers.Sync;

namespace Web.JQM.Controllers
{
    [Authorize]
    public class ToDoItemController : SyncMvcChildCRUDController<ToDoItem, ToDoList, ToDoItemJQMMvcModel, TDMDbContext>
    {
        #region Acgtion Methods
        public override ActionResult List(long? parentId)
        {
            if (parentId == null) throw new Exception("ToDoItemController.List(): parentId == null");
            var route = HttpContext.Request.QueryString.Supermodel().ToRouteValueDictionary();
            return this.Supermodel().RedirectToActionStrong<ToDoListController>(x => x.Detail((long)parentId, new HttpGet()), route);
        }
        #endregion

        #region Overrides
        protected override IQueryable<ToDoItem> GetItems()
        {
            var currentUserId = TDMUser.GetCurrentUserId(HttpContext);
            return base.GetItems().Where(x => x.ParentToDoList.ListOwnerId == currentUserId);
        }

        protected override ActionResult AfterDelete(long id, long parentId, ToDoItem entityItem)
        {
            UpdateParentModifiedDate(parentId, entityItem);
            return base.AfterDelete(id, parentId, entityItem);
        }
        protected override ActionResult AfterUpdate(long id, ToDoItem entityItem, ToDoItemJQMMvcModel mvcModelItem)
        {
            UpdateParentModifiedDate(entityItem.ParentToDoListId, entityItem);
            return base.AfterUpdate(id, entityItem, mvcModelItem);
        }
        protected override ActionResult AfterCreate(long id, long parentId, ToDoItem entityItem, ToDoItemJQMMvcModel mvcModelItem)
        {
            UpdateParentModifiedDate(entityItem.ParentToDoListId, entityItem);
            return base.AfterCreate(id, parentId, entityItem, mvcModelItem);
        }
        protected void UpdateParentModifiedDate(long parentId, ToDoItem entityItem)
        {
            if (entityItem.ParentToDoList != null) entityItem.ParentToDoList.ModifiedOnUtc = DateTime.UtcNow;
            else RepoFactory.Create<ToDoList>().GetById(parentId).ModifiedOnUtc = DateTime.UtcNow;
        }
        #endregion
    }
}