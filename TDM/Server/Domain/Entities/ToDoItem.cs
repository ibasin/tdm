﻿using System;
using System.ComponentModel.DataAnnotations;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc.JQMobile;
using Supermodel.DDD.Models.View.Mvc.TwitterBS;
using Supermodel.DDD.Models.View.WebApi;
using System.Data.Entity;
using Domain.Supermodel.Persistance;
using Supermodel.DDD.Repository;

namespace Domain.Entities
{
    public enum PriorityEnum { High, Medium, Low }

    public class ToDoItemApiModel : ApiModelForEntity<ToDoItem>
    {
        #region Properties
        [Required] public string Name { get; set; }
        public PriorityEnum? Priority { get; set; }
        public DateTime? DueOn { get; set; }
        public bool Completed { get; set; }
        #endregion
    }

    public class ToDoItemJQMMvcModel : JQMobile.ChildMvcModelForEntity<ToDoItem, ToDoList>
    {
        #region Overrides
        public override string Label => Name.Value + (Completed.Value ? " (Completed)" : "");
        public override ToDoList GetParentEntity(ToDoItem entity)
        {
            return entity.ParentToDoList;
        }
        public override void SetParentEntity(ToDoItem entity, ToDoList parent)
        {
            entity.ParentToDoList = parent;
        }
        #endregion

        #region Properties
        [Required] public JQMobile.TextBoxForStringMvcModel Name { get; set; }
        public JQMobile.RadioSelectHorizontalMvcModelUsingEnum<PriorityEnum> Priority { get; set; }
        public JQMobile.DateMvcModel DueOn { get; set; }
        public JQMobile.ToggleSwitchMvcModel Completed { get; set; }
        #endregion
    }


    public class ToDoItemTBSMvcModel : TwitterBS.ChildMvcModelForEntity<ToDoItem, ToDoList>
    {
        #region Overrides
        public override string Label => Name.Value + (Completed.Value ? " (Completed)" : "");
        public override ToDoList GetParentEntity(ToDoItem entity)
        {
            return entity.ParentToDoList;
        }
        public override void SetParentEntity(ToDoItem entity, ToDoList parent)
        {
            entity.ParentToDoList = parent;
        }
        #endregion

        #region Properties
        [Required] public TwitterBS.TextBoxForStringMvcModel Name { get; set; }
        public TwitterBS.DropdownMvcModelUsingEnum<PriorityEnum> Priority { get; set; }
        public TwitterBS.DateMvcModel DueOn { get; set; }
        public TwitterBS.CheckboxMvcModel Completed { get; set; }
        #endregion
    }

    public class ToDoItem : Entity
    {
        #region Properties
        public long ParentToDoListId { get; set; }
        public virtual ToDoList ParentToDoList { get; set; }

        [Required] public string Name { get; set; }
        public PriorityEnum? Priority { get; set; }
        public DateTime? DueOn { get; set; }
        public bool Completed { get; set; }
        #endregion
    }
}
