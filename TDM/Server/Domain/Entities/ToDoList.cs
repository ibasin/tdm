﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using ReflectionMapper;
using Supermodel;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.JQMobile;
using Supermodel.DDD.Models.View.Mvc.TwitterBS;
using Supermodel.DDD.Models.View.WebApi;

namespace Domain.Entities
{
    public class ToDoListApiModel : ApiModelForEntity<ToDoList>
    {
        #region Properties
        [Required] public DateTime CreatedOnUtc { get; set; }
        [Required] public DateTime ModifiedOnUtc { get; set; }

        [Required] public long ListOwnerId { get; set; }
        [Required] public string Name { get; set; }

        public ApiModelList<ToDoItemApiModel, ToDoItem> ToDoItems { get; set; }
        #endregion
    }

    public class ToDoListJQMMvcModel : JQMobile.MvcModelForEntity<ToDoList>
    {
        #region Overrdies
        public override string Label => Name.Value;
        #endregion

        #region Properties
        [Required] public JQMobile.TextBoxForStringMvcModel Name { get; set; }
        [ScaffoldColumn(false), NotRMappedTo] public MvcModelList<ToDoItemJQMMvcModel, ToDoItem> ToDoItems { get; set; } = new MvcModelList<ToDoItemJQMMvcModel, ToDoItem>();
        #endregion
    }

    public class ToDoListTBSMvcModel : TwitterBS.MvcModelForEntity<ToDoList>
    {
        #region Overrdies
        public override string Label => Name.Value;
        #endregion

        #region Properties
        [Required] public TwitterBS.TextBoxForStringMvcModel Name { get; set; }
        [ScaffoldColumn(false), NotRMappedTo] public MvcModelList<ToDoItemTBSMvcModel, ToDoItem> ToDoItems { get; set; } = new MvcModelList<ToDoItemTBSMvcModel, ToDoItem>();
        #endregion
    }

    public class ToDoList : Entity
    {
        #region Constrcutors
        public ToDoList()
        {
            CreatedOnUtc = ModifiedOnUtc = DateTime.UtcNow;
        }
        #endregion

        #region Overrides
        //public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    var vr = (ValidationResultList)base.Validate(validationContext) ?? new ValidationResultList();
        //    if (Name.Length <= 1) vr.AddValidationResult(this, x => x.Name, "Name must be longer than one character");
        //    return vr;
        //}
        protected override void DeleteInternal()
        {
            if (ToDoItems.Any()) throw new UnableToDeleteException("Must delete all items in the To Do List first");
            //foreach (var toDoItem in ToDoItems.ToList())
            //{
            //    toDoItem.Delete();
            //}
            base.DeleteInternal();
        }
        public override void BeforeSave(EntityState entityState)
        {
            if (entityState != EntityState.Unchanged) ModifiedOnUtc = DateTime.UtcNow;
            base.BeforeSave(entityState);
        }

        #endregion

        #region Properties
        [Required] public DateTime CreatedOnUtc { get; set; }
        [Required] public DateTime ModifiedOnUtc { get; set; }

        public long ListOwnerId { get; set; }
        public virtual TDMUser ListOwner { get; set; }

        [Required] public string Name { get; set; }
        public virtual List<ToDoItem> ToDoItems { get; set; } = new List<ToDoItem>();
        #endregion
    }
}
