using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Supermodel.Persistance;
using Encryptor;
using ReflectionMapper;
using Supermodel.DDD.Models.Domain;
using Supermodel.DDD.Models.View.Mvc;
using Supermodel.DDD.Models.View.Mvc.JQMobile;
using Supermodel.DDD.Models.View.Mvc.Metadata;
using Supermodel.DDD.Models.View.Mvc.TwitterBS;
using Supermodel.DDD.Models.View.WebApi;

namespace Domain.Entities
{
    public class TDMUserUpdatePasswordApiModel : ApiModelForEntity<TDMUser>
    {
        #region Overrides
        public override object MapToObjectCustom(object obj, Type objType)
        {
            var user = (TDMUser)obj;
            user.Password = NewPassword;
            return base.MapToObjectCustom(obj, objType);
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //we don't want to the domain-level validation to be called here (which called by the base method) because we don't fill that object completely
            // ReSharper disable once CollectionNeverUpdated.Local
            var vr = new ValidationResultList();
            return vr;
        }
        #endregion
        
        #region Properties
        [Required, NotRMapped] public string OldPassword { get; set; }

        [Required, NotRMapped, MustEqualTo("ConfirmPassword", ErrorMessage = "Passwords do not Match")]
        public string NewPassword { get; set; }

        [Required, NotRMapped, MustEqualTo("NewPassword", ErrorMessage = "Passwords do not Match")]
        public string ConfirmPassword { get; set; }
        #endregion
    }

    public class TDMUserUpdatePasswordMvcModel : MvcModelForEntity<TDMUser>
    {
        #region Overrides
        public override string Label => "N/A";
        public override object MapToObjectCustom(object obj, Type objType)
        {
            var user = (TDMUser)obj;
            user.Password = NewPassword;
            return base.MapToObjectCustom(obj, objType);
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //we don't want to the domain-level validation to be called here (which called by the base method) because we don't fill that object completely
            // ReSharper disable once CollectionNeverUpdated.Local
            var vr = new ValidationResultList();
            return vr;
        }
        #endregion

        #region Properties
        [Required, DataType(DataType.Password), NotRMapped] public string OldPassword { get; set; }

        [Required, DataType(DataType.Password), NotRMapped, MustEqualTo("ConfirmPassword", ErrorMessage = "Passwords do not Match")]
        public string NewPassword { get; set; }

        [Required, DataType(DataType.Password), NotRMapped, MustEqualTo("NewPassword", ErrorMessage = "Passwords do not Match")]
        public string ConfirmPassword { get; set; }
        #endregion
    }

    public class TDMUserUpdatePasswordJQMMvcModel : JQMobile.MvcModelForEntity<TDMUser>
    {
        #region Overrides
        public override string Label => "N/A";
        public override object MapToObjectCustom(object obj, Type objType)
        {
            var user = (TDMUser)obj;
            user.Password = NewPassword;
            return base.MapToObjectCustom(obj, objType);
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //we don't want to the domain-level validation to be called here (which called by the base method) because we don't fill that object completely
            // ReSharper disable once CollectionNeverUpdated.Local
            var vr = new ValidationResultList();
            return vr;
        }
        #endregion

        #region Properties
        [Required, DataType(DataType.Password), NotRMapped] public string OldPassword { get; set; }

        [Required, DataType(DataType.Password), NotRMapped, MustEqualTo("ConfirmPassword", ErrorMessage = "Passwords do not Match")]
        public string NewPassword { get; set; }

        [Required, DataType(DataType.Password), NotRMapped, MustEqualTo("NewPassword", ErrorMessage = "Passwords do not Match")]
        public string ConfirmPassword { get; set; }
        #endregion
    }

    public class TDMUserUpdatePasswordTBSMvcModel : TwitterBS.MvcModelForEntity<TDMUser>
    {
        #region Overrides
        public override string Label => "N/A";
        public override object MapToObjectCustom(object obj, Type objType)
        {
            var user = (TDMUser)obj;
            user.Password = NewPassword;
            return base.MapToObjectCustom(obj, objType);
        }
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            //we don't want to the domain-level validation to be called here (which called by the base method) because we don't fill that object completely
            // ReSharper disable once CollectionNeverUpdated.Local
            var vr = new ValidationResultList();
            return vr;
        }
        #endregion

        #region Properties
        [Required, DataType(DataType.Password), NotRMapped] public string OldPassword { get; set; }

        [Required, DataType(DataType.Password), NotRMapped, MustEqualTo("ConfirmPassword", ErrorMessage = "Passwords do not Match")]
        public string NewPassword { get; set; }

        [Required, DataType(DataType.Password), NotRMapped, MustEqualTo("NewPassword", ErrorMessage = "Passwords do not Match")]
        public string ConfirmPassword { get; set; }
        #endregion
    }

    public class TDMUser : UserEntity<TDMUser, TDMDbContext>
    {
        #region Overrdies
        public override string HashPassword(string password, string hash)
        {
            return HashAgent.HashPasswordSHA256(password, hash);
        }
        #endregion

        #region Properties
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual List<ToDoList> ToDoLists { get; set; } = new List<ToDoList>();
        #endregion
    }
}
