using Supermodel.DDD.UnitOfWork;

namespace Domain.Supermodel.Persistance
{
    public class TDMUnitOfWorkIfNoAmbientContext : UnitOfWorkIfNoAmbientContext<TDMDbContext>
    {
        #region Constructors
        public TDMUnitOfWorkIfNoAmbientContext(MustBeWritable mustBeWritable) : base(mustBeWritable) { }
        #endregion
    }
}
