using Supermodel.DDD.UnitOfWork;

namespace Domain.Supermodel.Persistance
{
    public class TDMUnitOfWork : UnitOfWork<TDMDbContext>
    {
        #region Constructors
        public TDMUnitOfWork(ReadOnly readOnly = ReadOnly.No) : base(readOnly) { }
        #endregion
    }
}
