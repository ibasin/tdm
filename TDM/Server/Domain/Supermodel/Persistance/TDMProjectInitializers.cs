using System.Data.Entity;
using Domain.Entities;

namespace Domain.Supermodel.Persistance
{
    public static class DataInfoInitializer
    {
        #region Methods
        public static void Seed(TDMDbContext context)
        {
            var users = new[]
            {
                new TDMUser
                {
                    FirstName = "Ilya",
                    LastName = "Basin",
                    Username = "ilya.basin@noblis.org",
                    Password = "1234"
                },
                new TDMUser
                {
                    FirstName = "Emily",
                    LastName = "Goldfein",
                    Username = "emily.goldfein@noblis.org",
                    Password = "1234"
                }
            };
            foreach (var user in users) context.Set<TDMUser>().Add(user);
        }
        #endregion
    }

    public class TDMDropCreateDatabaseIfModelChanges : DropCreateDatabaseIfModelChanges<TDMDbContext>
    {
        #region Overrides
        protected override void Seed(TDMDbContext context)
        {
            base.Seed(context);
            DataInfoInitializer.Seed(context);
        }
        #endregion
    }

    public class TDMDropCreateDatabaseAlways : DropCreateDatabaseAlways<TDMDbContext>
    {
        #region Overrides
        protected override void Seed(TDMDbContext context)
        {
            base.Seed(context);
            DataInfoInitializer.Seed(context);
        }
        #endregion
    }

    public class TDMCreateDatabaseIfNotExists : CreateDatabaseIfNotExists<TDMDbContext>
    {
        #region Overrides
        protected override void Seed(TDMDbContext context)
        {
            base.Seed(context);
            DataInfoInitializer.Seed(context);
        }
        #endregion
    }
}
