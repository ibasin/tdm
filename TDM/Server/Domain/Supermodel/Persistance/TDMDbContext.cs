using Supermodel.DDD.UnitOfWork;

namespace Domain.Supermodel.Persistance
{
    public class TDMDbContext : EFDbContext
    {
        #region Constructors
        public TDMDbContext() : base("TDMDb") {}
        #endregion
    }
}
