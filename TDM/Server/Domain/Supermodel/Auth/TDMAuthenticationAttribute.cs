using System;
using System.Linq;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using Supermodel.DDD.Repository;
using Supermodel.DDD.UnitOfWork;
using Supermodel.MvcAndWebApi.Auth.WebApi;

namespace Domain.Supermodel.Auth
{
    public class TDMAuthenticateAttribute: AuthenticateAttribute
    {
        #region Overrides
        protected override string AuthenticateBasicAndGetIdentityName(string username, string password)
        {
            using (new TDMUnitOfWork(ReadOnly.Yes))
            {
                var repo = (ISqlLinqEFDataRepo<TDMUser>)RepoFactory.Create<TDMUser>();
                var user = repo.Items.SingleOrDefault(u => u.Username == username);
                
                //if login does not exist or password does not match
                if (user == null || !user.PasswordEquals(password)) return null;
                
                return user.Id.ToString();
            }
        }

        protected override string AuthenticateEncryptedAndGetIdentityName(string[] args, out string password)
        {
            throw new NotImplementedException();
        }

        protected override byte[] EncryptionKey => throw new NotImplementedException();
        #endregion
    }
}
