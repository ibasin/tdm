using System;
using System.Collections.Generic;
using System.Web.Security;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using Supermodel.DDD.Repository;
using Supermodel.DDD.UnitOfWork;

namespace Domain.Supermodel.Auth
{
    public class TDMRoleProvider : RoleProvider
    {
        #region Overrides
        //For this class we only need to override the GetRolesForUser method
        public override string[] GetRolesForUser(string userIdStr)
        {
            using (new TDMUnitOfWork(ReadOnly.Yes))
            {
                var repo = RepoFactory.Create<TDMUser>();
                var user = repo.GetById(long.Parse(userIdStr));
                if (user == null) return new string[] { };

                // ReSharper disable once CollectionNeverUpdated.Local
                var roles = new List<string>();
                //if (user.Admin) roles.Add("Admin");
                return roles.ToArray();
            }
        }
        #endregion

        #region Not Implemented Overrides
        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }
        #endregion
    }
}
