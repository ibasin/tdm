using System;
using System.Linq;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using Encryptor;
using Supermodel.DDD.Repository;
using Supermodel.DDD.UnitOfWork;
using Supermodel.MvcAndWebApi.Auth.WebApi;

namespace Domain.Supermodel.Auth
{
    public class TDMSecureAuthenticateAttribute: AuthenticateAttribute
    {
        #region Constants
        public static readonly byte[] Key = { 0xAA, 0x68, 0x12, 0xB1, 0x35, 0x22, 0x51, 0xA0, 0xB2, 0x41, 0x27, 0x5C, 0x23, 0x9C, 0xF0, 0xDD };
        public static readonly string HeaderName = "X-TDM-Authorization";
        public static readonly string SecretToken = "JHFV_jhaegvdkjHGVBKJDHgbejdfh**&@$vJHgvkzsdhfbgkb37r3t84r7glSCGO834FG{YD^%^$";
        #endregion

        #region Overrides
        protected override string AuthenticateBasicAndGetIdentityName(string username, string password)
        {
            throw new InvalidOperationException("Basic HTTP Authentication is not supported"); 
        }

        protected override string AuthenticateEncryptedAndGetIdentityName(string[] args, out string password)
        {
            if (args.Length != 4)
            {
                password = "";
                return null;
            }

            using (new TDMUnitOfWork(ReadOnly.Yes))
            {
                var utcNow = DateTime.UtcNow;

                var username = args[0];
                password = args[1];
                var secretTokenHash = args[2];
                var secretTokenHashSalt = args[3];

                var repo = (ISqlLinqEFDataRepo<TDMUser>)RepoFactory.Create<TDMUser>();
                var user = repo.Items.SingleOrDefault(x => x.Username == username);

                var secretTokenValid = false;

                if (user != null && !string.IsNullOrEmpty(user.Username))
                {
                    var dateTimeSalt = HashAgent.Generate5MinTimeStampSalt(utcNow.AddMinutes(-5));
                    if (HashAgent.HashPasswordSHA256(SecretToken + dateTimeSalt, secretTokenHashSalt) == secretTokenHash) secretTokenValid = true;

                    dateTimeSalt = HashAgent.Generate5MinTimeStampSalt(utcNow);
                    if (HashAgent.HashPasswordSHA256(SecretToken + dateTimeSalt, secretTokenHashSalt) == secretTokenHash) secretTokenValid = true;

                    dateTimeSalt = HashAgent.Generate5MinTimeStampSalt(utcNow.AddMinutes(5));
                    if (HashAgent.HashPasswordSHA256(SecretToken + dateTimeSalt, secretTokenHashSalt) == secretTokenHash) secretTokenValid = true;
                }

                if (user == null || !user.PasswordEquals(password) || !secretTokenValid) return null;

                return user.Id.ToString();
            }
        }

        protected override byte[] EncryptionKey => Key;
        protected override string Realm => "TDM";
        protected override string AuthHeaderName => HeaderName;
        #endregion
    }
}
