using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;

using Domain.Supermodel.Persistance;
using Supermodel;
using Supermodel.MvcAndWebApi;

namespace Web.TBS
{
    public class Global : HttpApplication
    {
        #region Methods
        void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            SupermodelInitialization.WebInit<TDMDbContext>();
            InitializerManager.InitPerConfig(new List<IDatabaseInitializer<TDMDbContext>>
            {
                new TDMCreateDatabaseIfNotExists(),
                new TDMDropCreateDatabaseIfModelChanges(),
                new TDMDropCreateDatabaseAlways()
            });
        }
        #endregion
    }
}