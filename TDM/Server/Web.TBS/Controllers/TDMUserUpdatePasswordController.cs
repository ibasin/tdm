using System;
using System.Web.Mvc;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using Supermodel.Extensions;
using Supermodel.MvcAndWebApi;
using Supermodel.MvcAndWebApi.Controllers.MvcControllers.Sync;

namespace Web.TBS.Controllers
{
    [Authorize]
    public class TDMUserUpdatePasswordController : SyncMvcCRUDController<TDMUser, TDMUserUpdatePasswordTBSMvcModel, TDMDbContext>
    {
        #region Action Method Overrdies
        public override ActionResult Detail(long id, HttpGet ignore)
        {
            var userId = TDMUser.GetCurrentUserId(HttpContext);
            if (userId == null) throw new UnauthorizedAccessException();
            return base.Detail((long)userId, new HttpGet());
        }

        public override ActionResult Detail(long id, HttpPut ignore)
        {
            //Check if the old password matches
            var mvcModelItem = new TDMUserUpdatePasswordTBSMvcModel();
            TryUpdateModel(mvcModelItem);
            var user = TDMUser.GetCurrentUser(HttpContext);
            if (user.Id != id || !user.PasswordEquals(mvcModelItem.OldPassword))
            {
                TempData.Supermodel().NextPageModalMessage = "Incorrect Old Password!";
                return this.Supermodel().RedirectToActionStrong(x => x.Detail(id, new HttpGet()));
            }
            return base.Detail(user.Id, ignore);
        }

        protected override ActionResult AfterUpdate(long id, TDMUser entityItem, TDMUserUpdatePasswordTBSMvcModel mvcModelItem)
        {
            TempData.Supermodel().NextPageModalMessage = "Password Updated Successfully!";
            return this.Supermodel().RedirectToActionStrong(x => x.Detail(id, new HttpGet()));
        }
        #endregion

        #region Diasbled Action methods
        public override ActionResult List() { throw new InvalidOperationException(); }
        public override ActionResult Detail(long id, HttpDelete ignore) { throw new InvalidOperationException(); }
        public override ActionResult Detail(long id, HttpPost ignore) { throw new InvalidOperationException(); }
        public override ActionResult DeleteBinaryFile(long id, string pn) { throw new InvalidOperationException(); }
        #endregion
    }
}