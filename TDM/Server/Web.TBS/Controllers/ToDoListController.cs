﻿using System.Linq;
using System.Web.Mvc;
using Domain.Entities;
using Domain.Supermodel.Persistance;
using Supermodel.DDD.Models.View.Mvc.TwitterBS;
using Supermodel.DDD.UnitOfWork;
using Supermodel.Extensions;
using Supermodel.MvcAndWebApi.Controllers.MvcControllers.Sync;

namespace Web.TBS.Controllers
{
    [Authorize]
    public class ToDoListController : SyncEnhancedMvcCRUDController<ToDoList, ToDoListTBSMvcModel, TwitterBS.SimpleSearchMvcModel, TDMDbContext>
    {
        #region Overrdies
        protected override IQueryable<ToDoList> GetItems()
        {
            var currentUserId = TDMUser.GetCurrentUserId(HttpContext);
            return base.GetItems().Where(x => x.ListOwnerId == currentUserId);
        }
        protected override ActionResult AfterCreate(long id, ToDoList entityItem, ToDoListTBSMvcModel mvcModelItem)
        {
            // ReSharper disable once PossibleInvalidOperationException
            entityItem.ListOwnerId = (long)TDMUser.GetCurrentUserId(HttpContext);

            UnitOfWorkContext.FinalSaveChanges();
            TempData.Supermodel().NextPageModalMessage = "To Do List Created Successfully!";
            return StayOnDetailScreen(entityItem);
        }
        protected override IQueryable<ToDoList> ApplySearchBy(IQueryable<ToDoList> items, TwitterBS.SimpleSearchMvcModel searchBy)
        {
            items = base.ApplySearchBy(items, searchBy);
            var searchTerm = searchBy.SearchTerm.Value;
            if (!string.IsNullOrEmpty(searchTerm)) items = items.Where(l => l.Name.Contains(searchTerm) || l.ToDoItems.Any(i => i.Name.Contains(searchTerm)));
            return items;
        }
        #endregion
    }
}