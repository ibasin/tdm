using Android.App;
using Supermodel.Mobile.XForms.App;
using TDM.Shared.AppCore;

namespace TDM.Droid
{
    [Activity(Label = "TDM.Droid", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : FormsApplication<TDMApp> { }
}

