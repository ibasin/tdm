using System.Threading.Tasks;
using Supermodel.Mobile.Runtime.Models;
using Supermodel.Mobile.XForms.App;
using Supermodel.Mobile.XForms.Pages.Login;
using Xamarin.Forms;
using TDM.Shared.AppCore;
using TDM.Shared.Pages.MyToDoListList;
using TDM.Shared.Supermodel.Auth;
using TDM.Shared.Supermodel.Persistance;

namespace TDM.Shared.Pages.Login
{
    public class LoginPage : UsernameAndPasswordLoginPage<TDMUserUpdatePassword, TDMWebApiDataContext>
    {
        #region Constructors
        public LoginPage()
        {
            TDMApp.RunningApp.LoginPage = this;

            var logoImage = new Image
            {
                Aspect = Aspect.AspectFit,
                #pragma warning disable 618
                Source = ImageSource.FromResource(Device.OnPlatform("TDM.iOS.EmbeddedResources.Logo.png", "TDM.Droid.EmbeddedResources.Logo.png", "TDM.UWP.EmbeddedResources.Logo.png")),
                #pragma warning restore 618
                WidthRequest = 200,
                HorizontalOptions = LayoutOptions.CenterAndExpand

            };

            LoginView.ContentView.SetUpLoginImage(logoImage);
        }
        #endregion

        #region Overrdies
        public override async Task<bool> OnSuccessfulLoginAsync(bool autologin, bool isJumpBack)
        {
            var myToDoListListPage = FormsApplication<TDMApp>.RunningApp.MyToDoListListPage = (MyToDoListListPage)new MyToDoListListPage().Init("My Lists");
            await Navigation.PushAsync(myToDoListListPage);
            if (!isJumpBack) await myToDoListListPage.LoadListContentAsync();
            return true;
        }
        public override IAuthHeaderGenerator GetAuthHeaderGenerator(UsernameAndPasswordLoginViewModel loginViewModel)
        {
            return new TDMSecureAuthHeaderGenerator(loginViewModel.Username, loginViewModel.Password, TDMApp.RunningApp.LocalStorageEncryptionKey);
        }
        #endregion
    }
}
