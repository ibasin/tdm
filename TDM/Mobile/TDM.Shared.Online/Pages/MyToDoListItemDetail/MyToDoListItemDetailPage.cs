﻿using Supermodel.Mobile.Runtime.Models;
using Supermodel.Mobile.XForms.Pages.CRUDDetail;
using TDM.Shared.AppCore;
using TDM.Shared.Supermodel.Persistance;

namespace TDM.Shared.Pages.MyToDoListItemDetail
{
    public class MyToDoListItemDetailPage : CRUDChildDetailPage<ToDoList, ToDoItem, ToDoListItemXFModel, TDMWebApiDataContext>
    {
        #region Constructors
        public MyToDoListItemDetailPage()
        {
            TDMApp.RunningApp.MyToDoListItemDetailPage = this;
        }
        #endregion

        #region Overrides
        public override void OnLoad()
        {
            base.OnLoad();
            XFModel.UpdateActive();
        }
        #endregion

        #region Proeprties
        protected override bool CancelButton => true;
        #endregion
    }
}
