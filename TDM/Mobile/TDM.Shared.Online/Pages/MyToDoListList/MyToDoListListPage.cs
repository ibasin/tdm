﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Supermodel.Mobile.Repository;
using Supermodel.Mobile.Runtime.Models;
using Supermodel.Mobile.UnitOfWork;
using Supermodel.Mobile.XForms.App;
using Supermodel.Mobile.XForms.Pages.CRUDList;
using TDM.Shared.AppCore;
using TDM.Shared.Pages.MyToDoListDetail;
using TDM.Shared.Pages.Settings;
using TDM.Shared.Supermodel.Persistance;
using Xamarin.Forms;

namespace TDM.Shared.Pages.MyToDoListList
{
    public class MyToDoListListPage : EnhancedCRUDListPage<ToDoList, TDMWebApiDataContext>
    {
        #region Constrcuctors
        public MyToDoListListPage()
        {
            TDMApp.RunningApp.MyToDoListListPage = this;

            var settingsToolbarItem = new ToolbarItem("Settings", "settings.png", async() => 
            {
                var settingsPage = FormsApplication<TDMApp>.RunningApp.SettingsPage = new SettingsPage();
                await Navigation.PushAsync(settingsPage);
            });
            ToolbarItems.Add(settingsToolbarItem);
        }
        #endregion

        #region Overrdies
        protected override async Task OpenDetailInternalAsync(ToDoList model)
        {
            var detailPage = TDMApp.RunningApp.MyToDoListDetailPage = (MyToDoListDetailPage)new MyToDoListDetailPage().Init(Models, model.IsNew ? "New List" : "Edit List", model);
            await Navigation.PushAsync(detailPage);
        }
        protected override string NewBtnIconFilename => "plus.png";
        #endregion
    }
}
