using System;
using System.Collections.ObjectModel;
using Supermodel.Mobile.Runtime.Models;
using Supermodel.Mobile.XForms.App;
using Supermodel.Mobile.XForms.Views;
using Xamarin.Forms;
using TDM.Shared.AppCore;
using TDM.Shared.Pages.ChangePassword;

namespace TDM.Shared.Pages.Settings
{
    public class SettingsPage : ContentPage
    {
        #region Constructors
        public SettingsPage()
        {
            TDMApp.RunningApp.SettingsPage = this;

            var section = new TableSection();
            var cell = new TextCell { Text = "Change Password >" };
            
            #pragma warning disable CS0618 // Type or member is obsolete
            #pragma warning disable 612
            if (Device.OS == TargetPlatform.iOS) cell.TextColor = Color.FromHex("#007AFF");
            #pragma warning restore 612
            #pragma warning restore CS0618 // Type or member is obsolete

            cell.Tapped += async (sender, args) =>
            {
                if (!FormsApplication<TDMApp>.RunningApp.AuthHeaderGenerator.UserId.HasValue) throw new Exception("AuthHeaderGenerator.UserId must have value");
                var model = new TDMUserUpdatePassword { Id = FormsApplication<TDMApp>.RunningApp.AuthHeaderGenerator.UserId.Value };
                await FormsApplication<TDMApp>.RunningApp.LoginPage.Navigation.PushAsync(new ChangePasswordPage().Init(new ObservableCollection<TDMUserUpdatePassword> { model }, "Change Password", model));
            };
            section.Add(cell);

            Content = new ViewWithActivityIndicator<TableView>(new TableView { Intent = TableIntent.Form, HasUnevenRows = true, Root = new TableRoot{ section } });
        }
        #endregion
    }
}
