using System.Threading.Tasks;
using Supermodel.Mobile.Runtime.Models;
using Supermodel.Mobile.UnitOfWork;
using Supermodel.Mobile.XForms.App;
using Supermodel.Mobile.XForms.Pages.Login;
using TDM.Shared.AppCore;
using TDM.Shared.Supermodel.Persistance;
using Supermodel.Mobile.XForms.Pages.CRUDDetail;

namespace TDM.Shared.Pages.ChangePassword
{
    public class ChangePasswordPage : CRUDDetailPage<TDMUserUpdatePassword, TDMUserUpdatePasswordXFModel, TDMWebApiDataContext>
    {
        #region Constructors
        public ChangePasswordPage()
        {
            TDMApp.RunningApp.ChangePasswordPage = this;
        }
        #endregion

        #region Overrides
        protected override async Task SaveItemInternalAsync(TDMUserUpdatePassword model)
        {
            if (!string.IsNullOrEmpty(model.OldPassword) || !string.IsNullOrEmpty(model.NewPassword) || !string.IsNullOrEmpty(model.ConfirmPassword))
            {
                using (TDMApp.RunningApp.NewUnitOfWork<TDMWebApiDataContext>())
                {
                    model.Update();
                    await UnitOfWorkContext.FinalSaveChangesAsync();
                    var basicAuthHeader = (BasicAuthHeaderGenerator)FormsApplication<TDMApp>.RunningApp.AuthHeaderGenerator;
                    basicAuthHeader.Password = model.NewPassword;
                    await basicAuthHeader.SaveToAppPropertiesAsync();
                }
            }
        }
        #endregion
    }
}
