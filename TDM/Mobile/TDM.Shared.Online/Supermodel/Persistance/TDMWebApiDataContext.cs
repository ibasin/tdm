using Supermodel.Mobile.DataContext.WebApi;

namespace TDM.Shared.Supermodel.Persistance
{
    public class TDMWebApiDataContext : WebApiDataContext
    {
        #region Overrides
        public override string BaseUrl => "http://10.211.55.9:42415/api/";
        #endregion
    }
}
