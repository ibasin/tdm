using Supermodel.Mobile.UnitOfWork;

namespace TDM.Shared.Supermodel.Persistance
{
    public class TDMCachedWebApiUnitOfWork : UnitOfWork<TDMCachedWebApiDataContext>
    {
        #region Constructors
        public TDMCachedWebApiUnitOfWork() { }
        public TDMCachedWebApiUnitOfWork(ReadOnly readOnly) : base(readOnly) { }
        #endregion
    }
}
