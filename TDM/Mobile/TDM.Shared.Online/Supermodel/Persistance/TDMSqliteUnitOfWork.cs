using Supermodel.Mobile.UnitOfWork;

namespace TDM.Shared.Supermodel.Persistance
{
    public class TDMSqliteUnitOfWork : UnitOfWork<TDMSqliteDataContext>
    {
        #region Constructors
        public TDMSqliteUnitOfWork() { }
        public TDMSqliteUnitOfWork(ReadOnly readOnly) : base(readOnly) { }
        #endregion
    }
}
