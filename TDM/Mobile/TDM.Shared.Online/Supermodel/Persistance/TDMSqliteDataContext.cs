using System.Threading.Tasks;
using Supermodel.Mobile.DataContext.Sqlite;

namespace TDM.Shared.Supermodel.Persistance
{
    public class TDMSqliteDataContext : SqliteDataContext
    {
        #region Overrides
        //Optionally: Put your DbFileName here. For exmample if you need use multiple dbs 
        public override string DbFileName => base.DbFileName;

        //Optionally: Put your schema version here 
        public override int ContextSchemaVersion => base.ContextSchemaVersion;

        //Optionally: Put your schema migration code here
        public override Task MigrateDbAsync(int? fromVersion, int toVersion)
        {
            return base.MigrateDbAsync(fromVersion, toVersion);
        }
        #endregion
    }
}
