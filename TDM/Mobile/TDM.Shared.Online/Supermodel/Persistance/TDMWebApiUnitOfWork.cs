using Supermodel.Mobile.UnitOfWork;

namespace TDM.Shared.Supermodel.Persistance
{
    public class TDMWebApiUnitOfWork : UnitOfWork<TDMWebApiDataContext>
    {
        #region Constructors
        public TDMWebApiUnitOfWork() { }
        public TDMWebApiUnitOfWork(ReadOnly readOnly) : base(readOnly) { }
        #endregion
    }
}
