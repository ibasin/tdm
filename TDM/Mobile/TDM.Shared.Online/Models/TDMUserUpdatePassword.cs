using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Supermodel.Mobile.Attributes;
using Supermodel.Mobile.ReflectionMapper;
using Supermodel.Mobile.XForms.UIComponents;
using Supermodel.Mobile.XForms.ViewModels;

namespace Supermodel.Mobile.Runtime.Models
{
    public class TDMUserUpdatePasswordXFModel : XFModelForModel<TDMUserUpdatePassword>
    {
        #region Validation
        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var vr = (ValidationResultList)base.Validate(validationContext) ?? new ValidationResultList();
            //We only check if anyhting is filled in
            if (!string.IsNullOrEmpty(OldPassword.Text) || !string.IsNullOrEmpty(NewPassword.Text) || !string.IsNullOrEmpty(ConfirmPassword.Text))
            {
                if (NewPassword.Text != ConfirmPassword.Text) vr.AddValidationResult(this, x => x.NewPassword, x=> x.ConfirmPassword, "Passwords Must Equal");
                if (string.IsNullOrEmpty(OldPassword.Text)) vr.AddValidationResult(this, x => x.OldPassword, "The Old Password field is required");
                if (string.IsNullOrEmpty(NewPassword.Text)) vr.AddValidationResult(this, x => x.NewPassword, "The New Password field is required");
                if (string.IsNullOrEmpty(ConfirmPassword.Text)) vr.AddValidationResult(this, x => x.ConfirmPassword, "The Confirm Password field is required");
            }
            return vr;
        }
        #endregion

        #region Properties
        [ForceRequiredLabel] public TextBoxXFModel OldPassword { get; set; } = new TextBoxXFModel { TextEntry = { IsPassword = true } };
        [ForceRequiredLabel] public TextBoxXFModel NewPassword { get; set; } = new TextBoxXFModel { TextEntry = { IsPassword = true } };
        [ForceRequiredLabel] public TextBoxXFModel ConfirmPassword { get; set; } = new TextBoxXFModel{ TextEntry = { IsPassword = true } };
        #endregion
    }

    public partial class TDMUserUpdatePassword
    {
        //put your additional stuff here, if you need it
    }
}
