using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Xamarin.Forms;
using Supermodel.Mobile.Models;
using Supermodel.Mobile.XForms.UIComponents;
using Supermodel.Mobile.XForms.ViewModels;

namespace Supermodel.Mobile.Runtime.Models
{
    //Note that this is XFModel for ToDoList Model
    public class ToDoListItemXFModel : XFModelForChildModel<ToDoList, ToDoItem>
    {
        #region Methods
        public void UpdateActive()
        {
            Name.Active = Priority.Active = DueOn.Active = !Completed.IsToggled;
        }
        #endregion

        #region Properties
        [Required] public TextBoxXFModel Name { get; set; } = new TextBoxXFModel();
        public DateXFModel DueOn { get; set; } = new DateXFModel();
        public DropdownXFModelUsingEnum<PriorityEnum> Priority { get; set; } = new DropdownXFModelUsingEnum<PriorityEnum>();
        public ToggleSwitchXFModel Completed { get; set; } = new ToggleSwitchXFModel
        {
            OnChanged = page =>
            {
                page.GetXFModel<ToDoListItemXFModel>().UpdateActive();
            }
        };
        #endregion
    }

    public partial class ToDoItem : DirectChildModel
    {
        #region Overrides
        public override Cell ReturnACell()
        {
            return new ImageCell();
        }
        public override void SetUpBindings(DataTemplate dataTemplate)
        {
            // ReSharper disable AccessToStaticMemberViaDerivedType
            dataTemplate.SetBinding(ImageCell.TextProperty, nameof(Name));
            dataTemplate.SetBinding(ImageCell.DetailProperty, nameof(ItemsCount));
            // ReSharper restore AccessToStaticMemberViaDerivedType

            dataTemplate.SetBinding(ImageCell.ImageSourceProperty, nameof(IconImageSource));
        }
        #endregion

        #region Proeprties
        #pragma warning disable 618
        [JsonIgnore] public ImageSource IconImageSource => Completed ?
            #pragma warning disable 618
            ImageSource.FromResource(Device.OnPlatform("TDM.iOS.EmbeddedResources.Checked.png", "TDM.Droid.EmbeddedResources.Checked-droid.png", "TDM.UWP.EmbeddedResources.Checked.png")) :
            ImageSource.FromResource(Device.OnPlatform("TDM.iOS.EmbeddedResources.Unchecked.png", "TDM.Droid.EmbeddedResources.Unchecked-droid.png", "TDM.UWP.EmbeddedResources.Unchecked.png"));
            #pragma warning restore 618
        [JsonIgnore] public string ItemsCount => $"Due: {DueOn?.ToString("d")}  Priority: {Priority}";
        #pragma warning restore 618
        #endregion
    }
}