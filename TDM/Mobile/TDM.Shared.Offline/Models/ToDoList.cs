﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Xamarin.Forms;
using System.Linq;
using Supermodel.Mobile.XForms.UIComponents;
using Supermodel.Mobile.XForms.ViewModels;
using TDM.Shared.Supermodel.Persistance;
using TDM.Shared.AppCore;
using Supermodel.Mobile.DataContext.Core;
using Supermodel.Mobile.UnitOfWork;
using TDM.Shared.Pages.MyToDoListItemDetail;

namespace Supermodel.Mobile.Runtime.Models
{
    public class ToDoListXFModel : XFModelForModel<ToDoList>
    {
        #region Overrdies
        public override List<Cell> RenderDetail(Page parentPage, int screenOrderFrom = Int32.MinValue, int screenOrderTo = Int32.MaxValue)
        {
            var cells = new List<Cell>();

            if (screenOrderFrom <= 100 && screenOrderTo >= 100)
            {
                cells.AddRange(base.RenderDetail(parentPage, screenOrderFrom, screenOrderTo));
            }

            if (screenOrderFrom <= 200 && screenOrderTo >= 200)
            {
                //Prepare params
                var toDoLists = TDMApp.RunningApp.MyToDoListListPage.Models;
                var sortedItems = ToDoItems.OrderBy(x => x.Completed).ThenBy(x => x.DueOn).ThenBy(x => x.Priority).ToList();

                var childCells = RenderDeletableChildCells<ToDoItem, TDMSqliteDataContext>(parentPage, sortedItems, toDoLists, async (pg, item) =>
                {
                    var itemDetailPage = TDMApp.RunningApp.MyToDoListItemDetailPage = new MyToDoListItemDetailPage();
                    itemDetailPage.Init(toDoLists, "To Do Item", Model, item.ChildGuidIdentity, item.ParentGuidIdentities);
                    await pg.Navigation.PushAsync(itemDetailPage);
                });
                cells.AddRange(childCells);
            }
            return cells;
        }
        #endregion

        #region Properties
        [Required] public TextBoxXFModel Name { get; set; } = new TextBoxXFModel();
        [ScaffoldColumn(false)] public List<ToDoItem> ToDoItems { get; set; } = new List<ToDoItem>();
        #endregion
    }

    public partial class ToDoList
    {
        #region Constructors
        public ToDoList()
        {
            CreatedOnUtc = ModifiedOnUtc = DateTime.UtcNow;
        }
        #endregion

        #region Overrdies
        public override List<ChildModelT> GetChildList<ChildModelT>(params Guid[] parentIdentities)
        {
            if (parentIdentities == null) throw new ArgumentNullException(nameof(parentIdentities));
            if (parentIdentities.Length != 0) throw new Exception("ToDoList.GetChildList(): parentIdentities.Length != 0");
            return (List<ChildModelT>)(IEnumerable<ChildModelT>)ToDoItems;
        }
        //public override void AfterLoad()
        //{
        //    foreach(var toDoItem in ToDoItems) toDoItem.ParentIdentities = new Guid[0];
        //    base.AfterLoad();
        //}
        public override void BeforeSave(PendingAction.OperationEnum operation)
        {
            if (IsNew)
            {
                var userId = TDMApp.RunningApp.AuthHeaderGenerator.UserId;
                ListOwnerId = userId ?? throw new Exception("TDMApp.RunningApp.AuthHeaderGenerator.UserId == null");
            }
            if (!UnitOfWorkContext.CustomValues.ContainsKey("InSynchronizer")) ModifiedOnUtc = DateTime.UtcNow;
        }

        public override Cell ReturnACell()
        {
            return new ImageCell();
        }
        public override void SetUpBindings(DataTemplate dataTemplate)
        {
            // ReSharper disable AccessToStaticMemberViaDerivedType
            dataTemplate.SetBinding(ImageCell.TextProperty, nameof(Name));
            dataTemplate.SetBinding(ImageCell.DetailProperty, nameof(ItemsCount));
            // ReSharper restore AccessToStaticMemberViaDerivedType

            dataTemplate.SetBinding(ImageCell.ImageSourceProperty, nameof(IconImageSource));
        }
        #endregion

        #region Properties
        #pragma warning disable 618
        [JsonIgnore] public ImageSource IconImageSource => ImageSource.FromResource($"TDM.{Device.OnPlatform("iOS", "Droid", "UWP")}.EmbeddedResources.List.png");
        [JsonIgnore] public string ItemsCount => $"Completed: {ToDoItems.Count(x => x.Completed)} Pending:{ToDoItems.Count(x => !x.Completed)}";
        #pragma warning restore 618
        #endregion
    }
}
