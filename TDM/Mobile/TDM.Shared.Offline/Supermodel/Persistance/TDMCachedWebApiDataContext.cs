using Supermodel.Mobile.DataContext.CachedWebApi;

namespace TDM.Shared.Supermodel.Persistance
{
    public class TDMCachedWebApiDataContext : CachedWebApiDataContext<TDMWebApiDataContext, TDMSqliteDataContext> {}
}
