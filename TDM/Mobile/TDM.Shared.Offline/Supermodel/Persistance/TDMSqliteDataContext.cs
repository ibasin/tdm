using System;
using System.Text;
using System.Threading.Tasks;
using Supermodel.Mobile.DataContext.Sqlite;
using Supermodel.Mobile.Models;
using Supermodel.Mobile.ReflectionMapper;
using Supermodel.Mobile.Repository;
using Supermodel.Mobile.Runtime.Models;
using Supermodel.Mobile.UnitOfWork;
using TDM.Shared.AppCore;

namespace TDM.Shared.Supermodel.Persistance
{
    public class TDMSqliteDataContext : SqliteDataContext
    {
        #region Overrides
        //Optionally: Put your DbFileName here. For exmample if you need use multiple dbs 
        public override string DbFileName => base.DbFileName;

        //Optionally: Put your schema version here 
        public override int ContextSchemaVersion => 2;

        //Optionally: Put your schema migration code here
        public override async Task MigrateDbAsync(int? fromVersion, int toVersion)
        {
            if (fromVersion == 1 && toVersion == 2)
            {
                using (TDMApp.RunningApp.NewUnitOfWork<TDMSqliteDataContext>())
                {
                    var allToDoLists = await RepoFactory.Create<ToDoList>().GetAllAsync();
                    foreach (var toDoList in allToDoLists)
                    {
                        //Forces to update even if no changes were detected
                        toDoList.Update();
                    }
                    await UnitOfWorkContext.FinalSaveChangesAsync();
                }
            }
            else
            {
                await base.MigrateDbAsync(fromVersion, toVersion);
            }
        }

        public override string GetWhereClause<ModelT>(object searchBy, string sortBy)
        {
            var result = new StringBuilder();

            var searchTerm = (string)searchBy.PropertyGet(nameof(SimpleSearch.SearchTerm));
            if (!string.IsNullOrEmpty(searchTerm)) result.Append($"AND Index0 LIKE '%{searchTerm}%'");

            if (!string.IsNullOrEmpty(sortBy)) throw new Exception("Unexpected sortBy");

            return result.ToString();
        }

        public override string GetStringIndex<ModelT>(int idxNum0To9, ModelT model)
        {
            var toDoList = model as ToDoList;
            if (toDoList != null)
            {
                if (idxNum0To9 == 0)
                {
                    var sb = new StringBuilder();
                    sb.AppendLine(toDoList.Name);
                    foreach (var toDoItem in toDoList.ToDoItems) sb.AppendLine(toDoItem.Name);
                    return sb.ToString();
                }
                return null;
            }
            return base.GetStringIndex(idxNum0To9, model);
        }
        #endregion
    }
}
