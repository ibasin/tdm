using System;
using System.Linq;
using System.Threading.Tasks;
using Supermodel.Mobile.Runtime.Models;
using Supermodel.Mobile.XForms.Pages.Login;
using Xamarin.Forms;
using TDM.Shared.AppCore;
using TDM.Shared.Pages.MyToDoListList;
using TDM.Shared.Supermodel.Auth;
using TDM.Shared.Supermodel.Persistance;

namespace TDM.Shared.Pages.Login
{
    public class LoginPage : UsernameAndPasswordLoginPage<TDMUserUpdatePassword, TDMWebApiDataContext>
    {
        #region Constructors
        public LoginPage()
        {
            TDMApp.RunningApp.LoginPage = this;

            AutologinIfConnectionLost = true;

            var logoImage = new Image
            {
                Aspect = Aspect.AspectFit,
                #pragma warning disable 618
                Source = ImageSource.FromResource(Device.OnPlatform("TDM.iOS.EmbeddedResources.Logo.png", "TDM.Droid.EmbeddedResources.Logo.png", "TDM.UWP.EmbeddedResources.Logo.png")),
                #pragma warning restore 618
                WidthRequest = 200,
                HorizontalOptions = LayoutOptions.CenterAndExpand

            };

            LoginView.ContentView.SetUpLoginImage(logoImage);
        }
        #endregion

        #region Overrdies
        public override async Task<bool> OnSuccessfulLoginAsync(bool autologin, bool isJumpBack)
        {
            if (!autologin)
            {
                await DisplayAlert("First Sync", "Since this is your first login, you must sync before you can continue.", "Run First Sync");
                var synchronizer = new TDMSynchronizer();
                try
                {
                    LoginView.Message = "Synchronizing...";
                    await TDMApp.RunningApp.DeleteAllDataAsync();
                    await synchronizer.SynchronizeAsync();
                    await DisplayAlert("Success", "Synchronization with the cloud completed", "Ok");
                }
                catch (Exception)
                {
                    await DisplayAlert("Error", "Synchronization with the cloud could not be completed", "Ok");
                    return false;
                }
            }

            var myToDoListListPage = TDMApp.RunningApp.MyToDoListListPage = (MyToDoListListPage)new MyToDoListListPage().Init("My Lists");
            await Navigation.PushAsync(myToDoListListPage);
            if (!isJumpBack) await myToDoListListPage.LoadListContentAsync();
            return true;
        }
        protected override async Task<bool> OnConfirmedLogOutAsync()
        {
            var synchronizer = new TDMSynchronizer();
            if (TDMApp.RunningApp.MyToDoListListPage.Models.Any(x => synchronizer.IsUploadPending(x)))
            {
                var answer = await DisplayAlert("Alert", "Some of your lists have pending changes that need to be synchronized with the cloud. If you log out, you will lose your changes.", "Sign Out", "Go Back");
                return answer;
            }
            return true;
        }
        public override IAuthHeaderGenerator GetAuthHeaderGenerator(UsernameAndPasswordLoginViewModel loginViewModel)
        {
            return new TDMSecureAuthHeaderGenerator(loginViewModel.Username, loginViewModel.Password, TDMApp.RunningApp.LocalStorageEncryptionKey);
        }
        #endregion
    }
}
