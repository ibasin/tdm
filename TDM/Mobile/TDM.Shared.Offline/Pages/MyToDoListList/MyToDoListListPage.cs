﻿using System;
using System.Threading.Tasks;
using Supermodel.Mobile.Runtime.Models;
using Supermodel.Mobile.XForms.App;
using Supermodel.Mobile.XForms.Pages.CRUDList;
using Supermodel.Mobile.XForms.Views;
using TDM.Shared.AppCore;
using TDM.Shared.Pages.MyToDoListDetail;
using TDM.Shared.Pages.Settings;
using TDM.Shared.Supermodel.Persistance;
using Xamarin.Forms;

namespace TDM.Shared.Pages.MyToDoListList
{
    public class MyToDoListListPage : EnhancedCRUDListPage<ToDoList, TDMSqliteDataContext>
    {
        #region Constrcuctors
        public MyToDoListListPage()
        {
            TDMApp.RunningApp.MyToDoListListPage = this;

            var settingsToolbarItem = new ToolbarItem("Settings", "settings.png", async() => 
            {
                var settingsPage = FormsApplication<TDMApp>.RunningApp.SettingsPage = new SettingsPage();
                await Navigation.PushAsync(settingsPage);
            });
            ToolbarItems.Add(settingsToolbarItem);

            var synchToolbarItem = new ToolbarItem("Synch", "synch.png", async () => 
            {
                using (new ActivityIndicatorFor(ListView.ListPanel))
                {
                    try
                    {
                        var synchronizer = new TDMSynchronizer();
                        await synchronizer.SynchronizeAsync();
                        await LoadListContentAsync(searchTerm: ListView.SearchBar.Text, showActivityIndicator: false);
                        await DisplayAlert("Success", "Synchronization with the cloud completed.", "Ok");
                    }
                    catch (Exception)
                    {
                        await DisplayAlert("Error", "Synchronization with the cloud could not be completed.", "Ok");
                    }
                }
            });
            ToolbarItems.Add(synchToolbarItem);
        }
        #endregion

        #region Overrdies
        protected override async Task OpenDetailInternalAsync(ToDoList model)
        {
            var detailPage = TDMApp.RunningApp.MyToDoListDetailPage = (MyToDoListDetailPage)new MyToDoListDetailPage().Init(Models, model.IsNew ? "New List" : "Edit List", model);
            await Navigation.PushAsync(detailPage);
        }
        protected override string NewBtnIconFilename => "plus.png";
        #endregion
    }
}
