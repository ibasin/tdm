using Supermodel.Mobile.UnitOfWork;

namespace TDMClient.Cmd.Supermodel.Persistance
{
    public class TDMWebApiUnitOfWork : UnitOfWork<TDMWebApiDataContext>
    {
        #region Constructors
        public TDMWebApiUnitOfWork() { }
        public TDMWebApiUnitOfWork(ReadOnly readOnly) : base(readOnly) { }
        #endregion
    }
}
