﻿using System;
using System.Collections.Generic;
using System.Linq;
using Supermodel.Mobile.DataContext.Core;
using Supermodel.Mobile.Repository;
using Supermodel.Mobile.Runtime.Models;
using Supermodel.Mobile.UnitOfWork;
using TDMClient.Cmd.Supermodel.Auth;
using TDMClient.Cmd.Supermodel.Persistance;

namespace TDMClient.Cmd
{
    class Program
    {
        static void Main(string[] args)
        {
            DoStuffAsync();
            while(!_done);
        }

        private static async void DoStuffAsync()
        {
            var authGen = new TDMSecureAuthHeaderGenerator("ilya.basin@noblis.org", "1234", null);
            using (new TDMWebApiUnitOfWork(ReadOnly.Yes))
            {
                UnitOfWorkContext.AuthHeader = authGen.CreateAuthHeader();
                Console.ReadLine();
                var output = await UnitOfWorkContext<TDMWebApiDataContext>.CurrentDataContext.DeleteAllBeforeAsync(new DeleteAllBeforeInput { OlderThanUtc = DateTime.UtcNow });

                var repo = RepoFactory.Create<ToDoList>();
                Console.WriteLine($"Count: {await repo.GetCountAllAsync() }");
                Console.WriteLine($"Count: {await repo.GetCountWhereAsync(new SimpleSearch { SearchTerm = "KK" }) }");

                var allToDoLists = await repo.GetAllAsync();
                Print(allToDoLists);

                Console.WriteLine();

                var searchedToDoLists = await repo.GetWhereAsync(new SimpleSearch { SearchTerm = "KK" });
                Print(searchedToDoLists);

                var list2 = await repo.GetByIdAsync(2);
                var list5 = await repo.GetByIdOrDefaultAsync(5);
            }

            using (new TDMWebApiUnitOfWork())
            {
                UnitOfWorkContext.AuthHeader = authGen.CreateAuthHeader();

                var repo = RepoFactory.Create<ToDoList>();

                var list2 = await repo.GetByIdAsync(2);
                list2.Name = "Twice Renamed ToDo list";

                //var list1 = await repo.GetByIdAsync(1);
                //list1.Name = ""; //Invalid

                //var updatePassword = new TDMUserUpdatePassword{ Id = 1, OldPassword = "123", NewPassword = "123", ConfirmPassword = "123" };
                //updatePassword.Update();

                //var newList = new ToDoList { ListOwnerId = 1, Name = "New List", ToDoItems = new List<ToDoItem> { new ToDoItem { Name = "New Item"} } };
                //newList.Add();

                var newList = (await repo.GetWhereAsync(new SimpleSearch { SearchTerm = "New List"})).Single();
                newList.ToDoItems.Single().Name = "Not so new list3";
                //newList.ToDoItems.Clear();
                //newList.ToDoItems.Add(new ToDoItem { Name = "Newest Item" });

                UnitOfWorkContext.DetectUpdates();

                DelayedModels<ToDoList> delayedAll;
                repo.DelayedGetAll(out delayedAll);

                await UnitOfWorkContext.FinalSaveChangesAsync();

                Print(delayedAll.Values);
            }

            Console.WriteLine("Press Enter...");
            Console.ReadLine();
            _done = true;
        }

        private static void Print(List<ToDoList> toDoLists)
        {
            foreach (var toDoList in toDoLists)
            {
                Console.WriteLine($"List {toDoList.Id}: {toDoList.Name}");
                foreach (var toDoItem in toDoList.ToDoItems)
                {
                    Console.WriteLine($"\t{toDoItem.Name}");
                }
            }
        }

        private static bool _done;
    }
}
