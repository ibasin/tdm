using TDM.Shared.AppCore;

namespace TDM.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();

            LoadApplication(new TDMApp());
        }
    }
}
