using Foundation;
using Supermodel.Mobile.XForms.App;
using TDM.Shared.AppCore;

namespace TDM.iOS
{
    [Register("AppDelegate")]
    public class AppDelegate : FormsApplication<TDMApp> { }
}


